#!/usr/bin/env bash
set -e

export AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
export AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
export AWS_DEFAULT_REGION=eu-west-1

aws sns create-topic --name test1 --endpoint-url=http://localhost:4566

# screening queue
aws sqs create-queue --queue-name screening-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/screening-queue --endpoint-url=http://localhost:4566

# author verification queue
aws sqs create-queue --queue-name author-verification-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/author-verification-queue --endpoint-url=http://localhost:4566

# editor-suggestion-queue
aws sqs create-queue --queue-name editor-suggestion-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/editor-suggestion-queue --endpoint-url=http://localhost:4566

# file-conversion-service-queue
aws sqs create-queue --queue-name file-conversion-service-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/file-conversion-service-queue --endpoint-url=http://localhost:4566

# language-check-queue
aws sqs create-queue --queue-name language-check-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/language-check-queue --endpoint-url=http://localhost:4566

# sanction-list-queue
aws sqs create-queue --queue-name sanction-list-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/sanction-list-queue --endpoint-url=http://localhost:4566

# similarity-check-queue
aws sqs create-queue --queue-name similarity-check-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/similarity-check-queue --endpoint-url=http://localhost:4566

# suspicious-keywords-queue
aws sqs create-queue --queue-name suspicious-keywords-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/suspicious-keywords-queue --endpoint-url=http://localhost:4566

# app-review
aws sqs create-queue --queue-name review-queue --endpoint-url=http://localhost:4566
aws sns subscribe --topic-arn arn:aws:sns:eu-west-1:000000000000:test1 --protocol sqs --notification-endpoint http://localstack:4566/000000000000/review-queue --endpoint-url=http://localhost:4566

echo "Localstack setup done."