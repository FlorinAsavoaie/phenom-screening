#!/usr/bin/env bash

EB_ENVIRONMENT_NAME=demo-gsw-service-similarity-check
EB_APPLICATION_NAME=gsw
AWS_EB_PROFILE=HindawiReview

SM_NAME=demo-gsw/screening/similarity-check
SM_DESCRIPTION='Environment variables for demo-gsw service-similarity-check'
AWS_SM_PROFILE=AWSDevelopment

aws secretsmanager create-secret \
  --profile $AWS_SM_PROFILE \
  --name $SM_NAME \
  --description "$SM_DESCRIPTION" \
  --secret-string \
  "$(aws elasticbeanstalk describe-configuration-settings \
      --profile $AWS_EB_PROFILE \
      --environment-name $EB_ENVIRONMENT_NAME \
      --application-name $EB_APPLICATION_NAME \
      | jq -c '.ConfigurationSettings[0].OptionSettings[] | select( .Namespace | contains( "aws:elasticbeanstalk:application:environment"))  | {(.OptionName): .Value}' \
      | jq -s add)"
