#Specyfing CI/CD workflows config for a new service ( without workflow input )

Adding CI/CD workflows for a new service is as simple as cloning an existing file and adjusting the configuration inside it by changing the value  of a few variables.

##Examples

###Adding a new workflow for a NON-PROD environment (qa, qa-gamma, automation)

####1 - without workflow input (will become deprecated after enabling 2nd modality)

1. Clone service-editor-suggestion_HINDAWI_QA.yaml
2. Rename it based on the title of the new service by following the convention of this pattern: new-service_TENANT_ENVIRONMENT.yaml
3. Open the file
4. Read the following lines:
```
on:
  push:
    paths:
      - 'packages/service-editor-suggestion/**/*'
      - 'packages/lib-wos-data-store/**/*'
      - '.github/workflows/service-editor-suggestion_HINDAWI_DEMO-SALES.yaml'
      - '.github/workflows/generic-service-build-test-deploy.yaml'
      - 'packages/infrastructure-gaia/**/*'
```
5. Change the paths of the watched files accordingly - these will specify the content that will trigger the workflow when changes inside it occurs

6. Read the next following lines:
```
jobs:
  build-test-deploy:
    uses: './.github/workflows/generic-service-build-test-deploy.yaml'
    with:
      RUNS-ON: "['self-hosted', 'phenom-dev', '1x']"
      APP_NAME: service-editor-suggestion
      TENANT: hindawi
      ENVIRONMENT: qa
      K8S_CLUSTER_ID: hindawi-dev
      K8S_ENV_NAMESPACE: qa
      GITHUB_MANUAL_DEPLOY_ENVIRONMENT: qa-manual
```
7. Change the values of the variables `APP_NAME`, `TENANT`, `ENVIRONMENT` and `K8S_ENV_NAMESPACE` accordingly
8. Review changes
9. Push
10. Test (new worflows should appear in the right sidebar workflows list after accessing Actions section in the repo)

####2 - with workflow input
1. Clone service-editor-suggestion_NON-PROD.yaml
2. Rename it based on the title of the new service by following the convetion of this pattern: [new-service-name]_NON-PROD.yaml 
3. Open the file
4. Read the following lines:
```
on:
  workflow_dispatch:
    inputs:
      deploymentEnvironment:
        description: 'Prepare deployment for environment:'
        required: true
        type: choice
        default: 'qa'
        options:
        - qa
        - qa-gamma
        - automation
      tenant:
        description: 'Tenant:'
        required: true
        type: choice
        default: 'hindawi'
        options:
        - gsw
        - hindawi
```
5. Adjust the options for both inputs based on the existent environments and tenants for the corresponding service
6. Review changes
9. Push
10. Test:
- new workflow should be visible in the right sidebar workflows list after accesing Actions section in the repo
- after accesing the workflow, the inputs and their options should be visible in the 'Run workflow' dropdown located top-right above workflow schema
