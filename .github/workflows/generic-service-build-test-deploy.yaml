name: Build, test, deploy service

env: 
  AWS_REGION: 'eu-west-1'
  DOCKER_BUILDKIT: 1

on:
  workflow_call:
    inputs:
      RUNS-ON:
        required: true
        type: string
      APP_NAME:
        required: true
        type: string  
      TENANT:
        required: true
        type: string  
      ENVIRONMENT:
        required: true
        type: string  
      K8S_CLUSTER_ID:
        required: true
        type: string
      K8S_ENV_NAMESPACE:
        required: true
        type: string
      GITHUB_MANUAL_DEPLOY_ENVIRONMENT:
        required: true
        type: string
    secrets:
      FOSSA_API_KEY:
        required: true
      QA_SCHEMA_REGISTRY_URL:
        required: true

jobs:
  build-and-push-base-and-infra-docker-images:
    runs-on: ${{ fromJson(inputs.RUNS-ON) }}
    permissions:
      contents: write

    outputs:
      ECR_BUILD_IMAGE_FULL_PATH: ${{ steps.build-tag-push.outputs.ECR_BUILD_IMAGE_FULL_PATH }}
      ECR_INFRA_IMAGE_FULL_PATH: ${{ steps.build-tag-push-infra.outputs.ECR_INFRA_IMAGE_FULL_PATH }}
      ECR_PASS: ${{ steps.build-tag-push.outputs.ECR_PASS }}

    steps:
      - name: Checkout
        uses: actions/checkout@v3
        with:
          fetch-depth: 0

      - name: Login to Amazon ECR
        id: login-ecr
        uses: aws-actions/amazon-ecr-login@v1

      - name: Build, tag and push base docker image to Amazon ECR
        id: build-tag-push
        env:
          REGISTRY: ${{ steps.login-ecr.outputs.registry }}
          REPOSITORY: phenom-screening
          IMAGE_TAG: build-${{ github.sha }}
          IMAGE_TAG_LATEST: build-latest
          APP_NAME: ${{ inputs.APP_NAME }}
        run: |
          DOCKERFILE_PATH="packages/${APP_NAME}/Dockerfile"

          docker build \
            --cache-from $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST \
            --build-arg BUILDKIT_INLINE_CACHE=1 \
            --target='build' \
            --tag $REGISTRY/$REPOSITORY:$IMAGE_TAG \
            --tag $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST \
            -f $DOCKERFILE_PATH .

          docker push $REGISTRY/$REPOSITORY:$IMAGE_TAG
          docker push $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST

          echo "::set-output name=ECR_PASS::$(aws ecr get-login-password --region $AWS_DEFAULT_REGION)"

          BUILD_IMAGE_FULL_PATH=$REGISTRY/$REPOSITORY:$IMAGE_TAG
          echo "::set-output name=ECR_BUILD_IMAGE_FULL_PATH::${BUILD_IMAGE_FULL_PATH}"

          echo "docker image repo: $REGISTRY/$REPOSITORY"
          echo "docker image tag : $IMAGE_TAG"
          echo "docker image can also be fetched with 'latest' tag"
      
      - name: Build, tag and push infra docker image to Amazon ECR
        id: build-tag-push-infra
        env: 
          REGISTRY: ${{ steps.login-ecr.outputs.registry }}
          REPOSITORY: phenom-screening
          IMAGE_TAG: infra-${{ github.sha }}
          IMAGE_TAG_LATEST: infra-latest
          DOCKERFILE_PATH: packages/infrastructure-gaia/Dockerfile
        run: |
          docker build \
            --cache-from $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST \
            --build-arg BUILDKIT_INLINE_CACHE=1 \
            --tag $REGISTRY/$REPOSITORY:$IMAGE_TAG \
            --tag $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST \
            -f $DOCKERFILE_PATH .

          docker push $REGISTRY/$REPOSITORY:$IMAGE_TAG
          docker push $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST

          INFRA_IMAGE_FULL_PATH=$REGISTRY/$REPOSITORY:$IMAGE_TAG
          echo "::set-output name=ECR_INFRA_IMAGE_FULL_PATH::${INFRA_IMAGE_FULL_PATH}"

          echo "docker image repo: $REGISTRY/$REPOSITORY" 
          echo "docker image tag: $IMAGE_TAG"

  run-tests:
    runs-on: ${{ fromJson(inputs.RUNS-ON) }}
    needs: build-and-push-base-and-infra-docker-images
    container:
      image: ${{ needs.build-and-push-base-and-infra-docker-images.outputs.ECR_BUILD_IMAGE_FULL_PATH }}
      credentials:
          username: AWS
          password: ${{ needs.build-and-push-base-and-infra-docker-images.outputs.ECR_PASS }}

    steps:
        - name: Run unit tests
          run: | 
              cd /app
              yarn test:ci
        - name: Validate GraphQL Schema
          env: 
            SERVICE: service-editor-suggestion
            FILE_PATH: ./src/typeDefs.graphqls
            SCHEMA_REGISTRY_URL: ${{ secrets.QA_SCHEMA_REGISTRY_URL }}
            PROPERTY: ''
          run: |
                yarn global add @phenom.pub/schema-registry-cli
                cd /app/packages/$SERVICE
                schema-registry-cli validate --path $FILE_PATH --property $PROPERTY --service $SERVICE --service-version $GITHUB_SHA
        - name: Checkout
          uses: actions/checkout@v3
          with:
            fetch-depth: 0      
        - name: Analyze deps with FOSSA   
          env:
            FOSSA_API_KEY: ${{ secrets.FOSSA_API_KEY }}
          run: |
                apk add --update curl bash
                curl -H "Cache-Control: no-cache" https://raw.githubusercontent.com/fossas/fossa-cli/master/install-latest.sh | bash
                fossa analyze

  build-and-push-production-build-docker-image:
    runs-on: ${{ fromJson(inputs.RUNS-ON) }}
    needs: [build-and-push-base-and-infra-docker-images, run-tests]
    permissions:
      contents: write
    
    outputs:
      ECR_PROD_BUILD_IMAGE_REPOSITORY: ${{ steps.build-tag-push.outputs.ECR_PROD_BUILD_IMAGE_REPOSITORY }}
      ECR_PROD_BUILD_IMAGE_TAG: ${{ steps.build-tag-push.outputs.ECR_PROD_BUILD_IMAGE_TAG }}  

    steps:
      - name: Checkout
        uses: actions/checkout@v3
        with:
          fetch-depth: 0

      - name: Login to Amazon ECR
        id: login-ecr
        uses: aws-actions/amazon-ecr-login@v1

      - name: Build, tag and push production build docker image to Amazon ECR
        id: build-tag-push
        env:
          REGISTRY: ${{ steps.login-ecr.outputs.registry }}
          REPOSITORY: phenom-screening
          IMAGE_TAG: ${{ github.sha }}
          IMAGE_TAG_LATEST: latest
          APP_NAME: ${{ inputs.APP_NAME }}
        run: |
          DOCKERFILE_PATH="packages/${APP_NAME}/Dockerfile"

          docker build \
            --cache-from ${{ needs.build-and-push-base-and-infra-docker-images.outputs.ECR_BUILD_IMAGE_FULL_PATH }} \
            --build-arg BUILDKIT_INLINE_CACHE=1 \
            --target='production' \
            --tag $REGISTRY/$REPOSITORY:$IMAGE_TAG \
            --tag $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST \
            -f $DOCKERFILE_PATH .

          docker push $REGISTRY/$REPOSITORY:$IMAGE_TAG
          docker push $REGISTRY/$REPOSITORY:$IMAGE_TAG_LATEST

          PROD_BUILD_IMAGE_REPOSITORY=$REGISTRY/$REPOSITORY
          echo "::set-output name=ECR_PROD_BUILD_IMAGE_REPOSITORY::${PROD_BUILD_IMAGE_REPOSITORY}"
          echo "::set-output name=ECR_PROD_BUILD_IMAGE_TAG::${IMAGE_TAG}"

          echo "docker image repo: $PROD_BUILD_IMAGE_REPOSITORY"
          echo "docker image tag: $IMAGE_TAG"


  prepare-deployment-manifest:
    runs-on: ${{ fromJson(inputs.RUNS-ON) }}
    needs: [build-and-push-base-and-infra-docker-images, build-and-push-production-build-docker-image]
    environment: ${{ inputs.GITHUB_MANUAL_DEPLOY_ENVIRONMENT }}
    container:
      image: ${{ needs.build-and-push-base-and-infra-docker-images.outputs.ECR_INFRA_IMAGE_FULL_PATH }}
      credentials:
          username: AWS
          password: ${{ needs.build-and-push-base-and-infra-docker-images.outputs.ECR_PASS }}
    env:
      CLUSTER: ${{ inputs.K8S_CLUSTER_ID }}
      TENANT: ${{ inputs.TENANT }}
      NODE_ENV: ${{ inputs.ENVIRONMENT }}
      APP: ${{ inputs.APP_NAME }}
      IMAGE_REPOSITORY: ${{ needs.build-and-push-production-build-docker-image.outputs.ECR_PROD_BUILD_IMAGE_REPOSITORY }}
      IMAGE_TAG: ${{ needs.build-and-push-production-build-docker-image.outputs.ECR_PROD_BUILD_IMAGE_TAG }}
    steps:
      - name: Prepare deployment manifest yaml for ${{ inputs.ENVIRONMENT }}
        run: |
          cd /app/packages/infrastructure-gaia
          yarn synth
      
      - name: Archive deployment artifacts
        uses: actions/upload-artifact@v3
        with:
          name: deployment-yaml
          path: |
            /app/packages/infrastructure-gaia/dist-k8s/ # contains *.k8s.yaml app specific deployment manifest file


  deploy:
    runs-on: ${{ fromJson(inputs.RUNS-ON) }}
    needs: [prepare-deployment-manifest]
    env: 
      APP: ${{ inputs.APP_NAME }}
      CLUSTER_NAME: ${{ inputs.K8S_CLUSTER_ID }}
      K8S_ENV_NAMESPACE: ${{ inputs.K8S_ENV_NAMESPACE }}
    steps:
      - name: Download deployment artifacts
        uses: actions/download-artifact@v3
        with:
          name: deployment-yaml
      - name: Deploy to {{ inputs.ENVIRONMENT }}
        run: |
          aws eks --region $AWS_REGION update-kubeconfig --name $CLUSTER_NAME
          kubectl apply -n $K8S_ENV_NAMESPACE -f $APP.k8s.yaml
          DEPLOYMENT_NAME=$(kubectl get deployment -n $K8S_ENV_NAMESPACE --selector=app=$APP -o jsonpath='{.items[0].metadata.name}')
          kubectl rollout status deployment $DEPLOYMENT_NAME -n $K8S_ENV_NAMESPACE
