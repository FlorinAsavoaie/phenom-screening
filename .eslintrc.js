const rules = {
  complexity: ['error', 10],
  'consistent-return': 0,
  'global-require': 0,
  'jsx-a11y/click-events-have-key-events': 0,
  'import/no-extraneous-dependencies': 0,
  'import/prefer-default-export': 0,
  'max-params': ['error', 3],
  'no-console': [
    'error',
    {
      allow: ['error', 'warn', 'info'],
    },
  ],
  'no-param-reassign': 0,
  'no-shadow': 0,
  'no-underscore-dangle': 0,
  'react/prop-types': 0,
  'sort-keys': 0,
  'react/jsx-filename-extension': [
    1,
    {
      extensions: ['.js', '.jsx', '.tsx'],
    },
  ],
  'import/extensions': [
    'error',
    'always',
    {
      js: 'never',
      jsx: 'never',
      ts: 'never',
      tsx: 'never',
    },
  ],
  'react/jsx-uses-react': 'off',
  'react/react-in-jsx-scope': 'off',
  'no-useless-constructor': 0,
  'class-methods-use-this': 0,
}

module.exports = {
  env: {
    es6: true,
    browser: true,
  },
  extends: ['pubsweet'],
  parser: 'babel-eslint',
  rules,
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
      },
    },
  },
  overrides: [
    {
      files: ['packages/**/*.tsx', 'packages/**/*.ts'],
      extends: ['pubsweet', 'plugin:@typescript-eslint/recommended'],
      parserOptions: {
        ecmaFeatures: {
          jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
        project: './tsconfig.json',
      },
      rules: {
        ...rules,
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/member-delimiter-style': 0,
        '@typescript-eslint/no-floating-promises': [
          'error',
          { ignoreIIFE: true },
        ],
      },
      plugins: ['@typescript-eslint/eslint-plugin'],
    },
    {
      files: [
        'packages/infrastructure/**/*.ts',
        'packages/event-store-tool/**/*.ts',
        'packages/infrastructure-gaia/**/*.ts',
      ],
      rules: {
        'no-new': 0,
      },
    },
  ],
  globals: {
    ValidationError: true,
    NotFoundError: true,
    AuthorizationError: true,
    ConflictError: true,
    applicationEventBus: true,
  },
}
