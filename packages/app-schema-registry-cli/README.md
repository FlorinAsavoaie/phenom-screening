# `@phenom.pub/schema-registry-cli`

CLI tool to interact with the Phenom Schema Registry

## Usage

```shell
$ schema-registry-cli <command>

Commands:
  schema-registry-cli validate  validate provided schema against the Phenom
                                Schema Registry

Options:
  --help     Show help                                                 [boolean]
  --version  Show version number                                       [boolean]
```

```shell
$ schema-registry-cli validate

validate provided schema against the Phenom Schema Registry

Options:
  --help      Show help                                                [boolean]
  --version   Show version number                                      [boolean]
  --service   Name of the service for which the schema is validated
                                                             [string] [required]
  --path      Path to file exporting the typeDefs (either .js or .graphqls)
                                                             [string] [required]
  --property  Set in case the file provided at path exports an object.   
              The exported object must be a GraphQLSchema type          [string]
```

# Schema registry cli used in Phenom Apps

Schema registry cli is used by the Phenom Apps in 2 ways:  
-  in the gitlab pipelines to validate the graphql schema upon the registry on merge requests  
-  on start server to publish the new schema to the registry  - information for local development can be found [here](../app-gql-gateway/README.md)