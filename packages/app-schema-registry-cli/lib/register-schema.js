const { print } = require('graphql')
const { get } = require('lodash')

const fetch = (...args) =>
  import('node-fetch').then(({ default: fetch }) => fetch(...args))

const SCHEMA_REGISTRY_URL =
  process.env.SCHEMA_REGISTRY_URL || 'http://localhost:6001'

async function validateSchema(service, typeDefs) {
  return fetch(`${SCHEMA_REGISTRY_URL}/schema/validate`, {
    method: 'POST',
    body: JSON.stringify({
      name: service.name,
      version: service.version,
      type_defs: print(typeDefs),
    }),
    headers: { 'Content-Type': 'application/json' },
  }).then(async (response) => {
    if (response.ok) {
      return response.json()
    }
    throw new Error(await response.text())
  })
}

async function validateAndRegisterSchema(service, typeDefs) {
  const serviceTypeDefinitions = await fetch(
    `${SCHEMA_REGISTRY_URL}/schema/compose`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
    },
  ).then(async (response) => {
    if (response.ok) {
      return response.json()
    }
    throw new Error(await response.text())
  })

  const latestServiceSchema = get(serviceTypeDefinitions, 'data', []).find(
    (e) => e.name === service.name,
  )

  if (
    !latestServiceSchema ||
    print(typeDefs) !== latestServiceSchema.type_defs
  ) {
    const version = latestServiceSchema
      ? parseInt(latestServiceSchema.version, 10) + 1
      : 1

    return fetch(`${SCHEMA_REGISTRY_URL}/schema/push`, {
      method: 'POST',
      body: JSON.stringify({
        name: service.name,
        version: version.toString(),
        type_defs: print(typeDefs),
        url: service.url,
      }),
      headers: { 'Content-Type': 'application/json' },
    }).then(async (response) => {
      if (response.ok) {
        return response.json()
      }
      throw new Error(await response.text())
    })
  }
}

module.exports = {
  validateSchema,
  validateAndRegisterSchema,
}
