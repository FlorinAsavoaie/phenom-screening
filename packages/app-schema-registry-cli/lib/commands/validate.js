const path = require('path')
const { parse } = require('graphql')

const { validateSchema } = require('../register-schema')
const fs = require('fs')

module.exports = {
  command: 'validate',
  desc: 'validate provided schema against the Phenom Schema Registry',
  builder: {
    service: {
      desc: 'Name of the service for which the schema is validated',
      type: 'string',
      required: true,
    },
    'service-version': {
      desc: 'Service version',
      type: 'string',
      required: true,
    },
    path: {
      desc: 'Path to file exporting the typeDefs (either .js or .graphqls)',
      type: 'string',
      required: true,
    },
    property: {
      desc: 'Set in case the file provided at path exports an object. The exported object must be a GraphQLSchema type',
      type: 'string',
    },
  },
  handler: async function handler(argv) {
    const extension = path.extname(argv.path)
    let typeDefs
    if (extension === '.js') {
      const filePath = path.isAbsolute(argv.path)
        ? path
        : path.join(process.cwd(), argv.path)

      // eslint-disable-next-line import/no-dynamic-require
      const schema = require(filePath)
      typeDefs = await schema[argv.property]
    } else if (extension === '.graphqls' || extension === '.graphql') {
      const file = fs.readFileSync(path.join(process.cwd(), argv.path), 'utf8')
      typeDefs = parse(file)
    } else {
      throw new Error('File type not supported')
    }

    const service = {
      name: argv.service,
      version: argv.serviceVersion,
    }

    try {
      await validateSchema(service, typeDefs)

      console.info('Schema validated successfully!')
    } catch (err) {
      throw new Error(`Schema registration failed: ${err.message}`)
    }
  },
}
