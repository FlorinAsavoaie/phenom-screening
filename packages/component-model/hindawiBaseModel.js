const BaseModel = require('@pubsweet/base-model')
const logger = require('@pubsweet/logger')

class HindawiBaseModel extends BaseModel {
  static async find(id, eagerLoadRelations) {
    const object = await this.query()
      .findById(id)
      .withGraphFetched(eagerLoadRelations)

    if (!object) {
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)
    }

    return object
  }

  static async findOneBy({ queryObject, eagerLoadRelations }) {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .withGraphFetched(eagerLoadRelations)

    if (!object.length) {
      return
    }

    return object[0]
  }

  static async findBy({
    eagerLoadRelations,
    orderByField,
    queryObject,
    order,
  }) {
    const builder = this.query()
      .where(queryObject)
      .withGraphFetched(eagerLoadRelations)

    if (orderByField && order) {
      builder.orderBy(orderByField, order)
    }

    return builder
  }

  static async findIn(field, values, eagerLoadRelations) {
    return this.query()
      .whereIn(field, values)
      .withGraphFetched(eagerLoadRelations)
  }

  static async findAll(eagerLoadRelations) {
    return this.query().withGraphFetched(eagerLoadRelations)
  }

  static async updateBy({ queryObject, propertiesToUpdate }) {
    return this.query().patch(propertiesToUpdate).where(queryObject)
  }

  static async upsert(queryObject, entity) {
    const savedEntity = await this.findOneBy({
      queryObject,
    })

    if (savedEntity) {
      savedEntity.updateProperties(entity)
      return savedEntity.save()
    }

    return entity.save()
  }

  async deleteRelation(relationName, trx = null) {
    return this.$relatedQuery(relationName, trx).unrelate()
  }

  async selectProperty({ queryObject, relationName, property }) {
    const builder = this.$relatedQuery(relationName)

    const relation = await builder
    if (!relation.length) {
      logger.info(`No ${relationName} related`)
      return
    }

    const object = await builder.select(property).where(queryObject).first()

    return object && object[property]
  }

  static async deleteByIds(ids) {
    return this.query().delete().whereIn('id', ids)
  }

  static async insertGraph(object, options) {
    if (this.knex().isTransaction) {
      return this.query().insertGraph(object, options)
    }

    return this.knex().transaction(async (trx) =>
      this.query(trx).insertGraph(object, options),
    )
  }

  static async upsertGraph(object, options) {
    if (this.knex().isTransaction) {
      return this.query().upsertGraph(object, options)
    }

    return this.knex().transaction(async (trx) =>
      this.query(trx).upsertGraph(object, options),
    )
  }
}

module.exports = HindawiBaseModel
