const fs = require('fs')
const path = require('path')
const { merge, upperFirst } = require('lodash')
const { withAuthsome } = require('hindawi-utils')

const HindawiBaseModel = require('./hindawiBaseModel')

const baseSchema = fs.readFileSync(
  path.join(__dirname, 'baseSchema.graphqls'),
  'utf8',
)

function importModels() {
  const dirs = fs.readdirSync(path.join(__dirname, 'src'))

  return dirs.reduce(
    (acc, dir) => {
      const {
        model,
        useCases = {},
        typeDefs = '',
        resolvers = {},
        factory,
        mapper,
      } = require(path.join(__dirname, 'src', dir)) // eslint-disable-line import/no-dynamic-require

      return {
        ...acc,
        typeDefs: `${acc.typeDefs}${typeDefs}`,
        resolvers: merge(acc.resolvers, withAuthsome(resolvers, useCases)),
        models: [...acc.models, { model, modelName: upperFirst(dir) }, {}],
        factories: { ...acc.factories, [`${dir}Factory`]: factory },
        mappers: { ...acc.mappers, [`${dir}Mapper`]: mapper },
      }
    },
    {
      typeDefs: `${baseSchema}`,
      resolvers: {},
      models: [],
      factories: [],
      mappers: [],
    },
  )
}

module.exports = {
  HindawiBaseModel,
  ...importModels(),
}
