const { getManuscriptValidationsUseCase } = require('../use-cases')
const ManuscriptValidations = require('../manuscriptValidations')
const Chance = require('chance')

const chance = new Chance()

const models = {
  ManuscriptValidations,
}

describe('Get manuscript validations', () => {
  it('gets the validation checkbox values for a manuscript', async () => {
    const manuscriptId = chance.guid()

    ManuscriptValidations.findOneBy = jest.fn()

    await getManuscriptValidationsUseCase
      .initialize(models)
      .execute(manuscriptId)

    expect(ManuscriptValidations.findOneBy).toHaveBeenCalledWith({
      queryObject: { manuscriptId },
    })
  })
})
