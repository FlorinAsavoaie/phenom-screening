function initialize({ ManuscriptValidations }) {
  function execute(manuscriptId) {
    return ManuscriptValidations.findOneBy({
      queryObject: { manuscriptId },
    })
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
