const HindawiBaseModel = require('../../hindawiBaseModel')

class ManuscriptValidations extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript_validations'
  }
  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        includesCitations: { type: 'boolean', default: false },
        graphical: { type: 'boolean', default: false },
        differentAuthors: { type: 'boolean', default: false },
        missingAffiliations: { type: 'boolean', default: false },
        nonacademicAffiliations: { type: 'boolean', default: false },
        topicNotAppropriate: { type: 'boolean', default: false },
        articleTypeIsNotAppropriate: { type: 'boolean', default: false },
        manuscriptIsMissingSections: { type: 'boolean', default: false },
        missingReferences: { type: 'boolean', default: false },
        wrongReferences: { type: 'boolean', default: false },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'manuscript_validations.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }
}

module.exports = ManuscriptValidations
