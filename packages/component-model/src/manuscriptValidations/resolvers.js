const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async getManuscriptValidations(_, { manuscriptId }, ctx) {
      return useCases.getManuscriptValidationsUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
}
