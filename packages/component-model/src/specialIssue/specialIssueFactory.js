const SpecialIssue = require('./specialIssue')

module.exports = {
  create({ id, journalId, name, journalSectionId }) {
    return new SpecialIssue({ id, journalId, name, journalSectionId })
  },
}
