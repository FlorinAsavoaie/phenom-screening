function initialize({ SpecialIssue }) {
  return {
    execute,
  }

  async function execute(specialIssueId) {
    if (!specialIssueId) {
      return null
    }

    return SpecialIssue.findOneBy({
      queryObject: { id: specialIssueId },
    })
  }
}

module.exports = {
  initialize,
}
