const SpecialIssue = require('../specialIssue')
const useCases = require('../use-cases')

describe('Get Special Issue', () => {
  it('should return null if journalSectionId is null', async () => {
    const findOneBy = jest.spyOn(SpecialIssue, 'findOneBy')

    const journalSection = await useCases.getSpecialIssueUseCase
      .initialize({
        SpecialIssue,
      })
      .execute(null)

    expect(journalSection).toBe(null)
    expect(findOneBy).not.toHaveBeenCalled()
  })

  it('should return specialIssue section', async () => {
    const findOneBy = jest.spyOn(SpecialIssue, 'findOneBy').mockReturnValue({
      id: 'special-issue-id',
    })

    const specialIssue = await useCases.getSpecialIssueUseCase
      .initialize({
        SpecialIssue,
      })
      .execute('special-issue-id')

    expect(specialIssue).toStrictEqual({
      id: 'special-issue-id',
    })
    expect(findOneBy).toHaveBeenCalledWith({
      queryObject: {
        id: 'special-issue-id',
      },
    })
  })
})
