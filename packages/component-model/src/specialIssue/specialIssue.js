const HindawiBaseModel = require('../../hindawiBaseModel')

class SpecialIssue extends HindawiBaseModel {
  static get tableName() {
    return 'special_issue'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        journalId: { type: 'string', format: 'uuid' },
        journalSectionId: { type: ['string', 'null'], format: 'uuid' },
        name: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./../journal').model,
        join: {
          from: 'special_issue.journalId',
          to: 'journal.id',
        },
      },
      journalSection: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../journalSection').model,
        join: {
          from: 'special_issue.journalSectionId',
          to: 'journal_section.id',
        },
      },
    }
  }
}

module.exports = SpecialIssue
