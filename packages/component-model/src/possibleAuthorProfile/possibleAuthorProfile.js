const HindawiBaseModel = require('../../hindawiBaseModel')

class PossibleAuthorProfile extends HindawiBaseModel {
  static get tableName() {
    return 'possible_author_profile'
  }
  static get schema() {
    return {
      type: 'object',
      properties: {
        surname: { type: ['string', 'null'] },
        givenNames: { type: ['string', 'null'] },
        email: { type: ['string', 'null'] },
        affiliation: { type: ['string', 'null'] },
        noOfPublications: { type: ['integer', 'null'] },
        authorId: { type: 'string', format: 'uuid' },
        manuscriptId: { type: 'string', format: 'uuid' },
        chosen: { type: ['boolean'], default: false },
        weight: { type: ['integer'] },
      },
    }
  }

  static get relationMappings() {
    return {
      author: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../author').model,
        join: {
          from: 'possible_author_profile.authorId',
          to: 'author.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'possible_author_profile.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  static async getByAuthorIdAndManuscriptId({ authorId, manuscriptId }) {
    return PossibleAuthorProfile.query()
      .where({ authorId, manuscriptId })
      .orderBy('weight', 'desc')
      .orderBy('id', 'asc')
  }
}

module.exports = PossibleAuthorProfile
