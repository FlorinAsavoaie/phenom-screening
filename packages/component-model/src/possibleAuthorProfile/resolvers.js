const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async getPossibleAuthorProfiles(_, args, ctx) {
      return useCases.getPossibleAuthorProfilesUseCase
        .initialize(models)
        .execute(args)
    },
  },
}
