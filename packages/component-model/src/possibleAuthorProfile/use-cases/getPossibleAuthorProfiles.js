function initialize({ PossibleAuthorProfile }) {
  function execute({ authorId, manuscriptId }) {
    return PossibleAuthorProfile.getByAuthorIdAndManuscriptId({
      authorId,
      manuscriptId,
    })
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
