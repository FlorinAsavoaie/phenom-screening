const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./file'),
  factory: require('./fileFactory'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
