const File = require('./file')

module.exports = {
  createFile({ file, type }) {
    const providerKey =
      file.type === 'manuscript' && file.mimeType === 'application/pdf'
        ? file.providerKey
        : null

    return new File({
      type: type || file.type,
      fileName: file.fileName,
      originalName: file.originalName,
      originalFileProviderKey: file.providerKey,
      providerKey,
      size: file.size,
      mimeType: file.mimeType,
    })
  },
}
