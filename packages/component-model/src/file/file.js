const HindawiBaseModel = require('../../hindawiBaseModel')

class File extends HindawiBaseModel {
  static get tableName() {
    return 'file'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        providerKey: { type: ['string', 'null'] },
        originalFileProviderKey: { type: 'string' },
        type: { type: 'string' },
        originalName: { type: 'string' },
        fileName: { type: 'string' },
        manuscriptId: { type: ['string', 'null'], format: 'uuid' },
        reviewId: { type: ['string', 'null'], format: 'uuid' },
        mimeType: { type: 'string' },
        size: { type: 'integer' },
      },
    }
  }

  static get Types() {
    return {
      FIGURE: 'figure',
      MANUSCRIPT: 'manuscript',
      SUPPLEMENTARY: 'supplementary',
      COVER_LETTER: 'coverLetter',
      REVIEW_FILE: 'reviewFile',
    }
  }

  $beforeInsert() {
    super.$beforeInsert()
    if (!this.reviewId && !this.manuscriptId) {
      throw new Error('One of `reviewId` and `manuscriptId` should be null')
    }
  }

  static getTypeFormats(format) {
    const types = {
      converted: 'providerKey',
      original: 'originalFileProviderKey',
    }

    return types[format]
  }
}

module.exports = File
