process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const useCases = require('../use-cases')
const Chance = require('chance')

const chance = new Chance()

const ManuscriptChecks = require('../manuscriptChecks')

const models = { ManuscriptChecks }

describe('Get manuscript checks Use Case', () => {
  it('should get language percentage and word count', async () => {
    const mockManuscriptChecks = { suspiciousWords: { park: 1, test: 1 } }
    ManuscriptChecks.findOneBy = jest.fn().mockReturnValue(mockManuscriptChecks)

    const manuscriptId = chance.guid()

    const getManuscriptChecks = await useCases.getManuscriptChecksUseCase
      .initialize(models)
      .execute(manuscriptId)

    expect(ManuscriptChecks.findOneBy).toHaveBeenCalledWith({
      queryObject: { manuscriptId },
    })

    expect(getManuscriptChecks.suspiciousWords).toEqual({
      newSuspiciousWords: [{ word: 'park' }, { word: 'test' }],
      previouslyVerifiedSuspiciousWords: [],
    })
  })
})
