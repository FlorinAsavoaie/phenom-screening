const { forOwn } = require('lodash')

function initialize({ ManuscriptChecks }) {
  async function execute(manuscriptId) {
    const manuscriptChecks = await ManuscriptChecks.findOneBy({
      queryObject: { manuscriptId },
    })
    const suspiciousWords = []
    forOwn(manuscriptChecks.suspiciousWords, (value, key) => {
      const keyword = { ...value, word: key }
      suspiciousWords.push(keyword)
    })

    const previouslyVerifiedSuspiciousWords = suspiciousWords.filter(
      (word) => word.isPreviouslyVerified,
    )
    const newSuspiciousWords = suspiciousWords.filter(
      (word) => !word.isPreviouslyVerified,
    )

    return {
      ...manuscriptChecks,
      suspiciousWords: {
        previouslyVerifiedSuspiciousWords,
        newSuspiciousWords,
      },
    }
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
