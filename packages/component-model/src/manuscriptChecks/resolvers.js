const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async getManuscriptChecks(_, { manuscriptId }, ctx) {
      return useCases.getManuscriptChecksUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
}
