const config = require('config')
const HindawiBaseModel = require('../../hindawiBaseModel')
const models = require('@pubsweet/models')

class ManuscriptChecks extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript_checks'
  }
  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        languagePercentage: { type: ['integer', null] },
        wordCount: { type: ['integer', null] },
        suspiciousWords: { type: ['object', null] },
        firstSourceSimilarityPercentage: { type: ['integer', null] },
        totalSimilarityPercentage: { type: ['integer', null] },
        reportUrl: { type: ['string', null] },
        authorsVerified: { type: 'boolean', default: false },
        sanctionListCheckDone: { type: 'boolean', default: false },
        fileConverted: { type: 'boolean', default: false },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'manuscript_checks.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  static async areAllChecksPerformed(manuscriptId) {
    const manuscriptChecksConstrains =
      ManuscriptChecks.getManuscriptChecksConstrains()

    const object = await this.query()
      .where({ manuscriptId })
      .whereNot(manuscriptChecksConstrains)

    return !!object.length
  }

  static getManuscriptChecksConstrains() {
    let manuscriptChecksConstrains = {
      'manuscript_checks.language_percentage': null,
      'manuscript_checks.word_count': null,
      'manuscript_checks.suspicious_words': null,
      'manuscript_checks.authors_verified': false,
      'manuscript_checks.sanction_list_check_done': false,
      'manuscript_checks.file_converted': false,
    }

    if (config.get('similarityChecksServiceService') === 'true') {
      manuscriptChecksConstrains = {
        ...manuscriptChecksConstrains,
        'manuscript_checks.total_similarity_percentage': null,
        'manuscript_checks.first_source_similarity_percentage': null,
        'manuscript_checks.report_url': null,
      }
    }
    return manuscriptChecksConstrains
  }

  static async getPreviousSuspiciousWords(manuscriptId) {
    const manuscript = await models.Manuscript.find(manuscriptId)
    const manuscripts = await models.Manuscript.query()
      .where({
        submissionId: manuscript.submissionId,
      })
      .join(
        'manuscript_checks',
        'manuscript_checks.manuscriptId',
        '=',
        'manuscript.id',
      )
      .whereNot({ 'manuscript_checks.suspicious_words': null })
      .orderBy('version', 'desc')
      .orderBy('created', 'desc')

    if (!manuscripts.length) {
      return null
    }

    const checks = await ManuscriptChecks.findOneBy({
      queryObject: { manuscriptId: manuscripts[0].id },
    })

    return checks.suspiciousWords
  }
}

module.exports = ManuscriptChecks
