const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./manuscriptChecks'),
  useCases: require('./use-cases'),
  resolvers: require('./resolvers'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
