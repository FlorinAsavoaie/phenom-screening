const models = require('@pubsweet/models')
const useCases = require('./use-cases')

const resolvers = {
  Member: {
    async reviews(member) {
      return useCases.reviewsUseCase
        .initialize(models)
        .execute({ memberId: member.id })
    },
    async name(member) {
      return `${member.surname} ${member.givenNames}`
    },
  },
}

module.exports = resolvers
