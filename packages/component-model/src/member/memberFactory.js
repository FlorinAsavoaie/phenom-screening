const { get } = require('lodash')
const Member = require('./member')

module.exports = {
  createMember({ member, role }) {
    return new Member({
      surname: get(member, 'surname', null),
      givenNames: get(member, 'givenNames', null),
      aff: get(member, 'aff', null),
      country: get(member, 'country', null),
      title: get(member, 'title', null),
      reviewerNumber: member.reviewerNumber,
      phenomMemberId: member.id,
      phenomUserId: member.userId,
      email: member.email,
      role: role || member.role.type,
      roleLabel: get(member, 'role.label', null),
      reviews: [],
      status: member.status,
    })
  },
}
