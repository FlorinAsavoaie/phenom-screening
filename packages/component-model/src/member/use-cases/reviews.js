function initialize({ Review }) {
  async function execute({ memberId }) {
    return Review.findBy({
      queryObject: { memberId },
    })
  }

  return { execute }
}

module.exports = {
  initialize,
}
