const HindawiBaseModel = require('../../hindawiBaseModel')

const Manuscript = require('../manuscript/manuscript')

class Member extends HindawiBaseModel {
  static get tableName() {
    return 'member'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        surname: { type: ['string', 'null'] },
        givenNames: { type: ['string', 'null'] },
        email: { type: 'string' },
        aff: { type: ['string', 'null'] },
        country: { type: ['string', 'null'] },
        title: { type: ['string', 'null'] },
        manuscriptId: { type: 'string', format: 'uuid' },
        phenomMemberId: { type: 'string', format: 'uuid' },
        phenomUserId: { type: 'string', format: 'uuid' },
        role: { type: 'string' },
        roleLabel: { type: ['string', 'null'] },
        status: {
          type: 'string',
          enum: Object.values(Member.Status),
        },

        reviewerNumber: { type: ['integer', 'null'] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'member.manuscriptId',
          to: 'manuscript.id',
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../review').model,
        join: {
          from: 'member.id',
          to: 'review.memberId',
        },
      },
    }
  }

  static get Roles() {
    return {
      ACADEMIC_EDITOR: 'academicEditor',
      TRIAGE_EDITOR: 'triageEditor',
      REVIEWER: 'reviewer',
      EDITORIAL_ASSISTANT: 'editorialAssistant',
      SUBMITTING_STAFF_MEMBER: 'submittingStaffMember',
    }
  }

  static get Status() {
    return {
      PENDING: 'pending',
      ACCEPTED: 'accepted',
      DECLINED: 'declined',
      SUBMITTED: 'submitted',
      EXPIRED: 'expired',
      REMOVED: 'removed',
      ACTIVE: 'active',
      CONFLICTING: 'conflicting',
    }
  }

  static async getInvolvedReviewersFor(submissionId) {
    return Manuscript.query()
      .select('member.*')
      .distinctOn('email')
      .where({
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      })
      .leftJoin('member', 'member.manuscript_id', '=', 'manuscript.id')
      .where({
        role: Member.Roles.REVIEWER,
        'member.status': Member.Status.SUBMITTED,
      })
  }

  static async findAllReviewersBy(manuscriptIds, reviewersEmails) {
    const membersFromAllVersions = await Member.query()
      .select('phenom_member_id')
      .whereIn('manuscript_id', manuscriptIds)
      .whereIn('email', reviewersEmails)

    return membersFromAllVersions.map((m) => m.phenomMemberId)
  }

  static async getEditorialAssistant(manuscriptId) {
    const editorialAssistant = await this.query()
      .where({
        manuscriptId,
        role: Member.Roles.EDITORIAL_ASSISTANT,
        status: Member.Status.ACTIVE,
      })
      .first()

    if (!editorialAssistant) {
      throw new Error(
        `Manuscript ${manuscriptId} is not having an Editorial Assistant assigned`,
      )
    }

    return editorialAssistant
  }

  static async getSubmittingStaffMember(manuscriptId) {
    return Member.findOneBy({
      queryObject: {
        manuscriptId,
        role: Member.Roles.SUBMITTING_STAFF_MEMBER,
      },
    })
  }

  get fullName() {
    return `${this.givenNames} ${this.surname}`
  }

  hasReviews() {
    return this.reviews && this.reviews.length > 0
  }

  isReviewer() {
    return this.role === Member.Roles.REVIEWER
  }

  isAcademicEditor() {
    return this.role === Member.Roles.ACADEMIC_EDITOR
  }
}

module.exports = Member
