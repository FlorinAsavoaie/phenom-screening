const HindawiBaseModel = require('../../hindawiBaseModel')

class Journal extends HindawiBaseModel {
  static get tableName() {
    return 'journal'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: ['string'] },
        publisherName: { type: ['string', null] },
      },
    }
  }

  static get relationMappings() {
    return {
      teams: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('../team').model,
        join: {
          from: 'journal.id',
          through: {
            from: 'team_journal.journalId',
            to: 'team_journal.teamId',
          },
          to: 'team.id',
        },
      },
      sections: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../journalSection').model,
        join: {
          from: 'journal.id',
          to: 'journal_section.journalId',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../specialIssue').model,
        join: {
          from: 'journal.id',
          to: 'special_issue.journalId',
        },
      },
    }
  }
}

module.exports = Journal
