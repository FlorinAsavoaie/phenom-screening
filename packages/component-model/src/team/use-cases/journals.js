function initialize({ Team }) {
  async function execute({ id }) {
    const result = await Team.find(id, 'journals')
    return result.journals
  }

  return { execute }
}

module.exports = {
  initialize,
}
