function initialize({ Team }) {
  async function execute({ id }) {
    const team = await Team.findOneBy({
      queryObject: { id },
      eagerLoadRelations: 'users',
    })

    return team.users.map((user) => ({
      id: user.id,
      role: user.role,
      email: user.email,
      surname: user.surname,
      isConfirmed: user.isConfirmed,
      givenNames: user.givenNames,
    }))
  }

  return { execute }
}

module.exports = {
  initialize,
}
