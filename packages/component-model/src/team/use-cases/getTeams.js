function initialize({ Team }) {
  async function execute({ page = 0, pageSize = 20, searchValue = '' }) {
    return Team.findAllWithoutAdmin({ page, pageSize, searchValue })
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'isAdmin']

module.exports = {
  initialize,
  authsomePolicies,
}
