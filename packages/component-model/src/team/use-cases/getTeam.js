function initialize({ Team }) {
  async function execute({ teamId }) {
    return Team.find(teamId)
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'isAdmin']

module.exports = {
  initialize,
  authsomePolicies,
}
