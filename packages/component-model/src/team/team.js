const HindawiBaseModel = require('../../hindawiBaseModel')
const User = require('../user/user')

class Team extends HindawiBaseModel {
  static get tableName() {
    return 'team'
  }
  static get schema() {
    return {
      type: 'object',
      required: ['name'],
      properties: {
        name: { type: 'string' },
        type: { type: 'string' },
      },
      additionalProperties: true,
    }
  }
  static get relationMappings() {
    return {
      journals: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('../journal').model,
        join: {
          from: 'team.id',
          through: {
            from: 'team_journal.teamId',
            to: 'team_journal.journalId',
          },
          to: 'journal.id',
        },
      },
      users: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('../user').model,
        join: {
          from: 'team.id',
          through: {
            from: 'team_user.teamId',
            to: 'team_user.userId',
            extra: ['role', 'assignationDate'],
          },
          to: 'user.id',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'team.id',
          through: {
            from: 'team_manuscript.teamId',
            to: 'team_manuscript.manuscriptId',
          },
          to: 'manuscript.id',
        },
      },
    }
  }

  static get Type() {
    return {
      SCREENING: 'screening',
      QUALITY_CHECKING: 'qualityChecking',
    }
  }

  get typeLabel() {
    const types = {
      screening: 'screening',
      qualityChecking: 'quality checking',
      default: '-',
    }
    return types[this.type] || types.default
  }

  static async findByPaginated({
    page,
    order,
    pageSize,
    queryObject,
    orderByField,
  }) {
    return this.query()
      .where(queryObject)
      .orderBy(orderByField, order)
      .page(page, pageSize)
  }

  static async findAllWithoutAdmin({ page, pageSize, searchValue }) {
    return Team.findByPaginated({
      order: 'asc',
      orderByField: 'name',
      page,
      pageSize,
      queryObject: (builder) => {
        if (searchValue) {
          builder = builder.whereRaw('lower(name) LIKE ?', [
            `%${searchValue.toLowerCase()}%`,
          ])
        }
        return builder.whereNot('name', 'Admin')
      },
    })
  }

  static async findTeamsWithConfirmedTL() {
    return this.query()
      .whereNot('name', 'Admin')
      .whereExists(
        Team.relatedQuery('users').where({
          role: User.Roles.TEAM_LEADER,
          is_confirmed: true,
        }),
      )
      .withGraphFetched('users')
  }
}
module.exports = Team
