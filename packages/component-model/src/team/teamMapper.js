const { model: User } = require('../user')

module.exports = {
  domainToCheckerTeamEventModel,
}

function domainToCheckerTeamEventModel(team) {
  const journalIds = team.journals.map((journal) => journal.id)

  const teamLeaders = team.users
    .filter((user) => user.role === User.Roles.TEAM_LEADER)
    .map((user) => ({
      id: user.id,
      email: user.email,
      givenNames: user.givenNames,
      surname: user.surname,
      isConfirmed: user.isConfirmed,
      created: user.created,
      updated: user.updated,
    }))

  const checkers = team.users
    .filter((user) =>
      [User.Roles.SCREENER, User.Roles.QUALITY_CHECKER].includes(user.role),
    )
    .map((user) => ({
      id: user.id,
      email: user.email,
      givenNames: user.givenNames,
      surname: user.surname,
      isConfirmed: user.isConfirmed,
      role: user.role,
      created: user.created,
      updated: user.updated,
    }))

  return {
    id: team.id,
    name: team.name,
    type: team.type,
    journalIds,
    checkers,
    teamLeaders,
    created: team.created,
    updated: team.updated,
  }
}
