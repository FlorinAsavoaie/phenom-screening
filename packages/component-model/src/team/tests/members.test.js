const Chance = require('chance')

const Team = require('../team')
const useCases = require('../use-cases')

const chance = new Chance()
const id = chance.guid()

Team.findOneBy = jest.fn().mockReturnValue({ users: ['user1', 'user2'] })

describe('Get team members use case', () => {
  it(`fetches all team's members`, async () => {
    await useCases.membersUseCase.initialize({ Team }).execute({ id })

    expect(Team.findOneBy).toHaveBeenCalledTimes(1)
    expect(Team.findOneBy).toHaveBeenCalledWith(
      expect.objectContaining({
        queryObject: {
          id,
        },
        eagerLoadRelations: 'users',
      }),
    )
  })
})
