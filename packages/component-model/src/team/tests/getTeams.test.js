const useCases = require('../use-cases')
const Team = require('../team')

describe('Get teams use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should fetch all the teams', async () => {
    const findAllWithoutAdmin = jest
      .spyOn(Team, 'findAllWithoutAdmin')
      .mockReturnValue([{ name: 'FCSB' }, { name: 'Juventus Colentina' }])

    const result = await useCases.getTeamsUseCase
      .initialize({ Team })
      .execute({})

    expect(result).toHaveLength(2)
    expect(findAllWithoutAdmin).toHaveBeenCalledTimes(1)
  })

  it('should fetch teams by similar name', async () => {
    const findAllWithoutAdmin = jest
      .spyOn(Team, 'findAllWithoutAdmin')
      .mockReturnValue([{ name: 'FCSB' }])
    const result = await useCases.getTeamsUseCase
      .initialize({ Team })
      .execute({ searchValue: 'someName' })

    expect(result).toHaveLength(1)
    expect(findAllWithoutAdmin).toHaveBeenCalledWith({
      searchValue: 'someName',
      page: 0,
      pageSize: 20,
    })
    expect(findAllWithoutAdmin).toHaveBeenCalledTimes(1)
  })
})
