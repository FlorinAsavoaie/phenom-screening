const Chance = require('chance')

const Team = require('../team')
const useCases = require('../use-cases')

const chance = new Chance()
const id = chance.guid()

Team.find = jest.fn().mockReturnValue({ journal: ['first', 'second'] })

describe('Get journals use case', () => {
  it('fetches the journals for the team', async () => {
    await useCases.journalsUseCase.initialize({ Team }).execute({ id })

    expect(Team.find).toHaveBeenCalled()
    expect(Team.find).toHaveBeenCalledWith(id, 'journals')
  })
})
