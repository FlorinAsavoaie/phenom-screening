const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./team'),
  useCases: require('./use-cases'),
  resolvers: require('./resolvers'),
  mapper: require('./teamMapper'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
