const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async getTeams(_, params, ctx) {
      return useCases.getTeamsUseCase.initialize(models).execute(params)
    },
    async getTeam(_, params, ctx) {
      return useCases.getTeamUseCase.initialize(models).execute(params)
    },
  },
  ScreeningTeam: {
    async members(rootQuery, params, ctx) {
      return useCases.membersUseCase.initialize(models).execute(rootQuery)
    },
    async journals(rootQuery, params, ctx) {
      return useCases.journalsUseCase.initialize(models).execute(rootQuery)
    },
  },
}
