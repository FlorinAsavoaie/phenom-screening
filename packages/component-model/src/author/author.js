const HindawiBaseModel = require('../../hindawiBaseModel')

class Author extends HindawiBaseModel {
  static get tableName() {
    return 'author'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        surname: { type: ['string', 'null'] },
        givenNames: { type: ['string', 'null'] },
        email: { type: 'string' },
        aff: { type: ['string', 'null'] },
        country: { type: ['string', 'null'] },
        title: { type: ['string', 'null'] },
        isSubmitting: { type: ['boolean', null] },
        isCorresponding: { type: ['boolean', null] },
        manuscriptId: { type: 'string', format: 'uuid' },
        isOnBadDebtList: { type: ['boolean'], default: false },
        isOnBlackList: { type: ['boolean'], default: false },
        isOnWatchList: { type: ['boolean'], default: false },
        isVerified: { type: ['boolean', null], default: false },
        isVerifiedOutOfTool: { type: 'boolean', default: false },
        position: { type: ['integer'], default: 0 },
        orcid: { type: ['string', 'null'], default: null },
        hasOrcidIssue: { type: ['boolean', 'null'] },
        rorId: { type: ['string', 'null'], default: null },
        rorScore: { type: ['number', 'null'], default: null },
        rorAff: { type: ['string', 'null'], default: null },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'author.manuscriptId',
          to: 'manuscript.id',
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../review').model,
        join: {
          from: 'author.id',
          to: 'review.authorId',
        },
      },
    }
  }

  static async getCorrespondingAuthor(manuscriptId) {
    return this.findOneBy({
      queryObject: {
        manuscriptId,
        isCorresponding: true,
      },
    })
  }

  get fullName() {
    return `${this.givenNames} ${this.surname}`
  }
}

module.exports = Author
