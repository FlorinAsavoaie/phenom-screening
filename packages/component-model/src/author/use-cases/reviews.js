function initialize({ Review }) {
  async function execute({ authorId }) {
    return Review.findBy({
      queryObject: { authorId },
    })
  }

  return { execute }
}

module.exports = {
  initialize,
}
