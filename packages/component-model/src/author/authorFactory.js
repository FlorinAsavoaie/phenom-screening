const Author = require('./author')
const { get } = require('lodash')

module.exports = {
  createAuthor(author) {
    return new Author({
      isSubmitting: author.isSubmitting,
      aff: author.affiliation.name,
      email: author.email,
      country: author.country,
      surname: author.surname,
      givenNames: author.givenNames,
      title: author.title,
      isCorresponding: author.isCorresponding,
      position: author.position,
      orcid: get(author, 'orcidId', null),
    })
  },
}
