const models = require('@pubsweet/models')
const useCases = require('./use-cases')

const resolvers = {
  Author: {
    async reviews(author, query, ctx) {
      return useCases.reviewsUseCase
        .initialize(models)
        .execute({ authorId: author.id })
    },
  },
}

module.exports = resolvers
