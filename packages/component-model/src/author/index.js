const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./author'),
  factory: require('./authorFactory'),
  resolvers: require('./resolvers'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
