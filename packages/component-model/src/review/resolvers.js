const models = require('@pubsweet/models')
const useCases = require('./use-cases')

const resolvers = {
  ScreeningReview: {
    async file(review, query, ctx) {
      return useCases.fileUseCase
        .initialize({ models })
        .execute({ reviewId: review.id })
    },
  },
}

module.exports = resolvers
