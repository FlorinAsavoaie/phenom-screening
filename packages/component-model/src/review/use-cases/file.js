function initialize({ models: { File } }) {
  async function execute({ reviewId }) {
    return File.findOneBy({
      queryObject: { reviewId },
    })
  }

  return { execute }
}

module.exports = {
  initialize,
}
