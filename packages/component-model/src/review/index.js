const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./review'),
  factory: require('./reviewFactory'),
  resolvers: require('./resolvers'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
