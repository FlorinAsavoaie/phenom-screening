const Review = require('./review')
const { get } = require('lodash')

module.exports = {
  createReview(review) {
    const publicComment = review.comments.find(
      (review) => review.type === 'public',
    )
    const privateComment = review.comments.find(
      (review) => review.type === 'private',
    )

    return new Review({
      recommendation: review.recommendation,
      submitted: review.submitted,
      publicComment: get(publicComment, 'content'),
      privateComment: get(privateComment, 'content'),
      isValid: review.isValid,
    })
  },
}
