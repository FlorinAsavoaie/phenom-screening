const HindawiBaseModel = require('../../hindawiBaseModel')

class Review extends HindawiBaseModel {
  static get tableName() {
    return 'review'
  }
  static get schema() {
    return {
      type: 'object',
      properties: {
        memberId: { type: ['string', 'null'], format: 'uuid' },
        authorId: { type: ['string', 'null'], format: 'uuid' },
        recommendation: {
          enum: Object.values(this.Recommendation),
        },
        submitted: { type: ['string', 'object'], format: 'date-time' },
        publicComment: { type: ['string', 'null'] },
        privateComment: { type: ['string', 'null'] },
        isValid: { type: 'boolean', default: true },
      },
    }
  }

  static get Recommendation() {
    return {
      PUBLISH: 'publish',
      MINOR: 'minor',
      MAJOR: 'major',
      REVISION: 'revision',
      RESPONSE_TO_REVISION: 'responseToRevision',
      RETURN_TO_PEER_REVIEW: 'returnToAcademicEditor',
      REJECT: 'reject',
    }
  }

  $beforeInsert() {
    super.$beforeInsert()
    if (!this.memberId && !this.authorId) {
      throw new Error('One of `memberId` and `authorId` should be null')
    }
  }
  static get relationMappings() {
    return {
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../member').model,
        join: {
          from: 'review.memberId',
          to: 'member.id',
        },
      },
      author: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../author').model,
        join: {
          from: 'review.authorId',
          to: 'author.id',
        },
      },
      file: {
        relation: HindawiBaseModel.HasOneRelation,
        modelClass: require('../file').model,
        join: {
          from: 'review.id',
          to: 'file.reviewId',
        },
      },
    }
  }
}
module.exports = Review
