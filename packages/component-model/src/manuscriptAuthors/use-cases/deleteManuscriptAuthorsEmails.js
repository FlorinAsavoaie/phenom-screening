function initialize({ models: { ManuscriptAuthors } }) {
  async function execute({ manuscriptId }) {
    await ManuscriptAuthors.query().where('manuscriptId', manuscriptId).delete()
  }

  return { execute }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
