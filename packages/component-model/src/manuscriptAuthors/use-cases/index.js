const ingestManuscriptAuthorsEmailsUseCase = require('./ingestManuscriptAuthorsEmails')
const deleteManuscriptAuthorsEmailsUseCase = require('./deleteManuscriptAuthorsEmails')

module.exports = {
  ingestManuscriptAuthorsEmailsUseCase,
  deleteManuscriptAuthorsEmailsUseCase,
}
