function initialize({
  models: { ManuscriptAuthors },
  factories: { manuscriptAuthorsFactory },
}) {
  async function execute({
    manuscriptAuthorsList,
    manuscriptId,
    manuscriptTitle,
  }) {
    const manuscriptAuthorsEmailsList = manuscriptAuthorsList
      .map((author) => author.email)
      .sort()

    const manuscriptAuthorsObject = manuscriptAuthorsFactory.create({
      manuscriptId,
      manuscriptTitle,
      authorList: manuscriptAuthorsEmailsList,
    })

    await ManuscriptAuthors.insertGraph(manuscriptAuthorsObject)
  }

  return { execute }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
