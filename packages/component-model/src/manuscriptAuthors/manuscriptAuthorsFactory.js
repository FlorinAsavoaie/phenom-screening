const ManuscriptAuthors = require('./manuscriptAuthors')

module.exports = {
  create: ({ manuscriptId, manuscriptTitle, authorList }) =>
    new ManuscriptAuthors({
      manuscriptId,
      manuscriptTitle,
      authorList,
    }),
}
