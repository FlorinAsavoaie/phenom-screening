const HindawiBaseModel = require('../../hindawiBaseModel')
const { model } = require('../manuscript')

class ManuscriptAuthors extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript_authors'
  }

  static get schema() {
    return {
      type: 'object',
      required: ['manuscriptId', 'manuscriptTitle', 'authorList'],
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        authorList: { type: 'array' },
        manuscriptTitle: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: model,
        join: {
          from: 'manuscript_authors.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  /**
   * Objection maps object and array properties into strings so that they can be stored into json/jsonb columns.
   * We need to override the properties that are mapped by setting the jsonAttributes property of the model.
   * In this case, we set an empty array to signal that no properties should be mapped into JSON strings.
   * This is required because authorList is defined as an array of text (text[]) in the DB and not as jsonb.
   * Not doing this will cause a mismatch when trying to use a JS array at insert.
   */
  static get jsonAttributes() {
    return []
  }
}

module.exports = ManuscriptAuthors
