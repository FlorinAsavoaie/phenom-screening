const ManuscriptAuthors = require('./manuscriptAuthors')
const ManuscriptAuthorsFactory = require('./manuscriptAuthorsFactory')
const ManuscriptAuthorsUseCases = require('./use-cases')

module.exports = {
  model: ManuscriptAuthors,
  factory: ManuscriptAuthorsFactory,
  useCases: ManuscriptAuthorsUseCases,
}
