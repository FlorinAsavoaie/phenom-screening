const HindawiBaseModel = require('../../hindawiBaseModel')
const Manuscript = require('../manuscript/manuscript')

class MaterialCheckConfig extends HindawiBaseModel {
  static get tableName() {
    return 'material_check_config'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        config: {
          type: 'json',
        },
        version: {
          type: 'numeric',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: `${this.tableName}.id`,
          to: 'manuscript.materialCheckConfigId',
        },
      },
    }
  }

  static async getLatest() {
    return this.query()
      .whereRaw(
        `version = (
              select max(mcc.version) from material_check_config mcc
            )`,
      )
      .first()
  }

  static async getConfigByManuscriptIdAndName({ manuscriptId, name }) {
    const { materialCheckConfig } = await Manuscript.find(
      manuscriptId,
      'materialCheckConfig',
    )

    return materialCheckConfig.config[name]
  }

  static async isAdditionalInfoSupported({
    manuscriptId,
    name,
    selectedOption,
  }) {
    const { options } = await this.getConfigByManuscriptIdAndName({
      name,
      manuscriptId,
    })

    const option = options.find((o) => o.value === selectedOption)
    return option && option.supportsAdditionalInfo
  }

  static async getAdditionalInfoType({ manuscriptId, name, selectedOption }) {
    const { options } = await this.getConfigByManuscriptIdAndName({
      name,
      manuscriptId,
    })
    const option = options.find((o) => o.value === selectedOption)
    if (!option.type)
      throw new Error(
        `Additional information not supported for option ${option.value} for question ${name} on manuscript ${manuscriptId}`,
      )
    return option.type
  }
}

module.exports = MaterialCheckConfig
