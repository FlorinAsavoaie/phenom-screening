module.exports = {
  domainToCheckerEventModel,
}

function domainToCheckerEventModel({ checker, assignationType, manuscript }) {
  const team = checker.teams.find((team) => team.id === manuscript.team.id)

  return {
    id: checker.id,
    email: checker.email,
    surname: checker.surname,
    givenNames: checker.givenNames,
    isConfirmed: checker.isConfirmed,
    role: team.role,
    assignationDate: team.assignationDate,
    submissionId: manuscript.submissionId,
    teamId: team.id,
    created: checker.created,
    updated: checker.updated,
    assignationType,
  }
}
