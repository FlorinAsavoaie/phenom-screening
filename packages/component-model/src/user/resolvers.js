const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async currentScreeningUser(_, params, ctx) {
      return useCases.currentScreeningUserUseCase
        .initialize(models)
        .execute(ctx.user)
    },
    async getUsers(_, params, ctx) {
      return useCases.getUsersUseCase.initialize(models).execute(params)
    },
  },
  ScreeningUser: {
    async isAdmin(rootQuery, params, ctx) {
      return useCases.isAdminUseCase.initialize(models).execute(rootQuery)
    },
    async teamMemberships(rootQuery, params, ctx) {
      return useCases.teamMembershipsUseCase
        .initialize(models)
        .execute(rootQuery)
    },
    async isTeamLeadInAtLeastOneTeam(rootQuery, params, ctx) {
      return useCases.isTeamLeadInAtLeastOneTeam
        .initialize(models)
        .execute(rootQuery.id)
    },
  },
}
