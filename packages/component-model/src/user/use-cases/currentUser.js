function initialize({ User }) {
  async function execute(userId) {
    const currentUser = await User.find(userId)

    return currentUser
  }

  return { execute }
}
const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
