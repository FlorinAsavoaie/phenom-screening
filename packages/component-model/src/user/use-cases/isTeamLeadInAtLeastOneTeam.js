function initialize({ User }) {
  async function execute(userId) {
    return User.isTeamLeadInAtLeastOneTeam(userId)
  }

  return { execute }
}

module.exports = {
  initialize,
}
