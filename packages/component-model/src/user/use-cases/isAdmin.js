function initialize({ User }) {
  async function execute(rootQuery) {
    return User.isAdmin(rootQuery.id)
  }

  return { execute }
}

module.exports = {
  initialize,
}
