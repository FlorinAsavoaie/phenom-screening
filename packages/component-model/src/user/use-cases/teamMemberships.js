function initialize({ User }) {
  async function execute(rootQuery) {
    const result = await User.findOneBy({
      queryObject: { id: rootQuery.id },
      eagerLoadRelations: 'teams',
    })

    return result.teams.map((teamUser) => ({
      role: teamUser.role,
      teamName: teamUser.name,
    }))
  }

  return { execute }
}

module.exports = {
  initialize,
}
