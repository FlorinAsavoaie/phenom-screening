const HindawiBaseModel = require('../../hindawiBaseModel')
const models = require('@pubsweet/models')

class User extends HindawiBaseModel {
  static get tableName() {
    return 'user'
  }

  static get schema() {
    return {
      properties: {
        email: { type: 'string' },
        surname: { type: 'string' },
        givenNames: { type: 'string' },
        passwordHash: { type: ['string', 'null'] },
        confirmationToken: { type: ['string', 'null'] },
        isConfirmed: { type: 'boolean', default: false },
      },
      additionalProperties: true,
    }
  }
  static get relationMappings() {
    return {
      teams: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('../team').model,
        join: {
          from: 'user.id',
          through: {
            from: 'team_user.userId',
            to: 'team_user.teamId',
            extra: ['role', 'assignationDate'],
          },
          to: 'team.id',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.ManyToManyRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'user.id',
          through: {
            from: 'user_manuscript.userId',
            to: 'user_manuscript.manuscriptId',
          },
          to: 'manuscript.id',
        },
      },
    }
  }

  static get Roles() {
    return {
      ADMIN: 'admin',
      TEAM_LEADER: 'teamLeader',
      SCREENER: 'screener',
      QUALITY_CHECKER: 'qualityChecker',
    }
  }

  get roleLabel() {
    const types = {
      admin: 'Admin',
      teamLeader: 'Team Leader',
      screener: 'Screener',
      qualityChecker: 'Quality Checker',
      default: '-',
    }
    return types[this.role] || types.default
  }

  static getRoleByManuscriptFlowType(flowType) {
    const manuscriptFlowTypes = {
      screening: User.Roles.SCREENER,
      qualityCheck: User.Roles.QUALITY_CHECKER,
    }
    return manuscriptFlowTypes[flowType]
  }

  get fullName() {
    return `${this.givenNames} ${this.surname}`
  }

  static async isAdmin(userId) {
    const admin = await User.query()
      .distinct('user.*')
      .leftJoin('team_user', 'team_user.user_id', '=', 'user.id')
      .where({
        'user.id': userId,
        'team_user.role': User.Roles.ADMIN,
      })

    return !!admin.length
  }

  static async isTeamLeadInAtLeastOneTeam(userId) {
    const noOfTeamLeaderRoles = await User.query()
      .leftJoin('team_user', 'team_user.user_id', '=', 'user.id')
      .where({
        'user.id': userId,
        'team_user.role': User.Roles.TEAM_LEADER,
      })
      .resultSize()

    return noOfTeamLeaderRoles > 0
  }

  static async findByPaginated({
    page,
    order,
    pageSize,
    searchValue,
    orderByField,
  }) {
    return User.query()
      .distinct('user.*')
      .leftJoin('team_user', 'team_user.user_id', '=', 'user.id')
      .leftJoin('team', 'team.id', '=', 'team_user.team_id')
      .where((builder) => {
        if (searchValue) {
          builder
            .whereRaw('lower(email) LIKE ?', [`%${searchValue.toLowerCase()}%`])
            .orWhereRaw('lower(given_names) LIKE ?', [
              `%${searchValue.toLowerCase()}%`,
            ])
            .orWhereRaw('lower(surname) LIKE ?', [
              `%${searchValue.toLowerCase()}%`,
            ])
            .orWhereRaw(
              `concat(lower(given_names), ' ', lower(surname)) LIKE ?`,
              [`%${searchValue.toLowerCase()}%`],
            )
            .orWhereRaw(
              `concat(lower(surname), ' ', lower(given_names)) LIKE ?`,
              [`%${searchValue.toLowerCase()}%`],
            )
            .orWhereRaw('lower(team.name) LIKE ?', [
              `%${searchValue.toLowerCase()}%`,
            ])
        }
        return builder
      })
      .orderBy(orderByField, order)
      .page(page, pageSize)
  }

  static async findOneByEmail(email) {
    const object = await this.query()
      .whereRaw('LOWER(email) LIKE ?', [email.toLowerCase()])
      .limit(1)

    if (!object.length) {
      return
    }

    return object[0]
  }

  static async getUserRoleOnManuscript({ userId, manuscriptId }) {
    const isAdmin = await User.isAdmin(userId)
    if (isAdmin) return User.Roles.ADMIN

    const userRole = await this.knex()
      .first('role')
      .from('team_user')
      .where('team_user.user_id', userId)
      .andWhere(
        'team_id',
        '=',
        this.knex()
          .select('team_manuscript.team_id')
          .from('team_manuscript')
          .where('team_manuscript.manuscript_id', manuscriptId),
      )
      .then((userRole) => (userRole ? userRole.role : null))

    if ([this.Roles.SCREENER, this.Roles.QUALITY_CHECKER].includes(userRole)) {
      const assignationOnManuscript = await this.knex()
        .first('id')
        .from('user_manuscript')
        .where('user_manuscript.manuscript_id', manuscriptId)
        .andWhere('user_manuscript.user_id', userId)

      if (!assignationOnManuscript) return null
    }

    return userRole
  }

  static async getFirstAvailableChecker(role, journalId) {
    const result = await this.knex().raw(
      'select "user".*, "team_user"."team_id", "team_user"."assignation_date"\n' +
        'from "user"\n' +
        '         left join "team_user" on "team_user"."user_id" = "user"."id"\n' +
        'where "role" = ? \n' +
        '  and "is_confirmed" = true\n' +
        '  and "team_user"."team_id" in (\n' +
        '    select "team".id\n' +
        '    from "team"\n' +
        '      inner join "team_journal" on "team_journal"."team_id" = "team"."id"\n' +
        '    where "team_journal"."journal_id" = ? ' +
        '      and not "name" = \'Admin\'\n' +
        '      and exists(select "users".*\n' +
        '                 from "user" as "users"\n' +
        '                          inner join "team_user" on "users"."id" = "team_user"."user_id"\n' +
        '                 where "team_user"."team_id" = "team"."id"\n' +
        '                   and "role" = \'teamLeader\'\n' +
        '                   and "is_confirmed" = true)\n' +
        ')\n' +
        'order by ASSIGNATION_DATE ASC NULLS FIRST;',
      [role, journalId],
    )

    return result.rows[0]
  }

  static async setAssignationDate({ id, teamId }) {
    const checker = await User.findOneBy({
      queryObject: { id },
    })

    const team = await checker
      .$relatedQuery('teams')
      .where({ teamId })
      .where({ userId: id })
      .patchAndFetchById(teamId, { assignationDate: new Date() })

    checker.teams = [team]

    return checker
  }

  static async canDoActionAccordingToStatus({
    userId,
    manuscriptStatus,
    manuscriptId,
  }) {
    const userRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId,
    })

    const canDoActionOnInProgressStatus =
      manuscriptStatus === models.Manuscript.Statuses.IN_PROGRESS

    const canDoActionOnEscalatedStatus =
      manuscriptStatus === models.Manuscript.Statuses.ESCALATED &&
      (User.Roles.ADMIN === userRole || User.Roles.TEAM_LEADER === userRole)

    return canDoActionOnInProgressStatus || canDoActionOnEscalatedStatus
  }

  static async getCheckerFromTeamsWithManuscriptJournal({
    userId,
    checkerRole,
    journalId,
  }) {
    return this.query()
      .where({
        id: userId,
      })
      .withGraphFetched('teams(filterTeamsByJournalIdAndUserRole)')
      .modifiers({
        filterTeamsByJournalIdAndUserRole: (builder) => {
          builder.where({ role: checkerRole }).whereExists(
            models.Team.relatedQuery('journals').where({
              'journals.id': journalId,
            }),
          )
        },
      })
      .first()
  }

  static get AssignationTypes() {
    return {
      automaticAssignmentToTheSameChecker:
        'automaticAssignmentToTheSameChecker',
      automaticAssignmentToANewChecker: 'automaticAssignmentToANewChecker',
      automaticReassignmentToANewChecker: 'automaticReassignmentToANewChecker',
      automaticReassignmentToTheSameCheckerFromDifferentTeam:
        'automaticReassignmentToTheSameCheckerFromDifferentTeam',
      automaticAssignmentToANewCheckerFromSameTeam:
        'automaticAssignmentToANewCheckerFromSameTeam',
      manualReassignment: 'manualReassignment',
    }
  }

  static async findAssignedChecker(manuscriptId) {
    const checker = await User.query()
      .leftJoin('user_manuscript', 'user_manuscript.userId', '=', 'user.id')
      .where({ 'user_manuscript.manuscriptId': manuscriptId })
      .first()

    return checker
  }
}

module.exports = User
