const uuid = require('uuid')
const User = require('./user')

module.exports = {
  async createUser(member) {
    const newUser = new User({
      email: member.email.toLowerCase(),
      givenNames: member.givenNames,
      surname: member.surname,
      confirmationToken: uuid.v4(),
    })

    if (process.env.KEYCLOAK_SERVER_URL) {
      const { createSSOUser } = require('component-sso')
      await createSSOUser(newUser)
    }

    return newUser
  },
}
