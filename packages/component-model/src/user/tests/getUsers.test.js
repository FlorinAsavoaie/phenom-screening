const User = require('../user')
const useCases = require('../use-cases')

describe('Get users', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('fetches all users', async () => {
    const findByPaginated = jest
      .spyOn(User, 'findByPaginated')
      .mockReturnValue({ users: ['user1', 'user2'], totalCount: 2 })

    const page = 0
    const pageSize = 20
    await useCases.getUsersUseCase.initialize({ User }).execute(page, pageSize)

    expect(findByPaginated).toHaveBeenCalledTimes(1)
    expect(findByPaginated).toHaveBeenCalledWith(
      expect.objectContaining({
        orderByField: 'surname',
        order: 'asc',
        page,
        pageSize,
      }),
    )
  })
})
