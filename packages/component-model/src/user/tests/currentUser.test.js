const User = require('../user')
const useCases = require('../use-cases')

User.find = jest.fn().mockReturnValue({
  name: 'gianluigi.buffon@gmail.com',
})

describe('Current user', () => {
  it('fetches current logged user info', async () => {
    await useCases.currentScreeningUserUseCase.initialize({ User }).execute()

    expect(User.find).toHaveBeenCalledTimes(1)
  })
})
