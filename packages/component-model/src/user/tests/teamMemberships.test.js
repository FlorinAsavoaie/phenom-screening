const User = require('../user')
const useCases = require('../use-cases')

const mockUser = {
  teams: [
    { role: User.Roles.SCREENER, name: 'Juventus Torino' },
    { role: User.Roles.TEAM_LEADER, name: 'Real Madrid' },
  ],
}

User.findOneBy = jest.fn().mockReturnValue(mockUser)

describe('Team user', () => {
  it('fetches the team memberships for a user', async () => {
    const result = await useCases.teamMembershipsUseCase
      .initialize({ User })
      .execute({ id: '123' })

    expect(User.findOneBy).toHaveBeenCalledTimes(1)
    expect(result).toEqual([
      { role: User.Roles.SCREENER, teamName: 'Juventus Torino' },
      { role: User.Roles.TEAM_LEADER, teamName: 'Real Madrid' },
    ])
  })
})
