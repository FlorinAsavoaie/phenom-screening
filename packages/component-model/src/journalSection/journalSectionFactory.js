const JournalSection = require('./journalSection')

module.exports = {
  create({ id, journalId, name }) {
    return new JournalSection({ id, journalId, name })
  },
}
