const HindawiBaseModel = require('../../hindawiBaseModel')

class JournalSection extends HindawiBaseModel {
  static get tableName() {
    return 'journal_section'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        journalId: { type: 'string', format: 'uuid' },
        name: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../journal').model,
        join: {
          from: 'journal_section.journalId',
          to: 'journal.id',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../specialIssue').model,
        join: {
          from: 'journal_section.id',
          to: 'special_issue.journalSectionId',
        },
      },
    }
  }
}

module.exports = JournalSection
