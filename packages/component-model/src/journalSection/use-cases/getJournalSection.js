function initialize({ JournalSection, SpecialIssue }) {
  return {
    execute,
  }

  async function execute({ journalSectionId, specialIssueId }) {
    if (journalSectionId) {
      return JournalSection.findOneBy({
        queryObject: {
          id: journalSectionId,
        },
      })
    }

    if (specialIssueId) {
      const specialIssue = await SpecialIssue.findOneBy({
        queryObject: {
          id: specialIssueId,
        },
        eagerLoadRelations: 'journalSection',
      })

      return specialIssue.journalSection
    }

    return null
  }
}

module.exports = {
  initialize,
}
