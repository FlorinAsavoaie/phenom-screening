const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./journalSection'),
  factory: require('./journalSectionFactory'),
  useCases: require('./use-cases'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
