const JournalSection = require('./../journalSection')
const SpecialIssue = require('./../../specialIssue/specialIssue')
const useCases = require('./../use-cases')

describe('Get Journal Section', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should return null if journalSectionId and specialIssueId are null', async () => {
    const findOneByJournalSection = jest
      .spyOn(JournalSection, 'findOneBy')
      .mockReturnValue({})
    const findOneBySpecialIssue = jest
      .spyOn(SpecialIssue, 'findOneBy')
      .mockReturnValue({})

    const journalSection = await useCases.getJournalSectionUseCase
      .initialize({
        JournalSection,
        SpecialIssue,
      })
      .execute({
        journalSectionId: null,
        specialIssueId: null,
      })

    expect(journalSection).toBe(null)
    expect(findOneByJournalSection).not.toHaveBeenCalled()
    expect(findOneBySpecialIssue).not.toHaveBeenCalled()
  })

  it('should return journal section', async () => {
    const findOneByJournalSection = jest
      .spyOn(JournalSection, 'findOneBy')
      .mockReturnValue({
        id: 'journal-section-id',
      })
    const findOneBySpecialIssue = jest
      .spyOn(SpecialIssue, 'findOneBy')
      .mockReturnValue({})

    const journalSection = await useCases.getJournalSectionUseCase
      .initialize({
        JournalSection,
        SpecialIssue,
      })
      .execute({ journalSectionId: 'journal-section-id' })

    expect(journalSection).toStrictEqual({
      id: 'journal-section-id',
    })
    expect(findOneByJournalSection).toHaveBeenCalledWith({
      queryObject: {
        id: 'journal-section-id',
      },
    })
    expect(findOneBySpecialIssue).not.toHaveBeenCalled()
  })

  it('should return journal section from special issue', async () => {
    const findOneByJournalSection = jest
      .spyOn(JournalSection, 'findOneBy')
      .mockReturnValue({})
    const findOneBySpecialIssue = jest
      .spyOn(SpecialIssue, 'findOneBy')
      .mockReturnValue({
        journalSection: {
          id: 'special-issue-journal-section-id',
        },
      })

    const journalSection = await useCases.getJournalSectionUseCase
      .initialize({
        JournalSection,
        SpecialIssue,
      })
      .execute({ journalSectionId: null, specialIssueId: 'special-issue-id' })

    expect(journalSection).toStrictEqual({
      id: 'special-issue-journal-section-id',
    })

    expect(findOneByJournalSection).not.toHaveBeenCalled()
    expect(findOneBySpecialIssue).toHaveBeenCalledTimes(1)

    expect(findOneBySpecialIssue).toHaveBeenCalledWith({
      queryObject: {
        id: 'special-issue-id',
      },
      eagerLoadRelations: 'journalSection',
    })
  })
})
