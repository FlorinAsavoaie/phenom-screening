const { getMainManuscriptVersionsUseCase } = require('./../use-cases')

describe('getMainManuscriptVersions', () => {
  it('should get main manuscript versions by submissionId and flowType', async () => {
    const Manuscript = {
      getMainManuscriptVersions: jest.fn(),
    }

    await getMainManuscriptVersionsUseCase.initialize({ Manuscript }).execute({
      submissionId: 'some-submission-id',
      flowType: 'some-flow-type',
    })

    expect(Manuscript.getMainManuscriptVersions).toHaveBeenCalledTimes(1)
    expect(Manuscript.getMainManuscriptVersions).toHaveBeenCalledWith({
      submissionId: 'some-submission-id',
      flowType: 'some-flow-type',
    })
  })
})
