const useCases = require('../use-cases')
const Author = require('../../author/author')

describe('getSubmittingAuthorUseCase', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should fetch the submitting author for a specific manuscript', async () => {
    const author = {
      id: 1,
    }
    const manuscriptId = 3

    const findOneBy = jest.spyOn(Author, 'findOneBy').mockReturnValue(author)

    const result = await useCases.getSubmittingAuthorUseCase
      .initialize({ Author })
      .execute({ manuscriptId })

    expect(result).toBe(author)
    expect(findOneBy).toHaveBeenCalledTimes(1)
    expect(findOneBy).toHaveBeenCalledWith({
      queryObject: {
        manuscriptId,
        isSubmitting: true,
      },
    })
  })
})
