const { getManuscriptReviewersUseCase } = require('./../use-cases')
const Manuscript = require('../manuscript')

describe('getManuscriptReviewersUseCase', () => {
  it('should fetch involved reviewers for a specific manuscript', async () => {
    const Member = {
      getInvolvedReviewersFor: jest.fn(),
    }
    const manuscript = {
      id: 'some-manuscript-id',
      submissionId: 'some-submission-id',
    }

    jest.spyOn(Manuscript, 'find').mockReturnValue(manuscript)

    await getManuscriptReviewersUseCase
      .initialize({ Member, Manuscript })
      .execute({
        manuscriptId: manuscript.id,
      })

    expect(Member.getInvolvedReviewersFor).toHaveBeenCalledTimes(1)
    expect(Member.getInvolvedReviewersFor).toHaveBeenCalledWith(
      manuscript.submissionId,
    )
  })
})
