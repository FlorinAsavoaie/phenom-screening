const useCases = require('../use-cases')
const Manuscript = require('../manuscript')

describe('getAvailableCheckers', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should fetch all available screeners for a specific manuscript', async () => {
    const availableCheckers = [
      {
        id: 1,
      },
      {
        id: 2,
      },
    ]
    const manuscriptId = 3

    const getAvailableCheckers = jest
      .spyOn(Manuscript, 'getAvailableCheckers')
      .mockReturnValue(availableCheckers)

    const result = await useCases.getAvailableCheckersUseCase
      .initialize({ Manuscript })
      .execute({ manuscriptId })

    expect(result).toHaveLength(2)
    expect(getAvailableCheckers).toHaveBeenCalledTimes(1)
    expect(getAvailableCheckers).toHaveBeenCalledWith(manuscriptId)
  })
})
