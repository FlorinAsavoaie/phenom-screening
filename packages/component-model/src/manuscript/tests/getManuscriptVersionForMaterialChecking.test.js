const { getManuscriptVersionForMaterialChecking } = require('./../use-cases')

const generateManuscripts = (versions) =>
  versions.map((v) => ({
    id: `id-v${v}`,
    version: v,
  }))

describe('getManuscriptVersionForMaterialChecking', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should get main manuscript versions by submissionId and flowType', async () => {
    const Manuscript = {
      getManuscriptVersions: jest.fn(() => []),
    }

    await getManuscriptVersionForMaterialChecking
      .initialize({ Manuscript })
      .execute({
        submissionId: 'some-submission-id',
        flowType: 'some-flow-type',
      })

    expect(Manuscript.getManuscriptVersions).toHaveBeenCalledTimes(1)
    expect(Manuscript.getManuscriptVersions).toHaveBeenCalledWith({
      submissionId: 'some-submission-id',
      flowType: 'some-flow-type',
    })
  })

  it('[test 1] - should return correct versions in order', async () => {
    const versions = ['3.1', '2', '3.2', '1.2', '1', '3']
    const Manuscript = {
      getManuscriptVersions: jest.fn(() => generateManuscripts(versions)),
    }

    const result = await getManuscriptVersionForMaterialChecking
      .initialize({ Manuscript })
      .execute({
        submissionId: 'some-submission-id',
        flowType: 'some-flow-type',
      })

    expect(result).toStrictEqual([
      { id: 'id-v3', version: '3' },
      { id: 'id-v3.1', version: '3.1' },
      { id: 'id-v3.2', version: '3.2' },
    ])
  })

  it('[test 2] - should return correct versions in order', async () => {
    const versions = [
      '2.1',
      '2.11',
      '2.4',
      '2.5',
      '2.6',
      '2',
      '2.10',
      '2.7',
      '2.2',
      '2.8',
      '2.3',
      '2.12',
      '2.9',
    ]
    const Manuscript = {
      getManuscriptVersions: jest.fn(() => generateManuscripts(versions)),
    }

    const result = await getManuscriptVersionForMaterialChecking
      .initialize({ Manuscript })
      .execute({
        submissionId: 'some-submission-id',
        flowType: 'some-flow-type',
      })

    expect(result).toStrictEqual([
      { id: 'id-v2', version: '2' },
      { id: 'id-v2.1', version: '2.1' },
      { id: 'id-v2.2', version: '2.2' },
      { id: 'id-v2.3', version: '2.3' },
      { id: 'id-v2.4', version: '2.4' },
      { id: 'id-v2.5', version: '2.5' },
      { id: 'id-v2.6', version: '2.6' },
      { id: 'id-v2.7', version: '2.7' },
      { id: 'id-v2.8', version: '2.8' },
      { id: 'id-v2.9', version: '2.9' },
      { id: 'id-v2.10', version: '2.10' },
      { id: 'id-v2.11', version: '2.11' },
      { id: 'id-v2.12', version: '2.12' },
    ])
  })

  it('[test 3] - should return correct versions in order', async () => {
    const versions = ['1.1', '4.1', '1', '1.2', '5', '4', '4.2', '2']
    const Manuscript = {
      getManuscriptVersions: jest.fn(() => generateManuscripts(versions)),
    }

    const result = await getManuscriptVersionForMaterialChecking
      .initialize({ Manuscript })
      .execute({
        submissionId: 'some-submission-id',
        flowType: 'some-flow-type',
      })

    expect(result).toStrictEqual([{ id: 'id-v5', version: '5' }])
  })

  it('[test 4] - should return correct versions in order', async () => {
    const versions = ['3', '5', '2', '1', '4']
    const Manuscript = {
      getManuscriptVersions: jest.fn(() => generateManuscripts(versions)),
    }

    const result = await getManuscriptVersionForMaterialChecking
      .initialize({ Manuscript })
      .execute({
        submissionId: 'some-submission-id',
        flowType: 'some-flow-type',
      })

    expect(result).toStrictEqual([{ id: 'id-v5', version: '5' }])
  })

  it('[test 5] - should return correct versions in order', async () => {
    const versions = [
      '1.2',
      '1.10',
      '1.4',
      '1.3',
      '1.5',
      '1.1',
      '1.7',
      '1',
      '11',
      '1.8',
      '1.6',
      '1.9',
      '1.12',
      '1.11',
      '11.1',
    ]
    const Manuscript = {
      getManuscriptVersions: jest.fn(() => generateManuscripts(versions)),
    }

    const result = await getManuscriptVersionForMaterialChecking
      .initialize({ Manuscript })
      .execute({
        submissionId: 'some-submission-id',
        flowType: 'some-flow-type',
      })

    expect(result).toStrictEqual([
      { id: 'id-v11', version: '11' },
      { id: 'id-v11.1', version: '11.1' },
    ])
  })
})
