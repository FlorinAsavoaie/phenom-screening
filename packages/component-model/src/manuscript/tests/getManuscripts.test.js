const Chance = require('chance')

const Manuscript = require('../manuscript')

const useCases = require('../use-cases')

const chance = new Chance()

const mockManuscripts = [
  {
    id: '123',
    title: chance.sentence(),
    abstract: chance.paragraph(),
    articleType: 'review',
    submissionId: chance.guid(),
    references: null,
    status: Manuscript.Statuses.IN_PROGRESS,
  },
  {
    id: chance.guid(),
    title: chance.sentence(),
    abstract: chance.paragraph(),
    articleType: 'review',
    submissionDate: new Date(),
    status: Manuscript.Statuses.IN_PROGRESS,
  },
]

Manuscript.getDashboardManuscripts = jest.fn().mockReturnValue(mockManuscripts)

describe('Get Manuscripts Use Case', () => {
  it('should return only the screener manuscripts', async () => {
    const screenerId = chance.guid()
    Manuscript.getUserManuscripts = jest
      .fn()
      .mockReturnValue([mockManuscripts[0].id])

    const manuscripts = await useCases.getScreeningManuscriptsUseCase
      .initialize({ Manuscript })
      .execute({
        userId: screenerId,
        searchValue: '',
        filterOption: 'inProgress',
      })

    expect(Manuscript.getUserManuscripts).toHaveBeenCalled()
    expect(Manuscript.getUserManuscripts).toHaveBeenCalledWith({
      userId: screenerId,
      searchValue: '',
      filterOption: 'inProgress',
      page: 0,
      pageSize: 20,
    })

    expect(manuscripts.length).toEqual(1)
  })
})
