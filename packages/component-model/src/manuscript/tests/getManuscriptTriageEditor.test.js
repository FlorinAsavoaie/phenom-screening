const { getManuscriptTriageEditorUseCase } = require('./../use-cases')
const Member = require('../../member/member')

describe('getManuscriptReviewersUseCase', () => {
  it('should fetch the triage editor for a specific manuscript', async () => {
    const findOneBy = jest.spyOn(Member, 'findOneBy').mockReturnValue({})

    await getManuscriptTriageEditorUseCase.initialize({ Member }).execute({
      manuscriptId: 'some-manuscript-id',
    })

    expect(findOneBy).toHaveBeenCalledTimes(1)
    expect(findOneBy).toHaveBeenCalledWith({
      queryObject: {
        manuscriptId: 'some-manuscript-id',
        role: Member.Roles.TRIAGE_EDITOR,
      },
    })
  })
})
