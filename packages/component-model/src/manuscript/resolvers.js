const models = require('@pubsweet/models')
const config = require('config')
const useCases = require('./use-cases')
const journalSectionUseCases = require('./../journalSection').useCases
const specialIssueUseCases = require('./../specialIssue').useCases

const resolvers = {
  Query: {
    async getScreeningManuscript(_, { manuscriptId }, ctx) {
      return useCases.getScreeningManuscriptUseCase.initialize(models).execute({
        manuscriptId,
        userId: ctx.user,
      })
    },
    async getScreeningManuscriptByCustomId(_, { manuscriptCustomId }) {
      return useCases.getScreeningManuscriptByCustomIdUseCase
        .initialize(models)
        .execute({
          manuscriptCustomId,
        })
    },
    async getAvailableCheckers(_, { manuscriptId }, ctx) {
      return useCases.getAvailableCheckersUseCase.initialize(models).execute({
        manuscriptId,
        userId: ctx.user,
      })
    },
    async getScreeningManuscripts(
      _,
      { searchValue, filterOption, page, pageSize, authorResponded },
      ctx,
    ) {
      return useCases.getScreeningManuscriptsUseCase
        .initialize(models)
        .execute({
          userId: ctx.user,
          searchValue,
          filterOption,
          page,
          pageSize,
          authorResponded,
        })
    },
    async getManuscriptReviewers(_, { manuscriptId }) {
      return useCases.getManuscriptReviewersUseCase.initialize(models).execute({
        manuscriptId,
      })
    },
    async getManuscriptTriageEditor(_, { manuscriptId }) {
      return useCases.getManuscriptTriageEditorUseCase
        .initialize(models)
        .execute({
          manuscriptId,
        })
    },
    async getManuscriptAcademicEditor(_, { manuscriptId }) {
      return useCases.getManuscriptAcademicEditorUseCase
        .initialize(models)
        .execute({
          manuscriptId,
        })
    },
  },
  ScreeningManuscript: {
    async submittingAuthor(manuscript, query, ctx) {
      return useCases.getSubmittingAuthorUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id })
    },
    async authors(manuscript, query, ctx) {
      return useCases.authorsUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id })
    },
    async members(manuscript, query, ctx) {
      return useCases.membersUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id })
    },
    async files(manuscript, query, ctx) {
      return useCases.filesUseCase
        .initialize({ models })
        .execute({ manuscriptId: manuscript.id })
    },
    async fileId(manuscript, query, ctx) {
      return useCases.fileIdUseCase.initialize(models).execute(manuscript.id)
    },
    async journal(manuscript, query, ctx) {
      return useCases.journalUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id })
    },
    async journalSection(manuscript) {
      return journalSectionUseCases.getJournalSectionUseCase
        .initialize(models)
        .execute(manuscript)
    },
    async specialIssue(manuscript) {
      return specialIssueUseCases.getSpecialIssueUseCase
        .initialize(models)
        .execute(manuscript.specialIssueId)
    },
    async assignedChecker(manuscript, query, ctx) {
      return useCases.assignedCheckerUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id })
    },
    async team(manuscript, query, ctx) {
      return useCases.teamUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id, userId: ctx.user })
    },
    async currentUserRole(manuscript, query, ctx) {
      return useCases.currentUserRoleUseCase
        .initialize(models)
        .execute({ manuscriptId: manuscript.id, userId: ctx.user })
    },
    async mainManuscriptVersions(manuscript) {
      return useCases.getMainManuscriptVersionsUseCase
        .initialize(models)
        .execute({
          submissionId: manuscript.submissionId,
          flowType: manuscript.flowType,
        })
    },
    async materialCheckManuscriptVersions(manuscript) {
      return useCases.getManuscriptVersionForMaterialChecking
        .initialize(models)
        .execute({
          submissionId: manuscript.submissionId,
          flowType: manuscript.flowType,
        })
    },
    isShortArticleSupported(manuscript) {
      return useCases.isShortArticleSupported
        .initialize({
          configService: config,
        })
        .execute(manuscript.articleType)
    },
  },
}

module.exports = resolvers
