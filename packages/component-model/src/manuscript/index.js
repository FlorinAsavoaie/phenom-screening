const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./manuscript'),
  loaders: require('./loaders'),
  useCases: require('./use-cases'),
  resolvers: require('./resolvers'),
  factory: require('./manuscriptFactory'),
  mapper: require('./manuscriptMapper'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
