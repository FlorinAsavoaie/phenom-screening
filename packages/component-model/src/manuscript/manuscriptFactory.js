const Manuscript = require('./manuscript')

module.exports = {
  createManuscript({
    submissionId,
    manuscript,
    authors,
    files,
    flowType,
    journalId,
    members = [],
    step,
    authorResponded,
  }) {
    return new Manuscript({
      submissionId,
      phenomId: manuscript.id,
      customId: manuscript.customId,
      title: manuscript.title,
      abstract: manuscript.abstract,
      articleType: manuscript.articleType.name,
      submissionDate: manuscript.created
        ? manuscript.created
        : manuscript.submissionCreatedDate,
      qualityChecksSubmittedDate: manuscript.qualityChecksSubmittedDate,
      version: manuscript.version.toString(),
      conflictOfInterest: manuscript.conflictOfInterest,
      dataAvailability: manuscript.dataAvailability,
      fundingStatement: manuscript.fundingStatement,
      journalId,
      journalSectionId: manuscript.sectionId,
      specialIssueId: manuscript.specialIssueId,
      step,
      flowType,
      authors,
      files,
      manuscriptValidations: {},
      manuscriptChecks: {},
      members,
      authorResponded,
      linkedSubmissionCustomId: manuscript.linkedSubmissionCustomId,
    })
  },
}
