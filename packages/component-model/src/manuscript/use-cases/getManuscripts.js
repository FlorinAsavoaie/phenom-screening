function initialize({ Manuscript }) {
  async function execute({
    userId,
    searchValue,
    filterOption,
    page = 0,
    pageSize = 20,
    authorResponded,
  }) {
    const manuscripts = await Manuscript.getUserManuscripts({
      userId,
      searchValue,
      filterOption,
      page,
      pageSize,
      authorResponded,
    })
    return manuscripts
  }

  return {
    execute,
  }
}

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
