function initialize({ Author }) {
  async function execute({ manuscriptId }) {
    return Author.findOneBy({
      queryObject: {
        manuscriptId,
        isSubmitting: true,
      },
    })
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser']

module.exports = { initialize, authsomePolicies }
