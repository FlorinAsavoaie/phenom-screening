function initialize({ Member, Manuscript }) {
  return {
    execute,
  }

  async function execute({ manuscriptId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    return Member.getInvolvedReviewersFor(manuscript.submissionId)
  }
}

const authsomePolicies = [
  'authenticatedUser',
  'hasAccessToManuscript',
  'isAdminOrTeamLead',
]

module.exports = {
  initialize,
  authsomePolicies,
}
