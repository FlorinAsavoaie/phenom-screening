function initialize({ configService }) {
  return {
    execute,
  }

  function execute(articleType) {
    return configService
      .get('publisherConfig')
      .supportedShortArticleTypes.includes(articleType)
  }
}

module.exports = {
  initialize,
}
