function initialize({ Manuscript }) {
  async function execute({ manuscriptId }) {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { id: manuscriptId },
      eagerLoadRelations: 'journal',
    })

    return manuscript.journal
  }

  return { execute }
}

module.exports = {
  initialize,
}
