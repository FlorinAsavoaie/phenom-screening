function initialize({ Member }) {
  return {
    execute,
  }

  async function execute({ manuscriptId }) {
    return Member.findOneBy({
      queryObject: {
        manuscriptId,
        role: Member.Roles.ACADEMIC_EDITOR,
        status: Member.Status.ACCEPTED,
      },
    })
  }
}

const authsomePolicies = [
  'authenticatedUser',
  'hasAccessToManuscript',
  'isAdminOrTeamLead',
]

module.exports = {
  initialize,
  authsomePolicies,
}
