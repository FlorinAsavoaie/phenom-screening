const teamUseCase = require('./team')
const filesUseCase = require('./files')
const fileIdUseCase = require('./fileId')
const membersUseCase = require('./members')
const journalUseCase = require('./journal')
const authorsUseCase = require('./authors')
const getScreeningManuscriptUseCase = require('./getManuscript')
const getScreeningManuscriptByCustomIdUseCase = require('./getScreeningManuscriptByCustomId')
const getScreeningManuscriptsUseCase = require('./getManuscripts')
const currentUserRoleUseCase = require('./currentUserRole')
const assignedCheckerUseCase = require('./assignedChecker')
const getAvailableCheckersUseCase = require('./getAvailableCheckers')
const getSubmittingAuthorUseCase = require('./getSubmittingAuthor')
const getMainManuscriptVersionsUseCase = require('./getMainManuscriptVersions')
const getManuscriptVersionForMaterialChecking = require('./getManuscriptVersionForMaterialChecking')
const getManuscriptReviewersUseCase = require('./getManuscriptReviewers')
const getManuscriptTriageEditorUseCase = require('./getManuscriptTriageEditor')
const getManuscriptAcademicEditorUseCase = require('./getManuscriptAcademicEditor')
const isShortArticleSupported = require('./isShortArticleSupported')

module.exports = {
  filesUseCase,
  teamUseCase,
  fileIdUseCase,
  membersUseCase,
  journalUseCase,
  authorsUseCase,
  getScreeningManuscriptUseCase,
  getScreeningManuscriptByCustomIdUseCase,
  getScreeningManuscriptsUseCase,
  currentUserRoleUseCase,
  assignedCheckerUseCase,
  getSubmittingAuthorUseCase,
  getAvailableCheckersUseCase,
  getMainManuscriptVersionsUseCase,
  getManuscriptTriageEditorUseCase,
  getManuscriptVersionForMaterialChecking,
  getManuscriptReviewersUseCase,
  getManuscriptAcademicEditorUseCase,
  isShortArticleSupported,
}
