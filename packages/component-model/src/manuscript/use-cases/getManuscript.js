const { NotFoundError } = require('@pubsweet/errors')

function initialize({ Manuscript }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, userId }) {
    if (!isValidManuscriptIdFormat(manuscriptId)) {
      throw new NotFoundError()
    }
    await assertCanAccessManuscript({ manuscriptId, userId })

    return Manuscript.find(manuscriptId)
  }
  async function assertCanAccessManuscript({ manuscriptId, userId }) {
    const canAccessManuscript = await Manuscript.canAccessManuscript({
      manuscriptId,
      userId,
    })
    if (!canAccessManuscript) {
      throw new NotFoundError()
    }
  }
}
function isValidManuscriptIdFormat(id) {
  const regex = new RegExp(
    '^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$',
  )
  return regex.test(id)
}

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
