function initialize({ Manuscript, User }) {
  async function execute({ manuscriptId, userId }) {
    const userRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId,
    })

    if ([User.Roles.SCREENER, User.Roles.QUALITY_CHECKER].includes(userRole)) {
      return null
    }

    const manuscript = await Manuscript.find(manuscriptId, 'team')
    return manuscript.team
  }

  return {
    execute,
  }
}

module.exports = {
  initialize,
}
