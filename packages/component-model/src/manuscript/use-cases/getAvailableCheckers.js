function initialize({ Manuscript }) {
  async function execute({ manuscriptId }) {
    const availableCheckers = await Manuscript.getAvailableCheckers(
      manuscriptId,
    )
    return availableCheckers
  }

  return {
    execute,
  }
}

const authsomePolicies = ['authenticatedUser', 'isAdminOrTeamLead']

module.exports = { initialize, authsomePolicies }
