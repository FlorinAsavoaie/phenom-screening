function initialize({ Manuscript }) {
  return {
    execute,
  }

  async function execute({ manuscriptCustomId }) {
    return Manuscript.findOneBy({
      queryObject: {
        customId: manuscriptCustomId,
        status: Manuscript.Statuses.QUALITY_CHECK_APPROVED,
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
