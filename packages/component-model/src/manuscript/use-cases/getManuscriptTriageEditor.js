function initialize({ Member }) {
  return {
    execute,
  }

  async function execute({ manuscriptId }) {
    return Member.findOneBy({
      queryObject: { manuscriptId, role: Member.Roles.TRIAGE_EDITOR },
    })
  }
}

const authsomePolicies = [
  'authenticatedUser',
  'hasAccessToManuscript',
  'isAdminOrTeamLead',
]

module.exports = {
  initialize,
  authsomePolicies,
}
