function initialize({ File }) {
  async function execute(manuscriptId) {
    const file = await File.findOneBy({
      queryObject: {
        manuscriptId,
        type: 'manuscript',
      },
    })

    if (!file) throw new Error('This manuscript has no main manuscript file')

    return file.id
  }

  return { execute }
}

module.exports = {
  initialize,
}
