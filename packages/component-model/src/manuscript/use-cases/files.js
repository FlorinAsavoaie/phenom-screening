function initialize({ models: { File } }) {
  async function execute({ manuscriptId }) {
    return File.findBy({
      queryObject: { manuscriptId },
    })
  }

  return { execute }
}

module.exports = {
  initialize,
}
