function initialize({ Member }) {
  async function execute({ manuscriptId }) {
    return Member.findBy({
      queryObject: { manuscriptId },
    })
  }

  return { execute }
}

module.exports = {
  initialize,
}
