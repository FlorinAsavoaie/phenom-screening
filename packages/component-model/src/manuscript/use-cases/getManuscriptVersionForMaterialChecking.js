const { chain } = require('lodash')

function initialize({ Manuscript }) {
  return {
    execute,
  }

  async function execute({ submissionId, flowType }) {
    const manuscripts = await Manuscript.getManuscriptVersions({
      submissionId,
      flowType,
    })

    return chain(manuscripts)
      .groupBy(({ version }) => Math.floor(version))
      .findLast()
      .sortBy(({ version }) => Number(version.split('.')[1] ?? 0))
      .value()
  }
}

module.exports = {
  initialize,
}
