function initialize({ User }) {
  async function execute({ manuscriptId, userId }) {
    return User.getUserRoleOnManuscript({
      userId,
      manuscriptId,
    })
  }
  return {
    execute,
  }
}

module.exports = {
  initialize,
}
