function initialize({ Author }) {
  async function execute({ manuscriptId }) {
    return Author.findBy({
      queryObject: { manuscriptId },
      orderByField: 'position',
      order: 'asc',
    })
  }

  return { execute }
}

module.exports = {
  initialize,
}
