function initialize({ User }) {
  return {
    execute,
  }

  async function execute({ manuscriptId }) {
    return User.findAssignedChecker(manuscriptId)
  }
}

module.exports = {
  initialize,
}
