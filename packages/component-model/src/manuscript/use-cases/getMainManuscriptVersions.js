function initialize({ Manuscript }) {
  return {
    execute,
  }

  async function execute({ submissionId, flowType }) {
    return Manuscript.getMainManuscriptVersions({
      submissionId,
      flowType,
    })
  }
}

module.exports = {
  initialize,
}
