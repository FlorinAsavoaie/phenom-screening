const HindawiBaseModel = require('../../hindawiBaseModel')
const User = require('../user/user')
const ManuscriptChecks = require('../manuscriptChecks/manuscriptChecks')

const logger = require('@pubsweet/logger')

class Manuscript extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        status: { type: 'string', default: this.Statuses.AUTOMATIC_CHECKS },
        customId: { type: ['string', null] },
        references: { type: ['string', null] },
        title: { type: 'string', default: '' },
        abstract: { type: 'string', default: '' },
        articleType: { type: 'string', default: '' },
        journalId: { type: 'string', format: 'uuid' },
        journalSectionId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
        submissionId: { type: 'string', format: 'uuid' },
        phenomId: { type: 'string', format: 'uuid' },
        submissionDate: { type: ['string', 'object'], format: 'date-time' },
        qualityChecksSubmittedDate: {
          type: ['string', 'object', null],
          format: 'date-time',
        },
        version: { type: 'string', default: '1' },
        flowType: { type: 'string', default: this.FlowTypes.SCREENING },
        step: {
          enum: Object.values(this.Steps),
        },
        conflictOfInterest: { type: ['string', 'null'] },
        dataAvailability: { type: ['string', 'null'] },
        fundingStatement: { type: ['string', 'null'] },
        materialCheckConfigId: {
          type: ['string', 'null'],
          format: 'uuid',
        },
        peerReviewCheckConfigId: {
          type: ['string', 'null'],
          format: 'uuid',
        },
        authorResponded: { type: 'boolean', default: false },
        linkedSubmissionCustomId: { type: ['string', null] },
      },
    }
  }

  static get relationMappings() {
    return {
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../file').model,
        join: {
          from: 'manuscript.id',
          to: 'file.manuscriptId',
        },
      },
      authors: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../author').model,
        join: {
          from: 'manuscript.id',
          to: 'author.manuscriptId',
        },
      },
      manuscriptChecks: {
        relation: HindawiBaseModel.HasOneRelation,
        modelClass: require('../manuscriptChecks').model,
        join: {
          from: 'manuscript.id',
          to: 'manuscript_checks.manuscriptId',
        },
      },
      manuscriptValidations: {
        relation: HindawiBaseModel.HasOneRelation,
        modelClass: require('../manuscriptValidations').model,
        join: {
          from: 'manuscript.id',
          to: 'manuscript_validations.manuscriptId',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../journal').model,
        join: {
          from: 'manuscript.journalId',
          to: 'journal.id',
        },
      },
      user: {
        relation: HindawiBaseModel.HasOneThroughRelation,
        modelClass: require('../user').model,
        join: {
          from: 'manuscript.id',
          through: {
            from: 'user_manuscript.manuscriptId',
            to: 'user_manuscript.userId',
          },
          to: 'user.id',
        },
      },
      team: {
        relation: HindawiBaseModel.HasOneThroughRelation,
        modelClass: require('../team').model,
        join: {
          from: 'manuscript.id',
          through: {
            from: 'team_manuscript.manuscriptId',
            to: 'team_manuscript.teamId',
          },
          to: 'team.id',
        },
      },
      members: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../member').model,
        join: {
          from: 'manuscript.id',
          to: 'member.manuscriptId',
        },
      },
      manuscriptMaterialChecks: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../manuscriptMaterialCheck').model,
        join: {
          from: 'manuscript.id',
          to: 'manuscript_material_checks.manuscriptId',
        },
      },
      manuscriptPeerReviewChecks: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../manuscriptPeerReviewCheck').model,
        join: {
          from: 'manuscript.id',
          to: 'manuscript_peer_review_checks.manuscriptId',
        },
      },
      journalSection: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../journalSection').model,
        join: {
          from: 'manuscript.journalSectionId',
          to: 'journal_section.id',
        },
      },
      specialIssue: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./../specialIssue').model,
        join: {
          from: 'manuscript.specialIssueId',
          to: 'special_issue.id',
        },
      },
      materialCheckConfig: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../materialCheckConfig').model,
        join: {
          from: 'manuscript.materialCheckConfigId',
          to: 'material_check_config.id',
        },
      },
      peerReviewCheckConfig: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../peerReviewCheckConfig').model,
        join: {
          from: 'manuscript.peerReviewCheckConfigId',
          to: 'peer_review_check_config.id',
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../review').model,
        join: {
          from: 'manuscript.id',
          to: 'review.manuscriptId',
        },
      },
      notes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../note').model,
        join: {
          from: 'manuscript.id',
          to: 'note.manuscriptId',
        },
      },
      auditLogs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('../auditLog').model,
        join: {
          from: 'manuscript.id',
          to: 'audit_log.manuscriptId',
        },
      },
    }
  }

  static get FlowTypes() {
    return {
      SCREENING: 'screening',
      QUALITY_CHECK: 'qualityCheck',
    }
  }

  static get FlowTypeLabels() {
    return {
      qualityCheck: 'Quality Checking',
      screening: 'Screening',
    }
  }

  get flowTypeLabel() {
    return Manuscript.FlowTypeLabels[this.flowType]
  }

  static get Statuses() {
    return {
      PAUSED: 'paused',
      SCREENING_APPROVED: 'screeningApproved',
      SCREENING_REJECTED: 'screeningRejected',
      RETURNED_TO_DRAFT: 'returnedToDraft',
      OLDER_VERSION: 'olderVersion',
      IN_PROGRESS: 'inProgress',
      FILES_REQUESTED: 'filesRequested',
      QUALITY_CHECK_APPROVED: 'qualityCheckApproved',
      QUALITY_CHECK_REJECTED: 'qualityCheckRejected',
      WITHDRAWN: 'withdrawn',
      VOID: 'void',
      AUTOMATIC_CHECKS: 'automaticChecks',
      ESCALATED: 'escalated',
      SENT_TO_PEER_REVIEW: 'sentToPeerReview',
    }
  }

  static get filteredStatuses() {
    return {
      inProgress: [
        Manuscript.Statuses.PAUSED,
        Manuscript.Statuses.FILES_REQUESTED,
        Manuscript.Statuses.IN_PROGRESS,
        Manuscript.Statuses.RETURNED_TO_DRAFT,
        Manuscript.Statuses.SENT_TO_PEER_REVIEW,
      ],
      archived: [
        Manuscript.Statuses.SCREENING_APPROVED,
        Manuscript.Statuses.SCREENING_REJECTED,
        Manuscript.Statuses.QUALITY_CHECK_APPROVED,
        Manuscript.Statuses.QUALITY_CHECK_REJECTED,
        Manuscript.Statuses.WITHDRAWN,
        Manuscript.Statuses.VOID,
      ],
    }
  }

  static get filteredStatusesForAdmin() {
    return {
      ...this.filteredStatuses,
      processing: [Manuscript.Statuses.AUTOMATIC_CHECKS],
      escalated: [Manuscript.Statuses.ESCALATED],
    }
  }

  static get filteredStatusesForTeamLead() {
    return {
      ...this.filteredStatuses,
      escalated: [Manuscript.Statuses.ESCALATED],
    }
  }

  static get Steps() {
    return {
      IN_SCREENING_CHECKING: 'inScreeningChecking',
      IN_MATERIAL_CHECKING: 'inMaterialsChecking',
      IN_PEER_REVIEW_CYCLE_CHECKING: 'inPeerReviewCycleChecking',
    }
  }

  static async getUserManuscripts({
    page,
    pageSize,
    userId,
    searchValue,
    filterOption = Manuscript.Statuses.IN_PROGRESS,
    authorResponded,
  }) {
    const isAdmin = await User.isAdmin(userId)

    if (isAdmin) {
      return Manuscript.getManuscriptsForAdmin({
        page,
        pageSize,
        searchValue,
        statuses: Manuscript.filteredStatusesForAdmin[filterOption],
        authorResponded,
      })
    }
    const isTeamLead = User.isTeamLeadInAtLeastOneTeam(userId)

    return Manuscript.getManuscriptsForCheckersAndTl({
      userId,
      searchValue,
      statuses: isTeamLead
        ? Manuscript.filteredStatusesForTeamLead[filterOption]
        : Manuscript.filteredStatuses[filterOption],
      page,
      authorResponded,
      pageSize,
    })
  }

  static async getManuscriptsForAdmin({
    searchValue,
    statuses,
    page,
    pageSize,
    authorResponded,
  }) {
    return this.query()
      .distinct('manuscript.*')
      .join(
        'manuscript_checks',
        'manuscript_checks.manuscriptId',
        '=',
        'manuscript.id',
      )
      .leftJoin('author', 'author.manuscriptId', '=', 'manuscript.id')
      .leftJoin('journal', 'journal.id', '=', 'manuscript.journalId')
      .leftJoin(
        'special_issue',
        'special_issue.id',
        '=',
        'manuscript.specialIssueId',
      )
      .whereIn('manuscript.status', statuses)
      .where((qb) => {
        if (authorResponded) {
          qb.where('authorResponded', authorResponded)
        } else {
          qb.where({ authorResponded: true }).orWhere({
            authorResponded: false,
          })
        }
      })
      .where((builder) => search(builder, searchValue))
      .orderBy('submissionDate', 'asc')
      .page(page, pageSize)
  }

  static async getManuscriptsForCheckersAndTl({
    userId,
    searchValue,
    statuses,
    page,
    pageSize,
    authorResponded,
  }) {
    return this.query()
      .distinct('manuscript.*')
      .leftJoin(
        'user_manuscript',
        'user_manuscript.manuscript_id',
        '=',
        'manuscript.id',
      )
      .leftJoin('author', 'author.manuscriptId', '=', 'manuscript.id')
      .leftJoin('journal', 'journal.id', '=', 'manuscript.journalId')
      .leftJoin(
        'special_issue',
        'special_issue.id',
        '=',
        'manuscript.specialIssueId',
      )
      .where({
        'user_manuscript.user_id': userId,
      })
      .where((qb) => {
        if (authorResponded) {
          qb.where('authorResponded', authorResponded)
        } else {
          qb.where({ authorResponded: true }).orWhere({
            authorResponded: false,
          })
        }
      })
      .whereIn('manuscript.status', statuses)
      .where((builder) => search(builder, searchValue))
      .union([
        Manuscript.query()
          .distinct('manuscript.*')
          .leftJoin(
            'team_manuscript',
            'team_manuscript.manuscript_id',
            '=',
            'manuscript.id',
          )
          .leftJoin(
            'team_user',
            'team_user.team_id',
            '=',
            'team_manuscript.team_id',
          )
          .leftJoin('author', 'author.manuscriptId', '=', 'manuscript.id')
          .leftJoin('journal', 'journal.id', '=', 'manuscript.journalId')
          .leftJoin(
            'special_issue',
            'special_issue.id',
            '=',
            'manuscript.specialIssueId',
          )
          .where({
            'team_user.user_id': userId,
            'team_user.role': User.Roles.TEAM_LEADER,
          })
          .where((qb) => {
            if (authorResponded) {
              qb.where('authorResponded', authorResponded)
            } else {
              qb.where({ authorResponded: true }).orWhere({
                authorResponded: false,
              })
            }
          })
          .whereIn('manuscript.status', statuses)
          .where((builder) => search(builder, searchValue)),
      ])
      .orderBy('submissionDate', 'asc')
      .page(page, pageSize)
  }

  static async getAvailableCheckers(manuscriptId) {
    return this.query()
      .distinct('user.*')
      .leftJoin(
        'team_manuscript',
        'team_manuscript.manuscript_id',
        '=',
        'manuscript.id',
      )
      .leftJoin(
        'team_user',
        'team_user.team_id',
        '=',
        'team_manuscript.team_id',
      )
      .leftJoin('user', 'user.id', '=', 'team_user.user_id')
      .where({
        'manuscript.id': manuscriptId,
        'user.isConfirmed': true,
      })
      .whereNot({
        'team_user.role': User.Roles.TEAM_LEADER,
      })
  }

  static async canAccessManuscript({ manuscriptId, userId }) {
    const isAdmin = await User.isAdmin(userId)

    if (isAdmin) {
      return true
    }

    const manuscript = await Manuscript.find(manuscriptId)
    const isAvailableForCheckers =
      manuscript.status !== Manuscript.Statuses.ESCALATED
    if (manuscript.flowType === Manuscript.FlowTypes.SCREENING) {
      const userRole = await User.getUserRoleOnManuscript({
        userId,
        manuscriptId,
      })
      if (userRole === User.Roles.SCREENER) {
        return isAvailableForCheckers
      }
      return !!userRole
    }

    if (manuscript.flowType === Manuscript.FlowTypes.QUALITY_CHECK) {
      const latestManuscript = await Manuscript.getLatestManuscriptVersion({
        queryObject: {
          submissionId: manuscript.submissionId,
          flowType: Manuscript.FlowTypes.QUALITY_CHECK,
        },
      })

      const userRole = await User.getUserRoleOnManuscript({
        userId,
        manuscriptId: latestManuscript.id,
      })
      if (userRole === User.Roles.QUALITY_CHECKER) {
        return isAvailableForCheckers
      }
      return !!userRole
    }
  }

  static async canAccessSubmission({ submissionId, userId }) {
    const manuscript = await Manuscript.getLatestManuscriptVersion({
      queryObject: {
        submissionId,
      },
    })

    return Manuscript.canAccessManuscript({
      manuscriptId: manuscript.id,
      userId,
    })
  }

  static async getLatestManuscriptVersion({
    queryObject: { submissionId, flowType },
    eagerLoadRelations,
  }) {
    const queryObject = flowType ? { submissionId, flowType } : { submissionId }

    return Manuscript.query()
      .where(queryObject)
      .whereNot('status', 'olderVersion')
      .orderBy([
        {
          column: 'created',
          order: 'desc',
        },
        {
          column: 'version',
          order: 'desc',
        },
      ])
      .first()
      .withGraphFetched(eagerLoadRelations)
  }

  static async getSimilarSubmissions(manuscript) {
    const authorEmails = manuscript.authors.map((a) => a.email).sort()

    return Manuscript.query()
      .join(
        'manuscript_authors',
        'manuscript.id',
        '=',
        'manuscript_authors.manuscript_id',
      )
      .groupBy('manuscript.id')
      .where('status', '!=', Manuscript.Statuses.OLDER_VERSION)
      .where(
        'manuscript_authors.manuscript_title',
        '=',
        manuscript.title.trim().replace(/ {1,}/g, ' '),
      )
      .orWhere('manuscript_authors.author_list', '=', authorEmails)
      .having('submissionId', '!=', manuscript.submissionId)
  }

  async assignChecker(checker) {
    this.updateProperties({
      user: { id: checker.id },
      team: { id: checker.team_id },
    })

    const assignedChecker = await User.setAssignationDate({
      id: checker.id,
      teamId: checker.team_id,
    })

    await this.saveGraph({ relate: true, unrelate: true })

    logger.info(
      `Manuscript ${this.id} was assigned to the checker: ${checker.id} from the team: ${checker.team_id}.`,
    )

    return assignedChecker
  }

  getManuscriptFile() {
    return this.files.find((file) => file.type === 'manuscript')
  }

  isPending() {
    return [
      Manuscript.Statuses.PAUSED,
      Manuscript.Statuses.FILES_REQUESTED,
      Manuscript.Statuses.RETURNED_TO_DRAFT,
    ].includes(this.status)
  }

  isInAutomaticChecking() {
    return this.status === Manuscript.Statuses.AUTOMATIC_CHECKS
  }

  isSentToPeerReview() {
    return this.status === Manuscript.Statuses.SENT_TO_PEER_REVIEW
  }

  static async getUnassignedManuscripts({ journalIds, manuscriptFlowType }) {
    const manuscriptChecksConstrains =
      ManuscriptChecks.getManuscriptChecksConstrains()

    return this.query()
      .withGraphJoined('team')
      .join(
        'manuscript_checks',
        'manuscript_checks.manuscriptId',
        '=',
        'manuscript.id',
      )
      .whereNot(manuscriptChecksConstrains)
      .where({
        status: 'inProgress',
        team: null,
        flowType: manuscriptFlowType,
      })
      .whereIn('journalId', journalIds)
  }

  static async getPreviousVersion({
    submissionId,
    flowType,
    eagerLoadRelations,
  }) {
    const manuscripts = await this.query()
      .where({
        submissionId,
        flowType,
      })
      .orderBy([
        { column: 'version', order: 'desc' },
        { column: 'created', order: 'desc' },
      ])
      .withGraphFetched(eagerLoadRelations)

    if (manuscripts.length < 2) {
      return null
    }

    return manuscripts[1]
  }

  static async getMainManuscriptVersions({ submissionId, flowType }) {
    return Manuscript.query()
      .where({
        submissionId,
        flowType,
      })
      .andWhereRaw(`version not like '%.%'`)
      .orderByRaw('version::FLOAT asc')
      .select('id', 'version')
  }

  static async getManuscriptVersions({ submissionId, flowType }) {
    return Manuscript.query()
      .where({
        submissionId,
        flowType,
      })
      .select('id', 'version')
  }
}

function search(builder, searchValue) {
  if (searchValue) {
    builder
      .where({
        'manuscript.custom_id': searchValue,
      })
      .orWhereRaw('lower(manuscript.title) LIKE ?', [
        `%${searchValue.toLowerCase()}%`,
      ])
      .orWhereRaw('lower(journal.name) LIKE ?', [
        `%${searchValue.toLowerCase()}%`,
      ])
      .orWhereRaw('lower(special_issue.name) LIKE ?', [
        `%${searchValue.toLowerCase()}%`,
      ])
      .orWhereRaw('lower(author.surname) LIKE ?', [
        `%${searchValue.toLowerCase()}%`,
      ])
      .orWhereRaw('lower(author.given_names) LIKE ?', [
        `%${searchValue.toLowerCase()}%`,
      ])
      .orWhereRaw(
        `concat(lower(author.given_names), ' ', lower(author.surname)) LIKE ?`,
        [`%${searchValue.toLowerCase()}%`],
      )
      .orWhereRaw(
        `concat(lower(author.surname), ' ', lower(author.given_names)) LIKE ?`,
        [`%${searchValue.toLowerCase()}%`],
      )
  }

  return builder
}

module.exports = Manuscript
