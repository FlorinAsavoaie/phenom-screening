const { pick, get, chain, forOwn } = require('lodash')
const { model: Member } = require('../member')
const { model: User } = require('../user')
const { model: Note } = require('../note')

function domainToEventModel(manuscript) {
  const meta = {
    ...pick(manuscript, [
      'customId',
      'submissionId',
      'journalId',
      'title',
      'abstract',
      'version',
      'conflictOfInterest',
      'dataAvailability',
      'fundingStatement',
      'specialIssueId',
      'qualityChecksSubmittedDate',
    ]),
    id: manuscript.phenomId,
    articleType: { name: manuscript.articleType },
    sectionId: manuscript.journalSectionId,
    submissionCreatedDate: manuscript.submissionDate,
  }

  const authors = manuscript.authors.map((a) => ({
    ...pick(a, [
      'id',
      'created',
      'updated',
      'surname',
      'givenNames',
      'email',
      'aff', // deprecated. It can be removed after we ensure no other projects use it
      'country',
      'isSubmitting',
      'isCorresponding',
    ]),
    affiliation: {
      name: a.aff,
    },
    orcidId: a.orcid,
  }))

  const files = manuscript.files.map((f) => ({
    ...pick(f, [
      'id',
      'created',
      'updated',
      'type',
      'fileName',
      'mimeType',
      'size',
      'originalName',
    ]),
    providerKey: f.originalFileProviderKey || f.providerKey,
  }))

  const editors = manuscript.members
    .filter((e) =>
      [Member.Roles.ACADEMIC_EDITOR, Member.Roles.TRIAGE_EDITOR].includes(
        e.role,
      ),
    )
    .map((e) => ({
      ...pick(e, [
        'id',
        'aff',
        'email',
        'title',
        'country',
        'surname',
        'givenNames',
      ]),
      role: {
        type: e.role,
        label: e.roleLabel,
      },
    }))

  return {
    ...meta,
    authors,
    files,
    editors,
  }
}

const AutomaticCheckResultValue = {
  succeeded: 'succeeded',
  failed: 'failed',
}

const ConflictOfInterestType = {
  none: 'none',
  reviewers: 'reviewers',
  triageEditor: 'triageEditor',
  academicEditor: 'academicEditor',
}

const AuthorProfileVerificationStatus = {
  verifiedByWebOfScience: 'verifiedByWebOfScience',
  verifiedOutOfTool: 'verifiedOutOfTool',
  cantBeVerified: 'cantBeVerified',
  notVerified: 'notVerified',
}

function domainToPeerReviewCycleCheckingProcessEvent({
  manuscript,
  similarSubmissions,
  conflictOfInterest,
  improperReviewReviewerIds,
  isDecisionMadeInError,
  comment,
}) {
  return {
    id: manuscript.id,
    created: manuscript.created,
    updated: manuscript.updated,
    submissionId: manuscript.submissionId,
    customId: manuscript.customId,
    manuscriptId: manuscript.phenomId,
    checker: getAssignedChecker(manuscript),
    teamLeaders: getTeamLeaders(manuscript),
    checks: {
      preScreening: getPreScreeningChecks(manuscript, similarSubmissions),
      infoValidation: getManuscriptInfoValidation(manuscript),
      authorProfiles: getAuthorsProfiles(manuscript),
      peerReviewCycle: getPeerReviewCycleChecks({
        manuscript,
        conflictOfInterest,
        isDecisionMadeInError,
        improperReviewReviewerIds,
        comment,
      }),
    },
  }

  function getTeamLeaders(manuscript) {
    const users = get(manuscript, 'team.users', [])
    return users
      .filter((user) => user.role === User.Roles.TEAM_LEADER)
      .map(convertUserToChecker)
  }

  function getAssignedChecker(manuscript) {
    return {
      ...convertUserToChecker(manuscript.user),
      role: User.Roles.QUALITY_CHECKER,
    }
  }

  function convertUserToChecker(user) {
    return pick(user, [
      'id',
      'email',
      'surname',
      'givenNames',
      'role',
      'isConfirmed',
    ])
  }

  function getPreScreeningChecks(manuscript, similarSubmissions) {
    return {
      languageCheck: getLanguageCheckInfo(manuscript),
      suspiciousCheck: getSuspiciousCheckInfo(manuscript),
      similarityCheck: getSimilarityCheckInfo(manuscript),
      similarSubmissions: similarSubmissions.map(getSimilarSubmissionInfo),
    }
  }

  function getSimilarSubmissionInfo(similarSubmission) {
    return {
      submissionId: similarSubmission.submissionId,
      customId: similarSubmission.customId,
      manuscriptId: similarSubmission.phenomId,
    }
  }

  function getLanguageCheckInfo({
    manuscriptChecks: { wordCount, languagePercentage },
  }) {
    return {
      result: AutomaticCheckResultValue.succeeded,
      details: {
        wordCount,
        languageMatchPercentage: languagePercentage,
      },
    }
  }

  function getSuspiciousCheckInfo({
    manuscriptChecks: { suspiciousWords: wordsMap },
  }) {
    const suspiciousWords = []
    forOwn(wordsMap, (objValue, word) => {
      suspiciousWords.push({ word, occurrences: objValue.noOfOccurrences })
    })

    return {
      result: AutomaticCheckResultValue.succeeded,
      details: {
        suspiciousWords,
      },
    }
  }

  function getSimilarityCheckInfo({
    manuscriptChecks: {
      firstSourceSimilarityPercentage,
      totalSimilarityPercentage,
      reportUrl,
    },
  }) {
    return {
      result: AutomaticCheckResultValue.succeeded,
      details: {
        firstSourceSimilarityPercentage,
        totalSimilarityPercentage,
        similarityReportUrl: reportUrl,
      },
    }
  }

  function getManuscriptInfoValidation(manuscript) {
    const { manuscriptValidations, notes } = manuscript

    return {
      title: getTitle(notes),
      abstract: getAbstract(notes),
      authors: getAuthors(notes),
      relevance: getRelevance(notes),
      referenceExtraction: getReference(notes),
    }

    function getTitle(notes) {
      const observations = chain(notes)
        .find((note) => note.type === Note.Types.TITLE_NOTE)
        .get('content')
        .value()

      return {
        ...(observations ? { observations } : {}),
      }
    }

    function getAbstract(notes) {
      const observations = chain(notes)
        .find((note) => note.type === Note.Types.ABSTRACT_NOTE)
        .get('content')
        .value()

      return {
        ...pick(manuscriptValidations, ['includesCitations', 'graphical']),
        ...(observations ? { observations } : {}),
      }
    }

    function getAuthors(notes) {
      const observations = chain(notes)
        .find((note) => note.type === Note.Types.AUTHORS_NOTE)
        .get('content')
        .value()

      return {
        ...pick(manuscriptValidations, [
          'differentAuthors',
          'missingAffiliations',
          'nonacademicAffiliations',
        ]),
        ...(observations ? { observations } : {}),
      }
    }

    function getRelevance(notes) {
      const observations = chain(notes)
        .find((note) => note.type === Note.Types.RELEVANCE_NOTE)
        .get('content')
        .value()

      return {
        ...pick(manuscriptValidations, [
          'topicNotAppropriate',
          'articleTypeIsNotAppropriate',
          'manuscriptIsMissingSections',
        ]),
        ...(observations ? { observations } : {}),
      }
    }

    function getReference(notes) {
      const observations = chain(notes)
        .find((note) => note.type === Note.Types.REFERENCES_NOTE)
        .get('content')
        .value()

      return {
        ...pick(manuscriptValidations, [
          'missingReferences',
          'wrongReferences',
        ]),
        ...(observations ? { observations } : {}),
      }
    }
  }

  function getAuthorsProfiles(manuscript) {
    return manuscript.authors.map((author) => ({
      email: author.email,
      verificationStatus: getVerificationStatus(author),
      sanctionInfo: getSanctionInfo(author),
    }))
  }

  function getVerificationStatus({ isVerified, isVerifiedOutOfTool }) {
    if (isVerified === false && isVerifiedOutOfTool === false) {
      return AuthorProfileVerificationStatus.notVerified
    }

    if (isVerified === null) {
      return AuthorProfileVerificationStatus.cantBeVerified
    }

    return isVerifiedOutOfTool
      ? AuthorProfileVerificationStatus.verifiedOutOfTool
      : AuthorProfileVerificationStatus.verifiedByWebOfScience
  }

  function getSanctionInfo({ isOnBadDebtList, isOnWatchList, isOnBlackList }) {
    return {
      isOnBadDebtList,
      isOnWatchList,
      isOnBlackList,
    }
  }

  function getReturnToPeerReasons({
    conflictOfInterest,
    improperReviewReviewerIds,
    isDecisionMadeInError,
  }) {
    return {
      conflictOfInterest: getConflictOfInterest(manuscript, conflictOfInterest),
      decisionMadeInError: isDecisionMadeInError,
      improperReview: getImproperReview(improperReviewReviewerIds),
    }
  }
  function getPeerReviewCycleChecks({
    manuscript,
    conflictOfInterest,
    improperReviewReviewerIds,
    isDecisionMadeInError,
    comment,
  }) {
    return {
      returnToPeerReviewDetails: {
        reasons: getReturnToPeerReasons({
          conflictOfInterest,
          improperReviewReviewerIds,
          isDecisionMadeInError,
        }),
        comment,
      },
      questions: getPeerReviewCycleQuestions(manuscript),
    }
  }

  function getConflictOfInterest(
    { members },
    { withTriageEditor, withAcademicEditor, reviewerIds },
  ) {
    const conflictOfInterest = []

    if (withTriageEditor) {
      const triageEditorMember = members.find(
        (member) =>
          member.role === Member.Roles.TRIAGE_EDITOR &&
          member.status === Member.Status.ACTIVE,
      )

      conflictOfInterest.push({
        type: ConflictOfInterestType.triageEditor,
        conflictedTeamMembers: [{ id: triageEditorMember.phenomMemberId }],
      })
    }

    if (withAcademicEditor) {
      const academicEditorMember = members.find(
        (member) =>
          member.role === Member.Roles.ACADEMIC_EDITOR &&
          member.status === Member.Status.ACCEPTED,
      )
      conflictOfInterest.push({
        type: ConflictOfInterestType.academicEditor,
        conflictedTeamMembers: [{ id: academicEditorMember.phenomMemberId }],
      })
    }

    if (reviewerIds.length) {
      const conflictedTeamMembers = reviewerIds.map((reviewer) => ({
        id: reviewer,
      }))

      conflictOfInterest.push({
        type: ConflictOfInterestType.reviewers,
        conflictedTeamMembers,
      })
    }

    return conflictOfInterest
  }

  function getImproperReview(improperReviewReviewerIds) {
    return {
      reviewers: improperReviewReviewerIds.map((reviewerId) => ({
        id: reviewerId,
      })),
    }
  }

  function getPeerReviewCycleQuestions({ manuscriptPeerReviewChecks }) {
    return manuscriptPeerReviewChecks
      .filter(isOptionSelected)
      .map(convertPeerReviewCheckToQuestion)
  }

  function isOptionSelected(manuscriptPeerReviewCheck) {
    return manuscriptPeerReviewCheck.selectedOption !== null
  }

  function convertPeerReviewCheckToQuestion({
    name,
    selectedOption,
    observation,
  }) {
    if (observation) {
      return {
        name,
        selectedOption,
        observation,
      }
    }

    return {
      name,
      selectedOption,
    }
  }
}

module.exports = {
  domainToEventModel,
  domainToPeerReviewCycleCheckingProcessEvent,
}
