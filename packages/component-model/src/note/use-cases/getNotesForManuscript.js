function initialize({ Note }) {
  function execute(manuscriptId) {
    return Note.findBy({ queryObject: { manuscriptId } })
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']
module.exports = {
  initialize,
  authsomePolicies,
}
