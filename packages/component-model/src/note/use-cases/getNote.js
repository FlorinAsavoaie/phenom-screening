function initialize({ Note }) {
  function execute({ manuscriptId, type }) {
    return Note.findOneBy({ queryObject: { manuscriptId, type } })
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']
module.exports = {
  initialize,
  authsomePolicies,
}
