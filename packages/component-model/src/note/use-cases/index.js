const getNoteUseCase = require('./getNote')
const getNotesForManuscriptUseCase = require('./getNotesForManuscript')

module.exports = {
  getNoteUseCase,
  getNotesForManuscriptUseCase,
}
