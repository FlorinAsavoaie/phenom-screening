const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async getNote(_, args, ctx) {
      return useCases.getNoteUseCase.initialize(models).execute(args)
    },
    async getNotesForManuscript(_, { manuscriptId }, ctx) {
      return useCases.getNotesForManuscriptUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
}
