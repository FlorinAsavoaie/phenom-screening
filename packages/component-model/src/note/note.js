const HindawiBaseModel = require('../../hindawiBaseModel')

class Note extends HindawiBaseModel {
  static get tableName() {
    return 'note'
  }
  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: { type: 'string', format: 'uuid' },
        type: { type: 'string', default: '' },
        content: { type: 'string', default: '' },
      },
    }
  }
  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: 'note.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  static get Types() {
    return {
      TITLE_NOTE: 'titleNote',
      AUTHORS_NOTE: 'authorsNote',
      ABSTRACT_NOTE: 'abstractNote',
      DECISION_NOTE: 'decisionNote',
      REFERENCES_NOTE: 'referencesNote',
      RELEVANCE_NOTE: 'relevanceNote',
      VOID_REASON_NOTE: 'voidReasonNote',
      PAUSE_REASON_NOTE: 'pauseReasonNote',
      ESCALATION_REASON_NOTE: 'escalationReasonNote',
      SEND_TO_PEER_REVIEW_NOTE: 'sendToPeerReviewNote',
    }
  }

  static async findValidationNotes({ queryObject }) {
    const INFO_VALIDATION_TYPES = [
      Note.Types.TITLE_NOTE,
      Note.Types.AUTHORS_NOTE,
      Note.Types.ABSTRACT_NOTE,
      Note.Types.REFERENCES_NOTE,
      Note.Types.RELEVANCE_NOTE,
    ]

    return this.query()
      .where(queryObject)
      .whereIn('type', INFO_VALIDATION_TYPES)
  }

  static async findLatestNote({ queryObject }) {
    return this.query().where(queryObject).orderBy('created', 'desc').first()
  }
}

module.exports = Note
