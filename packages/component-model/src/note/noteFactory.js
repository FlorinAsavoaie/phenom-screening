const Note = require('./note')

module.exports = {
  createDecisionNote({ content, manuscriptId }) {
    return new Note({
      content,
      manuscriptId,
      type: Note.Types.DECISION_NOTE,
    })
  },
  createNote({ manuscriptId, type, content }) {
    return new Note({
      manuscriptId,
      type,
      content,
    })
  },
}
