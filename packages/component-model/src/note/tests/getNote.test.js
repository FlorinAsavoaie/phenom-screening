process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const useCases = require('../use-cases')
const Chance = require('chance')

const chance = new Chance()
const Note = require('../note')

describe('Get note Use case', () => {
  it('should get note', async () => {
    Note.findOneBy = jest.fn()

    const params = {
      manuscriptId: chance.guid(),
      type: chance.string(),
    }
    const models = {
      Note,
    }
    await useCases.getNoteUseCase.initialize(models).execute(params)

    expect(Note.findOneBy).toHaveBeenCalledWith({
      queryObject: { manuscriptId: params.manuscriptId, type: params.type },
    })
  })
})
