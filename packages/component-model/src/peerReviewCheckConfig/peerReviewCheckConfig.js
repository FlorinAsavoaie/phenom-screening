const HindawiBaseModel = require('../../hindawiBaseModel')
const Manuscript = require('./../manuscript/manuscript')

class PeerReviewCheckConfig extends HindawiBaseModel {
  static get tableName() {
    return 'peer_review_check_config'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        config: {
          type: 'json',
        },
        version: {
          type: 'numeric',
        },
      },
    }
  }

  static async getLatest() {
    return this.query()
      .whereRaw(
        `version = (
              select max(c.version) from peer_review_check_config c
            )`,
      )
      .first()
  }

  static async getByManuscriptIdAndName({ manuscriptId, name }) {
    const { peerReviewCheckConfig } = await Manuscript.find(
      manuscriptId,
      'peerReviewCheckConfig',
    )

    return peerReviewCheckConfig.config[name]
  }

  static async isObservationSupported({ manuscriptId, name, selectedOption }) {
    const { options } = await PeerReviewCheckConfig.getByManuscriptIdAndName({
      manuscriptId,
      name,
    })

    const option = options.find((o) => o.value === selectedOption)

    return option && option.withObservation
  }

  static async isSelectedOptionAvailable({
    manuscriptId,
    name,
    selectedOption,
  }) {
    const { options } = await PeerReviewCheckConfig.getByManuscriptIdAndName({
      manuscriptId,
      name,
    })

    return !!options.find((option) => option.value === selectedOption)
  }
}
module.exports = PeerReviewCheckConfig
