const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./manuscriptMaterialCheck'),
  useCases: require('./use-cases'),
  resolvers: require('./resolvers'),
  factory: require('./manuscriptMaterialCheckFactory'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
