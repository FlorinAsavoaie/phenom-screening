const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async getManuscriptMaterialChecks(_, { manuscriptId }) {
      return useCases.getManuscriptMaterialChecksUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
}
