function initialize({ ManuscriptMaterialCheck, Manuscript }) {
  return {
    execute,
  }

  async function execute(manuscriptId) {
    let manuscriptMaterialChecks = await ManuscriptMaterialCheck.findBy({
      queryObject: { manuscriptId },
    })

    const { materialCheckConfig } = await Manuscript.find(
      manuscriptId,
      'materialCheckConfig',
    )

    manuscriptMaterialChecks = manuscriptMaterialChecks.map(
      (materialCheck) => ({
        config: materialCheckConfig.config[materialCheck.name],
        ...materialCheck,
      }),
    )

    return manuscriptMaterialChecks
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
