const useCases = require('../use-cases')
const models = require('@pubsweet/models')
const Chance = require('chance')

const chance = new Chance()
const { Manuscript, ManuscriptMaterialCheck } = models

describe('getManuscriptMaterialChecks', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should get material checks for a specific manuscript', async () => {
    const manuscriptId = chance.guid()
    const manuscripts = [{ id: manuscriptId, name: 'hasFrontPage' }]

    const manuscript = {
      id: manuscriptId,
      materialCheckConfig: {
        config: { hasFrontPage: { options: [], order: 1 } },
      },
    }

    ManuscriptMaterialCheck.findBy = jest.fn().mockReturnValue(manuscripts)
    Manuscript.find = jest.fn().mockReturnValue(manuscript)

    const result = await useCases.getManuscriptMaterialChecksUseCase
      .initialize({ ManuscriptMaterialCheck, Manuscript })
      .execute(manuscriptId)

    expect(result).toStrictEqual([
      {
        id: manuscriptId,
        name: 'hasFrontPage',
        config: { options: [], order: 1 },
      },
    ])
    expect(ManuscriptMaterialCheck.findBy).toHaveBeenCalledTimes(1)
    expect(ManuscriptMaterialCheck.findBy).toHaveBeenCalledWith({
      queryObject: { manuscriptId },
    })

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      'materialCheckConfig',
    )
  })
})
