const HindawiBaseModel = require('../../hindawiBaseModel')

class ManuscriptMaterialCheck extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript_material_checks'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: {
          type: 'string',
          format: 'uuid',
        },
        name: {
          type: 'string',
          default: null,
        },
        selectedOption: {
          type: ['string', 'null'],
          default: null,
        },
        additionalInfo: {
          type: ['string'],
        },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: `${this.tableName}.manuscriptId`,
          to: 'manuscript.id',
        },
      },
    }
  }
}

module.exports = ManuscriptMaterialCheck
