const ManuscriptMaterialCheck = require('./manuscriptMaterialCheck')

module.exports = {
  createInitial({ manuscriptId, name }) {
    return new ManuscriptMaterialCheck({
      manuscriptId,
      name,
      additionalInfo: '',
      selectedOption: null,
    })
  },
}
