const ManuscriptPeerReviewCheck = require('./manuscriptPeerReviewCheck')

module.exports = {
  createInitial({ manuscriptId, name }) {
    return new ManuscriptPeerReviewCheck({
      manuscriptId,
      name,
      observation: '',
      selectedOption: null,
    })
  },
}
