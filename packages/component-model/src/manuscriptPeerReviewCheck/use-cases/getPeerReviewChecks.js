function initialize({ ManuscriptPeerReviewCheck, Manuscript }) {
  return {
    execute,
  }

  async function execute(manuscriptId) {
    let manuscriptPeerReviewChecks = await ManuscriptPeerReviewCheck.findBy({
      queryObject: { manuscriptId },
    })

    const { peerReviewCheckConfig } = await Manuscript.find(
      manuscriptId,
      'peerReviewCheckConfig',
    )

    manuscriptPeerReviewChecks = manuscriptPeerReviewChecks.map(
      (peerReviewCheck) => ({
        config: peerReviewCheckConfig.config[peerReviewCheck.name],
        ...peerReviewCheck,
      }),
    )

    return manuscriptPeerReviewChecks
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
