const models = require('@pubsweet/models')

const useCases = require('./use-cases')

module.exports = {
  Query: {
    async getManuscriptPeerReviewChecks(_, { manuscriptId }) {
      return useCases.getManuscriptPeerReviewChecksUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
}
