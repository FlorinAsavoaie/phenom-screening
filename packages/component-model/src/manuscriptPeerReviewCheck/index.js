const fs = require('fs')
const path = require('path')

module.exports = {
  model: require('./manuscriptPeerReviewCheck'),
  useCases: require('./use-cases'),
  resolvers: require('./resolvers'),
  factory: require('./manuscriptPeerReviewCheckFactory'),
  typeDefs: fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
}
