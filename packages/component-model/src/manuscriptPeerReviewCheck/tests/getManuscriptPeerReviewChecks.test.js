const useCases = require('../use-cases')

const models = require('@pubsweet/models')
const Chance = require('chance')

const chance = new Chance()
const { Manuscript, ManuscriptPeerReviewCheck } = models

describe('getManuscriptPeerReviewChecks', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should get peer review checks for a specific manuscript', async () => {
    const manuscriptId = chance.guid()
    const manuscripts = [
      { id: manuscriptId, name: 'reviewersIdentityConfirmed' },
    ]

    const manuscript = {
      id: manuscriptId,
      peerReviewCheckConfig: {
        config: { reviewersIdentityConfirmed: { options: [], order: 1 } },
      },
    }

    ManuscriptPeerReviewCheck.findBy = jest.fn().mockReturnValue(manuscripts)
    Manuscript.find = jest.fn().mockReturnValue(manuscript)

    const result = await useCases.getManuscriptPeerReviewChecksUseCase
      .initialize({
        ManuscriptPeerReviewCheck,
        Manuscript,
      })
      .execute(manuscriptId)

    expect(result).toStrictEqual([
      {
        id: manuscriptId,
        name: 'reviewersIdentityConfirmed',
        config: { options: [], order: 1 },
      },
    ])

    expect(ManuscriptPeerReviewCheck.findBy).toHaveBeenCalledTimes(1)
    expect(ManuscriptPeerReviewCheck.findBy).toHaveBeenCalledWith({
      queryObject: { manuscriptId },
    })

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      'peerReviewCheckConfig',
    )
  })
})
