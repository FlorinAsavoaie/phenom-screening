const HindawiBaseModel = require('../../hindawiBaseModel')

class ManuscriptPeerReviewCheck extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript_peer_review_checks'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        manuscriptId: {
          type: 'string',
          format: 'uuid',
        },
        name: {
          type: 'string',
          default: null,
        },
        selectedOption: {
          type: ['string', 'null'],
          default: null,
        },
        observation: {
          type: 'string',
          default: null,
        },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('../manuscript').model,
        join: {
          from: `${this.tableName}.manuscriptId`,
          to: 'manuscript.id',
        },
      },
    }
  }

  static get Option() {
    return Option
  }

  static async deleteByManuscriptId(manuscriptId) {
    return this.query().where('manuscriptId', manuscriptId).delete()
  }
}

module.exports = ManuscriptPeerReviewCheck
