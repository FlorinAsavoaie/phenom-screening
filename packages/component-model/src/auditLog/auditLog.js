const HindawiBaseModel = require('../../hindawiBaseModel')

const User = require('../user/user')

class AuditLog extends HindawiBaseModel {
  static get tableName() {
    return 'audit_log'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        userId: { type: 'string', format: 'uuid' },
        manuscriptId: { type: 'string', format: 'uuid' },
        submissionId: { type: 'string', format: 'uuid' },
        action: { enum: Object.values(this.Actions) },
        userRole: { enum: Object.values(User.Roles) },
        comment: { type: ['string', null] },
      },
    }
  }

  static get Actions() {
    return {
      SENT_TO_TEAM_LEAD: 'sendToTeamLead',
      RETURN_TO_CHECKER: 'returnToChecker',
      PAUSE_MANUSCRIPT: 'pauseManuscript',
      UNPAUSE_MANUSCRIPT: 'unpauseManuscript',
    }
  }

  static async getAuditLogsForSubmissionAndRole(submissionId, userRole) {
    let queryObject = { 'audit_log.submission_id': submissionId }
    if (userRole === User.Roles.SCREENER)
      queryObject = { ...queryObject, 'manuscript.flow_type': 'screening' }

    return AuditLog.query()
      .leftJoin('manuscript', 'manuscript.id', '=', 'audit_log.manuscript_id')
      .where(queryObject)
      .orderBy('created', 'desc')
  }
}

module.exports = AuditLog
