const models = require('@pubsweet/models')
const useCases = require('./use-cases')

const resolvers = {
  Query: {
    async getAuditLogsForSubmission(
      _,
      { submissionId, manuscriptId },
      { user: userId },
    ) {
      return useCases.getAuditLogsForSubmissionUseCase
        .initialize(models)
        .execute({ submissionId, manuscriptId, userId })
    },
  },
  ScreeningAuditLog: {
    async user(auditLog, query, ctx) {
      return useCases.getAuditLogUserUseCase
        .initialize(models)
        .execute({ userId: auditLog.userId })
    },
  },
}

module.exports = resolvers
