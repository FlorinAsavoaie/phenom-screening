function initialize({ User }) {
  return {
    execute,
  }

  async function execute({ userId }) {
    return User.find(userId)
  }
}

module.exports = {
  initialize,
}
