const getAuditLogsForSubmissionUseCase = require('./getAuditLogsForSubmission')
const getAuditLogUserUseCase = require('./getAuditLogUser')

module.exports = {
  getAuditLogUserUseCase,
  getAuditLogsForSubmissionUseCase,
}
