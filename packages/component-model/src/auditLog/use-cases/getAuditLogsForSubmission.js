function initialize({ AuditLog, User }) {
  return {
    execute,
  }

  async function execute({ submissionId, manuscriptId, userId }) {
    const userRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId,
    })

    return AuditLog.getAuditLogsForSubmissionAndRole(submissionId, userRole)
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
