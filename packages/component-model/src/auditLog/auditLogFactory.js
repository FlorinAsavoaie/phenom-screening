const AuditLog = require('./auditLog')

module.exports = {
  createAuditLog({
    userId,
    manuscriptId,
    action,
    userRole,
    comment,
    submissionId,
  }) {
    return new AuditLog({
      userId,
      manuscriptId,
      submissionId,
      action,
      userRole,
      comment,
    })
  },
}
