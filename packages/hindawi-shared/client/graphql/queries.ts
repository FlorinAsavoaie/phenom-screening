import { gql } from '@apollo/client'

export const getManuscriptByCustomId = gql`
  query getManuscriptByCustomId($manuscriptCustomId: ID!) {
    manuscript: getScreeningManuscriptByCustomId(
      manuscriptCustomId: $manuscriptCustomId
    ) {
      title
      submissionId
      phenomId
      status
    }
  }
`
