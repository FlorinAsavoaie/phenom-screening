import { useState, useEffect } from 'react'

interface DebounceOptions<T> {
  value: T
  delay: number
}

export function useDebounce<T>({ value, delay }: DebounceOptions<T>): T {
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)

    return (): void => {
      clearTimeout(handler)
    }
  }, [value])

  return debouncedValue
}
