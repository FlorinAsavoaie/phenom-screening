import { useQuery, ApolloError } from '@apollo/client'
import { get } from 'lodash'
import { getManuscriptByCustomId } from '../queries'

type ManuscriptByCustomIdHookReturn = {
  manuscriptData: {
    title: string
    submissionId: string
    phenomId: string
    status: string
  }
  loading: boolean
  error: ApolloError | undefined
}

export function useManuscriptByCustomId(
  manuscriptCustomId: string,
): ManuscriptByCustomIdHookReturn {
  const { data, loading, error } = useQuery(getManuscriptByCustomId, {
    variables: { manuscriptCustomId },
  })

  const manuscriptData = get(data, 'manuscript', null)

  return { manuscriptData, loading, error }
}
