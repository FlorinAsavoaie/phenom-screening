import { ReactElement } from 'react'
import { Tabs as BaseTabs, Text, ActionLink, Row } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import { SpaceProps } from 'styled-system'
import styled, { css } from 'styled-components'
import { Icon } from '../icon/'

const removeFragments: any = (children: any) => {
  if (children.type === Symbol.for('react.fragment')) {
    return children.props.children
  }
  return children
}
interface IconTab {
  hasIcon: boolean
  name: string
  color: string
}
interface Tab {
  label: string
  icon?: IconTab
  disabled?: boolean
}

interface Icon extends SpaceProps {
  name: string
}

export interface RightTab {
  label: string
  icon?: Icon
  handleClick: () => any
  component: ReactElement
  showComponent: boolean
}

interface TabsProps {
  tabs: Tab[]
  children?: any
  rightTabs?: any
  selectedTab: number
  handleClick: (value: number) => void
}

export const Tabs: React.FC<TabsProps> = ({
  tabs,
  children,
  rightTabs,
  selectedTab,
  handleClick,
}) => {
  const parsedChildren = removeFragments(children).filter(
    (child: any) => child !== false,
  )

  return (
    <BaseTabs selectedTab={selectedTab}>
      {() => (
        <Root>
          <TabsHeader>
            <Container>
              {tabs.map((tab, index) => (
                <Tab
                  disabled={tab.disabled ? tab.disabled : false}
                  key={tab.label}
                  onClick={() => handleClick(index)}
                  selected={index === selectedTab}
                >
                  <TabText pl={4} py={1}>
                    {tab.label}
                  </TabText>
                  {tab.icon && tab.icon.hasIcon && (
                    <Icon
                      color={tab.icon.color}
                      name={tab.icon.name}
                      size={4}
                    />
                  )}
                </Tab>
              ))}
            </Container>
            {rightTabs && (
              <RightContainer>
                {rightTabs.map((tab: any) => (
                  <ActionLink
                    key={tab.label}
                    onClick={() => {
                      tab.handleClick()
                    }}
                  >
                    <Row>
                      <Icon size={4} {...tab.icon} />
                      <Text fontWeight={700} ml={1}>
                        {tab.label}
                      </Text>
                    </Row>
                  </ActionLink>
                ))}
              </RightContainer>
            )}
          </TabsHeader>

          {rightTabs &&
            rightTabs.map(
              (tab: any) =>
                tab.showComponent && <div key={tab.label}>{tab.component}</div>,
            )}

          {Array.isArray(parsedChildren)
            ? parsedChildren[selectedTab]
            : parsedChildren}
        </Root>
      )}
    </BaseTabs>
  )
}

// #region styles
const TabsHeader = styled.nav`
  background-color: ${th('white')};
  box-shadow: inset 0 -1px 0 0 ${th('text.furnitureColor')};
  display: flex;
  min-height: calc(${th('gridUnit')} * 10);
  height: calc(${th('gridUnit')} * 10);
  justify-content: space-between;
  font-size: ${th('text.fontSizeBase')};
  font-family: ${th('defaultFont')};
  padding: 0 calc(${th('gridUnit')} * 4) 0 0;
`

const Container = styled.div`
  display: flex;
  color: ${th('actionSecondaryColor')};
`
const RightContainer = styled.div`
  display: flex;
  color: ${th('actionSecondaryColor')};
  div:not(:last-child) {
    margin-right: calc(${th('gridUnit')} * 6);
  }
`

const activeTab = (props: any) => {
  if (props.selected) {
    return css`
      color: ${th('mainTextColor')};
      border-bottom: 3px solid ${th('textSecondaryColor')};
    `
  }
}
const TabText = styled(Text)`
  color: inherit;
`

interface TabProps {
  disabled: boolean
  selected: boolean
}

const Tab = styled.button<TabProps>`
  background: none;
  border: none;
  cursor: pointer;
  font-weight: bold;
  padding: 0;
  outline: none;
  border-bottom: 3px solid transparent;
  text-decoration: none;
  padding-right: 16px;
  display: flex;
  align-items: center;
  color: ${th('actionSecondaryColor')};

  ${TabText} {
    border-left: 1px solid ${th('highlightColor')};
  }
  &:first-child > ${TabText} {
    border-left: 0px;
  }

  ${activeTab}
`

const Root = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  overflow: hidden;
  width: 100%;
`
// #endregion
