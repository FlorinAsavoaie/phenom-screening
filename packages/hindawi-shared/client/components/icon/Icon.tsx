import styled, { css } from 'styled-components'
import { space, position, SpaceProps } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import Icons from './icons'

interface IconProps extends SpaceProps, React.SVGProps<SVGSVGElement> {
  name: string
  color?: string
  size?: number
}

export const Icon = ({ name, ...rest }: IconProps): any => {
  const path = Icons[name]

  return (
    <Root fill="none" xmlns="http://www.w3.org/2000/svg" {...rest}>
      {Array.isArray(path) ? (
        path.map((p) => (
          <path clipRule="evenodd" d={p} fillRule="evenodd" key={p} />
        ))
      ) : (
        <path clipRule="evenodd" d={path} fillRule="evenodd" />
      )}
    </Root>
  )
}

interface IconSizeProps {
  size: number
}

Icon.defaultProps = {
  color: null,
  size: 3,
}

const iconSize = ({ size }: IconSizeProps): any => css`
  width: calc(${th('gridUnit')} * ${size});
  height: calc(${th('gridUnit')} * ${size});
`

const color = (props: { color?: string }): any => {
  if (props.color) {
    return css`
      fill: ${th(props.color)};
    `
  }

  return css`
    fill: currentColor;
  `
}

Icon.defaultProps = {
  color: null,
  size: 3,
}

interface RootProps {
  color?: string
  size: number
}

const Root: any = styled.svg<RootProps>`
  ${iconSize}
  ${space};
  ${position};
  ${color}
`
