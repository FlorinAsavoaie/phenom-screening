import { ActionLink, Icon } from '@hindawi/ui'
import { useDownloadAllFiles } from './useDownloadAllFiles'

interface ManuscriptFilesDownloaderProps {
  manuscriptId: string
}

export const ManuscriptFilesDownloader: React.FC<ManuscriptFilesDownloaderProps> =
  ({ manuscriptId }) => {
    const { downloadZip } = useDownloadAllFiles(manuscriptId)
    return (
      <ActionLink
        alignItems="center"
        fontWeight={700}
        mr={2}
        onClick={(event): void => {
          downloadZip()
          event.stopPropagation()
        }}
      >
        Download All
        <Icon fontSize="12px" icon="downloadZip" ml={1} />
      </ActionLink>
    )
  }
