import { useFetching } from '@hindawi/ui'

import {
  createAnchorElement,
  removeAnchorElement,
} from 'component-quality-checks-process/client/components/manuscript-files/fileUtils'

interface DownloadAllFilesHookResult {
  downloadZip: () => void
  isFetching: boolean
  fetchingError: string
}

export const useDownloadAllFiles = (
  manuscriptId: string,
): DownloadAllFilesHookResult => {
  const { isFetching, setFetching, fetchingError, setError } = useFetching()

  const getURL = `${window.location.origin}/files/zip/${manuscriptId}`
  const token = localStorage.getItem('token')
  const setErr = ({ name }: Error): void => setError(name)

  const downloadZip = (): void => {
    const file = { fileName: `${manuscriptId}_files.zip` }

    setFetching(true)
    const xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function onXhrStateChange(): void {
      if (this.readyState === 4) {
        setFetching(false)
        if (this.status >= 200 && this.status < 300) {
          const f = new File([this.response], file.fileName)

          const { a, url } = createAnchorElement(f, file.fileName)
          a.click()
          removeAnchorElement(a, url)
        } else if (this.status >= 400) {
          new Response(this.response)
            .text()
            .then(JSON.parse)
            .then(setErr)
            .catch(console.error)
        }
      }
    }
    xhr.open('GET', getURL)
    xhr.responseType = 'blob'
    xhr.setRequestHeader('Authorization', `Bearer ${token}`)
    xhr.send()
  }
  return { downloadZip, isFetching, fetchingError }
}

export default useDownloadAllFiles
