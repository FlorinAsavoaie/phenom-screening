import { Icon } from '@hindawi/ui'
import { TextField as BaseTextField } from '@pubsweet/ui'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

interface SearchFieldProps {
  value: string
  onChange: (event: any) => any
  onSearch: (value: string) => any
  placeholder: string
  disabled?: boolean
  inline?: boolean
  rest?: any
  width?: number
  mb?: number
}

export const SearchField: React.FC<SearchFieldProps> = ({
  value,
  onChange,
  onSearch,
  placeholder,
  disabled,
  ...rest
}) => {
  const handleOnKeyPress: any = (e: any) => {
    if (e.key === 'Enter') {
      onSearch(e.target.value)
    }
  }

  return (
    <Root>
      <TextField
        data-test-id="search"
        disabled={disabled}
        onChange={onChange}
        onKeyPress={handleOnKeyPress}
        placeholder={placeholder}
        value={value}
        {...rest}
      />
      <BaseIcon
        fontSize="16px"
        icon="search"
        onClick={onSearch ? () => onSearch(value) : null}
      />
    </Root>
  )
}

// #region styles
const Root = styled.div`
  display: flex;
`
const TextField = styled(BaseTextField)`
  input {
    text-overflow: ellipsis;
    padding: 0 calc(${th('gridUnit')} * 7) 0 calc(${th('gridUnit')} * 2);
  }

  ${({ width }: any): any =>
    css`
      width: calc(${th('gridUnit')} * ${width});
    `}

  ${space};
`
const BaseIcon = styled(Icon)`
  color: ${th('actionSecondaryColor')};
  position: relative;
  right: calc(${th('gridUnit')} * 6);
  top: 7px;
`
// #endregion
