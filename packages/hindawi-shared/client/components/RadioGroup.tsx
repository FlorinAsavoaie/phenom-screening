import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Radio as BaseRadio } from '@pubsweet/ui/src'
import { get } from 'lodash'

export interface RadioGroupOption {
  value: string
  label: string
  checked?: boolean
  disabled?: boolean
}

interface RadioGroupProps {
  options: RadioGroupOption[]
  onChange: (value: any) => void
  name?: string
  required?: boolean
  disabled?: boolean
  marginBetween?: number
}

const RadioGroup: React.FC<RadioGroupProps> = ({
  options,
  onChange,
  name,
  required,
  disabled,
  marginBetween,
}) => (
  <Root>
    {options.map((option) => (
      <Radio
        checked={option.checked}
        disabled={disabled || option.disabled}
        inline
        key={option.value}
        label={option.label}
        marginBetween={marginBetween}
        name={name}
        onChange={onChange}
        required={required}
        value={option.value}
      />
    ))}
  </Root>
)

export { RadioGroup }

// #region styles
const Root = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
`
const Radio = styled(BaseRadio)`
  font-size: ${th('fontSizeBase')};
  margin-right: calc(
    ${th('gridUnit')} * ${(props) => get(props, 'marginBetween', 6)}
  );

  &:last-child {
    margin-right: 0px;
  }
  & span {
    margin: 0px;
  }
`
// #endregion
