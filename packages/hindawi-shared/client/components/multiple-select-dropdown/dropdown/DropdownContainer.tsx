import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DropdownItem } from './DropdownItem'
import { Option } from '../MultipleSelectDropdown'

interface DropdownContainerProps {
  options: Option[]
  selectedItems: Option[]
  visible: boolean
  onItemAdd: (addedItemId: string) => void
  onItemRemove: (removedItemId: string) => void
}

const DropdownContainer: React.FC<DropdownContainerProps> = ({
  options,
  selectedItems,
  visible,
  onItemAdd,
  onItemRemove,
}) => {
  if (!visible) {
    return null
  }

  function isItemChecked(item: Option): boolean {
    const found = selectedItems.find(
      (selectedItem) => selectedItem.id === item.id,
    )
    return !!found
  }

  function propagateItemChange(item: Option): void {
    if (!isItemChecked(item)) {
      onItemAdd(item.id)
    } else {
      onItemRemove(item.id)
    }
  }

  function handleOnChange(item: Option): void {
    propagateItemChange(item)
  }

  return (
    <Root>
      {options.map((item) => (
        <DropdownItem
          checked={isItemChecked(item)}
          key={item.id}
          label={item.label}
          onChange={(): void => handleOnChange(item)}
          value={item.id}
        />
      ))}
    </Root>
  )
}

export { DropdownContainer }

const Root = styled.div`
  box-shadow: 0 2px 6px ${th('disabledColor')};
  background-color: ${th('white')};
  max-height: calc(${th('gridUnit')} * 72);
  position: absolute;
  overflow-y: scroll;
  z-index: 1;
  width: 100%;
  top: 102%;
  bottom: auto;
  border-radius: ${th('borderRadius')};
`
