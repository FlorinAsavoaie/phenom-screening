import { ChangeEventHandler } from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Checkbox } from 'hindawi-shared/client/components'

export interface DropdownItemProps {
  label: string
  checked?: boolean
  onChange?: ChangeEventHandler<HTMLInputElement>
  value: string
}

const DropdownItem: React.FC<DropdownItemProps> = ({
  label,
  checked,
  onChange,
  value,
}) => (
  <Root>
    <Checkbox
      checked={checked}
      label={label}
      onChange={onChange}
      value={value}
    />
  </Root>
)

const Root = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: calc(${th('gridUnit')} * 8);
  background-color: ${th('white')};

  font-family: ${th('defaultFont')};
  font-style: normal;
  font-weight: normal;
  font-size: ${th('mainTextSize')};
  line-height: ${th('mainTextLineHeight')};

  padding-left: calc(${th('gridUnit')});
  padding-right: calc(${th('gridUnit')});

  &:hover {
    background-color: ${th('backgroundColor')};
  }
`

export { DropdownItem }
