import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon as BaseIcon } from '@hindawi/ui'

import { DropdownItemBadge } from './DropdownItemBadge'
import { Option } from '../MultipleSelectDropdown'

interface DropdownInputProps {
  isDropdownVisible: boolean
  onOpenDropdownClick: () => void
  onRemoveClick?: () => void
  placeholder?: string
  selectedItems: Option[]
  onItemRemove: (itemId: string) => void
}

const DropdownInput: React.FC<DropdownInputProps> = ({
  isDropdownVisible,
  onOpenDropdownClick,
  onRemoveClick,
  placeholder = '',
  selectedItems,
  onItemRemove,
}) => (
  <Root active={isDropdownVisible} onClick={onOpenDropdownClick}>
    <SelectionArea>
      {selectedItems.length > 0 ? (
        selectedItems.map((selectedItem) => (
          <DropdownItemBadge
            key={selectedItem.id}
            label={selectedItem.label}
            onClick={(event): void => {
              event.stopPropagation()
              onItemRemove(selectedItem.id)
            }}
          />
        ))
      ) : (
        <Placeholder>{placeholder}</Placeholder>
      )}
    </SelectionArea>
    <ControlArea>
      {isDropdownVisible && selectedItems.length > 1 && (
        <Icon icon="remove" mr={4} onClick={onRemoveClick} />
      )}
      <Icon icon={isDropdownVisible ? 'caretUp' : 'caretDown'} />
    </ControlArea>
  </Root>
)

export { DropdownInput }

const Icon = styled(BaseIcon)`
  font-size: ${th('h3Size')};
  height: max-content;
`

const SelectionArea = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex: 1;
  align-items: center;
  cursor: pointer;
`

const ControlArea = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 6px;
`

const Placeholder = styled.div`
  font-family: ${th('defaultFont')};
  font-style: italic;
  font-weight: normal;
  font-size: ${th('mainTextSize')};
  line-height: ${th('mainTextLineHeight')};
  color: ${th('grey60')};
`

const Root = styled.div<{ active: boolean }>`
  background: ${th('white')};

  border: 1px solid
    ${(props): string =>
      props.active ? th('contrastGrayColor') : th('colorBorder')};

  border-radius: ${th('borderRadius')};
  box-sizing: border-box;
  min-height: calc(${th('gridUnit')} * 8);

  display: flex;

  padding-left: calc(${th('gridUnit')});
  padding-right: calc(${th('gridUnit')});
  padding-top: 1px;
  padding-bottom: 1px;
`
