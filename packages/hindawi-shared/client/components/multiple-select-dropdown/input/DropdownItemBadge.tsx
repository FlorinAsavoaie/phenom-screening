import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon as BaseIcon } from 'hindawi-shared/client/components'

interface DropdownItemBadgeProps {
  label: string
  onClick: (e: React.SyntheticEvent) => void
}
const DropdownItemBadge: React.FC<DropdownItemBadgeProps> = ({
  label,
  onClick,
}) => (
  <Root>
    <Label>{label}</Label>
    <Icon
      color="contrastGrayColor"
      ml="2"
      name="removeFill"
      onClick={onClick}
      size={3}
    />
  </Root>
)
export { DropdownItemBadge }

const Root = styled.div`
  border: 1px solid ${th('actionSecondaryColor')};
  border-radius: 3px;
  box-sizing: border-box;
  height: calc(${th('gridUnit')} * 6);

  display: flex;
  align-items: center;

  padding: calc(${th('gridUnit')});
  margin-right: calc(${th('gridUnit')});
  margin-top: 2px;
  margin-bottom: 2px;

  &:last-child {
    margin-right: 0px;
  }
`

const Label = styled.div`
  font-family: ${th('defaultFont')};
  font-style: normal;
  font-weight: bold;
  font-size: ${th('smallLabelSize')};
  line-height: ${th('smallLabelLineHeight')};

  text-transform: uppercase;
  align-self: center;

  color: ${th('actionSecondaryColor')};
`

const Icon = styled(BaseIcon)`
  cursor: pointer;
`
