import { useState } from 'react'
import { Option } from './MultipleSelectDropdown'

type SelectedOptionsHookResult = [
  Option[],
  (selectedOptionId: string) => void,
  (unselectedOptionId: string) => void,
  () => void,
]

export function useSelectedOptions(
  options: Option[],
): SelectedOptionsHookResult {
  const defaultOptions = options.filter((opt) => opt.checked === true)

  const [selectedOptions, setSelectedOptions] =
    useState<Option[]>(defaultOptions)

  function selectOption(selectedOptionId: string): void {
    const selectedOption = options.find(
      (option) => option.id === selectedOptionId,
    )

    if (selectedOption) {
      setSelectedOptions([...selectedOptions, selectedOption])
    }
  }

  function unselectOption(unselectedOptionId: string): void {
    setSelectedOptions(
      selectedOptions.filter((r) => r.id !== unselectedOptionId),
    )
  }

  function unselectAllOptions(): void {
    setSelectedOptions([])
  }

  return [selectedOptions, selectOption, unselectOption, unselectAllOptions]
}
