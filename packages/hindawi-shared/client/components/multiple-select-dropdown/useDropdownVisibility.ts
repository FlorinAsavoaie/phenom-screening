import { useState, useEffect, useRef } from 'react'

interface DropdownVisiblityHoookResult {
  ref: React.RefObject<HTMLDivElement>
  isVisible: boolean
  toggleVisibility: () => void
}

export function useDropdownVisibility(
  initialIsVisible: boolean,
): DropdownVisiblityHoookResult {
  const [isVisible, setIsVisible] = useState(initialIsVisible)
  const ref = useRef<HTMLDivElement>(null)

  const handleOutsideClick = (event: MouseEvent): void => {
    const currentElement = ref.current
    const targetElement = event.target as Node

    if (currentElement && !currentElement.contains(targetElement)) {
      setIsVisible(false)
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleOutsideClick, true)

    return (): void => {
      document.removeEventListener('click', handleOutsideClick, true)
    }
  }, [])

  function toggleVisibility(): void {
    setIsVisible(!isVisible)
  }

  return {
    ref,
    isVisible,
    toggleVisibility,
  }
}
