import { useEffect } from 'react'
import styled from 'styled-components'
import { Label } from '@hindawi/ui'

import { DropdownContainer } from './dropdown'
import { DropdownInput } from './input'
import { useDropdownVisibility } from './useDropdownVisibility'
import { useSelectedOptions } from './useSelectedOptions'

interface Option {
  id: string
  label: string
  checked?: boolean
}

interface MultipleSelectDropdownProps {
  onChange: (selectedItems: Option[]) => void
  options: Option[]
  label?: string
  placeholder?: string
  required?: boolean
}

const MultipleSelectDropdown: React.FC<MultipleSelectDropdownProps> = ({
  options,
  onChange,
  label,
  placeholder,
  required,
}) => {
  const { ref, isVisible, toggleVisibility } = useDropdownVisibility(false)

  const [selectedItems, selectItem, unselectItem, unselectAllItems] =
    useSelectedOptions(options)

  useEffect(() => {
    onChange(selectedItems)
  }, [selectedItems])

  return (
    <>
      {label && (
        <Label fontSize={14} fontWeight={700} mb="2px" required={required}>
          {label}
        </Label>
      )}
      <Root data-test-id="reasons-dropdown" ref={ref}>
        <DropdownInput
          isDropdownVisible={isVisible}
          onItemRemove={unselectItem}
          onOpenDropdownClick={toggleVisibility}
          onRemoveClick={unselectAllItems}
          placeholder={placeholder}
          selectedItems={selectedItems}
        />
        <DropdownContainer
          onItemAdd={selectItem}
          onItemRemove={unselectItem}
          options={options}
          selectedItems={selectedItems}
          visible={isVisible}
        />
      </Root>
    </>
  )
}
const Root = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  position: relative;
`

export { MultipleSelectDropdown, Option }
