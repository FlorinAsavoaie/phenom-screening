import { ChangeEventHandler } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

interface CheckboxProps {
  value: string
  label: string
  checked?: boolean
  onChange?: ChangeEventHandler<HTMLInputElement>
}
const Checkbox: React.FC<CheckboxProps> = ({
  value,
  label,
  checked,
  onChange,
}) => (
  <Root data-test-id={value}>
    <Input
      checked={checked}
      onChange={onChange}
      type="checkbox"
      value={value}
    />
    <Label>{label}</Label>
  </Root>
)

export { Checkbox }

const Label = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;

  font-family: Nunito;
  font-style: normal;
  font-weight: normal;
  font-size: ${th('mainTextSize')};
  line-height: ${th('mainTextLineHeight')};
  white-space: nowrap;
  color: #4f4f4f;
  &::before {
    content: ' ';
    background: transparent;
    border: 2px solid ${th('checkbox.borderColor')};
    border-radius: ${th('borderRadius')};
    box-sizing: border-box;
    display: inline-block;
    margin-right: ${th('gridUnit')};
    vertical-align: text-bottom;

    height: calc(${th('gridUnit')} * 4);
    width: calc(${th('gridUnit')} * 4);
  }
`
const Input = styled.input`
  margin-right: ${th('gridUnit')};
  position: absolute;
  opacity: 0;
  z-index: -1;
`

const checkIcon = `
  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd"
    clip-rule="evenodd"
    d="M1.77778 0H14.2222C15.1999 0 16 0.800142 16 1.77778V14.2222C16 15.1999 15.1999 16 14.2222 16H1.77778C0.800142 16 0 15.1999 0 14.2222V1.77778C0 0.800142 0.800142 0 1.77778 0ZM13.6261 3.92015C13.3164 3.6447 12.8418 3.67217 12.5661 3.98152L6.37463 10.1258L3.75133 7.51175C3.44228 7.23559 2.96765 7.262 2.69121 7.57072C2.41477 7.87945 2.4412 8.35359 2.75025 8.62974L5.93439 11.745C6.24393 12.0216 6.71945 11.9946 6.99564 11.6847L13.6875 4.97904C13.9632 4.66969 13.9357 4.19561 13.6261 3.92015Z"
    fill="#81BA40"/>
  </svg>
`

const Root = styled.label`
  display: flex;
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};
  width: 100%;
  height: 100%;

  input:checked + span::before {
    border-color: ${th('actionPrimaryColor')};
    background: url('data:image/svg+xml;utf8,${encodeURIComponent(checkIcon)}')
        center no-repeat,
      ${th('white')};
    background-size: contain;
  }
`
