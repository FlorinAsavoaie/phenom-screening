import { Row, Text } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Icon } from 'hindawi-shared/client/components'
import { FormikErrors } from 'formik'

interface ErrorFieldProps {
  errorText?:
    | string
    | string[]
    | FormikErrors<any>
    | FormikErrors<any>[]
    | undefined
  visible: boolean
}
const ErrorField: React.FC<ErrorFieldProps> = ({ errorText, visible }) => (
  <Root>
    {visible && (
      <Row justify="flex-start">
        <Icon color="warningColor" mr={1} name="warning" size={3} />
        <ErrorMessage error>{errorText}</ErrorMessage>
      </Row>
    )}
  </Root>
)

export { ErrorField }

const Root = styled(Row)`
  height: calc(${th('gridUnit')} * 6);
`

const ErrorMessage = styled(Text)`
  font-size: ${th('bannerTextSize')};
`
