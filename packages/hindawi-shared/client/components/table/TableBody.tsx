import { ApolloError } from '@apollo/client'
import { darken, th } from '@pubsweet/ui-toolkit'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { Loader, Row, Text as BaseText, Item } from '@hindawi/ui'
import styled, { css } from 'styled-components'
import { ColumnDefinition } from './Table'

interface TableBodyProps {
  columnDefinitions: [ColumnDefinition]
  error?: ApolloError
  items: any[]
  loading: boolean
  noItemsMessage: string
}

const TableBody: React.FC<TableBodyProps> = ({
  columnDefinitions,
  error,
  items,
  loading,
  noItemsMessage,
}) => {
  if (loading) {
    return (
      <LoaderRow alignItems="flex-start" justify="center">
        <Loader mt={2} />
      </LoaderRow>
    )
  }

  if (error) {
    return (
      <Row justify="center" mt={4}>
        <Text error>{parseGQLError(error)}</Text>
      </Row>
    )
  }

  if (!items.length) {
    return (
      <StyledRow>
        <StyledText fontWeight={700}>{noItemsMessage}</StyledText>
      </StyledRow>
    )
  }

  return (
    <>
      {items.map((item, index) => (
        <TableRow
          alignItems="flex-start"
          data-test-id="table-row"
          key={item.id || item.email}
          px={4}
        >
          {columnDefinitions.map((columnDefinition) => (
            <TableRowColumn
              flex={columnDefinition.flex}
              key={columnDefinition.headerName}
            >
              <ColumnDefinitionComponent
                columnDefinition={columnDefinition}
                index={index}
                item={item}
              />
            </TableRowColumn>
          ))}
        </TableRow>
      ))}
    </>
  )
}

function ColumnDefinitionComponent({
  columnDefinition,
  item,
  index,
}: any): any {
  if (columnDefinition.component) {
    return columnDefinition.component({ item, index })
  }

  if (columnDefinition.labelFunction) {
    return (
      <Text title={columnDefinition.labelFunction(item)}>
        {columnDefinition.labelFunction(item)}
      </Text>
    )
  }

  return <></>
}

export { TableBody }

// #region styles
const StyledText = styled(BaseText)`
  font-size: 20px;
  color: ${th('labelLineColor')};
`

const StyledRow = styled(Row)`
  min-height: calc(${th('gridUnit')} * 22);
  flex: 1;
  border-radius: 0px 0px 4px 4px;
  background-color: ${th('backgroundColor')};
`

const withEllipsis = css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const TableRow = styled(Row)`
  border-bottom: 1px solid ${th('colorBorder')};
  min-height: calc(${th('gridUnit')} * 9);

  &:hover {
    background-color: ${darken('white', 10)};
    .actionDropdown {
      visibility: visible;
    }
  }
`

const TableRowColumn = styled(Item)<{ children: any }>`
  overflow: ${({ children }): any =>
    children.props.columnDefinition?.disableEllipsis ? 'visible' : 'hidden'};
  justify-content: ${({ children }): any =>
    children.props.columnDefinition?.disableEllipsis ? 'flex-end' : ''};
  min-height: calc(${th('gridUnit')} * 6);
  margin-bottom: calc(${th('gridUnit')} * 1);
  margin-top: calc(${th('gridUnit')} * 1);
  align-items: center;
  white-space: nowrap;
  text-overflow: ellipsis;
`

const Text = styled(BaseText)`
  ${withEllipsis}
`

const LoaderRow = styled(Row)`
  min-height: calc(${th('gridUnit')} * 22);
  &:hover {
    background-color: ${th('white')};
  }
`
// #endregion
