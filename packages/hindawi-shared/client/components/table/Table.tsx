import { ApolloError } from '@apollo/client'
import { get, has } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { TableBody } from './TableBody'
import { TableHeader } from './TableHeader'

const ConditionalWrap = ({ condition, wrap, children }: any) =>
  condition ? wrap(children) : children

export interface ColumnDefinition {
  headerName: string
  labelFunction?: (item: any) => string
  component?: any
  flex?: number
}

interface TableProps {
  columnDefinitions: any
  enableScrolling?: boolean
  error?: ApolloError
  items: any
  loading: boolean
  maxHeight?: number
  noItemsMessage: string
}

const Table: React.FC<TableProps> = ({
  columnDefinitions,
  enableScrolling = false,
  error,
  items,
  loading,
  maxHeight,
  noItemsMessage,
}) => (
  <TableRoot enableScrolling={enableScrolling} maxHeight={maxHeight}>
    <TableHeader columnDefinitions={columnDefinitions} />
    <ConditionalWrap
      condition={enableScrolling}
      wrap={(children: any) => <ScrollContainer>{children}</ScrollContainer>}
    >
      <TableBody
        columnDefinitions={columnDefinitions}
        error={error}
        items={items}
        loading={loading}
        noItemsMessage={noItemsMessage}
      />
    </ConditionalWrap>
  </TableRoot>
)

export { Table }

// #region styles

const heightHelper = (props: { maxHeight?: number }): any =>
  has(props, 'maxHeight')
    ? css`
        max-height: calc(${th('gridUnit')} * ${get(props, 'maxHeight')});
      `
    : css`
        max-height: inherit;
      `

const TableRoot = styled.div<{ enableScrolling: boolean; maxHeight?: number }>`
  background-color: ${th('white')};
  border-radius: ${th('borderRadius')};
  border: 1px solid ${th('colorBorder')};
  display: flex;
  flex-direction: column;
  flex: 1;

  ${heightHelper}
`

const ScrollContainer = styled.div`
  overflow: auto;
`
// #endregion
