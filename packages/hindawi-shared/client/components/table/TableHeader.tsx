import { Item, Row, Label } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { ColumnDefinition } from './Table'

interface TableHeaderProps {
  columnDefinitions: [ColumnDefinition]
}

const TableHeader: React.FC<TableHeaderProps> = ({ columnDefinitions }) => (
  <Root pl={4} px={4}>
    {columnDefinitions.map((columnDefinition) => (
      <Item flex={columnDefinition.flex} key={columnDefinition.headerName}>
        <Label>{columnDefinition.headerName}</Label>
      </Item>
    ))}
  </Root>
)

export { TableHeader }

// #region styles
export const Root = styled(Row)`
  border-bottom: 1px solid ${th('colorBorder')};
  min-height: calc(${th('gridUnit')} * 9);
`
// #endregion
