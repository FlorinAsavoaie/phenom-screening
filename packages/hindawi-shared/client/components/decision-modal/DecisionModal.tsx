import {
  Row,
  Text,
  FormModal,
  Item,
  ValidatedFormField,
  Textarea,
  Label,
} from '@hindawi/ui'
import styled from 'styled-components'

interface DecisionModalProps {
  onConfirm: (
    _: unknown,
    {
      setFetching,
      setError,
    }: {
      setFetching: (isFetching: boolean) => void
      setError: (error: unknown) => void
    },
  ) => void
  additionalCommentTitle?: string
  confirmText?: string
  confirmButtonText: string
  cancelButtonText: string
  modalTitle: string
  hideModal: () => void
  additionalContent?: () => JSX.Element
  hasTextarea?: boolean
  additionalComment?: string
  validate?: ((value: any) => string | undefined)[]
  placeholder?: string
}

const DecisionModal: React.FC<DecisionModalProps> = ({
  onConfirm,
  additionalCommentTitle,
  confirmText,
  confirmButtonText,
  cancelButtonText,
  modalTitle,
  additionalContent,
  hasTextarea = true,
  additionalComment,
  validate,
  hideModal,
  placeholder,
}) => (
  <FormModal
    cancelText={cancelButtonText}
    confirmText={confirmButtonText}
    content={() => (
      <>
        {additionalContent && additionalContent()}
        {(hasTextarea || additionalComment) && (
          <Row mt={8}>
            <Item alignItems={hasTextarea ? 'flex-start' : 'center'} vertical>
              <Label
                fontSize={14}
                fontWeight={700}
                mb={1}
                required={!!validate}
              >
                {additionalCommentTitle}
              </Label>
              {hasTextarea ? (
                <ValidatedFormField
                  component={Textarea}
                  minHeight={35}
                  name="additionalComments"
                  placeholder={placeholder}
                  validate={validate}
                />
              ) : (
                <Text>{additionalComment}</Text>
              )}
            </Item>
          </Row>
        )}
        <Row mt={3}>
          <StyledText fontWeight={700}>{confirmText}</StyledText>
        </Row>
      </>
    )}
    hideModal={hideModal}
    initialValues={{ additionalComments: '' }}
    isCurrentUser
    onSubmit={onConfirm}
    title={modalTitle}
  />
)

const StyledText = styled(Text)`
  font-size: ${({ fontSize }: any): string =>
    fontSize ? `${fontSize}px` : '16px'};
`

export { DecisionModal }
