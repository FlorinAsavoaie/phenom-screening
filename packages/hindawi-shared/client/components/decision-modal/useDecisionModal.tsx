import { useModal } from '@pubsweet/ui'
import { DecisionModal } from './DecisionModal'

interface HookProps {
  onConfirm: (
    _: unknown,
    {
      setFetching,
      setError,
    }: {
      setFetching: (isFetching: boolean) => void
      setError: (error: unknown) => void
    },
  ) => void
  additionalCommentTitle?: string
  confirmText?: string
  confirmButtonText: string
  cancelButtonText: string
  modalTitle: string
  additionalContent?: any
  hasTextarea?: boolean
  additionalComment?: string
  validate?: ((value: any) => string | undefined)[]
  placeholder?: string
  selectedEscalatedOption?: any
  setSelectedEscalatedOption?: any
}

interface HookResult {
  showModal: () => void
  hideModal: () => void
}

export function useDecisionModal(props: HookProps): HookResult {
  const { showModal, hideModal } = useModal({
    component: () => <DecisionModal {...props} hideModal={hideModal} />,
  })

  return {
    showModal,
    hideModal,
  }
}
