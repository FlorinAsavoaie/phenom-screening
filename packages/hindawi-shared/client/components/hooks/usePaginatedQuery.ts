import { useState } from 'react'
import { get } from 'lodash'
import { useQuery } from '@apollo/client'
import { useDebounce } from 'hindawi-shared/client/graphql/hooks'

const getQueryName = (query: any) => query.definitions[0].name.value

export const DEFAULT_PAGE_SIZE = 20

interface VariablesProps {
  pageSize: number
  searchValue: string
  filterOption?: string
}

interface QueryVariables {
  variables: VariablesProps
  settings: {
    noDebounce?: boolean
  }
}

export const usePaginatedQuery = (
  query: any,
  { variables, settings }: QueryVariables,
): any => {
  const [page, setPage] = useState(0)

  const {
    data,
    loading: originalLoading,
    error,
    refetch,
  } = useQuery(query, {
    variables: { page, ...variables },
    fetchPolicy: 'network-only',
  })
  const queryName = getQueryName(query)

  const loading = settings.noDebounce
    ? originalLoading
    : useDebounce({ value: originalLoading, delay: 500 })
  const results = get(data, `${queryName}.results`, [])
  const total = get(data, `${queryName}.total`, 0)
  const lastPage = Math.ceil(total / variables.pageSize) - 1

  const toFirst = () => setPage(0)
  const toLast = () => setPage(lastPage)
  const toNext = () => page !== lastPage && setPage(page + 1)
  const toPrevious = () => page !== 0 && setPage(page - 1)

  return {
    error,
    loading,
    page,
    refetch,
    results,
    setPage,
    toFirst,
    toLast,
    toNext,
    toPrevious,
    total,
  }
}
