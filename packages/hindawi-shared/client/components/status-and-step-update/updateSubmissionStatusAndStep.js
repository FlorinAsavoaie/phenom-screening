import { useHistory } from 'react-router-dom'
import { useApolloClient } from '@apollo/client'

import { queries } from 'component-screening-process/client/graphql'
import { useSubmissionStatusAndStepUpdatedEffect } from './useSubmissionStatusAndStepUpdatedEffect'
import { useSubmissionStatusAndStepUpdatedModal } from './useSubmissionStatusAndStepUpdatedModal'

export const StatusChangeTitle = {
  screeningApproved: 'This manuscript has been approved',
  screeningRejected: 'This manuscript has been rejected',
  returnedToDraft: 'This manuscript has been returned to draft',
  qualityCheckApproved: 'This manuscript has been approved',
  qualityCheckRejected: 'This manuscript has been rejected',
  withdrawn: 'This manuscript has been withdrawn',
  filesRequested: 'Files were requested for this manuscript',
  paused: 'This manuscript has been paused',
  inProgress: 'This manuscript has been unpaused',
  void: 'This manuscript has been void',
  deescalated: 'Manuscript has been de-escalated',
  escalated: 'Manuscript Escalated',
  sentToPeerReview: 'Manuscript sent to Peer Review',
}
export const StatusChangeSubtitle = {
  screeningApproved:
    'You can find it on the dashboard under the Archived filter.',
  screeningRejected:
    'You can find it on the dashboard under the Archived filter.',
  qualityCheckApproved:
    'You can find it on the dashboard under the Archived filter.',
  qualityCheckRejected:
    'You can find it on the dashboard under the Archived filter.',
  withdrawn: 'You can find it on the dashboard under the Archived filter.',
  void: 'You can find it on the dashboard under the Archived filter',
  returnedToDraft:
    'You can find it on the dashboard under the In Progress filter.',
  filesRequested:
    'You can find it on the dashboard under the In Progress filter.',
  paused: 'You can find it on the dashboard under the In Progress filter.',
  inProgress: 'You can find it on the dashboard under the In Progress filter.',
  escalated:
    'The manuscript has been locked until further action from your team lead.',
  deescalated: 'You can find it on the dashboard under the In Progress filter.',
  sentToPeerReview:
    'You can find it on the dashboard under the Archived filter.',
}

const StepChangeTitle = {
  inMaterialsChecking:
    'Peer review cycle check has been approved for this manuscript',
}

const StepChangeSubtitle = {
  inMaterialsChecking:
    'You can find it on the dashboard under the In Progress filter.',
}
function isInProgress({ status }) {
  return (
    status === 'filesRequested' ||
    status === 'inProgress' ||
    status === 'returnedToDraft'
  )
}

const isInEscalated = ({ status }) => status === 'escalated'

function canDoActionsInEscalatedStatus({ status }, { currentUserRole }) {
  return (
    ['escalated', 'paused'].includes(status) &&
    ['teamLeader', 'admin'].includes(currentUserRole)
  )
}

function canContinueToWorkOnManuscript(newManuscript, manuscript) {
  return (
    isInProgress(newManuscript) ||
    canDoActionsInEscalatedStatus(newManuscript, manuscript)
  )
}

function getConfirmText(newManuscript, manuscript) {
  return canContinueToWorkOnManuscript(newManuscript, manuscript)
    ? 'RETURN TO DETAILS'
    : 'RETURN TO DASHBOARD'
}

function getDialogTitle(newManuscript, manuscript) {
  if (isInEscalated(manuscript) && newManuscript.status === 'inProgress') {
    return StatusChangeTitle.deescalated
  }

  if (newManuscript.status !== manuscript.status) {
    return StatusChangeTitle[newManuscript.status]
  }
  return StepChangeTitle[newManuscript.step]
}

function getDialogSubtitle(newManuscript, manuscript) {
  if (isInEscalated(manuscript) && newManuscript.status === 'inProgress') {
    return StatusChangeSubtitle.deescalated
  }

  if (newManuscript.status !== manuscript.status) {
    return StatusChangeSubtitle[newManuscript.status]
  }
  return StepChangeSubtitle[newManuscript.step]
}

export function updateSubmissionStatusAndStep(manuscript) {
  const history = useHistory()
  const client = useApolloClient()

  const { showModal, hideModal } = useSubmissionStatusAndStepUpdatedModal()
  return useSubmissionStatusAndStepUpdatedEffect(
    (newManuscript) => {
      showModal({
        onConfirm: () => {
          if (canContinueToWorkOnManuscript(newManuscript, manuscript)) {
            hideModal()
            client.writeQuery({
              query: queries.getScreeningManuscript,
              variables: { manuscriptId: manuscript.id },
              data: {
                getScreeningManuscript: {
                  ...manuscript,
                  status: newManuscript.status,
                  step: newManuscript.step,
                },
              },
            })
          } else {
            history.push('/manuscripts')
          }
        },
        confirmText: getConfirmText(newManuscript, manuscript),
        subtitle: getDialogSubtitle(newManuscript, manuscript),
        title: getDialogTitle(newManuscript, manuscript),
      })
    },
    [manuscript.submissionId],
  )
}
