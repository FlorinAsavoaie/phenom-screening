import { useCallback, useContext } from 'react'
import ModalContext from '@pubsweet/ui/src/molecules/modal/ModalContext'

import { SubmissionStatusAndStepUpdatedModal } from './SubmissionStatusAndStepUpdatedModal'

export function useSubmissionStatusAndStepUpdatedModal() {
  const { showModal, hideModal } = useModal({
    component: ({ onConfirm, subtitle, title, confirmText }) => (
      <SubmissionStatusAndStepUpdatedModal
        confirmText={confirmText}
        hideModal={hideModal}
        onConfirm={onConfirm}
        subtitle={subtitle}
        title={title}
      />
    ),
  })

  const showModalFn = useCallback(
    ({ onConfirm, subtitle, title, confirmText }) =>
      showModal({
        onConfirm,
        subtitle,
        title,
        confirmText,
      }),
    [],
  )

  return { showModal: showModalFn, hideModal }
}
const useModal = ({ component, dismissable, ...initialProps }) => {
  const { showModal, hideModal } = useContext(ModalContext)
  return {
    hideModal,
    showModal: (modalData = {}) =>
      showModal({
        component,
        dismissable,
        modalProps: {
          ...initialProps,
          ...modalData,
        },
      }),
  }
}
