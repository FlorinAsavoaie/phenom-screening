import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H2 } from '@pubsweet/ui'
import { SingleActionModal as BaseSingleActionModal } from '@hindawi/ui'

export function SubmissionStatusAndStepUpdatedModal({
  confirmText,
  hideModal,
  onConfirm,
  subtitle,
  title,
}) {
  return (
    <SingleActionModal
      confirmText={confirmText}
      hasCloseIcon={false}
      hideModal={hideModal}
      onConfirm={onConfirm}
      subtitle={subtitle}
      title={title}
      width={141}
    />
  )
}

const SingleActionModal = styled(BaseSingleActionModal)`
  background: ${th('colorBackgroundHue')};

  ${H2} {
    margin-bottom: calc(${th('gridUnit')} * 8);
    text-align: center;
  }

  ${Button} {
    margin-top: calc(${th('gridUnit')} * 8);
    width: fit-content;
  }
`
