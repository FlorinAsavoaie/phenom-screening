import { useEffect, useCallback } from 'react'

import { useSubmissionStatusAndStepUpdatedSubscription } from 'component-screening-process/client/graphql/hooks'

export function useSubmissionStatusAndStepUpdatedEffect(effect, submissionIds) {
  if (!process.env.SUBSCRIPTIONS_ENABLED) {
    return
  }

  const { manuscript } =
    useSubmissionStatusAndStepUpdatedSubscription(submissionIds)

  const effectFn = useCallback(() => {
    if (manuscript) {
      effect(manuscript)
    }
  }, [manuscript])

  useEffect(effectFn, [manuscript])
}
