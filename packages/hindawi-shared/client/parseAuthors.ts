interface AuthorProps {
  email: string
  aff: string
  surname: string
  givenNames: string
  isSubmitting: boolean

  id?: string
  orcid?: string
  isCorresponding?: boolean
  isVerified?: boolean
  isVerifiedOutOfTool?: boolean
  isOnBadDebtList?: boolean
  isOnBlackList?: boolean
  isOnWatchList?: boolean
  __typename?: Record<string, any>
}

interface AuthorResult {
  alias: {
    email: string
    aff: string
    name: {
      surname: string
      givenNames: string
    }
  }
  isSubmitting: boolean
  isCorresponding?: boolean
}

export const parseAuthors = (authors: AuthorProps[]): AuthorResult[] =>
  authors.map(
    (author: AuthorProps): AuthorResult => ({
      alias: {
        email: author.email,
        aff: author.aff,
        name: {
          surname: author.surname,
          givenNames: author.givenNames,
        },
      },
      isSubmitting: author.isSubmitting,
      isCorresponding: author.isCorresponding,
    }),
  )
