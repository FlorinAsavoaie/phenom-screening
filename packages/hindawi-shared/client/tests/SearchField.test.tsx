import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import { SearchField } from '../components/search/SearchField'

describe('Search Field', () => {
  const onChangeMock = jest.fn()
  const onSearchMock = jest.fn()

  const someInputValue = 'test value'

  it('should have a placeholder', () => {
    const { getByPlaceholderText } = render(
      <SearchField
        onChange={onChangeMock}
        onSearch={onSearchMock}
        placeholder="Search by Name"
        value={someInputValue}
        width={88}
      />,
    )

    const inputNode = getByPlaceholderText('Search by Name')
    expect(inputNode).toBeInTheDocument()
  })
})
