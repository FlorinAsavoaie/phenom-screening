const manuscriptMock = {
  id: '0be14728-335f-4f5c-9678-d7667af4d37f',
  title: '3RD TEST 3',
  fileId: '2a5c2bc1-aec1-4191-8f57-f7dbc307700a',
  version: '1',
  step: 'inMaterialsChecking',
  files: [
    {
      id: '2a5c2bc1-aec1-4191-8f57-f7dbc307700a',
      type: 'manuscript',
      providerKey:
        '453b014a-ae47-43f8-bd31-a95c10bfd13e/546d1503-4fef-45b1-88b2-e5ba60687428',
      originalFileProviderKey:
        '453b014a-ae47-43f8-bd31-a95c10bfd13e/546d1503-4fef-45b1-88b2-e5ba60687428',
      originalName: 'Blockchain.pdf',
      fileName: 'Blockchain.pdf',
      size: 515351,
      mimeType: 'application/pdf',
      __typename: 'ScreeningFile',
    },
  ],
  mainManuscriptVersions: [
    {
      id: '0be14728-335f-4f5c-9678-d7667af4d37f',
      version: '1',
      __typename: 'ManuscriptVersionIdentifier',
    },
  ],
  materialCheckManuscriptVersions: [
    {
      id: '0be14728-335f-4f5c-9678-d7667af4d37f',
      version: '1',
      __typename: 'ManuscriptVersionIdentifier',
    },
  ],
  customId: '5535593',
  phenomId: '453b014a-ae47-43f8-bd31-a95c10bfd13e',
  abstract: '3RD TEST 3',
  articleType: 'Corrigendum',
  isShortArticleSupported: true,
  submissionId: 'e66ce925-0ea4-445f-8b22-3db6decc2391',
  submissionDate: '2022-06-23T12:50:25.144Z',
  references: null,
  flowType: 'qualityCheck',
  status: 'inProgress',
  authors: [
    {
      id: 'ae6757a1-7bc5-4478-9844-287fa816162c',
      isSubmitting: true,
      isCorresponding: true,
      isVerifiedOutOfTool: false,
      aff: 'University of Negros Occidental – Recoletos, Bacolod City',
      email: 'luciancostin.ailenei@gmail.com',
      orcid: '',
      givenNames: 'LUCIAN',
      surname: 'AILENEI',
      isVerified: false,
      isOnBadDebtList: false,
      isOnBlackList: false,
      isOnWatchList: false,
      __typename: 'Author',
    },
  ],
  journal: {
    id: 'f7e946a4-5243-4a34-8777-64dc1e25f65a',
    name: 'FAKE SINGLE TIER',
    __typename: 'ScreeningJournal',
  },
  specialIssue: null,
  currentUserRole: 'admin',
  linkedSubmissionCustomId: '5527518',
}

export default manuscriptMock
