import { MockedProvider } from '@apollo/client/testing'
import { render, waitForGraphqlQueryDataResponse } from './testUtils'
import { useManuscriptByCustomId } from '../graphql/hooks'
import { getManuscriptByCustomId } from '../graphql/queries'

describe('useManuscriptByCustomId hook tests', () => {
  const MANUSCRIPT_CUSTOM_ID = '5527518'

  it('should dispatch getManuscriptByCustomId gql query at component render and return the result', async () => {
    const graphqlQueryMocks = [
      {
        request: {
          query: getManuscriptByCustomId,
          variables: {
            manuscriptCustomId: MANUSCRIPT_CUSTOM_ID,
          },
        },
        result: jest.fn().mockReturnValue({
          data: {
            manuscript: {
              title: 'FAKE 3RD Manuscript',
              submissionId: 'e865e6c3-09ff-47a3-bfd1-c457d0824c4c',
              phenomId: '1ee35830-f3ad-490b-a979-d2001d64ae4d',
              status: 'screeningApproved',
            },
          },
        }),
      },
    ]

    const FakeComponent = () => {
      const { manuscriptData, loading } =
        useManuscriptByCustomId(MANUSCRIPT_CUSTOM_ID)

      return (
        <>
          {loading && 'Loading...'}
          {!loading && <div>{manuscriptData.title}</div>}
        </>
      )
    }

    render(
      <MockedProvider addTypename={false} mocks={graphqlQueryMocks}>
        <FakeComponent />
      </MockedProvider>,
    )

    await waitForGraphqlQueryDataResponse()

    expect(graphqlQueryMocks[0].result).toHaveBeenCalled()
  })
})
