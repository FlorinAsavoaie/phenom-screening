import { theme } from '@hindawi/ui'
import { ThemeProvider } from 'styled-components'
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client'
import { ModalProvider } from '@pubsweet/ui'
import { act, render as rtlRender } from '@testing-library/react'
import { MockLink } from '@apollo/client/testing'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router'
import wait from 'waait'

function createClient(mocks?: any) {
  return new ApolloClient({
    cache: new InMemoryCache(),
    link: new MockLink(mocks),
  })
}

const history = createBrowserHistory()

const render = (ui, mocks?: any) => {
  const client = createClient(mocks)
  const Component = () => (
    <ModalProvider>
      <ApolloProvider client={client}>
        <Router history={history}>
          <ThemeProvider theme={theme}>{ui}</ThemeProvider>
        </Router>
      </ApolloProvider>
    </ModalProvider>
  )

  const utils = rtlRender(<Component />)

  return {
    ...utils,
  }
}

const waitForGraphqlQueryDataResponse = (): Promise<void> =>
  act(async () => {
    await wait()
  })

export { render, waitForGraphqlQueryDataResponse }
