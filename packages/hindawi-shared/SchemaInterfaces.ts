export interface Journal {
  id: string
  name: string
}

export interface ManuscriptFile {
  id: string
  type: string
  providerKey: string
  originalFileProviderKey: string
  originalName: string
  fileName: string
  size: number
  mimeType: string
}

export interface Author {
  id: string
  givenNames: string
  surname: string
  aff: string
  email: string
  orcid: string
  isSubmitting: boolean
  isCorresponding: boolean
  isVerifiedOutOfTool: boolean
  isVerified: boolean
  isOnBadDebtList: boolean
  isOnBlackList: boolean
  isOnWatchList: boolean
}
export interface Manuscript {
  id: string
  title: string
  customId: string
  version: string
  abstract: string
  articleType: string
  submissionId: string
  submissionDate: string
  references: string
  flowType: string
  status: string
  currentUserRole: string
  journal: Journal
  files: ManuscriptFile[]
  authors: Author[]
  phenomId: string
  linkedSubmissionCustomId: string
}

export interface User {
  id: string
  surname: string
  givenNames: string
  email: string
  isConfirmed: boolean
}

export interface AuditLog {
  id: string
  created: string
  userRole: string
  action: string
  comment: string
  user: User
}
