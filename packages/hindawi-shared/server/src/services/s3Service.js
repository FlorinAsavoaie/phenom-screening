const { S3, GetObjectCommand } = require('@aws-sdk/client-s3')
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner')
const config = require('config')
const logger = require('@pubsweet/logger')

const screeningBucket = config.get('aws.s3.screeningBucket')
const reviewBucket = config.get('aws.s3.reviewBucket')

const clientS3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
  useAccelerateEndpoint: true,
})

module.exports.uploadReviewFileToScreeningS3 = async (key) =>
  clientS3.copyObject({
    Bucket: screeningBucket,
    CopySource: `${reviewBucket}/${key}`,
    Key: key,
  })

module.exports.uploadReviewManuscriptFileToScreeningS3 = async (
  reviewProviderKey,
  screeningProviderKey,
) => {
  const reviewHeadFile = await clientS3.headObject({
    Bucket: reviewBucket,
    Key: reviewProviderKey,
  })

  return clientS3.copyObject({
    Bucket: screeningBucket,
    CopySource: `${reviewBucket}/${reviewProviderKey}`,
    Key: screeningProviderKey,
    Metadata: { ...reviewHeadFile.Metadata, reviewProviderKey },
    MetadataDirective: 'REPLACE',
    ContentType: reviewHeadFile.ContentType,
  })
}

module.exports.getObjectStream = (key) =>
  clientS3.getObject({ Key: key, Bucket: screeningBucket })

module.exports.getSignedUrl = async (key) =>
  getSignedUrl(
    clientS3,
    new GetObjectCommand({
      Key: key,
      Bucket: screeningBucket,
    }),
    {
      expiresIn: 3600,
    },
  )

module.exports.getObject = async (key) =>
  clientS3.getObject({ Key: key, Bucket: screeningBucket })

module.exports.listObjects = async (prefix) =>
  clientS3.listObjectsV2({ Prefix: prefix, Bucket: screeningBucket })

module.exports.deleteObjects = async (keys) => {
  if (keys.length === 0) {
    logger.info(`Nothing to delete from s3.`)
    return
  }
  const mappedKeys = keys.map((key) => ({ Key: key }))

  return clientS3.deleteObjects({
    Delete: { Objects: mappedKeys },
    Bucket: screeningBucket,
  })
}
