import { useState } from 'react'
import { chain, maxBy } from 'lodash'
import { Container } from './Container'
import {
  Tabs,
  SideBySide,
  ManuscriptPeerReviewDetails,
  PeerReviewChecksContainer,
} from '../components'
import { useManuscriptPeerReviewChecks } from '../graphql/hooks'
import { CompareButton } from '../components/compare-button'

export function PeerReviewCycleCheckTab({ manuscript, disabled }) {
  const manuscriptVersions = chain(manuscript)
    .get('mainManuscriptVersions')
    .map((manuscript, index) => ({ ...manuscript, orderNumber: index + 1 }))
    .value()

  const latestManuscriptVersion = getLatestManuscriptVersion(manuscriptVersions)

  const [selectedTab, setSelectedTab] = useState(
    latestManuscriptVersion.version,
  )

  const previousManuscriptVersion = getPreviousManuscriptVersion(
    manuscriptVersions,
    selectedTab,
  )
  const currentManuscriptVersion = getCurrentManuscriptVersion(
    manuscriptVersions,
    selectedTab,
  )
  const { sections } = useManuscriptPeerReviewChecks({
    manuscriptId: latestManuscriptVersion.id,
    numberOfSections: 3,
  })

  const [isInComparationMode, setComparisonMode] = useState(false)
  const toggleFn = () => {
    setComparisonMode(!isInComparationMode)
  }

  const showPeerReviewChecks = sections.length

  return (
    <Container>
      <PeerReviewChecksContainer
        disabled={disabled}
        manuscriptId={latestManuscriptVersion.id}
        sections={sections}
        showPeerReviewChecks={showPeerReviewChecks}
      />

      {!isInComparationMode ? (
        <Tabs
          actionButtons={
            selectedTab > 1
              ? CompareButton({ isInComparationMode, toggleFn })
              : null
          }
          selectedTab={selectedTab}
          setSelectedTab={setSelectedTab}
        >
          {manuscriptVersions.map(({ id, version }) => (
            <div key={version} label={version}>
              <ManuscriptPeerReviewDetails
                manuscriptId={id}
                p={4}
                type="peerReviewCycleCheck"
              />
            </div>
          ))}
        </Tabs>
      ) : (
        <SideBySide
          actionButtons={CompareButton({
            isInComparationMode,
            toggleFn,
          })}
          leftSide={previousManuscriptVersion}
          rightSide={currentManuscriptVersion}
          setSelectedTab={setSelectedTab}
          type="peerReviewCycleCheck"
        />
      )}
    </Container>
  )
}

function getCurrentManuscriptVersion(manuscriptVersions, currentVersionNumber) {
  return manuscriptVersions.find((man) => man.version === currentVersionNumber)
}

function getPreviousManuscriptVersion(
  manuscriptVersions,
  currentVersionNumber,
) {
  const currentMan = getCurrentManuscriptVersion(
    manuscriptVersions,
    currentVersionNumber,
  )

  const previousVersionNumber = currentMan.orderNumber - 1

  return manuscriptVersions.find(
    (man) => man.orderNumber === previousVersionNumber,
  )
}

function getLatestManuscriptVersion(manuscriptVersions) {
  return maxBy(manuscriptVersions, 'orderNumber')
}
