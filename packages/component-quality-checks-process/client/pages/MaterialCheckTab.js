import { useState } from 'react'
import styled from 'styled-components'
import {
  Tabs,
  SideBySide,
  MaterialChecksContainer,
  QualityCheckerRequest,
  ManuscriptPeerReviewDetails,
} from '../components'

import { useManuscriptMaterialChecks } from '../graphql'
import { Container } from './Container'
import { CompareButton } from '../components/compare-button'

export function MaterialCheckTab({ manuscript, disabled }) {
  const [isInComparationMode, setComparisonMode] = useState(false)

  const materialCheckManuscriptVersions =
    manuscript.materialCheckManuscriptVersions.map((mCheck) => mCheck.version)
  const materialCheckManuscripts =
    manuscript.materialCheckManuscriptVersions.map((manuscript, index) => ({
      ...manuscript,
      orderNumber: index,
    }))

  const lastMaterialCheckVersion = materialCheckManuscriptVersions.slice(-1)[0]

  const [selectedTab, setSelectedTab] = useState(lastMaterialCheckVersion)

  const { sections } = useManuscriptMaterialChecks({
    manuscriptId: manuscript.id,
    numberOfSections: 3,
  })

  const currentManuscriptVersion = materialCheckManuscripts.find(
    (man) => man.version === selectedTab,
  )
  const previousManuscriptVersion = materialCheckManuscripts.find(
    (man) => man.orderNumber === currentManuscriptVersion.orderNumber - 1,
  )
  const selectedTabHasMinorVersion = !Number.isInteger(parseFloat(selectedTab))

  const toggleFn = () => {
    setComparisonMode(!isInComparationMode)
  }

  return (
    <Container>
      <StyledMaterialChecksContainer>
        <MaterialChecksContainer
          disabled={disabled}
          manuscriptId={manuscript.id}
          sections={sections}
        />
      </StyledMaterialChecksContainer>
      {!isInComparationMode ? (
        <Tabs
          actionButtons={
            selectedTabHasMinorVersion
              ? CompareButton({ isInComparationMode, toggleFn })
              : null
          }
          selectedTab={selectedTab}
          setSelectedTab={setSelectedTab}
        >
          {materialCheckManuscripts.map(({ id, version }) => (
            <div key={version} label={version}>
              <ManuscriptPeerReviewDetails
                manuscriptId={id}
                p={4}
                type="materialCheck"
              >
                {() => (
                  <>
                    {selectedTabHasMinorVersion && (
                      <QualityCheckerRequest
                        manuscriptId={currentManuscriptVersion.id}
                      />
                    )}
                  </>
                )}
              </ManuscriptPeerReviewDetails>
            </div>
          ))}
        </Tabs>
      ) : (
        <SideBySide
          actionButtons={CompareButton({ isInComparationMode, toggleFn })}
          currentTabManuscript={manuscript}
          leftSide={previousManuscriptVersion}
          rightSide={currentManuscriptVersion}
          type="materialCheck"
        >
          <QualityCheckerRequest manuscriptId={manuscript.id} />
        </SideBySide>
      )}
    </Container>
  )
}

const StyledMaterialChecksContainer = styled.div`
  margin-bottom: 15px;
`
