import { useState, useEffect } from 'react'

interface TabDefinition {
  label: string
  disabled: boolean
  icon: {
    hasIcon: boolean
    name: string
    color: string
  }
}

const TABS_INITIAL_STATE: TabDefinition[] = [
  {
    label: 'Pre-screening Check',
    disabled: false,
    icon: {
      hasIcon: false,
      name: 'check',
      color: 'statusApproved',
    },
  },
  {
    label: 'Info Validation',
    disabled: false,
    icon: {
      hasIcon: false,
      name: 'check',
      color: 'statusApproved',
    },
  },
  {
    label: 'Author Identity Verification',
    disabled: false,
    icon: {
      hasIcon: false,
      name: 'check',
      color: 'statusApproved',
    },
  },
  {
    label: 'Peer Review Cycle Check',
    disabled: false,
    icon: {
      hasIcon: false,
      name: 'check',
      color: 'statusApproved',
    },
  },
  {
    label: 'Materials Check',
    disabled: true,
    icon: {
      hasIcon: true,
      name: 'lock',
      color: 'actionSecondaryColor',
    },
  },
]

function getTabsConfigurationForMaterialChecking(
  tabs: TabDefinition[],
): TabDefinition[] {
  return tabs.map((tab) => {
    if (tab.label === 'Peer Review Cycle Check') {
      return {
        ...tab,
        icon: {
          ...tab.icon,
          hasIcon: true,
        },
      }
    }
    if (tab.label === 'Materials Check') {
      return {
        ...tab,
        icon: {
          ...tab.icon,
          hasIcon: false,
        },
        disabled: false,
      }
    }
    return tab
  })
}

function getTabsStateByStep(
  manuscriptStep: string,
  isShortArticleSupported: boolean,
): TabDefinition[] {
  if (isShortArticleSupported) {
    return TABS_INITIAL_STATE.filter(
      (tab) => tab.label !== 'Peer Review Cycle Check',
    ).map((tab) => {
      if (tab.label === 'Materials Check') {
        return {
          ...tab,
          icon: {
            ...tab.icon,
            hasIcon: false,
          },
          disabled: false,
        }
      }
      return tab
    })
  }

  switch (manuscriptStep) {
    case 'inMaterialsChecking':
      return getTabsConfigurationForMaterialChecking(TABS_INITIAL_STATE)
    case 'inPeerReviewCycleChecking':
      return TABS_INITIAL_STATE
    default:
      throw new Error('Invalid manuscript step')
  }
}

export function useQualityCheckingTabsConfiguration(
  manuscriptStep: string,
  isShortArticleSupported: boolean,
): TabDefinition[] {
  const [tabs, setTabs] = useState(
    getTabsStateByStep(manuscriptStep, isShortArticleSupported),
  )

  useEffect(() => {
    setTabs(getTabsStateByStep(manuscriptStep, isShortArticleSupported))
  }, [manuscriptStep])

  return tabs
}
