import { createContext, useState, PropsWithChildren } from 'react'
import { useQualityCheckingTabsConfiguration } from './useQualityCheckingTabsConfiguration'

type SetSelectedTabFn = (newIndexTab: number) => void

interface QualityCheckingTabsContext {
  selectedCheckTabIndex: number
  setSelectedTab: SetSelectedTabFn
  tabs: any
  isPeerReviewCycleCheckSelected: () => boolean
  isMaterialCheckSelected: () => boolean
  goToNextCheck: () => void
}

const QualityCheckingTabsContext = createContext<QualityCheckingTabsContext>(
  {} as QualityCheckingTabsContext,
)

interface QualityCheckingTabsProviderProps {
  manuscriptStep: string
  isShortArticleSupported: boolean
}

const QualityCheckingTabsProvider: React.FC<
  PropsWithChildren<QualityCheckingTabsProviderProps>
> = ({ manuscriptStep, isShortArticleSupported, children }) => {
  const [selectedCheckTabIndex, setSelectedTab] = useState(0)
  const tabs = useQualityCheckingTabsConfiguration(
    manuscriptStep,
    isShortArticleSupported,
  )

  function isPeerReviewCycleCheckSelected() {
    return tabs[selectedCheckTabIndex].label === 'Peer Review Cycle Check'
  }

  function isAuthorIdentityVerificationSelected() {
    return tabs[selectedCheckTabIndex].label === 'Author Identity Verification'
  }
  function isMaterialCheckSelected() {
    return tabs[selectedCheckTabIndex].label === 'Materials Check'
  }

  function goToNextCheck() {
    const newTabIndex =
      isAuthorIdentityVerificationSelected() &&
      manuscriptStep === 'inMaterialsChecking' &&
      !isShortArticleSupported
        ? selectedCheckTabIndex + 2
        : selectedCheckTabIndex + 1
    setSelectedTab(newTabIndex)
  }

  return (
    <QualityCheckingTabsContext.Provider
      value={{
        selectedCheckTabIndex,
        setSelectedTab,
        tabs,
        isPeerReviewCycleCheckSelected,
        isMaterialCheckSelected,
        goToNextCheck,
      }}
    >
      {children}
    </QualityCheckingTabsContext.Provider>
  )
}

export { QualityCheckingTabsContext, QualityCheckingTabsProvider }
