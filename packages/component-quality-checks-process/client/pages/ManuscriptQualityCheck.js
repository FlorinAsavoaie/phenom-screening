import styled from 'styled-components'

import { updateSubmissionStatusAndStep } from 'hindawi-shared/client/components/status-and-step-update'

import { QualityCheckingTabsProvider } from './QualityCheckingTabsProvider'
import { QualityCheckingTabs } from './QualityCheckingTabs'
import { QualityCheckBottomBar } from '../components'

function ManuscriptQualityCheck({ manuscript, currentUser }) {
  updateSubmissionStatusAndStep(manuscript)
  const currentUserIsChecker = ['screener', 'qualityChecker'].includes(
    manuscript.currentUserRole,
  )
  const canUserMakeActionsOnManuscript =
    (!currentUserIsChecker && manuscript.status === 'escalated') ||
    manuscript.status === 'inProgress'
  const canUserMakeDecisionOnManuscript = manuscript.status === 'inProgress'

  return (
    <Root>
      <QualityCheckingTabsProvider
        isShortArticleSupported={manuscript.isShortArticleSupported}
        manuscriptStep={manuscript.step}
      >
        <QualityCheckingTabs
          currentUser={currentUser}
          disabled={!canUserMakeActionsOnManuscript}
          manuscript={manuscript}
        />
        <QualityCheckBottomBar
          currentUserIsChecker={currentUserIsChecker}
          disabled={!canUserMakeDecisionOnManuscript}
          isShortArticleSupported={manuscript.isShortArticleSupported}
          manuscriptCurrentUserRole={manuscript.currentUserRole}
          manuscriptId={manuscript.id}
          manuscriptStatus={manuscript.status}
          manuscriptStep={manuscript.step}
          specialIssueId={manuscript.specialIssue?.id}
        />
      </QualityCheckingTabsProvider>
    </Root>
  )
}

export default ManuscriptQualityCheck

const Root = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`
