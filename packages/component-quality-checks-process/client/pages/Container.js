import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export function Container({ children }) {
  return (
    <Root>
      <Content>{children}</Content>
    </Root>
  )
}

// #region styles
const Root = styled.div`
  overflow: scroll;
  overflow-x: hidden;
  height: 100%;
`

const Content = styled.div`
  border-radius: calc(${th('gridUnit')} * 1.5);
  box-shadow: calc(${th('gridUnit')} * 0.8) calc(${th('gridUnit')} * 0.8)
    calc(${th('gridUnit')} * 1.5) 0 rgba(141, 141, 141, 0.2);
  background-color: ${th('white')};
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: fit-content;
  margin: calc(${th('gridUnit')} * 2);
  padding: calc(${th('gridUnit')} * 4);
`
// #endregion
