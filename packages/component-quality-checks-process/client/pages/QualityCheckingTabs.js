import { useContext } from 'react'

import { Tabs } from 'hindawi-shared/client/components'
import {
  PreScreeningCheckTab,
  InfoValidationTab,
  AuthorIdentityVerificationTab,
} from 'component-screening-process/client'
import { useManuscriptRightTabs } from 'component-screening-process/client/hooks/useManuscriptRightTabs'
import { PeerReviewCycleCheckTab } from './PeerReviewCycleCheckTab'
import { MaterialCheckTab } from './MaterialCheckTab'
import { QualityCheckingTabsContext } from './QualityCheckingTabsProvider'

export function QualityCheckingTabs({ manuscript, currentUser, disabled }) {
  const { selectedCheckTabIndex, setSelectedTab, tabs } = useContext(
    QualityCheckingTabsContext,
  )
  const rightTabs = useManuscriptRightTabs(manuscript)

  return (
    <Tabs
      handleClick={setSelectedTab}
      rightTabs={rightTabs}
      selectedTab={selectedCheckTabIndex}
      tabs={tabs}
    >
      <PreScreeningCheckTab currentUser={currentUser} manuscript={manuscript} />
      <InfoValidationTab disabled={disabled} manuscript={manuscript} />
      <AuthorIdentityVerificationTab
        disabled={disabled}
        manuscript={manuscript}
      />
      {!manuscript.isShortArticleSupported && (
        <PeerReviewCycleCheckTab
          disabled={!canEditPeerReviewChecks(manuscript)}
          manuscript={manuscript}
        />
      )}
      <MaterialCheckTab disabled={disabled} manuscript={manuscript} />
    </Tabs>
  )
}

function canEditPeerReviewChecks({ status, step }) {
  return status === 'inProgress' && step !== 'inMaterialsChecking'
}
