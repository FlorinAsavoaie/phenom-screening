import { useFinalizePeerReviewCycleCheckModal } from './useFinalizePeerReviewCycleCheckModal'
import { DecisionButton } from '../decision-button'

const BUTTON_LABEL = 'Finalize Peer Review Cycle Check'

interface FinalizePeerReviewCycleCheckButtonProps {
  disabled: boolean
  onConfirm: () => Promise<unknown>
}

const FinalizePeerReviewCycleCheckButton: React.FC<FinalizePeerReviewCycleCheckButtonProps> =
  ({ onConfirm, disabled }) => {
    const { showModal } = useFinalizePeerReviewCycleCheckModal({
      onConfirm,
    })

    return (
      <DecisionButton
        disabled={disabled}
        label={BUTTON_LABEL}
        onClick={showModal}
        primary
      />
    )
  }

export { FinalizePeerReviewCycleCheckButton }
