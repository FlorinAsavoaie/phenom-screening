import { ErrorText } from '@pubsweet/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { useDecisionModal } from 'hindawi-shared/client/components/decision-modal'
import { Text } from '@hindawi/ui'

interface HookProps {
  onConfirm: () => Promise<unknown>
}

interface HookResult {
  showModal: () => void
}

const MODAL_TITLE = 'Finalize peer review cycle check?'
const CANCEL_BUTTON_TEXT = 'CANCEL'
const CONFIRM_BUTTON_TEXT = 'FINALIZE'
const ADDITIONAL_CONTENT_TEXT =
  'Once marked as completed, no more changes can be done to this section.'

export function useFinalizePeerReviewCycleCheckModal({
  onConfirm,
}: HookProps): HookResult {
  const { showModal, hideModal } = useDecisionModal({
    modalTitle: MODAL_TITLE,
    additionalContent: () => <Text mt={8}>{ADDITIONAL_CONTENT_TEXT}</Text>,
    onConfirm: (_, { setFetching, setError }) => {
      setFetching(true)

      onConfirm().then(
        () => {
          setFetching(false)
          hideModal()
        },
        (err) => {
          const error = <ErrorText>{parseGQLError(err)}</ErrorText>
          setFetching(false)
          setError(error)
        },
      )
    },
    cancelButtonText: CANCEL_BUTTON_TEXT,
    confirmButtonText: CONFIRM_BUTTON_TEXT,
    hasTextarea: false,
  })

  return { showModal }
}
