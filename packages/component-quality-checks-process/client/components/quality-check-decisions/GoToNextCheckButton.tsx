import { DecisionButton } from './decision-button'

const BUTTON_LABEL = 'Go to Next Check'

interface GoToNextCheckButtonProps {
  disabled: boolean
  onClick: () => void
}

const GoToNextCheckButton: React.FC<GoToNextCheckButtonProps> = ({
  onClick,
  disabled,
}) => (
  <DecisionButton disabled={disabled} label={BUTTON_LABEL} onClick={onClick} />
)

export { GoToNextCheckButton }
