import { useContext } from 'react'

import { Row } from '@hindawi/ui'
import {
  RefuseToConsiderButton,
  SendBackToCheckerButton,
} from 'component-screening-process/client/components'
import {
  ApproveQualityCheckButton,
  RequestFilesButton,
  FinalizePeerReviewCycleCheckButton,
  GoToNextCheckButton,
} from './index'
import { SendToPeerReviewButton } from './send-to-peer-review'

import {
  useApproveQualityCheck,
  useRequestFiles,
  useRefuseToConsiderQualityCheck,
  useApprovePeerReviewCycle,
} from '../../graphql'
import { useSendToPeerReview } from '../../graphql/hooks/useSendToPeerReview'
import { QualityCheckingTabsContext } from '../../pages/QualityCheckingTabsProvider'

export function QualityCheckRightSideBottomBar({
  disabled,
  manuscriptId,
  manuscriptStep,
  specialIssueId,
  manuscriptStatus,
  manuscriptCurrentUserRole,
  isShortArticleSupported,
}) {
  const {
    isPeerReviewCycleCheckSelected,
    isMaterialCheckSelected,
    goToNextCheck,
  } = useContext(QualityCheckingTabsContext)

  const manuscriptIsEscalated = manuscriptStatus === 'escalated'
  const userIsTeamLeadOrAdmin =
    manuscriptCurrentUserRole === 'teamLeader' ||
    manuscriptCurrentUserRole === 'admin'

  const specialIssueManuscript = !!specialIssueId

  return (
    <Row justify="flex-end">
      {manuscriptStep === 'inMaterialsChecking' ? (
        <DisplayButtonsForMaterialCheckingStep
          disabled={disabled}
          isMaterialCheckSelected={isMaterialCheckSelected}
          isShortArticleSupported={isShortArticleSupported}
          manuscriptId={manuscriptId}
          manuscriptIsEscalated={manuscriptIsEscalated}
          onGoToNextCheck={goToNextCheck}
        />
      ) : (
        <DisplayButtonsForPeerReviewCycleCheckingStep
          disabled={disabled}
          isPeerReviewCycleCheckSelected={isPeerReviewCycleCheckSelected}
          manuscriptId={manuscriptId}
          manuscriptIsEscalated={manuscriptIsEscalated}
          onGoToNextCheck={goToNextCheck}
          specialIssueManuscript={specialIssueManuscript}
        />
      )}
      {manuscriptIsEscalated && userIsTeamLeadOrAdmin && (
        <SendBackToCheckerButton
          manuscriptId={manuscriptId}
          mr={6}
          type="CHECKER"
        />
      )}
    </Row>
  )
}

function DisplayButtonsForMaterialCheckingStep({
  disabled,
  manuscriptId,
  isMaterialCheckSelected,
  onGoToNextCheck,
  manuscriptIsEscalated,
  isShortArticleSupported,
}) {
  const { approveQualityCheck } = useApproveQualityCheck()
  const { requestFiles } = useRequestFiles()
  const { refuseToConsiderQualityCheck } = useRefuseToConsiderQualityCheck()

  return (
    <>
      <RequestFilesButton
        disabled={disabled}
        mr={4}
        onConfirm={({ additionalComments }) =>
          requestFiles({ manuscriptId, additionalComments })
        }
      />
      {isShortArticleSupported && (
        <RefuseToConsiderButton
          disabled={disabled}
          mr={4}
          onConfirm={({ additionalComments, ...rest }) =>
            refuseToConsiderQualityCheck({
              manuscriptId,
              additionalComments,
              rest,
            })
          }
        />
      )}
      {isMaterialCheckSelected() && !manuscriptIsEscalated ? (
        <ApproveQualityCheckButton
          disabled={disabled}
          mr={6}
          onConfirm={({ additionalComments }) =>
            approveQualityCheck({ manuscriptId, additionalComments })
          }
        />
      ) : (
        !manuscriptIsEscalated && (
          <GoToNextCheckButton disabled={disabled} onClick={onGoToNextCheck} />
        )
      )}
    </>
  )
}

function DisplayButtonsForPeerReviewCycleCheckingStep({
  disabled,
  manuscriptId,
  isPeerReviewCycleCheckSelected,
  onGoToNextCheck,
  manuscriptIsEscalated,
  specialIssueManuscript,
}) {
  const { refuseToConsiderQualityCheck } = useRefuseToConsiderQualityCheck()
  const approvePeerReviewCycle = useApprovePeerReviewCycle()
  const sendToPeerReview = useSendToPeerReview()

  return (
    <>
      {manuscriptIsEscalated && (
        <SendToPeerReviewButton
          manuscriptId={manuscriptId}
          onConfirm={(formResult) =>
            sendToPeerReview({
              manuscriptId,
              isDecisionMadeInError: formResult.isDecisionMadeInError,
              conflictOfInterest: formResult.conflictOfInterest,
              improperReviewEmails: formResult.improperReviewEmails,
              comment: formResult.comment,
            })
          }
          specialIssueManuscript={specialIssueManuscript}
        />
      )}
      <RefuseToConsiderButton
        disabled={disabled}
        mr={4}
        onConfirm={({ additionalComments, ...rest }) =>
          refuseToConsiderQualityCheck({
            manuscriptId,
            additionalComments,
            rest,
          })
        }
      />
      {isPeerReviewCycleCheckSelected() && !manuscriptIsEscalated ? (
        <FinalizePeerReviewCycleCheckButton
          disabled={disabled}
          onConfirm={() =>
            approvePeerReviewCycle({ manuscriptId }).then(() => {
              onGoToNextCheck()
            })
          }
        />
      ) : (
        !manuscriptIsEscalated && (
          <GoToNextCheckButton disabled={disabled} onClick={onGoToNextCheck} />
        )
      )}
    </>
  )
}
