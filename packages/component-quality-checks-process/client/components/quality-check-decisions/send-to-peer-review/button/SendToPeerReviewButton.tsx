import { useState } from 'react'
import styled from 'styled-components'
import { Modal as BaseModal, Form, Button } from '@hindawi/phenom-ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import {
  SendToPeerReviewForm,
  FormResult,
  sendToPeerReviewDetailsToDTO,
  SendToPeerReviewFormData,
} from '../form'

interface SendToPeerReviewButtonProps {
  onConfirm: (formResult: FormResult) => Promise<unknown>
  manuscriptId: string
  specialIssueManuscript: boolean
}

const SendToPeerReviewButton: React.FC<SendToPeerReviewButtonProps> = ({
  onConfirm,
  manuscriptId,
  specialIssueManuscript,
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [form] = Form.useForm<SendToPeerReviewFormData>()
  const [confirmLoading, setConfirmLoading] = useState(false)
  const showModal = () => setIsModalVisible(true)
  const [error, setError] = useState('')

  const onCancel = () => {
    setIsModalVisible(false)
    form.resetFields()
    setError('')
  }

  return (
    <>
      <Button onClick={showModal} style={{ marginRight: '24px' }} type="ghost">
        Send to Peer Review
      </Button>
      <Modal
        cancelText="CANCEL"
        centered
        confirmLoading={confirmLoading}
        destroyOnClose
        keyboard={false}
        maskClosable={false}
        okText="RETURN TO PEER REVIEW"
        onCancel={onCancel}
        onOk={() => {
          setConfirmLoading(true)
          form
            .validateFields()
            .then((formValues) =>
              onConfirm(sendToPeerReviewDetailsToDTO(formValues)),
            )
            .then(() => {
              setConfirmLoading(false)
              form.resetFields()
              setIsModalVisible(false)
            })
            .catch((err) => {
              setConfirmLoading(false)
              if (err.graphQLErrors) {
                setError(parseGQLError(err))
              }
            })
        }}
        title="Return Manuscript to Peer Review"
        visible={isModalVisible}
      >
        <SendToPeerReviewForm
          error={error}
          form={form}
          manuscriptId={manuscriptId}
          specialIssueManuscript={specialIssueManuscript}
        />
      </Modal>
    </>
  )
}

export { SendToPeerReviewButton }

const Modal = styled(BaseModal)`
  .ant-btn-loading-icon {
    padding-right: 4px;
  }
`
