import { Select, Form } from '@hindawi/phenom-ui'
import { Icon } from 'hindawi-shared/client/components'
import { Member } from '../../../../graphql/hooks/useManuscriptMembers'

interface ReviewersDropdownProps {
  reviewers: Member[]
  formName: string | string[]
  style?: React.CSSProperties
}

const ReviewersDropdown: React.FC<ReviewersDropdownProps> = ({
  reviewers,
  formName,
  style,
}) => (
  <Form.Item
    label="Select reviewers"
    labelCol={{ span: 24 }}
    name={formName}
    required
    rules={[
      {
        required: true,
        message: (
          <>
            <Icon color="warningColor" mr={1} name="warning" />
            Please select at least one reviewer.
          </>
        ),
      },
    ]}
    style={style}
    valuePropName="value"
  >
    <Select
      getPopupContainer={(trigger) => trigger.parentNode}
      mode="multiple"
      placeholder="Select one or more"
    >
      {RenderOptions(reviewers)}
    </Select>
  </Form.Item>
)

function RenderOptions(options: Member[]) {
  return options.map((option: Member) => (
    <Select.Option key={option.id} label={option.name} value={option.email}>
      {option.name}
    </Select.Option>
  ))
}

export { ReviewersDropdown }
