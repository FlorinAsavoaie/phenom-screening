import { useState } from 'react'
import { Checkbox, Col, Form, Row } from '@hindawi/phenom-ui'
import { Icon } from 'hindawi-shared/client/components'

import { Member } from '../../../../graphql/hooks/useManuscriptMembers'
import { ReviewersDropdown } from './ReviewersDropdown'

interface MembersCheckboxListProps {
  reviewers: Member[]
  hasTriageEditor: boolean
  hasAcademicEditor: boolean
  specialIssueManuscript: boolean
}

const MembersCheckboxList: React.FC<MembersCheckboxListProps> = ({
  reviewers,
  hasTriageEditor,
  hasAcademicEditor,
  specialIssueManuscript,
}) => {
  const [isSelectedConflictWithReviewers, setConflictWithReviewers] =
    useState(false)

  const academicEditorLabel = specialIssueManuscript
    ? 'Guest Editor'
    : 'Academic Editor'

  const triageEditorLabel = specialIssueManuscript
    ? 'Lead Guest Editor'
    : 'Triage Editor'

  return (
    <>
      <Form.Item
        label="Conflict of interest found between author and:"
        labelCol={{ span: 24 }}
        name="conflictOfInterestReasons"
        required
        rules={[
          {
            required: true,
            message: (
              <>
                <Icon color="warningColor" mr={1} name="warning" />
                Please select at least one role there is a conflict with.
              </>
            ),
          },
        ]}
        style={{ marginTop: 8, marginLeft: 24, marginBottom: 0 }}
        valuePropName="checked"
      >
        <Checkbox.Group style={{ display: 'block' }}>
          <Row>
            <Checkbox disabled={!hasAcademicEditor} value="academicEditor">
              {academicEditorLabel}
            </Checkbox>
            <Checkbox disabled={!hasTriageEditor} value="triageEditor">
              {triageEditorLabel}
            </Checkbox>
            <Checkbox
              onChange={() =>
                setConflictWithReviewers(!isSelectedConflictWithReviewers)
              }
              value="reviewers"
            >
              Reviewers
            </Checkbox>
          </Row>
        </Checkbox.Group>
      </Form.Item>
      {isSelectedConflictWithReviewers && (
        <Row>
          <Col span={24}>
            <ReviewersDropdown
              formName={['conflictOfInterest', 'reviewers', 'emails']}
              reviewers={reviewers}
              style={{ marginLeft: 24 }}
            />
          </Col>
        </Row>
      )}
    </>
  )
}

export { MembersCheckboxList }
