import { useState } from 'react'
import styled from 'styled-components'
import { Form, Checkbox, Row, Col, Text, Spinner } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import { FormInstance } from 'antd'
import { Icon } from 'hindawi-shared/client/components'

import { useManuscriptMembers } from '../../../../graphql/hooks/useManuscriptMembers'
import {
  CommentTextArea,
  MembersCheckboxList,
  ReviewersDropdown,
  INITIAL_VALUES,
  SendToPeerReviewFormData,
} from '.'

interface SendToPeerReviewFormProps {
  error: string
  form: FormInstance<SendToPeerReviewFormData>
  manuscriptId: string
  specialIssueManuscript: boolean
}

const SendToPeerReviewForm: React.FC<SendToPeerReviewFormProps> = ({
  error,
  form,
  manuscriptId,
  specialIssueManuscript,
}) => {
  const { reviewers, triageEditor, academicEditor, loading } =
    useManuscriptMembers(manuscriptId)
  const [isImproperReviewSelected, setIsImproperReview] = useState(false)
  const [isConflictOfInterestSelected, setIsConflictOfInterest] =
    useState(false)

  const hasTriageEditor = triageEditor !== null
  const hasAcademicEditor = academicEditor !== null

  if (loading) {
    return (
      <Row justify="center" style={{ marginTop: 156, marginBottom: 117 }}>
        <Spinner />
      </Row>
    )
  }
  const toggleImproperReview = () => {
    setIsImproperReview((v) => !v)
  }

  const toggleConflictOfInterest = () => {
    setIsConflictOfInterest((v) => !v)
  }

  return (
    <Root>
      <Form
        form={form}
        initialValues={INITIAL_VALUES}
        name="sendToPeerReviewForm"
      >
        <Row>
          <Col span={24}>
            <Form.Item
              label="Select the reasons for returning to peer review"
              labelCol={{ span: 24 }}
              name="peerReviewReasons"
              required
              rules={[
                {
                  required: true,
                  message: (
                    <>
                      <Icon color="warningColor" mr={1} name="warning" />
                      Please select at least one reason for returning to peer
                      review.
                    </>
                  ),
                },
              ]}
              style={{ marginBottom: 24, fontSize: 16 }}
              valuePropName="checked"
            >
              <Checkbox.Group style={{ display: 'block' }}>
                <Row gutter={[0, 8]} style={{ marginTop: 4 }}>
                  <Col span={24}>
                    <Checkbox value="decisionMadeInError">
                      Decision made in error
                    </Checkbox>
                  </Col>
                  <Col span={24}>
                    <Checkbox
                      onChange={toggleImproperReview}
                      value="improperReview"
                    >
                      Improper review
                    </Checkbox>
                    {isImproperReviewSelected && (
                      <ReviewersDropdown
                        formName={['improperReview', 'emails']}
                        reviewers={reviewers}
                        style={{
                          marginLeft: 24,
                          marginTop: 8,
                        }}
                      />
                    )}
                  </Col>
                  <Col span={24}>
                    <Checkbox
                      onChange={toggleConflictOfInterest}
                      value="conflictOfInterest"
                    >
                      Conflict of interest
                    </Checkbox>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    {isConflictOfInterestSelected && (
                      <MembersCheckboxList
                        hasAcademicEditor={hasAcademicEditor}
                        hasTriageEditor={hasTriageEditor}
                        reviewers={reviewers}
                        specialIssueManuscript={specialIssueManuscript}
                      />
                    )}
                  </Col>
                </Row>
              </Checkbox.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <CommentTextArea />
          </Col>
        </Row>

        {error && (
          <Row style={{ position: 'absolute', bottom: 77 }}>
            <Col span={24}>
              <Text preset={Preset.MESSAGE} type="danger">
                <Icon color="warningColor" mr={1} name="warning" />
                Something went wrong. Please try again.
              </Text>
            </Col>
          </Row>
        )}
      </Form>
    </Root>
  )
}

export { SendToPeerReviewForm }

const Root = styled.div`
  margin-top: 32px;

  div.ant-form-item {
    margin-bottom: 16px;
  }

  div.ant-form-item-label {
    padding: 0;

    label {
      height: initial;
      font-size: inherit;
    }
  }

  h3 {
    margin: 0;
  }

  .ant-checkbox + span {
    padding-left: 4px;
  }

  .ant-form-item-explain.ant-form-item-explain-error {
    position: absolute;
    bottom: -24px;
  }

  .ant-form-item-explain.ant-form-item-explain-success {
    display: none;
  }
`
