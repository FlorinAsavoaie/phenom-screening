export interface SendToPeerReviewFormData {
  peerReviewReasons: string[]
  conflictOfInterestReasons: string[]
  improperReview: {
    emails?: string[]
  }
  conflictOfInterest: {
    reviewers?: {
      emails?: string[]
    }
  }
  comment: string
}

export interface FormResult {
  isDecisionMadeInError: boolean
  improperReviewEmails: string[]
  conflictOfInterest: {
    withAcademicEditor: boolean
    withTriageEditor: boolean
    withReviewerEmails: string[]
  }
  comment: string
}

export function sendToPeerReviewDetailsToDTO(
  data: SendToPeerReviewFormData,
): FormResult {
  return {
    isDecisionMadeInError:
      data.peerReviewReasons?.includes('decisionMadeInError') ?? false,
    improperReviewEmails: data.improperReview?.emails ?? [],
    conflictOfInterest: {
      withAcademicEditor:
        data.conflictOfInterestReasons?.includes('academicEditor') ?? false,
      withTriageEditor:
        data.conflictOfInterestReasons?.includes('triageEditor') ?? false,

      withReviewerEmails: data.conflictOfInterest?.reviewers?.emails ?? [],
    },
    comment: data.comment ?? '',
  }
}

export const INITIAL_VALUES = {
  isDecisionMadeInError: false,
  improperReview: {
    isSelected: false,
    emails: [],
  },
  conflictOfInterest: {
    isSelected: false,
    academicEditor: {
      isSelected: false,
    },
    triageEditor: {
      isSelected: false,
    },
    reviewers: {
      isSelected: false,
      emails: [],
    },
  },
  comment: '',
}
