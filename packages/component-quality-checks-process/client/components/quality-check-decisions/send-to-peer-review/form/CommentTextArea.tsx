import { Form, Input } from '@hindawi/phenom-ui'
import { Icon } from 'hindawi-shared/client/components'
import styled from 'styled-components'

const CommentTextArea: React.FC = () => (
  <Root>
    <Form.Item
      label="Your Comments"
      labelCol={{ span: 24 }}
      name="comment"
      required
      rules={[
        {
          required: true,
          message: (
            <>
              <Icon color="warningColor" mr={1} name="warning" />
              Please add details about your decision.
            </>
          ),
        },
      ]}
    >
      <Input.TextArea allowClear />
    </Form.Item>
  </Root>
)

export { CommentTextArea }

const Root = styled.div`
  .ant-form-item .ant-mentions,
  .ant-form-item textarea.ant-input {
    min-height: 106px;
  }
`
