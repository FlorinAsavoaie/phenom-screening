import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button as BaseButton } from '@pubsweet/ui'

interface DecisionButtonProps {
  disabled?: boolean
  label: string
  onClick: () => void
  primary?: true
  secondary?: true
}

const DecisionButton: React.FC<DecisionButtonProps> = ({
  label,
  onClick,
  disabled,
  primary,
  secondary,
}) => (
  <Button
    disabled={disabled}
    mr={6}
    onClick={onClick}
    primary={primary}
    secondary={secondary}
    small
  >
    {label}
  </Button>
)

const Button = styled(BaseButton)`
  min-width: unset;
  font-size: ${th('fontSizeBase')};
  white-space: nowrap;
`

export { DecisionButton }
