import { Row } from '@hindawi/ui'

import {
  EscalateButton,
  UnpauseButton,
} from 'component-screening-process/client/components/screeningDecisions'

import { useUnpauseManuscript } from 'component-screening-process/client/hooks/useUnpauseDecision'

export function QualityCheckLeftSideBottomBar({
  disabled,
  manuscriptId,
  manuscriptStatus,
  currentUserIsChecker,
}) {
  const manuscriptIsPaused = manuscriptStatus === 'paused'
  const manuscriptIsEscalated = manuscriptStatus === 'escalated'
  const { unpauseManuscript } = useUnpauseManuscript()

  return (
    <Row justify="flex-start" ml={6}>
      {!manuscriptIsPaused && (
        <EscalateButton
          currentUserIsChecker={currentUserIsChecker}
          disabled={disabled || manuscriptIsEscalated}
          onConfirm={({ additionalComments, selectedEscalatedOption }) =>
            selectedEscalatedOption.executeEscalationAction({
              manuscriptId,
              additionalComments,
            })
          }
        />
      )}
      {manuscriptIsPaused && (
        <UnpauseButton
          manuscriptId={manuscriptId}
          mr={4}
          onConfirm={() => unpauseManuscript({ manuscriptId })}
        />
      )}
    </Row>
  )
}
