import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'
import { QualityCheckRightSideBottomBar } from './QualityCheckRightSideBottomBar'
import { QualityCheckLeftSideBottomBar } from './QualityCheckLeftSideBottomBar'

export function QualityCheckBottomBar({
  disabled,
  manuscriptId,
  manuscriptStatus,
  manuscriptStep,
  specialIssueId,
  manuscriptCurrentUserRole,
  currentUserIsChecker,
  isShortArticleSupported,
}) {
  return (
    <Root>
      <QualityCheckLeftSideBottomBar
        currentUserIsChecker={currentUserIsChecker}
        disabled={disabled}
        manuscriptId={manuscriptId}
        manuscriptStatus={manuscriptStatus}
      />
      <QualityCheckRightSideBottomBar
        disabled={disabled}
        isShortArticleSupported={isShortArticleSupported}
        manuscriptCurrentUserRole={manuscriptCurrentUserRole}
        manuscriptId={manuscriptId}
        manuscriptStatus={manuscriptStatus}
        manuscriptStep={manuscriptStep}
        specialIssueId={specialIssueId}
      />
    </Root>
  )
}

export default QualityCheckBottomBar

const Root = styled.div`
  align-items: center;
  background-color: ${th('white')};
  border: solid 1px #dbdbdb;
  box-shadow: 0 2px 3px 0 rgba(25, 102, 141, 0.19);
  display: flex;
  height: calc(${th('gridUnit')}* 16);
`
