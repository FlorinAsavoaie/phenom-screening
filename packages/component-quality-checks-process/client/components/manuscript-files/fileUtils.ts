import { get } from 'lodash'

import { ManuscriptFile } from 'hindawi-shared/SchemaInterfaces'

const hasPreviewMimeTypes = ['application/pdf', 'text/plain', 'image/jpeg']

export const fileHasPreview = (file: ManuscriptFile): boolean =>
  hasPreviewMimeTypes.includes(get(file, 'mimeType'))

export const parseFileSize = (file: ManuscriptFile): string => {
  const size = get(file, 'size')
  const kbSize = size / 1000
  const mbSize = kbSize / 1000
  const gbSize = mbSize / 1000

  if (Math.floor(gbSize)) {
    return `${Math.floor(gbSize)} GB`
  } else if (Math.floor(mbSize)) {
    return `${Math.floor(mbSize)} MB`
  } else if (Math.floor(kbSize)) {
    return `${Math.floor(kbSize)} kB`
  }
  return `${size} bytes`
}

export const createAnchorElement = (
  file: File | Blob,
  filename: string,
): { a: HTMLAnchorElement; url: string } => {
  const url = URL.createObjectURL(file)
  const a = document.createElement('a')

  a.href = url
  a.download = filename
  document.body.appendChild(a)

  return {
    a,
    url,
  }
}

export const removeAnchorElement = (
  a: HTMLAnchorElement,
  url: string,
): void => {
  document.body.removeChild(a)
  URL.revokeObjectURL(url)
}
