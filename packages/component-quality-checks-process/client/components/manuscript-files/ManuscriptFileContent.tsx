import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import { Item as StyledItem } from '@hindawi/ui'
import { ManuscriptFile } from 'hindawi-shared/SchemaInterfaces'
import { space, SpaceProps } from 'styled-system'
import ManuscriptFilesSection from './ManuscriptFilesSection'

interface ManuscriptFilesContentProps extends SpaceProps {
  files: ManuscriptFile[]
  enableColumnDirection?: boolean
}

export const ManuscriptFilesContent: React.FC<ManuscriptFilesContentProps> = ({
  files,
  enableColumnDirection,
  ...rest
}) => {
  const mainManuscriptFile = files.filter((f) => f.type === 'manuscript')
  const supplementaryFiles = files.filter((f) => f.type === 'supplementary')
  const coverLetterFiles = files.filter((f) => f.type === 'coverLetter')
  const figureFiles = files.filter((f) => f.type === 'figure')

  return (
    <Container enableColumnDirection={enableColumnDirection} {...rest}>
      <BorderedContainer enableColumnDirection={enableColumnDirection}>
        <Item enableColumnDirection={enableColumnDirection} pr={4} vertical>
          <ManuscriptFilesSection
            files={mainManuscriptFile}
            label="Main Manuscript"
          />
          <ManuscriptFilesSection
            files={supplementaryFiles}
            label="Supplementary Files"
          />
        </Item>
      </BorderedContainer>
      <Item enableColumnDirection={enableColumnDirection} pl={4} vertical>
        <ManuscriptFilesSection files={coverLetterFiles} label="Cover Letter" />
        <ManuscriptFilesSection files={figureFiles} label="Figures & Tables" />
      </Item>
    </Container>
  )
}

ManuscriptFilesContent.propTypes = {
  /** Sets the direction of the files. If true all files are layed out in a column. */
  enableColumnDirection: PropTypes.bool,
}

ManuscriptFilesContent.defaultProps = {
  enableColumnDirection: false,
}

interface ContainerProps extends SpaceProps {
  enableColumnDirection?: boolean
}

const Container = styled.div<ContainerProps>`
  display: flex;

  ${(props): any => {
    if (props.enableColumnDirection) {
      return css`
        flex-direction: column;
      `
    }
  }}

  ${space};
`

interface ItemProps {
  enableColumnDirection?: boolean
}

const Item = styled(StyledItem)`
  ${(props: ItemProps): any => {
    if (props.enableColumnDirection) {
      return css`
        padding: 0px;
      `
    }
  }}
`

const BorderedContainer = styled(StyledItem)`
  height: auto;
  padding: 0px;
  ${(props: ItemProps): any => {
    if (!props.enableColumnDirection) {
      return css`
        border-right: calc(${th('gridUnit')} * 0.25) solid #e0e0e0;
      `
    }
  }}
`
