import styled, { css } from 'styled-components'
import { space, SpaceProps } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import { useSignedUrl } from 'component-screening-process/client/hooks/useSignedUrl'
import { Icon, Loader as BaseLoader, Text } from '@hindawi/ui'
import { ManuscriptFile } from 'hindawi-shared/SchemaInterfaces'
import { parseFileSize, fileHasPreview } from './fileUtils'
import { useFileDownload } from '../../graphql/hooks/useFileDownload'

interface FileProps extends SpaceProps {
  file: ManuscriptFile
  width?: string
  key?: string
}

const File: React.FC<FileProps> = ({ file, width, ...rest }) => {
  const { getSignedUrl } = useSignedUrl()
  const { isFetching, downloadFile } = useFileDownload(file)

  const onPreview = (file: ManuscriptFile) => (): void => {
    getSignedUrl({ fileId: file.id, format: 'original' })
      .then((url: string) => window.open(url))
      .catch(console.error)
  }
  const fileSize = parseFileSize(file)
  const hasPreview = fileHasPreview(file)

  return (
    <Root data-test-id={`file-${file.id}`} width={width} {...rest}>
      <FileInfo>
        <Text display="block" ellipsis title={file.fileName}>
          {file.fileName}
        </Text>

        <FileSize mx={2}>{fileSize}</FileSize>
      </FileInfo>
      <Icons>
        {hasPreview && (
          <Icon
            color="colorSecondary"
            data-test-id={`${file.id}-preview`}
            fontSize="15px"
            icon="preview"
            ml={4}
            onClick={onPreview(file)}
            secondary
          />
        )}

        {isFetching ? (
          <Loader iconSize={3} />
        ) : (
          <Icon
            color="colorSecondary"
            data-test-id={`${file.id}-preview`}
            fontSize="15px"
            icon="download"
            ml={4}
            onClick={downloadFile}
            secondary
          />
        )}
      </Icons>
    </Root>
  )
}

interface RootProps extends SpaceProps {
  width: any
  shadow?: boolean
}
// #region styles
const Root = styled.div<RootProps>`
  align-items: center;
  background: ${th('backgroundColor')};
  box-shadow: ${({ shadow }): string => (shadow ? th('boxShadow') : 'none')};
  border-radius: ${th('borderRadius')};
  display: flex;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  height: calc(${th('gridUnit')} * 8);
  position: relative;

  ${space};
  ${({ width }): any => {
    if (width) {
      return css`
        width: calc(${th('gridUnit')} * ${width});
      `
    }
    return css`
      width: inherit;
    `
  }}
`
const Loader = styled(BaseLoader)`
  margin: 0px 4px 0px 15px;
`
const Icons = styled.div`
  margin-right: calc(${th('gridUnit')} * 4);
  display: flex;
`
const FileInfo = styled.div`
  align-items: center;
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  font-family: ${th('defaultFont')};
  display: flex;
  height: inherit;
  flex: 1;
  justify-content: space-between;

  margin-left: calc(${th('gridUnit')} * 2);
  width: calc(${th('gridUnit')} * 47);
`

const FileSize = styled(Text)`
  font-weight: 700;
  font-size: 11px;
  white-space: nowrap;
`
// #endregion

export default File
