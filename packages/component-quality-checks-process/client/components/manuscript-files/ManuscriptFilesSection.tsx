import { Item, Row, Label, Text } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { ManuscriptFile } from 'hindawi-shared/SchemaInterfaces'
import File from './File'

interface ManuscriptFilesSectionProps {
  files: ManuscriptFile[]
  label: string
}

const ManuscriptFilesSection: React.FC<ManuscriptFilesSectionProps> = ({
  files,
  label,
}) => (
  <Item my={2} vertical>
    <Label>{label}</Label>

    {files.length ? (
      <Row flexDirection="column">
        {files.map((file) => (
          <File file={file} key={file.id} mt={2} />
        ))}
      </Row>
    ) : (
      <NoFiles mt={2}>No files uploaded</NoFiles>
    )}
  </Item>
)

export default ManuscriptFilesSection

// #region styles
const NoFiles = styled(Text)`
  height: calc(${th('gridUnit')} * 8);
  font-style: italic;
  color: ${th('labelLineColor')};
`
// #endregion
