import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { MaterialCheckRadioGroup as BaseMaterialCheckRadioGroup } from './MaterialCheckRadioGroup'
import {
  useSelectOptionForMaterialCheck,
  useUpdateObservationForMaterialCheck,
} from '../../graphql/hooks'

const materialChecksLabelMap = {
  hasFrontPage: 'Manuscript has front page',
  hasAllFiguresAndTables: 'Manuscript has all figures and tables',
  hasAllSupplementaryMaterials: 'Manuscript has all supplementary materials',
  hasPatiantParticipantConsentForm:
    'Manuscript has patient/participant consent form',
  hasAcknowledgements: 'Manuscript has acknowledgements',
  hasClinicalStudyRegistrationNumber:
    'Manuscript has clinical study registration number',
  hasCoverLetter: 'Manuscript has cover letter',
  hasAllSections: 'Manuscript has all sections',
  hasFinalVersionOfTheAuthorList:
    'Manuscript has final version of the author list',
  hasAllAffiliationsListed: 'Manuscript has all affiliations listed',
  hasConflictOfInterestMention: 'Manuscript has conflict of interest mention',

  frontPageApproved: 'Front Page Approved',
  headersAndFootersApproved: 'Headers and footers approved',
  figuresAndTablesPresentInPDFAndAllCited:
    'Figures and Tables present in PDF and all cited',
  figuresProvidedInEditableFormat: 'Figures provided in editable format',
  hasFileInEditableFormat: 'Manuscript file in an editable format',
  supplementaryMaterialsApproved: 'Supplementary Materials approved',
  hasPatientParticipantConsentFormApproved:
    'Patient/Participant Consent form approved',
  hasAcknowledgementsApproved: 'Acknowledgements approved',
  clinicalStudyRegistrationNumberApproved:
    'Clinical Study Registration Number approved',
  coverLetterApproved: 'Cover Letter approved',
  allSectionsPresentAndApproved: 'All sections present and approved',
  conflictOfInterestMention: 'Conflict of Interests approved',
  dataAvailabilityStatementApproved: 'Data Availability Statement approved',
  fundingStatementApproved: 'Funding Statement approved',
  manuscriptFigureFiles: 'Does this manuscript contain figure files?',
  editableFigureCaptions: 'Editable figure captions provided',
  arXivID: 'arXiv ID provided',
}

function MaterialCheckList({ materialChecks, manuscriptId, disabled }) {
  const { selectOptionForMaterialCheck } = useSelectOptionForMaterialCheck()
  const { updateObservationForMaterialCheck } =
    useUpdateObservationForMaterialCheck()

  return (
    <Root>
      {materialChecks.map(
        ({ name, selectedOption, additionalInfo, config: { options } }) => (
          <MaterialCheckRadioGroup
            additionalInfo={additionalInfo}
            disabled={disabled}
            key={name}
            label={materialChecksLabelMap[name]}
            onObservationChange={(additionalInfo) => {
              updateObservationForMaterialCheck({
                manuscriptId,
                name,
                additionalInfo,
              })
            }}
            onOptionChange={(selectedOption) =>
              selectOptionForMaterialCheck({
                manuscriptId,
                name,
                selectedOption,
              })
            }
            options={options}
            selectedOptionValue={selectedOption}
          />
        ),
      )}
    </Root>
  )
}
export default MaterialCheckList

// #region styles
const Root = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: calc(${th('gridUnit')} * 2);
  margin-right: calc(${th('gridUnit')} * 4);
  margin-bottom: calc(${th('gridUnit')} * 2);
  margin-left: calc(${th('gridUnit')} * 2);
`

const MaterialCheckRadioGroup = styled(BaseMaterialCheckRadioGroup)`
  width: 100%;
  margin-top: calc(${th('gridUnit')} * 2);
  margin-bottom: calc(${th('gridUnit')} * 4);

  &:first-child {
    margin-top: 0;
  }
  &:last-child {
    margin-bottom: 0;
  }
`
// #endregion
