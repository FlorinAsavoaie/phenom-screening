import { ContextualBox, Item } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import MaterialCheckList from './MaterialCheckList'

export function MaterialChecksContainer({ sections, manuscriptId, disabled }) {
  return (
    <ContextualBox label="Checks" startExpanded>
      <Item mx={2} my={2}>
        {sections.map(({ id, materialChecks }) => (
          <Section key={id}>
            <MaterialCheckList
              disabled={disabled}
              manuscriptId={manuscriptId}
              materialChecks={materialChecks}
            />
          </Section>
        ))}
      </Item>
    </ContextualBox>
  )
}

const Section = styled.div`
  display: flex;
  width: 100%;
  border-right: calc(${th('gridUnit')} * 0.25) solid #e0e0e0;
  margin-left: calc(${th('gridUnit')} * 2);

  &:first-child {
    margin-left: 0;
  }

  &:last-child {
    border-right: none;
  }
`
