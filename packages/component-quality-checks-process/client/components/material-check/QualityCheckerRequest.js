import styled from 'styled-components'
import { DateParser } from '@pubsweet/ui'
import { Text, Row, Item, Loader } from '@hindawi/ui'
import {
  Card,
  Comments,
  CardHeader,
  ContextualBox,
} from '../peer-review-cycle-check/manuscript-peer-review-details/StyledComponents'
import { usePreviousRequestFromQualityChecker } from './../../graphql'

export function QualityCheckerRequest({ manuscriptId }) {
  const { previousRequestFromQualityChecker, loading } =
    usePreviousRequestFromQualityChecker(manuscriptId)

  if (loading) {
    return <Loader mt={4} mx="auto" />
  }

  const additionalComments =
    previousRequestFromQualityChecker.additionalComments.content
  const commentDate =
    previousRequestFromQualityChecker.additionalComments.created
  const assignedChecker = previousRequestFromQualityChecker.assignedChecker
    ? `${previousRequestFromQualityChecker.assignedChecker.givenNames} ${previousRequestFromQualityChecker.assignedChecker.surname}`
    : 'No quality checker assigned'

  return (
    <Item mt={4}>
      <ContextualBox
        label="Request from Quality Checker"
        startExpanded
        transparent
      >
        <Card>
          <CardHeader>
            <Row>
              <Text fontWeight={700} whiteSpace="nowrap">
                Quality Checker:
              </Text>
              <Text ml={1}>{assignedChecker}</Text>
              <Item justify="flex-end">
                <DateParser humanizeThreshold={0} timestamp={commentDate}>
                  {(commentDate) => (
                    <Text display="flex">{`${commentDate}`}</Text>
                  )}
                </DateParser>
              </Item>
            </Row>
          </CardHeader>
          <Card border={0}>
            <Comments>
              <Row justify="flex-start" mt={4}>
                <Text>
                  Your manuscript submission is missing these sections, please
                  update:
                </Text>
              </Row>
              <StyledList>
                <ul>
                  {previousRequestFromQualityChecker.checkerObservations.map(
                    (checkerItem) => (
                      <li key={checkerItem.request}>
                        <Row>
                          <Item vertical>
                            <Text fontWeight={700} pb={1}>
                              {checkerItem.request}
                            </Text>
                            {checkerItem.observation && (
                              <ul>
                                <li>
                                  <Text lineHeight="19px" pb={1} pl={3}>
                                    {checkerItem.observation}
                                  </Text>
                                </li>
                              </ul>
                            )}
                          </Item>
                        </Row>
                      </li>
                    ),
                  )}
                </ul>
              </StyledList>
              <Item mb={4} vertical>
                <Text fontWeight={700}>The quality checker also added:</Text>
                <Text fontStyle="italic" ml={4} mt={2}>
                  {additionalComments}
                </Text>
              </Item>
            </Comments>
          </Card>
        </Card>
      </ContextualBox>
    </Item>
  )
}
const StyledList = styled.div`
  ul {
    list-style-type: none;
    padding-left: 0px;
  }
`
