import { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Label as BaseLabel, Textarea, Item } from '@hindawi/ui'

export function AdditionalInfoInput({
  onChange,
  value,
  disabled,
  options,
  selectedOptionValue,
}) {
  const [additionalInfo, setaAdditionalInfo] = useState(value)

  const handleChange = (event) => {
    onChange(event.target.value)
    setaAdditionalInfo(event.target.value)
  }
  const selectedOption = options.find(
    (option) => option.value === selectedOptionValue,
  )

  const additionalInfoLabel = {
    observation: 'Your observation',
    figure: 'Enter the figure files number here:',
  }

  return (
    <Root>
      {selectedOption.type === 'text' && (
        <>
          <Label required>{additionalInfoLabel[selectedOption.label]}</Label>
          <Textarea
            disabled={disabled}
            minHeight={18}
            onChange={handleChange}
            value={additionalInfo}
          />
        </>
      )}
      {selectedOption.type === 'number' && (
        <Item alignItems="flex-start">
          <Label required>{additionalInfoLabel[selectedOption.label]}</Label>
          <NumberInput
            disabled={disabled}
            onChange={handleChange}
            type="number"
            value={additionalInfo}
          />
        </Item>
      )}
    </Root>
  )
}

// #region styles
const Label = styled(BaseLabel)`
  display: flex;
  margin-bottom: calc(${th('gridUnit')} * 0.5);
`
const Root = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: calc(${th('gridUnit')} * 3.5);
`
const NumberInput = styled.input`
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  height: calc(${th('gridUnit')} * 8);
  width: calc(${th('gridUnit')} * 15);
  padding-left: calc(${th('gridUnit')} * 2);
  margin-left: calc(${th('gridUnit')} * 2);
  border-radius: ${th('borderRadius')};
  border-color: ${th('colorFurniture')};
  border-style: ${th('borderStyle')};
  border-width: ${th('borderWidth')};
  color: ${th('colorText')};
  margin-top: -8px;
`

// #endregion
