import { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Label as BaseLabel } from '@hindawi/ui'
import { RadioGroup } from 'hindawi-shared/client/components'
import { AdditionalInfoInput } from './AdditionalInfoInput'

const optionLabelMap = {
  yes: 'Yes',
  no: 'No',
  notRequired: 'Not Required',
}

function setInitialValues(options, selectedOptionValue) {
  return options.map((option) => {
    if (option.value === selectedOptionValue) {
      return { ...option, checked: true }
    }
    return { ...option, checked: false }
  })
}

export function MaterialCheckRadioGroup({
  label,
  additionalInfo,
  onObservationChange,
  onOptionChange,
  options,
  selectedOptionValue,
  disabled,
  ...rest
}) {
  const optionsWithLabel = options.map(({ value }) => ({
    value,
    label: optionLabelMap[value],
  }))

  const [radioGroupOptions, setOptions] = useState(
    setInitialValues(optionsWithLabel, selectedOptionValue),
  )

  const onChange = (event) => {
    const { value: selectedOptionValue } = event.target
    setOptions(setInitialValues(radioGroupOptions, selectedOptionValue))

    onOptionChange(selectedOptionValue)
  }

  return (
    <Root {...rest}>
      <Label required>{label}</Label>
      <RadioGroup
        disabled={disabled}
        inline
        onChange={onChange}
        options={radioGroupOptions}
      />
      {supportsAdditionalInfo(selectedOptionValue, options) && (
        <AdditionalInfoInput
          disabled={disabled}
          onChange={onObservationChange}
          options={options}
          selectedOptionValue={selectedOptionValue}
          value={additionalInfo}
        />
      )}
    </Root>
  )
}

function supportsAdditionalInfo(selectedOptionValue, options) {
  const option = options.find((option) => option.value === selectedOptionValue)
  return option && option.supportsAdditionalInfo
}

// #region styles
const Root = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

const Label = styled(BaseLabel)`
  display: flex;
  margin-bottom: calc(${th('gridUnit')} * 1.75);
`
// #endregion
