import { get } from 'lodash'

export function parseRecommendation(recommendation) {
  const types = {
    publish: 'Publish',
    reject: 'Reject',
    minor: 'Minor Revision',
    major: 'Major Revision',
    revision: 'Revision',
    returnToAcademicEditor: 'Return to Academic Editor',
  }

  return types[recommendation]
}

export function parseEditorContent(review) {
  const publicComment = get(review, 'publicComment', null)
  const privateComment = get(review, 'privateComment', null)

  const editorContents = {
    triageEditor: {
      publish: null,
      returnToAcademicEditor: [
        { id: 1, label: 'Return to Academic Editor', content: publicComment },
      ],
      reject: [{ id: 2, label: 'Reject', content: publicComment }],
      minor: [
        {
          id: 3,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
      major: [
        {
          id: 4,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
      revision: [
        {
          id: 5,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
    },
    academicEditor: {
      publish: [
        { id: 6, label: 'Message for Author', content: publicComment },
        {
          id: 7,
          label: 'Message for Editorial Team',
          content: privateComment,
        },
      ],
      minor: [
        {
          id: 8,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
      major: [
        {
          id: 9,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
      // revision option it's not sent by review anymore, it's used for already ingested manuscripts
      revision: [
        {
          id: 10,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
      reject: [
        {
          id: 11,
          label: 'Message for Author',
          content: publicComment,
        },
        {
          id: 12,
          label: 'Message for Editorial Team',
          content: privateComment,
        },
      ],
    },
    editorialAssistant: {
      publish: null,
      returnToAcademicEditor: [
        { id: 13, label: 'Return to Academic Editor', content: publicComment },
      ],
      reject: [{ id: 14, label: 'Reject', content: publicComment }],
      minor: [
        {
          id: 15,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
      major: [
        {
          id: 16,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
      // revision option it's not sent by review anymore, it's used for already ingested manuscripts
      revision: [
        {
          id: 17,
          label: 'Message for Author',
          content: publicComment,
        },
      ],
    },
  }

  const editorsDecisions = editorContents[review.member.role]
  const comments = editorsDecisions[review.recommendation]
  if (comments === null) return null

  return comments.filter((comments) => comments.content !== null)
}

export const editorialRoles = {
  'Editorial Assistant': 'EA',
  'Chief Editor': 'CE',
  'Academic Editor': 'AE',
  'Guest Editor': 'GE',
  'Lead Guest Editor': 'LGE',
  'Section Editor': 'SE',
  'Associate Editor': 'AssE',
}
