import styled from 'styled-components'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'

export function Tab({ label, onClick, selected, disabled, ...rest }) {
  return (
    <TabButtonContainer {...rest}>
      <TabButton
        disabled={disabled}
        key={label}
        ml={2}
        mr={2}
        onClick={onClick}
        selected={selected}
      >
        <TabButtonLabel mb={3} ml={6} mr={6} mt={3}>
          {label}
        </TabButtonLabel>
      </TabButton>
    </TabButtonContainer>
  )
}

// #region styles
const TabButtonContainer = styled.div`
  border-right: calc(${th('gridUnit')} * 0.25) solid ${th('furnitureColor')};

  &:last-child {
    border-right: none;
  }
`

const TabButton = styled.div`
  align-items: center;
  border-top: ${(props) =>
    props.selected
      ? `3px solid ${props.theme.textPrimaryColor}`
      : '3px solid transparent'};
  background-color: ${(props) =>
    props.selected ? th('colorBackgroundHue') : 'inherit'};
  box-sizing: border-box;
  cursor: pointer;
  display: flex;
  height: calc(${th('gridUnit')} * 10);
  justify-content: center;
`

const TabButtonLabel = styled.div`
  align-items: center;
  border-radius: calc(${th('gridUnit')} * 1);
  color: ${th('textPrimaryColor')};
  display: flex;
  font-family: Nunito;
  font-size: ${th('secondaryTextSize')};
  font-style: normal;
  font-weight: ${th('fontWeightBold')};
  line-height: ${th('lineHeightBase')};

  ${space};
`
// #endregion
