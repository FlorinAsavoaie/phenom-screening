import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Box } from '@hindawi/ui'
import { space } from 'styled-system'

import { Tab } from './Tab'

export function Tabs({
  children,
  actionButtons,
  selectedTab,
  setSelectedTab,
  ...rest
}) {
  return (
    <Root {...rest}>
      <Header>
        {children.map((child) => {
          const { label } = child.props
          return (
            <Tab
              key={label}
              label={`Version ${label}`}
              ml={2}
              mr={2}
              onClick={() => setSelectedTab(label)}
              selected={selectedTab === label}
            />
          )
        })}
        <ActionButtons>{actionButtons}</ActionButtons>
      </Header>
      {children.map((child) => {
        if (child.props.label !== selectedTab) return undefined
        return child.props.children
      })}
    </Root>
  )
}

// #region styles
const Root = styled(Box)`
  border: 1px solid ${th('disabledColor')};
  flex: ${(props) => get(props, 'flex', null)};
  width: ${(props) => get(props, 'width', 'inherit')};
  ${space};
`
const Header = styled.div`
  align-items: center;
  background-color: ${th('backgroundColor')};
  box-sizing: border-box;
  display: flex;
  justify-content: flex-start;
`
const ActionButtons = styled.div`
  margin-left: auto;
  margin-right: calc(${th('gridUnit')} * 4);
`

// #endregion
