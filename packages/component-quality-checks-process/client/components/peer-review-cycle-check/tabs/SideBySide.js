import styled from 'styled-components'
import { Box, Row } from '@hindawi/ui'
import { Tabs } from './Tabs'
import { ManuscriptPeerReviewDetails } from '../../peer-review-cycle-check/'
import { QualityCheckerRequest } from '../../material-check/'

export function SideBySide({
  actionButtons,
  leftSide,
  rightSide,
  setSelectedTab,
  type,
}) {
  const leftTabHasMinorVersion = !Number.isInteger(parseFloat(leftSide.version))
  const rightTabHasMinorVersion = !Number.isInteger(
    parseFloat(rightSide.version),
  )

  return (
    <Root>
      <Tabs
        flex={1}
        m={0}
        selectedTab={`${leftSide.version}`}
        setSelectedTab={setSelectedTab}
        width={0}
      >
        {[
          <div key={leftSide.version} label={`${leftSide.version}`}>
            <ScrollContainer>
              <ManuscriptPeerReviewDetails
                enableColumnDirection
                manuscriptId={leftSide.id}
                pt={4}
                px={4}
                type={type}
              />
              {leftTabHasMinorVersion && (
                <Row pb={4} px={4}>
                  <QualityCheckerRequest manuscriptId={leftSide.id} />
                </Row>
              )}
            </ScrollContainer>
          </div>,
        ]}
      </Tabs>
      <Tabs
        actionButtons={actionButtons}
        flex={1}
        m={0}
        selectedTab={`${rightSide.version}`}
        width={0}
      >
        {[
          <div key={rightSide.version} label={`${rightSide.version}`}>
            <ScrollContainer>
              <ManuscriptPeerReviewDetails
                enableColumnDirection
                manuscriptId={rightSide.id}
                pt={4}
                px={4}
                type={type}
              />
              {rightTabHasMinorVersion && (
                <Row pb={4} px={4}>
                  <QualityCheckerRequest manuscriptId={rightSide.id} />
                </Row>
              )}
            </ScrollContainer>
          </div>,
        ]}
      </Tabs>
    </Root>
  )
}

// #region styles
const Root = styled(Box)`
  border: 0px;
  display: flex;
  flex: 1;
`
const ScrollContainer = styled.div`
  overflow-y: scroll;
  height: 73vh;
`
// #endregion
