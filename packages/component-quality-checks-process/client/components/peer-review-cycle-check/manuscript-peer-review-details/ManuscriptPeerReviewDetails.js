import { Row, Loader } from '@hindawi/ui'
import {
  ManuscriptPeerReviewMetadata,
  ResponseToReviewerReports,
  EditorialComments,
  ReviewerReports,
} from '../manuscript-peer-review-details'
import { useManuscriptPeerReviewDetails } from '../../../graphql/hooks'
import { ManuscriptPeerReviewAbstract } from './ManuscriptPeerReviewAbstract'
import { ManuscriptFilesContent } from '../../manuscript-files/ManuscriptFileContent'
import { ManuscriptPeerReviewAuthorDeclaration } from './ManuscriptPeerReviewAuthorDeclaration'
import { ContextualBox } from './StyledComponents'

export function ManuscriptPeerReviewDetails({
  manuscriptId,
  enableColumnDirection,
  type,
  children,
  ...rest
}) {
  const { peerReviewDetails, loading } = useManuscriptPeerReviewDetails({
    manuscriptId,
  })
  if (loading) {
    return <Loader mt={4} mx="auto" />
  }

  const {
    abstract,
    articleType,
    authors,
    conflictOfInterest,
    dataAvailability,
    files,
    fundingStatement,
    journal: { name: journalName },
    submissionDate,
    title,
    members,
    specialIssue,
    journalSection,
  } = peerReviewDetails

  const editorTypes = ['academicEditor', 'triageEditor', 'editorialAssistant']
  const editors = members.filter((member) => editorTypes.includes(member.role))
  const reviewers = members.filter((member) => member.role === 'reviewer')
  const fileWidth = 100

  return (
    <Row flexDirection="column" {...rest}>
      <ManuscriptPeerReviewMetadata
        articleType={articleType}
        authors={authors}
        editors={editors}
        journalName={journalName}
        submissionDate={submissionDate}
        title={title}
      />
      <ManuscriptPeerReviewAbstract abstract={abstract} mt={4} />
      <ManuscriptPeerReviewAuthorDeclaration
        conflictOfInterest={conflictOfInterest}
        dataAvailability={dataAvailability}
        fundingStatement={fundingStatement}
        mt={4}
      />

      <ContextualBox label="Files" mt={4} startExpanded transparent>
        <ManuscriptFilesContent
          enableColumnDirection={enableColumnDirection}
          files={files}
        />
      </ContextualBox>
      {type === 'peerReviewCycleCheck' && (
        <>
          <ResponseToReviewerReports
            authors={authors}
            fileWidth={fileWidth}
            mt={6}
          />
          <ReviewerReports fileWidth={fileWidth} mt={6} reviewers={reviewers} />
          <EditorialComments
            editors={editors}
            journalSection={journalSection}
            mt={6}
            specialIssue={specialIssue}
          />
        </>
      )}

      {children && children()}
    </Row>
  )
}
