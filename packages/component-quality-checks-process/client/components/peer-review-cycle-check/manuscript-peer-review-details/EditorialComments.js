import { Text, Row, Item, TextTooltip } from '@hindawi/ui'
import styled from 'styled-components'
import { chain } from 'lodash'
import { DateParser } from '@pubsweet/ui'
import {
  parseRecommendation,
  parseEditorContent,
  editorialRoles,
} from '../utils'
import { ContextualBox, CardHeader, Card, Comments } from './StyledComponents'

export function EditorialComments({
  editors,
  journalSection,
  specialIssue,
  ...rest
}) {
  if (!editors.length) {
    return null
  }
  const reviews = chain(editors)
    .reduce((acc, editor) => {
      const reviews = editor.reviews
        .filter((review) => review.isValid)
        .map((review) => ({
          member: {
            surname: editor.surname,
            givenNames: editor.givenNames,
            email: editor.email,
            role: editor.role,
            roleLabel: editor.roleLabel,
          },
          ...review,
        }))

      return acc.concat(reviews)
    }, [])
    .orderBy('submitted', 'desc')
    .value()

  if (!reviews.length) {
    return null
  }

  return (
    <Item {...rest}>
      <ContextualBox label="Editorial Comments" startExpanded transparent>
        {reviews.map((review) => (
          <Card key={review.id}>
            <CardHeader>
              <Row>
                <EditorInfo
                  editors={editors}
                  journalSection={journalSection}
                  member={review.member}
                  specialIssue={specialIssue}
                />
                <Item justify="flex-end">
                  <Text fontWeight={700} pr={1} whiteSpace="nowrap">
                    {typeOfRecommendation(review.member)}:
                  </Text>
                  <Text pr={6} whiteSpace="nowrap">
                    {parseRecommendation(review.recommendation)}
                  </Text>
                  <DateParser
                    humanizeThreshold={0}
                    timestamp={review.submitted}
                  >
                    {(timestamp, timeAgo) => (
                      <Text display="flex">{`${timestamp}`}</Text>
                    )}
                  </DateParser>
                </Item>
              </Row>
            </CardHeader>
            <CommentsContent review={review} />
          </Card>
        ))}
      </ContextualBox>
    </Item>
  )
}

function CommentsContent({ review }) {
  const comments = parseEditorContent(review)
  if (!comments) return ''

  return (
    <Comments>
      {comments.map((comment) => (
        <Row key={comment.id} my={4}>
          <Item vertical>
            <Text fontWeight={700} pb={1}>
              {comment.label}
            </Text>
            <Text lineHeight="19px">{comment.content}</Text>
          </Item>
        </Row>
      ))}
    </Comments>
  )
}

function EditorInfo({ member, editors, specialIssue, journalSection }) {
  const manuscriptIsOnSectionOrSpecialIssue = Boolean(
    specialIssue || journalSection,
  )
  let decisionMaker = editors.find((editor) => editor.role === 'triageEditor')

  if (!decisionMaker || manuscriptIsOnSectionOrSpecialIssue) {
    decisionMaker = editors.find((editor) => editor.role === 'academicEditor')
  }
  return member.role === 'editorialAssistant' ? (
    <EditorialAssistantDecision decisionMaker={decisionMaker} member={member} />
  ) : (
    <EditorDecision decisionMaker={decisionMaker} member={member} />
  )
}

function EditorialAssistantDecision({ member, decisionMaker }) {
  return (
    <Text>
      Decision taken by {member.surname} {member.givenNames} {member.email} (
      <Text fontWeight={700}>{editorialRoles[member.roleLabel]}</Text>) on
      behalf of {decisionMaker.surname} {decisionMaker.givenNames}{' '}
      {decisionMaker.email} (
      <Text fontWeight={700}> {editorialRoles[decisionMaker.roleLabel]}</Text>)
    </Text>
  )
}

function EditorDecision({ member, decisionMaker }) {
  return (
    <StyledItem>
      <Text whiteSpace="nowrap">
        {member.surname} {member.givenNames}
      </Text>
      <TextTooltip title={member.email}>
        <Text ellipsis pl={1} whiteSpace="nowrap">
          {member.email}
        </Text>
      </TextTooltip>
    </StyledItem>
  )
}

const typeOfRecommendation = (member) =>
  ['triageEditor', 'editorialAssistant'].includes(member.role)
    ? 'Decision'
    : 'Recommendation'

const StyledItem = styled(Item)`
  min-width: 0;
`
