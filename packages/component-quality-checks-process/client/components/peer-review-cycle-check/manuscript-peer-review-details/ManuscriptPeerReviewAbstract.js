import { Item, Text } from '@hindawi/ui'
import { ContextualBox } from './StyledComponents'

export function ManuscriptPeerReviewAbstract({ abstract, ...rest }) {
  return (
    <Item {...rest}>
      <ContextualBox label="Abstract" startExpanded transparent>
        <Text lineHeight="19px">{abstract}</Text>
      </ContextualBox>
    </Item>
  )
}
