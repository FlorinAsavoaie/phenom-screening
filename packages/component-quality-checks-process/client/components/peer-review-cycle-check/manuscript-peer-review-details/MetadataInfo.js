import { Text, Item } from '@hindawi/ui'

export function MetadataInfo({
  children,
  label,
  value,
  withSeparator = false,
  ...rest
}) {
  return (
    <Item {...rest}>
      {value ? (
        <Text lineHeight="19px">
          <Text fontWeight="bold" lineHeight="19px" mr={1}>
            {label}
          </Text>
          {withSeparator && (
            <Text ml={1} mr={2}>
              |
            </Text>
          )}

          {value}
        </Text>
      ) : (
        <>
          <Text fontWeight="bold" mr={1}>
            {label}
          </Text>
          {children}
        </>
      )}
    </Item>
  )
}
