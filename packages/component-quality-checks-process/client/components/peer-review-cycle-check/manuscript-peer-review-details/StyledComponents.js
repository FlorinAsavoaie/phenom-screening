import styled from 'styled-components'
import { border } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import { ContextualBox as BaseContextualBox } from '@hindawi/ui'

export const ContextualBox = styled(BaseContextualBox)`
  box-shadow: none;

  .icn_icn_expand,
  .icn_icn_collapse {
    color: ${th('action.color')};
    margin-left: 0;
  }
  width: 100%;
`
export const CardHeader = styled.div`
  background-color: ${th('backgroundColor')};
  height: calc(${th('gridUnit')} * 8);
  display: flex;
  align-items: center;
  font-size: 14px;
  font-family: 'Nunito';
  padding: 0 calc(${th('gridUnit')} * 4);
  border-radius: ${th('borderRadius')} ${th('borderRadius')} 0 0;
`
export const Card = styled.div`
  margin: calc(${th('gridUnit')} * 2) 0;
  border-radius: ${th('borderRadius')};
  border: 1px solid #e0e0e0;

  ${border}
`
export const Comments = styled.div`
  margin: 0 calc(${th('gridUnit')} * 4);
`
