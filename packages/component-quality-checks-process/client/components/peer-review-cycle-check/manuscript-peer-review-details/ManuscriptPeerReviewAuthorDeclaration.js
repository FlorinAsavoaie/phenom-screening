import { Item } from '@hindawi/ui'
import { MetadataInfo } from './MetadataInfo'
import { ContextualBox } from './StyledComponents'

const NOT_DECLARED_LABEL = 'Not declared'

export function ManuscriptPeerReviewAuthorDeclaration({
  conflictOfInterest,
  dataAvailability,
  fundingStatement,
  ...rest
}) {
  return (
    <Item {...rest}>
      <ContextualBox label="Author Declaration" startExpanded transparent>
        <MetadataInfo
          label="Conflict of interest:"
          value={conflictOfInterest || NOT_DECLARED_LABEL}
        />
        <MetadataInfo
          label="Data availability statement:"
          mt={5}
          value={dataAvailability || NOT_DECLARED_LABEL}
        />

        <MetadataInfo
          label="Funding statement:"
          mt={5}
          value={fundingStatement || NOT_DECLARED_LABEL}
        />
      </ContextualBox>
    </Item>
  )
}
