import { Text, Row, Item, TextTooltip } from '@hindawi/ui'
import { DateParser } from '@pubsweet/ui'
import { chain } from 'lodash'
import styled from 'styled-components'
import { ContextualBox, CardHeader, Card, Comments } from './StyledComponents'
import { parseRecommendation } from '../utils'
import File from '../../manuscript-files/File'

export function ReviewerReports({ reviewers, fileWidth, ...rest }) {
  if (!reviewers.length) {
    return null
  }
  const reviews = reviewers.reduce((acc, reviewer) => {
    const reviews = chain(reviewer.reviews)
      .filter((review) => review.isValid)
      .map((review) => ({
        member: {
          surname: reviewer.surname,
          givenNames: reviewer.givenNames,
          email: reviewer.email,
          reviewerNumber: reviewer.reviewerNumber,
        },
        ...review,
      }))
      .orderBy('submitted', 'desc')
      .value()
    return acc.concat(reviews)
  }, [])

  return (
    <Item {...rest}>
      <ContextualBox label="Reviewer Reports" startExpanded transparent>
        {reviews.map((review) => (
          <Card key={review.id}>
            <CardHeader>
              <Row>
                <StyledItem>
                  <Text fontWeight={700} mr={1} whiteSpace="nowrap">
                    Reviewer {review.member.reviewerNumber}:
                  </Text>
                  <Text whiteSpace="nowrap">
                    {review.member.surname} {review.member.givenNames} -
                  </Text>
                  <TextTooltip title={review.member.email}>
                    <Text ellipsis pl={1} whiteSpace="nowrap">
                      {review.member.email}
                    </Text>
                  </TextTooltip>
                </StyledItem>
                <Item justify="flex-end">
                  <Text fontWeight={700} pr={1} whiteSpace="nowrap">
                    Recommendation:
                  </Text>
                  <Text pr={6} whiteSpace="nowrap">
                    {parseRecommendation(review.recommendation)}
                  </Text>
                  <DateParser
                    humanizeThreshold={0}
                    timestamp={review.submitted}
                  >
                    {(timestamp, timeAgo) => (
                      <Text display="flex">{`${timestamp}`}</Text>
                    )}
                  </DateParser>
                </Item>
              </Row>
            </CardHeader>
            <Comments>
              {review.publicComment && (
                <Row my={4}>
                  <Item vertical>
                    <Text lineHeight="19px">{review.publicComment}</Text>
                  </Item>
                </Row>
              )}
              {review.file && (
                <Row my={4}>
                  <Item vertical>
                    <File file={review.file} width={fileWidth} />
                  </Item>
                </Row>
              )}
              {review.privateComment && (
                <Row my={4}>
                  <Item vertical>
                    <Text fontWeight={700} pb={1}>
                      Confidential note for the Editorial Team
                    </Text>
                    <Text lineHeight="19px">{review.privateComment}</Text>
                  </Item>
                </Row>
              )}
            </Comments>
          </Card>
        ))}
      </ContextualBox>
    </Item>
  )
}

const StyledItem = styled(Item)`
  min-width: 0;
`
