import { Text, Row, Item } from '@hindawi/ui'
import { DateParser } from '@pubsweet/ui'
import { ContextualBox, Card, CardHeader, Comments } from './StyledComponents'
import File from '../../manuscript-files/File'

export function ResponseToReviewerReports({ authors, fileWidth, ...rest }) {
  const author = authors[0]
  const haveAuthorResponseToRevision = !!author.reviews.length
  if (!haveAuthorResponseToRevision) {
    return null
  }
  const review = author.reviews[0]

  return (
    <Item {...rest}>
      <ContextualBox
        label="Response to Reviewer Reports"
        startExpanded
        transparent
      >
        <Card>
          <CardHeader>
            <Row justify="space-between">
              <Text whiteSpace="nowrap">
                {author.surname} {author.givenNames}
              </Text>
              <DateParser humanizeThreshold={0} timestamp={review.submitted}>
                {(timestamp) => <Text display="flex">{`${timestamp}`}</Text>}
              </DateParser>
            </Row>
          </CardHeader>
          <Comments>
            {review.publicComment && (
              <Row my={4}>
                <Item vertical>
                  <Text lineHeight="19px">{review.publicComment}</Text>
                </Item>
              </Row>
            )}
            {review.file && (
              <Row my={4}>
                <Item vertical>
                  <File file={review.file} width={fileWidth} />
                </Item>
              </Row>
            )}
          </Comments>
        </Card>
      </ContextualBox>
    </Item>
  )
}
