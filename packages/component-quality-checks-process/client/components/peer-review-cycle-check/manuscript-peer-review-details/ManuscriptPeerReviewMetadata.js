import { useContext } from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled, { ThemeContext } from 'styled-components'
import { H3, DateParser } from '@pubsweet/ui'
import { Row, Text, Item, AuthorTagList } from '@hindawi/ui'
import { parseAuthors } from 'hindawi-shared/client/parseAuthors'

import { MetadataInfo } from './MetadataInfo'

const NOT_DECLARED_LABEL = 'No editor'

export function ManuscriptPeerReviewMetadata({
  editors,
  articleType,
  authors,
  journalName,
  title,
  submissionDate,
}) {
  const academicEditor = editors
    .filter((e) => e.reviews.length > 0)
    .find((member) => member.role === 'academicEditor')

  const triageEditor = editors.find((member) => member.role === 'triageEditor')

  const theme = useContext(ThemeContext)
  const parsedAuthors = parseAuthors(authors)

  return (
    <Root bgColor={theme.backgroundColor} flexDirection="column" p={2}>
      <Item>
        <H3>{title}</H3>
      </Item>
      <Item mt={4}>
        <MetadataInfo label="Authors:">
          <AuthorTagList authors={parsedAuthors} withAffiliations />
        </MetadataInfo>
      </Item>
      <Item mt={4}>
        <MetadataInfo label={articleType} value={journalName} withSeparator />
      </Item>
      <Item mt={4}>
        <MetadataInfo
          flex={1}
          label="Chief Editor:"
          value={
            triageEditor
              ? `${triageEditor.givenNames} ${triageEditor.surname}`
              : NOT_DECLARED_LABEL
          }
        />
        <MetadataInfo
          flex={2}
          label="Academic Editor:"
          value={
            academicEditor
              ? `${academicEditor.givenNames} ${academicEditor.surname}`
              : NOT_DECLARED_LABEL
          }
        />
        <MetadataInfo flex={1} justify="flex-end" label="Submitted on:">
          <DateParser
            dateFormat="DD-MM-YYYY"
            humanizeThreshold={0}
            timestamp={submissionDate}
          >
            {(timestamp) => <Text display="flex">{`${timestamp}`}</Text>}
          </DateParser>
        </MetadataInfo>
      </Item>
    </Root>
  )
}

export const Root = styled(Row)`
  border-radius: ${th('borderRadius')};
`
