import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {
  useSelectOptionForPeerReviewCheck,
  useUpdateObservationForPeerReviewCheck,
} from '../../../graphql/hooks'
import { PeerReviewCheckRadioGroup as BasePeerReviewCheckRadioGroup } from './PeerReviewCheckRadioGroup'

const materialChecksLabelMap = {
  reviewersIdentityConfirmed: 'Reviewers identity confirmed',
  reviewersAndAuthorsHaveNoConflictOfInterest:
    'Reviewers and authors have no conflict of interest',
  reviewerCommentsWereAddressed: 'Reviewer comments were addressed',
  authorsCommunicationWithReviewersIsGood:
    'Authors’ communication with reviewers is appropriate',
  reviewersCommunicationWithAuthorsIsGood:
    'Reviewers’ communication with authors is appropriate',
  editorialStaffsCommunicationIsGood:
    'Editorial staff’s communication is appropriate',
  reviewersIdentityConfirmedAndSeniortyLevelConfirmed:
    'Reviewers’ identity confirmed and seniority level confirmed',
  anyConcernsRaisedByReviewReportsAndRecommendations:
    'Any concerns raised by review reports and recommendations?',
  isThereAConflictOfInterestBetweenTheEditorAndTheAuthors:
    'Is there a conflict of interest between the Editor and the authors?',
  isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors:
    'Is there a conflict of interest between the reviewers and the authors?',
  emailsReceivedFromAuthorsRequireAction:
    'Emails received from authors require action',
  emailsReceivedFromEditorRequireAction:
    'Emails received from Editor require action',
  emailsReceivedFromReviewersRequireAction:
    'Emails received from reviewers require action',
  editorialStaffsCommunicationIsAppropriate:
    'Editorial staff’s communication is appropriate',
  otherConcernsOnReviewProcess: 'Other concerns on review process',
  conflictOfInterestMention:
    'Has author included an appropriate conflict of interest statement in the manuscript?',
  dataAvailabilityStatementApproved:
    'Has author included an appropriate data availability statement in the manuscript?',
}

function PeerReviewCheckList({ materialChecks, manuscriptId, disabled }) {
  const { selectOptionForPeerReviewCheck } = useSelectOptionForPeerReviewCheck()
  const { updateObservationForPeerReviewCheck } =
    useUpdateObservationForPeerReviewCheck()

  return (
    <Root>
      {materialChecks.map(
        ({ name, selectedOption, observation, config: { options } }) => (
          <PeerReviewCheckRadioGroup
            disabled={disabled}
            key={name}
            label={materialChecksLabelMap[name]}
            observation={observation}
            onObservationChange={(observation) => {
              updateObservationForPeerReviewCheck({
                manuscriptId,
                name,
                observation,
              })
            }}
            onOptionChange={(selectedOption) =>
              selectOptionForPeerReviewCheck({
                manuscriptId,
                name,
                selectedOption,
              })
            }
            options={options}
            selectedOptionValue={selectedOption}
          />
        ),
      )}
    </Root>
  )
}
export default PeerReviewCheckList

// #region styles
const Root = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: calc(${th('gridUnit')} * 2);
  margin-right: calc(${th('gridUnit')} * 4);
  margin-bottom: calc(${th('gridUnit')} * 2);
  margin-left: calc(${th('gridUnit')} * 2);
`

const PeerReviewCheckRadioGroup = styled(BasePeerReviewCheckRadioGroup)`
  width: 100%;
  margin-top: calc(${th('gridUnit')} * 2);
  margin-bottom: calc(${th('gridUnit')} * 4);

  &:first-child {
    margin-top: 0;
  }
  &:last-child {
    margin-bottom: 0;
  }
`
