import { ContextualBox, Item } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PeerReviewCheckList from './PeerReviewCheckList'

function PeerReviewChecksContainer({
  sections,
  manuscriptId,
  disabled,
  showPeerReviewChecks,
}) {
  if (!showPeerReviewChecks) return null
  return (
    <ContextualBox label="Checks" mb={4} startExpanded>
      <Item mx={2} my={2}>
        {sections.map(({ id, peerReviewChecks }) => (
          <Section key={id}>
            <PeerReviewCheckList
              disabled={disabled}
              manuscriptId={manuscriptId}
              materialChecks={peerReviewChecks}
            />
          </Section>
        ))}
      </Item>
    </ContextualBox>
  )
}
export default PeerReviewChecksContainer

const Section = styled.div`
  display: flex;
  width: 100%;
  border-right: calc(${th('gridUnit')} * 0.25) solid #e0e0e0;
  margin-left: calc(${th('gridUnit')} * 2);

  &:first-child {
    margin-left: 0;
  }

  &:last-child {
    border-right: none;
  }
`
