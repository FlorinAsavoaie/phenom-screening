import { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Label as BaseLabel, Textarea } from '@hindawi/ui'

export function ObservationTextarea({ onChange, value, disabled }) {
  const [observation, setObservation] = useState(value)

  const handleChange = (event) => {
    onChange(event.target.value)
    setObservation(event.target.value)
  }

  return (
    <Root>
      <Label required>Your observations</Label>
      <Textarea
        disabled={disabled}
        minHeight={18}
        onChange={handleChange}
        value={observation}
      />
    </Root>
  )
}

// #region styles
const Label = styled(BaseLabel)`
  display: flex;
  margin-bottom: calc(${th('gridUnit')} * 0.5);
`
const Root = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: calc(${th('gridUnit')} * 3.5);
`
