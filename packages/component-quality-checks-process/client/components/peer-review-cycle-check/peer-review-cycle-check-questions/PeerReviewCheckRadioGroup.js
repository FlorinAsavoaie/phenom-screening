import { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Label as BaseLabel } from '@hindawi/ui'
import { RadioGroup } from 'hindawi-shared/client/components'
import { ObservationTextarea } from './ObservationTextarea'

const optionLabelMap = {
  yes: 'Yes',
  no: 'No',
}

function setInitialValues(options, selectedOptionValue) {
  return options.map((option) => {
    if (option.value === selectedOptionValue) {
      return { ...option, checked: true }
    }
    return { ...option, checked: false }
  })
}

export function PeerReviewCheckRadioGroup({
  label,
  observation,
  onObservationChange,
  onOptionChange,
  options,
  selectedOptionValue,
  disabled,
  ...rest
}) {
  const optionsWithLabel = options.map((option) => ({
    value: option.value,
    label: optionLabelMap[option.value],
  }))

  const [radioGroupOptions, setOptions] = useState(
    setInitialValues(optionsWithLabel, selectedOptionValue),
  )

  const onChange = (event) => {
    const { value: selectedOptionValue } = event.target
    setOptions(setInitialValues(radioGroupOptions, selectedOptionValue))

    onOptionChange(selectedOptionValue)
  }

  return (
    <Root {...rest}>
      <Label required>{label}</Label>
      <RadioGroup
        disabled={disabled}
        inline
        onChange={onChange}
        options={radioGroupOptions}
      />
      {isObservationEnabled({ selectedOptionValue, options }) && (
        <ObservationTextarea
          disabled={disabled}
          onChange={onObservationChange}
          value={observation}
        />
      )}
    </Root>
  )
}

function isObservationEnabled({ selectedOptionValue, options }) {
  const option = options.find((option) => option.value === selectedOptionValue)

  return option && option.withObservation
}

// #region styles
const Root = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

const Label = styled(BaseLabel)`
  display: flex;
  margin-bottom: calc(${th('gridUnit')} * 1.75);
`

// #endregion
