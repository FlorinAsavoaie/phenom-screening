import { Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'

export function CompareButton({ isInComparationMode, toggleFn }) {
  return (
    <CompareWithPrevVersionButton onClick={toggleFn} xs>
      {isInComparationMode
        ? 'Close comparison'
        : 'Compare With Previous Version'}
    </CompareWithPrevVersionButton>
  )
}

const CompareWithPrevVersionButton = styled(Button)`
  background-color: ${th('mainTextColor')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  border-radius: 3px;
  color: ${th('white')};
  font-size: ${th('fontSizeBaseMedium')};

  :hover,
  :focus {
    background-color: ${th('mainTextColor')};
    border-radius: 3px;
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  }

  &[disabled] {
    cursor: not-allowed;
    opacity: 0.5;
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  }

  :focus,
  :hover {
    background: ${th('mainTextColor')};
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  }
`
