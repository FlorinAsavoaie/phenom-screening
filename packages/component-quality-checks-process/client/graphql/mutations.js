import { gql } from '@apollo/client'

export const selectOptionForMaterialCheck = gql`
  mutation selectOptionForMaterialCheck(
    $input: SelectOptionForMaterialCheckInput
  ) {
    materialCheck: selectOptionForMaterialCheck(input: $input) {
      selectedOption
      additionalInfo
    }
  }
`
export const selectOptionForPeerReviewCheck = gql`
  mutation selectOptionForPeerReviewCheck(
    $input: SelectOptionForPeerReviewCheckInput
  ) {
    peerReviewCheck: selectOptionForPeerReviewCheck(input: $input) {
      selectedOption
      observation
    }
  }
`

export const updateObservationForMaterialCheck = gql`
  mutation updateObservationForMaterialCheck(
    $input: UpdateObservationForMaterialCheck
  ) {
    materialCheck: updateObservationForMaterialCheck(input: $input) {
      additionalInfo
    }
  }
`

export const updateObservationForPeerReviewCheck = gql`
  mutation updateObservationForPeerReviewCheck(
    $input: UpdateObservationForPeerReviewCheck
  ) {
    peerReviewCheck: updateObservationForPeerReviewCheck(input: $input) {
      observation
    }
  }
`

export const approveQualityCheckMutation = gql`
  mutation approveQualityCheck(
    $manuscriptId: String!
    $additionalComments: String
  ) {
    manuscript: approveQualityCheck(
      manuscriptId: $manuscriptId
      additionalComments: $additionalComments
    )
  }
`

export const refuseToConsiderQualityCheckMutation = gql`
  mutation refuseToConsiderQualityCheck(
    $manuscriptId: String!
    $content: RTCInput
    $note: String
  ) {
    refuseToConsiderQualityCheck(
      manuscriptId: $manuscriptId
      content: $content
      note: $note
    )
  }
`

export const requestFilesMutation = gql`
  mutation requestFiles($manuscriptId: String!, $content: String) {
    requestFiles(manuscriptId: $manuscriptId, content: $content)
  }
`
