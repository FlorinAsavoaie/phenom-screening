import { useMutation } from '@apollo/client'
import { debounce } from 'lodash'
import * as mutations from '../mutations'
import * as queries from '../queries'

const DEBOUNCE_TIME = 500

function useUpdateObservationForPeerReviewCheck() {
  const [mutation] = useMutation(mutations.updateObservationForPeerReviewCheck)

  const updateObservationForPeerReviewCheck = (input) =>
    mutation({
      variables: { input },
      update: (
        cache,
        {
          data: {
            peerReviewCheck: { observation },
          },
        },
      ) => {
        const { manuscriptId, name } = input
        const { checks } = cache.readQuery({
          query: queries.getManuscriptPeerReviewChecks,
          variables: { manuscriptId },
        })

        const updatedPeerReviewChecks = checks.map((peerReviewCheck) => {
          if (peerReviewCheck.name === name) {
            return {
              ...peerReviewCheck,
              observation,
            }
          }
          return peerReviewCheck
        })

        cache.writeQuery({
          query: queries.getManuscriptPeerReviewChecks,
          variables: { manuscriptId },
          data: { checks: updatedPeerReviewChecks },
        })
      },
    })

  return {
    updateObservationForPeerReviewCheck: debounce(
      updateObservationForPeerReviewCheck,
      DEBOUNCE_TIME,
    ),
  }
}

export default useUpdateObservationForPeerReviewCheck
