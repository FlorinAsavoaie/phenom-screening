import { useMutation } from '@apollo/client'
import { debounce } from 'lodash'
import * as mutations from '../mutations'
import * as queries from '../queries'

const DEBOUNCE_TIME = 500

function useUpdateObservationForMaterialCheck() {
  const [mutation] = useMutation(mutations.updateObservationForMaterialCheck)

  const updateObservationForMaterialCheck = (input) =>
    mutation({
      variables: { input },
      update: (
        cache,
        {
          data: {
            materialCheck: { additionalInfo },
          },
        },
      ) => {
        const { manuscriptId, name } = input
        const { materialChecks } = cache.readQuery({
          query: queries.getManuscriptMaterialChecks,
          variables: { manuscriptId },
        })

        const updatedMaterialChecks = materialChecks.map((materialCheck) => {
          if (materialCheck.name === name) {
            return {
              ...materialCheck,
              additionalInfo,
            }
          }
          return materialCheck
        })

        cache.writeQuery({
          query: queries.getManuscriptMaterialChecks,
          variables: { manuscriptId },
          data: { materialChecks: updatedMaterialChecks },
        })
      },
    })

  return {
    updateObservationForMaterialCheck: debounce(
      updateObservationForMaterialCheck,
      DEBOUNCE_TIME,
    ),
  }
}

export default useUpdateObservationForMaterialCheck
