import { useMutation } from '@apollo/client'
import { requestFilesMutation } from '../mutations'

export function useRequestFiles() {
  const [mutation] = useMutation(requestFilesMutation)

  function requestFiles({ manuscriptId, additionalComments }) {
    return mutation({
      variables: { manuscriptId, content: additionalComments },
    })
  }

  return {
    requestFiles,
  }
}
