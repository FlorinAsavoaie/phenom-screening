import { useMutation, gql } from '@apollo/client'
import { ExecutionResult } from 'graphql'

const sendToPeerReviewMutation = gql`
  mutation sendToPeerReview(
    $manuscriptId: String!
    $isDecisionMadeInError: Boolean!
    $improperReviewEmails: [String]!
    $conflictOfInterest: ConflictOfInterest!
    $comment: String!
  ) {
    sendToPeerReview(
      manuscriptId: $manuscriptId
      isDecisionMadeInError: $isDecisionMadeInError
      improperReviewEmails: $improperReviewEmails
      conflictOfInterest: $conflictOfInterest
      comment: $comment
    )
  }
`

interface ConflictOfInterest {
  withAcademicEditor: boolean
  withTriageEditor: boolean
  withReviewerEmails: string[]
}

type MutationData = boolean
type MutationVariables = {
  manuscriptId: string
  isDecisionMadeInError: boolean
  conflictOfInterest: ConflictOfInterest
  improperReviewEmails: string[]
  comment: string
}

type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (args: MutationVariables) => MutationResult

function useSendToPeerReview(): HookResult {
  const [mutation] = useMutation<MutationData, MutationVariables>(
    sendToPeerReviewMutation,
    { refetchQueries: ['getScreeningManuscript'] },
  )

  return ({
    manuscriptId,
    isDecisionMadeInError,
    conflictOfInterest,
    improperReviewEmails,
    comment,
  }): MutationResult =>
    mutation({
      variables: {
        manuscriptId,
        isDecisionMadeInError,
        conflictOfInterest,
        improperReviewEmails,
        comment,
      },
    })
}

export { useSendToPeerReview }
