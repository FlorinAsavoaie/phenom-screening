import { useQuery } from '@apollo/client'
import { orderBy } from 'lodash'
import * as queries from '../queries'

function useManuscriptMaterialChecks({ manuscriptId, numberOfSections }) {
  const { data } = useQuery(queries.getManuscriptMaterialChecks, {
    variables: {
      manuscriptId,
    },
    fetchPolicy: 'cache-first',
  })

  return {
    sections: data ? groupBySection(data.materialChecks, numberOfSections) : [],
  }
}

export default useManuscriptMaterialChecks

function groupBySection(checks, numberOfSections) {
  const orderedChecks = orderBy(checks, [(item) => item.config.order], ['asc'])

  const sectionLength = Math.ceil(orderedChecks.length / numberOfSections)

  return orderedChecks.reduce((sections, check, index) => {
    const targetSectionIndex = Math.floor(index / sectionLength)

    if (index % Math.ceil(sectionLength) === 0) {
      sections.push({
        id: targetSectionIndex,
        materialChecks: [],
      })
    }

    sections[targetSectionIndex].materialChecks.push(check)

    return sections
  }, [])
}
