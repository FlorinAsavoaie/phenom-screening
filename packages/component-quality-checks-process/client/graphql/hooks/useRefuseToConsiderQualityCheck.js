import { useMutation } from '@apollo/client'
import { refuseToConsiderQualityCheckMutation } from '../mutations'

export function useRefuseToConsiderQualityCheck() {
  const [mutation] = useMutation(refuseToConsiderQualityCheckMutation)

  function refuseToConsiderQualityCheck({
    manuscriptId,
    additionalComments,
    rest,
  }) {
    return mutation({
      variables: { manuscriptId, note: additionalComments, content: rest },
    })
  }

  return {
    refuseToConsiderQualityCheck,
  }
}
