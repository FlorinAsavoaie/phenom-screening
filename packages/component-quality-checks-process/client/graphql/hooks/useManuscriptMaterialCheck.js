import { useQuery } from '@apollo/client'
import { orderBy } from 'lodash'
import * as queries from '../queries'

function useManuscriptMaterialChecks({ manuscriptId, numberOfSections }) {
  const { data } = useQuery(queries.getManuscriptMaterialChecks, {
    variables: {
      manuscriptId,
    },
    fetchPolicy: 'cache-first',
  })

  let sections = []

  if (data) {
    const { materialChecks: fetchedMaterialChecks } = data

    const materialChecks = orderBy(
      fetchedMaterialChecks,
      [(item) => item.config.order],
      ['asc'],
    )

    const sectionLength = materialChecks.length / numberOfSections

    sections = materialChecks.reduce((sections, materialCheck, index) => {
      const targetSectionIndex = Math.floor(index / sectionLength)
      if (index % Math.ceil(sectionLength) === 0) {
        sections.push({
          id: targetSectionIndex,
          materialChecks: [],
        })
      }
      sections[targetSectionIndex].materialChecks.push(materialCheck)
      return sections
    }, [])
  }

  return { sections }
}

export default useManuscriptMaterialChecks
