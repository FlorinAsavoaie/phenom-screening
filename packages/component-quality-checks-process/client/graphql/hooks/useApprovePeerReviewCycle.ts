import { useMutation, gql } from '@apollo/client'
import { ExecutionResult } from 'graphql'
import { screeningManuscript } from 'component-screening-process/client/graphql/fragments/screeningManuscriptFragment'

const approvePeerReviewCycleMutation = gql`
  mutation approvePeerReviewCycle($manuscriptId: String!) {
    manuscript: approvePeerReviewCycle(manuscriptId: $manuscriptId) {
      step
    }
  }
`

type MutationData = {
  manuscript: { step: string }
}
type FragmentData = any
type MutationVariables = { manuscriptId: string }
type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (args: MutationVariables) => MutationResult

function useApprovePeerReviewCycle(): HookResult {
  const [mutation] = useMutation<MutationData, MutationVariables>(
    approvePeerReviewCycleMutation,
  )

  return ({ manuscriptId }): MutationResult =>
    mutation({
      variables: { manuscriptId },
      update: (cache, { data }) => {
        if (!data) {
          return
        }

        const {
          manuscript: { step },
        } = data

        const manuscript = cache.readFragment<FragmentData>({
          id: `ScreeningManuscript:${manuscriptId}`,
          fragment: screeningManuscript,
          fragmentName: 'screeningManuscript',
        })

        cache.writeFragment<FragmentData>({
          id: `ScreeningManuscript:${manuscriptId}`,
          fragment: screeningManuscript,
          fragmentName: 'screeningManuscript',
          data: {
            ...manuscript,
            step,
          },
        })
      },
    })
}

export { useApprovePeerReviewCycle }
