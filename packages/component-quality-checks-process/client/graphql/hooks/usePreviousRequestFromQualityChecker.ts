import { useQuery, gql } from '@apollo/client'
import { get } from 'lodash'

const getPreviousRequestFromQualityChecker = gql`
  query getPreviousRequestFromQualityChecker($manuscriptId: ID!) {
    previousRequestFromQualityChecker: getPreviousRequestFromQualityChecker(
      manuscriptId: $manuscriptId
    ) {
      additionalComments {
        content
        created
      }
      checkerObservations {
        request
        observation
      }
      assignedChecker {
        surname
        givenNames
      }
    }
  }
`

function usePreviousRequestFromQualityChecker(manuscriptId: string) {
  const { data, loading } = useQuery(getPreviousRequestFromQualityChecker, {
    variables: {
      manuscriptId,
    },
    fetchPolicy: 'cache-first',
  })

  const previousRequestFromQualityChecker = get(
    data,
    'previousRequestFromQualityChecker',
    null,
  )

  return {
    previousRequestFromQualityChecker,
    loading,
  }
}

export { usePreviousRequestFromQualityChecker }
