import { useMutation } from '@apollo/client'
import { approveQualityCheckMutation } from '../mutations'

export function useApproveQualityCheck() {
  const [mutation] = useMutation(approveQualityCheckMutation)

  function approveQualityCheck({ manuscriptId, additionalComments }) {
    return mutation({
      variables: { manuscriptId, additionalComments },
    })
  }

  return {
    approveQualityCheck,
  }
}
