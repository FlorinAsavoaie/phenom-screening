import { useMutation } from '@apollo/client'
import { get } from 'lodash'
import * as mutations from '../mutations'
import * as queries from '../queries'

function useSelectOptionForMaterialCheck() {
  const [mutation] = useMutation(mutations.selectOptionForMaterialCheck)

  const selectOptionForMaterialCheck = (input) =>
    mutation({
      variables: { input },
      update: (cache, { data }) => {
        const { manuscriptId, name } = input
        const { materialChecks } = cache.readQuery({
          query: queries.getManuscriptMaterialChecks,
          variables: { manuscriptId },
        })
        const selectedOption = get(data.materialCheck, 'selectedOption', '')
        const additionalInfo = get(data.materialCheck, 'additionalInfo', '')

        const updatedMaterialChecks = materialChecks.map((materialCheck) => {
          if (materialCheck.name === name) {
            return {
              ...materialCheck,
              selectedOption,
              additionalInfo,
            }
          }
          return materialCheck
        })

        cache.writeQuery({
          query: queries.getManuscriptMaterialChecks,
          variables: { manuscriptId },
          data: { materialChecks: updatedMaterialChecks },
        })
      },
    })
  return {
    selectOptionForMaterialCheck,
  }
}

export default useSelectOptionForMaterialCheck
