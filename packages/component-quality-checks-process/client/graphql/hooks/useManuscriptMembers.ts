import { useQuery, gql } from '@apollo/client'
import { get } from 'lodash'
import { useDebounce } from 'hindawi-shared/client/graphql/hooks'

const getManuscriptMembers = gql`
  query getManuscriptMembers($manuscriptId: ID!) {
    reviewers: getManuscriptReviewers(manuscriptId: $manuscriptId) {
      id
      name
      email
    }
    triageEditor: getManuscriptTriageEditor(manuscriptId: $manuscriptId) {
      id
      name
      email
    }
    academicEditor: getManuscriptAcademicEditor(manuscriptId: $manuscriptId) {
      id
      name
      email
    }
  }
`

export interface Member {
  id: string
  name: string
  email: string
}

function useManuscriptMembers(manuscriptId: string): {
  reviewers: Member[]
  loading: boolean
  triageEditor: Member | null
  academicEditor: Member | null
} {
  const { data, loading } = useQuery(getManuscriptMembers, {
    variables: {
      manuscriptId,
    },
    fetchPolicy: 'cache-first',
  })

  const reviewers = get(data, 'reviewers', [])
  const triageEditor = get(data, 'triageEditor', null)
  const academicEditor = get(data, 'academicEditor', null)
  const debouncedLoading = useDebounce({ value: loading, delay: 200 })

  return {
    reviewers,
    triageEditor,
    academicEditor,
    loading: debouncedLoading,
  }
}

export { useManuscriptMembers }
