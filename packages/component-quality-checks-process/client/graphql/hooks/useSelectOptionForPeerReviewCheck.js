import { useMutation } from '@apollo/client'
import { get } from 'lodash'
import * as mutations from '../mutations'
import * as queries from '../queries'

function useSelectOptionForPeerReviewCheck() {
  const [mutation] = useMutation(mutations.selectOptionForPeerReviewCheck)

  const selectOptionForPeerReviewCheck = (input) =>
    mutation({
      variables: { input },
      update: (cache, { data }) => {
        const { manuscriptId, name } = input
        const { checks } = cache.readQuery({
          query: queries.getManuscriptPeerReviewChecks,
          variables: { manuscriptId },
        })
        const selectedOption = get(data.peerReviewCheck, 'selectedOption', '')
        const observation = get(data.peerReviewCheck, 'observation', '')

        const updatedChecks = checks.map((check) => {
          if (check.name === name) {
            return {
              ...check,
              selectedOption,
              observation,
            }
          }
          return check
        })

        cache.writeQuery({
          query: queries.getManuscriptPeerReviewChecks,
          variables: { manuscriptId },
          data: { checks: updatedChecks },
        })
      },
    })
  return {
    selectOptionForPeerReviewCheck,
  }
}

export default useSelectOptionForPeerReviewCheck
