import { useFetching } from '@hindawi/ui'
import { useSignedUrl } from 'component-screening-process/client/hooks/useSignedUrl'
import { ManuscriptFile } from 'hindawi-shared/SchemaInterfaces'

import {
  createAnchorElement,
  removeAnchorElement,
} from '../../components/manuscript-files/fileUtils'

interface DownloadFileHookResult {
  downloadFile: () => void
  isFetching: boolean
  fetchingError: string
}
export const useFileDownload = (
  file: ManuscriptFile,
): DownloadFileHookResult => {
  const { isFetching, setFetching, fetchingError, setError } = useFetching()
  const { getSignedUrl } = useSignedUrl()

  const downloadFile = (): void => {
    setFetching(true)
    getSignedUrl({ fileId: file.id, format: 'original' })
      .then(fetch)
      .then((response) => response.blob())
      .then((blob) => {
        setFetching(false)
        const { a, url } = createAnchorElement(blob, file.fileName)
        a.click()
        removeAnchorElement(a, url)
      })
      .catch(setError)
  }

  return { downloadFile, isFetching, fetchingError }
}
