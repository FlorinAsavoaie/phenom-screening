import { useQuery } from '@apollo/client'
import { get } from 'lodash'
import { getManuscriptPeerReviewDetails } from '../queries'

export function useManuscriptPeerReviewDetails({ manuscriptId }) {
  const { data, loading } = useQuery(getManuscriptPeerReviewDetails, {
    variables: { manuscriptId },
  })

  const peerReviewDetails = get(data, 'getScreeningManuscript', null)

  return {
    peerReviewDetails,
    loading,
  }
}
