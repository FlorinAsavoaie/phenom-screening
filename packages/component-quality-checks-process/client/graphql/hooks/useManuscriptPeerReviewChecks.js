import { useQuery } from '@apollo/client'
import { orderBy } from 'lodash'
import * as queries from '../queries'

function useManuscriptPeerReviewChecks({ manuscriptId, numberOfSections }) {
  const { data } = useQuery(queries.getManuscriptPeerReviewChecks, {
    variables: {
      manuscriptId,
    },
    fetchPolicy: 'cache-first',
  })

  let sections = []

  if (data) {
    const { checks: fetchedChecks } = data
    const checks = orderBy(
      fetchedChecks,
      [(item) => item.config.order],
      ['asc'],
    )
    const sectionLength = Math.ceil(checks.length / numberOfSections)

    sections = checks.reduce((sections, peerReviewCheck, index) => {
      const targetSectionIndex = Math.floor(index / sectionLength)
      if (index % sectionLength === 0) {
        sections.push({
          id: targetSectionIndex,
          peerReviewChecks: [],
        })
      }
      sections[targetSectionIndex].peerReviewChecks.push(peerReviewCheck)
      return sections
    }, [])
  }

  return { sections }
}

export default useManuscriptPeerReviewChecks
