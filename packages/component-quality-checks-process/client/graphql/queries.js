import { gql } from '@apollo/client'

export const getManuscriptMaterialChecks = gql`
  query getManuscriptMaterialChecks($manuscriptId: ID!) {
    materialChecks: getManuscriptMaterialChecks(manuscriptId: $manuscriptId) {
      name
      selectedOption
      additionalInfo
      config {
        options {
          value
          supportsAdditionalInfo
          label
          type
        }
        order
      }
    }
  }
`
export const getNotesForManuscript = gql`
  query getNotesForManuscript($manuscriptId: ID!) {
    getNotesForManuscript(manuscriptId: $manuscriptId) {
      id
      type
      content
    }
  }
`
export const getManuscriptPeerReviewChecks = gql`
  query getManuscriptPeerReviewChecks($manuscriptId: ID!) {
    checks: getManuscriptPeerReviewChecks(manuscriptId: $manuscriptId) {
      name
      selectedOption
      observation
      config {
        options {
          value
          withObservation
        }
        order
      }
    }
  }
`
export const getManuscriptPeerReviewDetails = gql`
  query getScreeningManuscript($manuscriptId: ID!) {
    getScreeningManuscript(manuscriptId: $manuscriptId) {
      id
      title
      articleType
      submissionDate
      abstract
      status
      step
      conflictOfInterest
      dataAvailability
      fundingStatement
      specialIssue {
        id
      }
      journalSection {
        id
      }
      files {
        id
        type
        providerKey
        originalFileProviderKey
        originalName
        fileName
        size
        mimeType
      }
      journal {
        id
        name
      }
      authors {
        id
        isSubmitting
        isVerifiedOutOfTool
        aff
        email
        givenNames
        surname
        isVerified
        isOnBadDebtList
        isOnBlackList
        isOnWatchList
        reviews {
          id
          recommendation
          publicComment
          privateComment
          submitted
          file {
            id
            type
            providerKey
            originalFileProviderKey
            originalName
            fileName
            size
            mimeType
          }
        }
      }
      members {
        id
        surname
        givenNames
        email
        reviewerNumber
        roleLabel
        role
        reviews {
          id
          recommendation
          publicComment
          privateComment
          submitted
          isValid
          file {
            id
            type
            providerKey
            originalFileProviderKey
            originalName
            fileName
            size
            mimeType
          }
        }
      }
    }
  }
`
