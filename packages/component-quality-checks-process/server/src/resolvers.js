const config = require('config')
const models = require('@pubsweet/models')
const logger = require('@pubsweet/logger')
const EmailTemplate = require('@pubsweet/component-email-templating')
const { withAuthsome } = require('hindawi-utils')
const { mappers, factories } = require('component-model')
const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')
const screeningUseCases = require('component-screening-process/server/src/use-cases')
const {
  emailService,
  notificationService: notifications,
} = require('component-screening-process/server/src/services')
const pubSubService = require('component-screening-process/server/src/services/pubSub')

const useCases = require('./use-cases')
const qualityCheckNotifications = require('./services/notificationService/notifications')

function getPublishAuthorConfirmedUseCase() {
  return screeningUseCases.publishAuthorConfirmedUseCase.initialize({
    models,
    services: {
      applicationEventBus,
    },
  })
}

const notificationService = qualityCheckNotifications.initialize({
  Email: EmailTemplate,
  emailService: emailService.initialize({
    EmailTemplate,
    configService: config,
  }),
  configService: config,
})

const isShortArticleSupportedUseCase = isShortArticleSupported.initialize({
  configService: config,
})

const resolvers = {
  Mutation: {
    async selectOptionForMaterialCheck(
      _,
      { input: { manuscriptId, name, selectedOption } },
      { user: userId },
    ) {
      return useCases.selectOptionForMaterialCheckUseCase
        .initialize(models)
        .execute({ manuscriptId, name, selectedOption, userId })
    },
    async updateObservationForMaterialCheck(
      _,
      { input: { manuscriptId, name, additionalInfo } },
      { user: userId },
    ) {
      return useCases.updateObservationForMaterialCheckUseCase
        .initialize(models)
        .execute({ manuscriptId, name, additionalInfo, userId })
    },
    async selectOptionForPeerReviewCheck(
      _,
      { input: { manuscriptId, name, selectedOption } },
      { user: userId },
    ) {
      return useCases.selectOptionForPeerReviewCheckUseCase
        .initialize(models)
        .execute({ manuscriptId, name, selectedOption, userId })
    },
    async updateObservationForPeerReviewCheck(
      _,
      { input: { manuscriptId, name, observation } },
      { user: userId },
    ) {
      return useCases.updateObservationForPeerReviewCheckUseCase
        .initialize(models)
        .execute({ manuscriptId, name, observation, userId })
    },
    async approveQualityCheck(
      _,
      { manuscriptId, additionalComments },
      { user: userId },
    ) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      await useCases.approveQualityCheckUseCase
        .initialize({
          models,
          mappers,
          useCases: {
            publishAuthorConfirmedUseCase: getPublishAuthorConfirmedUseCase(),
            isShortArticleSupportedUseCase,
          },
          services: {
            notificationService,
            pubSub,
            logger,
            configService: config,
          },
        })
        .execute({ manuscriptId, additionalComments, userId })
    },
    async approvePeerReviewCycle(_, { manuscriptId }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.approvePeerReviewCycleUseCase
        .initialize({
          models,
          mappers,
          services: {
            pubSub,
          },
        })
        .execute({ manuscriptId, userId })
    },
    async refuseToConsiderQualityCheck(
      _,
      { manuscriptId, content, note },
      { user: userId },
    ) {
      const notificationService = notifications.initialize({
        services: {
          emailService: emailService.initialize({
            EmailTemplate,
            configService: config,
          }),
          configService: config,
        },
      })
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.refuseToConsiderQualityCheckUseCase
        .initialize({
          models,
          mappers,
          factories,
          useCases: {
            publishAuthorConfirmedUseCase: getPublishAuthorConfirmedUseCase(),
            isShortArticleSupportedUseCase,
          },
          services: {
            notificationService,
            pubSub,
          },
        })
        .execute({ manuscriptId, content, note, userId })
    },
    async requestFiles(_, { manuscriptId, content }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      const getRequestFromQualityCheckerUseCase =
        useCases.getRequestFromQualityCheckerUseCase.initialize({
          models,
          useCases: { isShortArticleSupportedUseCase },
        })

      return useCases.requestFilesUseCase
        .initialize({
          models,
          mappers,
          useCases: {
            publishAuthorConfirmedUseCase: getPublishAuthorConfirmedUseCase(),
            getRequestFromQualityCheckerUseCase,
          },
          services: {
            notificationService,
            pubSub,
            configService: config,
          },
        })
        .execute({ manuscriptId, content, userId })
    },
    async sendToPeerReview(
      _,
      {
        manuscriptId,
        isDecisionMadeInError,
        improperReviewEmails,
        conflictOfInterest,
        comment,
      },
      { user: userId },
    ) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.sendToPeerReviewUseCase
        .initialize({
          models,
          factories,
          mappers,
          services: { pubSub, applicationEventBus },
        })
        .execute({
          manuscriptId,
          isDecisionMadeInError,
          improperReviewEmails,
          conflictOfInterest,
          comment,
          userId,
        })
    },
  },
  Query: {
    async getPreviousRequestFromQualityChecker(_, { manuscriptId }) {
      const getRequestFromQualityChecker =
        useCases.getRequestFromQualityCheckerUseCase.initialize({
          models,
          useCases: { isShortArticleSupportedUseCase },
        })
      return useCases.getPreviousRequestFromQualityCheckerUseCase
        .initialize({
          models,
          useCases: { getRequestFromQualityChecker },
        })
        .execute(manuscriptId)
    },
  },
}

module.exports = withAuthsome(resolvers, useCases)
