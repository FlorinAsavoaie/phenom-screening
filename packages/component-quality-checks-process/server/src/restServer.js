const models = require('@pubsweet/models')

const { s3Service } = require('hindawi-shared/server/src/services')
const handlers = require('./handlers')

const restServer = () => (app) => {
  const mw = app.locals.passport.authenticate(['bearer', 'keycloakBearer'], {
    session: false,
  })
  app.get(
    '/files/zip/:manuscriptId',
    mw,
    handlers.zipFiles({ s3Service, models }),
  )
}

module.exports = restServer
