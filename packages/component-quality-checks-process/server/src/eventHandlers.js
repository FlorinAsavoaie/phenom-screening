const config = require('config')
const models = require('@pubsweet/models')
const { factories } = require('component-model')
const {
  useCases: manuscriptAuthorsUseCases,
} = require('component-model/src/manuscriptAuthors')
const logger = require('@pubsweet/logger')
const { transaction } = require('objection')
const { s3Service } = require('hindawi-shared/server/src/services')
const { getConfig } = require('hindawi-screening/config/publisher')
const { getParsedTransactionModels } = require('hindawi-utils/utils')
const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')

const useCases = require('./use-cases')

const publisherConfig = getConfig()
const {
  File,
  Member,
  Journal,
  Manuscript,
  ManuscriptAuthors,
  MaterialCheckConfig,
  PeerReviewCheckConfig,
  ManuscriptMaterialCheck,
  ManuscriptPeerReviewCheck,
} = models

function getInitializeMaterialChecksUseCase(models) {
  return useCases.initializeMaterialChecksUseCase.initialize({
    models,
    factories,
  })
}

function getInitializePeerReviewChecksUseCase(models) {
  return useCases.initializePeerReviewChecksUseCase.initialize({
    models,
    factories,
  })
}

function getInitializeIngestManuscriptAuthorsEmailsUseCase(models) {
  return manuscriptAuthorsUseCases.ingestManuscriptAuthorsEmailsUseCase.initialize(
    {
      models,
      factories,
    },
  )
}

function getInitializeDeleteManuscriptAuthorsEmailsUseCase(models) {
  return manuscriptAuthorsUseCases.deleteManuscriptAuthorsEmailsUseCase.initialize(
    {
      models,
      factories,
    },
  )
}

async function ingestArticleTransaction(data) {
  return transaction(
    File,
    Member,
    Journal,
    Manuscript,
    ManuscriptAuthors,
    MaterialCheckConfig,
    PeerReviewCheckConfig,
    ManuscriptMaterialCheck,
    ManuscriptPeerReviewCheck,
    (...models) => {
      const transactionModels = getParsedTransactionModels(models)

      return useCases.ingestAcceptedManuscriptUseCase
        .initialize({
          models: transactionModels,
          factories,
          s3Service,
          logger,
          useCases: {
            initializeMaterialChecksUseCase:
              getInitializeMaterialChecksUseCase(transactionModels),
            initializePeerReviewChecksUseCase:
              getInitializePeerReviewChecksUseCase(transactionModels),
            ingestManuscriptAuthorsEmailsUseCase:
              getInitializeIngestManuscriptAuthorsEmailsUseCase(
                transactionModels,
              ),
            deleteManuscriptAuthorsEmailsUseCase:
              getInitializeDeleteManuscriptAuthorsEmailsUseCase(
                transactionModels,
              ),
          },
        })
        .execute(data)
    },
  )
}

async function ingestShortArticleTransaction(data) {
  return transaction(
    Member,
    Manuscript,
    ManuscriptAuthors,
    MaterialCheckConfig,
    ManuscriptMaterialCheck,
    (...models) => {
      const transactionModels = getParsedTransactionModels(models)

      return useCases.ingestShortArticle
        .initialize({
          models: transactionModels,
          factories,
          s3Service,
          configService: config,
          useCases: {
            initializeMaterialChecksUseCase:
              getInitializeMaterialChecksUseCase(transactionModels),
            isShortArticleSupportedUseCase,
            ingestManuscriptAuthorsEmailsUseCase:
              getInitializeIngestManuscriptAuthorsEmailsUseCase(
                transactionModels,
              ),
          },
        })
        .execute(data)
    },
  )
}

const isShortArticleSupportedUseCase = isShortArticleSupported.initialize({
  configService: config,
})

module.exports = {
  async SubmissionQualityChecksSubmitted(data) {
    await transaction(
      Member,
      Manuscript,
      ManuscriptAuthors,
      MaterialCheckConfig,
      ManuscriptMaterialCheck,
      async (...models) => {
        const transactionModels = getParsedTransactionModels(models)

        return useCases.ingestSubmittedQCManuscriptUseCase
          .initialize({
            models: transactionModels,
            factories,
            s3Service,
            useCases: {
              initializeMaterialChecksUseCase:
                getInitializeMaterialChecksUseCase(transactionModels),
              isShortArticleSupportedUseCase,
              ingestManuscriptAuthorsEmailsUseCase:
                getInitializeIngestManuscriptAuthorsEmailsUseCase(
                  transactionModels,
                ),
              deleteManuscriptAuthorsEmailsUseCase:
                getInitializeDeleteManuscriptAuthorsEmailsUseCase(
                  transactionModels,
                ),
            },
          })
          .execute(data)
      },
    )
  },
  async TriggeredQualityChecks(data) {
    const isShortArticle = publisherConfig.shortArticleTypes.includes(
      data.manuscripts[0]?.articleType?.name,
    )

    return isShortArticle
      ? ingestShortArticleTransaction(data)
      : ingestArticleTransaction(data)
  },
}
