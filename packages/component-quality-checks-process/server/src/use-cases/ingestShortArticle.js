const { Promise } = require('bluebird')

function initialize({
  models: { Manuscript, Member },
  factories: { authorFactory, fileFactory, manuscriptFactory },
  useCases: {
    initializeMaterialChecksUseCase,
    isShortArticleSupportedUseCase,
    ingestManuscriptAuthorsEmailsUseCase,
  },
  s3Service,
}) {
  async function execute({ submissionId, manuscripts }) {
    const submittedManuscript = manuscripts[0]
    const submittingStaffMember = submittedManuscript.submittingStaffMembers[0]

    const isShortArticleSupported = isShortArticleSupportedUseCase.execute(
      submittedManuscript.articleType.name,
    )

    if (!isShortArticleSupported) {
      throw new Error(
        `Article type ${submittedManuscript.articleType.name} is not supported by ingestShortArticle use-case  `,
      )
    }

    const authors = getAuthors(submittedManuscript.authors)
    const files = await getFiles(submittedManuscript.files)

    let manuscript = manuscriptFactory.createManuscript({
      submissionId,
      manuscript: submittedManuscript,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors,
      files,
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      journalId: submittedManuscript.journalId,
    })

    manuscript = await Manuscript.upsertGraph(manuscript)

    await ingestManuscriptAuthorsEmailsUseCase.execute({
      manuscriptAuthorsList: manuscript.authors,
      manuscriptId: manuscript.id,
      manuscriptTitle: manuscript.title,
    })

    const member = new Member({
      manuscriptId: manuscript.id,
      surname: submittingStaffMember.surname,
      givenNames: submittingStaffMember.givenNames,
      email: submittingStaffMember.email,
      aff: submittingStaffMember.aff,
      country: submittingStaffMember.country,
      title: submittingStaffMember.title,
      status: submittingStaffMember.status,
      role: Member.Roles.SUBMITTING_STAFF_MEMBER,
      roleLabel: 'Submitting Staff Member',
      phenomUserId: submittingStaffMember.userId,
    })

    const insertedSubmittingStaffMember = await Member.insertGraph(member)

    console.info(`Saved member with id: ${insertedSubmittingStaffMember.id}`)

    await initializeMaterialChecksUseCase.execute(manuscript.id)

    const manuscriptFile = manuscript.getManuscriptFile()
    if (!manuscriptFile) return

    await applicationEventBus.publishMessage({
      event: 'ManuscriptIngested',
      data: {
        authors: manuscript.authors,
        fileId: manuscriptFile.id,
        manuscriptId: manuscript.id,
        originalFileProviderKey: manuscriptFile.originalFileProviderKey,
      },
    })
  }

  function getAuthors(authors) {
    return authors.map((a) => authorFactory.createAuthor(a))
  }

  async function getFiles(files) {
    return Promise.map(files, async (file) => {
      await s3Service.uploadReviewFileToScreeningS3(file.providerKey)
      return fileFactory.createFile({
        file,
      })
    })
  }

  return { execute }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
