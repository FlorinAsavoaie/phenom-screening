function initialize({
  models: { Manuscript, ManuscriptPeerReviewCheck, PeerReviewCheckConfig },
  factories: { manuscriptPeerReviewCheckFactory },
}) {
  return {
    execute,
  }

  async function execute(manuscriptId) {
    const { config, id: peerReviewCheckConfigId } =
      await PeerReviewCheckConfig.getLatest()

    await Manuscript.updateBy({
      queryObject: {
        id: manuscriptId,
      },
      propertiesToUpdate: {
        peerReviewCheckConfigId,
      },
    })

    const peerReviewChecks = Object.keys(config).map((name) =>
      manuscriptPeerReviewCheckFactory.createInitial({
        manuscriptId,
        name,
      }),
    )

    await ManuscriptPeerReviewCheck.insertGraph(peerReviewChecks)
  }
}

module.exports = {
  initialize,
}
