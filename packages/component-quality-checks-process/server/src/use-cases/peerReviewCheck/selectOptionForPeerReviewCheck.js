const { ValidationError } = require('@pubsweet/errors')

function initialize({
  ManuscriptPeerReviewCheck,
  Manuscript,
  PeerReviewCheckConfig,
  User,
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, name, selectedOption, userId }) {
    await assertManuscriptIsInProgressAndInPeerReviewCycleChecking({
      manuscriptId,
      name,
      userId,
    })
    await assertSelectedOptionIsValid({ manuscriptId, name, selectedOption })

    return updateSelectedOption({ manuscriptId, name, selectedOption })
  }

  async function assertManuscriptIsInProgressAndInPeerReviewCycleChecking({
    manuscriptId,
    name,
    userId,
  }) {
    const manuscript = await Manuscript.find(manuscriptId)
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot select peer review option for ${name} on ${manuscript.status} status for manuscript ${manuscript.id}.`,
      )
    }

    if (manuscript.step !== Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING) {
      throw new ValidationError(
        `You cannot select peer review option for ${name} on ${manuscript.step} step for manuscript ${manuscript.id}.`,
      )
    }
  }

  async function assertSelectedOptionIsValid({
    manuscriptId,
    name,
    selectedOption,
  }) {
    const isSelectedOptionAvailable =
      await PeerReviewCheckConfig.isSelectedOptionAvailable({
        manuscriptId,
        name,
        selectedOption,
      })

    if (!isSelectedOptionAvailable) {
      throw new ValidationError(
        `Selected option, ${selectedOption}, is not valid for ${name} on manuscript ${manuscriptId}`,
      )
    }
  }

  async function updateSelectedOption({ manuscriptId, name, selectedOption }) {
    const manuscriptPeerReviewCheck = await ManuscriptPeerReviewCheck.findOneBy(
      {
        queryObject: { manuscriptId, name },
      },
    )
    const isObservationSupported =
      await PeerReviewCheckConfig.isObservationSupported({
        manuscriptId,
        name,
        selectedOption,
      })
    if (!isObservationSupported) {
      manuscriptPeerReviewCheck.observation = ''
    }
    manuscriptPeerReviewCheck.selectedOption = selectedOption
    return manuscriptPeerReviewCheck.save()
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
