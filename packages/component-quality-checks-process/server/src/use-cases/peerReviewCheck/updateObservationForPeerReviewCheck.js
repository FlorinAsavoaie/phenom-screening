const { ValidationError } = require('@pubsweet/errors')

function initialize({
  ManuscriptPeerReviewCheck,
  PeerReviewCheckConfig,
  Manuscript,
  User,
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, name, observation, userId }) {
    const manuscriptPeerReviewCheck = await ManuscriptPeerReviewCheck.findOneBy(
      {
        queryObject: { manuscriptId, name },
      },
    )
    await assertManuscriptIsInProgress({ manuscriptId, name, userId })
    await assertObservationIsSupported({
      manuscriptId,
      name,
      selectedOption: manuscriptPeerReviewCheck.selectedOption,
    })

    manuscriptPeerReviewCheck.observation = observation

    return manuscriptPeerReviewCheck.save()
  }

  async function assertManuscriptIsInProgress({ manuscriptId, name, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot select an option for ${name} on ${manuscript.status} status for manuscript ${manuscript.id}.`,
      )
    }

    if (manuscript.step !== Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING) {
      throw new ValidationError(
        `You cannot select peer review option for ${name} on ${manuscript.step} step for manuscript ${manuscript.id}.`,
      )
    }
  }

  async function assertObservationIsSupported({
    manuscriptId,
    name,
    selectedOption,
  }) {
    const isObservationSupported =
      await PeerReviewCheckConfig.isObservationSupported({
        manuscriptId,
        name,
        selectedOption,
      })

    if (!isObservationSupported) {
      throw new ValidationError(
        `Observation not supported for ${selectedOption} option for question ${name} on manuscript ${manuscriptId}.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
