function initialize({
  models: { Manuscript, Note },
  mappers: { manuscriptMapper },
  useCases: {
    publishAuthorConfirmedUseCase,
    getRequestFromQualityCheckerUseCase,
  },
  services: { notificationService, pubSub },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[manuscriptValidations, manuscriptMaterialChecks, authors, materialCheckConfig]',
    )
    assertFilesCanBeRequestedForManuscript(manuscript)

    manuscript.updateProperties({
      status: Manuscript.Statuses.FILES_REQUESTED,
    })

    await manuscript.save()

    const note = new Note({
      manuscriptId,
      type: Note.Types.DECISION_NOTE,
      content,
    })

    await note.save()

    await publishAuthorConfirmedUseCase.execute(manuscript.id)

    await notifyAuthor(manuscript.id, manuscript.articleType)

    await publishEvent(manuscript)

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)
  }

  function assertFilesCanBeRequestedForManuscript(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot request files for manuscript: ${manuscript.id} on status: ${manuscript.status}.`,
      )
    }
    if (manuscript.step !== Manuscript.Steps.IN_MATERIAL_CHECKING) {
      throw new ValidationError(
        `You cannot request files for manuscript: ${manuscript.id} on step: ${manuscript.step}.`,
      )
    }
  }

  async function notifyAuthor(manuscriptId, articleType) {
    const { person, manuscript, checkerObservations, additionalComments } =
      await getRequestFromQualityCheckerUseCase.execute(
        manuscriptId,
        articleType,
      )

    await notificationService.notifyAuthorWhenFilesAreRequested({
      person,
      manuscript,
      checkerObservations,
      additionalComments,
    })
  }

  async function publishEvent(manuscript) {
    const { submissionId } = manuscript
    const manuscripts = await Manuscript.findBy({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: '[authors, files, members]',
    })

    const mappedManuscripts = manuscripts.map(
      manuscriptMapper.domainToEventModel,
    )
    await applicationEventBus.publishMessage({
      event: 'SubmissionQualityCheckFilesRequested',
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: mappedManuscripts,
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
