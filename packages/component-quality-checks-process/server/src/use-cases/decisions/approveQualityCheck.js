const { get } = require('lodash')

function initialize({
  models: { Manuscript, ManuscriptMaterialCheck, Member, User },
  mappers: { manuscriptMapper },
  useCases: { publishAuthorConfirmedUseCase, isShortArticleSupportedUseCase },
  services: { notificationService, pubSub, logger },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, additionalComments, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, '[team.users, user]')

    assertManuscriptCanBeApproved(manuscript)

    manuscript.updateProperties({
      status: Manuscript.Statuses.QUALITY_CHECK_APPROVED,
    })
    await manuscript.save()

    await publishAuthorConfirmedUseCase.execute(manuscript.id)

    const isShortArticleSupported = isShortArticleSupportedUseCase.execute(
      manuscript.articleType,
    )

    let editorialAssistant = null
    if (!isShortArticleSupported) {
      editorialAssistant = await Member.getEditorialAssistant(manuscriptId)
      await notifyEditorialAssistant({
        manuscript,
        editorialAssistant,
        additionalComments,
      })
    }
    const extraData = await getExtraData(manuscript, editorialAssistant)

    await publishEvent(manuscript, extraData)

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)
  }

  function assertManuscriptCanBeApproved(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot approve manuscript: ${manuscript.id} on status: ${manuscript.status}.`,
      )
    }
    if (manuscript.step !== Manuscript.Steps.IN_MATERIAL_CHECKING) {
      throw new ValidationError(
        `You cannot approve manuscript: ${manuscript.id} on step: ${manuscript.step}.`,
      )
    }
  }

  async function getExtraData(manuscript, editorialAssistant) {
    const screenersContactData = await getScreenersContactData(
      manuscript.submissionId,
    )
    const qcContactData = await getQcContactData(manuscript)
    const eaContactData = {
      ea: editorialAssistant
        ? {
            surname: editorialAssistant.surname,
            givenNames: editorialAssistant.givenNames,
            email: editorialAssistant.email,
          }
        : null,
    }

    const figureFilesCount = await getManuscriptFigureFiles(manuscript.id)

    return {
      date: new Date(),
      figureFilesCount,
      refCount: '000',
      pageCount: '000',
      ...screenersContactData,
      ...qcContactData,
      ...eaContactData,
    }
  }

  async function getManuscriptFigureFiles(manuscriptId) {
    const manuscriptFigureFiles = await ManuscriptMaterialCheck.findOneBy({
      queryObject: {
        manuscriptId,
        name: 'manuscriptFigureFiles',
      },
    })

    if (!manuscriptFigureFiles || !manuscriptFigureFiles.additionalInfo) {
      return 0
    }

    return parseInt(manuscriptFigureFiles.additionalInfo, 10)
  }

  async function getScreenersContactData(submissionId) {
    const manuscript = await Manuscript.findOneBy({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.SCREENING,
        status: Manuscript.Statuses.SCREENING_APPROVED,
      },
      eagerLoadRelations: '[team.users, user]',
    })

    if (!manuscript) {
      logger.warn(
        `Submission with id ${submissionId} doesn't have a screening counterpart in Screening System`,
      )
      return {
        es: null,
        eaLeaders: [],
      }
    }

    return {
      es: getChecker(manuscript),
      esLeaders: getCheckerTeamLeaders(manuscript),
    }
  }

  async function getQcContactData(manuscript) {
    return {
      qc: getChecker(manuscript),
      qcLeaders: getCheckerTeamLeaders(manuscript),
    }
  }

  function getChecker(manuscript) {
    return manuscript.user
      ? {
          surname: manuscript.user.surname,
          givenNames: manuscript.user.givenNames,
          email: manuscript.user.email,
        }
      : null
  }

  function getCheckerTeamLeaders(manuscript) {
    return get(manuscript, 'team.users', [])
      .filter((user) => user.role === User.Roles.TEAM_LEADER)
      .map((user) => ({
        surname: user.surname,
        givenNames: user.givenNames,
        email: user.email,
      }))
  }

  async function notifyEditorialAssistant({
    manuscript,
    editorialAssistant,
    additionalComments,
  }) {
    await notificationService.notifyEAWhenSubmissionPassedQualityChecks({
      checker: manuscript.user,
      manuscript,
      editorialAssistant,
      additionalComments,
    })
  }

  async function publishEvent(manuscript, extraData) {
    const { submissionId } = manuscript

    const manuscripts = await Manuscript.findBy({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: '[authors, files, members]',
    })

    const mappedManuscripts = manuscripts.map(
      manuscriptMapper.domainToEventModel,
    )

    await applicationEventBus.publishMessage({
      event: 'SubmissionQualityCheckPassed',
      data: {
        submissionId,
        manuscripts: mappedManuscripts,
        ...extraData,
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
