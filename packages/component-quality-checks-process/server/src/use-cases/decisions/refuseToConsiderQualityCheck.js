const { pickBy } = require('lodash')
const { ValidationError } = require('@pubsweet/errors')

function initialize({
  models: { Manuscript, Author, User, Member },
  factories: { noteFactory },
  mappers: { manuscriptMapper },
  useCases: { publishAuthorConfirmedUseCase, isShortArticleSupportedUseCase },
  services: { notificationService, pubSub },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, content, note, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, '[authors, journal]')
    const isShortArticleSupported = isShortArticleSupportedUseCase.execute(
      manuscript.articleType,
    )

    assertManuscriptCanBeRTC(manuscript, isShortArticleSupported)

    manuscript.updateProperties({
      status: Manuscript.Statuses.QUALITY_CHECK_REJECTED,
    })
    await manuscript.save()

    const newNote = noteFactory.createDecisionNote({
      manuscriptId,
      content: note,
    })
    await newNote.save()

    await publishAuthorConfirmedUseCase.execute(manuscript.id)

    const editorialAssistant = !isShortArticleSupported
      ? await Member.getEditorialAssistant(manuscript.id)
      : {}

    await notifyAuthorOnEmail({
      manuscript,
      content,
      note,
      userId,
      editorialAssistant,
      isShortArticleSupported,
    })

    await publishEvent(manuscript)

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)
  }

  function assertManuscriptCanBeRTC(manuscript, isShortArticleSupported) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot refuse manuscript: ${manuscript.id} on status: ${manuscript.status}.`,
      )
    }
    if (
      manuscript.step !== Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING &&
      !isShortArticleSupported
    ) {
      throw new ValidationError(
        `You cannot refuse manuscript: ${manuscript.id} on step: ${manuscript.step}.`,
      )
    }
  }

  async function notifyAuthorOnEmail({
    manuscript,
    content,
    note,
    userId,
    editorialAssistant,
    isShortArticleSupported,
  }) {
    const observations = pickBy(content, (item) => item)

    // the email recipient for RTC is the corresponding author or the submitting staff member for short article types
    const emailRecipient = isShortArticleSupported
      ? await Member.getSubmittingStaffMember(manuscript.id)
      : await Author.getCorrespondingAuthor(manuscript.id)

    const currentUser = await User.find(userId)

    await notificationService.notifyThatManuscriptWasRefused({
      additionalComments: note,
      emailRecipient,
      editorialAssistant,
      currentUser,
      manuscript,
      observations: Object.keys(observations),
    })
  }

  async function publishEvent(manuscript) {
    const { submissionId } = manuscript
    const manuscripts = await Manuscript.findBy({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: '[authors, files, members]',
    })
    const mappedManuscripts = manuscripts.map(
      manuscriptMapper.domainToEventModel,
    )
    await applicationEventBus.publishMessage({
      event: 'SubmissionQualityCheckRTCd',
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: mappedManuscripts,
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
