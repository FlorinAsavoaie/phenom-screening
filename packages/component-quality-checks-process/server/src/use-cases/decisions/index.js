const requestFilesUseCase = require('./requestFiles')
const sendToPeerReviewUseCase = require('./sendToPeerReview')
const approveQualityCheckUseCase = require('./approveQualityCheck')
const approvePeerReviewCycleUseCase = require('./approvePeerReviewCycle')
const refuseToConsiderQualityCheckUseCase = require('./refuseToConsiderQualityCheck')

module.exports = {
  requestFilesUseCase,
  sendToPeerReviewUseCase,
  approveQualityCheckUseCase,
  approvePeerReviewCycleUseCase,
  refuseToConsiderQualityCheckUseCase,
}
