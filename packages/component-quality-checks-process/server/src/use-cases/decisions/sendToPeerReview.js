function initialize({
  models: { Manuscript, Note, Member },
  factories: { noteFactory },
  mappers: { manuscriptMapper },
  services: { pubSub, applicationEventBus },
}) {
  return {
    execute,
  }

  async function execute({
    manuscriptId,
    conflictOfInterest,
    improperReviewEmails,
    isDecisionMadeInError,
    comment,
    userId,
  }) {
    assertInputItsValid({
      conflictOfInterest,
      improperReviewEmails,
      isDecisionMadeInError,
    })

    const manuscript = await Manuscript.find(
      manuscriptId,
      `[
        team.users,
        user,
        notes,
        authors,
        members,
        manuscriptChecks,
        manuscriptValidations,
        manuscriptPeerReviewChecks,
      ]`,
    )

    assertManuscriptCanBeSentToPeerReview(manuscript)

    await moveManuscriptInSentToPeerReview(manuscript, comment)

    await publishEvent({
      manuscript,
      conflictOfInterest,
      improperReviewEmails,
      isDecisionMadeInError,
      comment,
    })

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)
  }

  function assertInputItsValid({
    conflictOfInterest,
    improperReviewEmails,
    isDecisionMadeInError,
  }) {
    const atLeastOneOptionItsSelected =
      isDecisionMadeInError ||
      conflictOfInterest.withAcademicEditor ||
      conflictOfInterest.withTriageEditor ||
      !!conflictOfInterest.withReviewerEmails.length ||
      !!improperReviewEmails.length

    if (!atLeastOneOptionItsSelected) {
      throw new Error(`There is no option selected.`)
    }
  }

  function assertManuscriptCanBeSentToPeerReview({ status, step, id }) {
    if (status !== Manuscript.Statuses.ESCALATED) {
      throw new ValidationError(
        `You cannot return to peer review manuscript: ${id} on status: ${status}.`,
      )
    }
    if (step !== Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING) {
      throw new ValidationError(
        `You cannot return to peer review manuscript: ${id} on step: ${step}.`,
      )
    }
  }

  async function moveManuscriptInSentToPeerReview(manuscript, comment) {
    const note = noteFactory.createNote({
      manuscriptId: manuscript.id,
      type: Note.Types.SEND_TO_PEER_REVIEW_NOTE,
      content: comment,
    })

    manuscript.notes.push(note)
    manuscript.status = Manuscript.Statuses.SENT_TO_PEER_REVIEW

    await Manuscript.upsertGraph(manuscript)
  }

  async function publishEvent({
    manuscript,
    conflictOfInterest,
    improperReviewEmails,
    isDecisionMadeInError,
    comment,
  }) {
    const similarSubmissions = await Manuscript.getSimilarSubmissions(
      manuscript,
    )
    conflictOfInterest.reviewerIds = await getReviewersFromAllVersions(
      manuscript.submissionId,
      conflictOfInterest.withReviewerEmails,
    )

    const improperReviewReviewerIds = await getReviewersFromAllVersions(
      manuscript.submissionId,
      improperReviewEmails,
    )

    const data = manuscriptMapper.domainToPeerReviewCycleCheckingProcessEvent({
      manuscript,
      similarSubmissions,
      conflictOfInterest,
      improperReviewReviewerIds,
      isDecisionMadeInError,
      comment,
    })

    await applicationEventBus.publishMessage({
      event: 'PeerReviewCycleCheckingProcessSentToPeerReview',
      data,
    })
  }

  async function getReviewersFromAllVersions(submissionId, reviewersEmails) {
    if (reviewersEmails.length === 0) return []

    const manuscripts = await Manuscript.findBy({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
    })

    const manuscriptIds = manuscripts.map((m) => m.id)

    return Member.findAllReviewersBy(manuscriptIds, reviewersEmails)
  }
}

const authsomePolicies = [
  'authenticatedUser',
  'hasAccessToManuscript',
  'isAdminOrTeamLead',
]

module.exports = { initialize, authsomePolicies }
