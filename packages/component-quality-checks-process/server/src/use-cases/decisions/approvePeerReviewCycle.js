function initialize({
  models: { Manuscript, User },
  mappers: { manuscriptMapper },
  services: { pubSub },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, userId }) {
    const manuscript = await moveManuscriptInMaterialChecking(manuscriptId)

    await publishSubmissionPeerReviewCycleCheckPassedEvent(manuscript)
    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)

    return manuscript
  }

  async function moveManuscriptInMaterialChecking(manuscriptId) {
    const manuscript = await Manuscript.find(manuscriptId)

    assertManuscriptCanMoveToMaterialsChecking(manuscript)

    manuscript.step = Manuscript.Steps.IN_MATERIAL_CHECKING

    await manuscript.save()

    return manuscript
  }

  function assertManuscriptCanMoveToMaterialsChecking(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot move manuscript ${manuscript.id} in material checking while on ${manuscript.status} status.`,
      )
    }
    if (manuscript.step !== Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING) {
      throw new ValidationError(
        `You cannot move manuscript ${manuscript.id} in material checking while on ${manuscript.step} step.`,
      )
    }
  }

  async function publishSubmissionPeerReviewCycleCheckPassedEvent(manuscript) {
    const { submissionId } = manuscript

    const manuscripts = await Manuscript.findBy({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: '[authors, files, members]',
    })

    const mappedManuscripts = manuscripts.map(
      manuscriptMapper.domainToEventModel,
    )

    await applicationEventBus.publishMessage({
      event: 'SubmissionPeerReviewCycleCheckPassed',
      data: {
        submissionId,
        manuscripts: mappedManuscripts,
      },
    })
  }
}
const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
