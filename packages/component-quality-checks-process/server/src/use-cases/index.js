const decisions = require('./decisions')
const ingestShortArticle = require('./ingestShortArticle')
const ingestAcceptedManuscriptUseCase = require('./ingestAcceptedManuscript')
const ingestSubmittedQCManuscriptUseCase = require('./ingestSubmittedQCManuscript')
const initializeMaterialChecksUseCase = require('./materialCheck/initializeMaterialChecks')
const initializePeerReviewChecksUseCase = require('./peerReviewCheck/initializePeerReviewChecks')
const selectOptionForMaterialCheckUseCase = require('./materialCheck/selectOptionForMaterialCheck')
const selectOptionForPeerReviewCheckUseCase = require('./peerReviewCheck/selectOptionForPeerReviewCheck')
const updateObservationForMaterialCheckUseCase = require('./materialCheck/updateObservationForMaterialCheck')
const updateObservationForPeerReviewCheckUseCase = require('./peerReviewCheck/updateObservationForPeerReviewCheck')
const materialCheckUseCases = require('./materialCheck')

module.exports = {
  ingestShortArticle,
  ingestAcceptedManuscriptUseCase,
  ingestSubmittedQCManuscriptUseCase,
  initializeMaterialChecksUseCase,
  selectOptionForMaterialCheckUseCase,
  updateObservationForMaterialCheckUseCase,
  initializePeerReviewChecksUseCase,
  selectOptionForPeerReviewCheckUseCase,
  updateObservationForPeerReviewCheckUseCase,
  ...decisions,
  ...materialCheckUseCases,
}
