const { Promise } = require('bluebird')
const { chain } = require('lodash')

function initialize({
  models: { Manuscript, Member },
  factories: { authorFactory, fileFactory, manuscriptFactory, memberFactory },
  s3Service,
  useCases: {
    initializeMaterialChecksUseCase,
    isShortArticleSupportedUseCase,
    ingestManuscriptAuthorsEmailsUseCase,
    deleteManuscriptAuthorsEmailsUseCase,
  },
}) {
  return {
    execute,
  }

  async function execute({ submissionId, manuscripts }) {
    const submittedManuscript = getLatestManuscriptVersion(manuscripts)

    const authors = getAuthors(submittedManuscript)
    const files = await getFiles(submittedManuscript)
    let members = getMembers(submittedManuscript)

    const previousManuscriptVersion =
      await Manuscript.getLatestManuscriptVersion({
        queryObject: {
          submissionId,
          flowType: Manuscript.FlowTypes.QUALITY_CHECK,
        },
      })

    if (!previousManuscriptVersion) {
      throw new Error(
        `Manuscript with submissionId:${submissionId} doesn't have a previous version.`,
      )
    }

    if (previousManuscriptVersion.version === submittedManuscript.version) {
      throw new Error(
        `Manuscript with submissionId:${submissionId}, version:${submittedManuscript.version} is already ingested.`,
      )
    }

    const isShortArticleSupported = isShortArticleSupportedUseCase.execute(
      submittedManuscript.articleType.name,
    )

    if (isShortArticleSupported) {
      const submittingStaffMember =
        submittedManuscript.submittingStaffMembers[0]

      const newSubmittingStaffMember = memberFactory.createMember({
        member: {
          surname: submittingStaffMember.surname,
          givenNames: submittingStaffMember.givenNames,
          aff: submittingStaffMember.aff,
          country: submittingStaffMember.country,
          title: submittingStaffMember.title,
          email: submittingStaffMember.email,
          status: submittingStaffMember.status,
          role: { type: Member.Roles.SUBMITTING_STAFF_MEMBER },
          roleLabel: 'Submitting Staff Member',
          phenomUserId: submittingStaffMember.userId,
        },
      })

      members = [...members, newSubmittingStaffMember]
    }

    let manuscript = manuscriptFactory.createManuscript({
      submissionId,
      manuscript: submittedManuscript,
      authors,
      files,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      journalId: submittedManuscript.journalId,
      members,
      authorResponded: true,
    })

    manuscript = await Manuscript.insertGraph(manuscript)

    await ingestManuscriptAuthorsEmailsUseCase.execute({
      manuscriptAuthorsList: manuscript.authors,
      manuscriptId: manuscript.id,
      manuscriptTitle: manuscript.title,
    })

    await Manuscript.upsertGraph({
      ...previousManuscriptVersion,
      status: Manuscript.Statuses.OLDER_VERSION,
      authorResponded: false,
    })

    await deleteManuscriptAuthorsEmailsUseCase.execute({
      manuscriptId: previousManuscriptVersion.id,
    })

    await initializeMaterialChecksUseCase.execute(manuscript.id)

    await publishManuscriptIngestedEvent(manuscript)
  }

  function getLatestManuscriptVersion(manuscripts) {
    return chain(manuscripts)
      .sortBy(
        [(m) => Number(m.version.split('.')[0])],
        [(m) => Number(m.version.split('.')[1] || 0)],
      )
      .last()
      .value()
  }

  function getAuthors(manuscript) {
    return manuscript.authors.map((author) =>
      authorFactory.createAuthor(author),
    )
  }

  async function getFiles(manuscript) {
    return Promise.map(manuscript.files, async (file) => {
      await s3Service.uploadReviewFileToScreeningS3(file.providerKey)
      return fileFactory.createFile({ file })
    })
  }

  function getMembers(manuscript) {
    return manuscript.editors.map((manuscriptEditor) =>
      memberFactory.createMember({ member: manuscriptEditor }),
    )
  }

  async function publishManuscriptIngestedEvent(manuscript) {
    const manuscriptFile = manuscript.getManuscriptFile()

    await applicationEventBus.publishMessage({
      event: 'ManuscriptIngested',
      data: {
        authors: manuscript.authors,
        fileId: manuscriptFile.id,
        manuscriptId: manuscript.id,
        originalFileProviderKey: manuscriptFile.originalFileProviderKey,
      },
    })
  }
}

module.exports = {
  initialize,
}
