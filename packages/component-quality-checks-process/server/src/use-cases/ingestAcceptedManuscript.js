const { chain, last } = require('lodash')
const { getConfig } = require('hindawi-screening/config/publisher')
const { Promise } = require('bluebird')

const publisherConfig = getConfig()

const SubmissionAcceptedFlow = {
  FIRST_ITERATION: 'FIRST_ITERATION',
  NEXT_ITERATION: 'NEXT_ITERATION',
  NEXT_ITERATION_WITH_REVISION: 'NEXT_ITERATION_WITH_REVISION',
}

function initialize({
  models: { Manuscript, Journal, Member, ManuscriptPeerReviewCheck, File },
  factories: {
    authorFactory,
    fileFactory,
    manuscriptFactory,
    memberFactory,
    reviewFactory,
  },
  useCases: {
    initializeMaterialChecksUseCase,
    initializePeerReviewChecksUseCase,
    ingestManuscriptAuthorsEmailsUseCase,
    deleteManuscriptAuthorsEmailsUseCase,
  },
  s3Service,
  logger,
}) {
  async function execute({ submissionId, manuscripts }) {
    const lastManuscript = last(manuscripts)

    if (
      publisherConfig.shortArticleTypes.includes(
        lastManuscript.articleType.name,
      )
    ) {
      logger.info(
        `The quality checking process is skipped for submission id ${submissionId} with article type: ${lastManuscript.articleType.name}.`,
      )
      return null
    }

    const latestSavedManuscript = await Manuscript.getLatestManuscriptVersion({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
      eagerLoadRelations: '[members.reviews]',
    })

    const flow = await getSubmissionAcceptedFlow({
      manuscript: latestSavedManuscript,
      manuscriptEventData: lastManuscript,
    })

    switch (flow) {
      case SubmissionAcceptedFlow.FIRST_ITERATION:
        await ingestManuscripts(manuscripts, submissionId)

        return
      case SubmissionAcceptedFlow.NEXT_ITERATION: {
        await updatePeerReviewCycleCheckingDetails({
          manuscript: latestSavedManuscript,
          manuscriptEventData: lastManuscript,
          newStatus: Manuscript.Statuses.IN_PROGRESS,
        })
        const targetManuscriptId = latestSavedManuscript.id

        await ManuscriptPeerReviewCheck.deleteByManuscriptId(targetManuscriptId)
        await initializePeerReviewChecksUseCase.execute(targetManuscriptId)

        return
      }
      case SubmissionAcceptedFlow.NEXT_ITERATION_WITH_REVISION: {
        const targetManuscriptEventData = manuscripts.find(
          (manuscript) => manuscript.version === latestSavedManuscript.version,
        )

        await updatePeerReviewCycleCheckingDetails({
          manuscript: latestSavedManuscript,
          manuscriptEventData: targetManuscriptEventData,
          newStatus: Manuscript.Statuses.OLDER_VERSION,
        })

        const newManuscripts = getNewManuscripts(
          latestSavedManuscript,
          manuscripts,
        )

        await ingestManuscripts(newManuscripts, submissionId)

        return
      }
      default: {
        logger.error(
          `Missing SubmissionAcceptedFlow.${flow} implementation for the following submission ${submissionId}'`,
        )
        throw new Error(
          'Implementation not provided for SubmissionAcceptedFlow',
        )
      }
    }
  }

  function getNewManuscripts(latestSavedManuscript, manuscripts) {
    const latestSavedManuscriptVersion = parseFloat(
      latestSavedManuscript.version,
    )
    return manuscripts
      .map((manuscript) => ({
        ...manuscript,
        version: parseFloat(manuscript.version),
      }))
      .filter((manuscript) => manuscript.version > latestSavedManuscriptVersion)
  }

  async function ingestManuscripts(manuscripts, submissionId) {
    let savedManuscripts = []

    await Promise.each(manuscripts, async (man) => {
      const savedAuthors = []
      const savedMembers = []

      const journal = await Journal.find(man.journalId)

      const files = await Promise.map(man.files, async (file) => {
        await s3Service.uploadReviewFileToScreeningS3(file.providerKey)
        return fileFactory.createFile({
          file,
        })
      })

      await Promise.each(man.authors, async (a) => {
        let authorResponseToRevision = man.reviews.find(
          (review) =>
            review.recommendation === 'responseToRevision' &&
            a.id === review.teamMemberId,
        )

        const author = authorFactory.createAuthor(a)
        savedAuthors.push(author)

        if (!authorResponseToRevision) return

        const file = chain(authorResponseToRevision)
          .get('comments')
          .find((review) => review.type === 'public')
          .get('files')
          .first()
          .value()

        authorResponseToRevision = reviewFactory.createReview(
          authorResponseToRevision,
        )

        if (file) {
          await s3Service.uploadReviewFileToScreeningS3(file.providerKey)
          authorResponseToRevision.file = fileFactory.createFile({
            file,
            type: File.Types.REVIEW_FILE,
          })
        }
        author.reviews = [authorResponseToRevision]
      })

      man.reviewers.forEach((r) => {
        const reviewer = memberFactory.createMember({
          member: r,
          role: Member.Roles.REVIEWER,
        })
        savedMembers.push(reviewer)
      })

      man.editors.forEach((e) => {
        const editor = memberFactory.createMember({ member: e })
        savedMembers.push(editor)
      })

      man.reviews.forEach((review) => {
        const member = savedMembers.find(
          (member) => member.phenomMemberId === review.teamMemberId,
        )
        if (!member) return

        const savedReview = reviewFactory.createReview(review)

        const file = chain(review)
          .get('comments')
          .find((review) => review.type === 'public')
          .get('files')
          .first()
          .value()

        if (file) {
          s3Service.uploadReviewFileToScreeningS3(file.providerKey)
          savedReview.file = fileFactory.createFile({
            file,
            type: File.Types.REVIEW_FILE,
          })
        }

        member.reviews.push(savedReview)
      })

      const manuscript = manuscriptFactory.createManuscript({
        submissionId,
        manuscript: man,
        authors: savedAuthors,
        files,
        step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
        journalId: journal.id,
        members: savedMembers,
      })

      savedManuscripts.push(manuscript)
    })

    savedManuscripts = await Promise.map(savedManuscripts, async (manuscript) =>
      Manuscript.insertGraph(manuscript),
    )

    const latestVersion = getLatestManuscriptVersion(savedManuscripts)
    const manuscriptFile = latestVersion.getManuscriptFile()

    const previousSavedVersionForSubmissionId =
      await Manuscript.getLatestManuscriptVersion({
        queryObject: { submissionId },
      })

    await Promise.each(savedManuscripts, async (manuscript) => {
      if (manuscript.id !== latestVersion.id) {
        await Manuscript.updateBy({
          queryObject: {
            id: manuscript.id,
          },
          propertiesToUpdate: {
            status: Manuscript.Statuses.OLDER_VERSION,
          },
        })
      }
    })

    await ingestManuscriptAuthorsEmailsUseCase.execute({
      manuscriptAuthorsList: latestVersion.authors,
      manuscriptId: latestVersion.id,
      manuscriptTitle: latestVersion.title,
    })

    if (previousSavedVersionForSubmissionId) {
      await deleteManuscriptAuthorsEmailsUseCase.execute({
        manuscriptId: previousSavedVersionForSubmissionId.id,
      })
    }

    await initializePeerReviewChecksUseCase.execute(latestVersion.id)
    await initializeMaterialChecksUseCase.execute(latestVersion.id)

    if (!manuscriptFile) return

    await applicationEventBus.publishMessage({
      event: 'ManuscriptIngested',
      data: {
        authors: latestVersion.authors,
        fileId: manuscriptFile.id,
        manuscriptId: latestVersion.id,
        originalFileProviderKey: manuscriptFile.originalFileProviderKey,
      },
    })
  }

  async function getSubmissionAcceptedFlow({
    manuscript,
    manuscriptEventData,
  }) {
    const isInQualityCheckingFlow = !!manuscript

    if (isInQualityCheckingFlow && manuscript.isSentToPeerReview()) {
      if (manuscript.version === manuscriptEventData.version) {
        return SubmissionAcceptedFlow.NEXT_ITERATION
      }
      return SubmissionAcceptedFlow.NEXT_ITERATION_WITH_REVISION
    }
    return SubmissionAcceptedFlow.FIRST_ITERATION
  }

  async function updatePeerReviewCycleCheckingDetails({
    manuscript,
    manuscriptEventData,
    newStatus,
  }) {
    const updatedMembers = getMembersWithReviewsAndStatusUpdated(
      manuscript,
      manuscriptEventData,
    )

    const newReviewerMembers = getNewReviewerMembersToSave(
      manuscript,
      manuscriptEventData,
    )

    const newAcademicEditors = getNewAcademicEditors(
      manuscript,
      manuscriptEventData,
    )

    const newEditorialAssistants = getNewEditorialAssistant(
      manuscript,
      manuscriptEventData,
    )

    manuscript.members = [
      ...updatedMembers,
      ...newReviewerMembers,
      ...newAcademicEditors,
      ...newEditorialAssistants,
    ]
    manuscript.status = newStatus

    await Manuscript.upsertGraph(manuscript, {
      insertMissing: true,
      update: true,
    })

    await uploadNewReviewFiles(newReviewerMembers)

    logger.info(
      `The peer review cycle checking details were updated for submission id ${manuscript.submissionId}`,
    )
  }

  function getNewAcademicEditors(manuscript, manuscriptEventData) {
    return manuscriptEventData.editors
      .filter(isAcademicEditor)
      .filter(isMemberNewFor(manuscript))
      .map(createAcademicEditorMember)
      .map(linkReviewFrom(manuscriptEventData))
  }

  function getNewEditorialAssistant(manuscript, manuscriptEventData) {
    return manuscriptEventData.editors
      .filter(isEditorialAssistant)
      .filter(isMemberNewFor(manuscript))
      .map(createEditorialAssistantMember)
      .map(linkReviewFrom(manuscriptEventData))
  }

  function isAcademicEditor(editor) {
    return editor.role.type === 'academicEditor'
  }

  function isEditorialAssistant(editor) {
    return editor.role.type === 'editorialAssistant'
  }

  function isMemberNewFor(manuscript) {
    return (targetMember) => {
      const found = manuscript.members.find(
        (member) => member.phenomMemberId === targetMember.id,
      )
      return !found
    }
  }

  async function uploadNewReviewFiles(newReviewerMembers) {
    const providerKeys = newReviewerMembers
      .filter(containsReviewReportFile)
      .map(getOriginalProviderKey)

    return Promise.each(providerKeys, async (providerKey) =>
      s3Service.uploadReviewFileToScreeningS3(providerKey),
    )
  }

  function getOriginalProviderKey(reviewerMember) {
    return reviewerMember.reviews[0].file.originalFileProviderKey
  }

  function containsReviewReportFile(reviewMember) {
    return reviewMember.reviews.length > 0 && reviewMember.reviews[0].file
  }

  function getMemberFromEventData(member, manuscriptEventData) {
    if (member.isReviewer()) {
      return manuscriptEventData.reviewers.find(
        (reviewer) => reviewer.id === member.phenomMemberId,
      )
    }

    return manuscriptEventData.editors.find(
      (editor) => editor.id === member.phenomMemberId,
    )
  }

  function getMembersWithReviewsAndStatusUpdated(
    manuscript,
    manuscriptEventData,
  ) {
    return manuscript.members.map((member) => {
      const memberFromEventData = getMemberFromEventData(
        member,
        manuscriptEventData,
      )

      if (member.isReviewer()) {
        const [currentReview] = member.reviews
        const [updatedReview] = manuscriptEventData.reviews.filter(
          isReviewLinkedWithMember(member),
        )

        if (updatedReview) {
          if (currentReview) {
            return {
              ...member,
              reviews: [{ ...currentReview, isValid: updatedReview.isValid }],
              status: memberFromEventData.status,
            }
          }
          return {
            ...member,
            reviews: [reviewFactory.createReview(updatedReview)],
            status: memberFromEventData.status,
          }
        }

        return {
          ...member,
          status: memberFromEventData.status,
        }
      }

      const updatedReviews = manuscriptEventData.reviews
        .filter(isReviewLinkedWithMember(member))
        .map(reviewFactory.createReview)

      return {
        ...member,
        reviews: updatedReviews,
        status: memberFromEventData.status,
      }
    })
  }

  function isReviewLinkedWithMember(member) {
    return (review) => review.teamMemberId === member.phenomMemberId
  }

  function getNewReviewerMembersToSave(manuscript, manuscriptEventData) {
    return manuscriptEventData.reviewers
      .filter(hasSubmittedReviewReport)
      .filter(isMemberNewFor(manuscript))
      .map(createReviewerMember)
      .map(linkReviewFrom(manuscriptEventData))
  }

  function linkReviewFrom(manuscriptEventData) {
    return (reviewer) => ({
      ...reviewer,
      reviews: getReviews(manuscriptEventData, reviewer),
    })
  }

  function getReviews(manuscriptEventData, reviewer) {
    return manuscriptEventData.reviews
      .filter(isReviewLinkedWithMember(reviewer))
      .map((review) => {
        const file = chain(review)
          .get('comments')
          .find((review) => review.type === 'public')
          .get('files')
          .first()
          .value()

        const reviewerModel = reviewFactory.createReview(review)

        if (file) {
          return {
            ...reviewerModel,
            file: fileFactory.createFile({
              file,
              type: File.Types.REVIEW_FILE,
            }),
          }
        }

        return reviewerModel
      })
  }

  function hasSubmittedReviewReport(reviewer) {
    return reviewer.status === 'submitted'
  }

  function createReviewerMember(reviewer) {
    return memberFactory.createMember({
      member: reviewer,
      role: Member.Roles.REVIEWER,
    })
  }

  function createAcademicEditorMember(academicEditor) {
    return memberFactory.createMember({
      member: academicEditor,
      role: Member.Roles.ACADEMIC_EDITOR,
    })
  }

  function createEditorialAssistantMember(academicEditor) {
    return memberFactory.createMember({
      member: academicEditor,
      role: Member.Roles.EDITORIAL_ASSISTANT,
    })
  }

  function getLatestManuscriptVersion(manuscripts) {
    return chain(manuscripts)
      .sortBy(
        [(m) => Number(m.version.split('.')[0])],
        [(m) => Number(m.version.split('.')[1] || 0)],
      )
      .last()
      .value()
  }

  return { execute }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
