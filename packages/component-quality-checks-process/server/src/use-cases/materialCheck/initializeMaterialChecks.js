function initialize({
  models: { Manuscript, MaterialCheckConfig, ManuscriptMaterialCheck },
  factories: { manuscriptMaterialCheckFactory },
}) {
  return {
    execute,
  }

  async function execute(manuscriptId) {
    const { config, id: materialCheckConfigId } =
      await MaterialCheckConfig.getLatest()

    await Manuscript.updateBy({
      queryObject: {
        id: manuscriptId,
      },
      propertiesToUpdate: {
        materialCheckConfigId,
      },
    })

    const materialChecks = Object.keys(config).map((name) =>
      manuscriptMaterialCheckFactory.createInitial({
        manuscriptId,
        name,
      }),
    )

    await ManuscriptMaterialCheck.insertGraph(materialChecks)
  }
}

module.exports = {
  initialize,
}
