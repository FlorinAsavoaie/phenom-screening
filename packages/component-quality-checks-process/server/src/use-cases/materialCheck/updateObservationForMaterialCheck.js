const { ValidationError } = require('@pubsweet/errors')

function initialize({
  ManuscriptMaterialCheck,
  MaterialCheckConfig,
  Manuscript,
  User,
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, name, additionalInfo, userId }) {
    await assertManuscriptIsInMaterialChecking({ manuscriptId, name, userId })

    const manuscriptMaterialCheck = await ManuscriptMaterialCheck.findOneBy({
      queryObject: { manuscriptId, name },
    })
    const { selectedOption } = manuscriptMaterialCheck
    await assertAdditionalInfoIsSupported({
      manuscriptId,
      name,
      selectedOption,
    })

    await assertAdditionalInfoIsValid({
      additionalInfo,
      name,
      manuscriptId,
      selectedOption,
    })

    return updateAdditionalInfo({
      manuscriptMaterialCheck,
      additionalInfo,
    })
  }

  async function assertManuscriptIsInMaterialChecking({
    manuscriptId,
    name,
    userId,
  }) {
    const manuscript = await Manuscript.find(manuscriptId)
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot update material check observation for ${name} on ${manuscript.status} status for manuscript ${manuscript.id}.`,
      )
    }
    if (manuscript.step !== Manuscript.Steps.IN_MATERIAL_CHECKING) {
      throw new ValidationError(
        `You cannot update material check observation for ${name} on ${manuscript.step} step for manuscript ${manuscript.id}.`,
      )
    }
  }

  async function assertAdditionalInfoIsSupported({
    manuscriptId,
    name,
    selectedOption,
  }) {
    const isAdditionalInfoSupported =
      await MaterialCheckConfig.isAdditionalInfoSupported({
        name,
        manuscriptId,
        selectedOption,
      })

    if (!isAdditionalInfoSupported) {
      throw new ValidationError(
        `Additional information not supported for ${selectedOption} option on manuscript ${manuscriptId}.`,
      )
    }
  }

  async function assertAdditionalInfoIsValid({
    additionalInfo,
    name,
    manuscriptId,
    selectedOption,
  }) {
    const additionalInfoTypeFromConfig =
      await MaterialCheckConfig.getAdditionalInfoType({
        name,
        manuscriptId,
        selectedOption,
      })

    // eslint-disable-next-line no-restricted-globals
    if (additionalInfoTypeFromConfig === 'number' && isNaN(additionalInfo)) {
      throw new ValidationError(
        `Additional information for question ${name} should be of type ${additionalInfoTypeFromConfig} for manuscript ${manuscriptId}.`,
      )
    }

    const parsedValue = parseFloat(additionalInfo)

    if (
      additionalInfoTypeFromConfig === 'number' &&
      (parsedValue < 0 || !Number.isInteger(parsedValue))
    ) {
      throw new ValidationError(
        `Additional information for question ${name} on manuscript ${manuscriptId} should be a natural number.`,
      )
    }
  }

  async function updateAdditionalInfo({
    manuscriptMaterialCheck,
    additionalInfo,
  }) {
    manuscriptMaterialCheck.additionalInfo = additionalInfo

    return manuscriptMaterialCheck.save()
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
