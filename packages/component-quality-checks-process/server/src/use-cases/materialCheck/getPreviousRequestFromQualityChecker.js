const { ValidationError } = require('@pubsweet/errors')

function initialize({
  models: { Manuscript, User },
  useCases: { getRequestFromQualityChecker },
}) {
  return {
    execute,
  }

  async function execute(manuscriptId) {
    const manuscript = await Manuscript.find(manuscriptId)
    const assignedChecker = await User.findAssignedChecker(manuscriptId)
    await assertManuscriptVersionIsMinor(manuscript)
    const previousManuscript = await getPreviousManuscript(manuscript)
    const {
      correspondingAuthor,
      manuscript: manuscriptNew,
      checkerObservations,
      additionalComments,
    } = await getRequestFromQualityChecker.execute(previousManuscript.id)

    const arrayOfCheckerObservations = Object.keys(checkerObservations).map(
      (key) => ({ request: key, observation: checkerObservations[key][0] }),
    )

    return {
      assignedChecker,
      correspondingAuthor,
      manuscript: manuscriptNew,
      checkerObservations: arrayOfCheckerObservations,
      additionalComments,
    }
  }

  async function assertManuscriptVersionIsMinor({ id, version }) {
    if (!version.includes('.')) {
      throw new ValidationError(
        `Previous request from quality checker cannot be fetched for the manuscript with ID "${id}" because it has major version.`,
      )
    }
  }

  function getPreviousVersion({ version }) {
    const [majorVersion, minorVersion] = `${version}`.split('.')

    if (minorVersion === '1') {
      return `${majorVersion}`
    }
    return `${majorVersion}.${minorVersion - 1}`
  }

  async function getPreviousManuscript(manuscript) {
    const previousVersion = getPreviousVersion(manuscript)

    return Manuscript.findOneBy({
      queryObject: {
        submissionId: manuscript.submissionId,
        version: previousVersion,
        flowType: 'qualityCheck',
      },
    })
  }
}
module.exports = {
  initialize,
}
