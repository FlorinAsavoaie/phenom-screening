const getRequestFromQualityCheckerUseCase = require('./getRequestFromQualityChecker')
const getPreviousRequestFromQualityCheckerUseCase = require('./getPreviousRequestFromQualityChecker')

module.exports = {
  getRequestFromQualityCheckerUseCase,
  getPreviousRequestFromQualityCheckerUseCase,
}
