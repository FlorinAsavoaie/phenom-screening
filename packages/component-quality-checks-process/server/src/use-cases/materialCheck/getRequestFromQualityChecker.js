const parseCheckerObservations = require('./parseCheckerObservations')

function initialize({
  models: { Author, Manuscript, Note, ManuscriptValidations, Member },
  useCases: { isShortArticleSupportedUseCase },
}) {
  return {
    execute,
  }

  async function execute(manuscriptId, articleType) {
    let person

    const isShortArticleSupported =
      isShortArticleSupportedUseCase.execute(articleType)

    if (isShortArticleSupported) {
      person = await Member.getSubmittingStaffMember(manuscriptId)
    } else {
      person = await Author.getCorrespondingAuthor(manuscriptId)
    }

    if (!person)
      throw new Error(
        `Could not find person with manuscript id: ${manuscriptId}`,
      )

    const manuscript = await Manuscript.find(manuscriptId)
    const checkerObservations = await getCheckerObservations(manuscriptId)
    const additionalComments = await getAdditionalComments(manuscriptId)

    return {
      person,
      manuscript,
      checkerObservations,
      additionalComments,
    }
  }

  async function getCheckerObservations(manuscriptId) {
    const infoValidationObservations = await ManuscriptValidations.findBy({
      queryObject: { manuscriptId },
    })
    const infoValidationNotes = await getInfoValidationNotes(manuscriptId)
    const materialChecksObservations = await getMaterialCheckObservations(
      manuscriptId,
    )

    const checkerObservations = parseCheckerObservations({
      infoValidationNotes,
      infoValidationObservations,
      materialChecksObservations,
    })

    return checkerObservations
  }

  async function getMaterialCheckObservations(manuscriptId) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[manuscriptMaterialChecks, materialCheckConfig]',
    )

    const { manuscriptMaterialChecks, materialCheckConfig } = manuscript

    const materialChecksObservations = manuscriptMaterialChecks.map((obs) => ({
      config: materialCheckConfig.config[obs.name],
      name: obs.name,
      selectedOption: obs.selectedOption,
      additionalInfo: obs.additionalInfo,
    }))
    return materialChecksObservations
  }

  async function getInfoValidationNotes(manuscriptId) {
    const manuscriptNotes = await Note.findValidationNotes({
      queryObject: { manuscriptId },
    })

    return manuscriptNotes.map((note) => ({
      type: note.type,
      content: note.content,
    }))
  }
  async function getAdditionalComments(manuscriptId) {
    const note = await Note.findOneBy({
      queryObject: { manuscriptId, type: Note.Types.DECISION_NOTE },
    })

    return {
      content: note.content,
      created: note.created.toString(),
    }
  }
}

module.exports = {
  initialize,
}
