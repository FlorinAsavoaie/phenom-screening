const { ValidationError } = require('@pubsweet/errors')

function initialize({
  ManuscriptMaterialCheck,
  MaterialCheckConfig,
  Manuscript,
  User,
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, name, selectedOption, userId }) {
    await assertManuscriptIsInMaterialChecking({ manuscriptId, name, userId })
    await assertSelectedOptionIsValid({ manuscriptId, name, selectedOption })
    return updateSelectedOption({ manuscriptId, name, selectedOption })
  }

  async function assertManuscriptIsInMaterialChecking({
    manuscriptId,
    name,
    userId,
  }) {
    const manuscript = await Manuscript.find(manuscriptId)
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot select material check option for ${name} on ${manuscript.status} status for manuscript ${manuscript.id}.`,
      )
    }
    if (manuscript.step !== Manuscript.Steps.IN_MATERIAL_CHECKING) {
      throw new ValidationError(
        `You cannot select material check option for ${name} on ${manuscript.step} step for manuscript ${manuscript.id}.`,
      )
    }
  }

  async function assertSelectedOptionIsValid({
    name,
    manuscriptId,
    selectedOption,
  }) {
    const { options } =
      await MaterialCheckConfig.getConfigByManuscriptIdAndName({
        manuscriptId,
        name,
      })

    const isSelectedOptionAvailable = !!options.find(
      ({ value }) => value === selectedOption,
    )

    if (!isSelectedOptionAvailable) {
      throw new ValidationError(
        `Selected option, ${selectedOption}, is not valid for ${name} on manuscript ${manuscriptId}`,
      )
    }
  }

  async function updateSelectedOption({ manuscriptId, name, selectedOption }) {
    const manuscriptMaterialCheck = await ManuscriptMaterialCheck.findOneBy({
      queryObject: { manuscriptId, name },
    })

    manuscriptMaterialCheck.additionalInfo = ''

    manuscriptMaterialCheck.selectedOption = selectedOption

    return manuscriptMaterialCheck.save()
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
