const { chain } = require('lodash')

const parseCheckerObservations = ({
  infoValidationNotes,
  infoValidationObservations,
  materialChecksObservations,
}) => {
  const observationsDictionary = {
    includesCitations: {
      content:
        'Please remove any citation(s) or reference(s) from the abstract.',
      title: 'Abstract',
      order: 2,
    },
    graphical: {
      content: 'Please remove graphical content from the abstract.',
      title: 'Abstract',
      order: 2,
    },
    differentAuthors: {
      content:
        'The author details entered in our system do not match those in the uploaded manuscript file. Please update whichever is incorrect.',
      title: 'Authors & Affiliations',
      order: 3,
    },
    missingAffiliations: {
      content:
        'One or more authors are missing their affiliation. Please include all affiliations in the manuscript file. ',
      title: 'Authors & Affiliations',
      order: 3,
    },
    nonacademicAffiliations: {
      content:
        'We have discovered non-academic affiliations in your manuscript file.',
      title: 'Authors & Affiliations',
      order: 3,
    },
    topicNotAppropriate: {
      content: 'Topic is not appropriate for the journal you selected.',
      title: 'Relevance Check',
      order: 4,
    },
    articleTypeIsNotAppropriate: {
      content:
        'The article type for your manuscript is not appropriate for what you have provided.',
      title: 'Relevance Check',
      order: 4,
    },
    manuscriptIsMissingSections: {
      content: 'The following sections are missing:',
      title: 'Relevance Check',
      order: 4,
    },
    missingReferences: {
      content: 'We have discovered that the following references are missing',
      title: 'Reference Extraction',
      order: 5,
    },
    wrongReferences: {
      content: 'Corrections to your references are required.',
      title: 'Reference Extraction',
      order: 5,
    },
    '': '',
  }

  const notesTypeDictionary = {
    titleNote: { title: 'Title', order: 1 },
    abstractNote: { title: 'Abstract', order: 2 },
    authorsNote: { title: 'Authors & Affiliations', order: 3 },
    relevanceNote: { title: 'Relevance Check', order: 4 },
    referencesNote: { title: 'Reference Extraction', order: 5 },
    '': '',
  }

  const materialChecksDictionary = {
    hasFrontPage:
      'We require every manuscript to have a front page, please upload it separately',
    hasAllFiguresAndTables: 'Please upload all figures and tables separately',
    hasAllSupplementaryMaterials: 'Missing the following supplementary files',
    hasPatiantParticipantConsentForm:
      'Please upload the participant consent form',
    hasAcknowledgements:
      'Please add an acknowledgement section to your manuscript file',
    hasClinicalStudyRegistrationNumber:
      'Please provide a clinical study registration number',
    hasCoverLetter: 'Please provide a cover letter for your manuscript',
    hasAllSections: 'Sections',
    hasFinalVersionOfTheAuthorList: 'Author list',
    hasAllAffiliationsListed: 'Affiliations listed',
    hasConflictOfInterestMention: 'Conflict of interest mention',

    frontPageApproved:
      'We require every manuscript to have a front page. This should include the title, author names, and affiliations. Please include this in your manuscript file.',
    headersAndFootersApproved:
      'We haven’t found headers and footers inserted in your main manuscript file',
    figuresAndTablesPresentInPDFAndAllCited:
      'Please provide all Figures and Tables in the PDF have them all cited',
    figuresProvidedInEditableFormat:
      'Please upload editable figure files and tables separately.',
    hasFileInEditableFormat:
      'Please upload the main manuscript file in an editable format',
    supplementaryMaterialsApproved:
      'We are missing the following supplementary files. Please ensure that these are uploaded to the system.',
    hasPatientParticipantConsentFormApproved:
      'Please upload the participant consent form.',
    hasAcknowledgementsApproved:
      'Please add an acknowledgement section to your manuscript file.',
    clinicalStudyRegistrationNumberApproved:
      'Please provide a Clinical Study Registration Number.',
    coverLetterApproved: 'Please provide a cover letter for your manuscript.',
    allSectionsPresentAndApproved: 'All sections present and approved',
    fundingStatementApproved: 'Please provide a funding statement.',
    manuscriptFigureFiles: '',
    editableFigureCaptions: 'Please provide editable figure captions.',
    arXivID: 'Please provide arXiv ID.',
  }

  const parsedInfoValidation = chain(infoValidationObservations)
    .pickBy((obs, key) => obs === true && key !== 'topicNotAppropriate')
    .keys()
    .map((obs = '') => observationsDictionary[obs])
    .value()

  const infoValidationTypes = [
    'titleNote',
    'authorsNote',
    'abstractNote',
    'referencesNote',
    'relevanceNote',
  ]

  const parsedInfoValidationNotes = infoValidationNotes
    .filter((n) => infoValidationTypes.includes(n.type))
    .map((n = '') => ({
      type: n.type,
      content: n.content.trim(),
      title: notesTypeDictionary[n.type].title,
      order: notesTypeDictionary[n.type].order,
    }))

  // !IMPORTANT! if excludedQuestions array has more than 2 terms we should think of another way to filter questions
  const excludedQuestions = [
    'allSectionsPresentAndApproved',
    'manuscriptFigureFiles',
  ]
  const parsedMaterialChecks = materialChecksObservations
    .filter((mc) => {
      const optionsWithObs = mc.config.options.some(
        (o) => o.value === mc.selectedOption && o.supportsAdditionalInfo,
      )
      return optionsWithObs && !excludedQuestions.includes(mc.name)
    })
    .map((mc = '') => ({
      content: mc.additionalInfo ? mc.additionalInfo.trim() : null,
      title: materialChecksDictionary[mc.name],
      order: parseInt(mc.config.order, 10) + 5,
    }))

  const checks = chain(parsedInfoValidation)
    .concat(parsedInfoValidationNotes)
    .concat(parsedMaterialChecks)
    .orderBy('order', 'asc')
    .reduce(groupObservationsByType, {})
    .value()

  return checks
}

function groupObservationsByType(result, observation) {
  if (!result[observation.title]) {
    result[observation.title] = []
  }
  result[observation.title].push(observation.content)
  return result
}

module.exports = parseCheckerObservations
