input SelectOptionForMaterialCheckInput {
  manuscriptId: String!
  name: String!
  selectedOption: MaterialCheckValueOption!
}
input UpdateObservationForMaterialCheck {
  manuscriptId: String!
  name: String!
  additionalInfo: String!
}

input SelectOptionForPeerReviewCheckInput {
  manuscriptId: String!
  name: String!
  selectedOption: String!
}
input UpdateObservationForPeerReviewCheck {
  manuscriptId: String!
  name: String!
  observation: String!
}

input ConflictOfInterest {
  withTriageEditor: Boolean!
  withAcademicEditor: Boolean!
  withReviewerEmails: [String]!
}


extend type Mutation {
  selectOptionForMaterialCheck(
    input: SelectOptionForMaterialCheckInput
  ): ManuscriptMaterialCheck
  updateObservationForMaterialCheck(
    input: UpdateObservationForMaterialCheck
  ): ManuscriptMaterialCheck

  selectOptionForPeerReviewCheck(
    input: SelectOptionForPeerReviewCheckInput
  ): ManuscriptPeerReviewCheck
  updateObservationForPeerReviewCheck(
    input: UpdateObservationForPeerReviewCheck
  ): ManuscriptPeerReviewCheck
  approvePeerReviewCycle(manuscriptId: String!): ScreeningManuscript
  approveQualityCheck(
    manuscriptId: String!
    additionalComments: String
  ): Boolean
  refuseToConsiderQualityCheck(
    manuscriptId: String!
    content: RTCInput
    note: String
  ): Boolean
  requestFiles(manuscriptId: String!, content: String): Boolean
  sendToPeerReview(
    manuscriptId: String!
    isDecisionMadeInError: Boolean!
    improperReviewEmails: [String]!
    conflictOfInterest: ConflictOfInterest!
    comment: String!
  ): Boolean
}

extend type Query {
  getPreviousRequestFromQualityChecker(
    manuscriptId: ID!
  ): RequestFromQualityChecker
}

type RequestFromQualityChecker {
  correspondingAuthor: Author
  manuscript: ScreeningManuscript
  additionalComments: AdditionalComments
  checkerObservations: [CheckerObservation]
  assignedChecker: UserDetails
}

type CheckerObservation {
  request: String
  observation: String
}

type AdditionalComments {
  content: String
  created: String
}
