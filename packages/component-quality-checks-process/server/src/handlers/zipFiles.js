const { get } = require('lodash')
const archiver = require('archiver')
const { Promise } = require('bluebird')

const zipFiles =
  ({ s3Service, models: { File } }) =>
  async (req, res) => {
    const { manuscriptId } = req.params

    const files = await File.findBy({
      queryObject: { manuscriptId },
    })

    const archive = archiver('zip')
    archive.pipe(res)

    const getObjectStream = async (fileKey) => {
      const { Body } = await s3Service.getObjectStream(fileKey)
      return Body
    }

    await Promise.map(files, async (file) => ({
      stream: await getObjectStream(file.originalFileProviderKey),
      file,
    })).each(({ stream, file }) => {
      if (get(file, 'type') === 'supplementary') file.type = 'supplemental'
      archive.append(stream, {
        name: `${get(file, 'type', 'supplementary')}/${get(file, 'fileName')}`,
      })
    })

    archive.finalize()
  }

module.exports = zipFiles
