const { get, reduce } = require('lodash')
const config = require('config')
const { getEmailCopy } = require('./emailCopy')
const { getNotificationEmailProps } = require('./getNotificationEmailProps')

const { emailData } = config.get('publisherConfig')
const emailSender = config.get('emailSender')

function initialize({ Email, emailService, configService }) {
  async function notifyAuthorWhenFilesAreRequested({
    person,
    manuscript,
    checkerObservations,
    additionalComments,
  }) {
    const transformToListObjects = (values) =>
      values
        .map((value) => (value ? `<li>${value}</li>` : null))
        .join()
        .replace(/,/g, '')

    const observations = reduce(
      checkerObservations,
      (acc, values, key) =>
        `${acc}
          <li><strong style = "margin-bottom: 5px"> ${key}</strong></li><ul style = "margin-bottom: 5px">${transformToListObjects(
          values,
        )}
          </ul>`,
      '',
    )

    const paragraph = `Dear Dr. ${person.fullName}, <br/><br/>
      Regarding your manuscript ${manuscript.title} ${
      manuscript.customId
    }, we would be grateful if you could please address the following:
          <ul>${observations}</ul>
          ${
            additionalComments.content &&
            `<strong>Additional Comments:</strong><br/><br/>
               <em style="padding: 0 10px; display: block; text-align: justify;">
                 ${additionalComments.content}
               </em>
               <br/>
             `
          }
          <ol>
            <li>
              Click on the manuscript title ${manuscript.title}, which will show
              the status "Submit Updates"
            </li>
            <li>
              Click on the "Submit Quality Check Updates" button to show the
              manuscript details and files
            </li>
            <li>
              The manuscript information will be pre-filled as you submitted the
              manuscript, so you will not need to complete the entire submission
              again. Scroll down the page until you reach the area(s) that need
              updating
            </li>
            <li>
              If the manuscript file needs updating then go to the 'Manuscript
              Files' section, and click the small bin icon next to the manuscript
              file. This will delete the file. You can then upload the new
              updated file
            </li>
            <li>
              When all necessary changes have been made, click the green "Submit
              manuscript" button at the bottom of the screen, and this will
              return the manuscript to the Hindawi team.
            </li>
          </ol>
          <br/><br/>`

    await emailService.sendNotification({
      fromUser: {
        email: configService.get('emailSender'),
        fullName: `${manuscript.flowTypeLabel} Team`,
      },
      toUser: {
        email: person.email,
        fullName: person.fullName,
      },
      content: {
        subject: `Files requested - ${manuscript.title}`,
        paragraph,
      },
    })
  }
  async function notifyEAWhenSubmissionPassedQualityChecks({
    checker,
    manuscript,
    additionalComments,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const checkerName = getFullName(checker)

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'quality-check-passed',
      customId,
      title,
      fromName: `The ${manuscript.flowTypeLabel} Team`,
      additionalComments,
      checkerName,
    })

    const emailProps = getNotificationEmailProps({
      subject: `[${customId}]: Quality Check Passed`,
      toUser: {
        email: editorialAssistant.email,
        name: editorialAssistant.fullName,
      },
      fromEmail: `${manuscript.flowTypeLabel} Team <${emailSender}>`,
      paragraph,
      signatureName: `${manuscript.flowTypeLabel} Team`,
      footerText: emailData.footerTextTemplate`${editorialAssistant.email}`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  }
  return {
    notifyAuthorWhenFilesAreRequested,
    notifyEAWhenSubmissionPassedQualityChecks,
  }
}

function getFullName(user) {
  return `${get(user, 'givenNames')} ${get(user, 'surname')}`
}

module.exports = { initialize }
