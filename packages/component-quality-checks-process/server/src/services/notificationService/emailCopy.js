const getEmailCopy = ({
  customId,
  additionalComments = '',
  emailType,
  fromName,
  title,
}) => {
  let paragraph
  const hasIntro = false
  const hasSignature = true
  switch (emailType) {
    case 'quality-check-passed':
      paragraph = `
      ${fromName} has confirmed that the manuscript ${title} with ID No. ${customId} has passed all quality checks and is ready for production.
      <br/><br/>
      ${
        additionalComments &&
        `<strong>Additional Comments:</strong><br/><br/>
           <em style="padding: 0 10px; display: block; text-align: justify;">
             ${additionalComments}
           </em>
           <br/> 
         `
      }
          `
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasIntro, hasSignature }
}

module.exports = {
  getEmailCopy,
}
