module.exports = {
  getNotificationEmailProps({
    toUser,
    subject,
    signatureName,
    paragraph,
    fromEmail,
    footerText,
    cc,
  }) {
    return {
      type: 'system',
      templateType: 'notification',
      fromEmail,
      toUser,
      cc,
      content: {
        subject,
        signatureName,
        paragraph,
        footerText,
      },
    }
  },
}
