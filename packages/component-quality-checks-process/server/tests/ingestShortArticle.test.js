const models = require('@pubsweet/models')
const useCases = require('../src/use-cases')
const { factories } = require('../../../component-model')
const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')

const { Manuscript, MaterialCheckConfig, ManuscriptMaterialCheck, Member } =
  models

const journalId = 'journal-id'
const submissionId = 'submission-id'

const generateInput = () => ({
  submissionId,
  manuscripts: [
    {
      id: 'manuscript-id',
      journalId,
      abstract: 'abstract',
      title: 'manuscript title',
      articleType: { name: 'Corrigendum' },
      customId: '1256186',
      version: 1,
      files: [
        {
          providerKey: 'originalFileProviderKey-id-1',
          type: 'manuscript',
          originalName: 'Test File 1',
          fileName: 'Test File 2',
          size: '107161',
          mimeType: 'application/pdf,',
        },
        {
          providerKey: 'originalFileProviderKey-id-2',
          type: 'coverLetter',
          originalName: 'Test File 2',
          fileName: 'Test File 2',
          size: '107161',
          mimeType: 'application/pdf,',
        },
      ],
      authors: [
        {
          isSubmitting: true,
          isCorresponding: true,
          position: 0,
          email: 'something@test.com',
          affiliation: { name: 'Hindawi' },
          country: 'Romania',
          givenNames: 'John',
          surname: 'Doe',
          title: 'Prof',
        },
      ],
      submittingStaffMembers: [
        {
          id: 'member-id',
          manuscriptId: 'member-manuscript-id',
          surname: 'Jane',
          givenNames: 'Doe',
          aff: 'Affiliation',
          country: 'UK',
          title: 'dr',
          email: 'member@test.com',
          role: 'submittingStaffMember',
          status: 'pending',
        },
      ],
    },
  ],
})

describe('Ingest Short Articles Use Case', () => {
  const configService = require('config')

  let manuscriptInput
  let s3Service
  let initializeMaterialChecksUseCase
  let isShortArticleSupportedUseCase
  let ingestManuscriptAuthorsEmailsUseCase

  beforeEach(async () => {
    manuscriptInput = generateInput()

    const savedManuscript = {
      id: 'saved-manuscript-id',
      journalId,
      authors: [
        {
          id: 'author-id',
          isSubmitting: true,
          isCorresponding: true,
          position: 0,
          email: 'something@test.com',
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'John',
          surname: 'Doe',
          title: 'Prof',
        },
      ],
      files: [
        {
          id: 'file-id-1',
          originalFileProviderKey: 'originalFileProviderKey-id-1',
          type: 'manuscript',
          originalName: 'Test File 1',
          fileName: 'Test File 1',
          size: '107161',
          mimeType: 'application/pdf,',
        },
        {
          id: 'file-id-2',
          originalFileProviderKey: 'originalFileProviderKey-id-2',
          type: 'coverLetter',
          originalName: 'Test File 2',
          fileName: 'Test File 2',
          size: '107161',
          mimeType: 'application/pdf,',
        },
      ],
      submittingStaffMembers: [
        {
          isSubmitting: true,
          isCorresponding: false,
          position: 0,
          email: 'submitting-staff-member@test.com',
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Jane',
          surname: 'Doe',
          title: 'Prof',
        },
      ],
      flowType: 'qualityCheck',
      step: 'inMaterialChecking',
      getManuscriptFile: () => {},
      manuscriptValidations: {},
      manuscriptChecks: {},
    }

    const savedMember = {
      id: 'submitting-staff-member-id',
      manuscriptId: 'saved-member-manuscript-id',
      surname: 'Doe',
      givenNames: 'Joe',
      email: 'submitting-staff-member@test.com',
      aff: 'Hindawi',
      country: 'Romania',
      title: 'Prof',
      role: 'submittingStaffMember',
      roleLabel: 'Submitting Staff Member',
      status: 'submitted',
    }

    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue(savedManuscript)
    jest.spyOn(Member, 'insertGraph').mockReturnValue(savedMember)

    jest.spyOn(savedManuscript, 'getManuscriptFile').mockReturnValueOnce({
      id: 'file-id-1',
      originalFileProviderKey: 'originalFileProviderKey-id-1',
    })

    s3Service = {
      uploadReviewFileToScreeningS3: jest.fn(async () => {}),
    }

    ingestManuscriptAuthorsEmailsUseCase = {
      execute: jest.fn(),
    }

    // initializeMaterialChecks use-case
    initializeMaterialChecksUseCase =
      useCases.initializeMaterialChecksUseCase.initialize({
        models,
        factories,
      })

    isShortArticleSupportedUseCase = isShortArticleSupported.initialize({
      configService,
    })

    jest.spyOn(initializeMaterialChecksUseCase, 'execute')

    jest.spyOn(Manuscript, 'updateBy').mockResolvedValue()

    jest.spyOn(MaterialCheckConfig, 'getLatest').mockResolvedValueOnce({
      id: 'material-checks-config-1',
      config: {
        frontPageApproved: {
          options: [
            {
              value: 'yes',
              supportsAdditionalInfo: false,
            },
            {
              value: 'no',
              supportsAdditionalInfo: true,
              type: 'text',
              label: 'observation',
            },
            {
              value: 'notRequired',
              supportsAdditionalInfo: false,
            },
          ],
          order: 1,
        },
      },
    })

    jest.spyOn(ManuscriptMaterialCheck, 'insertGraph').mockResolvedValue()

    global.applicationEventBus = {
      publishMessage: jest.fn(async () => {}),
    }
  })

  it('should throw error if article type is not a supported article type', async () => {
    const result = useCases.ingestShortArticle
      .initialize({
        models,
        factories,
        s3Service,
        useCases: {
          initializeMaterialChecksUseCase,
          isShortArticleSupportedUseCase,
          ingestManuscriptAuthorsEmailsUseCase,
        },
        configService,
      })
      .execute({
        submissionId,
        manuscripts: [
          {
            ...manuscriptInput.manuscripts[0],
            articleType: { name: 'Research Article' },
          },
        ],
      })

    await expect(result).rejects.toThrowError(
      'Article type Research Article is not supported by ingestShortArticle use-case',
    )
  })

  it('should save the manuscript with authors and files', async () => {
    await useCases.ingestShortArticle
      .initialize({
        models,
        factories,
        useCases: {
          initializeMaterialChecksUseCase,
          isShortArticleSupportedUseCase,
          ingestManuscriptAuthorsEmailsUseCase,
        },
        s3Service,
        configService,
      })
      .execute({ submissionId, manuscripts: manuscriptInput.manuscripts })

    expect(Manuscript.upsertGraph).toHaveBeenCalledTimes(1)
    expect(Manuscript.upsertGraph).toHaveBeenCalledWith({
      abstract: 'abstract',
      articleType: 'Corrigendum',
      authors: [
        {
          aff: 'Hindawi',
          country: 'Romania',
          email: 'something@test.com',
          givenNames: 'John',
          isCorresponding: true,
          isSubmitting: true,
          orcid: null,
          position: 0,
          surname: 'Doe',
          title: 'Prof',
        },
      ],
      customId: '1256186',
      files: [
        {
          fileName: 'Test File 2',
          mimeType: 'application/pdf,',
          originalFileProviderKey: 'originalFileProviderKey-id-1',
          originalName: 'Test File 1',
          providerKey: null,
          size: '107161',
          type: 'manuscript',
        },
        {
          fileName: 'Test File 2',
          mimeType: 'application/pdf,',
          originalFileProviderKey: 'originalFileProviderKey-id-2',
          originalName: 'Test File 2',
          providerKey: null,
          size: '107161',
          type: 'coverLetter',
        },
      ],
      flowType: 'qualityCheck',
      journalId: 'journal-id',
      manuscriptChecks: {},
      manuscriptValidations: {},
      members: [],
      phenomId: 'manuscript-id',
      step: 'inMaterialsChecking',
      submissionId: 'submission-id',
      title: 'manuscript title',
      version: '1',
    })
  })

  it('should upload manuscript files', async () => {
    await useCases.ingestShortArticle
      .initialize({
        models,
        factories,
        useCases: {
          initializeMaterialChecksUseCase,
          isShortArticleSupportedUseCase,
          ingestManuscriptAuthorsEmailsUseCase,
        },
        s3Service,
        configService,
      })
      .execute({ submissionId, manuscripts: manuscriptInput.manuscripts })

    expect(s3Service.uploadReviewFileToScreeningS3).toBeCalledTimes(2)
    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenNthCalledWith(
      1,
      'originalFileProviderKey-id-1',
    )
    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenNthCalledWith(
      2,
      'originalFileProviderKey-id-2',
    )
  })

  it('should initialize material checks', async () => {
    await useCases.ingestShortArticle
      .initialize({
        models,
        factories,
        useCases: {
          initializeMaterialChecksUseCase,
          isShortArticleSupportedUseCase,
          ingestManuscriptAuthorsEmailsUseCase,
        },
        s3Service,
        configService,
      })
      .execute({ submissionId, manuscripts: manuscriptInput.manuscripts })

    expect(initializeMaterialChecksUseCase.execute).toHaveBeenCalledTimes(1)
    expect(initializeMaterialChecksUseCase.execute).toBeCalledWith(
      'saved-manuscript-id',
    )
  })

  it('should publish ManuscriptIngested event', async () => {
    await useCases.ingestShortArticle
      .initialize({
        models,
        factories,
        useCases: {
          initializeMaterialChecksUseCase,
          isShortArticleSupportedUseCase,
          ingestManuscriptAuthorsEmailsUseCase,
        },
        s3Service,
        configService,
      })
      .execute({ submissionId, manuscripts: manuscriptInput.manuscripts })

    expect(global.applicationEventBus.publishMessage).toBeCalledTimes(1)
    expect(global.applicationEventBus.publishMessage).toHaveBeenCalledWith({
      data: {
        authors: [
          {
            aff: 'Hindawi',
            country: 'Romania',
            email: 'something@test.com',
            givenNames: 'John',
            id: 'author-id',
            isCorresponding: true,
            isSubmitting: true,
            position: 0,
            surname: 'Doe',
            title: 'Prof',
          },
        ],
        fileId: 'file-id-1',
        manuscriptId: 'saved-manuscript-id',
        originalFileProviderKey: 'originalFileProviderKey-id-1',
      },
      event: 'ManuscriptIngested',
    })
  })
})
