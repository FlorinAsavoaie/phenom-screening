const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const { factories } = require('../../../../component-model')

const { Manuscript, PeerReviewCheckConfig, ManuscriptPeerReviewCheck } = models

const chance = new Chance()

const manuscriptId = chance.guid()
const peerReviewCheckConfigId = chance.guid()

const peerReviewCheckConfig = {
  id: peerReviewCheckConfigId,
  config: {
    conflictOfInterestMention: {
      options: [
        {
          value: 'yes',
          supportsAdditionalInfo: false,
        },
        {
          value: 'no',
          supportsAdditionalInfo: true,
          type: 'text',
          label: 'observation',
        },
        {
          value: 'notRequired',
          supportsAdditionalInfo: false,
        },
      ],
      order: 1,
    },
    headersAndFootersApproved: {
      options: [
        {
          value: 'yes',
          supportsAdditionalInfo: false,
        },
        {
          value: 'no',
          supportsAdditionalInfo: true,
          type: 'text',
          label: 'observation',
        },
      ],
      order: 2,
    },
  },
}
PeerReviewCheckConfig.getLatest = jest
  .fn()
  .mockResolvedValue(peerReviewCheckConfig)

Manuscript.updateBy = jest.fn()

ManuscriptPeerReviewCheck.insertGraph = jest.fn()

describe('Initialize peer review checks', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should save material check', async () => {
    await useCases.initializePeerReviewChecksUseCase
      .initialize({ models, factories })
      .execute(manuscriptId)

    expect(Manuscript.updateBy).toHaveBeenCalledTimes(1)
    expect(Manuscript.updateBy).toHaveBeenCalledWith({
      queryObject: {
        id: manuscriptId,
      },
      propertiesToUpdate: {
        peerReviewCheckConfigId,
      },
    })

    expect(ManuscriptPeerReviewCheck.insertGraph).toHaveBeenCalledTimes(1)
    expect(ManuscriptPeerReviewCheck.insertGraph).toHaveBeenNthCalledWith(1, [
      {
        manuscriptId,
        name: 'conflictOfInterestMention',
        observation: '',
        selectedOption: null,
      },
      {
        manuscriptId,
        name: 'headersAndFootersApproved',
        observation: '',
        selectedOption: null,
      },
    ])
  })
})
