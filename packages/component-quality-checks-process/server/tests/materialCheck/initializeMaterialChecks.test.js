const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const { factories } = require('../../../../component-model')

const { Manuscript, MaterialCheckConfig, ManuscriptMaterialCheck } = models

const chance = new Chance()

const manuscriptId = chance.guid()
const materialCheckConfigId = chance.guid()

const materialChecksConfig = {
  id: materialCheckConfigId,
  config: {
    frontPageApproved: {
      options: [
        {
          value: 'yes',
          supportsAdditionalInfo: false,
        },
        {
          value: 'no',
          supportsAdditionalInfo: true,
          type: 'text',
          label: 'observation',
        },
        {
          value: 'notRequired',
          supportsAdditionalInfo: false,
        },
      ],
      order: 1,
    },
    headersAndFootersApproved: {
      options: [
        {
          value: 'yes',
          supportsAdditionalInfo: false,
        },
        {
          value: 'no',
          supportsAdditionalInfo: true,
          type: 'text',
          label: 'observation',
        },
      ],
      order: 2,
    },
  },
}
MaterialCheckConfig.getLatest = jest
  .fn()
  .mockResolvedValue(materialChecksConfig)

Manuscript.updateBy = jest.fn()

ManuscriptMaterialCheck.insertGraph = jest.fn()

describe('Initialize material checks', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should save material check', async () => {
    await useCases.initializeMaterialChecksUseCase
      .initialize({ models, factories })
      .execute(manuscriptId)

    expect(Manuscript.updateBy).toHaveBeenCalledTimes(1)
    expect(Manuscript.updateBy).toHaveBeenCalledWith({
      queryObject: {
        id: manuscriptId,
      },
      propertiesToUpdate: {
        materialCheckConfigId,
      },
    })

    expect(ManuscriptMaterialCheck.insertGraph).toHaveBeenCalledTimes(1)
    expect(ManuscriptMaterialCheck.insertGraph).toHaveBeenNthCalledWith(1, [
      {
        manuscriptId,
        name: 'frontPageApproved',
        additionalInfo: '',
        selectedOption: null,
      },
      {
        manuscriptId,
        name: 'headersAndFootersApproved',
        additionalInfo: '',
        selectedOption: null,
      },
    ])
  })
})
