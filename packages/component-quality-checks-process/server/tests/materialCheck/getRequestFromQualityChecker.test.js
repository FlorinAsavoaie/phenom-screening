const configService = require('config')
const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')

const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')

const isShortArticleSupportedUseCase = isShortArticleSupported.initialize({
  configService,
})

const { Author, Member, Manuscript, ManuscriptValidations, Note } = models

const manuscriptId = 'manuscript-id'

describe('Get Request From Quality Checker Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    const mockedMember = {
      id: 'member-id',
      manuscriptId: 'manuscript-id',
      surname: 'Jane',
      givenNames: 'Doe',
      title: 'dr',
      email: 'member@test.com',
      aff: 'Affiliation',
      country: 'UK',
      role: 'submittingStaffMember',
      roleLabel: 'Submitting Staff Member',
    }

    const mockedAuthor = {
      id: 'author-id',
      manuscriptId: 'manuscript-id',
      surname: 'John',
      givenNames: 'Doe',
      title: 'dr',
      email: 'author@test.com',
      aff: 'Affiliation',
      country: 'UK',
    }

    const mockedManuscriptValidation = {
      id: 'manuscript-validation-id',
      manuscriptId: 'manuscript-id',
      includesCitations: false,
      graphical: false,
      differentAuthors: false,
      missingAffiliations: false,
      nonacademicAffiliations: false,
      missingReferences: false,
      wrongReferences: false,
      topicNotAppropriate: false,
      articleTypeIsNotAppropriate: false,
      manuscriptIsMissingSections: false,
    }

    const mockedNote = {
      id: 'note-id',
      created: '2022-06-22T03:39:37.655Z',
      manuscriptId: 'manuscript-id',
      type: 'decisionNote',
      content: '',
    }

    jest.spyOn(Member, 'getSubmittingStaffMember').mockReturnValue(mockedMember)
    jest.spyOn(Author, 'getCorrespondingAuthor').mockReturnValue(mockedAuthor)
    jest
      .spyOn(ManuscriptValidations, 'findBy')
      .mockReturnValue(mockedManuscriptValidation)

    Manuscript.find = jest.fn().mockReturnValue({
      manuscriptMaterialChecks: [],
      materialCheckConfig: {
        config: {},
        name: 'headersAndFootersApproved',
        selectedOption: null,
        additionalInfo: '',
      },
    })
    Note.findValidationNotes = jest.fn().mockReturnValue([])
    Note.findOneBy = jest.fn().mockReturnValue(mockedNote)
  })

  it('should retrieve a member if its short article type: i.e.: Corrigendum', async () => {
    const articleType = 'Corrigendum'

    await useCases.getRequestFromQualityCheckerUseCase
      .initialize({
        models,
        useCases: {
          isShortArticleSupportedUseCase,
        },
      })
      .execute(manuscriptId, articleType)

    expect(Member.getSubmittingStaffMember).toHaveBeenCalledTimes(1)
    expect(Author.getCorrespondingAuthor).toHaveBeenCalledTimes(0)
  })

  it('should retrieve a member if its short article type: i.e.: Erratum', async () => {
    const articleType = 'Erratum'

    await useCases.getRequestFromQualityCheckerUseCase
      .initialize({
        models,
        useCases: {
          isShortArticleSupportedUseCase,
        },
      })
      .execute(manuscriptId, articleType)

    expect(Member.getSubmittingStaffMember).toHaveBeenCalledTimes(1)
    expect(Author.getCorrespondingAuthor).toHaveBeenCalledTimes(0)
  })

  it('should retrieve a member if its short article type: i.e.: Expression of Concern', async () => {
    const articleType = 'Expression of Concern'

    await useCases.getRequestFromQualityCheckerUseCase
      .initialize({
        models,
        useCases: {
          isShortArticleSupportedUseCase,
        },
      })
      .execute(manuscriptId, articleType)

    expect(Member.getSubmittingStaffMember).toHaveBeenCalledTimes(1)
    expect(Author.getCorrespondingAuthor).toHaveBeenCalledTimes(0)
  })

  it('should retrieve a member if its short article type: i.e.: Retraction', async () => {
    const articleType = 'Retraction'

    await useCases.getRequestFromQualityCheckerUseCase
      .initialize({
        models,
        useCases: {
          isShortArticleSupportedUseCase,
        },
      })
      .execute(manuscriptId, articleType)

    expect(Member.getSubmittingStaffMember).toHaveBeenCalledTimes(1)
    expect(Author.getCorrespondingAuthor).toHaveBeenCalledTimes(0)
  })

  it('should retrieve a member if its short article type: i.e.: Editorial', async () => {
    const articleType = 'Editorial'

    await useCases.getRequestFromQualityCheckerUseCase
      .initialize({
        models,
        useCases: {
          isShortArticleSupportedUseCase,
        },
      })
      .execute(manuscriptId, articleType)

    expect(Member.getSubmittingStaffMember).toHaveBeenCalledTimes(1)
    expect(Author.getCorrespondingAuthor).toHaveBeenCalledTimes(0)
  })

  it('should retrieve a member if its short article type: i.e.: Letter to the Editor', async () => {
    const articleType = 'Letter to the Editor'

    await useCases.getRequestFromQualityCheckerUseCase
      .initialize({
        models,
        useCases: {
          isShortArticleSupportedUseCase,
        },
      })
      .execute(manuscriptId, articleType)

    expect(Member.getSubmittingStaffMember).toHaveBeenCalledTimes(1)
    expect(Author.getCorrespondingAuthor).toHaveBeenCalledTimes(0)
  })

  it('should retrieve an author if its not a short article type: i.e.: Research Article', async () => {
    const articleType = 'Research Article'

    await useCases.getRequestFromQualityCheckerUseCase
      .initialize({
        models,
        useCases: {
          isShortArticleSupportedUseCase,
        },
      })
      .execute(manuscriptId, articleType)

    expect(Author.getCorrespondingAuthor).toHaveBeenCalledTimes(1)
    expect(Member.getSubmittingStaffMember).toHaveBeenCalledTimes(0)
  })
})
