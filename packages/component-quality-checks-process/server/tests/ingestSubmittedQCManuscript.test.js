const configService = require('config')
const models = require('@pubsweet/models')

const { factories } = require('../../../component-model')
const useCases = require('../src/use-cases')

const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

const { Manuscript } = models

const s3Service = {
  uploadReviewFileToScreeningS3: jest.fn(async () => {}),
}

const mockedUseCases = {
  initializeMaterialChecksUseCase: {
    execute: jest.fn(),
  },
  isShortArticleSupportedUseCase: isShortArticleSupported.initialize({
    configService,
  }),
  ingestManuscriptAuthorsEmailsUseCase: {
    execute: jest.fn(),
  },
  deleteManuscriptAuthorsEmailsUseCase: {
    execute: jest.fn(),
  },
}

const submissionId = 'submission-id'

const generateManuscriptVersion = ({ version }) => ({
  id: 'manuscript-id',
  journalId: 'journal-1',
  journalSectionId: null,
  specialIssueId: null,
  abstract: 'long abstract',
  submissionCreatedDate: '2051-12-21T14:26:23.554Z',
  qualityChecksSubmittedDate: '2051-12-21T14:26:23.554Z',
  conflictOfInterest: null,
  dataAvailability: null,
  fundingStatement: null,
  title: 'title',
  articleType: {
    name: 'review',
  },
  customId: '1256186',
  version,
  editors: [
    {
      id: 'editor-1',
      email: 'joanarudolf1@hindawi.com',
      aff: 'Hindawi',
      country: 'Romania',
      givenNames: 'Joana',
      surname: 'Rudolph',
      title: 'Prof',
      role: {
        type: 'triageEditor',
        label: 'Chief Editor',
      },
      status: 'active',
      reviewerNumber: null,
    },
    {
      id: 'editor-2',
      email: 'joanarudolf2@hindawi.com',
      aff: 'Hindawi',
      country: 'Romania',
      givenNames: 'Joana2',
      surname: 'Rudolph2',
      title: 'Prof',
      role: {
        type: 'academicEditor',
        label: 'Academic Editor',
      },
      status: 'active',
      reviewerNumber: null,
    },
  ],
  files: [
    {
      providerKey: '1234-4321-1234-4321',
      type: 'manuscript',
      originalName: 'Test File',
      fileName: 'Test File',
      size: '100',
      mimeType: 'pdf',
    },
    {
      providerKey: '1234-4321-1234-4322',
      type: 'cover letter',
      originalName: 'Test File 2',
      fileName: 'Test File 2',
      size: '100',
      mimeType: 'pdf',
    },
  ],
  authors: [
    {
      isSubmitting: true,
      isCorresponding: true,
      position: 0,
      id: 'authorId1',
      email: 'something1@test.com',
      affiliation: { name: 'Hindawi' },
      country: 'Romania',
      givenNames: 'John1',
      surname: 'Doe',
      title: 'Prof',
    },
    {
      isSubmitting: true,
      isCorresponding: true,
      position: 0,
      id: 'authorId2',
      email: 'something2@test.com',
      affiliation: { name: 'Hindawi' },
      country: 'Romania',
      givenNames: 'John2',
      surname: 'Doe',
      title: 'Prof',
    },
  ],
  submittingStaffMembers: [
    {
      id: 'member-id',
      manuscriptId: 'member-manuscript-id',
      surname: 'Jane',
      givenNames: 'Doe',
      aff: 'Affiliation',
      country: 'UK',
      title: 'dr',
      email: 'member@test.com',
      role: 'submittingStaffMember',
      status: 'pending',
    },
  ],
})

const manuscriptVersions = ['1', '2', '2.9', '2.10']

const manuscripts = manuscriptVersions.map((version) =>
  generateManuscriptVersion({ version }),
)

Manuscript.getLatestManuscriptVersion = jest.fn()
Manuscript.insertGraph = jest.fn()
Manuscript.upsertGraph = jest.fn()

describe('Ingest Submitted QC Manuscript Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should not ingest manuscript if the same version is already ingested', async () => {
    const previousManuscriptVersion = manuscripts[manuscripts.length - 1]

    jest
      .spyOn(Manuscript, 'getLatestManuscriptVersion')
      .mockResolvedValueOnce(previousManuscriptVersion)

    const result = useCases.ingestSubmittedQCManuscriptUseCase
      .initialize({ models, factories, s3Service, useCases: mockedUseCases })
      .execute({ submissionId, manuscripts })

    await expect(result).rejects.toThrow(
      `Manuscript with submissionId:${submissionId}, version:${previousManuscriptVersion.version} is already ingested.`,
    )
  })

  it('should save the manuscript with all information', async () => {
    const previousManuscriptVersion = manuscripts[manuscripts.length - 2]
    const manuscriptVersionToSave = {
      submissionId: 'submission-id',
      phenomId: 'manuscript-id',
      customId: '1256186',
      title: 'title',
      abstract: 'long abstract',
      articleType: 'review',
      submissionDate: '2051-12-21T14:26:23.554Z',
      qualityChecksSubmittedDate: '2051-12-21T14:26:23.554Z',
      version: '2.10',
      conflictOfInterest: null,
      dataAvailability: null,
      fundingStatement: null,
      journalId: 'journal-1',
      journalSectionId: undefined,
      specialIssueId: null,
      step: 'inMaterialsChecking',
      flowType: 'qualityCheck',
      authors: [
        {
          isSubmitting: true,
          aff: 'Hindawi',
          email: 'something1@test.com',
          country: 'Romania',
          surname: 'Doe',
          givenNames: 'John1',
          title: 'Prof',
          isCorresponding: true,
          position: 0,
          orcid: null,
        },
        {
          isSubmitting: true,
          aff: 'Hindawi',
          email: 'something2@test.com',
          country: 'Romania',
          surname: 'Doe',
          givenNames: 'John2',
          title: 'Prof',
          isCorresponding: true,
          position: 0,
          orcid: null,
        },
      ],
      files: [
        {
          type: 'manuscript',
          fileName: 'Test File',
          originalName: 'Test File',
          originalFileProviderKey: '1234-4321-1234-4321',
          providerKey: null,
          size: '100',
          mimeType: 'pdf',
        },
        {
          type: 'cover letter',
          fileName: 'Test File 2',
          originalName: 'Test File 2',
          originalFileProviderKey: '1234-4321-1234-4322',
          providerKey: null,
          size: '100',
          mimeType: 'pdf',
        },
      ],
      manuscriptValidations: {},
      manuscriptChecks: {},
      members: [
        {
          surname: 'Rudolph',
          givenNames: 'Joana',
          aff: 'Hindawi',
          country: 'Romania',
          title: 'Prof',
          reviewerNumber: null,
          phenomMemberId: 'editor-1',
          email: 'joanarudolf1@hindawi.com',
          role: 'triageEditor',
          roleLabel: 'Chief Editor',
          reviews: [],
          status: 'active',
        },
        {
          surname: 'Rudolph2',
          givenNames: 'Joana2',
          aff: 'Hindawi',
          country: 'Romania',
          title: 'Prof',
          reviewerNumber: null,
          phenomMemberId: 'editor-2',
          email: 'joanarudolf2@hindawi.com',
          role: 'academicEditor',
          roleLabel: 'Academic Editor',
          reviews: [],
          status: 'active',
        },
      ],
      authorResponded: true,
    }

    const manuscriptFile = {
      id: 'file-id',
      originalFileProviderKey: 'provider-key',
    }

    const manuscriptVersionSaved = {
      ...manuscriptVersionToSave,
      id: 'manuscript-id',
      getManuscriptFile: jest.fn().mockReturnValue(manuscriptFile),
    }

    jest
      .spyOn(Manuscript, 'getLatestManuscriptVersion')
      .mockResolvedValueOnce(previousManuscriptVersion)

    jest
      .spyOn(Manuscript, 'insertGraph')
      .mockResolvedValueOnce(manuscriptVersionSaved)

    await useCases.ingestSubmittedQCManuscriptUseCase
      .initialize({ models, factories, s3Service, useCases: mockedUseCases })
      .execute({ submissionId, manuscripts })

    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenCalledTimes(2)

    expect(Manuscript.insertGraph).toHaveBeenCalledTimes(1)
    expect(Manuscript.insertGraph).toHaveBeenCalledWith(manuscriptVersionToSave)

    expect(
      mockedUseCases.ingestManuscriptAuthorsEmailsUseCase.execute,
    ).toHaveBeenCalledTimes(1)

    expect(Manuscript.upsertGraph).toHaveBeenCalledTimes(1)
    expect(Manuscript.upsertGraph).toHaveBeenCalledWith({
      ...previousManuscriptVersion,
      status: Manuscript.Statuses.OLDER_VERSION,
      authorResponded: false,
    })

    expect(
      mockedUseCases.deleteManuscriptAuthorsEmailsUseCase.execute,
    ).toHaveBeenCalledTimes(1)
    expect(
      mockedUseCases.deleteManuscriptAuthorsEmailsUseCase.execute,
    ).toHaveBeenCalledWith({ manuscriptId: previousManuscriptVersion.id })

    expect(
      mockedUseCases.initializeMaterialChecksUseCase.execute,
    ).toHaveBeenCalledTimes(1)
    expect(
      mockedUseCases.initializeMaterialChecksUseCase.execute,
    ).toHaveBeenCalledWith(manuscriptVersionSaved.id)

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'ManuscriptIngested',
      data: {
        authors: manuscriptVersionSaved.authors,
        fileId: manuscriptFile.id,
        manuscriptId: manuscriptVersionSaved.id,
        originalFileProviderKey: manuscriptFile.originalFileProviderKey,
      },
    })
  })
})
