process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const configService = require('config')
const Chance = require('chance')
const models = require('@pubsweet/models')

const useCases = require('../../src/use-cases')

const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')

const chance = new Chance()
const { Note } = models

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

const notificationService = {
  notifyAuthorWhenFilesAreRequested: jest.fn(),
}

const publishAuthorConfirmedUseCase = {
  execute: jest.fn(),
}
const getRequestFromQualityCheckerUseCase = {
  execute: jest.fn(),
}

const isShortArticleSupportedUseCase = isShortArticleSupported.initialize({
  configService,
})

const manuscriptId = chance.guid()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const mockUpdateProperties = jest.fn()
const mockSave = jest.fn()

describe('Request Files Use Case', () => {
  let Manuscript
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript

    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
    jest
      .spyOn(getRequestFromQualityCheckerUseCase, 'execute')
      .mockResolvedValueOnce({
        correspondingAuthor: 'some-correspondingAuthor',
        manuscript: 'some-manuscript',
        checkerObservations: 'some-checker-observation',
        additionalComments: 'some-additional-comments',
      })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.PAUSED,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    await expect(result).rejects.toThrow(
      `You cannot request files for manuscript: ${manuscriptId} on status: ${manuscriptMock.status}.`,
    )
  })
  it(`should throw error if manuscript is not in 'inMaterialsChecking' step`, async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    await expect(result).rejects.toThrow(
      `You cannot request files for manuscript: ${manuscriptId} on step: ${manuscriptMock.step}.`,
    )
  })

  it('should change status of the manuscript in "filesRequested"', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
          isShortArticleSupportedUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(mockUpdateProperties).toHaveBeenCalledWith({
      status: Manuscript.Statuses.FILES_REQUESTED,
    })
    expect(mockSave).toHaveBeenCalled()
    expect(mockSaveNote).toHaveBeenCalled()
  })

  it('should notify author when files request', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
          isShortArticleSupportedUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(
      notificationService.notifyAuthorWhenFilesAreRequested,
    ).toHaveBeenCalledTimes(1)
  })

  it('should notify author when files request if its a supported short article type i.e.: Corrigendum', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Corrigendum',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(
      notificationService.notifyAuthorWhenFilesAreRequested,
    ).toHaveBeenCalledTimes(1)
  })

  it('should notify author when files request if its a supported short article type i.e.: Erratum', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Erratum',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(
      notificationService.notifyAuthorWhenFilesAreRequested,
    ).toHaveBeenCalledTimes(1)
  })

  it('should notify author when files request if its a supported short article type i.e.: Expression of Concern', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Expression of Concern',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(
      notificationService.notifyAuthorWhenFilesAreRequested,
    ).toHaveBeenCalledTimes(1)
  })

  it('should notify author when files request if its a supported short article type i.e.: Retraction', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Retraction',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(
      notificationService.notifyAuthorWhenFilesAreRequested,
    ).toHaveBeenCalledTimes(1)
  })

  it('should publish "SubmissionQualityCheckFilesRequested" event', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
          isShortArticleSupportedUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(global.applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionQualityCheckFilesRequested',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      authors: [
        {
          isCorresponding: true,
          surname: 'Johnson',
          givenNames: 'John',
        },
      ],
      manuscriptMaterialChecks: [],
      manuscriptValidations: [],
      materialCheckConfig: { config: [] },
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    const mockSaveNote = jest.fn()

    models.Note = jest.fn().mockImplementation(() => ({
      save: mockSaveNote,
    }))
    models.Note.Types = jest
      .fn()
      .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })
    models.Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.requestFilesUseCase
      .initialize({
        models,
        mappers,
        useCases: {
          publishAuthorConfirmedUseCase,
          getRequestFromQualityCheckerUseCase,
          isShortArticleSupportedUseCase,
        },
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })
})
