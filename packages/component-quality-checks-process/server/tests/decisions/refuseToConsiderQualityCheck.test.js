const Chance = require('chance')
const configService = require('config')
const { Manuscript, Author, User, Member } = require('@pubsweet/models')

const useCases = require('../../src/use-cases')

const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')

const chance = new Chance()

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

const notificationService = {
  notifyThatManuscriptWasRefused: jest.fn(),
}
const isShortArticleSupportedUseCase = isShortArticleSupported.initialize({
  configService,
})

const manuscriptId = chance.guid()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const mockSaveNote = jest.fn()

const factories = {
  noteFactory: {
    createDecisionNote: jest.fn(() => ({
      save: mockSaveNote,
    })),
  },
}

const screeningUseCases = {
  publishAuthorConfirmedUseCase: {
    execute: jest.fn(),
  },
  isShortArticleSupportedUseCase,
}

const mockUpdateProperties = jest.fn()
const mockSave = jest.fn()

describe('Refuse quality checks Use Case', () => {
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
    jest.spyOn(Author, 'getCorrespondingAuthor').mockResolvedValueOnce({})
    jest.spyOn(User, 'find').mockResolvedValueOnce({})
    jest.spyOn(Member, 'getEditorialAssistant').mockResolvedValueOnce({})
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      submissionId,
      status: Manuscript.Statuses.PAUSED,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.refuseToConsiderQualityCheckUseCase
      .initialize({
        models: { Manuscript, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    await expect(result).rejects.toThrow(
      `You cannot refuse manuscript: ${manuscriptId} on status: ${manuscriptMock.status}.`,
    )
  })
  it('should throw error if manuscript is not in `inPeerReviewCycleChecking`', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      submissionId,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.refuseToConsiderQualityCheckUseCase
      .initialize({
        models: { Manuscript, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    await expect(result).rejects.toThrow(
      `You cannot refuse manuscript: ${manuscriptId} on step: ${manuscriptMock.step}.`,
    )
  })

  it('should change status of the manuscript in "rejected"', async () => {
    const manuscriptMock = {
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      submissionId,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    await useCases.refuseToConsiderQualityCheckUseCase
      .initialize({
        models: { Manuscript, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, journal]',
    )

    expect(mockSaveNote).toHaveBeenCalled()

    expect(
      notificationService.notifyThatManuscriptWasRefused,
    ).toHaveBeenCalledTimes(1)
  })

  it('should notify EA', async () => {
    const manuscriptMock = {
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      submissionId,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    await useCases.refuseToConsiderQualityCheckUseCase
      .initialize({
        models: { Manuscript, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(
      notificationService.notifyThatManuscriptWasRefused,
    ).toHaveBeenCalledTimes(1)
  })

  it('should notify the submitting staff member if short article', async () => {
    const staffMember = {
      email: chance.email(),
      surname: 'Admin',
      givenNames: 'Admin',
    }
    const manuscriptMock = {
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      submittingStaffMembers: [staffMember],
      submissionId,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      articleType: 'Corrigendum',
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])
    Member.getSubmittingStaffMember = jest.fn().mockReturnValue(staffMember)

    await useCases.refuseToConsiderQualityCheckUseCase
      .initialize({
        models: { Manuscript, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(
      notificationService.notifyThatManuscriptWasRefused,
    ).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyThatManuscriptWasRefused,
    ).toHaveBeenCalledWith(
      expect.objectContaining({ emailRecipient: staffMember }),
    )
  })

  it('should publish "SubmissionQualityCheckRTCd" event', async () => {
    const manuscriptMock = {
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      submissionId,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    await useCases.refuseToConsiderQualityCheckUseCase
      .initialize({
        models: { Manuscript, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionQualityCheckRTCd',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const manuscriptMock = {
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      submissionId,
      status: Manuscript.Statuses.IN_PROGRESS,
      step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    await useCases.refuseToConsiderQualityCheckUseCase
      .initialize({
        models: { Manuscript, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })
})
