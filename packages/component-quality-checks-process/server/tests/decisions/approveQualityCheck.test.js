const useCases = require('../../src/use-cases')
const models = require('@pubsweet/models')
const { mappers } = require('component-model')
const {
  isShortArticleSupported,
} = require('component-model/src/manuscript/use-cases')

const Chance = require('chance')

const chance = new Chance()

const { Manuscript, Member, ManuscriptMaterialCheck } = models
const { manuscriptMapper } = mappers

const mockUpdateProperties = jest.fn()
const mockSave = jest.fn()
const publishMessage = jest.fn()
const manuscriptInSubmission = [{ id: '123' }, { id: '124' }]

Manuscript.findBy = jest.fn().mockReturnValue(manuscriptInSubmission)
Member.getEditorialAssistant = jest.fn().mockReturnValue({
  email: 'john@doe.test',
  givenNames: 'John',
  surname: 'Doe',
})

manuscriptMapper.domainToEventModel = jest
  .fn()
  .mockImplementation((manuscript) => ({ id: `${manuscript.id}_mapped_id` }))

global.applicationEventBus = {
  publishMessage,
}
const publishAuthorConfirmedUseCase = {
  execute: jest.fn(),
}
const manuscriptId = chance.guid()
const submissionId = chance.guid()
const userId = chance.guid()

const notificationService = {
  notifyEAWhenSubmissionPassedQualityChecks: jest.fn(),
}

const configService = require('config')

const isShortArticleSupportedUseCase = isShortArticleSupported.initialize({
  configService,
})

const manuscriptDefaultMock = {
  updateProperties: mockUpdateProperties,
  save: mockSave,
  submissionId,
  id: manuscriptId,
  user: {},
  team: {
    users: [],
  },
}

const manuscriptMaterialCheckMock = {
  additionalInfo: 2,
}

describe('Approve Quality Check Use Case', () => {
  let Manuscript
  let pubSub
  let getApproveQualityCheckPromise
  let wasUpdatedByUser

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript

    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }

    getApproveQualityCheckPromise = () =>
      useCases.approveQualityCheckUseCase
        .initialize({
          models,
          mappers,
          useCases: {
            publishAuthorConfirmedUseCase,
            isShortArticleSupportedUseCase,
          },
          services: { pubSub, notificationService, configService },
        })
        .execute({ manuscriptId, userId })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      ...manuscriptDefaultMock,
      status: Manuscript.Statuses.PAUSED,
      articleType: 'Research Article',
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await expect(getApproveQualityCheckPromise()).rejects.toThrow(
      `You cannot approve manuscript: ${manuscriptId} on status: ${manuscriptMock.status}.`,
    )
  })

  it(`should throw error if manuscript is not in 'inMaterialsChecking' step`, async () => {
    const manuscriptMock = {
      ...manuscriptDefaultMock,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Research Article',
      step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await expect(getApproveQualityCheckPromise()).rejects.toThrow(
      `You cannot approve manuscript: ${manuscriptId} on step: ${manuscriptMock.step}.`,
    )
  })

  it('should change status of the manuscript in "qualityCheckApproved"', async () => {
    const manuscriptMock = {
      ...manuscriptDefaultMock,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Research Article',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
    }
    ManuscriptMaterialCheck.findOneBy = jest
      .fn()
      .mockReturnValue(manuscriptMaterialCheckMock)
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

    await getApproveQualityCheckPromise()

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[team.users, user]',
    )

    expect(mockUpdateProperties).toHaveBeenCalledWith({
      status: Manuscript.Statuses.QUALITY_CHECK_APPROVED,
    })
    expect(
      notificationService.notifyEAWhenSubmissionPassedQualityChecks,
    ).toHaveBeenCalledTimes(1)
    expect(mockSave).toHaveBeenCalled()
  })

  it('should notify EA', async () => {
    const manuscriptMock = {
      ...manuscriptDefaultMock,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Research Article',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
    }
    ManuscriptMaterialCheck.findOneBy = jest
      .fn()
      .mockReturnValue(manuscriptMaterialCheckMock)
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

    await getApproveQualityCheckPromise()

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[team.users, user]',
    )

    expect(
      notificationService.notifyEAWhenSubmissionPassedQualityChecks,
    ).toHaveBeenCalledTimes(1)
  })

  describe('should not notify EA if its not a supported short article type', () => {
    it('Corrigendum', async () => {
      const manuscriptMock = {
        ...manuscriptDefaultMock,
        status: Manuscript.Statuses.IN_PROGRESS,
        articleType: 'Corrigendum',
        step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      }
      ManuscriptMaterialCheck.findOneBy = jest
        .fn()
        .mockReturnValue(manuscriptMaterialCheckMock)
      Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
      Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

      await getApproveQualityCheckPromise()

      expect(Manuscript.find).toHaveBeenCalledWith(
        manuscriptId,
        '[team.users, user]',
      )

      expect(
        notificationService.notifyEAWhenSubmissionPassedQualityChecks,
      ).toHaveBeenCalledTimes(0)
    })

    it('Erratum', async () => {
      const manuscriptMock = {
        ...manuscriptDefaultMock,
        status: Manuscript.Statuses.IN_PROGRESS,
        articleType: 'Erratum',
        step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      }
      ManuscriptMaterialCheck.findOneBy = jest
        .fn()
        .mockReturnValue(manuscriptMaterialCheckMock)
      Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
      Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

      await getApproveQualityCheckPromise()

      expect(Manuscript.find).toHaveBeenCalledWith(
        manuscriptId,
        '[team.users, user]',
      )

      expect(
        notificationService.notifyEAWhenSubmissionPassedQualityChecks,
      ).toHaveBeenCalledTimes(0)
    })

    it('Expression of Concern', async () => {
      const manuscriptMock = {
        ...manuscriptDefaultMock,
        status: Manuscript.Statuses.IN_PROGRESS,
        articleType: 'Expression of Concern',
        step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      }
      ManuscriptMaterialCheck.findOneBy = jest
        .fn()
        .mockReturnValue(manuscriptMaterialCheckMock)
      Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
      Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

      await getApproveQualityCheckPromise()

      expect(Manuscript.find).toHaveBeenCalledWith(
        manuscriptId,
        '[team.users, user]',
      )

      expect(
        notificationService.notifyEAWhenSubmissionPassedQualityChecks,
      ).toHaveBeenCalledTimes(0)
    })

    it('Retraction', async () => {
      const manuscriptMock = {
        ...manuscriptDefaultMock,
        status: Manuscript.Statuses.IN_PROGRESS,
        articleType: 'Retraction',
        step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      }
      ManuscriptMaterialCheck.findOneBy = jest
        .fn()
        .mockReturnValue(manuscriptMaterialCheckMock)
      Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
      Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

      await getApproveQualityCheckPromise()

      expect(Manuscript.find).toHaveBeenCalledWith(
        manuscriptId,
        '[team.users, user]',
      )

      expect(
        notificationService.notifyEAWhenSubmissionPassedQualityChecks,
      ).toHaveBeenCalledTimes(0)
    })

    it('Letter to the Editor', async () => {
      const manuscriptMock = {
        ...manuscriptDefaultMock,
        status: Manuscript.Statuses.IN_PROGRESS,
        articleType: 'Letter to the Editor',
        step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      }
      ManuscriptMaterialCheck.findOneBy = jest
        .fn()
        .mockReturnValue(manuscriptMaterialCheckMock)
      Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
      Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

      await getApproveQualityCheckPromise()

      expect(Manuscript.find).toHaveBeenCalledWith(
        manuscriptId,
        '[team.users, user]',
      )

      expect(
        notificationService.notifyEAWhenSubmissionPassedQualityChecks,
      ).toHaveBeenCalledTimes(0)
    })

    it('Editorial', async () => {
      const manuscriptMock = {
        ...manuscriptDefaultMock,
        status: Manuscript.Statuses.IN_PROGRESS,
        articleType: 'Editorial',
        step: Manuscript.Steps.IN_MATERIAL_CHECKING,
      }
      ManuscriptMaterialCheck.findOneBy = jest
        .fn()
        .mockReturnValue(manuscriptMaterialCheckMock)
      Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
      Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

      await getApproveQualityCheckPromise()

      expect(Manuscript.find).toHaveBeenCalledWith(
        manuscriptId,
        '[team.users, user]',
      )

      expect(
        notificationService.notifyEAWhenSubmissionPassedQualityChecks,
      ).toHaveBeenCalledTimes(0)
    })
  })

  it('should publish event "SubmissionQualityCheckPassed"', async () => {
    const manuscriptMock = {
      ...manuscriptDefaultMock,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Research Article',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
    }

    ManuscriptMaterialCheck.findOneBy = jest
      .fn()
      .mockReturnValue(manuscriptMaterialCheckMock)
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

    await getApproveQualityCheckPromise()

    expect(Manuscript.findBy).toHaveBeenCalledWith({
      queryObject: {
        submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: '[authors, files, members]',
    })
    expect(publishAuthorConfirmedUseCase.execute).toHaveBeenCalledWith(
      manuscriptId,
    )

    expect(publishMessage).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'SubmissionQualityCheckPassed',
        data: {
          date: expect.anything(),
          figureFilesCount: expect.anything(),
          refCount: expect.anything(),
          pageCount: expect.anything(),
          ea: expect.anything(),
          es: expect.anything(),
          esLeaders: expect.anything(),
          qc: expect.anything(),
          qcLeaders: expect.anything(),
          submissionId,
          manuscripts: [
            {
              id: '123_mapped_id',
            },
            {
              id: '124_mapped_id',
            },
          ],
        },
      }),
    )
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const manuscriptMock = {
      ...manuscriptDefaultMock,
      status: Manuscript.Statuses.IN_PROGRESS,
      articleType: 'Research Article',
      step: Manuscript.Steps.IN_MATERIAL_CHECKING,
    }

    ManuscriptMaterialCheck.findOneBy = jest
      .fn()
      .mockReturnValue(manuscriptMaterialCheckMock)
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

    await getApproveQualityCheckPromise()

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })
})
