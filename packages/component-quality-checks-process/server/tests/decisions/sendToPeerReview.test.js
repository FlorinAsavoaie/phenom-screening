const { sendToPeerReviewUseCase } = require('../../src/use-cases')
const { Manuscript, Note, Member } = require('@pubsweet/models')

const noteFactory = {
  createNote: () => {
    throw new Error('Not implemented')
  },
}

const manuscriptMapper = {
  domainToPeerReviewCycleCheckingProcessEvent: () => {
    throw new Error('Not implemented')
  },
}

let wasUpdatedByUser = () => {
  throw new Error('Not implemented')
}

const pubSub = {
  notifyThatSubmissionStatusFor: () => ({ wasUpdatedByUser }),
}

const applicationEventBus = {
  publishMessage: () => {
    throw new Error('Not implemented')
  },
}

describe('Send To Peer Review Use Case', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  const similarSubmissions = [
    { submissionId: 'sub-1' },
    { submissionId: 'sub-2' },
  ]

  const eventData = {
    id: 'event-data-id',
  }

  const manuscript = {
    id: 'manuscript-1',
    submissionId: 'submissionId-1',
    status: Manuscript.Statuses.ESCALATED,
    step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
    notes: [],
  }

  it(`throw error if the input it's not valid`, async () => {
    const useCaseInput = {
      conflictOfInterest: {
        withAcademicEditor: false,
        withTriageEditor: false,
        withReviewerEmails: [],
      },
      improperReviewEmails: [],
      isDecisionMadeInError: false,
      manuscriptId: 'manuscript-1',
      comment: 'some reason',
      userId: 'user-23',
    }

    const result = sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    await expect(result).rejects.toThrow(`There is no option selected.`)
  })

  it('should load manuscript relations', async () => {
    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)
    jest.spyOn(noteFactory, 'createNote').mockReturnValueOnce({
      id: 'some-note-id',
    })
    jest.spyOn(Manuscript, 'upsertGraph').mockResolvedValueOnce()
    jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockResolvedValueOnce(similarSubmissions)
    jest
      .spyOn(manuscriptMapper, 'domainToPeerReviewCycleCheckingProcessEvent')
      .mockReturnValueOnce(eventData)
    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce(eventData)
    wasUpdatedByUser = jest.fn()

    const useCaseInput = {
      conflictOfInterest: {
        withAcademicEditor: false,
        withTriageEditor: false,
        withReviewerEmails: [],
      },
      improperReviewEmails: [],
      isDecisionMadeInError: true,
      manuscriptId: 'manuscript-1',
      comment: 'some reason',
      userId: 'user-23',
    }

    await sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    expect(Manuscript.find).toHaveBeenCalledTimes(1)
    expect(Manuscript.find).toHaveBeenCalledWith(
      'manuscript-1',
      `[
        team.users,
        user,
        notes,
        authors,
        members,
        manuscriptChecks,
        manuscriptValidations,
        manuscriptPeerReviewChecks,
      ]`,
    )
  })

  it('should not move manuscript if status is not escalated', async () => {
    manuscript.status = Manuscript.Statuses.inProgress

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)
    jest.spyOn(noteFactory, 'createNote').mockReturnValueOnce({
      id: 'some-note-id',
    })
    jest.spyOn(Manuscript, 'upsertGraph').mockResolvedValueOnce()
    jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockResolvedValueOnce(similarSubmissions)
    jest
      .spyOn(manuscriptMapper, 'domainToPeerReviewCycleCheckingProcessEvent')
      .mockReturnValueOnce(eventData)
    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce(eventData)
    wasUpdatedByUser = jest.fn()

    const useCaseInput = {
      manuscriptId: 'manuscript-1',
      conflictOfInterest: {
        withAcademicEditor: false,
        withTriageEditor: false,
        withReviewerEmails: [],
      },
      improperReviewEmails: [],
      isDecisionMadeInError: true,
      comment: 'some reason',
      userId: 'user-23',
    }

    const result = sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    await expect(result).rejects.toThrow(
      `You cannot return to peer review manuscript: ${manuscript.id} on status: ${manuscript.status}.`,
    )
  })

  it('should not move manuscript if steps is not Peer Review Cycle Checking', async () => {
    manuscript.status = Manuscript.Statuses.ESCALATED
    manuscript.step = Manuscript.Steps.IN_MATERIAL_CHECKING

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)
    jest.spyOn(noteFactory, 'createNote').mockReturnValueOnce({
      id: 'some-note-id',
    })
    jest.spyOn(Manuscript, 'upsertGraph').mockResolvedValueOnce()
    jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockResolvedValueOnce(similarSubmissions)
    jest
      .spyOn(manuscriptMapper, 'domainToPeerReviewCycleCheckingProcessEvent')
      .mockReturnValueOnce(eventData)
    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce(eventData)
    wasUpdatedByUser = jest.fn()

    const useCaseInput = {
      manuscriptId: 'manuscript-1',
      conflictOfInterest: {
        withAcademicEditor: false,
        withTriageEditor: false,
        withReviewerEmails: [],
      },
      improperReviewEmails: [],
      isDecisionMadeInError: true,
      comment: 'some reason',
      userId: 'user-23',
    }

    const result = sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    await expect(result).rejects.toThrow(
      `You cannot return to peer review manuscript: ${manuscript.id} on step: ${manuscript.step}.`,
    )
  })

  it('should move manuscript if status and step are valid', async () => {
    manuscript.status = Manuscript.Statuses.ESCALATED
    manuscript.step = Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING
    manuscript.notes = []

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)
    jest.spyOn(noteFactory, 'createNote').mockReturnValueOnce({
      id: 'some-note-id',
    })
    jest.spyOn(Manuscript, 'upsertGraph').mockResolvedValueOnce()
    jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockResolvedValueOnce(similarSubmissions)
    jest
      .spyOn(manuscriptMapper, 'domainToPeerReviewCycleCheckingProcessEvent')
      .mockReturnValueOnce(eventData)
    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce(eventData)
    wasUpdatedByUser = jest.fn()

    const useCaseInput = {
      manuscriptId: 'manuscript-1',
      conflictOfInterest: {
        withAcademicEditor: false,
        withTriageEditor: false,
        withReviewerEmails: [],
      },
      improperReviewEmails: [],
      isDecisionMadeInError: true,
      comment: 'some reason',
      userId: 'user-23',
    }

    await sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    expect(noteFactory.createNote).toHaveBeenCalledTimes(1)
    expect(noteFactory.createNote).toHaveBeenCalledWith({
      manuscriptId: 'manuscript-1',
      type: Note.Types.SEND_TO_PEER_REVIEW_NOTE,
      content: useCaseInput.comment,
    })

    expect(Manuscript.upsertGraph).toHaveBeenCalledTimes(1)
    expect(Manuscript.upsertGraph).toHaveBeenCalledWith({
      id: 'manuscript-1',
      submissionId: 'submissionId-1',
      status: Manuscript.Statuses.SENT_TO_PEER_REVIEW,
      step: Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING,
      notes: [
        {
          id: 'some-note-id',
        },
      ],
    })
  })

  it('should publish event', async () => {
    manuscript.status = Manuscript.Statuses.ESCALATED
    manuscript.step = Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING
    manuscript.notes = []

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)
    jest.spyOn(noteFactory, 'createNote').mockReturnValueOnce({
      id: 'some-note-id',
    })
    jest.spyOn(Manuscript, 'upsertGraph').mockResolvedValueOnce()
    jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockResolvedValueOnce(similarSubmissions)
    jest
      .spyOn(manuscriptMapper, 'domainToPeerReviewCycleCheckingProcessEvent')
      .mockReturnValueOnce(eventData)
    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce(eventData)
    wasUpdatedByUser = jest.fn()

    const conflictOfInterest = {
      withAcademicEditor: false,
      withTriageEditor: false,
      withReviewerEmails: [],
    }

    const improperReviewEmails = []
    const improperReviewReviewerIds = []

    const useCaseInput = {
      manuscriptId: 'manuscript-1',
      conflictOfInterest,
      improperReviewEmails,
      isDecisionMadeInError: true,
      comment: 'some reason',
      userId: 'user-23',
    }

    await sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    expect(Manuscript.getSimilarSubmissions).toHaveBeenCalledTimes(1)
    expect(Manuscript.getSimilarSubmissions).toHaveBeenCalledWith(manuscript)

    expect(
      manuscriptMapper.domainToPeerReviewCycleCheckingProcessEvent,
    ).toHaveBeenCalledTimes(1)
    expect(
      manuscriptMapper.domainToPeerReviewCycleCheckingProcessEvent,
    ).toHaveBeenCalledWith({
      manuscript: {
        id: 'manuscript-1',
        notes: [
          {
            id: 'some-note-id',
          },
        ],
        status: 'sentToPeerReview',
        step: 'inPeerReviewCycleChecking',
        submissionId: 'submissionId-1',
      },
      conflictOfInterest,
      isDecisionMadeInError: true,
      improperReviewReviewerIds,
      similarSubmissions: [
        {
          submissionId: 'sub-1',
        },
        {
          submissionId: 'sub-2',
        },
      ],
      comment: useCaseInput.comment,
    })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'PeerReviewCycleCheckingProcessSentToPeerReview',
      data: eventData,
    })
  })

  it('should notify that submission status was updated', async () => {
    manuscript.status = Manuscript.Statuses.ESCALATED
    manuscript.step = Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING
    manuscript.notes = []

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)
    jest.spyOn(noteFactory, 'createNote').mockReturnValueOnce({
      id: 'some-note-id',
    })
    jest.spyOn(Manuscript, 'upsertGraph').mockResolvedValueOnce()
    jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockResolvedValueOnce(similarSubmissions)
    jest
      .spyOn(manuscriptMapper, 'domainToPeerReviewCycleCheckingProcessEvent')
      .mockReturnValueOnce(eventData)
    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce(eventData)
    wasUpdatedByUser = jest.fn()

    const useCaseInput = {
      manuscriptId: 'manuscript-1',
      conflictOfInterest: {
        withAcademicEditor: false,
        withTriageEditor: false,
        withReviewerEmails: [],
      },
      improperReviewEmails: [],
      isDecisionMadeInError: true,
      comment: 'some reason',
      userId: 'user-23',
    }

    await sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(useCaseInput.userId)
  })

  it('should take reviewer members from all versions of manuscript', async () => {
    manuscript.status = Manuscript.Statuses.ESCALATED
    manuscript.step = Manuscript.Steps.IN_PEER_REVIEW_CYCLE_CHECKING
    manuscript.notes = []

    jest.spyOn(Manuscript, 'find').mockResolvedValueOnce(manuscript)
    jest.spyOn(noteFactory, 'createNote').mockReturnValueOnce({
      id: 'some-note-id',
    })
    jest.spyOn(Manuscript, 'upsertGraph').mockResolvedValueOnce()
    jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockResolvedValueOnce(similarSubmissions)
    jest
      .spyOn(Manuscript, 'findBy')
      .mockResolvedValueOnce([{ id: 'manuscript-1' }, { id: 'manuscript-2' }])
      .mockResolvedValueOnce([{ id: 'manuscript-1' }, { id: 'manuscript-2' }])

    jest
      .spyOn(Member, 'findAllReviewersBy')
      .mockResolvedValueOnce([
        'reviewer1_man1@email.com',
        'reviewer1_man2@email.com',
      ])
      .mockResolvedValueOnce([
        'reviewer1_man1@email.com',
        'reviewer1_man2@email.com',
        'reviewer2_man1@email.com',
        'reviewer2_man2@email.com',
      ])

    jest
      .spyOn(manuscriptMapper, 'domainToPeerReviewCycleCheckingProcessEvent')
      .mockReturnValueOnce(eventData)
    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce(eventData)
    wasUpdatedByUser = jest.fn()

    const useCaseInput = {
      manuscriptId: 'manuscript-1',
      conflictOfInterest: {
        withAcademicEditor: false,
        withTriageEditor: false,
        withReviewerEmails: ['reviewer1@email.com'],
      },
      improperReviewEmails: ['reviewer1@email.com', 'reviewer2@email.com'],

      isDecisionMadeInError: true,
      comment: 'some reason',
      userId: 'user-23',
    }

    await sendToPeerReviewUseCase
      .initialize({
        models: {
          Manuscript,
          Note,
          Member,
        },
        factories: {
          noteFactory,
        },
        mappers: {
          manuscriptMapper,
        },
        services: {
          pubSub,
          applicationEventBus,
        },
      })
      .execute(useCaseInput)

    expect(Manuscript.findBy).toHaveBeenCalledTimes(2)
    expect(Manuscript.findBy).toHaveBeenCalledWith({
      queryObject: {
        submissionId: manuscript.submissionId,
        flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      },
    })

    expect(Member.findAllReviewersBy).toHaveBeenCalledTimes(2)
    expect(Member.findAllReviewersBy).toHaveBeenNthCalledWith(
      1,
      ['manuscript-1', 'manuscript-2'],
      ['reviewer1@email.com'],
    )

    expect(Member.findAllReviewersBy).toHaveBeenNthCalledWith(
      2,
      ['manuscript-1', 'manuscript-2'],
      ['reviewer1@email.com', 'reviewer2@email.com'],
    )
  })
})
