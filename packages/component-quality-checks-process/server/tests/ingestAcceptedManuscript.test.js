const models = require('@pubsweet/models')
const { factories } = require('../../../component-model')
const useCases = require('../src/use-cases')
const Chance = require('chance')

const chance = new Chance()
const { Manuscript, Journal } = models

const journalId = chance.guid()
const submissionId = chance.guid()

const generateInput = () => ({
  submissionId,
  manuscripts: [
    {
      id: '123-v2-321',
      journalId,
      abstract: chance.paragraph(),
      title: chance.sentence(),
      articleType: {
        name: 'review',
      },
      customId: '1256186',
      version: 2,
      editors: [
        {
          id: chance.guid(),
          email: chance.email(),
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Joana',
          surname: 'Rudolph',
          title: 'Prof',
          role: {
            type: 'triageEditor',
            label: 'Chief Editor',
          },
        },
        {
          id: chance.guid(),
          email: chance.email(),
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Leo',
          surname: 'Milo',
          title: 'Prof',
          role: {
            type: 'academicEditor',
            label: 'Academic Editor',
          },
        },
      ],

      files: [
        {
          providerKey: '1234-4321-1234-4321',
          type: 'manuscript',
          originalName: 'Test File',
          fileName: 'Test File',
        },
      ],
      authors: [
        {
          isSubmitting: true,
          isCorresponding: true,
          position: 0,
          id: 'authorId',
          email: 'something@test.com',
          affiliation: { name: 'Hindawi' },
          country: 'Romania',
          givenNames: 'John',
          surname: 'Doe',
          title: 'Prof',
        },
      ],
      reviewers: [
        {
          id: 'reviewerId',
          email: chance.email(),
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Gigi',
          surname: 'Ionescu',
          title: 'Prof',
        },
      ],
      reviews: [
        {
          manuscriptId: '123-v2-321',
          teamMemberId: 'authorId',
          recommendation: 'responseToRevision',
          comments: [],
        },
        {
          manuscriptId: '123-v2-321',
          teamMemberId: 'reviewerId',
          recommendation: 'publish',
          comments: [],
        },
      ],
    },
    {
      id: '123-v1-123',
      abstract: chance.paragraph(),
      title: chance.sentence(),
      articleType: {
        name: 'review',
      },
      customId: '1256186',
      version: 1,
      journal: {
        id: chance.guid(),
        name: 'Journal of Toxicology - CM',
        publisherName: null,
      },
      editors: [
        {
          id: chance.guid(),
          email: chance.email(),
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Joana',
          surname: 'Rudolph',
          title: 'Prof',
          role: {
            type: 'triageEditor',
            label: 'Chief Editor',
          },
        },
        {
          id: chance.guid(),
          email: chance.email(),
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Leo',
          surname: 'Milo',
          title: 'Prof',
          role: {
            type: 'academicEditor',
            label: 'Academic Editor',
          },
        },
      ],
      files: [
        {
          providerKey: '1234-4511-1234-4861',
          type: 'manuscript',
          originalName: 'Test File',
          fileName: 'Test File',
        },
      ],
      authors: [
        {
          isSubmitting: true,
          isCorresponding: true,
          position: 0,
          email: 'something@test.com',
          affiliation: { name: 'Hindawi' },
          country: 'Romania',
          givenNames: 'John',
          surname: 'Doe',
          title: 'Prof',
        },
        {
          isSubmitting: false,
          isCorresponding: false,
          position: 1,
          email: 'somethingElse@test.com',
          affiliation: { name: 'Hindawi' },
          country: 'Romania',
          givenNames: 'Chris',
          surname: 'Johnson',
          title: 'Mr',
        },
      ],
      reviewers: [
        {
          id: 'reviewerId',
          email: chance.email(),
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Gigi',
          surname: 'Ionescu',
          title: 'Prof',
        },
      ],
      reviews: [
        {
          manuscriptId: '123-v2-321',
          teamMemberId: 'reviewerId',
          recommendation: 'publish',
          comments: [],
        },
      ],
    },
  ],
})

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

describe('Ingest Accepted Manuscript Use Case', () => {
  let manuscriptsInput
  beforeEach(async () => {
    manuscriptsInput = generateInput()
  })

  Journal.find = jest.fn().mockReturnValue({
    id: chance.guid(),
    name: 'Journal of Toxicology - CM',
  })

  const savedManuscripts = [
    {
      id: 'id-from-manuscript-v2',
      authors: [
        {
          id: chance.guid(),
          isSubmitting: true,
          isCorresponding: true,
          position: 0,
          email: 'something@test.com',
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'John',
          surname: 'Doe',
          title: 'Prof',

          reviews: [
            {
              recommendation: 'responseToRevision',
            },
          ],
        },
      ],
      files: [
        {
          id: chance.guid(),
          originalFileProviderKey: '1234-4321-1234-4321',
          type: 'manuscript',
          originalName: 'Test File',
          fileName: 'Test File',
        },
      ],
      version: '2',
      flowType: 'qualityCheck',
      manuscriptValidations: { id: chance.guid() },
      manuscriptChecks: { id: chance.guid() },
      getManuscriptFile: jest.fn().mockReturnValue({
        id: '987654321',
        originalFileProviderKey: '1234-4321-1234-4321',
      }),
      updateProperties: jest.fn(),
      save: jest.fn(),
      editors: [{}],
    },
    {
      id: 'id-from-manuscript-v1',
      authors: [
        {
          id: chance.guid(),
          isSubmitting: true,
          isCorresponding: true,
          position: 0,
          email: 'something@test.com',
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'John',
          surname: 'Doe',
          title: 'Prof',
        },
        {
          id: chance.guid(),
          isSubmitting: false,
          isCorresponding: false,
          position: 1,
          email: 'somethingElse@test.com',
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Chris',
          surname: 'Johnson',
          title: 'Mr',
        },
      ],
      files: [
        {
          id: chance.guid(),
          originalFileProviderKey: '1234-4511-1234-4861',
          type: 'manuscript',
          originalName: 'Test File',
          fileName: 'Test File',
        },
      ],
      flowType: 'qualityCheck',
      version: '1',
      manuscriptValidations: { id: chance.guid() },
      manuscriptChecks: { id: chance.guid() },
      getManuscriptFile: jest.fn().mockReturnValue({
        id: '123456789',
        originalFileProviderKey: '1234-4511-1234-486',
      }),
      updateProperties: jest.fn(),
      save: jest.fn(),
      reviewers: [
        {
          id: 'reviewerId1',
          reviews: [
            {
              recommendation: 'publish',
            },
          ],
        },
      ],
    },
  ]

  Manuscript.insertGraph = jest.fn()
  Manuscript.updateBy = jest.fn()

  const mockedUseCases = {
    initializeMaterialChecksUseCase: {
      execute: jest.fn(),
    },
    initializePeerReviewChecksUseCase: {
      execute: jest.fn(),
    },
    ingestManuscriptAuthorsEmailsUseCase: {
      execute: jest.fn(),
    },
    deleteManuscriptAuthorsEmailsUseCase: {
      execute: jest.fn(),
    },
  }

  const s3Service = {
    uploadReviewFileToScreeningS3: jest.fn(async () => {}),
  }

  const logger = {
    info: jest.fn(),
  }

  it('should not save the manuscript if article type is on excluded list', async () => {
    const manuscript = {
      ...manuscriptsInput.manuscripts[0],
      articleType: { name: 'Corrigendum' },
    }

    await useCases.ingestAcceptedManuscriptUseCase
      .initialize({
        models,
        factories,
        s3Service,
        useCases: mockedUseCases,
        logger,
      })
      .execute({
        submissionId,
        manuscripts: [manuscript],
      })

    expect(logger.info).toHaveBeenCalledTimes(1)
    expect(logger.info).toHaveBeenCalledWith(
      `The quality checking process is skipped for submission id ${submissionId} with article type: ${manuscript.articleType.name}.`,
    )
  })

  it('should save the manuscript with all information', async () => {
    const {
      affiliation: { name: aff },
      country,
      email,
    } = manuscriptsInput.manuscripts[0].authors[0]

    jest
      .spyOn(Manuscript, 'getLatestManuscriptVersion')
      .mockResolvedValueOnce({ isSentToPeerReview: () => false })
      .mockResolvedValueOnce({ id: 'previousSavedVersionForSubmissionId' })

    jest
      .spyOn(Manuscript, 'insertGraph')
      .mockResolvedValueOnce(savedManuscripts[0])
      .mockResolvedValueOnce(savedManuscripts[1])

    jest.spyOn(Manuscript, 'updateBy').mockResolvedValue()

    await useCases.ingestAcceptedManuscriptUseCase
      .initialize({ models, factories, useCases: mockedUseCases, s3Service })
      .execute({ submissionId, manuscripts: manuscriptsInput.manuscripts })

    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenCalledTimes(2)

    expect(Manuscript.insertGraph).toHaveBeenCalledTimes(2)

    expect(
      mockedUseCases.initializeMaterialChecksUseCase.execute,
    ).toHaveBeenCalledTimes(1)
    expect(
      mockedUseCases.initializeMaterialChecksUseCase.execute,
    ).toHaveBeenCalledWith('id-from-manuscript-v2')

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'ManuscriptIngested',
        data: {
          authors: [
            {
              aff,
              country,
              email,
              surname: expect.anything(),
              title: expect.anything(),
              givenNames: expect.anything(),
              isCorresponding: true,
              isSubmitting: true,
              position: expect.anything(),
              id: expect.anything(),
              reviews: [expect.anything()],
            },
          ],
          fileId: expect.anything(),
          manuscriptId: expect.anything(),
          originalFileProviderKey: expect.anything(),
        },
      }),
    )
  })
})
