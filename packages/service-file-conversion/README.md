# Service File Conversion

This service adds the ability to convert doc and docx files to pdf and to extract the text from them.

The service listens to the following events:

- `ManuscriptIngested`

After converting the file and extracting the text it triggers the `FileConverted` event with the following format

```
{
      event: 'FileConverted',
      data: {
        manuscriptId,
        fileId,
        convertedFileProviderKey,
        textKey,
  }
```

### Required Env vars

```sh
AWS_S3_BUCKET=screening-sandbox-eu-west-1

AWS_REGION=eu-west-1

AWS_PROFILE=HindawiDevelopment

AWS_SNS_ENDPOINT=http://localstack:4566
AWS_SNS_TOPIC=test1

AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SQS_QUEUE_NAME=file-conversion-service-queue

FILE_CONVERSION_TIMEOUT=300000 # number of miliseconds to wait before aborting the file conversion

EVENT_NAMESPACE=pen
PUBLISHER_NAME=hindawi

AWS_S3_ENDPOINT=.
PHENOM_LARGE_EVENTS_BUCKET=.
PHENOM_LARGE_EVENTS_PREFIX=.

```


`AWS_S3_ACCESS_KEY` & `AWS_S3_SECRET_KEY` & `AWS_SNS_SQS_ACCESS_KEY` & `AWS_SNS_SQS_SECRET_KEY` are no longer needed in the .env file. \
Access keys are loaded from the Shared Credentials File (~/aws/credentials) for the AWS profile. This means that in order to work you need to be logged in with the Development AWS account.

# Config for docker-compose

```sh
AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SNS_ENDPOINT=http://localstack:4566
```

# Config for yarn start

```sh
PORT=3002
AWS_SNS_ENDPOINT=http://localhost:4566
AWS_SQS_ENDPOINT=http://localhost:4566
```

### Starting the service

To start the service run `docker-compose up service-file-conversion`

```
