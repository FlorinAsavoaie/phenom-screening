import { WithAwsSecretsServiceProps, ServiceType } from '@hindawi/phenom-charts'

const defaultValues: WithAwsSecretsServiceProps = {
  secretNames: [],
  serviceProps: {
    image: {
      repository:
        '496598730381.dkr.ecr.eu-west-1.amazonaws.com/service-file-conversion',
      tag: 'latest',
    },
    replicaCount: 1,
    resources: {
      requests: {
        memory: '100Mi',
        'ephemeral-storage': '2Gi',
      },
      limits: {
        memory: '4Gi',
        'ephemeral-storage': '10Gi',
      },
    },
    service: {
      port: 80,
      type: ServiceType.NODE_PORT,
    },
    containerPort: 3000,
    labels: {
      owner: 'QAlas',
    },
    livenessProbe: {
      path: '/',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    readinessProbe: {
      path: '/',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    startupProbe: {
      path: '/',
      failureThreshold: 30,
      periodSeconds: 10,
    },
  },
}

export { defaultValues }
