import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod/screening/file-conversion'],
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/service-file-conversion',
      tag: 'latest',
    },
    replicaCount: 4,
    resources: {
      requests: {
        memory: '800Mi',
        'ephemeral-storage': '2Gi',
      },
      limits: {
        memory: '4Gi',
        'ephemeral-storage': '10Gi',
      },
    },
    ingressOptions: {
      rules: [
        {
          host: 'file-conversion.prod.phenom.pub',
        },
      ],
    },
  },
}
