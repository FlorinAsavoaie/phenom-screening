import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['demo-gsw/screening/file-conversion'],
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'service-file-conversion.demo-gsw.phenom.pub',
        },
      ],
    },
  },
}
