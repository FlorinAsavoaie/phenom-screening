import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod-gsw/screening/file-conversion'],
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/service-file-conversion',
      tag: 'latest',
    },
    replicaCount: 2,
    ingressOptions: {
      rules: [
        {
          host: 'file-conversion.gsw-prod.phenom.pub',
        },
      ],
    },
  },
}
