const os = require('os')

function initialize({
  externalCommandExecutor,
  fileManager,
  logger,
  configService,
}) {
  return {
    convert,
  }

  async function convert(file) {
    const fileConverted = await convertFileToPDF(file)

    return {
      ...file,
      Body: fileConverted,
    }
  }

  async function convertFileToPDF(file) {
    const inputFilePath = await fileManager.createTempFile({
      content: file.Body,
    })

    const outputFilePath = `${inputFilePath}.pdf`

    const outputDir = os.tmpdir()

    return externalCommandExecutor
      .start({
        command: 'soffice',
        args: [
          '--headless',
          '--convert-to',
          'pdf',
          `--outdir`,
          outputDir,
          inputFilePath,
        ],
        options: { timeout: configService.get('fileConversionTimeout') },
      })
      .then(async () => {
        const outputFileBuffer = await fileManager.getFileContent(
          outputFilePath,
        )

        if (outputFileBuffer.length === 0) {
          throw new Error('File conversion failed: Converted file size is 0')
        }
        return outputFileBuffer
      })
      .finally(async () => {
        try {
          await fileManager.removeFiles(inputFilePath, outputFilePath)
        } catch (err) {
          logger.warn(
            `Problem removing temporary input/output files: ${err.message}`,
          )
        }
      })
  }
}

module.exports = {
  initialize,
}
