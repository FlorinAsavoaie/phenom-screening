function initialize({ fileToPdfConverter, pdfToTextConverter }) {
  return {
    convertToPDF,
    convertPdfFileToText,
  }

  async function convertToPDF(file, providerKey) {
    const body = await streamToBuffer(file.Body)
    return fileToPdfConverter.convert({ ...file, Body: body }, providerKey)
  }

  async function convertPdfFileToText(file) {
    let body = file.Body
    if (!Buffer.isBuffer(file.Body)) {
      body = await streamToBuffer(file.Body)
    }
    return pdfToTextConverter.convert(body)
  }

  async function streamToBuffer(stream) {
    return new Promise((resolve, reject) => {
      const chunks = []
      stream.on('data', (chunk) => chunks.push(chunk))
      stream.on('error', reject)
      stream.on('end', () => resolve(Buffer.concat(chunks)))
    })
  }
}

module.exports = {
  initialize,
}
