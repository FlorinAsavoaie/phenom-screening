const conversionService = require('./conversionService')
const externalCommandExecutor = require('./externalCommandExecutor')
const fileManager = require('./fileManager')
const fileToPdfConverter = require('./fileToPdfConverter')
const pdfToTextConverter = require('./pdfToTextConverter')
const s3Service = require('./s3Service')

module.exports = {
  conversionService,
  externalCommandExecutor,
  fileManager,
  fileToPdfConverter,
  pdfToTextConverter,
  s3Service,
}
