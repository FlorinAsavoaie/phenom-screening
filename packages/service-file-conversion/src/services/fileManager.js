const fs = require('fs')
const os = require('os')
const path = require('path')
const uuid = require('uuid')
const { promisify } = require('util')

const unlinkFile = promisify(fs.unlink.bind(fs))
const writeFile = promisify(fs.writeFile.bind(fs))

function initialize() {
  return {
    createTempFile,
    getFileContent,
    removeFiles,
  }

  async function createTempFile({ content } = {}) {
    const fileName = uuid.v4()
    const filePath = path.resolve(os.tmpdir(), fileName)

    if (content) {
      await writeFile(filePath, content)
    }

    return filePath
  }

  async function getFileContent(filePath) {
    return fs.readFileSync(filePath)
  }

  async function removeFile(filePath) {
    await unlinkFile(filePath)
  }

  async function removeFiles(...filePaths) {
    return Promise.all(filePaths.map(removeFile))
  }
}

module.exports = {
  initialize,
}
