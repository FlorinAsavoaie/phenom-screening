const PDF_TO_TEXT_COMMAND = 'pdftotext'

function initialize({ externalCommandExecutor, fileManager, configService }) {
  return {
    convert,
  }

  async function convert(file) {
    const inputFilePath = await fileManager.createTempFile({
      content: file,
    })
    const outputFilePath = await fileManager.createTempFile()

    return externalCommandExecutor
      .start({
        command: PDF_TO_TEXT_COMMAND,
        args: [
          '-nopgbrk',
          '-enc',
          'UTF-8',
          '-layout',
          inputFilePath,
          outputFilePath,
        ],
        options: { timeout: configService.get('fileConversionTimeout') },
      })
      .then(async () => {
        const outputFileBuffer = await fileManager.getFileContent(
          outputFilePath,
        )
        if (outputFileBuffer.length === 0) {
          throw new Error('File conversion failed: Converted file size is 0')
        }
        return outputFileBuffer
      })
      .finally(async () => {
        try {
          await fileManager.removeFiles(inputFilePath, outputFilePath)
        } catch (err) {
          throw new Error(err)
        }
      })
  }
}

module.exports = {
  initialize,
}
