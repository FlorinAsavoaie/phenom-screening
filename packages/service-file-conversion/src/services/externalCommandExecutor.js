const { spawn } = require('child_process')

function initialize() {
  return {
    start,
  }

  async function start({ command, args, options }) {
    return new Promise((resolve, reject) => {
      const commandProcess = spawn(command, args, { detached: true })

      let err = ''
      let timeoutId

      if (options.timeout) {
        timeoutId = setTimeout(() => {
          process.kill(-commandProcess.pid)
          err = new Error(`Timed out - ${command} ${args.join(' ')}`)
        }, options.timeout)
      }

      commandProcess.stderr.on('data', (data) => {
        err += data
      })

      commandProcess.on('exit', (code) => {
        clearTimeout(timeoutId)

        if (code === 0) {
          resolve()
          return
        }

        if (err) {
          reject(err instanceof Error ? err : new Error(err))
          return
        }

        reject(new Error(`${command} end with code ${code}`))
      })
    })
  }
}

module.exports = {
  initialize,
}
