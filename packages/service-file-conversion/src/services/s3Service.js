const config = require('config')
const { S3 } = require('@aws-sdk/client-s3')
const { Upload } = require('@aws-sdk/lib-storage')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

const bucket = config.get('aws.s3.bucket')

module.exports.getObject = (key) => s3.getObject({ Bucket: bucket, Key: key })

module.exports.upload = async ({ key, mimetype, stream, metadata = {} }) => {
  const uploadParams = {
    Key: key,
    Body: stream,
    ContentType: mimetype,
    Metadata: metadata,
    Bucket: bucket,
  }
  const upload = new Upload({
    params: uploadParams,
    client: s3,
    queueSize: 3,
  })
  await upload.done()

  return { ...upload, Key: key }
}
