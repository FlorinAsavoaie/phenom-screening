const { convertFileUseCase } = require('./use-cases')
const services = require('./services')
const config = require('config')
const logger = require('../config/loggerCustom')

module.exports = {
  async ManuscriptIngested(data) {
    const externalCommandExecutor =
      services.externalCommandExecutor.initialize()

    const fileManager = services.fileManager.initialize()

    const fileToPdfConverter = services.fileToPdfConverter.initialize({
      externalCommandExecutor,
      configService: config,
      fileManager,
      logger,
    })

    const pdfToTextConverter = services.pdfToTextConverter.initialize({
      externalCommandExecutor,
      fileManager,
      configService: config,
    })

    const conversionService = services.conversionService.initialize({
      fileToPdfConverter,
      pdfToTextConverter,
    })

    const { s3Service } = services

    return convertFileUseCase
      .initialize({
        conversionService,
        s3Service,
      })
      .execute(data)
  },
}
