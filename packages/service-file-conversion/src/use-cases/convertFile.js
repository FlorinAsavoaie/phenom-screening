/* eslint-disable no-control-regex */
function initialize({ conversionService, s3Service }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, fileId, originalFileProviderKey }) {
    const file = await s3Service.getObject(originalFileProviderKey)

    let pdfFile = file
    pdfFile.Key = originalFileProviderKey

    if (file.ContentType !== 'application/pdf') {
      pdfFile = await conversionService.convertToPDF(
        file,
        originalFileProviderKey,
      )

      const uploadResult = await s3Service.upload({
        key: `${originalFileProviderKey}-converted`,
        mimetype: 'application/pdf',
        stream: pdfFile.Body,
        metadata: { type: file.Metadata.type },
      })

      pdfFile.Key = uploadResult.Key
    }

    const text = await extractText(pdfFile)

    const textFileUploadResult = await s3Service.upload({
      key: `${originalFileProviderKey}-text`,
      mimetype: 'text/plain',
      stream: text,
    })

    await applicationEventBus.publishMessage({
      event: 'FileConverted',
      data: {
        fileId,
        manuscriptId,
        convertedFileProviderKey: pdfFile.Key,
        textKey: textFileUploadResult.Key,
      },
    })
  }

  async function extractText(pdfFile) {
    const text = await conversionService.convertPdfFileToText(pdfFile)
    return text.toString().replace(/[\x00-\x09]|[\x0B-\x1F]+/g, '')
  }
}

module.exports = {
  initialize,
}
