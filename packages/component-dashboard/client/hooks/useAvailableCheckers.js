import { useLazyQuery } from '@apollo/client'
import { get } from 'lodash'

import * as queries from '../graphql/queries'

export function useAvailableCheckers(manuscriptId) {
  const [loadAvailableCheckers, { data, error, loading }] = useLazyQuery(
    queries.getAvailableCheckers,
    {
      variables: {
        manuscriptId,
      },
    },
  )

  const availableCheckers = get(data, 'availableCheckers', [])

  return {
    availableCheckers,
    error,
    loadAvailableCheckers,
    loading,
  }
}
