import { useMutation } from '@apollo/client'

import * as fragments from '../graphql/fragments'
import * as mutations from '../graphql/mutations'

export function useReassignManuscript() {
  const [reassignMutation] = useMutation(mutations.reassignManuscriptToChecker)
  const reassignManuscript = ({ manuscriptId, userId }) =>
    reassignMutation({
      variables: { manuscriptId, userId },
      update: (
        cache,
        { data: { reassignManuscriptToChecker: assignedChecker } },
      ) => {
        const manuscript = cache.readFragment({
          id: `ScreeningManuscript:${manuscriptId}`,
          fragment: fragments.dashboardManuscriptDetails,
          fragmentName: 'dashboardManuscriptDetails',
        })

        const updatedManuscript = {
          ...manuscript,
          assignedChecker,
        }

        cache.writeFragment({
          id: `ScreeningManuscript:${manuscriptId}`,
          fragment: fragments.dashboardManuscriptDetails,
          fragmentName: 'dashboardManuscriptDetails',
          data: updatedManuscript,
        })
      },
    })

  return { reassignManuscript }
}
