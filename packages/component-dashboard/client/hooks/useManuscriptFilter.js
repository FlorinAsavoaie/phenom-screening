import { useState } from 'react'

const FILTER_OPTIONS = {
  inProgress: { label: 'In Progress', value: 'inProgress' },
  archived: { label: 'Archived', value: 'archived' },
  processing: { label: 'Processing', value: 'processing' },
  escalated: { label: 'Escalated', value: 'escalated' },
}

export function useManuscriptFilter(currentUser) {
  const filterOptions = getFilterOption(currentUser)
  const initialValue = FILTER_OPTIONS.inProgress.value

  const [filterOption, setFilterOption] = useState(initialValue)

  return { filterOption, setFilterOption, filterOptions }
}

function getFilterOption(currentUser) {
  const { inProgress, archived, processing, escalated } = FILTER_OPTIONS

  if (currentUser.isAdmin) {
    return [processing, inProgress, escalated, archived]
  }

  if (currentUser.isTeamLeadInAtLeastOneTeam)
    return [inProgress, escalated, archived]

  return [inProgress, archived]
}
