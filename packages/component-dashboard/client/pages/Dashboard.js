import { useState } from 'react'
import styled from 'styled-components'
import { Row } from '@hindawi/ui'
import {
  usePaginatedQuery,
  DEFAULT_PAGE_SIZE,
  Checkbox,
} from 'hindawi-shared/client/components'
import { th } from '@pubsweet/ui-toolkit'

import {
  DashboardManuscripts,
  ManuscriptsSearchField,
  ManuscriptFilters,
} from '../components'
import { getManuscripts } from '../graphql/queries'
import { useManuscriptFilter } from '../hooks'

const Dashboard = ({ currentUser }) => {
  const { filterOption, setFilterOption, filterOptions } =
    useManuscriptFilter(currentUser)
  const [searchValue, setSearchValue] = useState('')
  const [authorRespondedValue, setAuthorRespondedValue] = useState(false)
  const filtersForAuthorResponded = ['inProgress', 'escalated', 'processing']
  const {
    results: manuscripts,
    page,
    toLast,
    toNext,
    toFirst,
    loading,
    setPage,
    toPrevious,
    total,
  } = usePaginatedQuery(getManuscripts, {
    variables: {
      pageSize: DEFAULT_PAGE_SIZE,
      searchValue,
      filterOption,
      authorResponded: authorRespondedValue,
    },
    settings: { noDebounce: true },
  })
  const showAuthorResponded = () => {
    setAuthorRespondedValue(!authorRespondedValue)
    setPage(0)
  }

  return (
    <Root>
      <Container>
        <Row justify="flex-start">
          <ManuscriptFilters
            filterOption={filterOption}
            filterOptions={filterOptions}
            onFilterChange={(item) => {
              if (!filtersForAuthorResponded.includes(item)) {
                setAuthorRespondedValue(false)
              }
              toFirst()
              setFilterOption(item)
            }}
          />
          <ManuscriptsSearchField
            onSearch={(item) => {
              toFirst()
              setSearchValue(item)
            }}
          />
          {filtersForAuthorResponded.includes(filterOption) && (
            <CheckboxWrapper>
              <Checkbox
                label="Show Author Responded Only"
                onChange={showAuthorResponded}
              />
            </CheckboxWrapper>
          )}
        </Row>
        <DashboardManuscripts
          currentUser={currentUser}
          filterOption={filterOption}
          itemsPerPage={DEFAULT_PAGE_SIZE}
          loading={loading}
          manuscripts={manuscripts}
          nextPage={toNext}
          page={page}
          prevPage={toPrevious}
          searchValue={searchValue}
          setPage={setPage}
          toFirst={toFirst}
          toLast={toLast}
          totalCount={total}
        />
      </Container>
    </Root>
  )
}

const Root = styled.div`
  padding: calc(${th('gridUnit')} * 2);
  overflow: scroll;
  overflow-x: hidden;
  width: calc(100vw - ${th('gridUnit')} * 24);
`

const Container = styled.div`
  border-radius: 6px;
  box-shadow: 3px 3px 6px 0 rgba(141, 141, 141, 0.2);
  background-color: ${th('white')};
  padding: calc(${th('gridUnit')} * 4);
`
const CheckboxWrapper = styled.div`
  margin-top: calc(4px * 5);
  label {
    cursor: pointer;
  }
`
export default Dashboard
