import { gql } from '@apollo/client'

import { assignedCheckerDashboard } from './fragments'

export const reassignManuscriptToChecker = gql`
  mutation reassignManuscriptToChecker($manuscriptId: ID!, $userId: ID!) {
    reassignManuscriptToChecker(manuscriptId: $manuscriptId, userId: $userId) {
      ...assignedCheckerDashboard
    }
  }

  ${assignedCheckerDashboard}
`
