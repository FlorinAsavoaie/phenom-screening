import { gql } from '@apollo/client'

import { dashboardManuscriptDetails } from './fragments'

export const getManuscripts = gql`
  query getScreeningManuscripts(
    $searchValue: String
    $filterOption: FilterOptions
    $page: Int
    $pageSize: Int
    $authorResponded: Boolean
  ) {
    getScreeningManuscripts(
      searchValue: $searchValue
      filterOption: $filterOption
      page: $page
      pageSize: $pageSize
      authorResponded: $authorResponded
    ) {
      results {
        ...dashboardManuscriptDetails
      }
      total
    }
  }
  ${dashboardManuscriptDetails}
`
export const getAvailableCheckers = gql`
  query getAvailableCheckers($manuscriptId: ID!) {
    availableCheckers: getAvailableCheckers(manuscriptId: $manuscriptId) {
      id
      givenNames
      surname
    }
  }
`
