import { gql } from '@apollo/client'

export const journalDashboard = gql`
  fragment journalDashboard on ScreeningJournal {
    id
    name
  }
`

export const assignedCheckerDashboard = gql`
  fragment assignedCheckerDashboard on UserDetails {
    id
    givenNames
    surname
  }
`

export const dashboardManuscriptDetails = gql`
  fragment dashboardManuscriptDetails on ScreeningManuscript {
    id
    title
    flowType
    customId
    articleType
    submissionId
    submissionDate
    status
    step
    team {
      id
    }
    submittingAuthor {
      givenNames
      surname
    }
    journal {
      ...journalDashboard
    }
    journalSection {
      id
      name
    }
    specialIssue {
      id
      name
    }
    assignedChecker {
      ...assignedCheckerDashboard
    }
    authors {
      id
      surname
      givenNames
      isSubmitting
    }
    authorResponded
    linkedSubmissionCustomId
  }

  ${journalDashboard}
  ${assignedCheckerDashboard}
`
