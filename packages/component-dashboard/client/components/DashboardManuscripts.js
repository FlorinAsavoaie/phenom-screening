import { Row, Text as BaseText, Item, Pagination } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import { withRouter } from 'react-router'
import styled from 'styled-components'
import { useApolloClient } from '@apollo/client'
import { useSubmissionStatusAndStepUpdatedEffect } from 'hindawi-shared/client/components/status-and-step-update'
import { DEFAULT_PAGE_SIZE } from 'hindawi-shared/client/components'
import { getManuscripts } from '../graphql/queries'
import { SkeletonManuscriptCard, ManuscriptCard } from '../components'

function DashboardManuscripts({
  searchValue,
  manuscripts,
  currentUser,
  loading,
  history,
  itemsPerPage,
  nextPage,
  page,
  prevPage,
  setPage,
  toFirst,
  toLast,
  totalCount,
  filterOption,
}) {
  const skeletonCards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  if (loading) {
    return (
      <Root>
        {skeletonCards.map((nr) => (
          <SkeletonManuscriptCard key={nr} />
        ))}
      </Root>
    )
  }

  if (totalCount === 0) {
    return (
      <StyledRow>
        <StyledText fontWeight={700}>
          {searchValue
            ? 'No results found.'
            : 'No manuscripts assigned to this account.'}
        </StyledText>
      </StyledRow>
    )
  }
  const client = useApolloClient()
  const submissionIds = manuscripts.map((m) => m.submissionId)
  useSubmissionStatusAndStepUpdatedEffect(({ submissionId, status }) => {
    if (filterOption === 'inProgress') {
      const updatedManuscripts = getUpdatedManuscripts(
        status,
        manuscripts,
        submissionId,
      )

      client.writeQuery({
        query: getManuscripts,
        variables: {
          searchValue,
          filterOption,
          page,
          pageSize: DEFAULT_PAGE_SIZE,
        },
        data: {
          getScreeningManuscripts: {
            results: updatedManuscripts,
            total: totalCount,
            __typename: 'PaginatedManuscripts',
          },
        },
      })
    }
  }, submissionIds)

  return (
    <>
      {manuscripts.map((manuscript) => (
        <ManuscriptCard
          currentUser={currentUser}
          key={manuscript.id}
          manuscript={manuscript}
          my={6}
          onClick={() => history.push(`/manuscripts/${manuscript.id}`)}
        />
      ))}
      <Item justify="flex-end" mt={4}>
        <Pagination
          itemsPerPage={itemsPerPage}
          nextPage={nextPage}
          page={page}
          prevPage={prevPage}
          setPage={setPage}
          toFirst={toFirst}
          toLast={toLast}
          totalCount={totalCount}
        />
      </Item>
    </>
  )
}

export default withRouter(DashboardManuscripts)

const Root = styled.div`
  padding: 0;
  width: 100%;
`
const StyledText = styled(BaseText)`
  font-size: 20px;
  color: ${th('labelLineColor')};
`

function getUpdatedManuscripts(status, manuscripts, submissionId) {
  let updatedManuscripts
  if (['filesRequested', 'paused', 'inProgress'].includes(status)) {
    updatedManuscripts = manuscripts.map((manuscript) =>
      manuscript.submissionId !== submissionId
        ? manuscript
        : { ...manuscript, status },
    )
  } else {
    updatedManuscripts = manuscripts.filter(
      (manuscript) => manuscript.submissionId !== submissionId,
    )
  }
  return updatedManuscripts
}

const StyledRow = styled(Row)`
  margin-top: calc(${th('gridUnit')} * 4);
  min-height: calc(${th('gridUnit')} * 22);
  flex: 1;
  border-radius: 4px;
  background-color: ${th('backgroundColor')};
`
