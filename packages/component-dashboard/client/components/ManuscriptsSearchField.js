import { useState } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { SearchField } from 'hindawi-shared/client/components'

export function ManuscriptsSearchField({ onSearch }) {
  const [inputValue, setInputValue] = useState('')
  const handleOnChange = (event) => setInputValue(event.target.value)

  return (
    <Root>
      <SearchField
        inline
        onChange={handleOnChange}
        onSearch={onSearch}
        placeholder="Search"
        value={inputValue}
        width={64}
      />
    </Root>
  )
}

const Root = styled.div`
  margin-top: calc(${th('gridUnit')} * 5);
`
