import { H3 } from '@pubsweet/ui'
import { Item, Row, StatusTag as BaseStatusTag } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { useManuscriptByCustomId } from 'hindawi-shared/client/graphql/hooks'

import StatusLabel from './StatusLabel'
import { TextInfo } from './TextInfo'

const manuscriptFlowTypes = {
  qualityCheck: { label: 'QC' },
  screening: { label: 'Screening' },
}
const getFlowLabel = (flowType) => manuscriptFlowTypes[flowType].label

const statusProprieties = {
  paused: {
    label: 'paused',
    color: 'statusInvite',
  },
  returnedToDraft: {
    label: 'RTD',
    color: 'actionSecondaryColor',
  },
  screeningApproved: {
    label: 'approved',
    color: 'actionSecondaryColor',
  },
  screeningRejected: {
    label: 'RTC',
    color: 'actionSecondaryColor',
  },
  filesRequested: {
    label: 'Files Requested',
    color: 'actionSecondaryColor',
  },
  qualityCheckApproved: {
    label: 'approved',
    color: 'actionSecondaryColor',
  },
  qualityCheckRejected: {
    label: 'RTC',
    color: 'actionSecondaryColor',
  },
  withdrawn: {
    label: 'Withdrawn',
    color: 'actionSecondaryColor',
  },
  void: {
    label: 'Void',
    color: 'actionSecondaryColor',
  },
  escalated: {
    label: 'Escalated',
    color: 'actionSecondaryColor',
  },
  sentToPeerReview: {
    label: 'Sent To Peer Review',
    color: 'actionSecondaryColor',
  },
}
const getStatusProperties = (status, step) => {
  if (status === 'inProgress' && step === 'inMaterialsChecking') {
    return {
      label: 'Materials Check',
      color: 'actionSecondaryColor',
    }
  }
  return statusProprieties[status] || { color: 'actionSecondaryColor' }
}

const hasStatusTagLabel = (status, step) =>
  !['inProgress', 'automaticChecks'].includes(status) ||
  (status === 'inProgress' && step === 'inMaterialsChecking')

const ManuscriptCardContent = ({
  manuscript: {
    title,
    articleType,
    flowType,
    status,
    step,
    journal: { name: journalName },
    journalSection,
    specialIssue,
    linkedSubmissionCustomId,
  },
}) => {
  let linkedManuscriptData = null
  if (linkedSubmissionCustomId) {
    // component is used in multiple places and in some of them linked manuscript info is not mandatory to be displayed - this avoids overfetching
    const { manuscriptData } = useManuscriptByCustomId(linkedSubmissionCustomId)
    linkedManuscriptData = manuscriptData
  }

  return (
    <Root>
      <Row alignItems="center" justify="space-between">
        <H3 title={title}>{title}</H3>
        <Item justify="flex-end">
          <StatusTag
            hasLabel={hasStatusTagLabel(status, step)}
            height="inherit"
            pl={2}
            pr={2}
            statusColor={getStatusProperties(status, step).color}
          >
            {getFlowLabel(flowType)}
          </StatusTag>
          {hasStatusTagLabel(status, step) && (
            <StatusLabel
              label={getStatusProperties(status, step).label}
              statusColor={getStatusProperties(status, step).color}
            />
          )}
        </Item>
      </Row>

      <Row alignItems="center" justify="space-between" mt={2}>
        <TextInfo label="Journal" mr={8} value={journalName} />
        <TextInfo
          label="Section"
          value={journalSection ? journalSection.name : 'None'}
        />
      </Row>

      <Row mt={2}>
        <TextInfo label="Article Type" mr={8} value={articleType} />
        <TextInfo
          label="Special Issue"
          value={specialIssue ? specialIssue.name : 'None'}
        />
      </Row>

      {linkedSubmissionCustomId && linkedManuscriptData && (
        <Row mt={2}>
          <TextInfo
            label="Linked Article"
            mr={8}
            value={linkedManuscriptData.title}
          />
        </Row>
      )}
    </Root>
  )
}

export default ManuscriptCardContent

// #region styles
const Root = styled.div`
  background-color: ${th('white')};
  border-radius: ${th('borderRadius')} ${th('borderRadius')} 0 0;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 4)
    calc(${th('gridUnit')} * 2) calc(${th('gridUnit')} * 4);
  overflow: hidden;

  ${H3} {
    margin: 0;
    margin-bottom: ${th('gridUnit')};
  }

  ${Row} {
    [data-tooltipped] {
      overflow: hidden;
    }
    ${H3} {
      display: block;
      margin-bottom: 0;
      padding-right: calc(${th('gridUnit')} * 2);
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
  }
`

const StatusTag = styled(BaseStatusTag)`
  ${(props) => {
    if (props.hasLabel) {
      return css`
        border-radius: ${th('borderRadius')} 0 0 ${th('borderRadius')};
      `
    }
  }}
`
// #endregion
