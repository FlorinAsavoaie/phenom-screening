import styled, { css } from 'styled-components'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'

import { ManuscriptCardContent, ManuscriptCardFooter } from '../../components'

function ManuscriptCard({
  onClick,
  manuscript,
  currentUser,
  showAllAuthors,
  showAssignedChecker,
  ...rest
}) {
  const {
    id: manuscriptId,
    submittingAuthor,
    assignedChecker,
    team,
    flowType,
    customId,
    authors,
    submissionDate,
    status,
    authorResponded,
  } = manuscript

  return (
    <Root
      data-test-id={`manuscript-${manuscriptId}`}
      onClick={onClick}
      {...rest}
    >
      <ManuscriptCardContent manuscript={manuscript} />
      <ManuscriptCardFooter
        assignedChecker={assignedChecker}
        authorResponded={authorResponded}
        authors={authors}
        currentUser={currentUser}
        customId={customId}
        flowType={flowType}
        manuscriptId={manuscriptId}
        manuscriptStatus={status}
        showAllAuthors={showAllAuthors}
        showAssignedChecker={showAssignedChecker}
        submissionDate={submissionDate}
        submittingAuthor={submittingAuthor}
        team={team}
      />
    </Root>
  )
}

// TODO: use hook
export default ManuscriptCard

const clickable = ({ onClick }) => {
  if (onClick) {
    return css`
      cursor: pointer;

      &:hover {
        box-shadow: ${th('dashboardCard.hoverShadow')};
      }
    `
  }
}
const Root = styled.div`
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};

  width: ${({ width }) => (width ? `${width}` : 'initial')};

  ${clickable}
  ${space};
`
