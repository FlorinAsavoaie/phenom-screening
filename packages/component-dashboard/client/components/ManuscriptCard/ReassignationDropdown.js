import { useContext } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { MultiAction } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import ModalContext from '@pubsweet/ui/src/molecules/modal/ModalContext'

import { RemoteDropdown } from '../../components'

import { useReassignManuscript, useAvailableCheckers } from '../../hooks'

function ReassignationDropdown({ assignedChecker, manuscriptId, flowType }) {
  const flowTypes = {
    qualityCheck: 'No quality checkers in the team',
    screening: 'No screeners in the team.',
  }
  const { reassignManuscript } = useReassignManuscript()
  const { availableCheckers, loadAvailableCheckers, loading } =
    useAvailableCheckers(manuscriptId)

  const modal = useModal({
    subtitle: 'Are you sure?',
    component: (props) => <MultiAction {...props} />,
  })

  return (
    <RemoteDropdown
      emptyMessage={flowTypes[flowType]}
      initialValue={assignedChecker}
      items={availableCheckers}
      label={(member) => `${member.givenNames} ${member.surname}`}
      loading={loading}
      onOpen={loadAvailableCheckers}
      onSelect={(member, selectItem) => {
        if (get(assignedChecker, 'id') === member.id) {
          selectItem()
          return
        }

        modal.showModal({
          title: `Reassign manuscript to ${member.givenNames} ${member.surname}`,
          confirmText: 'REASSIGN',
          cancelText: 'CANCEL',
          onConfirm: ({ hideModal, setFetching, setError }) => {
            setFetching(true)
            reassignManuscript({
              manuscriptId,
              userId: member.id,
            })
              .then(() => {
                setFetching(false)
                selectItem()
                hideModal()
              })
              .catch((err) => {
                const error = <ErrorText>{parseGQLError(err)}</ErrorText>
                setFetching(false)
                setError(error)
              })
          },
        })
      }}
    />
  )
}

const useModal = ({ component, dismissable, ...initialProps }) => {
  const { showModal, hideModal } = useContext(ModalContext)
  return {
    hideModal,
    showModal: (modalData = {}) =>
      showModal({
        component,
        dismissable,
        modalProps: {
          ...initialProps,
          ...modalData,
        },
      }),
  }
}
export default ReassignationDropdown

const ErrorText = styled.div`
  padding-top: calc(${th('gridUnit')} * 2);
`
