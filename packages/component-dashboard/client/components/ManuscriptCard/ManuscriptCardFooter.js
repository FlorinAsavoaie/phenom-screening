import { useState } from 'react'
import { DateParser } from '@pubsweet/ui'
import { get } from 'lodash'
import {
  Row,
  Item,
  Text,
  ActionLink,
  Icon,
  StatusTag as BaseStatusTag,
} from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { parseListWithComma } from 'component-screening-process/client/utils'
import { ReassignationDropdown } from '../../components'

function ManuscriptCardFooter({
  assignedChecker,
  currentUser,
  manuscriptId,
  submittingAuthor,
  team,
  flowType,
  customId,
  submissionDate,
  manuscriptStatus,
  authors = [],
  showAllAuthors,
  showAssignedChecker = false,
  authorResponded,
}) {
  const [toggle, setToggle] = useState(false)
  const handlerToggle = () => setToggle(!toggle)

  const authorsName = authors
    .filter((author) => !author.isSubmitting)
    .map((author) => `${author.givenNames} ${author.surname}`)

  const displayChecker =
    (currentUser.id !== get(assignedChecker, 'id') || showAssignedChecker) &&
    manuscriptStatus !== 'automaticChecks'

  return (
    <Root>
      <Item vertical>
        <Content justify="flex-start">
          <Item alignItems="center">
            <AuthorDetails submittingAuthor={submittingAuthor} />

            {!!authorsName.length && showAllAuthors && (
              <StyledItem>
                <ActionLink
                  data-test-id="author-affiliations"
                  onClick={handlerToggle}
                >
                  <StyledText fontWeight="700">
                    {toggle ? 'Hide All Authors' : 'View All Authors'}
                  </StyledText>
                  <Icon
                    bold
                    color="colorSecondary"
                    fontSize="10px"
                    icon={toggle ? 'collapse' : 'expand'}
                    ml={1}
                    mr={2}
                  />
                </ActionLink>
              </StyledItem>
            )}
            <AuthorResponded authorResponded={authorResponded} />
          </Item>

          {displayChecker && (
            <Item alignItems="center">
              <Text fontWeight={700}>
                {flowType === 'screening' ? 'Screener' : 'Quality Checker'}
              </Text>
              <ReassignChecker
                assignedChecker={assignedChecker}
                currentUser={currentUser}
                flowType={flowType}
                manuscriptId={manuscriptId}
                manuscriptStatus={manuscriptStatus}
                team={team}
              />
            </Item>
          )}
          <Item justify="flex-end">
            <Text mr={1}>Submitted on</Text>
            <DateParser humanizeThreshold={0} timestamp={submissionDate}>
              {(timestamp, timeAgo) => (
                <Text display="flex">{`${timestamp} (${timeAgo} ago)`}</Text>
              )}
            </DateParser>
            <Text fontWeight={700} ml={2}>{`ID ${customId}`}</Text>
          </Item>
        </Content>
        <Row>
          {toggle && (
            <Row alignItems="flex-start" justify="flex-start" mb={1}>
              <AuthorsText>
                <Text fontWeight={700} mr={2}>
                  Authors
                </Text>
                {parseListWithComma(authorsName)}
              </AuthorsText>
            </Row>
          )}
        </Row>
      </Item>
    </Root>
  )
}

function AuthorDetails({ submittingAuthor }) {
  return (
    <>
      <Text fontWeight={700} mr={2} whiteSpace="nowrap">
        Submitting Author
      </Text>
      {submittingAuthor ? (
        <Text mr={2} whiteSpace="nowrap">
          {`${submittingAuthor.givenNames} ${submittingAuthor.surname}`}
        </Text>
      ) : (
        <Text error mr={2} whiteSpace="nowrap">
          No submitting author
        </Text>
      )}
    </>
  )
}
const AuthorResponded = ({ authorResponded }) => (
  <>{authorResponded && <AuthorLabel>Responded</AuthorLabel>}</>
)

function ReassignChecker({
  assignedChecker,
  manuscriptId,
  team,
  flowType,
  manuscriptStatus,
}) {
  const inProgressStatuses = [
    'paused',
    'inProgress',
    'returnedToDraft',
    'filesRequested',
  ]

  if (team && inProgressStatuses.includes(manuscriptStatus)) {
    return (
      <ReassignationDropdown
        assignedChecker={assignedChecker}
        flowType={flowType}
        manuscriptId={manuscriptId}
      />
    )
  }

  if (assignedChecker) {
    return (
      <Text data-test-id="assigned-checker" ml={2}>
        {assignedChecker.givenNames} {assignedChecker.surname}
      </Text>
    )
  }

  return (
    <Text data-test-id="assigned-checker" ml={2}>
      Unassigned
    </Text>
  )
}

export default ManuscriptCardFooter

const Root = styled.div`
  background-color: ${th('backgroundColor')};
  display: flex;
  padding: calc(${th('gridUnit')}) calc(${th('gridUnit')} * 4);
  border-radius: 0 0 ${th('borderRadius')} ${th('borderRadius')};
`
const StyledItem = styled(Item)`
  width: 130px;
`

const AuthorsText = styled(Text)`
  line-height: 19px;
`
const StyledText = styled(Text)`
  color: inherit;
`
const Content = styled(Row)`
  min-height: calc(${th('gridUnit')} * 7);
`
const AuthorLabel = styled(BaseStatusTag)`
  background-color: inherit;
  font-size: ${th('counterTextSize')};
  padding: 0 8px;
  line-height: ${th('smallLabelLineHeight')};
  height: inherit;
  border-radius: 3px;
  margin-right: 5px;
  border-color: ${th('actionSecondaryColor')};
  color: ${th('grey60')};
`
