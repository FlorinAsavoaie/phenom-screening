import { space, SpaceProps } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Theme } from '@hindawi/ui'

interface StatusLabelProps extends SpaceProps {
  label: string
  statusColor: string
}

const StatusLabel: React.FC<StatusLabelProps> = ({
  label,
  statusColor,
  ...rest
}) => (
  <Root
    data-test-id="status-label"
    pl={2}
    pr={2}
    statusColor={statusColor}
    {...rest}
  >
    {label}
  </Root>
)

export default StatusLabel

interface StatusColorConfig {
  statusColor: string
  theme: Theme
}
const getStatusColor = ({ statusColor, theme }: StatusColorConfig): string =>
  theme[statusColor]

// #region styles
interface RootProps extends SpaceProps {
  statusColor: string
}

const Root = styled.span<RootProps>`
  align-items: center;
  background-color: ${getStatusColor};
  border-top-right-radius: ${th('borderRadius')};
  border-bottom-right-radius: ${th('borderRadius')};
  color: ${th('backgroundColor')};
  display: flex;
  font-family: ${th('defaultFont')};
  font-size: ${th('text.fontSizeBaseSmall')};
  font-weight: 700;
  inline-size: max-content;
  height: calc(${th('gridUnit')} * 5);
  justify-content: center;
  min-width: calc(${th('gridUnit')} * 6);
  right: ${th('gridUnit')};
  text-transform: uppercase;
  ${space};
`
// #endregion
