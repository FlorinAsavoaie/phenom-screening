import { Item as BaseItem, Text as BaseText, ItemProps } from '@hindawi/ui'

import styled from 'styled-components'

interface TextInfoProps extends ItemProps {
  label: string
  value: string
}

const TextInfo: React.FC<TextInfoProps> = ({ label, value, ...rest }) => (
  <Item vertical {...rest}>
    <Text fontWeight={700} mb={1} whiteSpace="nowrap">
      {label}
    </Text>
    <Text title={value} whiteSpace="nowrap">
      {value}
    </Text>
  </Item>
)

// #region styles
const Text = styled(BaseText)`
  overflow: hidden;
  text-overflow: ellipsis;
  width: 100%;
`
const Item = styled(BaseItem)`
  width: 0;
`
// #endregion

export { TextInfo }
