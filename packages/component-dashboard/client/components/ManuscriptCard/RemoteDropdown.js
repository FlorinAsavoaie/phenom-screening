import { useRef, useState, useEffect } from 'react'
import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { Icon, Loader, Text, Item } from '@hindawi/ui'
import { useDebounce } from 'hindawi-shared/client/graphql/hooks'

const ClassNames = {
  ICON: 'dropdown-icon',
  ITEM: 'dropdown-item',
  SELECTED_ITEM: 'dropdown-selected-item',
}
function isNotInDropdown(target) {
  return (
    !target.getAttribute('class').includes(ClassNames.ICON) &&
    !target.getAttribute('class').includes(ClassNames.ITEM) &&
    !target.getAttribute('class').includes(ClassNames.SELECTED_ITEM)
  )
}
export default function RemoteDropdown({
  emptyMessage = 'No items.',
  error = undefined,
  initialValue,
  itemKey = 'id',
  items,
  label,
  loading,
  onOpen,
  onSelect,
  ...props
}) {
  const downwardPosition = { top: '28px', bottom: 'auto' }
  const upwardPosition = { top: 'auto', bottom: '28px' }
  const dropdownElement = useRef()
  const rootRef = useRef()
  const [isOpen, setIsOpen] = useState(false)
  const [selectedItem, setSelectedItem] = useState(initialValue)
  const [dropdownPosition, setPosition] = useState(downwardPosition)
  const debouncedLoading = useDebounce({ value: loading, delay: 200 })
  const getItemLabel = (item) => {
    if (typeof label === 'function') {
      return label(item)
    }
    return get(item, label)
  }
  const getItemKey = (item) => {
    if (typeof itemKey === 'function') {
      return itemKey(item)
    }
    return get(item, itemKey)
  }
  const selectItem = (item) => {
    setSelectedItem(item)
    setIsOpen(false)
  }
  const closeDropdown = (e) => {
    if (
      isOpen &&
      (e.which === 27 ||
        isNotInDropdown(e.target, rootRef) ||
        !rootRef.current.contains(e.target))
    ) {
      setIsOpen(false)
      setPosition(downwardPosition)
    }
  }
  const onClick = (e) => {
    if (!isOpen) {
      setPosition(downwardPosition)
      setIsOpen(true)
      onOpen()
    } else {
      setPosition(downwardPosition)
      setIsOpen(false)
    }
    e.stopPropagation()
  }
  const handleItemSelection = (item) => (e) => {
    e.stopPropagation()
    if (typeof onSelect === 'function') {
      onSelect(item, () => selectItem(item))
    } else {
      selectItem(item)
    }
  }
  useEffect(() => {
    const root = document.getElementById('root')
    root.addEventListener('click', closeDropdown)
    document.body.addEventListener('keyup', closeDropdown)
    return () => {
      root.removeEventListener('click', closeDropdown)
      document.body.removeEventListener('keyup', closeDropdown)
    }
  }, [isOpen])
  useEffect(() => {
    const observer = new IntersectionObserver(
      ([target]) => {
        if (target.intersectionRatio !== 1) {
          setPosition(upwardPosition)
        }
      },
      {
        threshold: 1.0,
        rootMargin: '0px',
        root: null,
      },
    )
    if (dropdownElement.current) {
      observer.observe(dropdownElement.current)
    }
    return () => {
      observer.disconnect(dropdownElement.current)
    }
  }, [isOpen])
  return (
    <Root {...props} ref={rootRef}>
      <DropdownHeaderItem
        alignItems="center"
        data-test-id="assigned-checker"
        isOpen={isOpen}
        onClick={onClick}
        pl={2}
        pr={2}
      >
        <Text className={ClassNames.SELECTED_ITEM} ellipsis>
          {selectedItem ? getItemLabel(selectedItem) : 'Unassigned'}
        </Text>

        {debouncedLoading ? (
          <Loader iconSize={3} mb={1} ml={1} />
        ) : (
          <Icon
            className={ClassNames.ICON}
            icon={isOpen ? 'caretUp' : 'caretDown'}
            ml={1}
          />
        )}
        {error && (
          <Text error ml={1}>
            {error}
          </Text>
        )}
      </DropdownHeaderItem>
      {isOpen && !loading && (
        <DropdownContainer position={dropdownPosition} ref={dropdownElement}>
          {items.length === 0 ? (
            <EmptyErrorText p={3}>{emptyMessage}</EmptyErrorText>
          ) : (
            items.map((item) => (
              <DropdownItem
                className={ClassNames.ITEM}
                data-test-id="reassignation-dropdown-item"
                key={getItemKey(item)}
                onClick={handleItemSelection(item)}
                selectedItem={get(selectedItem, 'id') === item.id}
              >
                {getItemLabel(item)}
              </DropdownItem>
            ))
          )}
        </DropdownContainer>
      )}
    </Root>
  )
}
// #region styles
// item height in grid units
const itemHeight = 8
const itemsOnDropdown = 10

const withEllipsis = css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const Root = styled.div`
  padding-left: ${th('gridUnit')};
  align-items: center;
  cursor: pointer;
  display: flex;
  position: relative;
`
const DropdownHeaderItem = styled(Item)`
  background-color: ${(props) => (props.isOpen ? 'white' : 'inherit')};
  height: calc(${th('gridUnit')} * 6);
  max-width: calc(${th('gridUnit')} * 52);
  border: 1px solid
    ${(props) =>
      props.isOpen ? th('textPrimaryColor') : th('backgroundColor')};
  border-radius: ${th('gridUnit')};
  &:hover {
    border: 1px solid ${th('textPrimaryColor')};
  }

  ${withEllipsis}
`
const DropdownItem = styled(Text)`
  cursor: pointer;
  font-weight: ${({ selectedItem }) => (selectedItem ? 700 : 'inherit')};
  height: calc(${th('gridUnit')} * ${itemHeight});
  line-height: 32px;
  padding: 0 calc(${th('gridUnit')} * 2);
  width: calc(${th('gridUnit')} * 56);
  display: block;
  &:hover {
    background-color: ${th('highlightColor')};
  }

  ${withEllipsis}
`
const dropdownPosition = ({ position }) => css`
  top: ${position.top};
  bottom: ${position.bottom};
`
const DropdownContainer = styled.div`
  box-shadow: 0 2px 6px ${th('disabledColor')};
  background-color: ${th('white')};
  max-height: calc(${th('gridUnit')} * ${itemHeight} * ${itemsOnDropdown});
  position: absolute;
  overflow-y: scroll;
  z-index: 1;
  width: calc(${th('gridUnit')} * 56);
  ${dropdownPosition};
`
const EmptyErrorText = styled(Text)`
  width: max-content;
`
// #endregion
