import { Card, Skeleton, SkeletonProps } from '@hindawi/phenom-ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'

const CardWrapper = styled(Card)`
  margin: 24px 0;
  border-radius: 4px;
  box-shadow: 0 1px 2px 1px #dbdbdb;
  padding: 0;
  width: inherit;

  .ant-card-body {
    padding: 0;
  }

  .ant-skeleton:last-child {
    width: 150px;

    .ant-skeleton-content {
      display: flex;
      justify-content: flex-end;
    }
  }
`
const TopManuscriptDetails = styled.div`
  display: flex;
  padding: 16px;
`
const FooterManuscriptDetails = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #f5f5f5;
  padding: calc(${th('gridUnit')}) calc(${th('gridUnit')} * 4);
  height: 35px;

  .ant-skeleton {
    width: 150px;
  }

  .ant-skeleton-paragraph {
    margin-bottom: 0;
  }
`

export const SkeletonManuscriptCard: React.FC<SkeletonProps> = () => (
  <CardWrapper>
    <TopManuscriptDetails>
      <Skeleton
        active
        paragraph={{ rows: 3, width: [140, 200, 100] }}
        title={{ width: '100%' }}
      />
      <Skeleton
        active
        paragraph={{ rows: 3, width: [140, 100, 100] }}
        title={{ width: 0 }}
      />
      <Skeleton active paragraph={{ rows: 0 }} title={{ width: 120 }} />
    </TopManuscriptDetails>

    <FooterManuscriptDetails>
      <Skeleton active paragraph={{ rows: 1, width: [150] }} title={false} />
      <Skeleton active paragraph={{ rows: 1, width: [150] }} title={false} />
      <Skeleton active paragraph={{ rows: 1, width: [150] }} title={false} />
    </FooterManuscriptDetails>
  </CardWrapper>
)
