import { Item, Label, Menu } from '@hindawi/ui'

export const ManuscriptFilters = ({
  filterOption,
  filterOptions,
  onFilterChange,
}) => (
  <Item alignItems="flex-start" flex={0} mr={4} vertical>
    <Label mb={1}>Filter by</Label>
    <Menu
      data-test-id="dashboard"
      onChange={onFilterChange}
      options={filterOptions}
      value={filterOption}
      width={40}
    />
  </Item>
)
