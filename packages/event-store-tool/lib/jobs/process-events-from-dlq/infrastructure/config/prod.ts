import { WithAwsSecretsCronProps } from '@hindawi/phenom-charts'

export const cronProps: WithAwsSecretsCronProps = {
  secretNames: ['prod/event-store-tool/job-event-store-tool-dlq'],
  cronProps: {
    schedule: '0 0 * * *', // everyday at 00:00.
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/job-event-store-tool-dlq',
      tag: 'production',
    },
    metadata: {
      labels: {
        owner: 'QAlas',
      },
    },
    concurrencyPolicy: 'Forbid',
    restartPolicy: 'Never',
    resources: {
      limits: {
        memory: '1942Mi',
      },
      requests: {
        memory: '1942Mi',
      },
    },
  },
}
