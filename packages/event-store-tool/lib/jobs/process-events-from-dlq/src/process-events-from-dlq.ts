/* eslint-disable */
import * as SQS from '@aws-sdk/client-sqs'
import { Knex, knex } from 'knex'

import { saveEvent } from '../../../lambdas/handle-event-store-events/src/saveEvent'
import { S3DataNotification } from '../../../lambdas/handle-event-store-events/src/interfaces'
import { getFileContent } from '../../../lambdas/handle-event-store-events/src/getFileContent'

require('dotenv').config()

interface Message extends S3DataNotification {
  ReceiptHandle: string
  ErrorMessage: string
}

const PRODUCER_QUEUE = process.env.EVENT_STORE_DLQ

if (!process.env.SQS_ACCESS_KEY) {
  throw new Error('Please provide sqs access key')
}

if (!process.env.SQS_SECRET_ACCESS_KEY) {
  throw new Error('Please provide sqs secret access key')
}

const sqs = new SQS.SQS({
  credentials: {
    accessKeyId: process.env.SQS_ACCESS_KEY,
    secretAccessKey: process.env.SQS_SECRET_ACCESS_KEY,
  },
  region: 'eu-west-1',
})

async function consumeEvents(): Promise<void> {
  const knexInstance = knex({
    client: 'pg',
    connection: {
      host: process.env.PROD_DB_HOST,
      database: process.env.PROD_DATABASE,
      user: process.env.PROD_DB_USER,
      password: process.env.PROD_DB_PASSWORD,
      port: 5432,
    },
  })
  const DELETE_FROM_PRODUCER_QUEUE = true

  async function* getDLQMessages(
    deleteMessages: boolean,
  ): AsyncGenerator<S3DataNotification> {
    const queueUrl = await sqs.getQueueUrl({ QueueName: PRODUCER_QUEUE! })

    while (true) {
      const messages = await receiveMessages(queueUrl.QueueUrl!)
      if (messages.length === 0) {
        break
      }
      const filteredMessages = messages.map(extractSQSMessageBody)

      for (const message of filteredMessages) {
        if (message.ErrorMessage?.includes('Task timed out')) {
          yield Object.assign(
            {},
            {
              Records: message.Records,
            },
          )

          if (deleteMessages) {
            await removeMessage(queueUrl.QueueUrl!, message)
          }
          console.log(`Deleted message ${message}`)
        }
      }
    }
  }

  for await (const event of getDLQMessages(DELETE_FROM_PRODUCER_QUEUE)) {
    await processEvent(event, knexInstance)
  }
}

async function processEvent(event: any, knex: Knex): Promise<void> {
  const data = await getFileContent(event)

  if (!data) return

  await Promise.all(
    data.events.map((event) =>
      saveEvent({
        knex,
        event,
        providerKey: data.providerKey,
        versionId: data.versionId,
      }),
    ),
  )
}
;(async () => {
  try {
    await consumeEvents()
  } catch (err) {
    console.log(err)
    process.exit(1)
  }
  process.exit(0)
})()

async function receiveMessages(queueUrl: string): Promise<SQS.Message[]> {
  const request: SQS.ReceiveMessageRequest = {
    QueueUrl: queueUrl,
    MaxNumberOfMessages: 10,
    MessageAttributeNames: ['ErrorMessage'],
  }
  const response = await sqs.receiveMessage(request)

  return response?.Messages || []
}

function extractSQSMessageBody(message: SQS.Message): Message {
  return {
    ...JSON.parse(message.Body!),
    ReceiptHandle: message.ReceiptHandle,
    ErrorMessage: message.MessageAttributes?.ErrorMessage.StringValue,
  }
}

async function removeMessage(
  queueUrl: string,
  message: Message,
): Promise<void> {
  const deleteParams = {
    QueueUrl: queueUrl,
    ReceiptHandle: message.ReceiptHandle,
  }
  await sqs.deleteMessage(deleteParams)
}
