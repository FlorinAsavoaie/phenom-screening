import * as cr from 'aws-cdk-lib/custom-resources'
import * as s3 from 'aws-cdk-lib/aws-s3'
import * as iam from 'aws-cdk-lib/aws-iam'
import * as lambda from 'aws-cdk-lib/aws-lambda'
import { Construct } from 'constructs'

export interface S3NotificationLambdaProps {
  bucket: s3.IBucket
  lambda: lambda.IFunction
  events: string[]
}

export class S3NotificationLambda extends Construct {
  constructor(scope: Construct, id: string, props: S3NotificationLambdaProps) {
    super(scope, id)

    const notificationResource = new cr.AwsCustomResource(
      scope,
      `${id}CustomResource`,
      {
        resourceType: 'Custom::S3NotificationResource',
        onCreate: {
          service: 'S3',
          action: 'putBucketNotificationConfiguration', // Enables notifications of specified events for a bucket.
          parameters: {
            // This bucket must be in the same region you are deploying to
            Bucket: props.bucket.bucketName,
            NotificationConfiguration: {
              // Describes the AWS Lambda function to invoke and the events for which to invoke it.
              LambdaFunctionConfigurations: [
                {
                  Events: props.events,
                  LambdaFunctionArn: props.lambda.functionArn,
                },
              ],
            },
          },
          physicalResourceId: (id +
            Date.now().toString()) as cr.PhysicalResourceId,
        },
        policy: cr.AwsCustomResourcePolicy.fromStatements([
          new iam.PolicyStatement({
            actions: ['S3:PutBucketNotification', 'S3:GetBucketNotification'],
            // allow this custom resource to modify this bucket
            resources: [props.bucket.bucketArn],
          }),
        ]),
      },
    )

    props.lambda.addPermission('AllowS3Invocation', {
      action: 'lambda:InvokeFunction',
      principal: new iam.ServicePrincipal('s3.amazonaws.com'),
      sourceArn: props.bucket.bucketArn,
    })

    // don't create the notification custom-resource until after both the bucket and lambda
    // are fully created and policies applied.
    notificationResource.node.addDependency(props.bucket)
    notificationResource.node.addDependency(props.lambda)
  }
}
