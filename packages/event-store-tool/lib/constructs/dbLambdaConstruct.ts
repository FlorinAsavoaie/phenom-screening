import * as cr from 'aws-cdk-lib/custom-resources'
import * as core from 'aws-cdk-lib'
import * as lambda from 'aws-cdk-lib/aws-lambda'
import * as rds from 'aws-cdk-lib/aws-rds'
import { Construct } from 'constructs'
import * as fs from 'fs'
import * as path from 'path'

export interface DBLambdaCustomResourceProps {
  lambda: lambda.IFunction
  database: rds.IDatabaseInstance
}

export class DBLambdaCustomResource extends Construct {
  constructor(
    scope: Construct,
    id: string,
    props: DBLambdaCustomResourceProps,
  ) {
    super(scope, id)

    const provider = new cr.Provider(this, 'CreateTablesLambda', {
      onEventHandler: props.lambda,
    })

    const customResource = new core.CustomResource(
      this,
      'CreateTablesResource',
      {
        resourceType: 'Custom::MigrationResource',
        serviceToken: provider.serviceToken,
        properties: {
          numberOfMigrations: DBLambdaCustomResource.getNumberOfMigrations(),
        },
      },
    )

    customResource.node.addDependency(props.lambda)
    customResource.node.addDependency(props.database)
  }

  private static getNumberOfMigrations(): number {
    const migrationsPath = path.join(
      __dirname,
      '../lambdas/migration-scripts/migrations',
    )

    const { length } = fs.readdirSync(migrationsPath)

    return length
  }
}
