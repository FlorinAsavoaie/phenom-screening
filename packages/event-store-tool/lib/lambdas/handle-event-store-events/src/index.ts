import { knex } from 'knex'
import { S3DataNotification } from './interfaces'
import { getFileContent } from './getFileContent'
import { saveEvent } from './saveEvent'

export const handler = async (event: S3DataNotification): Promise<void> => {
  const knexInstance = knex({
    client: 'pg',
    connection: {
      host: process.env.host,
      database: process.env.database,
      user: process.env.user,
      password: process.env.password,
      port: 5432,
    },
  })

  const data = await getFileContent(event)

  if (!data) return

  await Promise.all(
    data.events.map((event) =>
      saveEvent({
        knex: knexInstance,
        event,
        providerKey: data.providerKey,
        versionId: data.versionId,
      }),
    ),
  )

  return knexInstance.destroy()
}
