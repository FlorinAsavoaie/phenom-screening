/* eslint-disable camelcase */
interface EventToUpsert {
  event_id: string | null
  journal_id?: string
  submission_id?: string
  event_name: string | undefined
  event_timestamp: string | null
  provider_key: string
  version_id: string | null
  message_id: string
}

interface UpsertEventProps {
  trx: any
  event: EventToUpsert
  tableName: string
}

export async function upsertEvent({
  trx,
  event,
  tableName,
}: UpsertEventProps): Promise<void> {
  const objectToUpsert = {
    ...event,
    updated: trx.fn.now(),
  }

  if (!event.event_id) {
    return trx(tableName).insert(objectToUpsert)
  }

  const updatedQuery = await trx(tableName)
    .where('event_id', event.event_id)
    .orWhere('message_id', event.message_id)
    .update(objectToUpsert)

  if (updatedQuery < 1) {
    return trx(tableName).insert(objectToUpsert)
  }
}
