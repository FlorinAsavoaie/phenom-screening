import {
  createSubmissionEvent,
  createJournalEvent,
  createOtherEvent,
} from './factory'
import { upsertEvent } from './db/dbMethods'
import { SaveEventData } from './interfaces'

export async function saveEvent({
  knex,
  event,
  providerKey,
  versionId,
}: SaveEventData): Promise<void> {
  return knex
    .transaction(async (trx: any) => {
      const eventName = event.event.split(':').pop()
      if (event.data && event.data['production-in-flight-events']) {
        return
      }
      if (eventName?.includes('Submission')) {
        const mappedEvent = await createSubmissionEvent({
          event,
          providerKey,
          versionId,
        })
        return upsertEvent({
          trx,
          event: mappedEvent,
          tableName: 'submission_events',
        })
      } else if (eventName?.includes('Journal')) {
        const mappedEvent = await createJournalEvent({
          event,
          providerKey,
          versionId,
        })
        return upsertEvent({
          trx,
          event: mappedEvent,
          tableName: 'journal_events',
        })
      }
      const mappedEvent = await createOtherEvent({
        event,
        providerKey,
        versionId,
      })
      return upsertEvent({
        trx,
        event: mappedEvent,
        tableName: 'other_events',
      })
    })
    .catch((error: Error) => {
      throw new Error(`${error} at providerKey: ${providerKey}`)
    })
}
