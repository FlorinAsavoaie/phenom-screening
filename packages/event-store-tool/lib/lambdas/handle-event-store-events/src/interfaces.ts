import { Knex } from 'knex'

interface S3Object {
  key: string
  size: number
  eTag: string
  versionId?: string
  sequencer: string
}

interface OwnerIdentity {
  principalId: string
}

interface Bucket {
  name: string
  ownerIdentity: OwnerIdentity
  arn: string
}

interface S3 {
  s3SchemaVersion: string
  configurationId: string
  bucket: Bucket
  object: S3Object
}

interface UserIdentity {
  principalId: string
}

interface RequestParameters {
  sourceIPAddress: string
}

interface ResponseElements {
  'x-amz-request-id': string
  'x-amz-id-2': string
}

interface S3Record {
  eventVersion: string
  eventSource: string
  awsRegion: string
  eventTime: string
  eventName: string
  userIdentity: UserIdentity
  requestParameters: RequestParameters
  responseElements: ResponseElements
  s3: S3
}

export interface S3DataNotification {
  Records: S3Record[]
}

export interface PhenomEvent {
  messageId: string
  timestamp: string
  event: string
  data: any
  id?: string
}

export interface SaveEventData {
  knex: Knex
  event: PhenomEvent
  providerKey: string
  versionId: string | null
}
