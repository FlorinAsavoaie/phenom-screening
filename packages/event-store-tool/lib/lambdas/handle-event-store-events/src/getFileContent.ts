/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { S3 } from '@aws-sdk/client-s3'
import { S3DataNotification } from './interfaces'

const s3 = new S3({
  apiVersion: '2006-03-01',
})

const streamToString = (stream: any): Promise<string> =>
  new Promise((resolve, reject) => {
    const chunks: any[] = []
    stream.on('data', (chunk: any) => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
  })

async function parseEvents(data?: any): Promise<any[]> {
  const body = await streamToString(data?.Body)
  const parsedEvents = body.trimEnd()?.split('\n') ?? []

  return parsedEvents?.map((e: any) => {
    const event = JSON.parse(e)
    const eventBody = JSON.parse(event.body)

    return {
      messageId: event.messageId,
      ...eventBody,
    }
  })
}

export interface GetFileContentReturnValue {
  providerKey: string
  versionId: string | null
  events: any[]
}

async function getFileContent(
  event: S3DataNotification,
): Promise<GetFileContentReturnValue | undefined> {
  const bucket = event.Records[0].s3.bucket.name
  const providerKey = event.Records[0].s3.object.key
  const versionId = event.Records[0].s3.object.versionId ?? null

  const providerKeyDepth = providerKey.split('/').length
  // Provider key format: yyyy/mm/dd/hh/filename
  if (providerKeyDepth < 5) {
    return
  }

  const params = {
    Bucket: bucket,
    Key: providerKey,
  }

  try {
    const events = await s3.getObject(params).then((data) => parseEvents(data))

    return {
      providerKey,
      versionId,
      events,
    }
  } catch (error: any) {
    throw new Error(error)
  }
}

export { getFileContent }
