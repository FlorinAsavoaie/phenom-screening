/* eslint-disable camelcase */
import { PhenomEvent } from '../interfaces'

interface CreateJournalEventProps {
  event: PhenomEvent
  providerKey: string
  versionId: string | null
}

interface CreateJournalReturnValues {
  event_id: string | null
  journal_id: string
  event_name: string | undefined
  event_timestamp: string
  provider_key: string
  version_id: string | null
  message_id: string
}
export async function createJournalEvent({
  event,
  providerKey,
  versionId,
}: CreateJournalEventProps): Promise<CreateJournalReturnValues> {
  return {
    journal_id: event.data.id,
    event_id: event.id ?? null,
    event_name: event.event.split(':').pop(),
    event_timestamp: event.timestamp,
    provider_key: providerKey,
    version_id: versionId,
    message_id: event.messageId,
  }
}
