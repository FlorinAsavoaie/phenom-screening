/* eslint-disable camelcase */

import { PhenomEvent } from '../interfaces'

interface CreateOtherEventProps {
  event: PhenomEvent
  providerKey: string
  versionId: string | null
}

interface CreateOtherEventReturnValues {
  event_id: string | null
  event_name: string | undefined
  event_timestamp: string
  provider_key: string
  version_id: string | null
  message_id: string
}
export async function createOtherEvent({
  event,
  providerKey,
  versionId,
}: CreateOtherEventProps): Promise<CreateOtherEventReturnValues> {
  return {
    event_id: event.id ?? null,
    event_name: event.event.split(':').pop(),
    event_timestamp: event.timestamp,
    provider_key: providerKey,
    version_id: versionId,
    message_id: event.messageId,
  }
}
