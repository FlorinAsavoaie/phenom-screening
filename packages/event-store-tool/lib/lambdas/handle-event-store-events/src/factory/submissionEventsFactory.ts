/* eslint-disable camelcase */

import { PhenomEvent } from '../interfaces'

interface CreateSubmissionEventProps {
  event: PhenomEvent
  providerKey: string
  versionId: string | null
}

interface CreateSubmissionEventReturnValues {
  submission_id: string
  event_id: string | null
  event_name: string | undefined
  event_timestamp: string | null
  provider_key: string
  version_id: string | null
  message_id: string
}
export async function createSubmissionEvent({
  event,
  providerKey,
  versionId,
}: CreateSubmissionEventProps): Promise<CreateSubmissionEventReturnValues> {
  return {
    submission_id: event.data.submissionId,
    event_id: event.id ?? null,
    event_name: event.event.split(':').pop(),
    event_timestamp: event.timestamp ?? null,
    provider_key: providerKey,
    version_id: versionId,
    message_id: event.messageId,
  }
}
