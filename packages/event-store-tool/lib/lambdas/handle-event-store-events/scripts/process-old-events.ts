/* eslint-disable no-await-in-loop */

/** This script is used to process the event store and add all the old events in the database
 *
 * Required environment variables:
 * ```
 * PROD_DATABASE=***
 * PROD_DB_HOST=***
 * PROD_DB_PASSWORD=***
 * PROD_DB_USER=***
 * S3_EVENT_STORE_BUCKET=***
 * */

import { S3 } from '@aws-sdk/client-s3'
import { Promise as BluebirdPromise } from 'bluebird'

require('dotenv').config()
import { knex } from 'knex'

import { saveEvent } from '../src/saveEvent'

const s3 = new S3({
  region: process.env.AWS_REGION,
})

interface S3ObjectKeys {
  keys: any[]
  isTruncated: boolean
}

async function execute(errors: any[]): Promise<void> {
  const knexInstance = knex({
    client: 'pg',
    connection: {
      host: process.env.PROD_DB_HOST,
      database: process.env.PROD_DATABASE,
      user: process.env.PROD_DB_USER,
      password: process.env.PROD_DB_PASSWORD,
      port: 5432,
    },
  })
  const s3params = {
    Bucket: process.env.S3_EVENT_STORE_BUCKET!,
  }

  async function listObjects(lastKey: string): Promise<S3ObjectKeys> {
    const request = { Bucket: s3params.Bucket, StartAfter: lastKey }
    const response = await s3.listObjectsV2(request)
    const keys = response.Contents
      ? response?.Contents.map((obj) => obj.Key)
      : []
    return { isTruncated: response.IsTruncated || false, keys }
  }

  async function getObjectKeys(): Promise<string[]> {
    let lastKey = ''
    let allKeys: string[] = []
    while (true) {
      const { isTruncated, keys } = await listObjects(lastKey)
      allKeys = allKeys.concat(keys)
      if (!isTruncated) {
        break
      }
      lastKey = keys[keys.length - 1]
    }
    return allKeys
  }

  const streamToString = (stream: any): Promise<string> =>
    new Promise((resolve, reject) => {
      const chunks: any[] = []
      stream.on('data', (chunk: any) => chunks.push(chunk))
      stream.on('error', reject)
      stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
    })

  async function parseEvents(key: string, data?: any): Promise<any[]> {
    const body = await streamToString(data?.Body)
    const parsedEvents = body?.trimEnd()?.split('\n') ?? []

    return parsedEvents?.map((e: any) => {
      const event = JSON.parse(e)
      let eventBody
      try {
        eventBody = JSON.parse(event.body)
      } catch (e) {
        errors.push(`Can't parse event ${JSON.stringify(event)}\n${e}`)
        console.log(`Can't parse event ${JSON.stringify(event)}\n${e}`)
      }

      return {
        messageId: event.messageId,
        versionId: data.versionId,
        ...eventBody,
      }
    })
  }

  const keys = await getObjectKeys()

  await BluebirdPromise.each(keys, async (key: string) => {
    const events = await s3
      .getObject({ Bucket: s3params.Bucket, Key: key })
      .then((data) => parseEvents(key, data))
      .catch((err: Error) => {
        errors.push(`Cannot get events from key ${key} due to error: ${err}\n`)
        console.log(`Cannot get events from key ${key}`)
      })

    if (events) {
      return BluebirdPromise.each(events, async (event: any) =>
        saveEvent({
          knex: knexInstance,
          event,
          providerKey: key,
          versionId: event.versionId,
        }),
      ).catch((err: Error) => {
        errors.push(`Cannot save event from key ${key} due to ${err}\n`)
        console.log(`Cannot save event from key ${key} due to ${err}`)
      })
    }
  })
}

;(async function main() {
  const errors: any[] = []
  try {
    await execute(errors)
  } catch (err) {
    console.error('Something went wrong while processing...')

    console.error(err)
    process.exit(1)
  }

  console.info(`Process completed with the following errors: ${errors}`)
  process.exit(0)
})()
