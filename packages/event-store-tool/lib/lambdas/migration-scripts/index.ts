import { knex } from 'knex'

export const handler = async (): Promise<void> => {
  const knexInstance = knex({
    client: 'pg',
    connection: {
      host: process.env.host,
      database: process.env.database,
      user: process.env.user,
      password: process.env.password,
      port: 5432,
    },
  })

  return knexInstance.migrate.latest({
    directory: './dist/migrations',
  })
}
