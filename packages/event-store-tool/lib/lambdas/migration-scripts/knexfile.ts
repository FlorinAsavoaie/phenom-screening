require('ts-node/register')

module.exports = {
  client: 'pg',
  migrations: {
    directory: './migrations',
  },
}
