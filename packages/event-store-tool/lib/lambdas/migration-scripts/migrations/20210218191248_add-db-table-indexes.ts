/* eslint-disable no-console */
import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .table('submission_events', (table) => {
      table.index('message_id', 'idx_submission_message_id')
      table.index('event_id', 'idx_submission_event_id')
    })
    .then(() => {
      console.log('indexes added successfully on submission_events table')
      return knex.schema.table('journal_events', (table) => {
        table.index('message_id', 'idx_journal_message_id')
        table.index('event_id', 'idx_journal_event_id')
      })
    })
    .then(() => {
      console.log('indexes added successfully on journal_events table')
      return knex.schema.table('other_events', (table) => {
        table.index('message_id', 'idx_other_message_id')
        table.index('event_id', 'idx_other_event_id')
      })
    })
    .then(() => console.log('indexes added successfully on other_events table'))
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .table('submission_events', (table) => {
      table.dropIndex('message_id', 'idx_submission_message_id')
      table.dropIndex('event_id', 'idx_submission_event_id')
    })
    .then(() => {
      console.log('indexes dropped successfully on submission_events table')
      return knex.schema.table('journal_events', (table) => {
        table.dropIndex('message_id', 'idx_journal_message_id')
        table.dropIndex('event_id', 'idx_journal_event_id')
      })
    })
    .then(() => {
      console.log('indexes dropped successfully on journal_events table')
      return knex.schema.table('other_events', (table) => {
        table.dropIndex('message_id', 'idx_other_message_id')
        table.dropIndex('event_id', 'idx_other_event_id')
      })
    })
    .then(() =>
      console.log('indexes dropped successfully on other_events table'),
    )
}
