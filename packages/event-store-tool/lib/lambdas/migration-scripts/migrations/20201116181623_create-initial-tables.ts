/* eslint-disable no-console */

import { Knex } from 'knex'

async function createSubmissionTable(trx: Knex.Transaction): Promise<void> {
  const tableName = 'submission_events'
  await trx.schema
    .createTable(tableName, (table: Knex.TableBuilder) => {
      table.uuid('id').primary().defaultTo(trx.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(trx.fn.now())
      table.timestamp('updated').notNullable().defaultTo(trx.fn.now())
      table.uuid('submission_id')
      table.text('event_id')
      table.text('event_name')
      table.timestamp('event_timestamp')
      table.text('provider_key')
      table.text('version_id')
      table.uuid('message_id')

      table.unique(['event_id', 'message_id'])
    })
    .then(() => console.log(`${tableName} table created successfully`))
}

async function createJournalTable(trx: Knex.Transaction): Promise<void> {
  const tableName = 'journal_events'
  await trx.schema
    .createTable(tableName, (table: Knex.TableBuilder) => {
      table.uuid('id').primary().defaultTo(trx.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(trx.fn.now())
      table.timestamp('updated').notNullable().defaultTo(trx.fn.now())
      table.uuid('journal_id')
      table.text('event_id')
      table.text('event_name')
      table.timestamp('event_timestamp')
      table.text('provider_key')
      table.text('version_id')
      table.uuid('message_id')

      table.unique(['event_id', 'message_id'])
    })
    .then(() => console.log(`${tableName} table created successfully`))
}

async function createOtherTable(trx: Knex.Transaction): Promise<void> {
  const tableName = 'other_events'
  await trx.schema
    .createTable(tableName, (table: Knex.TableBuilder) => {
      table.uuid('id').primary().defaultTo(trx.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(trx.fn.now())
      table.timestamp('updated').notNullable().defaultTo(trx.fn.now())
      table.text('event_id')
      table.text('event_name')
      table.timestamp('event_timestamp')
      table.text('provider_key')
      table.text('version_id')
      table.uuid('message_id')

      table.unique(['event_id', 'message_id'])
    })
    .then(() => console.log(`${tableName} table created successfully`))
}

export async function up(knex: Knex): Promise<void> {
  return knex
    .transaction(async (trx: Knex.Transaction): Promise<void> => {
      await trx.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
      await createSubmissionTable(trx)
      await createJournalTable(trx)
      await createOtherTable(trx)
    })
    .then(() => {
      console.log('All tables were created.')
    })
    .catch((error: Error) => {
      throw new Error(error.message)
    })
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .dropTable('submission_events')
    .dropTable('journal_events')
    .dropTable('other_events')
    .then(() => console.log('All tables were droped.'))
    .catch((error: Error) => {
      throw new Error(error.message)
    })
}
