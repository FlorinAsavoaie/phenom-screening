const webpack = require('webpack')
const path = require('path')
const glob = require('glob')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

function toObject(paths) {
  const ret = {}

  paths.forEach((path) => {
    ret[path.replace('.ts', '')] = path
  })

  return ret
}

module.exports = {
  mode: 'production',
  entry: {
    index: './index.ts',
    ...toObject(glob.sync('./migrations/*')),
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  target: 'node',
  module: {
    rules: [
      {
        // Include ts, tsx, js, and jsx files.
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: {
                      node: '14',
                    },
                  },
                ],
                ['@babel/preset-typescript'],
              ],
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    // force unused dialects to resolve to the only one we use
    // and for whom we have the dependencies installed
    new webpack.ContextReplacementPlugin(
      /knex\/lib\/dialects/,
      /postgres\/index.js/,
    ),
  ],
  // pg optionally tries to require pg-native
  externals: {
    'pg-query-stream': 'pg-query-stream',
    'pg-native': 'pg-native',
    knex: 'commonjs knex',
  },
}
