import * as rds from 'aws-cdk-lib/aws-rds'
import * as ec2 from 'aws-cdk-lib/aws-ec2'
import * as core from 'aws-cdk-lib'
import * as lambda from 'aws-cdk-lib/aws-lambda'
import * as s3 from 'aws-cdk-lib/aws-s3'
import * as sm from 'aws-cdk-lib/aws-secretsmanager'
import * as sqs from 'aws-cdk-lib/aws-sqs'
import { Construct } from 'constructs'
import * as path from 'path'
import { S3NotificationLambda } from './constructs/s3NotificationLambdaConstruct'
import { DBLambdaCustomResource } from './constructs/dbLambdaConstruct'

export interface EventStoreToolProps extends core.StackProps {
  environmentName: string
  bucketName: string
  env: {
    account: string | undefined
    region: string | undefined
  }
}

export class EventStoreTool extends core.Stack {
  constructor(scope: Construct, id: string, props: EventStoreToolProps) {
    super(scope, id, props)

    const DATABASE_NAME = `${props.environmentName}EventStoreDatabase`

    const vpc = ec2.Vpc.fromLookup(
      this,
      `${props.environmentName}EventStore-vpc`,
      {
        isDefault: true,
      },
    )

    const secrets = new sm.Secret(
      this,
      `${props.environmentName}-event-store-secret`,
      {
        generateSecretString: {
          generateStringKey: 'password',
          secretStringTemplate: JSON.stringify({
            username: `${props.environmentName}EventStoreUser`,
          }),
          excludePunctuation: true,
        },
      },
    )

    const dbInstance = new rds.DatabaseInstance(
      this,
      `${props.environmentName}EventStoreDatabase`,
      {
        engine: rds.DatabaseInstanceEngine.postgres({
          version: rds.PostgresEngineVersion.VER_11_8,
        }),
        instanceType: ec2.InstanceType.of(
          ec2.InstanceClass.R5,
          ec2.InstanceSize.LARGE,
        ),
        vpc,
        maxAllocatedStorage: 200,
        vpcSubnets: {
          subnetType: ec2.SubnetType.PUBLIC,
        },
        storageType: rds.StorageType.GP2,
        databaseName: DATABASE_NAME,
        credentials: rds.Credentials.fromSecret(secrets),
        instanceIdentifier: DATABASE_NAME,
        enablePerformanceInsights: true,
      },
    )
    dbInstance.connections.allowDefaultPortFromAnyIpv4()
    dbInstance.connections.allowDefaultPortFrom(ec2.Peer.ipv6('::0/0'))

    const lambdaDLQ = new sqs.Queue(
      this,
      `${props.environmentName}EventStoreDLQ`,
      {
        queueName: `${props.environmentName}-event-store-DLQ`,
        retentionPeriod: core.Duration.days(14),
      },
    )

    const lambdaFunction = new lambda.Function(
      this,
      `${props.environmentName}GetEventStoreData`,
      {
        runtime: lambda.Runtime.NODEJS_14_X,
        handler: 'index.handler',
        code: lambda.Code.fromAsset(
          path.join(__dirname, './lambdas/handle-event-store-events/dist'),
        ),
        environment: {
          host: dbInstance.dbInstanceEndpointAddress,
          database: DATABASE_NAME,
          user: secrets.secretValueFromJson('username').toString(),
          password: secrets.secretValueFromJson('password').toString(),
        },
        retryAttempts: 1,
        timeout: core.Duration.seconds(15),
        deadLetterQueue: lambdaDLQ,
      },
    )

    const dbMigrationLambdaFunction = new lambda.Function(
      this,
      `${props.environmentName}CreateDbTables`,
      {
        runtime: lambda.Runtime.NODEJS_14_X,
        handler: './dist/index.handler',
        timeout: core.Duration.seconds(60),
        code: lambda.Code.fromAsset(
          path.join(__dirname, './lambdas/migration-scripts'),
        ),
        environment: {
          host: dbInstance.dbInstanceEndpointAddress,
          database: DATABASE_NAME,
          user: secrets.secretValueFromJson('username').toString(),
          password: secrets.secretValueFromJson('password').toString(),
        },
      },
    )

    new DBLambdaCustomResource(
      this,
      `${props.environmentName}DatabaseMigrationResource`,
      {
        lambda: dbMigrationLambdaFunction,
        database: dbInstance,
      },
    )

    const bucket = s3.Bucket.fromBucketName(
      this,
      `${props.environmentName}-event-store`,
      props.bucketName,
    )

    bucket.grantRead(lambdaFunction)

    new S3NotificationLambda(this, `${props.environmentName}S3Notification`, {
      bucket,
      lambda: lambdaFunction,
      events: [s3.EventType.OBJECT_CREATED],
    })
  }
}
