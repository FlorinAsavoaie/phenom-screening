import * as core from 'aws-cdk-lib'

import { EventStoreTool } from '../lib/event-store-tool'

const app = new core.App()

new EventStoreTool(app, 'ProdEventStoreTool', {
  environmentName: 'production',
  bucketName: 'phenom-event-storage-prod-3',
  env: {
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION,
  },
})
