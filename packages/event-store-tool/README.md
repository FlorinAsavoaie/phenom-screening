# Event Store Tool

This tool listens to an S3 event fired when a new phenom event is added into the event-store-bucket and triggers a lambda function that saves into a database information about each event

## Information saved in DB
- submission_id / journal_id 
- event_id
- event_name
- event_timestamp
- provider_key - path to the s3 file containing the event
- message_id 

## How to deploy the Event Store Tool

The deploy to production can be made in two ways:

1. the recommended way -> through a manual pipeline step in Gitlab. For this 2 variables have been added in the Gitlab variables :

``` 
AWS_DEFAULT_ACCOUNT
AWS_DEFAULT_REGION
```


2. manual using the cdk CLI command

```
cdk deploy "ProdEventStoreTool*" --profile X
```




