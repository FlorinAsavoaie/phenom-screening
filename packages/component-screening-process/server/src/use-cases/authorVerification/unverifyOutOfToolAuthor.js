function initialize({ Author, Manuscript, User }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, authorId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    await assertCanUnverifyOutOfToolAuthor({ manuscript, userId, authorId })

    await Author.updateBy({
      queryObject: {
        id: authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        isVerifiedOutOfTool: false,
      },
    })
  }

  async function assertCanUnverifyOutOfToolAuthor({
    manuscript,
    userId,
    authorId,
  }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })
    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot unverify out of tool author ${authorId} from manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
