const { forOwn } = require('lodash')

const initialize = ({ Author, ManuscriptChecks }) => ({
  execute: async ({ manuscriptId, authors }) => {
    forOwn(authors, async (authorList, authorId) => {
      try {
        await Author.updateBy({
          queryObject: {
            manuscriptId,
            id: authorId,
          },
          propertiesToUpdate: {
            isOnBadDebtList: authorList.isOnBadDebtList,
            isOnBlackList: authorList.isOnBlackList,
            isOnWatchList: authorList.isOnWatchList,
          },
        })
      } catch (error) {
        console.info(`Something went wrong while updating: ${error}`)
      }
    })

    try {
      await ManuscriptChecks.updateBy({
        queryObject: { manuscriptId },
        propertiesToUpdate: {
          sanctionListCheckDone: true,
        },
      })
    } catch (error) {
      console.info(`Something went wrong while updating: ${error}`)
    }

    const allChecksPerformed = await ManuscriptChecks.areAllChecksPerformed(
      manuscriptId,
    )
    if (allChecksPerformed) {
      await applicationEventBus.publishMessage({
        event: 'AutomaticChecksDone',
        data: {
          manuscriptId,
        },
      })
    }
  },
})

module.exports = {
  initialize,
}
