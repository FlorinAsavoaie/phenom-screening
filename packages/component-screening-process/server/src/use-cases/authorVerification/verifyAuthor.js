function initialize({ Author, PossibleAuthorProfile, Manuscript, User }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, authorId, possibleAuthorId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    await assertAuthorCanBeVerified({
      manuscript,
      authorId,
      userId,
    })

    await Author.updateBy({
      queryObject: {
        id: authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        isVerified: true,
        isVerifiedOutOfTool: false,
      },
    })

    await PossibleAuthorProfile.updateBy({
      queryObject: {
        id: possibleAuthorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        chosen: true,
      },
    })
  }

  async function assertAuthorCanBeVerified({ manuscript, authorId, userId }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot verify author ${authorId} from manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
