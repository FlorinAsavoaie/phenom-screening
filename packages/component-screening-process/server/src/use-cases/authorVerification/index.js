const verifyAuthorUseCase = require('./verifyAuthor')
const updateAuthorsUseCase = require('./updateAuthors')
const unverifyAuthorUseCase = require('./unverifyAuthor')
const cannotVerifyAuthorUseCase = require('./cannotVerifyAuthor')
const persistAuthorsSanctionListUseCase = require('./persistAuthorsSanctionList')
const verifyOutOfToolAuthorUseCase = require('./verifyOutOfToolAuthor')
const unverifyOutOfToolAuthorUseCase = require('./unverifyOutOfToolAuthor')
const publishAuthorConfirmedUseCase = require('./publishAuthorConfirmed')

module.exports = {
  verifyAuthorUseCase,
  updateAuthorsUseCase,
  unverifyAuthorUseCase,
  cannotVerifyAuthorUseCase,
  persistAuthorsSanctionListUseCase,
  verifyOutOfToolAuthorUseCase,
  unverifyOutOfToolAuthorUseCase,
  publishAuthorConfirmedUseCase,
}
