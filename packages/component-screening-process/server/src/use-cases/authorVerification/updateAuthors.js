const { Promise } = require('bluebird')
const { forOwn } = require('lodash')

const initialize = ({ Author, PossibleAuthorProfile, ManuscriptChecks }) => ({
  execute: async (data) => {
    //  will be removed
    //  since the event's data shape was changed, this will handle old data messages
    const customData = hasOldDataShape(data)
      ? transformIntoNewDataShape(data)
      : data

    const { manuscriptId, authors: authorsHashMap } = customData

    const manuscriptAuthors = await Author.findBy({
      queryObject: { manuscriptId },
    })
    await Promise.each(manuscriptAuthors, async (author) => {
      author.updateProperties({
        isVerified: authorsHashMap[author.email].isVerified,
        hasOrcidIssue: authorsHashMap[author.email].hasOrcidIssue,
        rorId: authorsHashMap[author.email].ror.id,
        rorScore: authorsHashMap[author.email].ror.score,
        rorAff: authorsHashMap[author.email].ror.aff,
      })
      return author.save()
    })

    forOwn(authorsHashMap, async ({ profiles: possibleAuthorProfiles }) => {
      if (
        possibleAuthorProfiles !== null &&
        Array.isArray(possibleAuthorProfiles)
      ) {
        await Promise.each(possibleAuthorProfiles, async (wosProfile) => {
          const authorProfile = new PossibleAuthorProfile({
            manuscriptId,
            email: wosProfile.email,
            surname: wosProfile.lastName,
            authorId: wosProfile.authorId,
            givenNames: wosProfile.firstName,
            noOfPublications: wosProfile.noOfPublications || 0,
            affiliation: wosProfile.affiliation,
            weight: wosProfile.weight || 0,
          })
          return authorProfile.save()
        })
      }
    })

    await ManuscriptChecks.updateBy({
      queryObject: { manuscriptId },
      propertiesToUpdate: {
        authorsVerified: true,
      },
    })

    const allChecksPerformed = await ManuscriptChecks.areAllChecksPerformed(
      manuscriptId,
    )

    if (allChecksPerformed) {
      await applicationEventBus.publishMessage({
        event: 'AutomaticChecksDone',
        data: {
          manuscriptId,
        },
      })
    }
  },
})

module.exports = {
  initialize,
}

//  will be removed
function hasOldDataShape(data) {
  const emailKey = Object.keys(data.authors)[0]
  const author = data.authors[emailKey]

  return typeof author === 'boolean' || Array.isArray(author)
}

function transformIntoNewDataShape(data) {
  const authors = {}

  Object.entries(data.authors).forEach(([email, value]) => {
    authors[email] =
      value === true
        ? (authors[email] = {
            isVerified: value,
            profiles: null,
            hasOrcidIssue: null,
          })
        : (authors[email] = {
            isVerified: false,
            profiles: value,
            hasOrcidIssue: null,
          })

    return authors
  })

  return {
    ...data,
    authors,
  }
}
