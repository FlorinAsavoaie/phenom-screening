const { Promise } = require('bluebird')

function initialize({
  models: { Author, PossibleAuthorProfile },
  services: { applicationEventBus },
}) {
  return {
    execute,
  }

  async function execute(manuscriptId) {
    const authors = await Author.findBy({
      queryObject: {
        manuscriptId,
        isVerifiedOutOfTool: true,
      },
    })

    const possibleAuthorProfiles = await PossibleAuthorProfile.findBy({
      queryObject: {
        manuscriptId,
        chosen: true,
      },
    })

    const authorProfiles = await Promise.mapSeries(
      possibleAuthorProfiles,
      async (profile) => {
        if (!profile.email || !profile.givenNames || !profile.surname) {
          const author = await Author.find(profile.authorId)

          return {
            ...profile,
            email: profile.email || author.email,
            givenNames: profile.givenNames || author.givenNames,
            surname: profile.surname || author.surname,
          }
        }
        return profile
      },
    )

    const allAuthors = [...authors, ...authorProfiles]

    await Promise.all(
      allAuthors.map(async (author) => {
        await applicationEventBus.publishMessage({
          event: 'AuthorConfirmed',
          data: {
            email: author.email,
            firstName: author.givenNames,
            lastName: author.surname,
          },
        })
      }),
    )
  }
}

module.exports = {
  initialize,
}
