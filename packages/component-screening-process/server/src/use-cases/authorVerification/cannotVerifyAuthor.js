function initialize({ Author, PossibleAuthorProfile, Manuscript, User }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, authorId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    await assertAuthorCannotBeVerified({ manuscript, userId, authorId })

    await Author.updateBy({
      queryObject: {
        id: authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        isVerified: null,
        isVerifiedOutOfTool: false,
      },
    })

    await PossibleAuthorProfile.updateBy({
      queryObject: {
        authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        chosen: false,
      },
    })
  }

  async function assertAuthorCannotBeVerified({
    manuscript,
    authorId,
    userId,
  }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot perform 'can't be verified' action for author ${authorId} from manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = { initialize, authsomePolicies }
