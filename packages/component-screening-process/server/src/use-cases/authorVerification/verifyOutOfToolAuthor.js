function initialize({ Author, PossibleAuthorProfile, Manuscript, User }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, authorId, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    await assertAuthorCanBeVerifiedOutOfTool({ manuscript, authorId, userId })

    await Author.updateBy({
      queryObject: {
        id: authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        isVerifiedOutOfTool: true,
        isVerified: false,
      },
    })

    await PossibleAuthorProfile.updateBy({
      queryObject: {
        authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        chosen: false,
      },
    })
  }

  async function assertAuthorCanBeVerifiedOutOfTool({
    manuscript,
    authorId,
    userId,
  }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })
    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot verify out of tool author ${authorId} from manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']
module.exports = {
  initialize,
  authsomePolicies,
}
