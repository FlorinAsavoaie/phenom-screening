function initialize({ models: { Manuscript }, services: { pubSub } }) {
  return {
    execute,
  }

  async function execute(submissionId) {
    await updateStatusForSubmission(submissionId)

    await pubSub
      .notifyThatSubmissionStatusFor(submissionId)
      .wasUpdatedBySystem()
  }
  async function updateStatusForSubmission(submissionId) {
    await Manuscript.updateBy({
      queryObject: {
        submissionId,
      },
      propertiesToUpdate: {
        status: Manuscript.Statuses.WITHDRAWN,
      },
    })
  }
}

module.exports = {
  initialize,
}
