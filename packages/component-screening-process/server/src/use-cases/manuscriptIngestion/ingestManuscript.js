const { getConfig } = require('hindawi-screening/config/publisher')
const { Promise } = require('bluebird')

const publisherConfig = getConfig()

function initialize({
  logger,
  models: { Manuscript },
  factories: { authorFactory, fileFactory, manuscriptFactory, memberFactory },
  useCases: {
    ingestManuscriptAuthorsEmailsUseCase,
    deleteManuscriptAuthorsEmailsUseCase,
  },
  s3Service,
}) {
  async function execute({ submissionId, manuscripts }) {
    const submittedManuscript = manuscripts[0]

    if (
      publisherConfig.shortArticleTypes.includes(
        submittedManuscript.articleType.name,
      )
    ) {
      logger.info(
        `The screening process is skipped for submission id ${submissionId} with article type: ${submittedManuscript.articleType.name}.`,
      )
      return
    }

    const authors = getAuthors(submittedManuscript.authors)
    const files = await getFiles(submittedManuscript.files)
    const editors = getEditors(submittedManuscript.editors)

    const latestManuscript = await Manuscript.getLatestManuscriptVersion({
      queryObject: {
        submissionId,
      },
    })

    const manuscriptIsADuplicate =
      latestManuscript &&
      latestManuscript.status !== Manuscript.Statuses.RETURNED_TO_DRAFT

    if (manuscriptIsADuplicate) {
      logger.error(
        `Manuscript with submissionId: ${submissionId}, version: ${submittedManuscript.version} was already ingested.`,
      )
      return
    }

    if (latestManuscript?.status === Manuscript.Statuses.RETURNED_TO_DRAFT) {
      latestManuscript.status = Manuscript.Statuses.OLDER_VERSION
      latestManuscript.authorResponded = false

      await latestManuscript.save()
      await deleteManuscriptAuthorsEmailsUseCase.execute({
        manuscriptId: latestManuscript.id,
      })
    }

    let manuscript = manuscriptFactory.createManuscript({
      submissionId,
      manuscript: submittedManuscript,
      step: Manuscript.Steps.IN_SCREENING_CHECKING,
      authors,
      files,
      members: editors,
      flowType: Manuscript.FlowTypes.SCREENING,
      journalId: submittedManuscript.journalId,
      authorResponded: !!latestManuscript,
    })

    manuscript = await Manuscript.upsertGraph(manuscript)

    await ingestManuscriptAuthorsEmailsUseCase.execute({
      manuscriptAuthorsList: manuscript.authors,
      manuscriptId: manuscript.id,
      manuscriptTitle: manuscript.title,
    })

    const manuscriptFile = manuscript.getManuscriptFile()

    if (!manuscriptFile) return

    await applicationEventBus.publishMessage({
      event: 'ManuscriptIngested',
      data: {
        authors: manuscript.authors,
        fileId: manuscriptFile.id,
        manuscriptId: manuscript.id,
        originalFileProviderKey: manuscriptFile.originalFileProviderKey,
      },
    })
  }

  function getAuthors(authors) {
    return authors.map((a) => authorFactory.createAuthor(a))
  }

  async function getFiles(files) {
    return Promise.map(files, async (file) => {
      await s3Service.uploadReviewFileToScreeningS3(file.providerKey)
      return fileFactory.createFile({
        file,
      })
    })
  }

  function getEditors(editors) {
    return editors.map((editor) =>
      memberFactory.createMember({ member: editor }),
    )
  }

  return { execute }
}

module.exports = {
  initialize,
}
