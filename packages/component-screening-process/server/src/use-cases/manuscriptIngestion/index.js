const ingestManuscriptUseCase = require('./ingestManuscript')
const persistConvertedFileUseCase = require('./persistConvertedFile')
const persistSuspiciousWordsUseCase = require('./persistSuspiciousWords')
const persistEnglishPercentageUseCase = require('./persistEnglishPercentage')
const persistSimilarityPercentageUseCase = require('./persistSimilarityPercentage')
const updateManuscriptStatusAfterChecksDoneUseCase = require('./updateManuscriptStatusAfterChecksDone')

module.exports = {
  ingestManuscriptUseCase,
  persistConvertedFileUseCase,
  persistSuspiciousWordsUseCase,
  persistEnglishPercentageUseCase,
  persistSimilarityPercentageUseCase,
  updateManuscriptStatusAfterChecksDoneUseCase,
}
