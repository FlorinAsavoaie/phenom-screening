const initialize = ({ ManuscriptChecks }) => ({
  execute: async ({ manuscriptId, englishPercentage = 0, wordCount }) => {
    await ManuscriptChecks.updateBy({
      queryObject: { manuscriptId },
      propertiesToUpdate: {
        wordCount,
        languagePercentage: englishPercentage,
      },
    })

    const allChecksPerformed = await ManuscriptChecks.areAllChecksPerformed(
      manuscriptId,
    )

    if (allChecksPerformed) {
      await applicationEventBus.publishMessage({
        event: 'AutomaticChecksDone',
        data: {
          manuscriptId,
        },
      })
    }
  },
})

module.exports = {
  initialize,
}
