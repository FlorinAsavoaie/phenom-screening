const initialize = ({ models: { File, ManuscriptChecks } }) => ({
  execute: async ({ fileId, convertedFileProviderKey }) => {
    const file = await File.find(fileId)

    file.updateProperties({ providerKey: convertedFileProviderKey })

    await file.save()

    await ManuscriptChecks.updateBy({
      queryObject: { manuscriptId: file.manuscriptId },
      propertiesToUpdate: {
        fileConverted: true,
      },
    })

    const allChecksPerformed = await ManuscriptChecks.areAllChecksPerformed(
      file.manuscriptId,
    )

    if (allChecksPerformed) {
      await applicationEventBus.publishMessage({
        event: 'AutomaticChecksDone',
        data: {
          manuscriptId: file.manuscriptId,
        },
      })
    }
  },
})

module.exports = {
  initialize,
}
