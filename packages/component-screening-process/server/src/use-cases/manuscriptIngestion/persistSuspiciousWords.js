const { forOwn, intersection } = require('lodash')

const initialize = ({ ManuscriptChecks }) => ({
  execute: async ({ manuscriptId, suspiciousWords }) => {
    const parsedSuspiciousWords = {}
    forOwn(suspiciousWords, (value, key) => {
      parsedSuspiciousWords[key] = {
        noOfOccurrences: typeof value === 'number' ? value : value.count,
        isPreviouslyVerified: false,
        reason: value.reason ? value.reason : '',
      }
    })

    const previousVersionSuspiciousWords =
      await ManuscriptChecks.getPreviousSuspiciousWords(manuscriptId)

    if (!previousVersionSuspiciousWords) {
      await ManuscriptChecks.updateBy({
        queryObject: { manuscriptId },
        propertiesToUpdate: {
          suspiciousWords: parsedSuspiciousWords,
        },
      })
    } else {
      const commonSuspiciousWords = intersection(
        Object.keys(parsedSuspiciousWords),
        Object.keys(previousVersionSuspiciousWords),
      )
      commonSuspiciousWords.forEach((word) => {
        parsedSuspiciousWords[word] = {
          ...parsedSuspiciousWords[word],
          isPreviouslyVerified: true,
        }
      })
      await ManuscriptChecks.updateBy({
        queryObject: { manuscriptId },
        propertiesToUpdate: {
          suspiciousWords: parsedSuspiciousWords,
        },
      })
    }

    const allChecksPerformed = await ManuscriptChecks.areAllChecksPerformed(
      manuscriptId,
    )

    if (allChecksPerformed) {
      await applicationEventBus.publishMessage({
        event: 'AutomaticChecksDone',
        data: {
          manuscriptId,
        },
      })
    }
  },
})

module.exports = {
  initialize,
}
