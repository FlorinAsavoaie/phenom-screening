const initialize = ({ ManuscriptChecks }) => ({
  execute: async ({
    manuscriptId,
    totalSimilarityPercentage,
    firstSourceSimilarityPercentage,
    reportUrl,
  }) => {
    await ManuscriptChecks.updateBy({
      queryObject: { manuscriptId },
      propertiesToUpdate: {
        totalSimilarityPercentage,
        firstSourceSimilarityPercentage,
        reportUrl,
      },
    })

    const allChecksPerformed = await ManuscriptChecks.areAllChecksPerformed(
      manuscriptId,
    )

    if (allChecksPerformed) {
      await applicationEventBus.publishMessage({
        event: 'AutomaticChecksDone',
        data: {
          manuscriptId,
        },
      })
    }
  },
})

module.exports = {
  initialize,
}
