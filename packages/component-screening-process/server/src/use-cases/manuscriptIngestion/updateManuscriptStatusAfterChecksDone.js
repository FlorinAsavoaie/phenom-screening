function initialize({
  models: { Manuscript },
  useCases: { assignManuscriptToChecker },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    if (manuscript.isInAutomaticChecking()) {
      await Manuscript.updateBy({
        queryObject: {
          id: manuscriptId,
        },
        propertiesToUpdate: {
          status: Manuscript.Statuses.IN_PROGRESS,
        },
      })

      await assignManuscriptToChecker.execute({ manuscriptId })
    }
  }
}

module.exports = {
  initialize,
}
