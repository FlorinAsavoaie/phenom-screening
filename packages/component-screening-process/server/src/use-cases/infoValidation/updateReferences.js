function initialize({ Manuscript, User }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, references, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    await assertCanUpdateReferences(manuscript, userId)

    manuscript.updateProperties({
      references,
    })
    await manuscript.save()
  }

  async function assertCanUpdateReferences(manuscript, userId) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot add references for manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
