function initialize({ ManuscriptValidations, Manuscript, User }) {
  return {
    execute,
  }

  async function execute({ manuscriptId, field, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    await assertCanToggleValidation({ manuscript, userId, field })

    const manuscriptValidations = await ManuscriptValidations.findOneBy({
      queryObject: { manuscriptId },
    })
    manuscriptValidations.updateProperties({
      [field]: !manuscriptValidations[field],
    })

    await manuscriptValidations.save()
  }

  async function assertCanToggleValidation({ manuscript, userId, field }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot select or deselect an option for field ${field} on manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
