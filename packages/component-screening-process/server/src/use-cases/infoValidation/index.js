const addNoteUseCase = require('./addNote')
const removeNoteUseCase = require('./removeNote')
const updateNoteUseCase = require('./updateNote')
const getScreeningSignedUrlUseCase = require('./getSignedURL')
const toggleValidationUseCase = require('./toggleValidation')
const updateReferencesUseCase = require('./updateReferences')

module.exports = {
  addNoteUseCase,
  removeNoteUseCase,
  updateNoteUseCase,
  getScreeningSignedUrlUseCase,
  toggleValidationUseCase,
  updateReferencesUseCase,
}
