const initialize = ({ models: { File }, s3Service }) => ({
  execute: async ({ fileId, format }) => {
    const file = await File.find(fileId)

    const fileFormat = File.getTypeFormats(format)

    if (!file[fileFormat]) {
      throw new Error('File not available')
    }
    return s3Service.getSignedUrl(file[fileFormat])
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
