function initialize({ Note, Manuscript, User }) {
  return { execute }

  async function execute({ noteId, userId }) {
    const note = await Note.find(noteId)
    const manuscript = await Manuscript.find(note.manuscriptId)

    await assertCanRemoveNote({ manuscript, note, userId })

    await note.delete()
  }

  async function assertCanRemoveNote({ manuscript, note, userId }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot remove a note of type ${note.type} from manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
