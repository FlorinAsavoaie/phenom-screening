function initialize({ Note, Manuscript, User }) {
  return {
    execute,
  }

  async function execute({ noteId, content, userId }) {
    const note = await Note.find(noteId)
    const manuscript = await Manuscript.find(note.manuscriptId)

    await assertCanUpdateNote({ manuscript, note, userId })

    note.updateProperties({
      content,
    })
    await note.save()
  }

  async function assertCanUpdateNote({ manuscript, note, userId }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })

    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot write a note of type ${note.type} on manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
