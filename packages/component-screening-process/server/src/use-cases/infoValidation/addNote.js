function initialize({
  models: { Note, Manuscript, User },
  factories: { noteFactory },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, type, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    await assertCanAddNote({ manuscript, noteType: type, userId })

    let note = await Note.findOneBy({ queryObject: { manuscriptId, type } })
    if (note) return

    note = noteFactory.createNote({ manuscriptId, type })

    return note.save()
  }

  async function assertCanAddNote({ manuscript, noteType, userId }) {
    const canDoActionAccordingToStatus =
      await User.canDoActionAccordingToStatus({
        userId,
        manuscriptStatus: manuscript.status,
        manuscriptId: manuscript.id,
      })
    if (!canDoActionAccordingToStatus) {
      throw new ValidationError(
        `You cannot add a note of type ${noteType} for manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
