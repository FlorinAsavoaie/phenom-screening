function initialize({
  models: { Manuscript, Note, AuditLog, User },
  services: { pubSub },
  mappers: { manuscriptMapper },
  factories: { auditLogFactory },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[authors, files, members, auditLogs]',
    )

    if (manuscript.status !== Manuscript.Statuses.PAUSED) {
      throw new ValidationError(
        `You cannot unpause manuscript ${manuscriptId} while on ${manuscript.status} status.`,
      )
    }

    const updatedManuscript = await updateManuscript(manuscript, userId)

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)

    await emitEventWhenManuscriptIsUnpaused(updatedManuscript)

    return updatedManuscript
  }

  async function updateManuscript(manuscript, userId) {
    const decisionMakerRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId: manuscript.id,
    })

    const auditLog = auditLogFactory.createAuditLog({
      userId,
      manuscriptId: manuscript.id,
      submissionId: manuscript.submissionId,
      action: AuditLog.Actions.UNPAUSE_MANUSCRIPT,
      userRole: decisionMakerRole,
    })

    manuscript.auditLogs = [...manuscript.auditLogs, auditLog]
    manuscript.status = Manuscript.Statuses.IN_PROGRESS

    return Manuscript.upsertGraph(manuscript)
  }

  async function emitEventWhenManuscriptIsUnpaused(manuscript) {
    const manuscriptEventData = manuscriptMapper.domainToEventModel(manuscript)

    const FlowTypes = {
      qualityCheck: 'QualityChecking',
      screening: 'Screening',
    }

    await applicationEventBus.publishMessage({
      event: `Submission${FlowTypes[manuscript.flowType]}Unpaused`,
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: [manuscriptEventData],
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
