const { pickBy } = require('lodash')
const { ValidationError } = require('@pubsweet/errors')

function initialize({
  models: { Manuscript, Author, User, Member },
  mappers: { manuscriptMapper },
  factories: { noteFactory },
  useCases: { publishAuthorConfirmedUseCase },
  services: { notificationService, pubSub },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, content, note, userId }) {
    const editorialAssistant = await Member.getEditorialAssistant(manuscriptId)
    const manuscript = await moveManuscriptInScreeningRejected(manuscriptId)

    await saveDecisionNote({ manuscriptId, note })
    await publishAuthorConfirmedUseCase.execute(manuscript.id)
    await notifyAuthorOnEmail({
      manuscript,
      content,
      note,
      userId,
      editorialAssistant,
    })
    await publishEvent(manuscript)
    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)
  }

  async function moveManuscriptInScreeningRejected(manuscriptId) {
    const manuscript = await Manuscript.find(manuscriptId, '[authors, journal]')

    assertManuscriptIsInProgress(manuscript)

    manuscript.updateProperties({
      status: Manuscript.Statuses.SCREENING_REJECTED,
    })
    await manuscript.save()

    return manuscript
  }

  function assertManuscriptIsInProgress(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot refuse to consider manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }

  async function saveDecisionNote({ manuscriptId, note }) {
    const newNote = noteFactory.createDecisionNote({
      note,
      manuscriptId,
    })

    await newNote.save()
  }

  async function notifyAuthorOnEmail({
    manuscript,
    content,
    note,
    userId,
    editorialAssistant,
  }) {
    const observations = pickBy(content, (item) => item)

    const correspondingAuthor = await Author.getCorrespondingAuthor(
      manuscript.id,
    )
    const currentUser = await User.find(userId)

    await notificationService.notifyThatManuscriptWasRefused({
      additionalComments: note,
      emailRecipient: correspondingAuthor,
      editorialAssistant,
      currentUser,
      manuscript,
      observations: Object.keys(observations),
    })
  }

  async function publishEvent(manuscript) {
    const { submissionId } = manuscript
    const manuscripts = await Manuscript.findBy({
      queryObject: {
        submissionId,
      },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: '[authors, files, members]',
    })
    const mappedManuscripts = manuscripts.map(
      manuscriptMapper.domainToEventModel,
    )
    await applicationEventBus.publishMessage({
      event: 'SubmissionScreeningRTCd',
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: mappedManuscripts,
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
