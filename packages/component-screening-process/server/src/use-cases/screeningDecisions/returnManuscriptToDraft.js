const { pickBy } = require('lodash')
const { ValidationError } = require('@pubsweet/errors')

function initialize({
  models: { Manuscript, Note, Author, User, Member },
  mappers: { manuscriptMapper },
  factories: { noteFactory },
  services: { notificationService, pubSub },
  useCases: { publishAuthorConfirmedUseCase },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, content, userId }) {
    const editorialAssistant = await Member.getEditorialAssistant(manuscriptId)
    const manuscript = await moveManuscriptInReturnedToDraft(manuscriptId)

    await saveDecisionNote({ manuscriptId, content })
    await notifyAuthorOnEmail({
      manuscript,
      content,
      userId,
      editorialAssistant,
    })
    await publishAuthorConfirmedUseCase.execute(manuscriptId)
    await publishSubmissionScreeningReturnedToDraftEvent(manuscript)
    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)
  }

  async function moveManuscriptInReturnedToDraft(manuscriptId) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[manuscriptValidations, authors, files, members]',
    )

    assertManuscriptIsInProgress(manuscript)

    manuscript.updateProperties({
      status: Manuscript.Statuses.RETURNED_TO_DRAFT,
    })
    await manuscript.save()

    return manuscript
  }

  function assertManuscriptIsInProgress(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot return manuscript ${manuscript.id} to draft while on ${manuscript.status} status.`,
      )
    }
  }

  async function saveDecisionNote({ manuscriptId, content }) {
    const note = noteFactory.createDecisionNote({
      content,
      manuscriptId,
    })

    await note.save()
  }

  async function notifyAuthorOnEmail({
    manuscript,
    content,
    userId,
    editorialAssistant,
  }) {
    const manuscriptNotes = await Note.findValidationNotes({
      queryObject: { manuscriptId: manuscript.id },
    })

    const parseNotes = (notes) =>
      notes.map((note) => ({
        type: note.type,
        content: note.content,
      }))

    const observations = pickBy(
      manuscript.manuscriptValidations,
      (obs) => obs === true,
    )
    const correspondingAuthor = await Author.getCorrespondingAuthor(
      manuscript.id,
    )
    const currentUser = await User.find(userId)

    await notificationService.notifyThatManuscriptReturnedToDraft({
      additionalComments: content,
      correspondingAuthor,
      editorialAssistant,
      currentUser,
      manuscript,
      manuscriptNotes: parseNotes(manuscriptNotes),
      observations: Object.keys(observations),
    })
  }

  async function publishSubmissionScreeningReturnedToDraftEvent(manuscript) {
    const manuscriptEventData = manuscriptMapper.domainToEventModel(manuscript)
    await applicationEventBus.publishMessage({
      event: 'SubmissionScreeningReturnedToDraft',
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: [manuscriptEventData],
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
