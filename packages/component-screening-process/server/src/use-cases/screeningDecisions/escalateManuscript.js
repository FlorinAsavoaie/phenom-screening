const { Promise } = require('bluebird')

function initialize({
  models: { Manuscript, Note, User, AuditLog },
  services: { pubSub, notificationService },
  mappers: { manuscriptMapper },
  factories: { auditLogFactory },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, reason, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[authors, files, members, team.users, journal, auditLogs, notes]',
    )

    assertManuscriptCanBeEscalated(manuscript)

    const updatedManuscript = await updateManuscript({
      manuscript,
      reason,
      userId,
    })

    const currentUser = await User.find(userId)
    await notifyTeamLeaders({
      currentUser,
      manuscript: updatedManuscript,
      reason,
    })

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)

    await emitEventWhenManuscriptIsEscalated(updatedManuscript)

    return updatedManuscript
  }

  function assertManuscriptCanBeEscalated(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot escalate manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }

  async function updateManuscript({ manuscript, reason, userId }) {
    const note = new Note({
      manuscriptId: manuscript.id,
      type: Note.Types.ESCALATION_REASON_NOTE,
      content: reason,
    })

    const decisionMakerRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId: manuscript.id,
    })
    const auditLog = auditLogFactory.createAuditLog({
      userId,
      manuscriptId: manuscript.id,
      submissionId: manuscript.submissionId,
      action: AuditLog.Actions.SENT_TO_TEAM_LEAD,
      comment: reason,
      userRole: decisionMakerRole,
    })

    manuscript.notes = [...manuscript.notes, note]
    manuscript.auditLogs = [...manuscript.auditLogs, auditLog]
    manuscript.status = Manuscript.Statuses.ESCALATED

    return Manuscript.upsertGraph(manuscript)
  }

  async function notifyTeamLeaders({ currentUser, manuscript, reason }) {
    const confirmedTeamLeaders = manuscript.team.users.filter(
      (user) => user.role === User.Roles.TEAM_LEADER && user.isConfirmed,
    )
    await Promise.each(confirmedTeamLeaders, async (teamLeader) =>
      notificationService.notifyTLThatManuscriptWasEscalated({
        reason,
        teamLeader,
        currentUser,
        manuscript,
      }),
    )
  }

  async function emitEventWhenManuscriptIsEscalated(manuscript) {
    const manuscriptEventData = manuscriptMapper.domainToEventModel(manuscript)

    const FlowTypes = {
      qualityCheck: 'QualityChecking',
      screening: 'Screening',
    }

    await applicationEventBus.publishMessage({
      event: `Submission${FlowTypes[manuscript.flowType]}Escalated`,
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: [manuscriptEventData],
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
