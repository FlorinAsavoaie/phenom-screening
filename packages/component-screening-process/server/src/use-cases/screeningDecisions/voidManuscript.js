function initialize({
  models: { Manuscript, Note, User },
  services: { pubSub },
  mappers: { manuscriptMapper },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, reason, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[authors, files, members]',
    )

    assertManuscriptCanBeVoid(manuscript)

    const note = new Note({
      type: Note.Types.VOID_REASON_NOTE,
      content: reason,
    })

    manuscript.notes = [note]
    manuscript.status = Manuscript.Statuses.VOID

    await manuscript.saveGraph()

    pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)

    await emitEventWhenManuscriptIsVoid(manuscript)
  }

  function assertManuscriptCanBeVoid(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot void manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
    if (manuscript.flowType !== Manuscript.FlowTypes.SCREENING) {
      throw new ValidationError(
        `You cannot void manuscript ${manuscript.id} while on ${manuscript.flowType} flow.`,
      )
    }
  }

  async function emitEventWhenManuscriptIsVoid(manuscript) {
    const manuscriptEventData = manuscriptMapper.domainToEventModel(manuscript)

    await applicationEventBus.publishMessage({
      event: `SubmissionScreeningVoid`,
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: [manuscriptEventData],
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
