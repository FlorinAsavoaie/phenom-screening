const approveDecisionUseCase = require('./approveDecision')
const returnManuscriptToDraftUseCase = require('./returnManuscriptToDraft')
const refuseToConsiderDecisionUseCase = require('./refuseToConsiderDecision')
const pauseManuscriptUseCase = require('./pauseManuscript')
const unpauseManuscriptUseCase = require('./unpauseManuscript')
const voidManuscriptUseCase = require('./voidManuscript')
const escalateManuscriptUseCase = require('./escalateManuscript')
const returnToCheckerUseCase = require('./returnToChecker')

module.exports = {
  voidManuscriptUseCase,
  approveDecisionUseCase,
  pauseManuscriptUseCase,
  unpauseManuscriptUseCase,
  returnManuscriptToDraftUseCase,
  refuseToConsiderDecisionUseCase,
  escalateManuscriptUseCase,
  returnToCheckerUseCase,
}
