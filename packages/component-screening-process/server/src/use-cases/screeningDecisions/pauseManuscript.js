function initialize({
  models: { Manuscript, Note, AuditLog, User },
  services: { pubSub },
  mappers: { manuscriptMapper },
  factories: { auditLogFactory },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, reason, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[authors, files, members, notes, auditLogs]',
    )

    assertManuscriptCanBePaused(manuscript)

    const updatedManuscript = await updateManuscript({
      manuscript,
      reason,
      userId,
    })

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)

    await emitEventWhenManuscriptIsPaused(updatedManuscript)

    return updatedManuscript
  }

  function assertManuscriptCanBePaused(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot pause manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }

  async function updateManuscript({ manuscript, reason, userId }) {
    const note = new Note({
      manuscriptId: manuscript.id,
      type: Note.Types.PAUSE_REASON_NOTE,
      content: reason,
    })

    const decisionMakerRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId: manuscript.id,
    })

    const auditLog = auditLogFactory.createAuditLog({
      userId,
      manuscriptId: manuscript.id,
      submissionId: manuscript.submissionId,
      action: AuditLog.Actions.PAUSE_MANUSCRIPT,
      comment: reason,
      userRole: decisionMakerRole,
    })

    manuscript.notes = [...manuscript.notes, note]
    manuscript.auditLogs = [...manuscript.auditLogs, auditLog]
    manuscript.status = Manuscript.Statuses.PAUSED

    return Manuscript.upsertGraph(manuscript)
  }

  async function emitEventWhenManuscriptIsPaused(manuscript) {
    const manuscriptEventData = manuscriptMapper.domainToEventModel(manuscript)

    const FlowTypes = {
      qualityCheck: 'QualityChecking',
      screening: 'Screening',
    }

    await applicationEventBus.publishMessage({
      event: `Submission${FlowTypes[manuscript.flowType]}Paused`,
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: [manuscriptEventData],
      },
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
