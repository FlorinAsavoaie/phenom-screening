function initialize({
  models: { Manuscript, User, AuditLog },
  services: { notificationService, pubSub },
  mappers: { manuscriptMapper },
  factories: { auditLogFactory },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId, userId, reason }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[authors, files, members, journal, notes, auditLogs]',
    )
    assertManuscriptCanBeReturnedToChecker(manuscript)

    const updatedManuscript = await updateManuscript({
      manuscript,
      reason,
      userId,
    })

    const currentUser = await User.find(userId)

    await notifyChecker({ currentUser, manuscript: updatedManuscript, reason })

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)

    await emitEventWhenManuscriptIsReturnedToChecker(updatedManuscript)

    return updatedManuscript
  }

  function assertManuscriptCanBeReturnedToChecker(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.ESCALATED) {
      throw new ValidationError(
        `You cannot return to checker(de-escalate) manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }

  async function updateManuscript({ manuscript, reason, userId }) {
    const decisionMakerRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId: manuscript.id,
    })
    const auditLog = auditLogFactory.createAuditLog({
      userId,
      manuscriptId: manuscript.id,
      submissionId: manuscript.submissionId,
      action: AuditLog.Actions.RETURN_TO_CHECKER,
      comment: reason,
      userRole: decisionMakerRole,
    })

    manuscript.auditLogs = [...manuscript.auditLogs, auditLog]
    manuscript.status = Manuscript.Statuses.IN_PROGRESS

    return Manuscript.upsertGraph(manuscript)
  }

  async function notifyChecker({ currentUser, manuscript, reason }) {
    const checker = await User.findAssignedChecker(manuscript.id)

    await notificationService.notifyCheckerThatManuscriptIsUnescalated({
      reason,
      checker,
      currentUser,
      manuscript,
    })
  }

  async function emitEventWhenManuscriptIsReturnedToChecker(manuscript) {
    const manuscriptEventData = manuscriptMapper.domainToEventModel(manuscript)

    const FlowTypes = {
      qualityCheck: 'QualityChecking',
      screening: 'Screening',
    }

    await applicationEventBus.publishMessage({
      event: `Submission${FlowTypes[manuscript.flowType]}ReturnedToChecker`,
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: [manuscriptEventData],
      },
    })
  }
}
const authsomePolicies = [
  'authenticatedUser',
  'hasAccessToManuscript',
  'isAdminOrTeamLead',
]

module.exports = {
  initialize,
  authsomePolicies,
}
