function initialize({
  models: { Manuscript, Member },
  mappers: { manuscriptMapper },
  useCases: { publishAuthorConfirmedUseCase },
  services: { notificationService, pubSub },
}) {
  return { execute }

  async function execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[authors, files, members]',
    )
    assertManuscriptCanBeApproved(manuscript)

    const editorialAssistant = await Member.getEditorialAssistant(manuscript.id)

    await publishAuthorConfirmedUseCase.execute(manuscript.id)

    manuscript.updateProperties({
      status: Manuscript.Statuses.SCREENING_APPROVED,
    })

    await manuscript.save()

    await notifyEditorialAssistant({ manuscript, editorialAssistant, content })

    const manuscriptEventData = manuscriptMapper.domainToEventModel(manuscript)

    await applicationEventBus.publishMessage({
      event: 'SubmissionScreeningPassed',
      data: {
        submissionId: manuscript.submissionId,
        manuscripts: [manuscriptEventData],
      },
    })

    await pubSub
      .notifyThatSubmissionStatusFor(manuscript.submissionId)
      .wasUpdatedByUser(userId)
  }

  function assertManuscriptCanBeApproved(manuscript) {
    if (manuscript.status !== Manuscript.Statuses.IN_PROGRESS) {
      throw new ValidationError(
        `You cannot approve manuscript ${manuscript.id} while on ${manuscript.status} status.`,
      )
    }
  }

  async function notifyEditorialAssistant({
    manuscript,
    editorialAssistant,
    content,
  }) {
    await notificationService.notifyEAWhenManuscriptIsAccepted({
      additionalComments: content,
      editorialAssistant,
      manuscript,
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
