function initialize({ models: { Manuscript } }) {
  return {
    execute,
  }

  async function execute({ manuscriptId }) {
    const targetManuscript = await Manuscript.find(manuscriptId, 'authors')

    const similarSubmissions = await Manuscript.getSimilarSubmissions(
      targetManuscript,
    )

    return similarSubmissions.filter(filterScreeningVersionIfQCExists)
  }

  function filterScreeningVersionIfQCExists(manuscript, index, manuscripts) {
    if (manuscript.flowType === Manuscript.FlowTypes.SCREENING) {
      return !manuscripts.find(
        (m) =>
          m.submissionId === manuscript.submissionId &&
          m.flowType === Manuscript.FlowTypes.QUALITY_CHECK,
      )
    }
    return true
  }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
