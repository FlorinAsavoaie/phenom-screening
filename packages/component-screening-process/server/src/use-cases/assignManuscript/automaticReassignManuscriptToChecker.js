const { isEmpty, upperFirst } = require('lodash')
const logger = require('@pubsweet/logger')

const initialize = ({ models: { User }, mappers: { userMapper } }) => {
  return {
    execute,
  }

  async function execute(manuscript) {
    const checkerRole = User.getRoleByManuscriptFlowType(manuscript.flowType)

    const checkerWithTeams =
      await User.getCheckerFromTeamsWithManuscriptJournal({
        userId: manuscript.user.id,
        checkerRole,
        journalId: manuscript.journalId,
      })

    if (isEmpty(checkerWithTeams) || isEmpty(checkerWithTeams.teams)) {
      await assignNewChecker({ checkerRole, manuscript })
    } else {
      await assignTheSameCheckerFromDifferentTeam({
        checkerWithTeams,
        manuscript,
      })
    }
  }

  async function assignNewChecker({ checkerRole, manuscript }) {
    const newChecker = await User.getFirstAvailableChecker(
      checkerRole,
      manuscript.journalId,
    )

    if (isEmpty(newChecker)) {
      logger.info(
        `No checker available with role: ${checkerRole} for manuscript: ${manuscript.id}`,
      )

      await manuscript.deleteRelation('user')
      await manuscript.deleteRelation('team')

      return
    }

    const assignedChecker = await manuscript.assignChecker(newChecker)

    await emitEventWhenCheckerIsAssigned({
      checker: assignedChecker,
      manuscript,
      assignationType: User.AssignationTypes.automaticReassignmentToANewChecker,
    })
  }

  async function assignTheSameCheckerFromDifferentTeam({
    checkerWithTeams,
    manuscript,
  }) {
    const newChecker = {
      id: checkerWithTeams.id,
      team_id: checkerWithTeams.teams[0].id,
    }

    const assignedChecker = await manuscript.assignChecker(newChecker)

    await emitEventWhenCheckerIsAssigned({
      checker: assignedChecker,
      manuscript,
      assignationType:
        User.AssignationTypes
          .automaticReassignmentToTheSameCheckerFromDifferentTeam,
    })
  }

  async function emitEventWhenCheckerIsAssigned({
    checker,
    manuscript,
    assignationType,
  }) {
    const mappedChecker = userMapper.domainToCheckerEventModel({
      checker,
      manuscript,
      assignationType,
    })

    await applicationEventBus.publishMessage({
      event: `${upperFirst(mappedChecker.role)}Assigned`,
      data: mappedChecker,
    })
  }
}

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
