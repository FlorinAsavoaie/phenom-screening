const assignManuscriptToCheckerUseCase = require('./assignManuscriptToChecker')
const reassignManuscriptToCheckerUseCase = require('./reassignManuscriptToChecker')
const assignUnassignedManuscriptsToCheckersUseCase = require('./assignUnassignedManuscriptsToCheckers')
const automaticReassignManuscriptToCheckerUseCase = require('./automaticReassignManuscriptToChecker')

module.exports = {
  assignManuscriptToCheckerUseCase,
  reassignManuscriptToCheckerUseCase,
  assignUnassignedManuscriptsToCheckersUseCase,
  automaticReassignManuscriptToCheckerUseCase,
}
