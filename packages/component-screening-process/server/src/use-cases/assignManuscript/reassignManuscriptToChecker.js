const { upperFirst } = require('lodash')

function initialize({ models: { Manuscript, User }, mappers: { userMapper } }) {
  return { execute }

  async function execute({ manuscriptId, userId }) {
    const manuscript = await Manuscript.findOneBy({
      queryObject: {
        id: manuscriptId,
      },
      eagerLoadRelations: 'team',
    })
    if (Manuscript.filteredStatuses.archived.includes(manuscript.status)) {
      throw new ValidationError(
        `You cannot reassign manuscript ${manuscriptId} while on ${manuscript.status} status.`,
      )
    }

    const checkerRole = User.getRoleByManuscriptFlowType(manuscript.flowType)

    const checker = await getChecker({ userId, manuscript, checkerRole })

    const assignedChecker = await manuscript.assignChecker(checker)

    await emitEventWhenCheckerIsAssigned({
      checker: assignedChecker,
      manuscript,
      assignationType: User.AssignationTypes.manualReassignment,
    })

    return assignedChecker
  }

  async function getChecker({ userId, manuscript, checkerRole }) {
    const checker = await User.findOneBy({
      queryObject: {
        id: userId,
      },
      eagerLoadRelations: 'teams',
    })

    if (!checker.isConfirmed) {
      throw new Error(`Manuscripts can only be assigned to confirmed checkers.`)
    }
    const team = checker.teams.find((team) => team.id === manuscript.team.id)

    if (team.role !== checkerRole)
      throw new Error(`Manuscripts can only be assigned to ${checkerRole}s.`)

    return { id: checker.id, team_id: team.id }
  }

  async function emitEventWhenCheckerIsAssigned({
    checker,
    manuscript,
    assignationType,
  }) {
    const mappedChecker = userMapper.domainToCheckerEventModel({
      checker,
      manuscript,
      assignationType,
    })

    await applicationEventBus.publishMessage({
      event: `${upperFirst(mappedChecker.role)}Assigned`,
      data: mappedChecker,
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'isAdminOrTeamLead']

module.exports = { initialize, authsomePolicies }
