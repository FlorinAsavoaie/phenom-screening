const logger = require('@pubsweet/logger')
const { isEmpty, upperFirst } = require('lodash')

function initialize({
  models: { Manuscript, User, Team },
  mappers: { userMapper },
}) {
  return {
    execute,
  }

  async function execute({ manuscriptId }) {
    const manuscript = await Manuscript.findOneBy({
      queryObject: {
        id: manuscriptId,
      },
      eagerLoadRelations: '[user, team]',
    })

    const manuscriptIsAlreadyAssigned = manuscript.user && manuscript.team

    if (manuscriptIsAlreadyAssigned) {
      logger.info(
        `Manuscript ${manuscriptId} is already assigned to checker: ${manuscript.user.id} from the team: ${manuscript.team.id}`,
      )
      return
    }

    const previousManuscript = await Manuscript.getPreviousVersion({
      submissionId: manuscript.submissionId,
      flowType: manuscript.flowType,
      eagerLoadRelations: '[user, team]',
    })

    const isPreviousManuscriptVersionInATeamButIsUnassigned =
      !!previousManuscript &&
      !previousManuscript.user &&
      !!previousManuscript.team

    const hasManuscriptAPreviousVersionWithAnAssignedChecker =
      !!previousManuscript && !!previousManuscript.user

    const checkerRole = User.getRoleByManuscriptFlowType(manuscript.flowType)

    if (isPreviousManuscriptVersionInATeamButIsUnassigned) {
      await assignCheckerFromCurrentTeam({
        previousManuscript,
        manuscript,
        checkerRole,
      })
      return
    }

    if (hasManuscriptAPreviousVersionWithAnAssignedChecker) {
      await assignManuscriptToThePreviousChecker({
        previousManuscript,
        manuscript,
      })
      return
    }

    await assignNewChecker({ checkerRole, manuscript })
  }

  async function assignCheckerFromCurrentTeam({
    previousManuscript,
    manuscript,
    checkerRole,
  }) {
    const team = await Team.findOneBy({
      queryObject: { id: previousManuscript.team.id },
      eagerLoadRelations: 'users',
    })

    const haveAConfirmedTeamLead = team.users
      .filter((teamMember) => teamMember.role === User.Roles.TEAM_LEADER)
      .some((teamMember) => teamMember.isConfirmed)

    const checker = team.users
      .filter((teamMember) => teamMember.role === checkerRole)
      .map((teamMember) => ({ team_id: team.id, ...teamMember }))
      .find((teamMember) => teamMember.isConfirmed)

    if (!haveAConfirmedTeamLead && !!checker) {
      await assignNewChecker({ checkerRole, manuscript })
      return
    }

    const assignedChecker = await manuscript.assignChecker(checker)

    await emitEventWhenCheckerIsAssigned({
      checker: assignedChecker,
      manuscript,
      assignationType:
        User.AssignationTypes.automaticAssignmentToANewCheckerFromSameTeam,
    })

    await previousManuscript.deleteRelation('team')
  }

  async function assignManuscriptToThePreviousChecker({
    previousManuscript,
    manuscript,
  }) {
    const newChecker = {
      id: previousManuscript.user.id,
      team_id: previousManuscript.team.id,
    }

    const assignedChecker = await manuscript.assignChecker(newChecker)

    await emitEventWhenCheckerIsAssigned({
      checker: assignedChecker,
      manuscript,
      assignationType:
        User.AssignationTypes.automaticAssignmentToTheSameChecker,
    })

    await previousManuscript.deleteRelation('user')
    await previousManuscript.deleteRelation('team')
  }

  async function assignNewChecker({ checkerRole, manuscript }) {
    const newChecker = await User.getFirstAvailableChecker(
      checkerRole,
      manuscript.journalId,
    )

    if (isEmpty(newChecker)) {
      logger.info(
        `No checker available with role: ${checkerRole} for manuscript: ${manuscript.id}`,
      )
      return
    }
    const assignedChecker = await manuscript.assignChecker(newChecker)

    await emitEventWhenCheckerIsAssigned({
      checker: assignedChecker,
      manuscript,
      assignationType: User.AssignationTypes.automaticAssignmentToANewChecker,
    })
  }

  async function emitEventWhenCheckerIsAssigned({
    checker,
    manuscript,
    assignationType,
  }) {
    const mappedChecker = userMapper.domainToCheckerEventModel({
      checker,
      manuscript,
      assignationType,
    })

    await applicationEventBus.publishMessage({
      event: `${upperFirst(mappedChecker.role)}Assigned`,
      data: mappedChecker,
    })
  }
}

module.exports = {
  initialize,
}
