const { isEmpty } = require('lodash')
const { Promise } = require('bluebird')
const { upperFirst } = require('lodash')

const initialize = ({
  models: { Manuscript, User, Team },
  mappers: { userMapper },
}) => {
  return {
    execute,
  }

  async function execute({ team, teamMembers, journalIds, teamType }) {
    const rolesDictionary = {
      [Team.Type.SCREENING]: {
        manuscriptFlowType: Manuscript.FlowTypes.SCREENING,
        checkerType: User.Roles.SCREENER,
      },
      [Team.Type.QUALITY_CHECKING]: {
        manuscriptFlowType: Manuscript.FlowTypes.QUALITY_CHECK,
        checkerType: User.Roles.QUALITY_CHECKER,
      },
    }

    const unassignedManuscripts = await Manuscript.getUnassignedManuscripts({
      journalIds,
      manuscriptFlowType: rolesDictionary[teamType].manuscriptFlowType,
    })

    if (isEmpty(unassignedManuscripts)) {
      return
    }

    const haveAConfirmedTeamLead = teamMembers
      .filter((teamMember) => teamMember.role === User.Roles.TEAM_LEADER)
      .some(isTeamMemberConfirmed)

    if (!haveAConfirmedTeamLead) {
      return
    }

    let confirmedCheckers = teamMembers
      .filter(isTeamMemberChecker)
      .filter(isTeamMemberConfirmed)

    await Promise.each(unassignedManuscripts, async (manuscript) => {
      const checkerToAssign = getChecker(confirmedCheckers)
      if (!checkerToAssign) {
        return
      }
      const assignedChecker = await manuscript.assignChecker({
        ...checkerToAssign,
        team_id: team.id,
      })

      await emitEventWhenCheckerIsAssigned({
        checker: assignedChecker,
        manuscript,
        assignationType: User.AssignationTypes.automaticAssignmentToANewChecker,
      })

      confirmedCheckers = confirmedCheckers.map((checker) =>
        checker.id === assignedChecker.id
          ? {
              ...checker,
              assignationDate: assignedChecker.teams[0].assignationDate,
            }
          : checker,
      )
    })

    function getChecker(confirmedCheckers) {
      const checkers = confirmedCheckers.sort(compareCheckersByAssignationDate)

      return checkers[0]
    }

    function isTeamMemberConfirmed(teamMember) {
      return !!teamMember.isConfirmed
    }

    function isTeamMemberChecker(teamMember) {
      return teamMember.role === rolesDictionary[teamType].checkerType
    }

    function compareCheckersByAssignationDate(first, second) {
      return first.assignationDate - second.assignationDate
    }

    async function emitEventWhenCheckerIsAssigned({
      checker,
      manuscript,
      assignationType,
    }) {
      const mappedChecker = userMapper.domainToCheckerEventModel({
        checker,
        manuscript,
        assignationType,
      })

      await applicationEventBus.publishMessage({
        event: `${upperFirst(mappedChecker.role)}Assigned`,
        data: mappedChecker,
      })
    }
  }
}

module.exports = {
  initialize,
}
