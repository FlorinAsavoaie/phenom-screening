function initialize({ Note }) {
  async function execute({ manuscriptId }) {
    return Note.findLatestNote({
      queryObject: { manuscriptId, type: Note.Types.PAUSE_REASON_NOTE },
    })
  }

  return { execute }
}

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
