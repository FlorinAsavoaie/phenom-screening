const { Promise } = require('bluebird')
const { last } = require('lodash')

function initialize({
  models: { Manuscript, File },
  s3Service,
  factories: { fileFactory },
  logger,
}) {
  return {
    execute,
  }
  async function execute({ submissionId, manuscripts }) {
    const reviewManuscript = last(manuscripts)

    const screeningManuscript = await Manuscript.getLatestManuscriptVersion({
      queryObject: {
        submissionId,
      },
      eagerLoadRelations: '[files,authors]',
    })

    if (screeningManuscript.status !== Manuscript.Statuses.AUTOMATIC_CHECKS) {
      logger.info(
        `Edits made on manuscript ${screeningManuscript.id} will not be saved while on status ${screeningManuscript.status}.`,
      )
      return
    }

    const mappedFiles = mapFiles(reviewManuscript, screeningManuscript)

    await deleteAllS3Files(screeningManuscript)

    await uploadFilesToS3(mappedFiles, reviewManuscript)

    const savedManuscript = await Manuscript.upsertGraph({
      ...screeningManuscript,
      files: mappedFiles,
      manuscriptChecks: {},
    })

    await emitEventWhenManuscriptIsEdited(savedManuscript)
  }

  function mapFiles(reviewManuscript, screeningManuscript) {
    const dbMainManuscriptFile = screeningManuscript.getManuscriptFile()

    return reviewManuscript.files.map((file) => {
      const providerKey =
        file.type === File.Types.MANUSCRIPT && dbMainManuscriptFile
          ? dbMainManuscriptFile.originalFileProviderKey
          : file.providerKey

      return fileFactory.createFile({
        file: { ...file, providerKey },
      })
    })
  }

  async function deleteAllS3Files(manuscript) {
    const filesFromS3 = await s3Service.listObjects(`${manuscript.phenomId}/`)
    const filesKeysToDeleteFromS3 = filesFromS3.Contents.map((file) => file.Key)

    await s3Service.deleteObjects(filesKeysToDeleteFromS3)
  }

  async function uploadFilesToS3(mappedFiles, reviewManuscript) {
    const reviewMainManuscriptFile = reviewManuscript.files.find(
      (file) => file.type === File.Types.MANUSCRIPT,
    )

    await Promise.each(mappedFiles, async (file) => {
      if (file.type === File.Types.MANUSCRIPT) {
        await s3Service.uploadReviewManuscriptFileToScreeningS3(
          reviewMainManuscriptFile.providerKey,
          file.originalFileProviderKey,
        )
      } else {
        await s3Service.uploadReviewFileToScreeningS3(
          file.originalFileProviderKey,
        )
      }
    })
  }

  async function emitEventWhenManuscriptIsEdited(manuscript) {
    const manuscriptFile = manuscript.getManuscriptFile()

    await applicationEventBus.publishMessage({
      event: 'ManuscriptIngested',
      data: {
        authors: manuscript.authors,
        fileId: manuscriptFile.id,
        manuscriptId: manuscript.id,
        originalFileProviderKey: manuscriptFile.originalFileProviderKey,
      },
    })
  }
}

module.exports = {
  initialize,
}
