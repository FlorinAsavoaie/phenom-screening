const { chain } = require('lodash')

function initialize({ Author }) {
  return {
    execute,
  }

  async function execute(user) {
    const { email: userEmail } = user.identities.find(
      (identity) => identity.type === 'local',
    )
    const orcid = chain(user.identities)
      .find((identity) => identity.type === 'orcid')
      .get('identifier', null)
      .value()

    await Author.updateBy({
      queryObject: { email: userEmail },
      propertiesToUpdate: { orcid },
    })
  }
}

module.exports = {
  initialize,
}
