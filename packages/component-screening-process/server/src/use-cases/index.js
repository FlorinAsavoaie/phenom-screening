const authorVerification = require('./authorVerification')
const infoValidation = require('./infoValidation')
const screeningDecisions = require('./screeningDecisions')
const manuscriptIngestion = require('./manuscriptIngestion')
const assignManuscript = require('./assignManuscript')

const withdrawSubmissionUseCase = require('./withdrawSubmission')
const getSimilarSubmissionsUseCase = require('./getSimilarSubmissions')
const getLatestPauseReasonUseCase = require('./getLatestPauseReason')
const editSubmissionUseCase = require('./editSubmission')
const updateUserOrcidUseCase = require('./updateUserOrcid')

module.exports = {
  ...assignManuscript,
  ...infoValidation,
  ...authorVerification,
  ...screeningDecisions,
  ...manuscriptIngestion,
  editSubmissionUseCase,
  updateUserOrcidUseCase,
  withdrawSubmissionUseCase,
  getLatestPauseReasonUseCase,
  getSimilarSubmissionsUseCase,
}
