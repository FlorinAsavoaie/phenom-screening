function initialize({ EmailTemplate, configService }) {
  return {
    sendNotification,
  }

  async function sendNotification({
    fromUser,
    toUser,
    cc,
    content: { subject, paragraph },
  }) {
    const emailTemplate = new EmailTemplate({
      type: 'system',
      templateType: 'notification',
      fromEmail: `${fromUser.fullName} <${fromUser.email}>`,
      toUser: {
        name: toUser.fullName,
        email: toUser.email,
      },
      cc,
      content: {
        subject,
        signatureName: fromUser.fullName,
        paragraph,
        footerText: configService.get('publisherConfig').emailData
          .footerTextTemplate`${toUser.email}`,
      },
      bodyProps: {
        hasIntro: false,
        hasSignature: true,
      },
    })

    await emailTemplate.sendEmail()
  }
}

module.exports = {
  initialize,
}
