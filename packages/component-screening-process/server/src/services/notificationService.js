const { chain } = require('lodash')

const initialize = ({ services: { emailService, configService } }) => ({
  async notifyThatManuscriptReturnedToDraft({
    manuscriptNotes,
    observations,
    additionalComments,
    correspondingAuthor,
    manuscript,
    currentUser,
    editorialAssistant = {},
  }) {
    const { title, customId } = manuscript

    const observationsDictionary = {
      includesCitations: {
        content: 'Includes Citations',
        type: 'Abstract',
      },
      graphical: { content: 'Graphical', type: 'Abstract' },
      differentAuthors: {
        content: 'Different Authors',
        type: 'Authors & Affiliations',
      },
      missingAffiliations: {
        content: 'Missing Affiliations',
        type: 'Authors & Affiliations',
      },
      nonacademicAffiliations: {
        content: 'Nonacademic Affiliations',
        type: 'Authors & Affiliations',
      },
      missingReferences: {
        content: 'Missing References',
        type: 'Reference Extraction',
      },
      wrongReferences: {
        content: 'Wrong References',
        type: 'Reference Extraction',
      },
      topicNotAppropriate: {
        content: 'Topic Not Appropriate',
        type: 'Relevance Check',
      },
      articleTypeIsNotAppropriate: {
        content: 'Article Type Is Not Appropriate',
        type: 'Relevance Check',
      },
      manuscriptIsMissingSections: {
        content: 'Manuscript Is Missing Sections',
        type: 'Relevance Check',
      },
      '': '',
    }

    const notesTypeDictionary = {
      abstractNote: 'Abstract',
      titleNote: 'Title',
      authorsNote: 'Authors & Affiliations',
      referencesNote: 'Reference Extraction',
      relevanceNote: 'Relevance Check',
      '': '',
    }
    const parseObservation = (observation = '') =>
      observationsDictionary[observation]

    const parseNote = (note = '') => ({
      ...note,
      type: notesTypeDictionary[note.type],
    })

    const groupObservationsByType = (result, observation) => {
      if (!result[observation.type]) {
        result[observation.type] = []
      }
      result[observation.type].push(observation.content)
      return result
    }

    const transformToListObjects = (values) =>
      values
        .map((value) => `<li>${value}</li>`)
        .join()
        .replace(/,/g, '')

    const notes = manuscriptNotes.map(parseNote)
    const parsedObservations = observations.map(parseObservation)

    const screenerObservations = chain(notes)
      .concat(parsedObservations)
      .reduce(groupObservationsByType, {})
      .reduce(
        (acc, values, key) =>
          `${acc}
          <li><strong style = 'margin-bottom: 5px'> ${key}</strong></li><ul style = 'margin-bottom: 5px'>${transformToListObjects(
            values,
          )}
          </ul>`,
        '',
      )
      .value()

    const paragraph = `Dear Dr. ${correspondingAuthor.fullName}, <br/><br/>
      We have reviewed your manuscript "${title}" with ID No. ${customId}${
      screenerObservations
        ? ` and found the following issues which need to be solved before moving on:
          <br/><br/>
          <ul>${screenerObservations}</ul><br/>`
        : '.<br/><br/>'
    }
      ${
        additionalComments &&
        `<strong>Additional Comments:</strong><br/><br/>
           <em style='padding: 0 10px; display: block; text-align: justify;'>
             ${additionalComments}
           </em>
           <br/>
         `
      }
      Please log into your account to make the required updates. Follow the steps outlined below, and we kindly ask that you do not submit a brand new manuscript.<br/>
        <ol>
          <li>Click on the manuscript tile for ${title}, which will show the status “Complete submission”.</li>
          <li>You will see all 4 submission steps. All the information will be pre-filled as you submitted the manuscript, so you will not need to complete the entire submission again. Click through the steps until you reach the area that needs updating.</li>
          <li>If the manuscript file needs updating then go to step 4, and click the small bin icon next to the manuscript file. This will delete the file. You can then upload the new updated file. </li>
          <li>When all necessary changes have been made, click the green “Submit manuscript” button at the bottom of step 4, and this will return the manuscript to the Hindawi team.</li>
        </ol>`

    await emailService.sendNotification({
      fromUser: {
        email: currentUser.email,
        fullName: currentUser.fullName,
      },
      toUser: {
        email: correspondingAuthor.email,
        fullName: correspondingAuthor.fullName,
      },
      content: {
        subject: `${title}: Manuscript returned to draft`,
        paragraph,
      },
      cc: editorialAssistant.email,
    })
  },

  async notifyThatManuscriptWasRefused({
    additionalComments,
    emailRecipient,
    currentUser,
    manuscript,
    observations,
    editorialAssistant = {},
  }) {
    const {
      customId,
      flowType,
      title,
      journal: { name: journalName },
    } = manuscript

    const { observationsDictionary } = configService.get(
      'publisherConfig.emailData.rtc',
    )

    const parseObservations = (observation = '') =>
      observationsDictionary[observation]

    const screenerObservations = observations
      .map(parseObservations)
      .reduce((acc, observation) => `${acc}<li>${observation}</li><br/>`, '')

    const manuscriptTypes = {
      screening: 'Screening',
      qualityCheck: 'Quality Checking',
    }

    const paragraph = `Dear Dr. ${emailRecipient.fullName}, <br/><br/>
      The Editorial ${
        manuscriptTypes[flowType]
      } team have assessed your manuscript "${title}"\
      with ID No. ${customId} and have unfortunately found it\
      unsuitable for publication in "${journalName}"${
      screenerObservations
        ? ` for the following reason${observations.length > 1 ? 's' : ''}:  
      <ul>${screenerObservations}</ul>`
        : '.<br/><br/>'
    }
     ${
       additionalComments &&
       `<strong>Additional Comments:</strong><br/><br/>
          <em style='padding: 0 10px; display: block; text-align: justify;'>
            ${additionalComments}
          </em>
          <br/> 
        `
     }
        Please do let me know if you have any questions.`

    await emailService.sendNotification({
      fromUser: {
        email: currentUser.email,
        fullName: currentUser.fullName,
      },
      toUser: {
        email: emailRecipient.email,
        fullName: emailRecipient.fullName,
      },
      content: {
        subject: `${title}: Manuscript refused to consider`,
        paragraph,
      },
      cc: editorialAssistant.email,
    })
  },

  async notifyEAWhenManuscriptIsAccepted({
    editorialAssistant,
    additionalComments,
    manuscript,
  }) {
    const { customId, title, flowTypeLabel } = manuscript

    const paragraph = `The ${flowTypeLabel} Team has confirmed that the manuscript ${title} with ID No. ${customId} has passed all screening checks.
      <br/><br/>
      ${
        additionalComments &&
        `<strong>Additional Comments:</strong><br/><br/>
           <em style='padding: 0 10px; display: block; text-align: justify;'>
             ${additionalComments}
           </em>
           <br/> 
         `
      }`

    await emailService.sendNotification({
      fromUser: {
        email: configService.get('emailSender'),
        fullName: `${manuscript.flowTypeLabel} Team`,
      },
      toUser: {
        email: editorialAssistant.email,
        fullName: editorialAssistant.fullName,
      },
      content: {
        subject: `[${customId}]: Manuscript accepted`,
        paragraph,
      },
    })
  },
  async notifyTLThatManuscriptWasEscalated({
    teamLeader,
    reason,
    manuscript,
    currentUser,
  }) {
    const {
      customId,
      title,
      journal: { name: journalName },
    } = manuscript
    const paragraph = `Dear Team Leader,  <br/><br/>
    I found an issue on manuscript "${title}"\
    with ID no. ${customId} from ${journalName}.\
     Please see the reason for escalation below:<br/><br/>
     ${reason}
    `
    await emailService.sendNotification({
      fromUser: {
        email: currentUser.email,
        fullName: currentUser.fullName,
      },
      toUser: {
        email: teamLeader.email,
        fullName: teamLeader.fullName,
      },
      content: {
        subject: `${currentUser.fullName} has escalated manuscript ${customId}`,
        paragraph,
      },
    })
  },

  async notifyCheckerThatManuscriptIsUnescalated({
    manuscript,
    currentUser,
    reason,
    checker,
  }) {
    const {
      title,
      customId,
      journal: { name: journalName },
    } = manuscript
    const { fullName } = checker
    const paragraph = `
      Dear ${fullName}, <br/><br/>

      I resolved your issue on manuscript ${title} with ID no. ${customId} from ${journalName}.
      <br/><br/>
      <strong>Additional Comments:</strong> <br/><br/>
      <em style='padding: 0 10px; display: block; text-align: justify;'>
      ${reason}
      </em>
      <br/>
      `

    await emailService.sendNotification({
      fromUser: {
        email: currentUser.email,
        fullName: currentUser.fullName,
      },
      toUser: {
        email: checker.email,
        fullName: checker.fullName,
      },
      content: {
        subject: `${currentUser.fullName} has resolved manuscript ${customId}`,
        paragraph,
      },
    })
  },
})

module.exports = { initialize }
