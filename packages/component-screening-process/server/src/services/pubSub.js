const { pubsubManager } = require('pubsweet-server')

const Events = {
  SUBMISSION_STATUS_AND_STEP_UPDATED: 'submissionStatusAndStepUpdated',
}

const SYSTEM_ID = Symbol('SYSTEM_ID')

async function initialize({ services: { config }, models: { Manuscript } }) {
  const subscriptionsEnabled = config.get(
    'pubsweet-server.apollo.subscriptionsEnabled',
  )

  let pubSub = await getPubSub()

  pubSub.Events = Events

  pubSub.notifyThatSubmissionStatusFor = function notifySubmissionStatusUpdate(
    submissionId,
  ) {
    async function publish(userId) {
      if (!subscriptionsEnabled) {
        return
      }

      const latestManuscript = await Manuscript.getLatestManuscriptVersion({
        queryObject: {
          submissionId,
        },
      })

      pubSub.publish(pubSub.Events.SUBMISSION_STATUS_AND_STEP_UPDATED, {
        data: {
          manuscript: latestManuscript,
        },
        context: {
          userId,
        },
      })
    }

    return {
      wasUpdatedByUser: publish,
      wasUpdatedBySystem: async () => publish(SYSTEM_ID),
    }
  }

  async function getPubSub() {
    if (!subscriptionsEnabled) {
      return {}
    }

    pubSub = await pubsubManager.getPubsub()
  }

  return pubSub
}
module.exports = {
  initialize,
}
