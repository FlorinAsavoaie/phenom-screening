const config = require('config')
const models = require('@pubsweet/models')
const EmailTemplate = require('@pubsweet/component-email-templating')
const { withAuthsome } = require('hindawi-utils')
const { mappers, factories } = require('component-model')
const { withFilter } = require('apollo-server-express')

const useCases = require('./use-cases')
const { s3Service } = require('hindawi-shared/server/src/services')
const {
  pubSubService,
  emailService,
  notificationService: notifications,
} = require('./services')

function getPublishAuthorConfirmedUseCase() {
  return useCases.publishAuthorConfirmedUseCase.initialize({
    models,
    services: {
      applicationEventBus,
    },
  })
}
const notificationService = notifications.initialize({
  services: {
    emailService: emailService.initialize({
      EmailTemplate,
      configService: config,
    }),
    configService: config,
  },
})

const resolvers = {
  Mutation: {
    // Info Validation Tab
    async getScreeningSignedUrl(_, params, ctx) {
      return useCases.getScreeningSignedUrlUseCase
        .initialize({ models, s3Service })
        .execute(params)
    },
    async updateReferences(_, { manuscriptId, references }, { user: userId }) {
      return useCases.updateReferencesUseCase
        .initialize(models)
        .execute({ manuscriptId, references, userId })
    },
    async addNote(_, { manuscriptId, type }, { user: userId }) {
      return useCases.addNoteUseCase
        .initialize({ models, factories })
        .execute({ manuscriptId, type, userId })
    },
    async updateNote(_, { noteId, content }, { user: userId }) {
      return useCases.updateNoteUseCase
        .initialize(models)
        .execute({ noteId, content, userId })
    },
    async removeNote(_, { noteId }, { user: userId }) {
      return useCases.removeNoteUseCase
        .initialize(models)
        .execute({ noteId, userId })
    },
    async toggleValidation(_, { manuscriptId, field }, { user: userId }) {
      return useCases.toggleValidationUseCase
        .initialize(models)
        .execute({ manuscriptId, field, userId })
    },

    // Author Verification Tab
    async verifyAuthor(
      _,
      { manuscriptId, authorId, possibleAuthorId },
      { user: userId },
    ) {
      return useCases.verifyAuthorUseCase
        .initialize(models)
        .execute({ manuscriptId, authorId, possibleAuthorId, userId })
    },
    async unverifyAuthor(
      _,
      { manuscriptId, authorId, possibleAuthorId },
      { user: userId },
    ) {
      return useCases.unverifyAuthorUseCase.initialize(models).execute({
        manuscriptId,
        authorId,
        possibleAuthorId,
        userId,
      })
    },
    async cannotVerifyAuthor(_, { manuscriptId, authorId }, { user: userId }) {
      return useCases.cannotVerifyAuthorUseCase
        .initialize(models)
        .execute({ manuscriptId, authorId, userId })
    },
    async verifyOutOfToolAuthor(
      _,
      { manuscriptId, authorId },
      { user: userId },
    ) {
      return useCases.verifyOutOfToolAuthorUseCase
        .initialize(models)
        .execute({ manuscriptId, authorId, userId })
    },
    async unverifyOutOfToolAuthor(
      _,
      { manuscriptId, authorId },
      { user: userId },
    ) {
      return useCases.unverifyOutOfToolAuthorUseCase
        .initialize(models)
        .execute({ manuscriptId, authorId, userId })
    },

    // Screening Decisions
    async returnManuscriptToDraft(
      _,
      { manuscriptId, content },
      { user: userId },
    ) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.returnManuscriptToDraftUseCase
        .initialize({
          models,
          factories,
          mappers,
          services: {
            notificationService,
            pubSub,
          },
          useCases: {
            publishAuthorConfirmedUseCase: getPublishAuthorConfirmedUseCase(),
          },
        })
        .execute({
          manuscriptId,
          content,
          userId,
        })
    },
    async refuseToConsiderDecision(
      _,
      { manuscriptId, content, note },
      { user: userId },
    ) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.refuseToConsiderDecisionUseCase
        .initialize({
          models,
          mappers,
          factories,
          useCases: {
            publishAuthorConfirmedUseCase: getPublishAuthorConfirmedUseCase(),
          },
          services: {
            notificationService,
            pubSub,
          },
        })
        .execute({ manuscriptId, content, note, userId })
    },
    async approveDecision(_, { manuscriptId, content }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.approveDecisionUseCase
        .initialize({
          models,
          mappers,
          useCases: {
            publishAuthorConfirmedUseCase: getPublishAuthorConfirmedUseCase(),
          },
          services: {
            notificationService,
            pubSub,
          },
        })
        .execute({ manuscriptId, content, userId })
    },
    async pauseManuscript(_, { manuscriptId, reason }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.pauseManuscriptUseCase
        .initialize({ models, services: { pubSub }, mappers, factories })
        .execute({
          manuscriptId,
          reason,
          userId,
        })
    },
    async unpauseManuscript(_, { manuscriptId }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.unpauseManuscriptUseCase
        .initialize({ models, services: { pubSub }, mappers, factories })
        .execute({
          manuscriptId,
          userId,
        })
    },
    async escalateManuscript(_, { manuscriptId, reason }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.escalateManuscriptUseCase
        .initialize({
          models,
          services: { pubSub, notificationService },
          mappers,
          factories,
        })
        .execute({
          manuscriptId,
          userId,
          reason,
        })
    },
    async returnToChecker(_, { manuscriptId, reason }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.returnToCheckerUseCase
        .initialize({
          models,
          services: {
            pubSub,
            notificationService,
          },
          mappers,
          factories,
        })
        .execute({ manuscriptId, userId, reason })
    },
    async voidManuscript(_, { manuscriptId, reason }, { user: userId }) {
      const pubSub = await pubSubService.initialize({
        services: { config },
        models,
      })

      return useCases.voidManuscriptUseCase
        .initialize({ models, services: { pubSub }, mappers })
        .execute({
          manuscriptId,
          reason,
          userId,
        })
    },

    // Assignation
    async reassignManuscriptToChecker(_, params, ctx) {
      return useCases.reassignManuscriptToCheckerUseCase
        .initialize({
          models,
          mappers,
        })
        .execute(params)
    },
  },
  Query: {
    async getSimilarSubmissions(_, params, ctx) {
      return useCases.getSimilarSubmissionsUseCase
        .initialize({ models })
        .execute(params)
    },
    async getLatestPauseReason(_, params, ctx) {
      return useCases.getLatestPauseReasonUseCase
        .initialize(models)
        .execute(params)
    },
  },
  Subscription: {
    submissionStatusAndStepUpdated: {
      // eslint-disable-next-line max-params
      subscribe: async (parent, args, context, info) => {
        const userId = context.user

        const pubSub = await pubSubService.initialize({
          services: { config },
          models,
        })

        const { Manuscript } = models

        return withFilter(
          () =>
            pubSub.asyncIterator(
              pubSub.Events.SUBMISSION_STATUS_AND_STEP_UPDATED,
            ),
          async (payload, variables) => {
            if (payload.context.userId === userId) {
              return false
            }

            const { submissionId } = payload.data.manuscript

            if (variables.submissionIds) {
              if (variables.submissionIds.includes(submissionId)) {
                return Manuscript.canAccessSubmission({
                  submissionId,
                  userId,
                })
              }
              return false
            }

            return Manuscript.canAccessSubmission({
              submissionId,
              userId,
            })
          },
        )(parent, args, context, info)
      },
      resolve: (payload) => payload.data.manuscript,
    },
  },
}

module.exports = withAuthsome(resolvers, useCases)
