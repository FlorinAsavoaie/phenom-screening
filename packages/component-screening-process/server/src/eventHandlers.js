const { transaction } = require('objection')
const useCases = require('./use-cases')
const models = require('@pubsweet/models')
const { factories } = require('component-model')
const {
  useCases: manuscriptAuthorsUseCases,
} = require('component-model/src/manuscriptAuthors')
const { s3Service } = require('hindawi-shared/server/src/services')
const { getParsedTransactionModels } = require('hindawi-utils/utils')
const pubSubService = require('./services/pubSub')
const { mappers } = require('component-model')

const config = require('config')
const logger = require('@pubsweet/logger')

function assignManuscriptToChecker() {
  return useCases.assignManuscriptToCheckerUseCase.initialize({
    models,
    mappers,
  })
}

function getInitializeIngestManuscriptAuthorsEmailsUseCase() {
  return manuscriptAuthorsUseCases.ingestManuscriptAuthorsEmailsUseCase.initialize(
    {
      models,
      factories,
    },
  )
}

function getInitializeDeleteManuscriptAuthorsEmailsUseCase() {
  return manuscriptAuthorsUseCases.deleteManuscriptAuthorsEmailsUseCase.initialize(
    {
      models,
      factories,
    },
  )
}

module.exports = {
  /**
   *
   * EditorialModel: Screening Activity Properties + Global Properties
   *
   * TriggeredScreening is a kitchen sink event
   * TriggeredScreening extends Submission Events { editorialModel: EditorialModel }
   *
   * data: {
   *  manuscripts: Manuscript[],
   *  submissionId: string,
   *  editorialModel: EditorialModel
   * }
   */

  async TriggeredScreening(data) {
    return useCases.ingestManuscriptUseCase
      .initialize({
        logger,
        models,
        factories,
        s3Service,
        useCases: {
          ingestManuscriptAuthorsEmailsUseCase:
            getInitializeIngestManuscriptAuthorsEmailsUseCase(),
          deleteManuscriptAuthorsEmailsUseCase:
            getInitializeDeleteManuscriptAuthorsEmailsUseCase(),
        },
      })
      .execute(data)
  },
  async SubmissionWithdrawn({ submissionId }) {
    const pubSub = await pubSubService.initialize({
      services: { config },
      models,
    })

    await useCases.withdrawSubmissionUseCase
      .initialize({
        models,
        services: {
          pubSub,
        },
      })
      .execute(submissionId)
  },
  async SubmissionEdited(data) {
    return useCases.editSubmissionUseCase
      .initialize({ models, s3Service, factories, logger })
      .execute(data)
  },

  // Screening Internal Events
  async FileConverted({ fileId, convertedFileProviderKey }) {
    return useCases.persistConvertedFileUseCase
      .initialize({ models })
      .execute({ fileId, convertedFileProviderKey })
  },
  async LanguageChecked(data) {
    return useCases.persistEnglishPercentageUseCase
      .initialize(models)
      .execute(data)
  },
  async SuspiciousWordsProcessingDone(data) {
    return useCases.persistSuspiciousWordsUseCase
      .initialize(models)
      .execute(data)
  },
  async SimilarityCheckFinished(data) {
    return useCases.persistSimilarityPercentageUseCase
      .initialize(models)
      .execute(data)
  },
  async AuthorProfilesSearchFinished(data) {
    return useCases.updateAuthorsUseCase.initialize(models).execute(data)
  },
  async SanctionListCheckDone(data) {
    const { Author, ManuscriptChecks } = models

    return transaction(Author, ManuscriptChecks, (...models) => {
      const transactionModels = getParsedTransactionModels(models)

      return useCases.persistAuthorsSanctionListUseCase
        .initialize(transactionModels)
        .execute(data)
    })
  },
  async AutomaticChecksDone(data) {
    return useCases.updateManuscriptStatusAfterChecksDoneUseCase
      .initialize({
        models,
        mappers,
        useCases: { assignManuscriptToChecker: assignManuscriptToChecker() },
      })
      .execute(data)
  },

  // User Events
  async UserORCIDAdded(user) {
    return useCases.updateUserOrcidUseCase.initialize(models).execute(user)
  },
  async UserORCIDRemoved(data) {
    return useCases.updateUserOrcidUseCase.initialize(models).execute(data)
  },
}
