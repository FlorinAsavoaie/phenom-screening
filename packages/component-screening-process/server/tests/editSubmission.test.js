process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const models = require('@pubsweet/models')
const useCases = require('../src/use-cases')

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}
const chance = new Chance()

const { Manuscript, File } = models

const logger = {
  info: jest.fn(),
}
const mockSaveFile = jest.fn()
const factories = {
  fileFactory: {
    createFile: jest.fn().mockReturnValue({
      id: chance.guid(),
      save: mockSaveFile,
    }),
  },
}

const s3Service = {
  uploadReviewManuscriptFileToScreeningS3: jest.fn(async () => {}),
  uploadReviewFileToScreeningS3: jest.fn(async () => {}),
  listObjects: jest.fn(async () => {}),
  deleteObjects: jest.fn(async () => {}),
}

describe('Edit Submission Use Case', () => {
  const submissionId = chance.guid()

  const manuscriptFileScreening = {
    id: chance.guid(),
    type: File.Types.MANUSCRIPT,
    originalFileProviderKey: chance.guid(),
  }

  const coverLetterFileScreening = {
    id: chance.guid(),
    type: File.Types.COVER_LETTER,
    originalFileProviderKey: chance.guid(),
  }

  const supplementaryFileScreening = {
    id: chance.guid(),
    type: File.Types.SUPPLEMENTARY,
    originalFileProviderKey: chance.guid(),
  }

  const screeningManuscript = {
    id: chance.guid(),
    status: Manuscript.Statuses.AUTOMATIC_CHECKS,
    getManuscriptFile: jest.fn().mockReturnValue(manuscriptFileScreening),
    files: [
      manuscriptFileScreening,
      coverLetterFileScreening,
      supplementaryFileScreening,
    ],
  }
  Manuscript.find = jest.fn().mockResolvedValue(screeningManuscript)
  Manuscript.upsertGraph = jest.fn().mockResolvedValue(screeningManuscript)

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should successfully edit main manuscript files', async () => {
    const manuscriptFileReview = {
      id: chance.guid(),
      type: File.Types.MANUSCRIPT,
      providerKey: chance.guid(),
    }

    const data = {
      submissionId,
      manuscripts: [
        {
          files: [manuscriptFileReview],
        },
      ],
    }
    Manuscript.getLatestManuscriptVersion = jest
      .fn()
      .mockResolvedValue(screeningManuscript)

    s3Service.listObjects = jest.fn().mockResolvedValue({
      Contents: [{ Key: manuscriptFileScreening.originalFileProviderKey }],
    })
    jest.spyOn(factories.fileFactory, 'createFile').mockReturnValueOnce({
      ...manuscriptFileReview,
      providerKey: manuscriptFileScreening.originalFileProviderKey,
      originalFileProviderKey: manuscriptFileScreening.originalFileProviderKey,
    })

    await useCases.editSubmissionUseCase
      .initialize({
        models,
        s3Service,
        factories,
        logger,
      })
      .execute(data)

    expect(
      s3Service.uploadReviewManuscriptFileToScreeningS3,
    ).toHaveBeenCalledTimes(1)
    expect(
      s3Service.uploadReviewManuscriptFileToScreeningS3,
    ).toHaveBeenCalledWith(
      manuscriptFileReview.providerKey,
      manuscriptFileScreening.originalFileProviderKey,
    )

    expect(factories.fileFactory.createFile).toHaveBeenCalledTimes(1)
    expect(factories.fileFactory.createFile).toHaveBeenCalledWith({
      file: {
        ...manuscriptFileReview,
        providerKey: manuscriptFileScreening.originalFileProviderKey,
      },
    })
  })

  it('should successfully edit cover letter manuscript files', async () => {
    const manuscriptCoverLetterFileReview = {
      id: chance.guid(),
      type: File.Types.COVER_LETTER,
      providerKey: chance.guid(),
    }

    const data = {
      submissionId,
      manuscripts: [
        {
          files: [manuscriptCoverLetterFileReview],
        },
      ],
    }
    Manuscript.getLatestManuscriptVersion = jest
      .fn()
      .mockResolvedValue(screeningManuscript)

    jest.spyOn(factories.fileFactory, 'createFile').mockReturnValueOnce({
      ...manuscriptCoverLetterFileReview,
      originalFileProviderKey: manuscriptCoverLetterFileReview.providerKey,
    })

    s3Service.listObjects = jest.fn().mockResolvedValue({
      Contents: [{ Key: coverLetterFileScreening.originalFileProviderKey }],
    })
    await useCases.editSubmissionUseCase
      .initialize({
        models,
        s3Service,
        factories,
        logger,
      })
      .execute(data)

    expect(factories.fileFactory.createFile).toHaveBeenCalledTimes(1)
    expect(factories.fileFactory.createFile).toHaveBeenCalledWith({
      file: manuscriptCoverLetterFileReview,
    })

    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenCalledTimes(1)
    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenCalledWith(
      manuscriptCoverLetterFileReview.providerKey,
    )
  })

  it('should successfully edit supplementary manuscript files', async () => {
    const manuscriptSupplementaryFileReview1 = {
      id: chance.guid(),
      type: File.Types.SUPPLEMENTARY,
      providerKey: chance.guid(),
    }
    const manuscriptSupplementaryFileReview2 = {
      id: chance.guid(),
      type: File.Types.SUPPLEMENTARY,
      providerKey: chance.guid(),
    }

    const data = {
      submissionId,
      manuscripts: [
        {
          files: [
            manuscriptSupplementaryFileReview1,
            manuscriptSupplementaryFileReview2,
          ],
        },
      ],
    }
    Manuscript.getLatestManuscriptVersion = jest
      .fn()
      .mockResolvedValue(screeningManuscript)

    jest.spyOn(factories.fileFactory, 'createFile').mockReturnValueOnce({
      ...manuscriptSupplementaryFileReview1,
      originalFileProviderKey: manuscriptSupplementaryFileReview1.providerKey,
    })

    jest.spyOn(factories.fileFactory, 'createFile').mockReturnValueOnce({
      ...manuscriptSupplementaryFileReview1,
      originalFileProviderKey: manuscriptSupplementaryFileReview2.providerKey,
    })

    s3Service.listObjects = jest.fn().mockResolvedValue({
      Contents: [{ Key: supplementaryFileScreening.originalFileProviderKey }],
    })
    await useCases.editSubmissionUseCase
      .initialize({
        models,
        s3Service,
        factories,
        logger,
      })
      .execute(data)

    expect(factories.fileFactory.createFile).toHaveBeenCalledTimes(2)
    expect(factories.fileFactory.createFile).toHaveBeenNthCalledWith(1, {
      file: manuscriptSupplementaryFileReview1,
    })
    expect(factories.fileFactory.createFile).toHaveBeenNthCalledWith(2, {
      file: manuscriptSupplementaryFileReview2,
    })

    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenCalledTimes(2)
  })

  it('should successfully delete no needed files', async () => {
    const manuscriptFileReview = {
      id: chance.guid(),
      type: File.Types.MANUSCRIPT,
      providerKey: chance.guid(),
    }

    const savedFile = {
      ...manuscriptFileReview,
      originalFileProviderKey: manuscriptFileScreening.originalFileProviderKey,
    }

    const savedManuscript = {
      files: [savedFile],
      getManuscriptFile: jest.fn().mockReturnValue(savedFile),
    }
    Manuscript.upsertGraph = jest.fn().mockResolvedValue(savedManuscript)
    s3Service.listObjects = jest.fn().mockResolvedValue({
      Contents: [
        { Key: manuscriptFileScreening.originalFileProviderKey },
        { Key: coverLetterFileScreening.originalFileProviderKey },
        { Key: supplementaryFileScreening.originalFileProviderKey },
      ],
    })

    const data = {
      submissionId,
      manuscripts: [
        {
          files: [manuscriptFileReview],
        },
      ],
    }
    Manuscript.getLatestManuscriptVersion = jest
      .fn()
      .mockResolvedValue(screeningManuscript)

    await useCases.editSubmissionUseCase
      .initialize({
        models,
        s3Service,
        factories,
        logger,
      })
      .execute(data)

    expect(s3Service.deleteObjects).toHaveBeenCalledTimes(1)
    expect(s3Service.deleteObjects).toHaveBeenCalledWith([
      manuscriptFileScreening.originalFileProviderKey,
      coverLetterFileScreening.originalFileProviderKey,
      supplementaryFileScreening.originalFileProviderKey,
    ])
  })

  it('should publish event "ManuscriptIngested"', async () => {
    const manuscriptFileReview = {
      id: chance.guid(),
      type: File.Types.MANUSCRIPT,
      providerKey: chance.guid(),
    }

    const savedFile = {
      ...manuscriptFileReview,
      originalFileProviderKey: manuscriptFileScreening.originalFileProviderKey,
    }

    const savedManuscript = {
      id: chance.guid(),
      files: [savedFile],
      getManuscriptFile: jest.fn().mockReturnValue(savedFile),
      authors: [],
    }

    Manuscript.upsertGraph = jest.fn().mockResolvedValue(savedManuscript)

    const data = {
      submissionId,
      manuscripts: [
        {
          files: [manuscriptFileReview],
        },
      ],
    }
    Manuscript.getLatestManuscriptVersion = jest
      .fn()
      .mockResolvedValue(screeningManuscript)

    await useCases.editSubmissionUseCase
      .initialize({
        models,
        s3Service,
        factories,
        logger,
      })
      .execute(data)

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'ManuscriptIngested',
      data: {
        authors: savedManuscript.authors,
        fileId: savedFile.id,
        manuscriptId: savedManuscript.id,
        originalFileProviderKey: savedFile.originalFileProviderKey,
      },
    })
  })

  it('should not save files while status is not automaticChecks', async () => {
    const manuscriptFileReview = {
      id: chance.guid(),
      type: File.Types.MANUSCRIPT,
      providerKey: chance.guid(),
    }
    screeningManuscript.status = Manuscript.Statuses.IN_PROGRESS

    const data = {
      submissionId,
      manuscripts: [
        {
          files: [manuscriptFileReview],
        },
      ],
    }
    Manuscript.getLatestManuscriptVersion = jest
      .fn()
      .mockResolvedValue(screeningManuscript)

    await useCases.editSubmissionUseCase
      .initialize({
        models,
        s3Service,
        factories,
        logger,
      })
      .execute(data)

    expect(logger.info).toHaveBeenCalledTimes(1)
    expect(
      s3Service.uploadReviewManuscriptFileToScreeningS3,
    ).toHaveBeenCalledTimes(0)
    expect(factories.fileFactory.createFile).toHaveBeenCalledTimes(0)
  })
})
