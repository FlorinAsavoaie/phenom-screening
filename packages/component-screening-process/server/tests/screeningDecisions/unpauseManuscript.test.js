process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { Manuscript, Note, User, AuditLog } = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const journalId = chance.guid()
const submissionId = chance.guid()
const manuscriptId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const factories = {
  auditLogFactory: {
    createAuditLog: jest
      .fn()
      .mockReturnValue({ action: AuditLog.Actions.UNPAUSE_MANUSCRIPT }),
  },
}

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

describe('Unpause manuscript Use Case', () => {
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is not in "paused" status', async () => {
    const manuscriptId = chance.guid()

    const manuscriptMock = {
      status: Manuscript.Statuses.SCREENING_APPROVED,
      submissionId,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.unpauseManuscriptUseCase
      .initialize({
        models: { Manuscript, Note, User, AuditLog },
        services: { pubSub },
        mappers,
        factories,
      })
      .execute({ manuscriptId, userId })

    await expect(result).rejects.toThrow(
      `You cannot unpause manuscript ${manuscriptId} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change status of the manuscript in "inProgress"', async () => {
    const manuscriptMock = {
      status: Manuscript.Statuses.PAUSED,
      submissionId,
      version: '2',
      auditLogs: [],
    }

    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    const upsertGraph = jest
      .spyOn(Manuscript, 'upsertGraph')
      .mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await useCases.unpauseManuscriptUseCase
      .initialize({
        models: { Manuscript, Note, User, AuditLog },
        services: { pubSub },
        mappers,
        factories,
      })
      .execute({ manuscriptId, userId })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, files, members, auditLogs]',
    )
    expect(upsertGraph).toHaveBeenCalledTimes(1)
    expect(upsertGraph).toHaveBeenCalledWith(
      expect.objectContaining({
        status: Manuscript.Statuses.IN_PROGRESS,
      }),
    )
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const manuscriptMock = {
      status: Manuscript.Statuses.PAUSED,
      submissionId,
      auditLogs: [],
    }

    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await useCases.unpauseManuscriptUseCase
      .initialize({
        models: { Manuscript, Note, User, AuditLog },
        services: { pubSub },
        mappers,
        factories,
      })
      .execute({ manuscriptId, userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })

  it('should publish "SubmissionScreeningUnpaused" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: Manuscript.FlowTypes.SCREENING,
      status: Manuscript.Statuses.PAUSED,
      submissionId,
      auditLogs: [],
    }

    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      submissionId,
      flowType: Manuscript.FlowTypes.SCREENING,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.unpauseManuscriptUseCase
      .initialize({
        models: { Manuscript, Note, User, AuditLog },
        services: { pubSub },
        mappers,
        factories,
      })
      .execute({ manuscriptId, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningUnpaused',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "SubmissionQualityCheckingUnpaused" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      status: Manuscript.Statuses.PAUSED,
      submissionId,
      auditLogs: [],
    }
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      submissionId,
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.unpauseManuscriptUseCase
      .initialize({
        models: { Manuscript, Note, User, AuditLog },
        services: { pubSub },
        mappers,
        factories,
      })
      .execute({ manuscriptId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionQualityCheckingUnpaused',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })
})
