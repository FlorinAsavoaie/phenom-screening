process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const useCases = require('../../src/use-cases')
const Chance = require('chance')

const models = require('@pubsweet/models')

const chance = new Chance()
const reason = chance.sentence()
const manuscriptId = chance.guid()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const { User, AuditLog } = models

const notificationService = {
  notifyCheckerThatManuscriptIsUnescalated: jest.fn(),
  emailService: jest.fn(),
}

const factories = {
  auditLogFactory: {
    createAuditLog: jest
      .fn()
      .mockReturnValue({ action: AuditLog.Actions.RETURN_TO_CHECKER }),
  },
}

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

describe('Return to checker manuscript Use Case', () => {
  let Manuscript
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript
    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      notes: [],
      auditLogs: [],
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    User.findAssignedChecker = jest.fn().mockReturnValue({})

    const result = useCases.returnToCheckerUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason: chance.paragraph(), userId })

    await expect(result).rejects.toThrow(
      `You cannot return to checker(de-escalate) manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change status of the manuscript in "in progress"', async () => {
    const mockManuscript = {
      id: manuscriptId,
      status: Manuscript.Statuses.ESCALATED,
      submissionId,
      notes: [],
      auditLogs: [],
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    const upsertGraph = jest
      .spyOn(Manuscript, 'upsertGraph')
      .mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)
    User.findAssignedChecker = jest.fn().mockReturnValue({})

    await useCases.returnToCheckerUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason: chance.paragraph(), userId })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, files, members, journal, notes, auditLogs]',
    )

    expect(upsertGraph).toHaveBeenCalledTimes(1)
    expect(upsertGraph).toHaveBeenCalledWith(
      expect.objectContaining({
        status: Manuscript.Statuses.IN_PROGRESS,
      }),
    )
    expect(
      notificationService.notifyCheckerThatManuscriptIsUnescalated,
    ).toHaveBeenCalledTimes(1)
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      status: Manuscript.Statuses.ESCALATED,
      submissionId,
      notes: [],
      auditLogs: [],
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)
    User.findAssignedChecker = jest.fn().mockReturnValue({})

    await useCases.returnToCheckerUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })

  it('should publish "SubmissionScreeningReturnedToChecker" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: Manuscript.FlowTypes.SCREENING,
      status: Manuscript.Statuses.ESCALATED,
      submissionId,
      notes: [],
      auditLogs: [],
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      submissionId,
      flowType: Manuscript.FlowTypes.SCREENING,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)
    User.findAssignedChecker = jest.fn().mockReturnValue({})

    await useCases.returnToCheckerUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningReturnedToChecker',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "SubmissionQualityCheckingReturnedToChecker" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      status: Manuscript.Statuses.ESCALATED,
      submissionId,
      notes: [],
      auditLogs: [],
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      submissionId,
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)
    User.findAssignedChecker = jest.fn().mockReturnValue({})

    await useCases.returnToCheckerUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionQualityCheckingReturnedToChecker',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })
})
