process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { Note } = models

const mockSaveGraph = jest.fn()
const manuscriptId = chance.guid()
const reason = chance.sentence()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

models.Note = jest.fn().mockImplementation(() => ({
  content: reason,
}))

models.Note.Types = jest
  .fn()
  .mockReturnValue({ VOID_REASON_NOTE: Note.Types.VOID_REASON_NOTE })

describe('Void manuscript Use Case', () => {
  let Manuscript
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript
    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      saveGraph: mockSaveGraph,
      status: Manuscript.Statuses.PAUSED,
      flowType: Manuscript.FlowTypes.SCREENING,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.voidManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers })
      .execute({ manuscriptId, reason, userId })

    await expect(result).rejects.toThrow(
      `You cannot void manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })
  it('should throw error if manuscript is in quality check', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      saveGraph: mockSaveGraph,
      status: Manuscript.Statuses.IN_PROGRESS,
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.voidManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers })
      .execute({ manuscriptId, reason, userId })

    await expect(result).rejects.toThrow(
      `You cannot void manuscript ${manuscriptMock.id} while on ${manuscriptMock.flowType} flow.`,
    )
  })

  it('should change status of the manuscript in "void"', async () => {
    const mockManuscript = {
      id: manuscriptId,
      saveGraph: mockSaveGraph,
      status: Manuscript.Statuses.IN_PROGRESS,
      flowType: Manuscript.FlowTypes.SCREENING,
      submissionId,
    }

    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.voidManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers })
      .execute({ manuscriptId, reason, userId })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, files, members]',
    )

    expect(mockManuscript.status).toEqual(Manuscript.Statuses.VOID)
    expect(mockSaveGraph).toHaveBeenCalled()
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      saveGraph: mockSaveGraph,
      status: Manuscript.Statuses.IN_PROGRESS,
      flowType: Manuscript.FlowTypes.SCREENING,
      submissionId,
    }

    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.voidManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers })
      .execute({ manuscriptId, reason, userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })

  it('should publish "SubmissionScreeningVoid" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      saveGraph: mockSaveGraph,
      status: Manuscript.Statuses.IN_PROGRESS,
      flowType: Manuscript.FlowTypes.SCREENING,
      submissionId,
    }

    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.voidManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers })
      .execute({ manuscriptId, reason, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningVoid',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })
})
