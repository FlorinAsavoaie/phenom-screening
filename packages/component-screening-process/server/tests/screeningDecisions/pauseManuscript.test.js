process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { Note, AuditLog, User } = models

const mockSave = jest.fn()
const manuscriptId = chance.guid()
const reason = chance.sentence()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const factories = {
  auditLogFactory: {
    createAuditLog: jest
      .fn()
      .mockReturnValue({ action: AuditLog.Actions.PAUSE_MANUSCRIPT }),
  },
}

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

models.Note = jest.fn().mockImplementation(() => ({
  content: reason,
  save: mockSave,
}))

models.Note.Types = jest
  .fn()
  .mockReturnValue({ DECISION_NOTE: Note.Types.DECISION_NOTE })

describe('Pause manuscript Use Case', () => {
  let Manuscript
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript
    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.PAUSED,
      isPending: jest.fn().mockReturnValue(true),
      notes: [],
      auditLogs: [],
    }
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce(User.Roles.SCREENER)
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.pauseManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers, factories })
      .execute({ manuscriptId, reason, userId })

    await expect(result).rejects.toThrow(
      `You cannot pause manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change status of the manuscript in "paused"', async () => {
    const mockManuscript = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      isPending: jest.fn().mockReturnValue(false),
      submissionId,
      notes: [],
      auditLogs: [],
    }

    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce(User.Roles.SCREENER)
    const upsertGraph = jest
      .spyOn(Manuscript, 'upsertGraph')
      .mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.pauseManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers, factories })
      .execute({ manuscriptId, reason, userId })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, files, members, notes, auditLogs]',
    )

    expect(upsertGraph).toHaveBeenCalledTimes(1)
    expect(upsertGraph).toHaveBeenCalledWith(
      expect.objectContaining({
        status: Manuscript.Statuses.PAUSED,
      }),
    )
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      isPending: jest.fn().mockReturnValue(false),
      submissionId,
      notes: [],
      auditLogs: [],
    }

    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce(User.Roles.SCREENER)
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.pauseManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers, factories })
      .execute({ manuscriptId, reason, userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })

  it('should publish "SubmissionScreeningPaused" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: 'screening',
      status: Manuscript.Statuses.IN_PROGRESS,
      isPending: jest.fn().mockReturnValue(false),
      submissionId,
      notes: [],
      auditLogs: [],
    }

    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce(User.Roles.SCREENER)
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      flowType: Manuscript.FlowTypes.SCREENING,
      submissionId,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.pauseManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers, factories })
      .execute({ manuscriptId, reason, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningPaused',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "SubmissionQualityCheckingPaused" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: 'qualityCheck',
      status: Manuscript.Statuses.IN_PROGRESS,
      isPending: jest.fn().mockReturnValue(false),
      submissionId,
      notes: [],
      auditLogs: [],
    }

    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce(User.Roles.SCREENER)
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      submissionId,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.pauseManuscriptUseCase
      .initialize({ models, services: { pubSub }, mappers, factories })
      .execute({ manuscriptId, reason, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionQualityCheckingPaused',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })
})
