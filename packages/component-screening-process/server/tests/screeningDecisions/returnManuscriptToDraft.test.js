const Chance = require('chance')
const { Manuscript, Note, Author, User, Member } = require('@pubsweet/models')
const useCases = require('../../src/use-cases')

const chance = new Chance()

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

const notificationService = {
  notifyThatManuscriptReturnedToDraft: jest.fn(),
}

const manuscriptId = chance.guid()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}
const mockSaveNote = jest.fn()

const factories = {
  noteFactory: {
    createDecisionNote: jest.fn(() => ({
      save: mockSaveNote,
    })),
  },
}

const screeningUseCases = {
  publishAuthorConfirmedUseCase: {
    execute: jest.fn(),
  },
}

const mockUpdateProperties = jest.fn()
const mockSave = jest.fn()

describe('Return manuscript to Draft Use Case', () => {
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
    jest.spyOn(Author, 'getCorrespondingAuthor').mockResolvedValueOnce({})
    jest.spyOn(User, 'find').mockResolvedValueOnce({})
    jest.spyOn(Member, 'getEditorialAssistant').mockResolvedValueOnce({})
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.PAUSED,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.returnManuscriptToDraftUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    await expect(result).rejects.toThrow(
      `You cannot return manuscript ${manuscriptMock.id} to draft while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change status of the manuscript in "returnedToDraft"', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.returnManuscriptToDraftUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,

        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(mockUpdateProperties).toHaveBeenCalledWith({
      status: Manuscript.Statuses.RETURNED_TO_DRAFT,
    })
    expect(mockSave).toHaveBeenCalled()
    expect(mockSaveNote).toHaveBeenCalled()
  })

  it('should notify Author', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.returnManuscriptToDraftUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(
      notificationService.notifyThatManuscriptReturnedToDraft,
    ).toHaveBeenCalledTimes(1)
  })

  it('should publish "SubmissionScreeningReturnedToDraft" event', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.returnManuscriptToDraftUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph() })

    expect(global.applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
    expect(global.applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningReturnedToDraft',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    Note.findValidationNotes = jest.fn().mockReturnValue([])

    await useCases.returnManuscriptToDraftUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })
})
