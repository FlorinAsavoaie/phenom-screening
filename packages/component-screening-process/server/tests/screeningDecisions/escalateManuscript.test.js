process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { Note, User, AuditLog } = models

const manuscriptId = chance.guid()
const reason = chance.sentence()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const factories = {
  auditLogFactory: {
    createAuditLog: jest
      .fn()
      .mockReturnValue({ action: AuditLog.Actions.SENT_TO_TEAM_LEAD }),
  },
}

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}
const notificationService = {
  notifyTLThatManuscriptWasEscalated: jest.fn(),
}

const note = {
  content: reason,
}
models.Note = jest.fn().mockImplementation(() => note)

models.Note.Types = jest.fn().mockReturnValue({
  ESCALATION_REASON_NOTE: Note.Types.ESCALATION_REASON_NOTE,
})

describe('Escalate manuscript Use Case', () => {
  let Manuscript
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript
    wasUpdatedByUser = jest.fn()

    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.ESCALATED,
      team: { users: [] },
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.escalateManuscriptUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    await expect(result).rejects.toThrow(
      `You cannot escalate manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change status of the manuscript in "escalated"', async () => {
    const mockManuscript = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      team: { users: [] },
      notes: [],
      auditLogs: [],
    }

    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    const upsertGraph = jest
      .spyOn(Manuscript, 'upsertGraph')
      .mockReturnValue({ team: { users: [] } })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.escalateManuscriptUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, files, members, team.users, journal, auditLogs, notes]',
    )
    expect(upsertGraph).toHaveBeenCalledTimes(1)
    expect(upsertGraph).toHaveBeenCalledWith(
      expect.objectContaining({
        status: Manuscript.Statuses.ESCALATED,
      }),
    )
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      team: { users: [] },
      notes: [],
      auditLogs: [],
    }

    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest
      .spyOn(Manuscript, 'upsertGraph')
      .mockReturnValue({ team: { users: [] } })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.escalateManuscriptUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })

  it('should publish "SubmissionScreeningEscalated" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: 'screening',
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      team: { users: [] },
      notes: [],
      auditLogs: [],
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      team: { users: [] },
      flowType: Manuscript.FlowTypes.SCREENING,
      submissionId,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.escalateManuscriptUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningEscalated',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "SubmissionQualityCheckingEscalated" event', async () => {
    const mockManuscript = {
      id: manuscriptId,
      flowType: 'qualityCheck',
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      team: { users: [] },
      notes: [],
      auditLogs: [],
    }
    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({
      team: { users: [] },
      flowType: Manuscript.FlowTypes.QUALITY_CHECK,
      submissionId,
    })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.escalateManuscriptUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionQualityCheckingEscalated',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })
  it('should send a notification to all confirmed team leaders', async () => {
    const team = {
      users: [
        {
          role: User.Roles.TEAM_LEADER,
          isConfirmed: false,
        },
        {
          role: User.Roles.TEAM_LEADER,
          isConfirmed: true,
        },
        {
          role: User.Roles.TEAM_LEADER,
          isConfirmed: true,
        },
        {
          role: User.Roles.SCREENER,
          isConfirmed: true,
        },
      ],
    }
    const mockManuscript = {
      id: manuscriptId,
      flowType: 'screening',
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      team,
      notes: [],
      auditLogs: [],
    }

    jest.spyOn(User, 'find').mockReturnValue({})
    jest
      .spyOn(User, 'getUserRoleOnManuscript')
      .mockReturnValueOnce('teamLeader')
    jest.spyOn(Manuscript, 'upsertGraph').mockReturnValue({ team })
    Manuscript.find = jest.fn().mockReturnValue(mockManuscript)

    await useCases.escalateManuscriptUseCase
      .initialize({
        models,
        services: { pubSub, notificationService },
        mappers,
        factories,
      })
      .execute({ manuscriptId, reason, userId })

    expect(
      notificationService.notifyTLThatManuscriptWasEscalated,
    ).toHaveBeenCalledTimes(2)
  })
})
