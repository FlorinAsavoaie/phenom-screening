const Chance = require('chance')
const { Manuscript, Note, Author, User, Member } = require('@pubsweet/models')
const useCases = require('../../src/use-cases')

const chance = new Chance()

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

const notificationService = {
  notifyThatManuscriptWasRefused: jest.fn(),
}

const manuscriptId = chance.guid()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

const mockUpdateProperties = jest.fn()
const mockSave = jest.fn()

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const screeningUseCases = {
  publishAuthorConfirmedUseCase: {
    execute: jest.fn(),
  },
}

const mockSaveNote = jest.fn()
const factories = {
  noteFactory: {
    createDecisionNote: jest.fn(() => ({
      save: mockSaveNote,
    })),
  },
}

describe('Refuse manuscript Use Case', () => {
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
    jest.spyOn(Author, 'getCorrespondingAuthor').mockResolvedValueOnce({})
    jest.spyOn(User, 'find').mockResolvedValueOnce({})
    jest.spyOn(Member, 'getEditorialAssistant').mockResolvedValueOnce({})
    jest.spyOn(User, 'canDoActionAccordingToStatus').mockReturnValue(true)
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      status: Manuscript.Statuses.PAUSED,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.refuseToConsiderDecisionUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    await expect(result).rejects.toThrow(
      `You cannot refuse to consider manuscript ${manuscriptId} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change status of the manuscript in "rejected"', async () => {
    const manuscriptMock = {
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    const manuscriptId = chance.guid()

    await useCases.refuseToConsiderDecisionUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, journal]',
    )

    expect(mockSaveNote).toHaveBeenCalled()

    expect(mockUpdateProperties).toHaveBeenCalledWith({
      status: Manuscript.Statuses.SCREENING_REJECTED,
    })
    expect(mockSave).toHaveBeenCalled()
  })

  it('should publish "AuthorConfirmed" event', async () => {
    const manuscriptId = chance.guid()

    const manuscriptMock = {
      id: manuscriptId,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    const publishAuthorConfirmedUseCase = jest
      .spyOn(screeningUseCases.publishAuthorConfirmedUseCase, 'execute')
      .mockReturnValue()

    await useCases.refuseToConsiderDecisionUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })
    expect(publishAuthorConfirmedUseCase).toHaveBeenCalledTimes(1)
    expect(publishAuthorConfirmedUseCase).toHaveBeenCalledWith(manuscriptId)
  })

  it('should notify EA', async () => {
    const manuscriptMock = {
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    const manuscriptId = chance.guid()

    await useCases.refuseToConsiderDecisionUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(
      notificationService.notifyThatManuscriptWasRefused,
    ).toHaveBeenCalledTimes(1)
  })

  it('should publish "SubmissionScreeningRTCd" event', async () => {
    const manuscriptMock = {
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    const manuscriptId = chance.guid()

    await useCases.refuseToConsiderDecisionUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningRTCd',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const manuscriptMock = {
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
      authors: [],
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    Manuscript.findBy = jest.fn().mockReturnValue([manuscriptMock])

    const manuscriptId = chance.guid()

    await useCases.refuseToConsiderDecisionUseCase
      .initialize({
        models: { Manuscript, Note, Author, User, Member },
        mappers,
        factories,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({
        manuscriptId,
        content: chance.paragraph(),
        note: chance.paragraph(),
        userId,
      })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })
})
