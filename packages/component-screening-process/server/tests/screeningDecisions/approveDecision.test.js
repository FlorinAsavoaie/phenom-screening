process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')

const chance = new Chance()

const manuscriptId = chance.guid()
const submissionId = chance.guid()
const journalId = chance.guid()
const userId = chance.guid()

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

const notificationService = {
  notifyEAWhenManuscriptIsAccepted: jest.fn(),
}

const mappers = {
  manuscriptMapper: {
    domainToEventModel: jest.fn(() => ({
      submissionId,
      journalId,
    })),
  },
}

const screeningUseCases = {
  publishAuthorConfirmedUseCase: {
    execute: jest.fn(),
  },
}

const mockUpdateProperties = jest.fn()
const mockSave = jest.fn()

describe('Approve manuscript Use Case', () => {
  let Manuscript
  let Member
  let pubSub
  let wasUpdatedByUser

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript
    // eslint-disable-next-line prefer-destructuring
    Member = models.Member

    wasUpdatedByUser = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedByUser,
      }),
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.PAUSED,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    const result = useCases.approveDecisionUseCase
      .initialize({
        models,
        mappers,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    await expect(result).rejects.toThrow(
      `You cannot approve manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change status of the manuscript in "accepted"', async () => {
    const manuscriptId = chance.guid()

    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    jest.spyOn(Member, 'getEditorialAssistant').mockReturnValue({
      email: chance.email(),
      givenNames: 'John',
      surname: 'Doe',
    })

    await useCases.approveDecisionUseCase
      .initialize({
        models,
        mappers,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(Manuscript.find).toHaveBeenCalledWith(
      manuscriptId,
      '[authors, files, members]',
    )

    expect(mockUpdateProperties).toHaveBeenCalledWith({
      status: Manuscript.Statuses.SCREENING_APPROVED,
    })
    expect(mockSave).toHaveBeenCalled()

    expect(
      notificationService.notifyEAWhenManuscriptIsAccepted,
    ).toHaveBeenCalledTimes(1)
  })

  it('should notify EA', async () => {
    const manuscriptId = chance.guid()

    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    jest.spyOn(Member, 'getEditorialAssistant').mockReturnValue({
      email: chance.email(),
      givenNames: 'John',
      surname: 'Doe',
    })

    await useCases.approveDecisionUseCase
      .initialize({
        models,
        mappers,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(
      notificationService.notifyEAWhenManuscriptIsAccepted,
    ).toHaveBeenCalledTimes(1)
  })

  it('should publish "SubmissionScreeningPassed" event', async () => {
    const manuscriptId = chance.guid()

    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    jest.spyOn(Member, 'getEditorialAssistant').mockReturnValue({
      email: chance.email(),
      givenNames: 'John',
      surname: 'Doe',
    })

    await useCases.approveDecisionUseCase
      .initialize({
        models,
        mappers,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SubmissionScreeningPassed',
      data: {
        manuscripts: [{ submissionId, journalId }],
        submissionId,
      },
    })
  })

  it('should publish "submissionStatusUpdated" event', async () => {
    const manuscriptId = chance.guid()

    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
      submissionId,
      updateProperties: mockUpdateProperties,
      save: mockSave,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    jest.spyOn(Member, 'getEditorialAssistant').mockReturnValue({
      email: chance.email(),
      givenNames: 'John',
      surname: 'Doe',
    })

    await useCases.approveDecisionUseCase
      .initialize({
        models,
        mappers,
        useCases: screeningUseCases,
        services: { pubSub, notificationService },
      })
      .execute({ manuscriptId, content: chance.paragraph(), userId })

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      submissionId,
    )
    expect(wasUpdatedByUser).toHaveBeenCalledTimes(1)
    expect(wasUpdatedByUser).toHaveBeenCalledWith(userId)
  })
})
