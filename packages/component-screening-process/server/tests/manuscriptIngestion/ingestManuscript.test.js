process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const { Manuscript, Journal } = models

const chance = new Chance()
const journalId = chance.guid()
const submissionId = chance.guid()

const generateInput = () => ({
  submissionId,
  manuscripts: [
    {
      id: chance.guid(),
      journalId,
      abstract: chance.paragraph(),
      title: chance.sentence(),
      version: 1,
      articleType: { name: 'Research Article' },
      customId: '1256186',
      files: [
        {
          providerKey: '1234-4321-1234-4321',
          type: 'manuscript',
          originalName: 'Test File',
          fileName: 'Test File',
          size: '107161',
          mimeType: 'application/pdf,',
        },
      ],
      authors: [
        {
          isSubmitting: true,
          isCorresponding: true,
          position: 0,
          email: 'something@test.com',
          affiliation: { name: 'Hindawi' },
          country: 'Romania',
          givenNames: 'John',
          surname: 'Doe',
          title: 'Prof',
        },
      ],
      editors: [
        {
          id: chance.guid(),
          email: chance.email(),
          aff: 'Hindawi',
          country: 'Romania',
          givenNames: 'Leo',
          surname: 'Milo',
          title: 'Prof',
          role: {
            type: 'editorialAssistant',
            label: 'Editorial Assistant',
          },
        },
      ],
    },
  ],
})

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

const ingestedUseCases = {
  ingestManuscriptAuthorsEmailsUseCase: {
    execute: jest.fn(),
  },
  deleteManuscriptAuthorsEmailsUseCase: {
    execute: jest.fn(),
  },
}

const logger = {
  info: jest.fn(),
  error: jest.fn(),
}

describe('Ingest Manuscript Use Case', () => {
  let manuscriptInput
  beforeEach(async () => {
    jest.clearAllMocks()
    manuscriptInput = generateInput()
  })

  const savedManuscript = {
    id: chance.guid(),
    journalId,
    authors: [
      {
        id: chance.guid(),
        isSubmitting: true,
        isCorresponding: true,
        position: 0,
        email: 'something@test.com',
        aff: 'Hindawi',
        country: 'Romania',
        givenNames: 'John',
        surname: 'Doe',
        title: 'Prof',
      },
    ],
    files: [
      {
        id: chance.guid(),
        originalFileProviderKey: '1234-4321-1234-4321',
        type: 'manuscript',
        originalName: 'Test File',
        fileName: 'Test File',
        size: '107161',
        mimeType: 'application/pdf,',
      },
    ],
    flowType: 'screening',
    getManuscriptFile: jest.fn().mockReturnValue({
      id: '123456789',
      originalFileProviderKey: '1234-4321-1234-4321',
    }),
    manuscriptValidations: { id: chance.guid() },
    manuscriptChecks: { id: chance.guid() },
  }

  Manuscript.upsertGraph = jest.fn().mockReturnValue(savedManuscript)
  Manuscript.getLatestManuscriptVersion = jest
    .fn()
    .mockReturnValue({ id: chance.guid(), save: jest.fn() })

  Journal.find = jest.fn().mockReturnValue({
    id: journalId,
    name: 'Bioinorganic Chemistry and Application',
  })

  const factories = {
    authorFactory: { createAuthor: jest.fn() },
    fileFactory: { createFile: jest.fn() },
    manuscriptFactory: {
      createManuscript: jest.fn().mockReturnValue(savedManuscript),
    },
    memberFactory: {
      createMember: jest.fn(),
    },
  }

  const s3Service = {
    uploadReviewFileToScreeningS3: jest.fn(async () => {}),
  }

  it('should not save manuscript if article type is on excluded list', async () => {
    await useCases.ingestManuscriptUseCase
      .initialize({
        logger,
        models,
        factories,
        useCases: ingestedUseCases,
        s3Service,
      })
      .execute({
        submissionId,
        manuscripts: [
          {
            ...manuscriptInput.manuscripts[0],
            articleType: { name: 'Corrigendum' },
          },
        ],
      })
    expect(logger.info).toHaveBeenCalledWith(
      `The screening process is skipped for submission id ${manuscriptInput.submissionId} with article type: Corrigendum.`,
    )
    expect(Manuscript.upsertGraph).toHaveBeenCalledTimes(0)
  })

  it('should not save manuscript if the version was already ingested', async () => {
    Manuscript.getLatestManuscriptVersion = jest.fn().mockReturnValue({
      id: chance.guid(),
      save: jest.fn(),
      version: 1,
      status: Manuscript.Statuses.IN_PROGRESS,
    })

    await useCases.ingestManuscriptUseCase
      .initialize({
        logger,
        models,
        factories,
        useCases: ingestedUseCases,
        s3Service,
      })
      .execute(manuscriptInput)

    expect(logger.error).toHaveBeenCalledWith(
      `Manuscript with submissionId: ${manuscriptInput.submissionId}, version: ${manuscriptInput.manuscripts[0].version} was already ingested.`,
    )
    expect(Manuscript.upsertGraph).toHaveBeenCalledTimes(0)
  })

  it('should save manuscript after a returned to draft', async () => {
    const latestManuscript = {
      id: chance.guid(),
      save: jest.fn(),
      version: 1,
      status: Manuscript.Statuses.RETURNED_TO_DRAFT,
    }

    Manuscript.getLatestManuscriptVersion = jest
      .fn()
      .mockReturnValue(latestManuscript)

    await useCases.ingestManuscriptUseCase
      .initialize({
        logger,
        models,
        factories,
        useCases: ingestedUseCases,
        s3Service,
      })
      .execute(manuscriptInput)

    expect(latestManuscript.save).toBeCalledTimes(1)

    expect(
      ingestedUseCases.deleteManuscriptAuthorsEmailsUseCase.execute,
    ).toBeCalledTimes(1)
    expect(
      ingestedUseCases.deleteManuscriptAuthorsEmailsUseCase.execute,
    ).toHaveBeenCalledWith({ manuscriptId: latestManuscript.id })
  })

  it('should save the manuscript with authors and files', async () => {
    const {
      affiliation: { name: aff },
      country,
      email,
      title,
    } = manuscriptInput.manuscripts[0].authors[0]

    Manuscript.getLatestManuscriptVersion = jest.fn().mockReturnValue(null)

    await useCases.ingestManuscriptUseCase
      .initialize({
        logger,
        models,
        factories,
        useCases: ingestedUseCases,
        s3Service,
      })
      .execute({ submissionId, manuscripts: manuscriptInput.manuscripts })

    expect(Manuscript.upsertGraph).toHaveBeenCalledTimes(1)
    expect(
      ingestedUseCases.ingestManuscriptAuthorsEmailsUseCase.execute,
    ).toHaveBeenCalledTimes(1)
    expect(s3Service.uploadReviewFileToScreeningS3).toHaveBeenCalledTimes(1)
    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'ManuscriptIngested',
        data: {
          authors: [
            {
              aff,
              country,
              email,
              title,
              surname: expect.anything(),
              givenNames: expect.anything(),
              isCorresponding: true,
              isSubmitting: true,
              position: expect.anything(),
              id: expect.anything(),
            },
          ],
          fileId: expect.anything(),
          manuscriptId: expect.anything(),
          originalFileProviderKey: expect.anything(),
        },
      }),
    )
  })
})
