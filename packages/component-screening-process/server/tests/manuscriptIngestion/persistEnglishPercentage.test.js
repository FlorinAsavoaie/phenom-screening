process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const models = require('@pubsweet/models')

const { ManuscriptChecks } = models
const chance = new Chance()

const useCases = require('../../src/use-cases')

ManuscriptChecks.updateBy = jest.fn()
ManuscriptChecks.areAllChecksPerformed = jest.fn().mockReturnValue(false)

describe('Persist English Percentage Use Case', () => {
  it('should update manuscriptCheck with the english percent', async () => {
    const englishPercentage = chance.d100()
    const wordCount = chance.d100()
    const manuscriptId = chance.guid()

    await useCases.persistEnglishPercentageUseCase
      .initialize(models)
      .execute({ manuscriptId, englishPercentage, wordCount })

    expect(ManuscriptChecks.updateBy).toHaveBeenCalled()
    expect(ManuscriptChecks.updateBy).toHaveBeenCalledWith({
      queryObject: { manuscriptId },
      propertiesToUpdate: {
        wordCount,
        languagePercentage: englishPercentage,
      },
    })
    expect(ManuscriptChecks.areAllChecksPerformed).toHaveBeenCalled()
  })
})
