const Chance = require('chance')
const models = require('@pubsweet/models')

const { File, ManuscriptChecks } = models
const useCases = require('../../src/use-cases')

const chance = new Chance()

const mockFile = {
  id: chance.guid(),
  providerKey: null,
  originalFileProviderKey: '123',
  type: 'manuscript',
  originalName: chance.name(),
  fileName: chance.name(),
  manuscriptId: chance.guid(),
}

const providerKey = chance.guid()

File.find = jest.fn().mockReturnValue(new File(mockFile))

File.prototype.updateProperties = jest
  .fn()
  .mockReturnValue({ ...mockFile, providerKey })

File.prototype.save = jest.fn()
ManuscriptChecks.updateBy = jest.fn()
ManuscriptChecks.areAllChecksPerformed = jest.fn().mockReturnValue(false)

describe('Persist Converted File UseCase', () => {
  it('should update the providerKey property of the converted file', async () => {
    const input = {
      fileId: chance.guid(),
      convertedFileProviderKey: providerKey,
    }
    await useCases.persistConvertedFileUseCase
      .initialize({ models })
      .execute(input)

    expect(File.find).toHaveBeenCalled()
    expect(File.find).toHaveBeenCalledWith(input.fileId)
    expect(File.prototype.updateProperties).toHaveBeenCalledWith({
      providerKey,
    })
    expect(File.prototype.save).toHaveBeenCalled()
    expect(ManuscriptChecks.areAllChecksPerformed).toHaveBeenCalled()
  })

  it('should update the fileConverted property for the ManuscriptChecks', async () => {
    const input = {
      fileId: chance.guid(),
      convertedFileProviderKey: 'providerKey',
    }

    await useCases.persistConvertedFileUseCase
      .initialize({ models })
      .execute(input)

    expect(ManuscriptChecks.updateBy).toHaveBeenCalled()
    expect(ManuscriptChecks.updateBy).toHaveBeenCalledWith({
      propertiesToUpdate: { fileConverted: true },
      queryObject: { manuscriptId: mockFile.manuscriptId },
    })
    expect(ManuscriptChecks.areAllChecksPerformed).toHaveBeenCalled()
  })
})
