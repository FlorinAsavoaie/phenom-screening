const models = require('@pubsweet/models')
const useCases = require('./../src/use-cases')

describe('Withdraw Submission', () => {
  let Manuscript
  let updateBy
  let pubSub
  let wasUpdatedBySystem

  beforeEach(() => {
    // eslint-disable-next-line prefer-destructuring
    Manuscript = models.Manuscript

    updateBy = jest.spyOn(Manuscript, 'updateBy').mockReturnValueOnce()
    wasUpdatedBySystem = jest.fn()
    pubSub = {
      notifyThatSubmissionStatusFor: jest.fn().mockReturnValue({
        wasUpdatedBySystem,
      }),
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should set "withdrawn" status for all manuscripts for the given submissionId', async () => {
    await useCases.withdrawSubmissionUseCase
      .initialize({
        models: { Manuscript },
        services: { pubSub },
      })
      .execute('some-submission-id')

    expect(updateBy).toHaveBeenCalledWith({
      queryObject: {
        submissionId: 'some-submission-id',
      },
      propertiesToUpdate: {
        status: 'withdrawn',
      },
    })
  })

  it('should notify submission status updated', async () => {
    await useCases.withdrawSubmissionUseCase
      .initialize({
        models: { Manuscript },
        services: { pubSub },
      })
      .execute('some-submission-id')

    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledTimes(1)
    expect(pubSub.notifyThatSubmissionStatusFor).toHaveBeenCalledWith(
      'some-submission-id',
    )
    expect(wasUpdatedBySystem).toHaveBeenCalledTimes(1)
  })
})
