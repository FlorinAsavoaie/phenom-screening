const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { User, Manuscript, Team } = models

const publishMessage = jest.fn()

global.applicationEventBus = {
  publishMessage,
}

const assignChecker = {
  id: chance.guid(),
  teams: [{ assignationDate: new Date() }],
}

const mappers = {
  userMapper: {
    domainToCheckerEventModel: jest.fn().mockReturnValue(assignChecker),
  },
}

describe('Assign unassigned manuscript to checkers', () => {
  it('Should assign checker to manuscript if members are confirmed.', async () => {
    const journalId = chance.guid()
    const teamId = chance.guid()
    const users = [
      {
        id: chance.guid(),
        email: chance.email(),
        givenNames: chance.first(),
        surname: chance.last(),
        isConfirmed: true,
        role: User.Roles.SCREENER,
        team: {
          id: teamId,
        },
      },
      {
        id: chance.guid(),
        email: chance.email(),
        givenNames: chance.first(),
        surname: chance.last(),
        isConfirmed: true,
        role: User.Roles.TEAM_LEADER,
        team: {
          id: teamId,
        },
      },
    ]
    const params = {
      journalIds: [journalId],
      teamMembers: users,
      teamType: Team.Type.SCREENING,
      team: {
        id: teamId,
        type: Team.Type.SCREENING,
        journals: [
          {
            id: journalId,
          },
        ],
        users,
      },
    }

    const unassignedManuscripts = [
      {
        id: chance.guid(),
        journalId,
        status: Manuscript.Statuses.IN_PROGRESS,
        flowType: Manuscript.FlowTypes.SCREENING,
        team: null,
        assignChecker: jest.fn().mockReturnValue(assignChecker),
      },
    ]

    Manuscript.getUnassignedManuscripts = jest
      .fn()
      .mockReturnValueOnce(unassignedManuscripts)

    User.setAssignationDate = jest.fn()

    await useCases.assignUnassignedManuscriptsToCheckersUseCase
      .initialize({
        models: { Manuscript, User, Team },
        mappers,
      })
      .execute(params)

    expect(unassignedManuscripts[0].assignChecker).toHaveBeenCalledTimes(1)

    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledTimes(
      1,
    )
    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledWith({
      checker: assignChecker,
      manuscript: unassignedManuscripts[0],
      assignationType: 'automaticAssignmentToANewChecker',
    })
  })
})
