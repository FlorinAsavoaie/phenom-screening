const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { User } = models

const publishMessage = jest.fn()

global.applicationEventBus = {
  publishMessage,
}

const assignChecker = {
  id: chance.guid(),
}

const mappers = {
  userMapper: {
    domainToCheckerEventModel: jest.fn().mockReturnValue(assignChecker),
  },
}

describe('Automatic reassign manuscript to checkers', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  it('Should assign manuscript to the same screener from another team if the other team also have the manuscript journal', async () => {
    const journal = { id: chance.guid(), name: 'Bio' }

    const screener = {
      id: chance.guid(),
      email: chance.email(),
      givenNames: chance.first(),
      surname: chance.last(),
      isConfirmed: true,
      role: User.Roles.SCREENER,
    }

    const oldTeam = { id: chance.guid(), name: 'oltTeam' }
    const newTeam = { id: chance.guid(), name: 'newTeam', journals: [journal] }

    const manuscript = {
      id: chance.guid(),
      journalId: journal.id,
      user: screener,
      team: oldTeam,
      assignChecker: jest.fn().mockReturnValue(screener),
    }

    User.getRoleByManuscriptFlowType = jest.fn().mockReturnValue('screener')

    User.getCheckerFromTeamsWithManuscriptJournal = jest
      .fn()
      .mockReturnValueOnce({
        teams: [newTeam],
        ...screener,
      })

    await useCases.automaticReassignManuscriptToCheckerUseCase
      .initialize({ models: { User }, mappers })
      .execute(manuscript)

    expect(manuscript.assignChecker).toHaveBeenCalledTimes(1)
    expect(manuscript.assignChecker).toHaveBeenCalledWith({
      id: screener.id,
      team_id: newTeam.id,
    })

    expect(publishMessage).toHaveBeenCalledTimes(1)
    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledWith({
      checker: screener,
      manuscript,
      assignationType: 'automaticReassignmentToTheSameCheckerFromDifferentTeam',
    })
  })

  it('Should assign manuscript to the a new screener', async () => {
    const journal = { id: chance.guid(), name: 'Bio' }

    const screener1 = {
      id: chance.guid(),
      email: chance.email(),
      givenNames: chance.first(),
      surname: chance.last(),
      isConfirmed: true,
      role: User.Roles.SCREENER,
    }

    const screener2 = {
      id: chance.guid(),
      email: chance.email(),
      givenNames: chance.first(),
      surname: chance.last(),
      isConfirmed: true,
      role: User.Roles.SCREENER,
    }

    const screener1Team = { id: chance.guid(), name: 'screener1Team' }
    const screener2Team = {
      id: chance.guid(),
      name: 'screener2Team',
      journals: [journal],
    }

    const manuscript = {
      id: chance.guid(),
      journalId: journal.id,
      user: screener1,
      team: screener1Team,
      assignChecker: jest.fn().mockReturnValue(screener2),
    }

    User.getRoleByManuscriptFlowType = jest.fn().mockReturnValue('screener')

    User.getCheckerFromTeamsWithManuscriptJournal = jest
      .fn()
      .mockReturnValueOnce(screener1)

    User.getFirstAvailableChecker = jest
      .fn()
      .mockReturnValueOnce({ team_id: screener2Team.id, ...screener2 })

    await useCases.automaticReassignManuscriptToCheckerUseCase
      .initialize({ models: { User }, mappers })
      .execute(manuscript)

    expect(manuscript.assignChecker).toHaveBeenCalledTimes(1)
    expect(manuscript.assignChecker).toHaveBeenCalledWith({
      team_id: screener2Team.id,
      ...screener2,
    })

    expect(publishMessage).toHaveBeenCalledTimes(1)
    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledWith({
      checker: screener2,
      manuscript,
      assignationType: 'automaticReassignmentToANewChecker',
    })
  })
})
