const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { Manuscript, User, Team } = models

const checkerId = chance.guid()
const checkerTeamId = chance.guid()
const checker = {
  id: checkerId,
  role: User.Roles.SCREENER,
  team_id: checkerTeamId,
  isConfirmed: true,
}

const manuscriptId = chance.guid()

const publishMessage = jest.fn()

global.applicationEventBus = {
  publishMessage,
}

const mappers = {
  userMapper: {
    domainToCheckerEventModel: jest.fn().mockReturnValue(checker),
  },
}

const mockedManuscript = {
  id: manuscriptId,
  flowType: Manuscript.FlowTypes.SCREENING,
  assignChecker: jest.fn().mockReturnValue(checker),
}

describe('Assign manuscript to checker after automatic checks are done', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    Manuscript.findOneBy = jest.fn().mockReturnValue(mockedManuscript)
    Manuscript.getPreviousVersion = jest.fn().mockReturnValue(undefined)
    User.getRoleByManuscriptFlowType = jest.fn().mockReturnValue('screener')
  })

  it('Should return, because of no available screeners', async () => {
    User.getFirstAvailableChecker = jest.fn().mockReturnValue(undefined)

    await useCases.assignManuscriptToCheckerUseCase
      .initialize({
        models,
        mappers,
      })
      .execute({
        manuscriptId,
      })

    expect(mockedManuscript.assignChecker).toHaveBeenCalledTimes(0)
    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledTimes(
      0,
    )
  })

  it('Should successfully assign a new checker to manuscript', async () => {
    Manuscript.getPreviousVersion = jest.fn().mockReturnValue(undefined)
    User.getFirstAvailableChecker = jest.fn().mockReturnValue(checker)

    await useCases.assignManuscriptToCheckerUseCase
      .initialize({
        models,
        mappers,
      })
      .execute({
        manuscriptId,
      })

    expect(mockedManuscript.assignChecker).toHaveBeenCalledTimes(1)
    expect(mockedManuscript.assignChecker).toHaveBeenCalledWith(checker)

    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledTimes(
      1,
    )
    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledWith({
      checker,
      manuscript: mockedManuscript,
      assignationType: 'automaticAssignmentToANewChecker',
    })
  })

  it('Should successfully assign the same checker from the previous manuscript', async () => {
    const mockedPreviousManuscript = {
      flowType: Manuscript.FlowTypes.SCREENING,
      user: checker,
      team: {
        id: checkerTeamId,
      },
      deleteRelation: jest.fn(),
    }

    Manuscript.getPreviousVersion = jest
      .fn()
      .mockReturnValue(mockedPreviousManuscript)

    await useCases.assignManuscriptToCheckerUseCase
      .initialize({
        models,
        mappers,
      })
      .execute({
        manuscriptId,
      })

    expect(mockedManuscript.assignChecker).toHaveBeenCalledTimes(1)
    expect(mockedManuscript.assignChecker).toHaveBeenCalledWith({
      id: checker.id,
      team_id: checkerTeamId,
    })

    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledTimes(
      1,
    )
    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledWith({
      checker,
      manuscript: mockedManuscript,
      assignationType: 'automaticAssignmentToTheSameChecker',
    })

    expect(mockedPreviousManuscript.deleteRelation).toHaveBeenCalledTimes(2)
    expect(mockedPreviousManuscript.deleteRelation).toHaveBeenCalledWith('user')
    expect(mockedPreviousManuscript.deleteRelation).toHaveBeenCalledWith('team')
  })

  it('Should successfully assign a new checker from the previous manuscript team', async () => {
    const teamId = chance.guid()
    const mockedPreviousManuscript = {
      flowType: Manuscript.FlowTypes.SCREENING,
      user: null,
      team: {
        id: teamId,
      },
      deleteRelation: jest.fn(),
    }

    const teamLeader = {
      id: chance.guid(),
      role: User.Roles.TEAM_LEADER,
      isConfirmed: true,
    }

    Manuscript.getPreviousVersion = jest
      .fn()
      .mockReturnValue(mockedPreviousManuscript)

    Team.findOneBy = jest
      .fn()
      .mockReturnValue({ teamId, users: [{ ...checker }, { ...teamLeader }] })

    await useCases.assignManuscriptToCheckerUseCase
      .initialize({
        models,
        mappers,
      })
      .execute({
        manuscriptId,
      })

    expect(mockedManuscript.assignChecker).toHaveBeenCalledTimes(1)
    expect(mockedManuscript.assignChecker).toHaveBeenCalledWith({
      id: checker.id,
      team_id: checkerTeamId,
      isConfirmed: checker.isConfirmed,
      role: checker.role,
    })

    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledTimes(
      1,
    )
    expect(mappers.userMapper.domainToCheckerEventModel).toHaveBeenCalledWith({
      checker,
      manuscript: mockedManuscript,
      assignationType: 'automaticAssignmentToANewCheckerFromSameTeam',
    })

    expect(mockedPreviousManuscript.deleteRelation).toHaveBeenCalledTimes(1)
    expect(mockedPreviousManuscript.deleteRelation).toHaveBeenCalledWith('team')
  })
})
