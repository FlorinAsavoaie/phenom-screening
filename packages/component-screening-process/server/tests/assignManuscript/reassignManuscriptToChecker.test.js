const { Manuscript, User } = require('@pubsweet/models')
const Chance = require('chance')
const useCases = require('../../src/use-cases')

const chance = new Chance()

const manuscriptId = chance.guid()
const teamId = chance.guid()
const userId = chance.guid()

const publishMessage = jest.fn()

global.applicationEventBus = {
  publishMessage,
}

const assignChecker = {
  id: chance.guid(),
}

const manuscriptMock = {
  id: manuscriptId,
  flowType: Manuscript.FlowTypes.SCREENING,
  team: {
    id: teamId,
  },
  assignChecker: jest.fn().mockReturnValue(assignChecker),
}

const mappers = {
  userMapper: {
    domainToCheckerEventModel: jest.fn().mockReturnValue(assignChecker),
  },
}

Manuscript.findOneBy = jest.fn().mockReturnValue(manuscriptMock)

describe('Reassign Manuscript to Checker Use Case', () => {
  it('should reassign the manuscript to another screener', async () => {
    const screenerMock = {
      id: userId,
      isConfirmed: true,
      teams: [{ id: teamId, role: User.Roles.SCREENER }],
    }
    const mockRole = User.Roles.SCREENER
    User.getRoleByManuscriptFlowType = jest.fn().mockReturnValue(mockRole)
    User.findOneBy = jest.fn().mockReturnValue(screenerMock)

    await useCases.reassignManuscriptToCheckerUseCase
      .initialize({
        models: { Manuscript, User },
        mappers,
      })
      .execute({ manuscriptId, userId })

    expect(manuscriptMock.assignChecker).toHaveBeenCalledTimes(1)
    expect(manuscriptMock.assignChecker).toHaveBeenCalledWith({
      id: screenerMock.id,
      team_id: teamId,
    })
    expect(publishMessage).toHaveBeenCalledTimes(1)
  })

  it('should throw an error when the new assigned checker is not confirmed', async () => {
    User.findOneBy = jest.fn().mockReturnValue({
      id: userId,
      isConfirmed: false,
      selectProperty: jest.fn().mockReturnValue(User.Roles.SCREENER),
    })
    User.getRoleByManuscriptFlowType = jest
      .fn()
      .mockReturnValue(User.Roles.SCREENER)

    const result = useCases.reassignManuscriptToCheckerUseCase
      .initialize({
        models: { Manuscript, User },
        mappers,
      })
      .execute({ manuscriptId, userId })

    await expect(result).rejects.toThrow(
      'Manuscripts can only be assigned to confirmed checkers.',
    )
  })
})
