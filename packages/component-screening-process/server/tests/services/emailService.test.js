const { noop } = require('lodash')
const { emailService } = require('./../../src/services')

describe('Email Service', () => {
  it('should send a notification with the right configuration', () => {
    const mockedEmailTemplate = {
      sendEmail: jest.fn(),
    }
    const EmailTemplate = jest
      .fn()
      .mockImplementation(() => mockedEmailTemplate)

    const configService = {
      get: noop,
    }
    jest.spyOn(configService, 'get').mockReturnValueOnce({
      emailData: {
        footerTextTemplate: (_, recepeintMail) =>
          `Some footer text for ${recepeintMail}`,
      },
    })

    emailService.initialize({ EmailTemplate, configService }).sendNotification({
      toUser: {
        fullName: 'toUserFullName',
        email: 'toUserEmail',
      },
      fromUser: {
        fullName: 'fromUserFullName',
        email: 'fromUserEmail',
      },
      content: {
        subject: 'some subject',
        paragraph: 'someParagraph',
      },
    })

    expect(EmailTemplate.mock.calls[0][0]).toStrictEqual({
      type: 'system',
      templateType: 'notification',
      fromEmail: 'fromUserFullName <fromUserEmail>',
      toUser: { email: 'toUserEmail', name: 'toUserFullName' },
      content: {
        footerText: 'Some footer text for toUserEmail',
        paragraph: 'someParagraph',
        signatureName: 'fromUserFullName',
        subject: 'some subject',
      },
      bodyProps: { hasIntro: false, hasSignature: true },
      cc: undefined,
    })
    expect(EmailTemplate.mock.calls.length).toEqual(1)
  })

  it('should send into consideration CC', () => {
    const mockedEmailTemplate = {
      sendEmail: jest.fn(),
    }
    const EmailTemplate = jest
      .fn()
      .mockImplementation(() => mockedEmailTemplate)

    const configService = {
      get: noop,
    }
    jest.spyOn(configService, 'get').mockReturnValueOnce({
      emailData: {
        footerTextTemplate: (_, recepeintMail) =>
          `Some footer text for ${recepeintMail}`,
      },
    })

    emailService.initialize({ EmailTemplate, configService }).sendNotification({
      toUser: {
        fullName: 'toUserFullName',
        email: 'toUserEmail',
      },
      fromUser: {
        fullName: 'fromUserFullName',
        email: 'fromUserEmail',
      },
      content: {
        subject: 'some subject',
        paragraph: 'someParagraph',
      },
      cc: 'some-email-for-cc',
    })

    expect(EmailTemplate.mock.calls[0][0]).toStrictEqual({
      type: 'system',
      templateType: 'notification',
      fromEmail: 'fromUserFullName <fromUserEmail>',
      toUser: { email: 'toUserEmail', name: 'toUserFullName' },
      content: {
        footerText: 'Some footer text for toUserEmail',
        paragraph: 'someParagraph',
        signatureName: 'fromUserFullName',
        subject: 'some subject',
      },
      bodyProps: { hasIntro: false, hasSignature: true },
      cc: 'some-email-for-cc',
    })
    expect(EmailTemplate.mock.calls.length).toEqual(1)
  })
})
