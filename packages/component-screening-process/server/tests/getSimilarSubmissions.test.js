const useCases = require('./../src/use-cases')
const models = require('@pubsweet/models')

describe('Get Similar Submissions', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should return an empty array', async () => {
    const { Manuscript } = models
    const find = jest.spyOn(Manuscript, 'find').mockReturnValue({
      id: '1',
      title: 'Title 1',
      authors: [
        {
          id: '1',
          email: 'email-1',
        },
        {
          id: '2',
          email: 'email-2',
        },
      ],
    })

    const getSimilarSubmissions = jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockReturnValue([])

    const similarSubmissions = await useCases.getSimilarSubmissionsUseCase
      .initialize({
        models: { Manuscript },
      })
      .execute({ manuscriptId: '1' })

    expect(find).toHaveBeenCalledTimes(1)
    expect(find).toHaveBeenCalledWith('1', 'authors')
    expect(getSimilarSubmissions).toHaveBeenCalledTimes(1)

    expect(similarSubmissions).toStrictEqual([])
  })

  it('should return similar submissions by title', async () => {
    const { Manuscript } = models
    const find = jest.spyOn(Manuscript, 'find').mockReturnValue({
      id: '1',
      title: 'Title 1',
      authors: [
        {
          id: '1',
          email: 'email-1',
        },
        {
          id: '2',
          email: 'email-2',
        },
      ],
    })

    const getSimilarSubmissions = jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockReturnValue([
        {
          id: '2',
          title: 'Title 1',
          authors: [
            {
              id: '3',
              email: 'email-3',
            },
            {
              id: '4',
              email: 'email-4',
            },
          ],
        },
      ])

    const similarSubmissions = await useCases.getSimilarSubmissionsUseCase
      .initialize({
        models: { Manuscript },
      })
      .execute({ manuscriptId: '1' })

    expect(find).toHaveBeenCalledTimes(1)
    expect(find).toHaveBeenCalledWith('1', 'authors')
    expect(getSimilarSubmissions).toHaveBeenCalledTimes(1)

    expect(similarSubmissions).toStrictEqual([
      {
        id: '2',
        title: 'Title 1',
        authors: [
          {
            id: '3',
            email: 'email-3',
          },
          {
            id: '4',
            email: 'email-4',
          },
        ],
      },
    ])
  })

  it('should return similar submissions by authors email', async () => {
    const { Manuscript } = models
    const find = jest.spyOn(Manuscript, 'find').mockReturnValue({
      id: '1',
      title: 'Title 1',
      authors: [
        {
          id: '1',
          email: 'email-1',
        },
        {
          id: '2',
          email: 'email-2',
        },
      ],
    })

    const getSimilarSubmissions = jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockReturnValue([
        {
          id: '2',
          title: 'Title 2',
          authors: [
            {
              id: '3',
              email: 'email-1',
            },
            {
              id: '4',
              email: 'email-2',
            },
          ],
        },
      ])

    const similarSubmissions = await useCases.getSimilarSubmissionsUseCase
      .initialize({
        models: { Manuscript },
      })
      .execute({ manuscriptId: '1' })

    expect(find).toHaveBeenCalledTimes(1)
    expect(find).toHaveBeenCalledWith('1', 'authors')
    expect(getSimilarSubmissions).toHaveBeenCalledTimes(1)

    expect(similarSubmissions).toStrictEqual([
      {
        id: '2',
        title: 'Title 2',
        authors: [
          {
            id: '3',
            email: 'email-1',
          },
          {
            id: '4',
            email: 'email-2',
          },
        ],
      },
    ])
  })

  it('should return similar submissions by title and authors email', async () => {
    const { Manuscript } = models
    const find = jest.spyOn(Manuscript, 'find').mockReturnValue({
      id: '1',
      title: 'Title 1',
      authors: [
        {
          id: '1',
          email: 'email-1',
        },
        {
          id: '2',
          email: 'email-2',
        },
      ],
    })

    const getSimilarSubmissions = jest
      .spyOn(Manuscript, 'getSimilarSubmissions')
      .mockReturnValue([
        {
          id: '2',
          title: 'Title 2',
          authors: [
            {
              id: '3',
              email: 'email-1',
            },
            {
              id: '4',
              email: 'email-2',
            },
          ],
        },
        {
          id: '3',
          title: 'Title 1',
          authors: [
            {
              id: '5',
              email: 'email-1',
            },
            {
              id: '6',
              email: 'email-6',
            },
          ],
        },
      ])

    const similarSubmissions = await useCases.getSimilarSubmissionsUseCase
      .initialize({
        models: { Manuscript },
      })
      .execute({ manuscriptId: '1' })

    expect(find).toHaveBeenCalledTimes(1)
    expect(find).toHaveBeenCalledWith('1', 'authors')
    expect(getSimilarSubmissions).toHaveBeenCalledTimes(1)

    expect(similarSubmissions).toStrictEqual([
      {
        id: '2',
        title: 'Title 2',
        authors: [
          {
            id: '3',
            email: 'email-1',
          },
          {
            id: '4',
            email: 'email-2',
          },
        ],
      },
      {
        id: '3',
        title: 'Title 1',
        authors: [
          {
            id: '5',
            email: 'email-1',
          },
          {
            id: '6',
            email: 'email-6',
          },
        ],
      },
    ])
  })
})
