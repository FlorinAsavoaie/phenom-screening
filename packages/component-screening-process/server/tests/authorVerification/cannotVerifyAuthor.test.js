process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const useCases = require('../../src/use-cases')
const models = require('@pubsweet/models')
const Chance = require('chance')

const chance = new Chance()
const { Author, PossibleAuthorProfile, Manuscript, User } = models

describe('Cannot verify author Use Case', () => {
  beforeEach(() => {
    Author.updateBy = jest.fn()
    PossibleAuthorProfile.updateBy = jest.fn()
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(true)
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptId = chance.guid()
    const authorId = chance.guid()
    const userId = chance.guid()

    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.PAUSED,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(false)

    const result = useCases.cannotVerifyAuthorUseCase
      .initialize(models)
      .execute({ manuscriptId, authorId, userId })

    await expect(result).rejects.toThrow(
      `You cannot perform 'can't be verified' action for author ${authorId} from manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should change author `isVerified` field to null', async () => {
    const manuscriptId = chance.guid()
    const authorId = chance.guid()
    const userId = chance.guid()

    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await useCases.cannotVerifyAuthorUseCase
      .initialize(models)
      .execute({ manuscriptId, authorId, userId })

    expect(Author.updateBy).toHaveBeenCalledWith({
      queryObject: {
        id: authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        isVerified: null,
        isVerifiedOutOfTool: false,
      },
    })

    expect(PossibleAuthorProfile.updateBy).toHaveBeenCalledWith({
      queryObject: {
        authorId,
        manuscriptId,
      },
      propertiesToUpdate: {
        chosen: false,
      },
    })
  })
})
