const Chance = require('chance')
const models = require('@pubsweet/models')

const { Author, ManuscriptChecks } = models
const useCases = require('../../src/use-cases')

const chance = new Chance()

Author.updateBy = jest.fn()

ManuscriptChecks.updateBy = jest.fn()
ManuscriptChecks.areAllChecksPerformed = jest.fn().mockReturnValue(false)

describe('Persist authors sanction list', () => {
  it('should update Author with isOnBadDebtList value', async () => {
    const data = {
      manuscriptId: chance.guid(),
      authors: {
        [chance.guid()]: {
          isOnBadDebtList: true,
          isOnBlackList: false,
          isOnWatchList: false,
        },
      },
    }
    await useCases.persistAuthorsSanctionListUseCase
      .initialize(models)
      .execute(data)

    expect(Author.updateBy).toHaveBeenCalled()
    expect(Author.updateBy).toHaveBeenCalledWith({
      propertiesToUpdate: {
        isOnBadDebtList: true,
        isOnBlackList: false,
        isOnWatchList: false,
      },
      queryObject: {
        manuscriptId: data.manuscriptId,
        id: expect.anything(),
      },
    })
    expect(ManuscriptChecks.areAllChecksPerformed).toHaveBeenCalled()
  })

  it('should update ManuscriptChecks with sanctionLstCheckDone', async () => {
    const data = {
      manuscriptId: chance.guid(),
      sanctionListCheckDone: true,
    }

    await useCases.persistAuthorsSanctionListUseCase
      .initialize(models)
      .execute(data)

    expect(ManuscriptChecks.updateBy).toHaveBeenCalled()
    expect(ManuscriptChecks.updateBy).toHaveBeenCalledWith({
      propertiesToUpdate: { sanctionListCheckDone: true },
      queryObject: { manuscriptId: data.manuscriptId },
    })
    expect(ManuscriptChecks.areAllChecksPerformed).toHaveBeenCalled()
  })
})
