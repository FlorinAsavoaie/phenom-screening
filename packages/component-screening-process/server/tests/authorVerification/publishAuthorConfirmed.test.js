process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const Chance = require('chance')

const { publishAuthorConfirmedUseCase } = require('../../src/use-cases')

const { Author, PossibleAuthorProfile } = models

const chance = new Chance()

const applicationEventBus = {
  publishMessage: () => {
    throw new Error('Not implemented')
  },
}

const generateAuthor = (email) => ({
  email,
  givenNames: chance.name(),
  surname: chance.name(),
})
const generatePossibleAuthor = (email) => ({
  email,
  givenNames: chance.name(),
  surname: chance.name(),
})

describe('Publish author confirmed Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should publish event Author Confirmed for authors verified out of tool', async () => {
    jest
      .spyOn(Author, 'findBy')
      .mockResolvedValueOnce([
        generateAuthor('author1@test.test'),
        generateAuthor('author2@test.test'),
      ])

    jest
      .spyOn(PossibleAuthorProfile, 'findBy')
      .mockResolvedValueOnce([
        generatePossibleAuthor('possibleAuthor1@test.test'),
        generatePossibleAuthor('possibleAuthor2@test.test'),
      ])

    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()

    const manuscriptId = chance.guid()

    await publishAuthorConfirmedUseCase
      .initialize({ models, services: { applicationEventBus } })
      .execute(manuscriptId)

    expect(Author.findBy).toHaveBeenCalledWith({
      queryObject: {
        manuscriptId,
        isVerifiedOutOfTool: true,
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'AuthorConfirmed',
      data: {
        email: 'author1@test.test',
        firstName: expect.anything(),
        lastName: expect.anything(),
      },
    })
    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'AuthorConfirmed',
      data: {
        email: 'author2@test.test',
        firstName: expect.anything(),
        lastName: expect.anything(),
      },
    })

    expect(PossibleAuthorProfile.findBy).toHaveBeenCalledWith({
      queryObject: {
        manuscriptId,
        chosen: true,
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'AuthorConfirmed',
      data: {
        email: 'possibleAuthor1@test.test',
        firstName: expect.anything(),
        lastName: expect.anything(),
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'AuthorConfirmed',
      data: {
        email: 'possibleAuthor2@test.test',
        firstName: expect.anything(),
        lastName: expect.anything(),
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(4)
  })

  it('should set email from Author models if it is null', async () => {
    jest.spyOn(Author, 'findBy').mockResolvedValueOnce([
      {
        email: 'author-email-1',
        givenNames: 'author-given-names-1',
        surname: 'author-surname-1',
      },
      {
        email: 'author-email-2',
        givenNames: 'author-given-names-2',
        surname: 'author-surname-2',
      },
      {
        email: 'author-email-3',
        givenNames: 'author-given-names-3',
        surname: 'author-surname-3',
      },
    ])

    jest.spyOn(PossibleAuthorProfile, 'findBy').mockResolvedValueOnce([
      {
        email: 'possible-author-profile-email-1',
        givenNames: 'possible-author-profile-given-names-1',
        surname: 'possible-author-profile-surname-1',
      },
      {
        email: null,
        givenNames: 'possible-author-profile-given-names-2',
        surname: 'possible-author-profile-surname-2',
      },
      {
        email: null,
        givenNames: 'possible-author-profile-given-names-3',
        surname: 'possible-author-profile-surname-3',
      },
    ])

    jest
      .spyOn(Author, 'find')
      .mockResolvedValueOnce({
        email: 'author-new-email-for-2',
      })
      .mockResolvedValueOnce({
        email: 'author-new-email-for-3',
      })

    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()

    const manuscriptId = chance.guid()

    await publishAuthorConfirmedUseCase
      .initialize({ models, services: { applicationEventBus } })
      .execute(manuscriptId)

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(6)

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(1, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-1',
        firstName: 'author-given-names-1',
        lastName: 'author-surname-1',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(2, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-2',
        firstName: 'author-given-names-2',
        lastName: 'author-surname-2',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(3, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-3',
        firstName: 'author-given-names-3',
        lastName: 'author-surname-3',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(4, {
      event: 'AuthorConfirmed',
      data: {
        email: 'possible-author-profile-email-1',
        firstName: 'possible-author-profile-given-names-1',
        lastName: 'possible-author-profile-surname-1',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(5, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-new-email-for-2',
        firstName: 'possible-author-profile-given-names-2',
        lastName: 'possible-author-profile-surname-2',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(6, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-new-email-for-3',
        firstName: 'possible-author-profile-given-names-3',
        lastName: 'possible-author-profile-surname-3',
      },
    })
  })

  it('should set givenNames from Author models if it is null', async () => {
    jest.spyOn(Author, 'findBy').mockResolvedValueOnce([
      {
        email: 'author-email-1',
        givenNames: 'author-given-names-1',
        surname: 'author-surname-1',
      },
      {
        email: 'author-email-2',
        givenNames: 'author-given-names-2',
        surname: 'author-surname-2',
      },
      {
        email: 'author-email-3',
        givenNames: 'author-given-names-3',
        surname: 'author-surname-3',
      },
    ])

    jest.spyOn(PossibleAuthorProfile, 'findBy').mockResolvedValueOnce([
      {
        email: 'possible-author-profile-email-1',
        givenNames: 'possible-author-profile-given-names-1',
        surname: 'possible-author-profile-surname-1',
      },
      {
        email: null,
        givenNames: 'possible-author-profile-given-names-2',
        surname: 'possible-author-profile-surname-2',
      },
      {
        email: 'possible-author-profile-email-3',
        givenNames: null,
        surname: 'possible-author-profile-surname-3',
      },
    ])

    jest
      .spyOn(Author, 'find')
      .mockResolvedValueOnce({
        email: 'author-new-email-for-2',
      })
      .mockResolvedValueOnce({
        givenNames: 'author-new-given-names-for-3',
      })

    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()

    const manuscriptId = chance.guid()

    await publishAuthorConfirmedUseCase
      .initialize({ models, services: { applicationEventBus } })
      .execute(manuscriptId)

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(6)

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(1, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-1',
        firstName: 'author-given-names-1',
        lastName: 'author-surname-1',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(2, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-2',
        firstName: 'author-given-names-2',
        lastName: 'author-surname-2',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(3, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-3',
        firstName: 'author-given-names-3',
        lastName: 'author-surname-3',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(4, {
      event: 'AuthorConfirmed',
      data: {
        email: 'possible-author-profile-email-1',
        firstName: 'possible-author-profile-given-names-1',
        lastName: 'possible-author-profile-surname-1',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(5, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-new-email-for-2',
        firstName: 'possible-author-profile-given-names-2',
        lastName: 'possible-author-profile-surname-2',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(6, {
      event: 'AuthorConfirmed',
      data: {
        email: 'possible-author-profile-email-3',
        firstName: 'author-new-given-names-for-3',
        lastName: 'possible-author-profile-surname-3',
      },
    })
  })

  it('should set surname from Author models if it is null', async () => {
    jest.spyOn(Author, 'findBy').mockResolvedValueOnce([
      {
        email: 'author-email-1',
        givenNames: 'author-given-names-1',
        surname: 'author-surname-1',
      },
      {
        email: 'author-email-2',
        givenNames: 'author-given-names-2',
        surname: 'author-surname-2',
      },
      {
        email: 'author-email-3',
        givenNames: 'author-given-names-3',
        surname: 'author-surname-3',
      },
    ])

    jest.spyOn(PossibleAuthorProfile, 'findBy').mockResolvedValueOnce([
      {
        email: 'possible-author-profile-email-1',
        givenNames: 'possible-author-profile-given-names-1',
        surname: null,
      },
      {
        email: null,
        givenNames: 'possible-author-profile-given-names-2',
        surname: 'possible-author-profile-surname-2',
      },
      {
        email: 'possible-author-profile-email-3',
        givenNames: null,
        surname: 'possible-author-profile-surname-3',
      },
    ])

    jest
      .spyOn(Author, 'find')
      .mockResolvedValueOnce({
        surname: 'author-new-surname-for-1',
      })
      .mockResolvedValueOnce({
        email: 'author-new-email-for-2',
      })
      .mockResolvedValueOnce({
        givenNames: 'author-new-given-names-for-3',
      })

    jest
      .spyOn(applicationEventBus, 'publishMessage')
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()

    const manuscriptId = chance.guid()

    await publishAuthorConfirmedUseCase
      .initialize({ models, services: { applicationEventBus } })
      .execute(manuscriptId)

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(6)

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(1, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-1',
        firstName: 'author-given-names-1',
        lastName: 'author-surname-1',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(2, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-2',
        firstName: 'author-given-names-2',
        lastName: 'author-surname-2',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(3, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-email-3',
        firstName: 'author-given-names-3',
        lastName: 'author-surname-3',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(4, {
      event: 'AuthorConfirmed',
      data: {
        email: 'possible-author-profile-email-1',
        firstName: 'possible-author-profile-given-names-1',
        lastName: 'author-new-surname-for-1',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(5, {
      event: 'AuthorConfirmed',
      data: {
        email: 'author-new-email-for-2',
        firstName: 'possible-author-profile-given-names-2',
        lastName: 'possible-author-profile-surname-2',
      },
    })

    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(6, {
      event: 'AuthorConfirmed',
      data: {
        email: 'possible-author-profile-email-3',
        firstName: 'author-new-given-names-for-3',
        lastName: 'possible-author-profile-surname-3',
      },
    })
  })
})
