process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()
const { Manuscript, User } = models

const mockUpdateProperties = jest.fn()

const mockSave = jest.fn()
const manuscriptId = chance.guid()
const userId = chance.guid()

const mockManuscript = {
  id: manuscriptId,
  updateProperties: mockUpdateProperties,
  save: mockSave,
}

const references = 'here are the new references'

describe('Update References Use Case', () => {
  beforeEach(() => {
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(true)
  })

  it('should throw error if manuscript is in pending', async () => {
    Manuscript.find = jest.fn().mockReturnValue({
      ...mockManuscript,
      status: Manuscript.Statuses.PAUSED,
    })
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(false)

    const result = useCases.updateReferencesUseCase
      .initialize(models)
      .execute({ manuscriptId, references, userId })

    await expect(result).rejects.toThrow(
      `You cannot add references for manuscript ${manuscriptId} while on ${Manuscript.Statuses.PAUSED} status.`,
    )
  })

  it('should update the references from manuscript', async () => {
    Manuscript.find = jest.fn().mockReturnValue({
      ...mockManuscript,
      status: Manuscript.Statuses.IN_PROGRESS,
    })

    await useCases.updateReferencesUseCase
      .initialize(models)
      .execute({ manuscriptId, references, userId })

    expect(Manuscript.find).toHaveBeenCalledWith(manuscriptId)
    expect(mockUpdateProperties).toHaveBeenCalledWith({ references })
    expect(mockSave).toHaveBeenCalled()
  })
})
