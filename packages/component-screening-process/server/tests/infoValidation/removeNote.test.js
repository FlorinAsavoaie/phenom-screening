process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const chance = new Chance()
const { Note, Manuscript, User } = models

const noteId = chance.guid()
const manuscriptId = chance.guid()
const userId = chance.guid()

const mockDelete = jest.fn()
const mockNote = { delete: mockDelete, type: Note.Types.DECISION_NOTE }

describe('Remove note use case', () => {
  beforeEach(() => {
    Note.find = jest.fn().mockReturnValue(mockNote)
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(true)
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.PAUSED,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(false)

    const result = useCases.removeNoteUseCase
      .initialize(models)
      .execute({ noteId, userId })

    await expect(result).rejects.toThrow(
      `You cannot remove a note of type ${mockNote.type} from manuscript ${manuscriptId} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should successfully remove a note', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await useCases.removeNoteUseCase
      .initialize(models)
      .execute({ noteId, userId })

    expect(Note.find).toHaveBeenCalledWith(noteId)
    expect(mockDelete).toHaveBeenCalled()
  })
})
