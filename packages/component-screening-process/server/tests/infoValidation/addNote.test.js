process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const models = require('@pubsweet/models')
const { factories } = require('component-model')
const useCases = require('../../src/use-cases')
const Chance = require('chance')

const { Manuscript, Note, User } = models
const chance = new Chance()

const manuscriptId = chance.guid()
const userId = chance.guid()
const type = Note.Types.TITLE_NOTE

const params = {
  manuscriptId,
  type,
  userId,
}

const mockSave = jest.fn().mockReturnValue({
  manuscriptId,
  type,
})
const mockNote = {
  manuscriptId: params.manuscriptId,
  type,
  save: mockSave,
}
const createNote = jest
  .spyOn(factories.noteFactory, 'createNote')
  .mockReturnValue(mockNote)

describe('Add note Use Case', () => {
  beforeEach(() => {
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(true)
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.PAUSED,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(false)

    const result = useCases.addNoteUseCase
      .initialize({ models, factories })
      .execute(params)
    await expect(result).rejects.toThrow(
      `You cannot add a note of type ${params.type} for manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
    expect(mockSave).toHaveBeenCalledTimes(0)
  })

  it('should return if a note if already added', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    const findOneBy = jest.spyOn(Note, 'findOneBy').mockReturnValueOnce({
      manuscriptId: params.manuscriptId,
      type: params.type,
    })

    await useCases.addNoteUseCase
      .initialize({ models, factories })
      .execute(params)

    expect(findOneBy).toHaveBeenCalled()
    expect(createNote).not.toHaveBeenCalled()
    expect(mockNote.save).not.toHaveBeenCalled()
  })

  it('should add note', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    const findOneBy = jest
      .spyOn(Note, 'findOneBy')
      .mockReturnValueOnce(undefined)

    const note = await useCases.addNoteUseCase
      .initialize({ models, factories })
      .execute(params)

    expect(findOneBy).toHaveBeenCalled()
    expect(createNote).toHaveBeenCalled()
    expect(createNote).toHaveBeenCalledWith({
      manuscriptId,
      type,
    })
    expect(mockNote.save).toHaveBeenCalled()
    expect(note.manuscriptId).toEqual(manuscriptId)
  })
})
