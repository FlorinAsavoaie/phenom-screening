process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const models = require('@pubsweet/models')
const useCases = require('../../src/use-cases')

const chance = new Chance()
const { Note, Manuscript, User } = models
const noteId = chance.guid()

const noteInput = {
  noteId,
  content: chance.paragraph(),
}
const mockUpdateProperties = jest.fn()
const mockSave = jest.fn()

const mockNote = {
  type: Note.Types.DECISION_NOTE,
  updateProperties: mockUpdateProperties,
  save: mockSave,
}

describe('UpdateNote Use Case', () => {
  beforeEach(() => {
    Note.find = jest.fn().mockReturnValue(mockNote)
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(true)
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptId = chance.guid()
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.PAUSED,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(false)

    const result = useCases.updateNoteUseCase
      .initialize(models)
      .execute(noteInput)

    await expect(result).rejects.toThrow(
      `You cannot write a note of type ${mockNote.type} on manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })

  it('should successfully update the content of a note', async () => {
    const manuscriptId = chance.guid()
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
    }

    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await useCases.updateNoteUseCase.initialize(models).execute(noteInput)

    expect(Note.find).toHaveBeenCalledWith(noteInput.noteId)
    expect(mockUpdateProperties).toHaveBeenCalledWith({
      content: noteInput.content,
    })
    expect(mockSave).toHaveBeenCalled()
  })
})
