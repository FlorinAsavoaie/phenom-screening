const models = require('@pubsweet/models')
const Chance = require('chance')
const useCases = require('../../src/use-cases')

const chance = new Chance()
const { ManuscriptValidations, Manuscript, User } = models

const mockUpdateProperties = jest.fn()
const mockManuscriptValidations = {
  graphical: true,
  updateProperties: mockUpdateProperties,
}
mockManuscriptValidations.save = jest.fn()

const manuscriptId = chance.guid()
const userId = chance.guid()

describe('Toggle manuscript validation checkboxes', () => {
  beforeEach(() => {
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(true)
    ManuscriptValidations.findOneBy = jest
      .fn()
      .mockReturnValue(mockManuscriptValidations)
  })

  it('should throw error if manuscript is in pending', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.PAUSED,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)
    User.canDoActionAccordingToStatus = jest.fn().mockReturnValue(false)

    const result = useCases.toggleValidationUseCase
      .initialize(models)
      .execute({ manuscriptId, field: 'graphical', userId })

    await expect(result).rejects.toThrow(
      `You cannot select or deselect an option for field graphical on manuscript ${manuscriptMock.id} while on ${manuscriptMock.status} status.`,
    )
  })
  it('set the correct value to a validation field', async () => {
    const manuscriptMock = {
      id: manuscriptId,
      status: Manuscript.Statuses.IN_PROGRESS,
    }
    Manuscript.find = jest.fn().mockReturnValue(manuscriptMock)

    await useCases.toggleValidationUseCase
      .initialize(models)
      .execute({ manuscriptId, field: 'graphical', userId })

    expect(ManuscriptValidations.findOneBy).toHaveBeenCalledWith({
      queryObject: { manuscriptId },
    })
    expect(mockUpdateProperties).toHaveBeenCalledWith({ graphical: false })
    expect(mockManuscriptValidations.save).toHaveBeenCalled()
  })
})
