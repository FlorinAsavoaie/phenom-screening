const models = require('@pubsweet/models')
const useCases = require('./../src/use-cases')

describe('Update Orcid Use Case', () => {
  const { Author } = models

  it('should set orcid for author', async () => {
    const updateBy = jest.spyOn(Author, 'updateBy').mockResolvedValueOnce({})

    const user = {
      identities: [
        { type: 'local', email: 'someTestEmail@email' },
        { type: 'orcid', identifier: '123-000-123' },
      ],
    }
    await useCases.updateUserOrcidUseCase.initialize({ Author }).execute(user)

    expect(updateBy).toHaveBeenCalledWith({
      queryObject: { email: 'someTestEmail@email' },
      propertiesToUpdate: { orcid: '123-000-123' },
    })
  })

  it('should set orcid null if author removes it', async () => {
    const updateBy = jest.spyOn(Author, 'updateBy').mockResolvedValueOnce({})

    const user = {
      identities: [{ type: 'local', email: 'someTestEmail@email' }],
    }
    await useCases.updateUserOrcidUseCase.initialize({ Author }).execute(user)

    expect(updateBy).toHaveBeenCalledWith({
      queryObject: { email: 'someTestEmail@email' },
      propertiesToUpdate: { orcid: null },
    })
  })
})
