import { MockedProvider } from '@apollo/client/testing'

import { getManuscriptByCustomId } from 'hindawi-shared/client/graphql/queries'

import {
  render,
  manuscriptMock,
  waitForGraphqlQueryDataResponse,
} from 'hindawi-shared/client/tests'

import ManuscriptInfo from '../components/ManuscriptInfo'

describe('ManuscriptInfo component unit tests', () => {
  it('should display linked article title, review link and customId for manuscripts that are in phenom', async () => {
    const graphqlQueryMocks = [
      {
        request: {
          query: getManuscriptByCustomId,
          variables: {
            manuscriptCustomId: '5527518',
            // !IMPORTANT: make sure the values you provide for variables in the query mock
            // !are the same as values provided from within components mocks,
            // !in this case manuscriptCustomId value here should be the same
            // !as the one from manuscriptMock provided as prop to the <ManuscriptInfo> component
          },
        },
        result: {
          data: {
            manuscript: {
              title: 'FAKE 3RD Manuscript',
              submissionId: 'e865e6c3-09ff-47a3-bfd1-c457d0824c4c',
              phenomId: '1ee35830-f3ad-490b-a979-d2001d64ae4d',
              status: 'screeningApproved',
            },
          },
        },
      },
    ]

    const container = render(
      <MockedProvider addTypename={false} mocks={graphqlQueryMocks}>
        <ManuscriptInfo manuscript={manuscriptMock} />
      </MockedProvider>,
    )

    const { queryByText } = container

    await waitForGraphqlQueryDataResponse()

    expect(queryByText('Linked Article Information')).toBeInTheDocument()
    expect(queryByText('See linked article in Review')).toBeInTheDocument()
    expect(queryByText('Linked Article No')).toBeInTheDocument()
    expect(queryByText('Linked Article Title')).toBeInTheDocument()
    expect(queryByText('Published Link')).not.toBeInTheDocument()
  })

  it('should display an information message for manuscripts that are missing from screening DB', async () => {
    const manuscriptFakeCustomId = '99999999'

    const graphqlQueryMocks = [
      {
        request: {
          query: getManuscriptByCustomId,
          variables: {
            manuscriptCustomId: manuscriptFakeCustomId,
            // !IMPORTANT: make sure the values you provide for variables in the query mock
            // !are the same as values provided from within components mocks,
            // !in this case manuscriptCustomId value here should be the same
            // !as the one from manuscriptMock provided as prop to the <ManuscriptInfo> component
          },
        },
        result: {
          errors: [
            {
              message: 'Failed to find manuscript!',
            },
          ],
        },
      },
    ]

    const container = render(
      <MockedProvider addTypename={false} mocks={graphqlQueryMocks}>
        <ManuscriptInfo
          manuscript={{
            ...manuscriptMock,
            linkedSubmissionCustomId: manuscriptFakeCustomId,
          }}
        />
      </MockedProvider>,
    )

    const { queryByText } = container

    await waitForGraphqlQueryDataResponse()

    const MANUSCRIPT_MISSING_IN_SCREENING_ERROR = `Manuscript details for ${manuscriptFakeCustomId} are unavailable in Phenom Screening. For more information, please search this custom ID in Phenom Review.`

    expect(queryByText('Linked Article Information')).toBeInTheDocument()
    expect(
      queryByText(MANUSCRIPT_MISSING_IN_SCREENING_ERROR),
    ).toBeInTheDocument()
  })
})
