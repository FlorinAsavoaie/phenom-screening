import { screen } from '@testing-library/react'
import { render } from 'hindawi-shared/client/tests'

import AuthorCard from '../components/AuthorIdentityVerification/AuthorCard'

const authorCardProps = {
  author: {
    givenNames: 'John',
    surname: 'Smith',
    email: 'johnsmith@test.test',
    aff: 'Affiliation',
    orcid: '0000-0000-0000-0000',
    isOnBadDebtList: false,
    isOnBlackList: false,
    isOnWatchList: false,
    hasOrcidIssue: false,
  },
}

describe('AuthorCard component unit tests', () => {
  it('should have orcid sandbox href when ORCID_SANDBOX_ENABLED=true', async () => {
    process.env.ORCID_SANDBOX_ENABLED = 'true'

    render(<AuthorCard {...authorCardProps} />)

    const expectedOrcidHref = `https://sandbox.orcid.org/${authorCardProps.author.orcid}`

    const orcidLinkElement = screen.queryByTestId('author-card-orcid-link')

    expect(orcidLinkElement.href).toBe(expectedOrcidHref)
  })

  it('should have orcid production href when ORCID_SANDBOX_ENABLED=false', async () => {
    process.env.ORCID_SANDBOX_ENABLED = 'false'

    render(<AuthorCard {...authorCardProps} />)

    const expectedOrcidHref = `https://orcid.org/${authorCardProps.author.orcid}`

    const orcidLinkElement = screen.queryByTestId('author-card-orcid-link')

    expect(orcidLinkElement.href).toBe(expectedOrcidHref)
  })
})
