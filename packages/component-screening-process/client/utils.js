export const parseError = (e) => e.message.replace('GraphQL error: ', '')
export const parseListWithComma = (list) =>
  list.reduce(
    (result, value, index) =>
      index === 0 ? result + value : `${result}, ${value}`,
    '',
  )
