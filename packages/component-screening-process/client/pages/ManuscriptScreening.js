import { useState } from 'react'
import styled from 'styled-components'

import {
  InfoValidationTab,
  PreScreeningCheckTab,
  AuthorIdentityVerificationTab,
} from 'component-screening-process/client'
import { Tabs } from 'hindawi-shared/client/components'
import { updateSubmissionStatusAndStep } from 'hindawi-shared/client/components/status-and-step-update'

import { ScreeningBottomBar } from '../components'
import { useManuscriptRightTabs } from '../hooks/useManuscriptRightTabs'

function ManuscriptScreening({ manuscript, currentUser }) {
  updateSubmissionStatusAndStep(manuscript)
  const currentUserIsChecker = ['screener', 'qualityChecker'].includes(
    manuscript.currentUserRole,
  )

  const canUserMakeActionsOnManuscript =
    (!currentUserIsChecker && manuscript.status === 'escalated') ||
    manuscript.status === 'inProgress'

  const canUserMakeDecisionOnManuscript = manuscript.status === 'inProgress'

  const [selectedTab, setSelectedTab] = useState(0)
  const handleClick = (index) => setSelectedTab(index)

  const tabs = [
    {
      label: 'Pre-screening Check',
      icon: {
        hasIcon: false,
        name: 'check',
        color: 'statusApproved',
      },
    },
    {
      label: 'Info Validation',
      icon: {
        hasIcon: false,
        name: 'check',
        color: 'statusApproved',
      },
    },
    {
      label: 'Author Identity Verification',
      icon: {
        hasIcon: false,
        name: 'check',
        color: 'statusApproved',
      },
    },
  ]

  const rightTabs = useManuscriptRightTabs(manuscript)

  return (
    <Root>
      <Tabs
        handleClick={handleClick}
        rightTabs={rightTabs}
        selectedTab={selectedTab}
        tabs={tabs}
      >
        <PreScreeningCheckTab
          currentUser={currentUser}
          manuscript={manuscript}
        />
        <InfoValidationTab
          disabled={!canUserMakeActionsOnManuscript}
          manuscript={manuscript}
        />
        <AuthorIdentityVerificationTab
          disabled={!canUserMakeActionsOnManuscript}
          manuscript={manuscript}
        />
      </Tabs>
      <ScreeningBottomBar
        currentUserIsChecker={currentUserIsChecker}
        disabled={!canUserMakeDecisionOnManuscript}
        manuscriptCurrentUserRole={manuscript.currentUserRole}
        manuscriptId={manuscript.id}
        manuscriptStatus={manuscript.status}
      />
    </Root>
  )
}

export default ManuscriptScreening

const Root = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`
