import styled from 'styled-components'
import { get } from 'lodash'
import { useQuery } from '@apollo/client'
import { Loader } from '@hindawi/ui'

import { InfoValidation, PdfViewer, ManuscriptContainer } from '../components'
import { getManuscriptValidations } from '../graphql/queries'

function InfoValidationTab({ manuscript, disabled }) {
  const { data, loading } = useQuery(getManuscriptValidations, {
    variables: { manuscriptId: manuscript.id },
  })
  const validations = get(data, 'getManuscriptValidations')

  if (loading) {
    return <Loader iconSize={4} mx="auto" />
  }
  return (
    <Root id="info-tab">
      <PdfViewer manuscript={manuscript} />
      <InfoValidation
        disabled={disabled}
        manuscript={manuscript}
        validations={validations}
      />
    </Root>
  )
}

export default InfoValidationTab

// #region styles
const Root = styled(ManuscriptContainer)`
  display: grid;
  grid-template-columns: 70% 30%;
`
// #endregion
