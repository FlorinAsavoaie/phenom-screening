import { get } from 'lodash'
import { Loader } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useQuery } from '@apollo/client'

import { getManuscriptChecks } from '../graphql/queries'
import {
  LanguageAndWordcount,
  SuspiciousWords,
  IThenticateScore,
  SubmissionSimilarity,
} from '../components'

function PreScreeningCheckTab({ manuscript, currentUser }) {
  const { data, loading } = useQuery(getManuscriptChecks, {
    variables: { manuscriptId: manuscript.id },
  })
  const languagePercentage =
    get(data, 'getManuscriptChecks.languagePercentage') || 0
  const wordCount = get(data, 'getManuscriptChecks.wordCount') || 0
  const suspiciousWords = get(data, 'getManuscriptChecks.suspiciousWords')
  const totalSimilarityPercentage =
    get(data, 'getManuscriptChecks.totalSimilarityPercentage') || 0
  const firstSourceSimilarityPercentage =
    get(data, 'getManuscriptChecks.firstSourceSimilarityPercentage') || 0
  const reportUrl = get(data, 'getManuscriptChecks.reportUrl')

  if (loading) {
    return <Loader iconSize={4} mx="auto" />
  }

  return (
    <Root>
      <Container>
        <LanguageAndWordcount
          languagePercentage={languagePercentage}
          wordCount={wordCount}
        />
        <SuspiciousWords suspiciousWords={suspiciousWords} />
        <IThenticateScore
          firstSourceSimilarityPercentage={firstSourceSimilarityPercentage}
          reportUrl={reportUrl}
          totalSimilarityPercentage={totalSimilarityPercentage}
        />
        <SubmissionSimilarity
          currentUser={currentUser}
          manuscriptId={manuscript.id}
        />
      </Container>
    </Root>
  )
}

export default PreScreeningCheckTab

// #region styles

const Root = styled.div`
  overflow: scroll;
  overflow-x: hidden;
  height: 100%;
`

const Container = styled.div`
  border-radius: 6px;
  box-shadow: 3px 3px 6px 0 rgba(141, 141, 141, 0.2);
  background-color: ${th('white')};
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: fit-content;
  margin: calc(${th('gridUnit')} * 2);
  padding: calc(${th('gridUnit')} * 4);
`
// #endregion
