import { useState, useEffect } from 'react'
import { get } from 'lodash'
import { useQuery } from '@apollo/client'

import { getPossibleAuthorProfiles } from '../graphql/queries'
import {
  AuthorsSidebar,
  AuthorsVerification,
  ManuscriptContainer,
} from '../components'

function AuthorIdentityVerificationTab({ manuscript, disabled }) {
  const [selectedAuthor, setSelectedAuthor] = useState({
    ...manuscript.authors[0],
    authorNumber: 1,
  })

  useEffect(() => {
    setSelectedAuthor({
      ...selectedAuthor,
      ...manuscript.authors.find((a) => a.id === selectedAuthor.id),
    })
  }, [manuscript])

  const { loading, data } = useQuery(getPossibleAuthorProfiles, {
    variables: { authorId: selectedAuthor.id, manuscriptId: manuscript.id },
  })
  const possibleAuthorProfiles = get(data, 'getPossibleAuthorProfiles', [])
  return (
    <ManuscriptContainer>
      <AuthorsSidebar
        authors={manuscript.authors}
        selectedAuthor={selectedAuthor}
        setSelectedAuthor={setSelectedAuthor}
      />
      <AuthorsVerification
        disabled={disabled}
        loading={loading}
        manuscriptId={manuscript.id}
        possibleAuthorProfiles={possibleAuthorProfiles}
        selectedAuthor={selectedAuthor}
      />
    </ManuscriptContainer>
  )
}

export default AuthorIdentityVerificationTab
