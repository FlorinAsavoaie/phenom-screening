import { ReactElement } from 'react'
import { DateParser, H3 } from '@pubsweet/ui'
import {
  Label,
  Text,
  Row,
  Item,
  AuthorTagList,
  Icon,
  ActionLink,
} from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { parseAuthors } from 'hindawi-shared/client/parseAuthors'
import { ManuscriptFilesDownloader } from 'hindawi-shared/client/components'
import { useManuscriptByCustomId } from 'hindawi-shared/client/graphql/hooks'
import { Manuscript } from 'hindawi-shared/SchemaInterfaces'

interface ManuscriptInfoProps {
  manuscript: Manuscript
}

const ManuscriptInfo: React.FC<ManuscriptInfoProps> = ({ manuscript }) => {
  const {
    authors,
    title,
    customId,
    articleType,
    submissionDate,
    submissionId,
    phenomId,
    currentUserRole,
    status,
    journal: { name: journalName },
    linkedSubmissionCustomId,
  } = manuscript

  const manuscriptParsedAuthors = parseAuthors(authors)
  const userIsAdmin = currentUserRole === 'admin'

  let reviewUrl = `${process.env.REVIEW_URL}/details/${submissionId}/${phenomId}`
  if (status === 'returnedToDraft') {
    reviewUrl = `${process.env.REVIEW_URL}/submit/${submissionId}/${phenomId}`
  }

  return (
    <Root>
      <Row justify="flex-start">
        <H3>Manuscript Information</H3>
        {userIsAdmin && (
          <Item ml={2}>
            <ActionLinkStyled to={reviewUrl}>
              See manuscript in Review
              <IconLink icon="link" ml={1} />
            </ActionLinkStyled>
          </Item>
        )}
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Manuscript No</Label>
        </Item>
        <Item flex={6} ml={2}>
          <Text>ID {customId}</Text>
        </Item>
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Manuscript Title</Label>
        </Item>
        <Item flex={6} ml={2}>
          <Text>{title}</Text>
        </Item>
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Submitted On</Label>
        </Item>
        <Item flex={6} ml={2}>
          <DateParser humanizeThreshold={0} timestamp={submissionDate}>
            {(timestamp: string, timeAgo: string): ReactElement => (
              <Text>{`${timestamp} (${timeAgo} ago)`}</Text>
            )}
          </DateParser>
        </Item>
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Journal</Label>
        </Item>
        <Item flex={6} ml={2}>
          <Text>{journalName}</Text>
        </Item>
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Issue & Type</Label>
        </Item>
        <Item flex={6} ml={2}>
          <Text>{articleType}</Text>
        </Item>
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Invited Contribution</Label>
        </Item>
        <Item flex={6} ml={2}>
          <Text>No</Text>
        </Item>
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Authors</Label>
        </Item>
        <Item flex={6} ml={2}>
          <AuthorTagList authors={manuscriptParsedAuthors} withAffiliations />
        </Item>
      </Row>

      <Row mt={3}>
        <Item justify="flex-end" mr={2}>
          <Label>Files</Label>
        </Item>
        <Item flex={6} ml={2}>
          <ManuscriptFilesDownloader manuscriptId={manuscript.id} />
        </Item>
      </Row>

      {linkedSubmissionCustomId && (
        <LinkedManuscriptInfo
          currentUserRole={currentUserRole}
          linkedManuscriptCustomId={linkedSubmissionCustomId}
        />
      )}
    </Root>
  )
}

const LinkedManuscriptInfo = ({
  linkedManuscriptCustomId,
  currentUserRole,
}: {
  linkedManuscriptCustomId: string
  currentUserRole: string
}) => {
  const { manuscriptData, loading, error } = useManuscriptByCustomId(
    linkedManuscriptCustomId,
  )

  if (loading) return <Text>Linked manuscript information loading...</Text>

  if (!manuscriptData || error) {
    const MANUSCRIPT_MISSING_IN_SCREENING_INFO = `Manuscript details for ${linkedManuscriptCustomId} are unavailable in Phenom Screening. For more information, please search this custom ID in Phenom Review.`

    return (
      <>
        <Row justify="flex-start" mt={3}>
          <H3>Linked Article Information</H3>
        </Row>

        <Row justify="flex-start" mt={3}>
          <Text>{MANUSCRIPT_MISSING_IN_SCREENING_INFO}</Text>
        </Row>
      </>
    )
  }

  const { title, phenomId, submissionId, status } = manuscriptData

  let linkedArticleReviewUrl = `${process.env.REVIEW_URL}/details/${submissionId}/${phenomId}`
  if (status === 'returnedToDraft') {
    linkedArticleReviewUrl = `${process.env.REVIEW_URL}/submit/${submissionId}/${phenomId}`
  }

  const userIsAdmin = currentUserRole === 'admin'

  return (
    <>
      <Row justify="flex-start" mt={3}>
        <H3>Linked Article Information</H3>

        {userIsAdmin && manuscriptData && (
          <Item ml={2}>
            <ActionLinkStyled to={linkedArticleReviewUrl}>
              See linked article in Review
              <IconLink icon="link" ml={1} />
            </ActionLinkStyled>
          </Item>
        )}
      </Row>

      {manuscriptData && (
        <>
          <Row mt={3}>
            <Item justify="flex-end" mr={2}>
              <Label>Linked Article No</Label>
            </Item>
            <Item flex={6} ml={2}>
              <Text>ID {linkedManuscriptCustomId}</Text>
            </Item>
          </Row>

          <Row alignItems="flex-start" mt={3}>
            <Item justify="flex-end" mr={2}>
              <Label>Linked Article Title</Label>
            </Item>
            <Item flex={6} ml={2}>
              <Text>{title}</Text>
            </Item>
          </Row>
        </>
      )}
    </>
  )
}

// #region styles
const Root = styled.div`
  background-color: ${th('white')};
  border-bottom: 1px solid ${th('furnitureColor')};
  font-family: ${th('defaultFont')};
  padding: calc(${th('gridUnit')} * 3) calc(${th('gridUnit')} * 6);
`

const ActionLinkStyled = styled(ActionLink)`
  font-family: ${th('defaultFont')};
  color: ${th('action.color')};
  cursor: pointer;
  font-weight: 600;
  text-decoration: none;

  &:hover {
    color: ${th('textSecondaryColor')};
    span {
      color: ${th('textSecondaryColor')};
    }
  }

  &:focus {
    color: ${th('action.colorActive')};
  }
`

const IconLink = styled(Icon)`
  color: ${th('action.color')};
`
// #endregion

export default ManuscriptInfo
