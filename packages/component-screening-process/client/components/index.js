export * from './StyledComponents'
export * from './screeningDecisions'

export { default as InfoValidation } from './InfoValidation'
export { default as PdfViewer } from './pdfViewer/PdfViewer'
export { default as ManuscriptInfo } from './ManuscriptInfo'
export { default as Note } from './Note'

export { default as LanguageAndWordcount } from './preScreeningCheck/LanguageAndWordcount'
export { SuspiciousWords } from './preScreeningCheck/SuspiciousWords/SuspiciousWords'
export { default as IThenticateScore } from './preScreeningCheck/ithenticate-score/IThenticateScore'

export { default as AuthorsSidebar } from './AuthorIdentityVerification/AuthorsSidebar'

export { default as AuthorsVerification } from './AuthorIdentityVerification/AuthorVerification'

export { default as AuthorsVerificationBar } from './AuthorIdentityVerification/AuthorsVerificationBar'

export { default as AuthorsVerificationTable } from './AuthorIdentityVerification/AuthorsVerificationTable'

export { default as AuthorCard } from './AuthorIdentityVerification/AuthorCard'

export { SubmissionSimilarity } from './preScreeningCheck/SubmissionSimilarity/SubmissionSimilarity'
