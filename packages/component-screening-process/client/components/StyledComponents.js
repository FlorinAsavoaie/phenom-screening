import styled from 'styled-components'

export const ManuscriptContainer = styled.div`
  display: flex;
  flex: 1;
  min-height: 0;
`
