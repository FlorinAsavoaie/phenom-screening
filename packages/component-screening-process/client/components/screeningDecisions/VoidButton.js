import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'
import { Button as BaseButton, ErrorText } from '@pubsweet/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { useHistory } from 'react-router-dom'
import { validators } from '@hindawi/ui'
import { useDecisionModal, Icon } from 'hindawi-shared/client/components'

export function VoidButton({ disabled, onConfirm, ...buttonProps }) {
  const history = useHistory()

  const { showModal, hideModal } = useDecisionModal({
    onConfirm: ({ additionalComments }, { setFetching, setError }) => {
      setFetching(true)

      onConfirm({ additionalComments }).then(
        () => {
          setFetching(false)
          hideModal()
          history.replace('/manuscripts')
        },
        (err) => {
          const error = <ErrorText>{parseGQLError(err)}</ErrorText>
          setFetching(false)
          setError(error)
        },
      )
    },
    additionalCommentTitle: 'Reason for voiding manuscript',
    confirmText: 'Are you sure you want to void this manuscript?',
    confirmButtonText: 'VOID MANUSCRIPT',
    cancelButtonText: 'CANCEL',
    modalTitle: 'Void Manuscript',
    validate: [validators.required],
  })

  return (
    <Button
      disabled={disabled}
      onClick={showModal}
      secondary
      small
      {...buttonProps}
    >
      <Icon color="contrastGrayColor" mr={1} name="remove" size={4} />
      Void Manuscript
    </Button>
  )
}

const Button = styled(BaseButton)`
  min-width: unset;
  font-size: ${th('fontSizeBase')};
`
