import { Row } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { UnpauseButton, VoidButton, EscalateButton } from './index'
import { useVoidManuscript } from '../../hooks'
import { useUnpauseManuscript } from '../../hooks/useUnpauseDecision'

export function ScreeningLeftSideBottomBar({
  disabled,
  manuscriptId,
  manuscriptStatus,
  currentUserIsChecker,
  manuscriptIsEscalated,
}) {
  const manuscriptIsPaused = manuscriptStatus === 'paused'
  const { unpauseManuscript } = useUnpauseManuscript()
  const { voidManuscript } = useVoidManuscript()

  return (
    <Row justify="flex-start" ml={6}>
      {manuscriptIsPaused && (
        <UnpauseButton
          manuscriptId={manuscriptId}
          mr={4}
          onConfirm={() => unpauseManuscript({ manuscriptId })}
        />
      )}
      {!manuscriptIsPaused && (
        <EscalateButton
          currentUserIsChecker={currentUserIsChecker}
          disabled={disabled || manuscriptIsEscalated}
          mr={4}
          onConfirm={({ additionalComments, selectedEscalatedOption }) =>
            selectedEscalatedOption.executeEscalationAction({
              manuscriptId,
              additionalComments,
            })
          }
        />
      )}
      <Delimiter />
      <VoidButton
        disabled={disabled}
        onConfirm={({ additionalComments }) =>
          voidManuscript({ manuscriptId, additionalComments })
        }
      />
    </Row>
  )
}
const Delimiter = styled.div`
  padding-top: calc(${th('gridUnit')} * 1);
  height: calc(${th('gridUnit')} * 10);
  border-left: 1px solid ${th('furnitureColor')};
  margin-right: calc(${th('gridUnit')} * 4);
`
