import { usePauseManuscript } from '../../../hooks/usePauseDecision'
import { useEscalateManuscript } from '../../../hooks/useEscalateDecision'

interface EscalationOption {
  value: string
  confirmButtonText: string
  executeEscalationAction: () => any
  label?: string
}
interface EscalationsOptions {
  [key: string]: EscalationOption
}

export const useEscalationOptions = (): EscalationsOptions => {
  const { pauseManuscript } = usePauseManuscript()
  const { escalateManuscript } = useEscalateManuscript()

  return {
    pause: {
      value: 'pause',
      confirmButtonText: 'PAUSE',
      executeEscalationAction: pauseManuscript,
      label: 'Pause manuscript',
    },
    sendToTeamLead: {
      value: 'sendToTeamLead',
      confirmButtonText: 'SEND TO TEAM LEAD',
      executeEscalationAction: escalateManuscript,
      label: 'Send to Team Lead',
    },
    default: {
      value: '',
      confirmButtonText: 'SUBMIT',
      executeEscalationAction: (): any => ({}),
    },
  }
}
