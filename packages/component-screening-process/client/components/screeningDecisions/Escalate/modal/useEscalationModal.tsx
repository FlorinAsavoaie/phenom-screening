import { useModal } from '@pubsweet/ui'
import { EscalationModal } from './EscalationModal'

interface EscalationHookProps {
  onConfirm: (
    _: unknown,
    {
      setFetching,
      setError,
    }: {
      setFetching: (isFetching: boolean) => void
      setError: (error: unknown) => void
    },
  ) => void
  additionalCommentTitle?: string
  confirmButtonText: string
  cancelButtonText: string
  modalTitle: string
}

interface EscalationHookResult {
  showModal: () => void
  hideModal: () => void
}

export function useEscalationModal(
  props: EscalationHookProps,
): EscalationHookResult {
  const { showModal, hideModal } = useModal({
    component: () => <EscalationModal {...props} hideModal={hideModal} />,
  })

  return {
    showModal,
    hideModal,
  }
}
