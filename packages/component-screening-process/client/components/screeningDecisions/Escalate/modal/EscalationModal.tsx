import { useState } from 'react'
import {
  FormModal,
  Item,
  ValidatedFormField,
  Textarea,
  Label,
} from '@hindawi/ui'
import { useEscalationOptions } from 'component-screening-process/client/components/screeningDecisions/Escalate/useEscalationOptions'
import { RadioGroup, ErrorField } from 'hindawi-shared/client/components'

interface OnConfirmModalProps {
  setFetching: (isFetching: boolean) => void
  setError: (error: unknown) => void
}
interface EscalationModalProps {
  onConfirm: (
    _: unknown,
    { setFetching, setError }: OnConfirmModalProps,
  ) => void
  additionalCommentTitle?: string
  confirmText?: string
  cancelButtonText: string
  modalTitle: string
  hideModal: () => void
}

interface OnConfirmProps {
  additionalComments: string
}

function validateEscalateForm(values: any): any {
  interface Errors {
    [key: string]: string
  }

  const errors: Errors = {}

  if (values.additionalComments === '') {
    errors.emptyAdditionalComments = 'Please add comments detailing your issue'
  }

  if (values.escalationOption === '') {
    errors.emptyEscalationOption = 'Please select an escalation type'
  }

  return errors
}
const options = [
  {
    value: 'pause',
    label: 'Pause manuscript',
  },
  {
    value: 'sendToTeamLead',
    label: 'Send to Team Lead',
  },
]

const EscalationModal: React.FC<EscalationModalProps> = ({
  onConfirm,
  additionalCommentTitle,
  cancelButtonText,
  modalTitle,
  hideModal,
}) => {
  const escalationOptions = useEscalationOptions()
  const [selectedEscalatedOption, setSelectedEscalatedOption] = useState(
    escalationOptions.default,
  )

  return (
    <FormModal
      cancelText={cancelButtonText}
      confirmText={selectedEscalatedOption.confirmButtonText}
      content={({ formProps: { errors, submitCount } }: any) => (
        <>
          <Item mt={8} vertical>
            <Label fontSize={14} fontWeight={700} mb={3} required>
              Escalation Type
            </Label>
            <Item>
              <ValidatedFormField
                component={RadioGroup}
                minHeight={35}
                name="escalationOption"
                onChange={(event: any): void => {
                  setSelectedEscalatedOption(
                    escalationOptions[event.target.value],
                  )
                }}
                options={options}
              />
            </Item>
            <ErrorField
              errorText={errors.emptyEscalationOption}
              visible={!!errors.emptyEscalationOption && submitCount !== 0}
            />
          </Item>
          <Item alignItems="flex-start" vertical>
            <Label fontSize={14} fontWeight={700} mb={1} required>
              {additionalCommentTitle}
            </Label>
            <Item>
              <ValidatedFormField
                component={Textarea}
                minHeight={35}
                name="additionalComments"
              />
            </Item>
            <ErrorField
              errorText={errors.emptyAdditionalComments}
              visible={!!errors.emptyAdditionalComments && submitCount !== 0}
            />
          </Item>
        </>
      )}
      hideModal={hideModal}
      initialValues={{ additionalComments: '', escalationOption: '' }}
      isCurrentUser
      onSubmit={(
        { additionalComments }: OnConfirmProps,
        { setFetching, setError }: OnConfirmModalProps,
      ): any =>
        onConfirm(
          { additionalComments, selectedEscalatedOption },
          { setFetching, setError },
        )
      }
      title={modalTitle}
      validate={validateEscalateForm}
    />
  )
}

export { EscalationModal }
