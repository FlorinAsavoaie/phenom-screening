import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import { th } from '@pubsweet/ui-toolkit'
import { Button as BaseButton, ErrorText } from '@pubsweet/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { Icon } from 'hindawi-shared/client/components'
import {
  useSubmissionStatusAndStepUpdatedModal,
  StatusChangeTitle,
  StatusChangeSubtitle,
} from 'hindawi-shared/client/components/status-and-step-update'
import { useEscalationModal } from './modal/useEscalationModal'

export function EscalateButton({
  disabled,
  onConfirm,
  selectedEscalatedOption,
  setSelectedEscalatedOption,
  currentUserIsChecker,
  ...buttonProps
}) {
  const history = useHistory()

  const { showModal: showSecondModal, hideModal: hideSecondModal } =
    useSubmissionStatusAndStepUpdatedModal()

  const { showModal, hideModal } = useEscalationModal({
    onConfirm: (
      { additionalComments, selectedEscalatedOption },
      { setFetching, setError },
    ) => {
      setFetching(true)
      onConfirm({ additionalComments, selectedEscalatedOption }).then(
        () => {
          setFetching(false)
          hideModal()
          showConfirmationModal({
            currentUserIsChecker,
            history,
            showSecondModal,
            hideSecondModal,
            selectedEscalatedOption,
          })
        },
        (err) => {
          const error = <ErrorText>{parseGQLError(err)}</ErrorText>
          setFetching(false)
          setError(error)
        },
      )
    },
    additionalCommentTitle: 'Your Comments',
    confirmButtonText: 'SEND TO TEAM LEAD',
    cancelButtonText: 'CANCEL',
    modalTitle: 'Escalate Manuscript',
  })

  return (
    <Button
      disabled={disabled}
      onClick={showModal}
      secondary
      small
      {...buttonProps}
    >
      <Icon color="contrastGrayColor" mr={1} name="pause" size={4} />
      Escalate
    </Button>
  )
}

const Button = styled(BaseButton)`
  min-width: unset;
  font-size: ${th('fontSizeBase')};
`

function showConfirmationModal({
  currentUserIsChecker,
  history,
  showSecondModal,
  hideSecondModal,
  selectedEscalatedOption,
}) {
  showSecondModal({
    onConfirm: () => {
      currentUserIsChecker ? history.push('/manuscripts') : hideSecondModal()
    },
    title:
      selectedEscalatedOption.value === 'pause'
        ? StatusChangeTitle.paused
        : StatusChangeTitle.escalated,
    subtitle:
      selectedEscalatedOption.value === 'pause'
        ? StatusChangeSubtitle.paused
        : StatusChangeSubtitle.escalated,
    confirmText: getConfirmText(currentUserIsChecker),
  })
}

function getConfirmText(currentUserIsChecker) {
  return currentUserIsChecker ? 'RETURN TO DASHBOARD' : 'RETURN TO DETAILS'
}
