import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'
import { Button as BaseButton, ErrorText } from '@pubsweet/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'

import { useHistory } from 'react-router-dom'

import { useDecisionModal } from 'hindawi-shared/client/components'

export function ApproveScreeningButton({
  disabled,
  onConfirm,
  ...buttonProps
}) {
  const history = useHistory()

  const { showModal, hideModal } = useDecisionModal({
    onConfirm: ({ additionalComments }, { setFetching, setError }) => {
      setFetching(true)

      onConfirm({ additionalComments }).then(
        () => {
          setFetching(false)
          hideModal()
          history.replace('/manuscripts')
        },
        (err) => {
          const error = <ErrorText>{parseGQLError(err)}</ErrorText>
          setFetching(false)
          setError(error)
        },
      )
    },
    additionalCommentTitle: 'Additional Comments',
    confirmText: 'Are you sure? This decision is final.',
    confirmButtonText: 'APPROVE',
    cancelButtonText: 'CANCEL',
    modalTitle: 'Approve Manuscript',
  })

  return (
    <Button
      disabled={disabled}
      onClick={showModal}
      primary
      small
      {...buttonProps}
    >
      Approve Manuscript
    </Button>
  )
}

const Button = styled(BaseButton)`
  min-width: unset;
  font-size: ${th('fontSizeBase')};
`
