import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'
import { ScreeningRightSideBottomBar } from './ScreeningRightSideBottomBar'
import { ScreeningLeftSideBottomBar } from './ScreeningLeftSideBottomBar'

export function ScreeningBottomBar({
  disabled,
  manuscriptId,
  manuscriptStatus,
  manuscriptCurrentUserRole,
  currentUserIsChecker,
}) {
  const manuscriptIsEscalated = manuscriptStatus === 'escalated'
  return (
    <Root>
      <ScreeningLeftSideBottomBar
        currentUserIsChecker={currentUserIsChecker}
        disabled={disabled}
        manuscriptId={manuscriptId}
        manuscriptIsEscalated={manuscriptIsEscalated}
        manuscriptStatus={manuscriptStatus}
      />
      <ScreeningRightSideBottomBar
        disabled={disabled}
        manuscriptCurrentUserRole={manuscriptCurrentUserRole}
        manuscriptId={manuscriptId}
        manuscriptIsEscalated={manuscriptIsEscalated}
        manuscriptStatus={manuscriptStatus}
      />
    </Root>
  )
}

export default ScreeningBottomBar

const Root = styled.div`
  align-items: center;
  background-color: ${th('white')};
  border: solid 1px #dbdbdb;
  box-shadow: 0 2px 3px 0 rgba(25, 102, 141, 0.19);
  display: flex;
  height: calc(${th('gridUnit')}* 16);
`
