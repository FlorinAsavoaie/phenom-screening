import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { Button as BaseButton, ErrorText } from '@pubsweet/ui'
import { useDecisionModal, Icon } from 'hindawi-shared/client/components'
import { useLatestPauseReason } from '../../graphql/hooks'

export function UnpauseButton({ manuscriptId, onConfirm, ...buttonProps }) {
  const { showModal, hideModal } = useDecisionModal({
    onConfirm: ({ additionalComments }, { setFetching, setError }) => {
      setFetching(true)

      onConfirm({ additionalComments }).then(
        () => {
          setFetching(false)
          hideModal()
        },
        (err) => {
          const error = <ErrorText>{parseGQLError(err)}</ErrorText>
          setFetching(false)
          setError(error)
        },
      )
    },
    additionalCommentTitle: 'Reason for pausing manuscript',
    confirmText: 'Are you sure you want to unpause this manuscript?',
    confirmButtonText: 'YES',
    cancelButtonText: 'NO',
    modalTitle: 'Unpause Manuscript',
    hasTextarea: false,
    additionalComment: get(
      useLatestPauseReason(manuscriptId),
      'latestPauseReason',
    ),
  })

  return (
    <Button onClick={showModal} secondary small {...buttonProps}>
      <Icon color="contrastGrayColor" mr={1} name="play" size={4} />
      Unpause
    </Button>
  )
}

const Button = styled(BaseButton)`
  min-width: unset;
  font-size: ${th('fontSizeBase')};
`
