import { Row } from '@hindawi/ui'
import {
  ApproveScreeningButton,
  ReturnToDraftButton,
  RefuseToConsiderButton,
  SendBackToCheckerButton,
} from './index'

import {
  useApproveScreening,
  useReturnToDraft,
  useRefuseToConsiderScreening,
} from '../../hooks'

export function ScreeningRightSideBottomBar({
  disabled,
  manuscriptId,
  manuscriptCurrentUserRole,
  manuscriptIsEscalated,
}) {
  const { approveScreening } = useApproveScreening()
  const { returnToDraft } = useReturnToDraft()
  const { refuseToConsiderScreening } = useRefuseToConsiderScreening()
  const userIsTeamLeadOrAdmin =
    manuscriptCurrentUserRole === 'teamLeader' ||
    manuscriptCurrentUserRole === 'admin'
  return (
    <Row justify="flex-end">
      <ReturnToDraftButton
        disabled={disabled}
        mr={4}
        onConfirm={({ additionalComments }) =>
          returnToDraft({ manuscriptId, additionalComments })
        }
      />

      <RefuseToConsiderButton
        disabled={disabled}
        mr={4}
        onConfirm={({ additionalComments, ...rest }) =>
          refuseToConsiderScreening({
            manuscriptId,
            additionalComments,
            rest,
          })
        }
      />

      {!manuscriptIsEscalated && (
        <ApproveScreeningButton
          disabled={disabled}
          mr={6}
          onConfirm={({ additionalComments }) =>
            approveScreening({ manuscriptId, additionalComments })
          }
        />
      )}

      {manuscriptIsEscalated && userIsTeamLeadOrAdmin && (
        <SendBackToCheckerButton
          manuscriptId={manuscriptId}
          mr={6}
          type="SCREENER"
        />
      )}
    </Row>
  )
}
