import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'
import { Button as BaseButton, ErrorText } from '@pubsweet/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { useHistory } from 'react-router-dom'
import { ValidatedCheckboxField, Row, Item, Text } from '@hindawi/ui'

import { useDecisionModal } from 'hindawi-shared/client/components'

export function RefuseToConsiderButton({
  disabled,
  onConfirm,
  ...buttonProps
}) {
  const history = useHistory()

  const { showModal, hideModal } = useDecisionModal({
    onConfirm: ({ additionalComments, ...rest }, { setFetching, setError }) => {
      setFetching(true)

      onConfirm({ additionalComments, ...rest }).then(
        () => {
          setFetching(false)
          hideModal()
          history.replace('/manuscripts')
        },
        (err) => {
          const error = <ErrorText>{parseGQLError(err)}</ErrorText>
          setFetching(false)
          setError(error)
        },
      )
    },
    additionalCommentTitle: 'Additional Comments',
    confirmText: 'Are you sure? This decision is final.',
    confirmButtonText: 'REFUSE TO CONSIDER',
    cancelButtonText: 'CANCEL',
    modalTitle: 'Refuse to Consider Manuscript',
    additionalContent: AdditionalContent,
  })

  return (
    <Button
      disabled={disabled}
      onClick={showModal}
      secondary
      small
      {...buttonProps}
    >
      Refuse to Consider
    </Button>
  )
}

const Button = styled(BaseButton)`
  min-width: unset;
  font-size: ${th('fontSizeBase')};
`

const AdditionalContent = () => (
  <>
    <Row justify="flex-start" mb={1} mt={9}>
      <StyledText fontWeight={700}>Select the reasons for refusal</StyledText>
    </Row>

    <Row justify="flex-start" my={2}>
      <Item flex={1}>
        <ValidatedCheckboxField
          label="Language test failed"
          name="languageCheck"
        />
      </Item>

      <Item flex={1}>
        <ValidatedCheckboxField
          label="Wordcount test failed"
          name="wordCount"
        />
      </Item>
    </Row>

    <Row justify="flex-start" my={2}>
      <Item flex={1}>
        <ValidatedCheckboxField
          label="Suspicious keywords"
          name="suspiciousKeywords"
        />
      </Item>

      <Item flex={1}>
        <ValidatedCheckboxField label="iThenticate failed" name="iThenticate" />
      </Item>
    </Row>
  </>
)

const StyledText = styled(Text)`
  font-size: ${({ fontSize }) => (fontSize ? `${fontSize}px` : '16px')};
`
