import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'
import { Button as BaseButton, ErrorText } from '@pubsweet/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { useHistory } from 'react-router-dom'
import { validators } from '@hindawi/ui'
import { useDecisionModal } from 'hindawi-shared/client/components'
import { useReturnToChecker } from 'component-screening-process/client/hooks'

const confirmButtonTextMapping = {
  CHECKER: 'RETURN TO CHECKER',
  SCREENER: 'RETURN TO SCREENER',
}
const modalTitleMapping = {
  CHECKER: 'Return manuscript to Checker',
  SCREENER: 'Return manuscript to Screener',
}
const buttonTextMapping = {
  CHECKER: 'Return to Checker',
  SCREENER: 'Return to Screener',
}

export function SendBackToCheckerButton({
  manuscriptId,
  type,
  ...buttonProps
}) {
  const history = useHistory()
  const { returnToChecker } = useReturnToChecker()
  const { showModal, hideModal } = useDecisionModal({
    onConfirm: ({ additionalComments }, { setFetching, setError }) => {
      setFetching(true)

      returnToChecker({ manuscriptId, additionalComments }).then(
        () => {
          setFetching(false)
          hideModal()
          history.replace('/manuscripts')
        },
        (err) => {
          const error = <ErrorText>{parseGQLError(err)}</ErrorText>
          setFetching(false)
          setError(error)
        },
      )
    },
    additionalCommentTitle: 'Your Comments',
    confirmButtonText: confirmButtonTextMapping[type],
    cancelButtonText: 'CANCEL',
    modalTitle: modalTitleMapping[type],
    placeholder: 'Please provide details',
    validate: [validators.required],
  })

  return (
    <Button mr={6} onClick={showModal} primary small {...buttonProps}>
      {buttonTextMapping[type]}
    </Button>
  )
}

const Button = styled(BaseButton)`
  min-width: unset;
  font-size: ${th('fontSizeBase')};
  white-space: nowrap;
`
