import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { Text, Loader, useFetching } from '@hindawi/ui'
import { parseError } from 'component-screening-process/client'
import { useSignedUrl } from 'component-screening-process/client/hooks/useSignedUrl'

const PDFViewer = ({ manuscript, pdfFile }) => {
  const [url, setUrl] = useState(null)
  const { isFetching, setFetching, setError, fetchingError } = useFetching()
  const { getSignedUrl } = useSignedUrl()
  const { fileId } = manuscript

  useEffect(() => {
    setFetching(true)
    getSignedUrl({ fileId, format: 'converted' })
      .then((file) => setUrl(file))
      .then(() => setFetching(false))
      .catch(setError)
  }, [])

  if (fetchingError) {
    return (
      <Root>
        <Text error mt={2}>
          {parseError(fetchingError)}
        </Text>
      </Root>
    )
  }

  return (
    <Root>
      {isFetching ? (
        <Loader iconSize={4} />
      ) : (
        <StyledIFrame frameBorder="0" src={`${url}`} title={url} width="100%" />
      )}
    </Root>
  )
}

export default PDFViewer

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
`

const StyledIFrame = styled.iframe`
  flex-grow: 1;
  margin: 0;
`
// #endregion
