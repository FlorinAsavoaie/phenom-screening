import { useCallback } from 'react'
import { get } from 'lodash'
import styled, { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useQuery } from '@apollo/client'
import { useNote } from 'component-screening-process/client'
import {
  Icon,
  Row,
  Label,
  Textarea,
  ActionLink,
  Loader,
  Text,
} from '@hindawi/ui'
import { ErrorField } from 'hindawi-shared/client/components'

import { getNote } from '../graphql/queries'

function Note({ manuscriptId, type, noTopBorder, disabled }) {
  const { addNote, removeNote, updateNote, removeNoteError, updateNoteError } =
    useNote()
  const { data, loading } = useQuery(getNote, {
    variables: {
      manuscriptId,
      type,
    },
  })

  const note = get(data, 'getNote')
  const handleAddNote = useCallback(
    () => addNote({ manuscriptId, type }),
    [manuscriptId, type],
  )

  const handleUpdateNote = useCallback(
    (event) =>
      updateNote({
        manuscriptId,
        type,
        noteId: note.id,
        content: event.target.value,
      }),
    [manuscriptId, type, note],
  )

  const handleRemoveNote = useCallback(
    () => removeNote({ manuscriptId, type, noteId: note.id }),
    [manuscriptId, type, note],
  )

  if (loading) {
    return <Loader iconSize={4} mt={4} mx="auto" />
  }

  if (note)
    return (
      <Root noTopBorder={noTopBorder}>
        <Row justify="space-between" mb={1} mt={4}>
          <Label>Your Observations</Label>
          <ActionLink
            alignItems="center"
            data-test-id="form-report-remove-note"
            disabled={disabled}
            onClick={handleRemoveNote}
          >
            <RemoveWrapper>
              <Icon bold fontSize="14px" icon="remove1" />
              Remove Note
            </RemoveWrapper>
          </ActionLink>
        </Row>
        <Textarea
          defaultValue={note.content}
          disabled={disabled}
          onChange={handleUpdateNote}
        />
        <ErrorField
          errorText="Note removed, please reload page"
          visible={!!removeNoteError || !!updateNoteError}
        />
      </Root>
    )

  return (
    <Root noTopBorder={noTopBorder}>
      <Row justify="space-between" mt={4}>
        <Label>Your Observations</Label>
        <ActionLink
          alignItems="center"
          data-test-id="form-report-remove-note"
          disabled={disabled}
          onClick={handleAddNote}
        >
          <Icon bold fontSize="9px" icon="expand" mr={1} />
          <StyledText fontWeight="700">Add Note</StyledText>
        </ActionLink>
      </Row>
    </Root>
  )
}

export default Note

const RemoveWrapper = styled.div`
  align-items: center;
  display: flex;
  font-weight: bold;
`
const StyledText = styled(Text)`
  color: inherit;
`

const Root = styled.div`
  width: inherit;
  ${(props) =>
    !props.noTopBorder &&
    css`
      margin-top: calc(${th('gridUnit')} * 4);
      border-top: 1px solid ${th('backgroundColor')};
    `}
`
