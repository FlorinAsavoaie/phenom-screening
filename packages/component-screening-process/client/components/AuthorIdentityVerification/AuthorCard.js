import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Text, Item } from '@hindawi/ui'
import {
  Popover,
  ValidationMessage,
  Card as PhenomCard,
  Button as PhenomButton,
} from '@hindawi/phenom-ui'
import { transform } from 'lodash'

import RorInformation from './RorInformation'
import { parseListWithComma } from '../../utils'

function AuthorCard({ author, setSelectedAuthor, authorNumber, isSelected }) {
  const {
    givenNames,
    surname,
    email,
    aff,
    orcid,
    isOnBadDebtList,
    isOnBlackList,
    isOnWatchList,
    hasOrcidIssue,
    rorAff,
    rorId,
    rorScore,
    isSubmitting,
    isCorresponding,
  } = author

  const ROR_SCORE_PERCENTAGE = rorScore * 100

  const sanctionList = transformSanctionListIntoArray({
    isOnBadDebtList,
    isOnBlackList,
    isOnWatchList,
  })

  const orcidUrl = JSON.parse(process.env.ORCID_SANDBOX_ENABLED || false)
    ? 'https://sandbox.orcid.org/'
    : 'https://orcid.org/'

  return (
    <Card
      className={isSelected ? 'card-selected' : undefined}
      hoverable
      isSelected={isSelected}
      onClick={() => setSelectedAuthor({ ...author, authorNumber })}
    >
      <Row justify="space-between" pl={3} pt={3}>
        <Item justify="flex-start">
          <Text fontWeight={700} pr={1}>
            First Name
          </Text>
          <Text>{givenNames}</Text>
        </Item>
        <Item>
          <Text fontWeight={700} pr={1}>
            Last Name
          </Text>
          <Text>{surname}</Text>
        </Item>
      </Row>
      <Row justify="flex-start" pl={3} pt={2}>
        <Text fontWeight={700} pr={1}>
          Email
        </Text>

        <Text display="block" ellipsis title={email}>
          {email}
        </Text>
      </Row>
      <Row justify="flex-start" pl={3} pt={2}>
        <Text fontWeight={700} pr={1}>
          ORCiD ID
        </Text>
        {orcid ? (
          <Button
            className="tertiary-navigation"
            data-test-id="author-card-orcid-link"
            href={`${orcidUrl}${orcid}`}
            target="_blank"
            type="link"
          >
            {orcid}
          </Button>
        ) : (
          <Text pr={1}>Not provided.</Text>
        )}
      </Row>

      {hasOrcidIssue && (
        <Row justify="flex-start" mb="-4px" pl={3} pt={1}>
          <Popover
            content="Author-supplied ORCiD ID does not match the ORCiD profile."
            placement="bottomLeft"
          >
            <ValidationMessage message="ORCiD ID issue" type="error" />
          </Popover>
        </Row>
      )}

      <Row justify="flex-start" pl={3} py={2}>
        <Text display="inline">
          <Text fontWeight={700} pr={1}>
            Affiliation
          </Text>
          {aff}
        </Text>
      </Row>

      {ROR_SCORE_PERCENTAGE < 70 && (isSubmitting || isCorresponding) && (
        <RorInformation rorAff={rorAff} rorId={rorId} />
      )}

      {!!sanctionList.length && (
        <StyledRow display="inherit;" px={3} py={3}>
          <Text fontWeight={700}>On Sanction List:</Text>
          <StyledText error fontWeight={700} pl={2}>
            {parseListWithComma(sanctionList)}
          </StyledText>
        </StyledRow>
      )}
    </Card>
  )
}

const transformSanctionListIntoArray = (sanctionList) => {
  const sanctionListLabel = {
    isOnBadDebtList: 'BAD DEBT',
    isOnBlackList: 'BLACKLIST',
    isOnWatchList: 'WATCHLIST',
  }

  return transform(
    sanctionList,
    (result, value, key) => {
      value && result.push(sanctionListLabel[key])
    },
    [],
  )
}

export default AuthorCard

const Card = styled(PhenomCard)`
  width: 100%;

  .ant-card-body {
    padding: 0;
  }
`
const Button = styled(PhenomButton)`
  padding: 0;
  height: 18px;
`

const StyledRow = styled(Row)`
  border-radius: 0 0 6px 6px;
  background-color: ${th('backgroundColor')};
`

const StyledText = styled(Text)`
  font-size: ${th('text.fontSizeBaseSmall')};
`
