import { get } from 'lodash'

import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { AuthorsVerificationTable, AuthorsVerificationBar } from '..'

import { useAuthorVerification } from '../../graphql/hooks'

function AuthorsVerification({
  disabled,
  loading,
  manuscriptId,
  possibleAuthorProfiles,
  selectedAuthor,
}) {
  const {
    verifyAuthor,
    unverifyAuthor,
    cannotVerifyAuthor,
    verifyOutOfToolAuthor,
    unverifyOutOfToolAuthor,
  } = useAuthorVerification({
    manuscriptId,
    authorId: selectedAuthor.id,
  })

  const isVerified = get(selectedAuthor, 'isVerified')
  const isVerifiedOutOfTool = get(selectedAuthor, 'isVerifiedOutOfTool')

  return (
    <Root>
      <AuthorsVerificationBar
        cannotVerifyAuthor={cannotVerifyAuthor}
        disabled={disabled}
        isVerified={isVerified}
        isVerifiedOutOfTool={isVerifiedOutOfTool}
        possibleAuthorProfiles={possibleAuthorProfiles}
        selectedAuthor={selectedAuthor}
        unverifyOutOfToolAuthor={unverifyOutOfToolAuthor}
        verifyOutOfToolAuthor={verifyOutOfToolAuthor}
      />
      <AuthorsVerificationTable
        disabled={disabled}
        isVerified={isVerified}
        isVerifiedOutOfTool={isVerifiedOutOfTool}
        loading={loading}
        possibleAuthorProfiles={possibleAuthorProfiles}
        unverifyAuthor={unverifyAuthor}
        verifyAuthor={verifyAuthor}
      />
    </Root>
  )
}

export default AuthorsVerification

// #region styles
const Root = styled.div`
  background-color: ${th('white')};
  display: flex;
  flex-direction: column;
  flex: 2;
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 2);
`
// #endregion
