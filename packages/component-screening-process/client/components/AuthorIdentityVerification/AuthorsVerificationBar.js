import { isEmpty } from 'lodash'
import { Row, Text } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button } from '@pubsweet/ui'

function AuthorsVerificationBar({
  isVerified,
  selectedAuthor,
  isVerifiedOutOfTool,
  cannotVerifyAuthor,
  possibleAuthorProfiles,
  verifyOutOfToolAuthor,
  unverifyOutOfToolAuthor,
  disabled,
}) {
  const buttonTitle = "Can't be verified"
  const automaticVerified = isVerified && isEmpty(possibleAuthorProfiles)
  const cannotBeVerified = isVerified === null || automaticVerified

  return (
    <Row justify="flex-start" pb={2}>
      <Text fontWeight={700}>
        #{selectedAuthor.authorNumber} Author database results
      </Text>

      {isVerifiedOutOfTool ? (
        <VerifyOutOfToolButton
          disabled={disabled}
          ml={4}
          onClick={unverifyOutOfToolAuthor}
          xs
        >
          Unverify Out of Tool
        </VerifyOutOfToolButton>
      ) : (
        <VerifyOutOfToolButton
          disabled={disabled || isVerified}
          ml={4}
          onClick={verifyOutOfToolAuthor}
          xs
        >
          Verify Out of Tool
        </VerifyOutOfToolButton>
      )}

      <CannotBeVerified
        disabled={disabled || cannotBeVerified}
        ml={4}
        onClick={cannotVerifyAuthor}
        xs
      >
        <Text fontWeight={700} medium>
          {buttonTitle}
        </Text>
      </CannotBeVerified>
    </Row>
  )
}
export default AuthorsVerificationBar

// #region styles
const VerifyOutOfToolButton = styled(Button)`
  background-color: ${th('mainTextColor')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  border-radius: 3px;
  color: ${th('white')};
  font-size: ${th('fontSizeBaseMedium')};

  :hover,
  :focus {
    background-color: ${th('mainTextColor')};
    border-radius: 3px;
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  }

  &[disabled] {
    cursor: not-allowed;
    opacity: 0.5;
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  }

  :focus,
  :hover {
    background: ${th('mainTextColor')};
    border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  }
`

const CannotBeVerified = styled(Button)`
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('mainTextColor')};
  border-radius: 3px;
`
// #endregion
