import { Row, Text } from '@hindawi/ui'
import { Popover, ValidationMessage, Button } from '@hindawi/phenom-ui'

import { Icon } from 'hindawi-shared/client/components'

function RorInformation({ rorAff, rorId }) {
  if (!rorId) {
    return (
      <Row justify="flex-start" mb={2} mt="-4px" pl={3}>
        <Popover
          content={() => (
            <>
              <Text>
                No ROR ID assigned to this affiliation. Please check the
                Research Organization Registry.
              </Text>
              <Row justify="flex-start" mt={2}>
                <ShowRorIdLink
                  rorIdLink="https://ror.org"
                  rorIdLinkName="ror.org"
                />
              </Row>
            </>
          )}
          placement="bottomLeft"
        >
          <ValidationMessage message="Affiliation match warning" type="error" />
        </Popover>
      </Row>
    )
  }

  return (
    <Row justify="flex-start" mb={2} mt="-4px" pl={3}>
      <Popover
        content={() => (
          <>
            <Text>
              Closest affiliation match (ROR ID):
              <Text fontWeight={700} pr={1}>
                {rorAff}
              </Text>
            </Text>
            <Row justify="flex-start" mt={2}>
              <ShowRorIdLink
                rorIdLink={`https://ror.org/${rorId}`}
                rorIdLinkName="View matches"
              />
            </Row>
          </>
        )}
        placement="bottomLeft"
      >
        <ValidationMessage message="Affiliation match warning" type="error" />
      </Popover>
    </Row>
  )
}

function ShowRorIdLink({ rorIdLink = null, rorIdLinkName }) {
  return (
    <Button
      className="tertiary-navigation"
      href={rorIdLink}
      target="_blank"
      type="link"
    >
      <Text pr={1}>{rorIdLinkName}</Text>
      <Icon name="goToLink" size={4} />
    </Button>
  )
}

export default RorInformation
