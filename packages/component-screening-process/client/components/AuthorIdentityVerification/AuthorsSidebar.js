import styled, { css } from 'styled-components'
import { Text, Item, Row } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import { AuthorCard } from '../../components'

const MESSAGES = {
  VERIFIED: 'VERIFIED',
  CANT_BE_VERIFIED: `CAN'T BE VERIFIED`,
}

function AuthorsSidebar({ authors, selectedAuthor, setSelectedAuthor }) {
  return (
    <Root>
      {authors.map((author, index) => (
        <Item key={author.id} mb={3} vertical>
          <Row justify="flex-start" pb={1}>
            <Text fontWeight={700} py={1}>
              #{index + 1} Author
            </Text>
            {author.isSubmitting && <Text fontWeight={700}>, Submitting</Text>}
            {(author.isVerified || author.isVerifiedOutOfTool) && (
              <Status
                isVerified={author.isVerified || author.isVerifiedOutOfTool}
              >
                {MESSAGES.VERIFIED}
              </Status>
            )}
            {author.isVerified === null && !author.isVerifiedOutOfTool && (
              <Status isVerified={author.isVerified}>
                {MESSAGES.CANT_BE_VERIFIED}
              </Status>
            )}
          </Row>
          <AuthorCard
            author={author}
            authorNumber={index + 1}
            isSelected={selectedAuthor.email === author.email}
            setSelectedAuthor={setSelectedAuthor}
          />
        </Item>
      ))}
    </Root>
  )
}

export default AuthorsSidebar

const Root = styled.div`
  flex: 1;
  padding: calc(${th('gridUnit')} * 3) calc(${th('gridUnit')} * 3) 0
    calc(${th('gridUnit')} * 3);
  overflow: scroll;
  overflow-x: hidden;
`

const verifiedStatus = ({ isVerified }) => {
  if (isVerified) {
    return css`
      border: solid 1px ${th('actionPrimaryColor')};
      color: ${th('actionPrimaryColor')};
    `
  }
  if (isVerified === null) {
    return css`
      border: solid 1px ${th('statusPending')};
      color: ${th('statusPending')};
    `
  }
}
const Status = styled.div`
  font-weight: bold;
  font-size: 11px;
  border-radius: 3px;
  background-color: ${th('white')};
  font-family: ${th('defaultFont')};
  margin-left: calc(${th('gridUnit')} * 2);
  padding: 0 ${th('gridUnit')};
  text-align: center;

  ${verifiedStatus}
`
