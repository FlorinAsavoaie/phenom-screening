import { Row, Text, Loader, Item, Label } from '@hindawi/ui'
import { get } from 'lodash'
import { darken, th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { Button } from '@pubsweet/ui'

function AuthorsVerificationTable({
  possibleAuthorProfiles,
  isVerifiedOutOfTool,
  unverifyAuthor,
  verifyAuthor,
  isVerified,
  loading,
  disabled,
}) {
  if (loading) {
    return (
      <Row justify="center" mt={8}>
        <Loader />
      </Row>
    )
  }

  return (
    <TableRoot possibleAuthorProfiles={possibleAuthorProfiles}>
      <HeaderRow pl={4}>
        <Item>
          <Label>First Name</Label>
        </Item>
        <Item>
          <Label>Last Name</Label>
        </Item>
        <Item flex={2}>
          <Label>Email</Label>
        </Item>
        <Item flex={2}>
          <Label>Affiliation</Label>
        </Item>
        <Item>
          <Label># of Publications</Label>
        </Item>
      </HeaderRow>

      <TableBody
        disabled={disabled}
        isVerified={isVerified}
        isVerifiedOutOfTool={isVerifiedOutOfTool}
        possibleAuthorProfiles={possibleAuthorProfiles}
        unverifyAuthor={unverifyAuthor}
        verifyAuthor={verifyAuthor}
      />
    </TableRoot>
  )
}

function TableBody({
  isVerified,
  verifyAuthor,
  unverifyAuthor,
  isVerifiedOutOfTool,
  possibleAuthorProfiles,
  disabled,
}) {
  if (!possibleAuthorProfiles.length) {
    return (
      <StyledRow height={22}>
        <StyledText fontWeight={700}>
          {isVerified ? `Author identified.` : `No results found.`}
        </StyledText>
      </StyledRow>
    )
  }
  return (
    <ScrollContainer>
      {possibleAuthorProfiles.map((author, index) => (
        <TableRow
          chosen={author.chosen}
          data-test-id="authors-table-row"
          key={author.id}
          pl={4}
        >
          <Item>
            <Text>{get(author, 'givenNames')}</Text>
          </Item>
          <Item>
            <Text>{get(author, 'surname')}</Text>
          </Item>
          <Item flex={2}>
            <Text>{get(author, 'email')}</Text>
          </Item>
          <Item flex={2}>
            <Text>{get(author, 'affiliation')}</Text>
          </Item>
          <Item justify="flex-end" pr={5}>
            {(function renderVerifyButton() {
              if (!(isVerified || isVerifiedOutOfTool))
                return (
                  <VerifyButton
                    data-test-id="verify-author"
                    disabled={disabled}
                    onClick={verifyAuthor(author.id)}
                    small
                  >
                    Verify
                  </VerifyButton>
                )
              if (author.chosen)
                return (
                  <VerifyButton
                    data-test-id="unverify-author"
                    disabled={disabled}
                    onClick={unverifyAuthor(author.id)}
                    small
                  >
                    Unverify
                  </VerifyButton>
                )
            })()}
            <Publications>{get(author, 'noOfPublications')}</Publications>
          </Item>
        </TableRow>
      ))}
    </ScrollContainer>
  )
}

export default AuthorsVerificationTable

const TableRootStyle = ({ possibleAuthorProfiles }) => {
  if (possibleAuthorProfiles.length) {
    return css`
      flex: 1;
      height: 0;
    `
  }
}
// #region styles
const TableRoot = styled.div`
  border: 1px solid ${th('colorBorder')};
  border-radius: ${th('borderRadius')};
  display: flex;
  flex-direction: column;

  ${TableRootStyle}
`

const TableRow = styled(Row)`
  background-color: ${(props) =>
    props.chosen ? darken('white', 10) : th('white')};
  border-bottom: 1px solid ${th('colorBorder')};
  min-height: calc(${th('gridUnit')} * 9);

  &:hover {
    background-color: ${darken('white', 10)};
  }
`

const HeaderRow = styled(TableRow)`
  min-height: calc(${th('gridUnit')} * 13);
  border-radius: 4px 4px 0px 0px;
  &:hover {
    background-color: ${th('white')};
  }
`

const ScrollContainer = styled.div`
  overflow: auto;
`

const Publications = styled(Text)`
  ${TableRow}:hover & {
    display: none;
  }
`
const VerifyButton = styled(Button)`
  background-color: ${th('mainTextColor')};
  border-color: ${th('mainTextColor')};
  color: ${th('white')};
  display: none;
  height: calc(${th('gridUnit')} * 6);
  min-width: calc(${th('gridUnit')} * 5);
  padding: 0 calc(${th('gridUnit')} * 2);

  &:hover {
    background-color: ${th('mainTextColor')};
    border-color: ${th('mainTextColor')};
  }

  ${TableRow}:hover & {
    display: inherit;
  }
`

const StyledText = styled(Text)`
  font-size: 20px;
  color: ${th('labelLineColor')};
`

const StyledRow = styled(Row)`
  border-radius: 0px 0px 4px 4px;
  background-color: ${th('backgroundColor')};
`
// #endregion
