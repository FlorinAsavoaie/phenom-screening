import { Checkbox, H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { get, memoize, debounce } from 'lodash'
import { useMutation } from '@apollo/client'
import {
  Row,
  Text,
  Item,
  Label,
  Textarea,
  ContextualBox,
  AuthorTagList,
} from '@hindawi/ui'
import { parseAuthors } from 'hindawi-shared/client/parseAuthors'

import { Note } from '../components'
import { mutations } from '../graphql'
import { useValidationCheckbox } from '../graphql/hooks'

const DEBOUNCE_TIME = 500
const useInfoValidation = (manuscriptId) => {
  const [mutateFn] = useMutation(mutations.updateReferences, {
    refetchQueries: ['getScreeningManuscript'],
  })

  const updateRefs = (references) => {
    mutateFn({ variables: { manuscriptId, references } })
  }

  return debounce(memoize(updateRefs), DEBOUNCE_TIME)
}

function InfoValidation({ manuscript, validations, disabled }) {
  const updateRefs = useInfoValidation(manuscript.id)
  const toggleValidation = useValidationCheckbox(manuscript.id)

  const authors = parseAuthors(manuscript.authors)

  return (
    <Root>
      <Row justify="flex-start">
        <H3>Info Validation</H3>
      </Row>

      <Row>
        <ContextualBox label="Manuscript Title" mt={4} startExpanded>
          <Row>
            <Item px={2} py={4} vertical>
              <Label>Provided in Submission</Label>
              <Text>{manuscript.title}</Text>
              <Note
                disabled={disabled}
                manuscriptId={manuscript.id}
                type="titleNote"
              />
            </Item>
          </Row>
        </ContextualBox>
      </Row>

      <Row>
        <ContextualBox label="Abstract" mt={4} startExpanded>
          <Row>
            <Item px={2} py={4} vertical>
              <Label mb={1}>Provided in Submission</Label>
              <Text>{manuscript.abstract}</Text>

              <Note
                disabled={disabled}
                manuscriptId={manuscript.id}
                type="abstractNote"
              />
            </Item>
          </Row>

          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.includesCitations}
              disabled={disabled}
              label="Includes Citations"
              onChange={toggleValidation('includesCitations')}
            />
          </Row>

          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.graphical}
              disabled={disabled}
              label="Graphical"
              onChange={toggleValidation('graphical')}
            />
          </Row>
        </ContextualBox>
      </Row>

      <Row>
        <ContextualBox label="Authors & Affiliations" mt={4} startExpanded>
          <Row>
            <Item px={2} py={4} vertical>
              <Label mb={1}>Provided in Submission</Label>
              <AuthorTagList authors={authors} withAffiliations />

              <Note
                disabled={disabled}
                manuscriptId={manuscript.id}
                type="authorsNote"
              />
            </Item>
          </Row>

          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.differentAuthors}
              disabled={disabled}
              label="Different Authors"
              onChange={toggleValidation('differentAuthors')}
            />
          </Row>

          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.missingAffiliations}
              disabled={disabled}
              label="Missing Affiliations"
              onChange={toggleValidation('missingAffiliations')}
            />
          </Row>

          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.nonacademicAffiliations}
              disabled={disabled}
              label="Nonacademic Affiliations"
              onChange={toggleValidation('nonacademicAffiliations')}
            />
          </Row>
        </ContextualBox>
      </Row>

      <Row>
        <ContextualBox label="Relevance Check" mt={4} startExpanded>
          <Row>
            <Item pb={4} px={2} vertical>
              <Note
                disabled={disabled}
                manuscriptId={manuscript.id}
                noTopBorder
                type="relevanceNote"
              />
            </Item>
          </Row>
          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.topicNotAppropriate}
              disabled={disabled}
              label="Topic not appropriate"
              onChange={toggleValidation('topicNotAppropriate')}
            />
          </Row>
          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.articleTypeIsNotAppropriate}
              disabled={disabled}
              label="Article type is not appropriate"
              onChange={toggleValidation('articleTypeIsNotAppropriate')}
            />
          </Row>
          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.manuscriptIsMissingSections}
              disabled={disabled}
              label="Manuscript is missing sections"
              onChange={toggleValidation('manuscriptIsMissingSections')}
            />
          </Row>
        </ContextualBox>
      </Row>

      <Row>
        <ContextualBox label="Reference Extraction" mt={4} startExpanded>
          <Row>
            <Item px={2} py={4} vertical>
              <Label mb={1}>Extracted References</Label>
              <Textarea
                defaultValue={get(manuscript, 'references', '')}
                disabled={disabled}
                onChange={(e) => {
                  updateRefs(e.target.value)
                }}
                placeholder="Copy paste the manuscript references here"
              />
              <Note
                disabled={disabled}
                manuscriptId={manuscript.id}
                type="referencesNote"
              />
            </Item>
          </Row>

          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.missingReferences}
              disabled={disabled}
              label="Missing References"
              onChange={toggleValidation('missingReferences')}
            />
          </Row>

          <Row justify="flex-start" mb={2} ml={2}>
            <Checkbox
              checked={validations.wrongReferences}
              disabled={disabled}
              label="Wrong References"
              onChange={toggleValidation('wrongReferences')}
            />
          </Row>
        </ContextualBox>
      </Row>
    </Root>
  )
}

export default InfoValidation

// #region styles
const Root = styled.aside`
  background-color: ${th('white')};
  overflow: scroll;
  overflow-x: hidden;
  padding: calc(${th('gridUnit')} * 4);

  & label input {
    display: none;
  }
`
// #endregion
