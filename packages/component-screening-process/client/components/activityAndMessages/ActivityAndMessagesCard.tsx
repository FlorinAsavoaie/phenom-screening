import { ReactElement } from 'react'
import { Text, Row } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DateParser } from '@pubsweet/ui'

import { AuditLog } from 'hindawi-shared/SchemaInterfaces'

interface ActivityAndMessagesProps {
  auditLog: AuditLog
  id: string
}

type Roles = {
  [name: string]: string
}

type Actions = {
  [name: string]: string
}

const parsedRoles: Roles = {
  admin: 'Admin',
  teamLeader: 'Team Lead',
  screener: 'Screener',
  qualityChecker: 'Quality Checker',
}

const parsedActions: Actions = {
  sendToTeamLead: 'Send to Team Lead',
  returnToChecker: 'Return to Checker',
  pauseManuscript: 'Pause Manuscript',
  unpauseManuscript: 'Unpause Manuscript',
}

const ActivityAndMessagesCard: React.FC<ActivityAndMessagesProps> = ({
  auditLog,
  id,
}) => {
  const { userRole, user, created, action, comment } = auditLog

  return (
    <Card key={id}>
      <CardHeader>
        <Item>
          <Text color={th('colorTextPlaceholder')} fontWeight={700} pr={1}>
            {parsedRoles[userRole]}:
          </Text>
          <Text
            color={th('colorTextPlaceholder')}
          >{`${user.givenNames} ${user.surname}`}</Text>
        </Item>
        <Item>
          <DateParser
            dateFormat="YYYY-MM-DD"
            humanizeThreshold={0}
            timestamp={created}
          >
            {(timestamp: string): ReactElement => (
              <Text display="flex">{timestamp}</Text>
            )}
          </DateParser>
        </Item>
      </CardHeader>

      <CardContent>
        <Row>
          <Text fontWeight={700} mr={1}>
            Action:
          </Text>
          <Text>{parsedActions[action]}</Text>
        </Row>
        {comment && (
          <Row mt={2}>
            <Text whiteSpace="pre-wrap">{comment}</Text>
          </Row>
        )}
      </CardContent>
    </Card>
  )
}

export { ActivityAndMessagesCard }

export const CardHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: ${th('backgroundColor')};
  height: calc(${th('gridUnit')} * 8);
  padding: 0px calc(${th('gridUnit')} * 2);
`

export const Card = styled.div`
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  background-color: ${th('white')};
  border: solid 1px ${th('furnitureColor')};
  width: 100%;
  margin-bottom: calc(${th('gridUnit')} * 4);
  color: ${th('mainTextColor')};
`
export const CardContent = styled.div`
  div {
    display: flex;
    justify-content: flex-start;
  }
  padding: calc(${th('gridUnit')} * 4);
`

const Item = styled.div``
