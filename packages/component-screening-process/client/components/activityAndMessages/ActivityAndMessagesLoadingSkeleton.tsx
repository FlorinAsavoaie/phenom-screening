/* eslint-disable react/no-array-index-key */
import styled from 'styled-components'
import { Row } from '@hindawi/ui'
import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { Card, CardHeader, CardContent } from '../activityAndMessages'

const ActivityAndMessagesLoadingSkeleton = () => (
  <Root>
    {[...Array(3)].map((_, index) => (
      <Card key={`activities-and-message-card-loader-${index}`}>
        <CardHeader>
          <LoadingSkeleton height="8px" width="134px" />
          <LoadingSkeleton height="8px" width="75px" />
        </CardHeader>

        <CardContent>
          <Row mb={4}>
            <LoadingSkeleton height="8px" width="50px" />
          </Row>
          <Row mb={4}>
            <LoadingSkeleton height="8px" width="212px" />
          </Row>
          <Row mb={4}>
            <LoadingSkeleton height="8px" width="196px" />
          </Row>
          <Row>
            <LoadingSkeleton height="8px" width="330px" />
          </Row>
        </CardContent>
      </Card>
    ))}
  </Root>
)

export { ActivityAndMessagesLoadingSkeleton }

interface LoadingSkeletonProps {
  width?: string
  height?: string
}
const Root = styled.div`
  width: 100%;
`

const LoadingSkeleton = styled.div<LoadingSkeletonProps>`
  position: relative;
  overflow: hidden;
  width: ${(props) => get(props, 'width', '100px')};
  height: ${(props) => get(props, 'height', '100px')};
  background-color: ${th('grey30')};
  ::before {
    content: '';
    display: block;
    position: absolute;
    left: -150px;
    top: 0;
    height: 100%;
    width: 100%;
    background: linear-gradient(
      90deg,
      ${th('grey30')} 17.04%,
      ${th('grey20')} 29.4%,
      ${th('grey30')} 41.25%
    );
    animation-duration: 1.7s;
    animation-fill-mode: forwards;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
    animation-name: breath-animation;

    @keyframes breath-animation{
      from {
        left: -150px;
      };
      to{
        left: 100%;
      }
    }
`
