/* eslint-disable react/no-array-index-key */
import { useEffect } from 'react'
import styled from 'styled-components'
import { Item, Text } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Icon } from 'hindawi-shared/client/components'
import { useAuditLogsForSubmission } from '../../hooks/useAuditLogsForSubmission'
import {
  ActivityAndMessagesCard,
  ActivityAndMessagesLoadingSkeleton,
} from '../activityAndMessages'

interface ActivityAndMessagesContent {
  manuscriptId: string
  submissionId: string
}
const ActivityAndMessagesContent: React.FC<ActivityAndMessagesContent> = ({
  manuscriptId,
  submissionId,
}) => {
  const { auditLogs, loading, error } = useAuditLogsForSubmission({
    manuscriptId,
    submissionId,
  })

  useEffect(() => {
    const element = document.querySelector('.activityAndMessages')
    if (element) {
      element.scrollTo(0, 0)
    }
  }, [auditLogs])

  if (loading) {
    return <ActivityAndMessagesLoadingSkeleton />
  }

  if (error) {
    return (
      <DisplayError message="Activity couldn't be loaded due to technical issues" />
    )
  }

  if (!auditLogs.length) {
    return <DisplayError message="No activity or messages to show" />
  }

  return (
    <Root>
      {auditLogs.map((auditLog, index) => (
        <ActivityAndMessagesCard
          auditLog={auditLog}
          id={`activities-and-message-card-${index}`}
        />
      ))}
    </Root>
  )
}

export { ActivityAndMessagesContent }

const DisplayError: React.FC<{ message: string }> = ({ message }) => (
  <Item alignItems="center" mt={5} vertical>
    <Icon color="grey30" name="noMessages" size={12} />
    <ErrorText fontWeight={700} mt={3}>
      {message}
    </ErrorText>
  </Item>
)

const Root = styled.div`
  width: 100%;
`

const ErrorText = styled(Text)`
  font-size: 19px;
  color: ${th('grey50')};
  line-height: 27.28px;
`
