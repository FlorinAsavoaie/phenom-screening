import styled from 'styled-components'
import { Text as BaseText, Row } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Icon } from 'hindawi-shared/client/components'
import { Manuscript } from 'hindawi-shared/SchemaInterfaces'
import { ActivityAndMessagesContent } from './ActivityAndMessagesContent'

interface ManuscriptInfoProps {
  manuscript: Manuscript
  handleSetShowAuditLog: () => any
}

const ActivityAndMessages: React.FC<ManuscriptInfoProps> = ({
  manuscript,
  handleSetShowAuditLog,
}) => (
  <Root className="activityAndMessages">
    <Row justify="space-between" mb={4}>
      <Text fontWeight={700} mt={1}>
        Activity & Messages
      </Text>
      <CloseDrawerIcon
        height="16px"
        name="close"
        onClick={handleSetShowAuditLog}
        width="16px"
      />
    </Row>
    <ActivityAndMessagesContent
      manuscriptId={manuscript.id}
      submissionId={manuscript.submissionId}
    />
  </Root>
)

export { ActivityAndMessages }

const Root = styled.div`
  align-items: flex-start;
  background: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  border: none;
  display: flex;
  flex-direction: column;
  height: calc(100vh - ${th('gridUnit')} * 43);
  padding: calc(${th('gridUnit')} * 4);
  position: absolute;
  right: 0;
  top: 108px;
  overflow: scroll;
  z-index: 2;
  box-shadow: ${th('boxShadow')};
  width: calc(${th('gridUnit')} * 123);
`

const Text = styled(BaseText)`
  font-size: 16px;
  color: ${th('mainTextColor')};
`
const CloseDrawerIcon = styled(Icon)`
  cursor: pointer;
`
