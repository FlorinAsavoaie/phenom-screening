import { ContextualBox, Text, Loader } from '@hindawi/ui'
import { SubmissionSimilarityManuscripts } from './SubmissionSimilarityManuscripts'
import { useSimilarSubmissions } from '../../../graphql/hooks'

export function SubmissionSimilarity({ currentUser, manuscriptId }) {
  const { similarSubmissions, loading } = useSimilarSubmissions(manuscriptId)

  return (
    <ContextualBox
      label="Submission Similarity"
      mt={4}
      rightChildren={() =>
        SubmissionSimilarityLabel({ similarSubmissions, loading })
      }
    >
      <SubmissionSimilarityManuscripts
        currentUser={currentUser}
        manuscripts={similarSubmissions}
      />
    </ContextualBox>
  )
}

function SubmissionSimilarityLabel({ similarSubmissions, loading }) {
  if (loading) {
    return <Loader iconSize={4} mb="2px" mr={2} />
  }

  return (
    <Text customId error={similarSubmissions.length} fontWeight={700} mr={2}>
      {`${similarSubmissions.length} `}
      {similarSubmissions.length === 1 ? 'MATCH' : 'MATCHES'} FOUND
    </Text>
  )
}
