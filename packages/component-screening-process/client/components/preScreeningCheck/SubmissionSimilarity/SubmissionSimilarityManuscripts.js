import { ManuscriptCard } from 'component-dashboard/client/components'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Item, Text as BaseText } from '@hindawi/ui'

export function SubmissionSimilarityManuscripts({ manuscripts, currentUser }) {
  if (manuscripts.length === 0) {
    return (
      <StyledRow>
        <StyledText fontWeight={700}>No manuscripts found.</StyledText>
      </StyledRow>
    )
  }

  return (
    <StyledItem px={4} py={2} vertical>
      {manuscripts.map((manuscript) => (
        <ManuscriptCard
          currentUser={currentUser}
          key={manuscript.id}
          manuscript={manuscript}
          my={2}
          showAllAuthors
          showAssignedChecker
          width="inherit"
        />
      ))}
    </StyledItem>
  )
}

const StyledText = styled(BaseText)`
  font-size: 20px;
  color: ${th('labelLineColor')};
`
const StyledRow = styled(Row)`
  min-height: calc(${th('gridUnit')} * 22);
  flex: 1;
  border-radius: 0px 0px 4px 4px;
  background-color: ${th('backgroundColor')};
`
const StyledItem = styled(Item)`
  width: calc(100vw - 176px);
` // 176 all margins and paddings + side navBar width
