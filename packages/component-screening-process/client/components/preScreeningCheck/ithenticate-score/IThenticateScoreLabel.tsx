import { Text } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const MAX_SIMILARITY_PERCENTAGE = 76
const MAX_FIRST_SOURCE_SIMILARITY_PERCENTAGE = 61
const MIN_SIMILARITY_PERCENTAGE = 65
const MIN_FIRST_SOURCE_SIMILARITY_PERCENTAGE = 50

interface IThenticateScoreLabelProps {
  totalSimilarityPercentage: number
  firstSourceSimilarityPercentage: number
}

const IThenticateScoreLabel: React.FC<IThenticateScoreLabelProps> = ({
  totalSimilarityPercentage,
  firstSourceSimilarityPercentage,
}) => {
  const plagiarised =
    totalSimilarityPercentage >= MAX_SIMILARITY_PERCENTAGE ||
    firstSourceSimilarityPercentage >= MAX_FIRST_SOURCE_SIMILARITY_PERCENTAGE
  const notPlagiarised =
    totalSimilarityPercentage <= MIN_SIMILARITY_PERCENTAGE &&
    firstSourceSimilarityPercentage <= MIN_FIRST_SOURCE_SIMILARITY_PERCENTAGE
  if (plagiarised) {
    return (
      <Text error fontWeight={700} mr={2}>
        PLAGIARISED
      </Text>
    )
  } else if (notPlagiarised) {
    return (
      <Text customId fontWeight={700} mr={2}>
        OK
      </Text>
    )
  }
  return (
    <ScoreText fontWeight={700} mr={2}>
      INCONCLUSIVE
    </ScoreText>
  )
}

export { IThenticateScoreLabel }

const ScoreText = styled(Text)`
  color: ${th('infoColor')};
`
