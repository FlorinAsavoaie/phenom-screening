import { ContextualBox, Item, Text, Row, Icon } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { IThenticateScoreLabel } from './IThenticateScoreLabel'

interface IThenticateScoreProps {
  totalSimilarityPercentage: number
  firstSourceSimilarityPercentage: number
  reportUrl: string
}

const IThenticateScore: React.FC<IThenticateScoreProps> = ({
  totalSimilarityPercentage,
  firstSourceSimilarityPercentage,
  reportUrl,
}) => (
  <ContextualBox
    label="iThenticate Score"
    mt={4}
    rightChildren={(): React.ReactNode =>
      IThenticateScoreLabel({
        totalSimilarityPercentage,
        firstSourceSimilarityPercentage,
      })
    }
  >
    <Item px={4} py={4} vertical>
      <Row justify="flex-start">
        <Text fontWeight={700} mr={2}>
          Total Similarity Percentage:
        </Text>
        <Text mr={4}>{totalSimilarityPercentage}%</Text>

        <Text fontWeight={700} mr={2}>
          1st Source Similarity Percentage:
        </Text>
        <Text mr={4}>{firstSourceSimilarityPercentage}%</Text>
        <Item>
          <Link href={reportUrl} target="blank">
            More Details
          </Link>
          <IconLink icon="link" ml={1} />
        </Item>
      </Row>
    </Item>
  </ContextualBox>
)

export default IThenticateScore

const Link = styled.a`
  font-family: ${th('defaultFont')};
  color: ${th('action.color')};
  cursor: pointer;
  font-weight: 600;

  &:hover {
    color: ${th('colorSecondary')};
  }

  &:active,
  &:focus {
    color: ${th('action.colorActive')};
  }
`

const IconLink = styled(Icon)`
  color: ${th('action.color')};
`
