import { ContextualBox, Item, Text, Row } from '@hindawi/ui'

const MIN_WORDS = 750
const MIN_PERCENTAGE = 80

interface LanguageProps {
  languagePercentage: number
  wordCount: number
}

const LanguageRightLabel: React.FC<LanguageProps> = ({
  languagePercentage,
  wordCount,
}) => {
  const hasMinimumWords = wordCount >= MIN_WORDS
  const hasMinimumPercentage = languagePercentage >= MIN_PERCENTAGE

  if (languagePercentage < 0 || wordCount < 0) {
    return (
      <Text error fontWeight={700} mr={2}>
        TEST FAILED
      </Text>
    )
  }

  return (
    <aside>
      <Text fontWeight={700} mr={2}>
        Word Count
      </Text>
      <Text
        customId={hasMinimumWords}
        error={!hasMinimumWords}
        fontWeight={700}
        mr={2}
      >
        {wordCount}
      </Text>
      <Text
        customId={hasMinimumPercentage}
        error={!hasMinimumPercentage}
        fontWeight={700}
        mr={2}
      >
        {hasMinimumPercentage ? 'ENGLISH' : 'NOT ENGLISH'}
      </Text>
    </aside>
  )
}

const LanguageAndWordcount: React.FC<LanguageProps> = ({
  languagePercentage = 0,
  wordCount = 0,
}) => (
  <ContextualBox
    label="Language & Wordcount"
    rightChildren={(): React.ReactNode =>
      LanguageRightLabel({ languagePercentage, wordCount })
    }
  >
    <Item px={4} py={4} vertical>
      <Row justify="flex-start">
        <Text fontWeight={700} mr={2}>
          Text in English:
        </Text>
        <Text mr={4}>{languagePercentage}%</Text>

        <Text fontWeight={700} mr={2}>
          Text not in English:
        </Text>
        <Text mr={4}>{100 - languagePercentage}%</Text>
      </Row>
    </Item>
  </ContextualBox>
)

export default LanguageAndWordcount
