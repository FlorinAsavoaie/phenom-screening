import { Row, Tooltip, Typography, Button } from '@hindawi/phenom-ui'
import styled, { createGlobalStyle } from 'styled-components'

export interface SuspiciousWord {
  word: string
  noOfOccurrences: number
  reason: string
}

interface SuspiciousWordsSectionProps {
  suspiciousWords: SuspiciousWord[]
}

const { Text } = Typography

const SuspiciousCountText = styled(Text)`
  margin-left: 5px;
`
const SuspiciousWordsWrapper = styled(Row)`
  align-items: baseline;
`
const WordButton = styled(Button)`
  padding: 0;
  align-items: baseline;
`

const SuspiciousWordsSection: React.FC<SuspiciousWordsSectionProps> = ({
  suspiciousWords,
}) => (
  <SuspiciousWordsWrapper justify="start">
    {suspiciousWords
      .map((suspiciousWord) => (
        <div key={suspiciousWord.word}>
          <Tooltip
            copyContent={
              suspiciousWord.reason
                ? suspiciousWord.reason
                : 'No recommendation found'
            }
            placement="topLeft"
            replaceTooltipText="Text"
            showCopyContent
          >
            <WordButton type="link">{suspiciousWord.word}:</WordButton>
          </Tooltip>
          <SuspiciousCountText type="secondary">
            {suspiciousWord.noOfOccurrences}
          </SuspiciousCountText>
        </div>
      ))
      .reduce(
        (prev, curr, index): any =>
          index === 0 ? [prev, curr] : [prev, `,\u2004`, curr],
        [],
      )}
  </SuspiciousWordsWrapper>
)
export { SuspiciousWordsSection }
