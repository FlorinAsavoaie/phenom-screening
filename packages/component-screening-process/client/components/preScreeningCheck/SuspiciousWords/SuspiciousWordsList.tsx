import { Label, Item } from '@hindawi/ui'

import {
  SuspiciousWordsSection,
  SuspiciousWord,
} from './SuspiciousWordsSection'

interface SuspiciousWordsListProps {
  suspiciousWords: {
    previouslyVerifiedSuspiciousWords: SuspiciousWord[]
    newSuspiciousWords: SuspiciousWord[]
  }
}

const SuspiciousWordsList: React.FC<SuspiciousWordsListProps> = ({
  suspiciousWords,
}) => {
  const previousWordsPresent =
    suspiciousWords.previouslyVerifiedSuspiciousWords.length > 0
  const newWordsPresent = suspiciousWords.newSuspiciousWords.length > 0

  if (!previousWordsPresent && newWordsPresent) {
    return (
      <Item pt={4} px={4} vertical>
        <SuspiciousWordsSection
          suspiciousWords={suspiciousWords.newSuspiciousWords}
        />
      </Item>
    )
  }

  if (previousWordsPresent && newWordsPresent) {
    return (
      <Item pt={4} px={4} vertical>
        <Label>Previously verified words</Label>
        <SuspiciousWordsSection
          suspiciousWords={suspiciousWords.previouslyVerifiedSuspiciousWords}
        />

        <Label>New suspicious words</Label>
        <SuspiciousWordsSection
          suspiciousWords={suspiciousWords.newSuspiciousWords}
        />
      </Item>
    )
  }

  if (previousWordsPresent && !newWordsPresent) {
    return (
      <Item pt={4} px={4} vertical>
        <Label>Previously verified words</Label>
        <SuspiciousWordsSection
          suspiciousWords={suspiciousWords.previouslyVerifiedSuspiciousWords}
        />
      </Item>
    )
  }

  return null
}

export { SuspiciousWordsList }
