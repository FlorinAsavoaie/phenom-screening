import { isEmpty } from 'lodash'
import { ContextualBox, Text } from '@hindawi/ui'

import { SuspiciousWordsList } from './SuspiciousWordsList'
import { SuspiciousWordsLabel } from './SuspiciousWordsLabel'
import { SuspiciousWord } from './SuspiciousWordsSection'

interface SuspiciousWordsProps {
  suspiciousWords: {
    previouslyVerifiedSuspiciousWords: SuspiciousWord[]
    newSuspiciousWords: SuspiciousWord[]
  }
}

const SuspiciousWords: React.FC<SuspiciousWordsProps> = ({
  suspiciousWords,
}) => {
  const noSuspiciousWords =
    isEmpty(suspiciousWords.previouslyVerifiedSuspiciousWords) &&
    isEmpty(suspiciousWords.newSuspiciousWords)

  return (
    <ContextualBox
      label="Suspicious Words"
      mt={4}
      rightChildren={(): React.ReactNode =>
        SuspiciousWordsLabel({ noSuspiciousWords })
      }
    >
      {noSuspiciousWords ? (
        <Text m={4}>No suspicious words found in this manuscript.</Text>
      ) : (
        <SuspiciousWordsList suspiciousWords={suspiciousWords} />
      )}
    </ContextualBox>
  )
}
export { SuspiciousWords }
