import { Text } from '@hindawi/ui'

interface SuspiciousWordsLabelProps {
  noSuspiciousWords: boolean
}

const SuspiciousWordsLabel: React.FC<SuspiciousWordsLabelProps> = ({
  noSuspiciousWords,
}) => {
  if (noSuspiciousWords) {
    return (
      <Text customId mr={2}>
        NONE
      </Text>
    )
  }
  return (
    <Text customId error fontWeight={700} mr={2}>
      FOUND
    </Text>
  )
}
export { SuspiciousWordsLabel }
