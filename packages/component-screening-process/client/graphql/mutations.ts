import { gql } from '@apollo/client'

export const updateReferences = gql`
  mutation updateReferences($manuscriptId: String!, $references: String) {
    updateReferences(manuscriptId: $manuscriptId, references: $references)
  }
`
export const getScreeningSignedUrl = gql`
  mutation getScreeningSignedUrl($fileId: String!, $format: FileFormat!) {
    getScreeningSignedUrl(fileId: $fileId, format: $format)
  }
`
export const addNote = gql`
  mutation addNote($manuscriptId: String, $type: NoteType) {
    addNote(manuscriptId: $manuscriptId, type: $type) {
      id
      type
      content
      manuscriptId
    }
  }
`
export const removeNote = gql`
  mutation removeNote($noteId: String!) {
    removeNote(noteId: $noteId)
  }
`
export const updateNote = gql`
  mutation updateNote($noteId: String!, $content: String) {
    updateNote(noteId: $noteId, content: $content)
  }
`

export const toggleValidation = gql`
  mutation toggleValidation($manuscriptId: String!, $field: ValidationFields) {
    toggleValidation(manuscriptId: $manuscriptId, field: $field)
  }
`

export const approveDecision = gql`
  mutation approveDecision($manuscriptId: String!, $content: String) {
    approveDecision(manuscriptId: $manuscriptId, content: $content)
  }
`
export const returnToDraftMutation = gql`
  mutation returnManuscriptToDraft($manuscriptId: String!, $content: String) {
    returnManuscriptToDraft(manuscriptId: $manuscriptId, content: $content)
  }
`

export const refuseToConsiderDecision = gql`
  mutation refuseToConsiderDecision(
    $manuscriptId: String!
    $content: RTCInput
    $note: String
  ) {
    refuseToConsiderDecision(
      manuscriptId: $manuscriptId
      content: $content
      note: $note
    )
  }
`

export const pauseManuscriptMutation = gql`
  mutation pauseManuscript($manuscriptId: String!, $reason: String!) {
    manuscript: pauseManuscript(manuscriptId: $manuscriptId, reason: $reason) {
      status
    }
  }
`
export const unpauseManuscriptMutation = gql`
  mutation unpauseManuscript($manuscriptId: String!) {
    manuscript: unpauseManuscript(manuscriptId: $manuscriptId) {
      status
    }
  }
`

export const voidManuscriptMutation = gql`
  mutation voidManuscript($manuscriptId: String!, $reason: String!) {
    voidManuscript(manuscriptId: $manuscriptId, reason: $reason)
  }
`

export const escalateManuscriptMutation = gql`
  mutation escalateManuscript($manuscriptId: String!, $reason: String!) {
    manuscript: escalateManuscript(
      manuscriptId: $manuscriptId
      reason: $reason
    ) {
      status
    }
  }
`

export const verifyAuthor = gql`
  mutation verifyAuthor(
    $manuscriptId: ID!
    $authorId: ID!
    $possibleAuthorId: ID!
  ) {
    verifyAuthor(
      manuscriptId: $manuscriptId
      authorId: $authorId
      possibleAuthorId: $possibleAuthorId
    )
  }
`
export const unverifyAuthor = gql`
  mutation unverifyAuthor(
    $manuscriptId: ID!
    $authorId: ID!
    $possibleAuthorId: ID!
  ) {
    unverifyAuthor(
      manuscriptId: $manuscriptId
      authorId: $authorId
      possibleAuthorId: $possibleAuthorId
    )
  }
`
export const cannotVerifyAuthor = gql`
  mutation cannotVerifyAuthor($manuscriptId: ID!, $authorId: ID!) {
    cannotVerifyAuthor(manuscriptId: $manuscriptId, authorId: $authorId)
  }
`

export const verifyOutOfToolAuthor = gql`
  mutation verifyOutOfToolAuthor($manuscriptId: ID!, $authorId: ID!) {
    verifyOutOfToolAuthor(manuscriptId: $manuscriptId, authorId: $authorId)
  }
`

export const unverifyOutOfToolAuthor = gql`
  mutation unverifyOutOfToolAuthor($manuscriptId: ID!, $authorId: ID!) {
    unverifyOutOfToolAuthor(manuscriptId: $manuscriptId, authorId: $authorId)
  }
`
export const returnToCheckerMutation = gql`
  mutation returnToChecker($manuscriptId: String!, $reason: String!) {
    manuscript: returnToChecker(manuscriptId: $manuscriptId, reason: $reason) {
      status
    }
  }
`
