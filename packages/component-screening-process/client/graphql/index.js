import * as queries from './queries'
import * as mutations from './mutations'
import * as fragments from './fragments'

export { queries, mutations, fragments }
