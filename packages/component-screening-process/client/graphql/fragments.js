import { gql } from '@apollo/client'
// TODO: find a better approach for shared fragments between component-modules
import {
  journalDashboard,
  assignedCheckerDashboard,
} from 'component-dashboard/client/graphql/fragments'

export { screeningManuscript } from './fragments/screeningManuscriptFragment'

export const manuscriptValidations = gql`
  fragment manuscriptValidations on ManuscriptValidations {
    includesCitations
    graphical
    differentAuthors
    missingAffiliations
    nonacademicAffiliations
    topicNotAppropriate
    articleTypeIsNotAppropriate
    manuscriptIsMissingSections
    missingReferences
    wrongReferences
  }
`
export const submissionSimilarityCard = gql`
  fragment submissionSimilarityCard on ScreeningManuscript {
    id
    title
    flowType
    customId
    articleType
    submissionDate
    status
    submittingAuthor {
      givenNames
      surname
    }
    journal {
      ...journalDashboard
    }
    journalSection {
      id
      name
    }
    specialIssue {
      id
      name
    }
    assignedChecker {
      ...assignedCheckerDashboard
    }
    authors {
      id
      surname
      givenNames
      isSubmitting
    }
  }

  ${journalDashboard}
  ${assignedCheckerDashboard}
`
