import { gql } from '@apollo/client'

export const submissionStatusAndStepUpdated = gql`
  subscription submissionStatusAndStepUpdated($submissionIds: [ID]) {
    manuscript: submissionStatusAndStepUpdated(submissionIds: $submissionIds) {
      submissionId
      status
      step
    }
  }
`
