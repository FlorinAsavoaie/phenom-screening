import { gql } from '@apollo/client'

import {
  manuscriptValidations,
  screeningManuscript,
  submissionSimilarityCard,
} from './fragments'

export const getScreeningManuscript = gql`
  query getScreeningManuscript($manuscriptId: ID!) {
    getScreeningManuscript(manuscriptId: $manuscriptId) {
      ...screeningManuscript
    }
  }
  ${screeningManuscript}
`

export const getNote = gql`
  query getNote($manuscriptId: ID!, $type: NoteType!) {
    getNote(manuscriptId: $manuscriptId, type: $type) {
      id
      created
      updated
      type
      content
      manuscriptId
    }
  }
`

export const getManuscriptChecks = gql`
  query getManuscriptChecks($manuscriptId: ID!) {
    getManuscriptChecks(manuscriptId: $manuscriptId) {
      wordCount
      reportUrl
      suspiciousWords {
        previouslyVerifiedSuspiciousWords {
          word
          noOfOccurrences
          reason
        }
        newSuspiciousWords {
          word
          noOfOccurrences
          reason
        }
      }
      languagePercentage
      totalSimilarityPercentage
      firstSourceSimilarityPercentage
    }
  }
`

export const getManuscriptValidations = gql`
  query getManuscriptValidations($manuscriptId: ID!) {
    getManuscriptValidations(manuscriptId: $manuscriptId) {
      ...manuscriptValidations
    }
  }
  ${manuscriptValidations}
`

export const getPossibleAuthorProfiles = gql`
  query getPossibleAuthorProfiles($authorId: ID!, $manuscriptId: ID!) {
    getPossibleAuthorProfiles(
      authorId: $authorId
      manuscriptId: $manuscriptId
    ) {
      id
      email
      chosen
      surname
      givenNames
      affiliation
      noOfPublications
    }
  }
`
export const getSimilarSubmissions = gql`
  query getSimilarSubmissions($manuscriptId: ID!) {
    similarSubmissions: getSimilarSubmissions(manuscriptId: $manuscriptId) {
      ...submissionSimilarityCard
    }
  }
  ${submissionSimilarityCard}
`
export const getLatestPauseReason = gql`
  query getLatestPauseReason($manuscriptId: ID!) {
    latestPauseReason: getLatestPauseReason(manuscriptId: $manuscriptId) {
      id
      content
    }
  }
`
