import { memoize, debounce, get } from 'lodash'
import { useMutation, useQuery, useSubscription } from '@apollo/client'

import * as queries from './queries'
import * as mutations from './mutations'
import * as subscriptions from './subscriptions'

export const useNote = () => {
  const DEBOUNCE_TIME = 500
  const [addNoteMutation] = useMutation(mutations.addNote)
  const [removeNoteMutation, { error: removeNoteError }] = useMutation(
    mutations.removeNote,
  )
  const [updateNoteMutation, { error: updateNoteError }] = useMutation(
    mutations.updateNote,
  )

  const addNote = ({ manuscriptId, type }) =>
    addNoteMutation({
      variables: { manuscriptId, type },
      refetchQueries: [
        { query: queries.getNote, variables: { manuscriptId, type } },
      ],
    })

  const removeNote = ({ manuscriptId, type, noteId }) =>
    removeNoteMutation({
      variables: { noteId },
      refetchQueries: [
        { query: queries.getNote, variables: { manuscriptId, type } },
      ],
    })

  const updateNote = ({ manuscriptId, type, noteId, content }) =>
    updateNoteMutation({
      variables: { noteId, content },
      refetchQueries: [
        { query: queries.getNote, variables: { manuscriptId, type } },
      ],
    })

  return {
    addNote,
    removeNote,
    updateNote: debounce(memoize(updateNote), DEBOUNCE_TIME),
    removeNoteError,
    updateNoteError,
  }
}

export const useValidationCheckbox = (manuscriptId) => {
  const [toggleMutation] = useMutation(mutations.toggleValidation)

  const toggleValidation = (field) => () =>
    toggleMutation({
      variables: { manuscriptId, field },
      refetchQueries: [
        {
          query: queries.getManuscriptValidations,
          variables: { manuscriptId },
        },
      ],
    })

  return toggleValidation
}

export const useAuthorVerification = ({ manuscriptId, authorId }) => {
  const [verifyAuthorMutation] = useMutation(mutations.verifyAuthor)
  const [unverifyAuthorMutation] = useMutation(mutations.unverifyAuthor)
  const [cannotVerifyAuthorMutation] = useMutation(mutations.cannotVerifyAuthor)
  const [verifyOutOfToolAuthorMutation] = useMutation(
    mutations.verifyOutOfToolAuthor,
  )
  const [unverifyOutOfToolAuthorMutation] = useMutation(
    mutations.unverifyOutOfToolAuthor,
  )

  const verifyAuthor = (possibleAuthorId) => () =>
    verifyAuthorMutation({
      variables: { manuscriptId, authorId, possibleAuthorId },
      refetchQueries: [
        {
          query: queries.getPossibleAuthorProfiles,
          variables: { authorId, manuscriptId },
        },
        {
          query: queries.getScreeningManuscript,
          variables: { manuscriptId },
        },
      ],
    })
  const unverifyAuthor = (possibleAuthorId) => () =>
    unverifyAuthorMutation({
      variables: { manuscriptId, authorId, possibleAuthorId },
      refetchQueries: [
        {
          query: queries.getPossibleAuthorProfiles,
          variables: { authorId, manuscriptId },
        },
        {
          query: queries.getScreeningManuscript,
          variables: { manuscriptId },
        },
      ],
    })

  const cannotVerifyAuthor = () =>
    cannotVerifyAuthorMutation({
      variables: { manuscriptId, authorId },
      refetchQueries: [
        {
          query: queries.getPossibleAuthorProfiles,
          variables: { authorId, manuscriptId },
        },
        {
          query: queries.getScreeningManuscript,
          variables: { manuscriptId },
        },
      ],
    })

  const verifyOutOfToolAuthor = () =>
    verifyOutOfToolAuthorMutation({
      variables: { manuscriptId, authorId },
      refetchQueries: [
        {
          query: queries.getPossibleAuthorProfiles,
          variables: { authorId, manuscriptId },
        },
        {
          query: queries.getScreeningManuscript,
          variables: { manuscriptId },
        },
      ],
    })

  const unverifyOutOfToolAuthor = () =>
    unverifyOutOfToolAuthorMutation({
      variables: { manuscriptId, authorId },
      refetchQueries: [
        {
          query: queries.getPossibleAuthorProfiles,
          variables: { authorId, manuscriptId },
        },
        {
          query: queries.getScreeningManuscript,
          variables: { manuscriptId },
        },
      ],
    })

  return {
    verifyAuthor,
    unverifyAuthor,
    cannotVerifyAuthor,
    verifyOutOfToolAuthor,
    unverifyOutOfToolAuthor,
  }
}

export function useSimilarSubmissions(manuscriptId) {
  const { data, loading } = useQuery(queries.getSimilarSubmissions, {
    variables: { manuscriptId },
    fetchPolicy: 'network-only',
  })

  const similarSubmissions = get(data, 'similarSubmissions', [])

  return {
    similarSubmissions,
    loading,
  }
}

export function useSubmissionStatusAndStepUpdatedSubscription(submissionIds) {
  const { data, loading } = useSubscription(
    subscriptions.submissionStatusAndStepUpdated,
    {
      variables: {
        submissionIds,
      },
    },
  )

  const manuscript = get(data, 'manuscript', null)

  return { manuscript, loading }
}

export function useLatestPauseReason(manuscriptId) {
  const { data } = useQuery(queries.getLatestPauseReason, {
    variables: { manuscriptId },
    fetchPolicy: 'network-only',
  })

  const latestPauseReason = get(data, 'latestPauseReason.content', null)

  return { latestPauseReason }
}
