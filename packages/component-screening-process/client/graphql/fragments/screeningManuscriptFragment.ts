import { gql } from '@apollo/client'

export const screeningManuscript = gql`
  fragment screeningManuscript on ScreeningManuscript {
    id
    title
    fileId
    version
    step
    files {
      id
      type
      providerKey
      originalFileProviderKey
      originalName
      fileName
      size
      mimeType
    }
    mainManuscriptVersions {
      id
      version
    }
    materialCheckManuscriptVersions {
      id
      version
    }
    customId
    phenomId
    abstract
    articleType
    isShortArticleSupported
    submissionId
    submissionDate
    references
    flowType
    status
    authors {
      id
      isSubmitting
      isCorresponding
      isVerifiedOutOfTool
      aff
      email
      orcid
      givenNames
      surname
      isVerified
      isOnBadDebtList
      isOnBlackList
      isOnWatchList
      hasOrcidIssue
      rorId
      rorScore
      rorAff
    }
    journal {
      id
      name
    }
    specialIssue {
      id
    }
    currentUserRole
    linkedSubmissionCustomId
  }
`
