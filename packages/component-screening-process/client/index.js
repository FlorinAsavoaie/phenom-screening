export { default as ManuscriptScreening } from './pages/ManuscriptScreening'
export { default as PreScreeningCheckTab } from './pages/PreScreeningCheckTab'
export { default as InfoValidationTab } from './pages/InfoValidationTab'

export { useNote } from './graphql/hooks'

export { default as AuthorIdentityVerificationTab } from './pages/AuthorIdentityVerificationTab'
export { parseError } from './utils'
