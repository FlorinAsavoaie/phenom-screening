import { useQuery, gql } from '@apollo/client'
import { get } from 'lodash'

import { AuditLog } from 'hindawi-shared/SchemaInterfaces'

const getAuditLogsForSubmission = gql`
  query getAuditLogsForSubmission($manuscriptId: ID!, $submissionId: ID!) {
    auditLogs: getAuditLogsForSubmission(
      manuscriptId: $manuscriptId
      submissionId: $submissionId
    ) {
      id
      created
      userRole
      comment
      action
      user {
        id
        surname
        givenNames
      }
    }
  }
`

interface AuditLogProps {
  manuscriptId: string
  submissionId: string
}

function useAuditLogsForSubmission({
  manuscriptId,
  submissionId,
}: AuditLogProps): {
  auditLogs: AuditLog[]
  loading: boolean
  error?: any
} {
  const { data, loading, error } = useQuery(getAuditLogsForSubmission, {
    variables: { manuscriptId, submissionId },
    fetchPolicy: 'network-only',
  })

  const auditLogs = get(data, 'auditLogs', [])

  return { auditLogs, loading, error }
}

export { useAuditLogsForSubmission }
