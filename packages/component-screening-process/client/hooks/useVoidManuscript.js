import { useMutation } from '@apollo/client'
import { voidManuscriptMutation } from '../graphql/mutations'

export function useVoidManuscript() {
  const [mutation] = useMutation(voidManuscriptMutation)

  function voidManuscript({ manuscriptId, additionalComments }) {
    return mutation({
      variables: { manuscriptId, reason: additionalComments },
    })
  }

  return {
    voidManuscript,
  }
}
