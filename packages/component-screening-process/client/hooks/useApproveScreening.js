import { useMutation } from '@apollo/client'
import { approveDecision } from '../graphql/mutations'

export function useApproveScreening() {
  const [mutation] = useMutation(approveDecision)

  function approveScreening({ manuscriptId, additionalComments }) {
    return mutation({
      variables: { manuscriptId, content: additionalComments },
    })
  }

  return {
    approveScreening,
  }
}
