import { useMutation } from '@apollo/client'
import { returnToCheckerMutation } from '../graphql/mutations'
import { updateManuscriptStatusInCache } from '../hooks/updateManuscriptStatusInCache'

export function useReturnToChecker() {
  const [mutation] = useMutation(returnToCheckerMutation, {
    refetchQueries: ['getAuditLogsForSubmission'],
  })

  function returnToChecker({ manuscriptId, additionalComments }) {
    return mutation({
      variables: { manuscriptId, reason: additionalComments },
      update: (
        cache,
        {
          data: {
            manuscript: { status },
          },
        },
      ) => {
        updateManuscriptStatusInCache({
          cache,
          manuscriptId,
          status,
        })
      },
    })
  }
  return { returnToChecker }
}
