import { useMutation } from '@apollo/client'
import { refuseToConsiderDecision } from '../graphql/mutations'

export function useRefuseToConsiderScreening() {
  const [mutation] = useMutation(refuseToConsiderDecision)

  function refuseToConsiderScreening({
    manuscriptId,
    additionalComments,
    rest,
  }) {
    return mutation({
      variables: { manuscriptId, note: additionalComments, content: rest },
    })
  }

  return {
    refuseToConsiderScreening,
  }
}
