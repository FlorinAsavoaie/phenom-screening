import { useMutation } from '@apollo/client'
import { returnToDraftMutation } from '../graphql/mutations'

export function useReturnToDraft() {
  const [mutation] = useMutation(returnToDraftMutation)

  function returnToDraft({ manuscriptId, additionalComments }) {
    return mutation({
      variables: { manuscriptId, content: additionalComments },
    })
  }

  return {
    returnToDraft,
  }
}
