import { useMutation } from '@apollo/client'
import * as mutations from '../graphql/mutations'

interface SignedUrlProps {
  fileId: string
  format: string
}

type SignedUrlType = (props: SignedUrlProps) => Promise<string>

interface SignedUrlPropsHookResult {
  getSignedUrl: SignedUrlType
}

export const useSignedUrl = (): SignedUrlPropsHookResult => {
  const [useSignedUrlMutation] = useMutation(mutations.getScreeningSignedUrl)

  const getSignedUrl: SignedUrlType = ({ fileId, format }) =>
    useSignedUrlMutation({ variables: { fileId, format } }).then(
      (r) => r.data.getScreeningSignedUrl,
    )

  return { getSignedUrl }
}
