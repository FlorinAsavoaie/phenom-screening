import { useMutation } from '@apollo/client'
import { unpauseManuscriptMutation } from '../graphql/mutations'
import { updateManuscriptStatusInCache } from './updateManuscriptStatusInCache'

interface UnPauseManuscriptProps {
  manuscriptId: string
}

export function useUnpauseManuscript(): any {
  const [mutation] = useMutation(unpauseManuscriptMutation, {
    refetchQueries: ['getAuditLogsForSubmission'],
  })

  function unpauseManuscript({ manuscriptId }: UnPauseManuscriptProps): any {
    return mutation({
      variables: { manuscriptId },
      update: (
        cache,
        {
          data: {
            manuscript: { status },
          },
        },
      ) => {
        updateManuscriptStatusInCache({
          cache,
          manuscriptId,
          status,
        })
      },
    })
  }
  return { unpauseManuscript }
}
