import { useState } from 'react'
import { Manuscript } from 'hindawi-shared/SchemaInterfaces'
import { RightTab } from 'hindawi-shared/client/components'
import ManuscriptInfo from 'component-screening-process/client/components/ManuscriptInfo'

import { ActivityAndMessages } from 'component-screening-process/client/components/activityAndMessages/ActivityAndMessages'

const useManuscriptRightTabs = (manuscript: Manuscript): RightTab[] => {
  const [showManuscriptInfo, setShowManuscriptInfo] = useState(false)
  const [showAuditLog, setShowAuditLog] = useState(false)
  const handleSetShowAuditLog = () => setShowAuditLog(!showAuditLog)
  const rightTabs = [
    {
      label: 'Manuscript Info',
      icon: {
        name: 'manuscripts',
      },
      handleClick: () => setShowManuscriptInfo(!showManuscriptInfo),
      component: <ManuscriptInfo manuscript={manuscript} />,
      showComponent: showManuscriptInfo,
    },
    {
      label: 'Activity & Messages',
      icon: {
        name: 'ellipse',
        mt: 1,
      },
      handleClick: handleSetShowAuditLog,
      component: (
        <ActivityAndMessages
          handleSetShowAuditLog={handleSetShowAuditLog}
          manuscript={manuscript}
        />
      ),
      showComponent: showAuditLog,
    },
  ]

  return rightTabs
}

export { useManuscriptRightTabs }
