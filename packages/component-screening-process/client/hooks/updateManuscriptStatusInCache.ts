import { screeningManuscript } from '../graphql/fragments/screeningManuscriptFragment'

interface UpdateManuscriptStatusInCacheProps {
  cache: any
  manuscriptId: string
  status: string
}
export function updateManuscriptStatusInCache({
  cache,
  manuscriptId,
  status,
}: UpdateManuscriptStatusInCacheProps): void {
  const manuscript = cache.readFragment({
    id: `ScreeningManuscript:${manuscriptId}`,
    fragment: screeningManuscript,
    fragmentName: 'screeningManuscript',
  })

  const updatedManuscript = {
    ...manuscript,
    status,
  }
  cache.writeFragment({
    id: `ScreeningManuscript:${manuscriptId}`,
    fragment: screeningManuscript,
    fragmentName: 'screeningManuscript',
    data: updatedManuscript,
  })
}
