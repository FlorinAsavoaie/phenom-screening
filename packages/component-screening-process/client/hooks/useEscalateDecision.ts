import { useMutation } from '@apollo/client'
import { escalateManuscriptMutation } from '../graphql/mutations'
import { updateManuscriptStatusInCache } from './updateManuscriptStatusInCache'

interface EscalateManuscriptProps {
  manuscriptId: string
  additionalComments: string
}
export function useEscalateManuscript(): any {
  const [mutation] = useMutation(escalateManuscriptMutation, {
    refetchQueries: ['getAuditLogsForSubmission'],
  })

  function escalateManuscript({
    manuscriptId,
    additionalComments,
  }: EscalateManuscriptProps): any {
    return mutation({
      variables: { manuscriptId, reason: additionalComments },
      update: (
        cache,
        {
          data: {
            manuscript: { status },
          },
        },
      ) => {
        updateManuscriptStatusInCache({
          cache,
          manuscriptId,
          status,
        })
      },
    })
  }
  return { escalateManuscript }
}
