import { useMutation } from '@apollo/client'
import { pauseManuscriptMutation } from '../graphql/mutations'
import { updateManuscriptStatusInCache } from './updateManuscriptStatusInCache'

interface PauseManuscriptProps {
  manuscriptId: string
  additionalComments: string
}

export function usePauseManuscript(): any {
  const [mutation] = useMutation(pauseManuscriptMutation, {
    refetchQueries: ['getAuditLogsForSubmission'],
  })

  function pauseManuscript({
    manuscriptId,
    additionalComments,
  }: PauseManuscriptProps): any {
    return mutation({
      variables: { manuscriptId, reason: additionalComments },
      update: (
        cache,
        {
          data: {
            manuscript: { status },
          },
        },
      ) => {
        updateManuscriptStatusInCache({
          cache,
          manuscriptId,
          status,
        })
      },
    })
  }
  return { pauseManuscript }
}
