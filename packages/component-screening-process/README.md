# Component Screening Process

This component listens to the following events:

Submission Events: 
- `ManuscriptSubmitted` -- triggered from the Review App on manuscript submission
- `SubmissionWithdrawn`
- `SubmissionEdited`

Screening Internal Events:
- `FileConverted`
- `LanguageChecked`
- `SuspiciousWordsProcessingDone`
- `SanctionListCheckDone`
- `SimilarityCheckFinished`
- `AuthorProfilesSearchFinished`
- `AutomaticChecksDone`

User Events:
- `UserORCIDAdded`
- `UserORCIDRemoved`

After receiving the manuscript from Review, component-screening-process triggers the `ManuscriptIngested` event with the following format:

```
{
      event: 'ManuscriptIngested',
      data: {
        authors,
        manuscriptId,
        fileId,
        originalFileProviderKey,
      },
  }
```
