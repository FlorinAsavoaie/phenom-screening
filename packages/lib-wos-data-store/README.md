# lib-wos-data-store

A set of utility functions that help managing an OpenSearch cluster used for ingesting authors data from Web of Science.

## Usage

`import { initialize } from '@hindawi/lib-wos-data-store/wosDataRepository'`

Later, you should call the `initialize` function as `initialize({ config })` where config is an instance of **https://www.npmjs.com/package/config**.

Your config set-up should take a set of properties under this form:

```
wos: {

    elasticsearch: {

        node: '[opensearch-node-endpoint]',

        index: '[opensearch-index-name]',

        username: '[opensearch-auth-username]',

        password: '[opensearch-auth-password]',

    },

},
```

The previous described function call will return an object containing these methods:

- indexWoSAuthors
- searchWoSAuthors
- getWoSProfilesByEmailOrOrcid
- deleteWoSProfilesByManuscript
- indexIngestedFiles
- getLastIngestedFileDetails

For more granular technical details about the functionality, you may read the source code located inside src/lib/wosDataRepository.js.
