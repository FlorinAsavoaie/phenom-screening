const { Client } = require('@elastic/elasticsearch')
const { flatMap } = require('lodash')

module.exports.initialize = function initialize({ configService }) {
  const client = new Client({
    node: configService.get('wos.elasticsearch.node'),
    auth: {
      username: configService.get('wos.elasticsearch.username'),
      password: configService.get('wos.elasticsearch.password'),
    },
  })
  const index = configService.get('wos.elasticsearch.index')

  return {
    indexWoSAuthors,
    searchWoSAuthors,
    getWoSProfilesByEmailOrOrcid,
    deleteWoSProfilesByManuscript,
    indexIngestedFiles,
    getLastIngestedFileDetails,
  }

  async function indexWoSAuthors({ wosAuthors, abstract, manuscriptWosId }) {
    await deleteWoSProfilesByManuscript(manuscriptWosId)

    if (wosAuthors.length === 0) return

    const body = flatMap(wosAuthors, (doc) => [
      { index: { _index: index } },
      { ...doc, manuscriptAbstract: abstract },
    ])

    await client.bulk({
      body,
    })
  }

  async function indexIngestedFiles(fileName, summary) {
    await client.index({
      index: configService.get('wos.elasticsearch.indexIngestionDetails'),
      body: {
        fileName,
        summary,
      },
    })
  }

  async function deleteWoSProfilesByManuscript(manuscriptWosId) {
    await client.deleteByQuery({
      index,
      refresh: true,
      body: {
        query: {
          match: {
            'manuscriptWosId.keyword': manuscriptWosId,
          },
        },
      },
    })
  }

  async function searchWoSAuthors(authorDetails) {
    const esResult = await client.search({
      index,
      body: {
        size: 1000, // Specifying this size the query will return the first 1000 best authors from this query otherwise will return 10 authors which is the default. This value can be changed, we stick to this value only to have enough authors.
        query: {
          bool: {
            should: [
              {
                bool: {
                  must: [
                    {
                      match: {
                        email: authorDetails.email,
                      },
                    },
                    {
                      match: {
                        lastName: authorDetails.lastName,
                      },
                    },
                  ],
                },
              },
              {
                bool: {
                  must: [
                    {
                      match: {
                        email: authorDetails.email,
                      },
                    },
                    {
                      match: {
                        firstName: authorDetails.firstName,
                      },
                    },
                  ],
                },
              },
              {
                bool: {
                  must: [
                    {
                      match: {
                        email: authorDetails.email,
                      },
                    },
                    {
                      match: {
                        affiliation: authorDetails.affiliation || 'null',
                      },
                    },
                  ],
                },
              },
              {
                bool: {
                  must: [
                    {
                      match: {
                        lastName: authorDetails.lastName,
                      },
                    },
                    {
                      match: {
                        firstName: authorDetails.firstName,
                      },
                    },
                  ],
                },
              },
              {
                bool: {
                  must: [
                    {
                      match: {
                        lastName: authorDetails.lastName,
                      },
                    },
                    {
                      match: {
                        affiliation: authorDetails.affiliation || 'null',
                      },
                    },
                  ],
                },
              },
            ],
          },
        },
      },
    })

    return esResult.body.hits.hits.map((esItem) => esItem._source)
  }

  async function getWoSProfilesByEmailOrOrcid({ email, orcid }) {
    const result = await client.search({
      index,
      body: {
        size: 2000,
        query: {
          bool: {
            should: [
              {
                match: {
                  'email.keyword': email,
                },
              },
              {
                match: {
                  'orcId.keyword': orcid || 'null', // default "null" value required for elastic query
                },
              },
            ],
          },
        },
      },
    })

    return result.body.hits.hits.map((esItem) => esItem._source)
  }

  async function getLastIngestedFileDetails() {
    let lastIngestedFileDetails = {
      Key: '',
      LastModified: new Date('1900'),
    }
    const indices = await client.indices.exists({
      index: configService.get('wos.elasticsearch.indexIngestionDetails'),
    })

    if (!indices.body) {
      return lastIngestedFileDetails
    }
    const lastIngestedFile = await client.search({
      index: configService.get('wos.elasticsearch.indexIngestionDetails'),
      body: {
        query: {
          match_all: {},
        },
        sort: { 'fileName.LastModified': 'desc' },
        size: 1,
      },
    })

    if (lastIngestedFile) {
      lastIngestedFileDetails = {
        Key: lastIngestedFile.body.hits.hits[0]._source.fileName.Key,
        LastModified:
          lastIngestedFile.body.hits.hits[0]._source.fileName.LastModified,
      }
    }
    return lastIngestedFileDetails
  }
}
