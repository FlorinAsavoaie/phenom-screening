module.exports = {
  aws: {
    region: process.env.AWS_REGION,
    s3: {
      accessKeyId: process.env.AWS_S3_ACCESS_KEY,
      secretAccessKey: process.env.AWS_S3_SECRET_KEY,
      wosBucket: process.env.AWS_S3_WOS_BUCKET,
    },
  },
  wos: {
    ftp: {
      host: process.env.WOS_FTP_HOST,
      user: process.env.WOS_FTP_USER,
      password: process.env.WOS_FTP_PASSWORD,
      path: process.env.WOS_FTP_PATH,
    },
    elasticsearch: {
      node: process.env.WOS_ELASTICSEARCH_NODE,
      index: process.env.WOS_ELASTICSEARCH_INDEX,
      username: process.env.WOS_ELASTICSEARCH_USERNAME,
      password: process.env.WOS_ELASTICSEARCH_PASSWORD,
      indexIngestionDetails:
        process.env.WOS_ELASTICSEARCH_INDEX_INGESTION_DETAILS,
    },
  },
}
