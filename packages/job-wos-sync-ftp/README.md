# Job wos sync ftp

The purpose of this service is to save archives from wos sftp to s3.

### Required Env vars

```sh
AWS_REGION=eu-west-1

AWS_S3_WOS_BUCKET=****

WOS_FTP_HOST=****
WOS_FTP_USER=****
WOS_FTP_PASSWORD=****
WOS_FTP_PATH=****
```

### How to test it:
`yarn start`

### How to deploy it
Create image and deploy it. 
The `yarn synth` command will generate kubernetes manifests in `./dist-k8s` Those can be used with `kubectl apply -f ./dist-k8s/` to apply the changes on the cluster or `kubectl apply -f ./dist-k8s/ -n <namespace>` 


# Update yearly backups script

### Required Env vars

```sh
AWS_REGION=eu-west-1

AWS_S3_WOS_BUCKET=****

WOS_FTP_HOST=****
WOS_FTP_USER=****
WOS_FTP_PASSWORD=****
WOS_FTP_PATH=****
```

### How to run it:
`yarn updateYearlyBackups`
