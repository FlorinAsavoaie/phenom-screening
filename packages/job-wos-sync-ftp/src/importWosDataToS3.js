require('dotenv').config()
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')
const config = require('config')
const s3Service = require('./services/s3Service')
const { createFTPConnection } = require('./services/ftpService')
const { chain } = require('lodash')
const { handleDelFile } = require('./handleDelFile')
const { handleTxtFile } = require('./handleTxtFile')
const { handleTarFile } = require('./handleTarFile')
const wosDataRepository =
  require('@hindawi/lib-wos-data-store').wosDataRepository.initialize({
    configService: config,
  })

const BUCKET = config.get('aws.s3.wosBucket')
const FOLDER = 'UNPROCESSED_FILES'

;(async () => {
  const ftpConnection = await createFTPConnection()

  const lastSyncedFile = await getLastSyncedFile()

  const newFiles = await getNewFilesFromWoS(ftpConnection, lastSyncedFile)

  logger.info(`New files to upload: ${newFiles.length}`)

  await Promise.each(newFiles, async (file) => {
    const { name: archiveName } = file
    logger.info(`\nBegin to process ${archiveName}`)

    const streamData = await ftpConnection.streamDownload(archiveName)

    if (archiveName.endsWith('.del.gz')) {
      await handleDelFile({
        bucket: BUCKET,
        streamData,
        archiveName,
        prefix: FOLDER,
      })
    } else if (archiveName.endsWith('.txt.gz')) {
      await handleTxtFile({
        bucket: BUCKET,
        streamData,
        archiveName,
        prefix: FOLDER,
      })
    } else {
      await handleTarFile({ bucket: BUCKET, streamData, prefix: FOLDER })
    }

    logger.info(`Finished to process ${archiveName}`)
  })

  ftpConnection.end()
})()

async function getLastSyncedFile() {
  const s3Files = await s3Service.getAllS3Files({
    Bucket: BUCKET,
    Prefix: FOLDER,
  })

  let lastSyncedFile = chain(s3Files)
    .map((file) => ({ ...file, Key: file.Key.split('/').slice(1).join('/') })) // remove Folder prefix
    .sortBy(['LastModified', 'Key'])
    .last()
    .value()

  if (!lastSyncedFile) {
    lastSyncedFile = await wosDataRepository.getLastIngestedFileDetails()
  }
  return lastSyncedFile
}

async function getNewFilesFromWoS(ftpConnection, lastSyncedFile) {
  const wosUpdateFiles = await ftpConnection.list()

  const ftpFiles = chain(wosUpdateFiles)
    .filter(
      (wosFile) =>
        wosFile.name.endsWith('.del.gz') ||
        wosFile.name.endsWith('.tar.gz') ||
        wosFile.name.endsWith('.txt.gz'),
    )
    .sortBy(['lastModified', 'name'])
    .value()

  if (!lastSyncedFile) return ftpFiles

  const lastSyncedFileName =
    lastSyncedFile.Key.endsWith('.del') || lastSyncedFile.Key.endsWith('.txt')
      ? lastSyncedFile.Key.concat('.gz')
      : lastSyncedFile.Key.split('/').shift().concat('.tar.gz')

  const lastSyncedFileInWoSDate = chain(ftpFiles)
    .find((wosFile) => wosFile.name === lastSyncedFileName)
    .get('lastModified', new Date('1900')) // default time if the last modified file from s3 doesn't exist in ftp(ex 2020.tar.gz)
    .value()

  return ftpFiles.filter(
    (wosFile) => wosFile.lastModified >= lastSyncedFileInWoSDate,
  )
}
