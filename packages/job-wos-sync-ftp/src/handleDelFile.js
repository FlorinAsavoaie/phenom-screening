const stream = require('stream')
const s3Service = require('./services/s3Service')
const gunzip = require('gunzip-maybe')
const logger = require('@pubsweet/logger')

async function handleDelFile({ bucket, streamData, archiveName, prefix }) {
  const checkAsciiInput = new stream.Transform({
    transform(chunk, _, callback) {
      const asciiChars = /^[\n\r\x20-\x7F]*$/i
      if (!asciiChars.test(chunk.toString())) {
        callback(new Error('Content contains non-ASCII characters'))
      }
      this.push(chunk)
      return callback()
    },
  })

  const fileName = archiveName.slice(0, -3)

  const fileExistInS3 = await s3Service.headObject({
    key: fileName,
    bucket,
  })

  if (fileExistInS3) {
    logger.info(`File ${fileName} already exist in s3.`)
    return
  }

  const { writeStream, writeStreamPromise } = s3Service.uploadFromStream({
    fileName: `${prefix}/${fileName}`,
    bucket,
  })

  streamData.pipe(gunzip()).pipe(checkAsciiInput).pipe(writeStream)

  try {
    await writeStreamPromise
    logger.info(`File ${fileName} uploaded to s3.`)
  } catch (error) {
    throw new Error(error)
  }
}

module.exports = { handleDelFile }
