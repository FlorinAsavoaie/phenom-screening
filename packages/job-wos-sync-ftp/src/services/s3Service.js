const { S3 } = require('@aws-sdk/client-s3')
const { Upload } = require('@aws-sdk/lib-storage')
const config = require('config')
const stream = require('stream')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

module.exports.headObject = async ({ key, bucket }) => {
  let file
  try {
    file = await s3.headObject({
      Bucket: bucket,
      Key: key,
    })
  } catch {
    file = null
  }
  return file
}

async function upload(uploadParams) {
  const upload = new Upload({
    params: uploadParams,
    client: s3,
  })
  return upload.done()
}

module.exports.uploadFromStream = ({ fileName, bucket }) => {
  const streamData = new stream.PassThrough()

  const uploadParams = {
    Key: fileName,
    Body: streamData,
    Bucket: bucket,
  }

  return {
    writeStream: streamData,
    writeStreamPromise: upload(uploadParams),
  }
}

const listAllObjects = (s3, params) =>
  s3
    .listObjectsV2(params)
    .then(({ Contents, IsTruncated, NextContinuationToken }) =>
      IsTruncated && NextContinuationToken
        ? listAllObjects(
            s3,
            Object.assign({}, params, {
              ContinuationToken: NextContinuationToken,
            }),
          ).then((x) => Contents.concat(x))
        : Contents,
    )

module.exports.getAllS3Files = async (params) => listAllObjects(s3, params)
