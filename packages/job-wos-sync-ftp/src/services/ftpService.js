const pathUtils = require('path')
const config = require('config')

const { Client } = require('ssh2')

const FTP_PATH = config.get('wos.ftp.path')

async function createFTPConnection(
  options = {
    host: config.get('wos.ftp.host'),
    port: 22,
    user: config.get('wos.ftp.user'),
    password: config.get('wos.ftp.password'),
  },
) {
  const ftpClient = new Client()
  ftpClient.connect(options)

  return new Promise((resolve, reject) => {
    ftpClient.on('ready', () => {
      ftpClient.sftp((err, sftp) => {
        if (err) {
          reject(err)
        }

        resolve({
          list: creatListCmd(sftp),
          streamDownload: createStreamDownloadCmd(sftp),
          end: ftpClient.end.bind(ftpClient),
        })
      })
    })
  })

  function createStreamDownloadCmd(sftp) {
    return async function streamDownload(path) {
      return new Promise((resolve) => {
        const fileStream = sftp.createReadStream(pathUtils.join(FTP_PATH, path))

        resolve(fileStream)
      })
    }
  }

  function creatListCmd(sftp) {
    return async function list() {
      return new Promise((resolve, reject) => {
        sftp.readdir(FTP_PATH, (err, files) => {
          if (err) {
            reject(err)
          }
          const mappedFiles = files.map((file) => ({
            name: file.filename,
            lastModified: file.attrs.mtime,
            size: file.attrs.size,
          }))

          resolve(mappedFiles)
        })
      })
    }
  }
}

module.exports = {
  createFTPConnection,
}
