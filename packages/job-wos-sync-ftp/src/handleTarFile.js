const stream = require('stream')
const s3Service = require('./services/s3Service')
const gunzip = require('gunzip-maybe')
const logger = require('@pubsweet/logger')
const tar = require('tar-stream')

async function handleTarFile({ bucket, streamData, prefix }) {
  const tarExtractStream = createTarExtractionStream(bucket, prefix)

  return new Promise((resolve, reject) => {
    stream.pipeline(streamData, gunzip(), tarExtractStream, (err) => {
      if (err) {
        reject()
      }
      resolve()
    })
  })
}

function createTarExtractionStream(bucket, prefix) {
  const tarExtract = tar.extract()

  tarExtract.on('entry', async (header, stream, next) => {
    if (header.type !== 'file') {
      stream.resume()
      return next()
    }

    const fileName = header.name.endsWith('.xml.gz')
      ? header.name.slice(0, -3)
      : header.name

    const fileExistInS3 = await s3Service.headObject({
      key: fileName,
      bucket,
    })

    if (fileExistInS3) {
      logger.info(`File ${fileName} already exist in s3.`)
      stream.resume()
      return next()
    }

    if (header.name.endsWith('.xml.gz')) {
      const { writeStream, writeStreamPromise } = s3Service.uploadFromStream({
        fileName: `${prefix}/${fileName}`,
        bucket,
      })

      stream.pipe(gunzip()).pipe(writeStream)

      try {
        await writeStreamPromise
        logger.info(`File ${fileName} uploaded to s3.`)
        return next()
      } catch (error) {
        throw new Error(error)
      }
    } else {
      const { writeStream, writeStreamPromise } = s3Service.uploadFromStream({
        fileName: `${prefix}/${fileName}`,
        bucket,
      })

      stream.pipe(writeStream)

      try {
        await writeStreamPromise
        logger.info(`File ${fileName} uploaded to s3.`)
        return next()
      } catch (error) {
        throw new Error(error)
      }
    }
  })

  return tarExtract
}

module.exports = { handleTarFile }
