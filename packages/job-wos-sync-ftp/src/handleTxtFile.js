const s3Service = require('./services/s3Service')
const gunzip = require('gunzip-maybe')
const logger = require('@pubsweet/logger')

async function handleTxtFile({ bucket, streamData, archiveName }) {
  const fileName = archiveName.slice(0, -3)

  const fileExistInS3 = await s3Service.headObject({
    key: fileName,
    bucket,
  })

  if (fileExistInS3) {
    logger.info(`File ${fileName} already exist in s3.`)
    return
  }

  const { writeStream, writeStreamPromise } = s3Service.uploadFromStream({
    fileName,
    bucket,
  })

  streamData.pipe(gunzip()).pipe(writeStream)

  try {
    await writeStreamPromise
    logger.info(`File ${fileName} uploaded to s3.`)
  } catch (error) {
    throw new Error(error)
  }
}

module.exports = { handleTxtFile }
