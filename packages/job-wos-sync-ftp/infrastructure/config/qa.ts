import { WithAwsSecretsCronProps } from '@hindawi/phenom-charts'

export const cronProps: WithAwsSecretsCronProps = {
  secretNames: ['qa/screening/job-wos-sync-ftp'],
  cronProps: {
    schedule: '0 0 * * *', // everyday at 00:00.
    image: {
      repository:
        '496598730381.dkr.ecr.eu-west-1.amazonaws.com/job-wos-sync-ftp',
      tag: 'latest',
    },
    metadata: {
      labels: {
        owner: 'QAlas',
      },
    },
    concurrencyPolicy: 'Forbid',
    restartPolicy: 'Never',
    resources: {
      limits: {
        memory: '1300Mi',
      },
      requests: {
        memory: '1300Mi',
      },
    },
  },
}
