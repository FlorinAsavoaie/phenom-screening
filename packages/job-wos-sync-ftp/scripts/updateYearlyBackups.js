require('dotenv').config()
const config = require('config')
const logger = require('@pubsweet/logger')
const { Promise } = require('bluebird')
const unzipper = require('unzipper')
const gunzip = require('gunzip-maybe')
const s3Service = require('../src/services/s3Service')
const { createFTPConnection } = require('../src/services/ftpService')
const { chain } = require('lodash')

const BUCKET = config.get('aws.s3.wosBucket')

async function updateYearlyBackups() {
  const ftpConnection = await createFTPConnection()

  const wosUpdateFiles = await ftpConnection.list()

  const filesToUpload = chain(wosUpdateFiles)
    .filter((file) => file.name.endsWith('.zip'))
    .sortBy('name')
    .value()

  logger.info(`New files to upload: ${filesToUpload.length}`)

  await Promise.each(filesToUpload, async (file) =>
    saveFileToS3(file, ftpConnection),
  )

  ftpConnection.end()
}

async function saveFileToS3(file, ftpConnection) {
  return new Promise(async (resolve, reject) => {
    logger.info(`\nBegin to upload file: ${file.name}`)

    const archiveName = file.name.split('.').shift()
    const streamData = await ftpConnection.streamDownload(file.name)

    return streamData
      .pipe(unzipper.Parse())
      .on('entry', async (entry) => {
        if (entry.type === 'File' && entry.path.endsWith('.xml.gz')) {
          const xmlFileName = entry.path.substring(0, entry.path.length - 3)

          const {
            writeStream,
            writeStreamPromise,
          } = s3Service.uploadFromStream({
            fileName: `${archiveName}/${xmlFileName}`,
            bucket: BUCKET,
          })
          entry.pipe(gunzip()).pipe(writeStream)
          try {
            await writeStreamPromise
            logger.info(`File ${archiveName}/${xmlFileName} uploaded to s3.`)
          } catch (error) {
            throw new Error(error)
          }
        } else {
          const {
            writeStream,
            writeStreamPromise,
          } = s3Service.uploadFromStream({
            fileName: `${archiveName}/${entry.path}`,
            bucket: BUCKET,
          })

          entry.pipe(writeStream)

          try {
            await writeStreamPromise
            logger.info(`File ${archiveName}/${entry.path} uploaded to s3.`)
          } catch (error) {
            throw new Error(error)
          }
        }
      })
      .on('finish', async () => {
        resolve()
      })
      .on('error', (err) => {
        reject(err)
      })
  })
}

updateYearlyBackups()
