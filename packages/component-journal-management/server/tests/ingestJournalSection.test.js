const { JournalSection } = require('@pubsweet/models')
const { journalSectionFactory } = require('component-model').factories

const useCases = require('../src/use-cases')

describe('Ingest Journal Section', () => {
  it('should not save any section', async () => {
    const journalSection = {
      save: jest.fn(),
    }
    const findOneBy = jest
      .spyOn(JournalSection, 'findOneBy')
      .mockReturnValue({})
    const create = jest
      .spyOn(journalSectionFactory, 'create')
      .mockReturnValue(journalSection)

    await useCases.ingestJournalSectionUseCase
      .initialize({
        models: { JournalSection },
        factories: { journalSectionFactory },
      })
      .execute({
        id: 'journal-id-123',
        sections: [],
      })

    expect(findOneBy).not.toHaveBeenCalled()
    expect(create).not.toHaveBeenCalled()
    expect(journalSection.save).not.toHaveBeenCalled()
  })

  it('should save only the section', async () => {
    const journalSection = {
      save: jest.fn(),
    }
    const findOneBy = jest
      .spyOn(JournalSection, 'findOneBy')
      .mockReturnValueOnce({
        id: 'section-id-1',
        name: 'Some Section',
      })
      .mockReturnValueOnce(null)
    const create = jest
      .spyOn(journalSectionFactory, 'create')
      .mockReturnValueOnce(journalSection)

    await useCases.ingestJournalSectionUseCase
      .initialize({
        models: { JournalSection },
        factories: { journalSectionFactory },
      })
      .execute({
        id: 'journal-id-123',
        sections: [
          {
            id: 'section-id-1',
            name: 'Some Section',
          },
          {
            id: 'section-id-2',
            name: 'Some Section 2',
          },
        ],
      })

    expect(findOneBy).toHaveBeenCalledTimes(2)
    expect(findOneBy).toHaveBeenNthCalledWith(1, {
      queryObject: { id: 'section-id-1' },
    })
    expect(findOneBy).toHaveBeenNthCalledWith(2, {
      queryObject: { id: 'section-id-2' },
    })

    expect(create).toHaveBeenCalledTimes(1)
    expect(create).toHaveBeenCalledWith({
      id: 'section-id-2',
      journalId: 'journal-id-123',
      name: 'Some Section 2',
    })
    expect(journalSection.save).toHaveBeenCalledTimes(1)
    expect(journalSection.save).toHaveBeenCalled()
  })
})
