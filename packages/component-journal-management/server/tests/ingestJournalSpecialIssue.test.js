const { SpecialIssue } = require('@pubsweet/models')
const { specialIssueFactory } = require('component-model').factories

const useCases = require('../src/use-cases')

describe('Ingest Journal Special Issue', () => {
  it('should not save any special issue', async () => {
    const specialIssue = {
      save: jest.fn(),
    }
    const findOneBy = jest.spyOn(SpecialIssue, 'findOneBy').mockReturnValue({})
    const create = jest
      .spyOn(specialIssueFactory, 'create')
      .mockReturnValue(specialIssue)

    await useCases.ingestJournalSpecialIssueUseCase
      .initialize({
        models: { SpecialIssue },
        factories: { specialIssueFactory },
      })
      .execute({
        id: 'journal-id-123',
        specialIssues: [],
      })

    expect(findOneBy).not.toHaveBeenCalled()
    expect(create).not.toHaveBeenCalled()
    expect(specialIssue.save).not.toHaveBeenCalled()
  })

  it('should save only the special issue that is not available', async () => {
    const specialIssue = {
      save: jest.fn(),
    }
    const findOneBy = jest
      .spyOn(SpecialIssue, 'findOneBy')
      .mockReturnValueOnce({
        id: 'special-issue-id-1',
        name: 'Some Special Issue 1',
      })
      .mockReturnValueOnce(null)
    const create = jest
      .spyOn(specialIssueFactory, 'create')
      .mockReturnValueOnce(specialIssue)

    await useCases.ingestJournalSpecialIssueUseCase
      .initialize({
        models: { SpecialIssue },
        factories: { specialIssueFactory },
      })
      .execute({
        id: 'journal-id-123',
        specialIssues: [
          {
            id: 'special-issue-id-1',
            name: 'Some Special Issue 1',
          },
          {
            id: 'special-issue-id-2',
            name: 'Some Special Issue 2',
          },
        ],
      })

    expect(findOneBy).toHaveBeenCalledTimes(2)
    expect(findOneBy).toHaveBeenNthCalledWith(1, {
      queryObject: { id: 'special-issue-id-1' },
    })
    expect(findOneBy).toHaveBeenNthCalledWith(2, {
      queryObject: { id: 'special-issue-id-2' },
    })

    expect(create).toHaveBeenCalledTimes(1)
    expect(create).toHaveBeenCalledWith({
      id: 'special-issue-id-2',
      journalId: 'journal-id-123',
      name: 'Some Special Issue 2',
    })
    expect(specialIssue.save).toHaveBeenCalledTimes(1)
    expect(specialIssue.save).toHaveBeenCalled()
  })
})
