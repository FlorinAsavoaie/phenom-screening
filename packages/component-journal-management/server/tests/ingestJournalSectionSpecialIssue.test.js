const { SpecialIssue } = require('@pubsweet/models')
const { specialIssueFactory } = require('component-model').factories

const useCases = require('../src/use-cases')

describe('Ingest Journal Section Special Issue', () => {
  it('should not save any special issue', async () => {
    const specialIssue = {
      save: jest.fn(),
    }
    const findOneBy = jest.spyOn(SpecialIssue, 'findOneBy').mockReturnValue({})
    const create = jest
      .spyOn(specialIssueFactory, 'create')
      .mockReturnValue(specialIssue)

    await useCases.ingestJournalSectionSpecialIssueUseCase
      .initialize({
        models: { SpecialIssue },
        factories: { specialIssueFactory },
      })
      .execute({
        id: 'journal-id-123',
        sections: [],
      })

    expect(findOneBy).not.toHaveBeenCalled()
    expect(create).not.toHaveBeenCalled()
    expect(specialIssue.save).not.toHaveBeenCalled()
  })

  it('should save only the special issue that is not available', async () => {
    const specialIssue = {
      save: jest.fn(),
    }
    const findOneBy = jest
      .spyOn(SpecialIssue, 'findOneBy')
      .mockReturnValueOnce({
        id: 'special-issue-id-1',
        name: 'Some Special Issue Linked to Section 1',
      })
      .mockReturnValueOnce(null)
    const create = jest
      .spyOn(specialIssueFactory, 'create')
      .mockReturnValueOnce(specialIssue)

    await useCases.ingestJournalSectionSpecialIssueUseCase
      .initialize({
        models: { SpecialIssue },
        factories: { specialIssueFactory },
      })
      .execute({
        id: 'journal-id-123',
        sections: [
          {
            id: 'section-id-1',
            name: 'Some Section 1',
            specialIssues: [
              {
                id: 'special-issue-id-1',
                name: 'Some Special Issue Linked to Section 1',
              },
            ],
          },
          {
            id: 'section-id-1',
            name: 'Some Section 2',
            specialIssues: [
              {
                id: 'special-issue-id-2',
                name: 'Some Special Issue Linked to Section 2',
              },
            ],
          },
        ],
      })

    expect(findOneBy).toHaveBeenCalledTimes(2)
    expect(findOneBy).toHaveBeenNthCalledWith(1, {
      queryObject: { id: 'special-issue-id-1' },
    })
    expect(findOneBy).toHaveBeenNthCalledWith(2, {
      queryObject: { id: 'special-issue-id-2' },
    })

    expect(create).toHaveBeenCalledTimes(1)
    expect(create).toHaveBeenCalledWith({
      id: 'special-issue-id-2',
      journalId: 'journal-id-123',
      journalSectionId: 'section-id-1',
      name: 'Some Special Issue Linked to Section 2',
    })
    expect(specialIssue.save).toHaveBeenCalledTimes(1)
    expect(specialIssue.save).toHaveBeenCalled()
  })
})
