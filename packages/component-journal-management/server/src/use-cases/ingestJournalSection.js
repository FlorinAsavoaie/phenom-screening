const { Promise } = require('bluebird')

function initialize({
  models: { JournalSection },
  factories: { journalSectionFactory },
}) {
  return {
    execute,
  }

  async function execute({ id: journalId, sections }) {
    await Promise.each(sections, async ({ id, name }) => {
      const existingJournalSection = await JournalSection.findOneBy({
        queryObject: {
          id,
        },
      })

      if (existingJournalSection) {
        return
      }

      const journalSection = journalSectionFactory.create({
        id,
        journalId,
        name,
      })
      await journalSection.save()
    })
  }
}
module.exports = {
  initialize,
}
