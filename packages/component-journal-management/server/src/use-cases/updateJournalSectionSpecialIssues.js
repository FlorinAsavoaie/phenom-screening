function initialize({ Journal, JournalSection, SpecialIssue }) {
  async function execute(data) {
    const journal = await Journal.find(data.id)

    journal.sections = data.sections.map((section) => {
      const specialIssues = section.specialIssues.map(
        (si) =>
          new SpecialIssue({
            id: si.id,
            name: si.name,
          }),
      )

      return new JournalSection({
        id: section.id,
        name: section.name,
        specialIssues,
      })
    })

    return journal.saveGraph()
  }
  return { execute }
}

module.exports = {
  initialize,
}
