function initialize({ Journal }) {
  async function execute(journalData) {
    const journal = await Journal.find(journalData.id)

    journal.updateProperties({
      name: journalData.name,
      publisherName: journalData.publisherName,
    })

    return journal.save()
  }
  return { execute }
}

module.exports = {
  initialize,
}
