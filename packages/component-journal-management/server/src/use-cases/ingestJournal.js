function initialize({ Journal }) {
  async function execute(data) {
    const journal = new Journal({
      id: data.id,
      name: data.name,
      publisherName: data.publisherName,
    })
    return journal.save()
  }
  return { execute }
}

module.exports = {
  initialize,
}
