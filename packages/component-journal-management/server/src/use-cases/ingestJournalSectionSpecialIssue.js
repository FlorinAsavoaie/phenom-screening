const { Promise } = require('bluebird')

function initialize({
  models: { SpecialIssue },
  factories: { specialIssueFactory },
}) {
  return {
    execute,
  }

  async function execute({ id: journalId, sections }) {
    await Promise.each(
      sections,
      async ({ id: journalSectionId, specialIssues }) => {
        await Promise.each(specialIssues, async ({ id, name }) => {
          const existingSpecialIssue = await SpecialIssue.findOneBy({
            queryObject: {
              id,
            },
          })

          if (existingSpecialIssue) {
            return
          }

          const specialIssue = specialIssueFactory.create({
            id,
            journalId,
            journalSectionId,
            name,
          })
          await specialIssue.save()
        })
      },
    )
  }
}
module.exports = {
  initialize,
}
