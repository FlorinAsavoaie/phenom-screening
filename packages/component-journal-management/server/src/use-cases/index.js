const ingestJournalUseCase = require('./ingestJournal')
const ingestJournalSectionUseCase = require('./ingestJournalSection')
const ingestJournalSpecialIssueUseCase = require('./ingestJournalSpecialIssue')
const ingestJournalSectionSpecialIssueUseCase = require('./ingestJournalSectionSpecialIssue')
const updateJournalUseCase = require('./updateJournal')
const updateJournalSectionsUseCase = require('./updateJournalSections')
const updateJournalSpecialIssuesUseCase = require('./updateJournalSpecialIssues')
const updateJournalSectionSpecialIssuesUseCase = require('./updateJournalSectionSpecialIssues')

module.exports = {
  ingestJournalUseCase,
  ingestJournalSectionUseCase,
  ingestJournalSpecialIssueUseCase,
  ingestJournalSectionSpecialIssueUseCase,
  updateJournalUseCase,
  updateJournalSectionsUseCase,
  updateJournalSpecialIssuesUseCase,
  updateJournalSectionSpecialIssuesUseCase,
}
