function initialize({ Journal, JournalSection }) {
  async function execute(data) {
    const journal = await Journal.find(data.id)

    journal.sections = data.sections.map(
      (section) =>
        new JournalSection({
          id: section.id,
          name: section.name,
        }),
    )

    return journal.saveGraph()
  }
  return { execute }
}

module.exports = {
  initialize,
}
