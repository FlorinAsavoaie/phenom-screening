function initialize({ Journal, SpecialIssue }) {
  async function execute(data) {
    const journal = await Journal.find(data.id)

    journal.specialIssues = data.specialIssues.map(
      (specialIssue) =>
        new SpecialIssue({
          id: specialIssue.id,
          name: specialIssue.name,
        }),
    )

    return journal.saveGraph()
  }
  return { execute }
}

module.exports = {
  initialize,
}
