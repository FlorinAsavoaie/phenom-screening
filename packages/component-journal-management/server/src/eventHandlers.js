const models = require('@pubsweet/models')
const { factories } = require('component-model')

const useCases = require('./use-cases')

module.exports = {
  async JournalAdded(data) {
    return useCases.ingestJournalUseCase.initialize(models).execute(data)
  },
  async JournalUpdated(data) {
    return useCases.updateJournalUseCase.initialize(models).execute(data)
  },
  async JournalSectionUpdated(data) {
    return useCases.updateJournalSectionsUseCase
      .initialize(models)
      .execute(data)
  },
  async JournalSpecialIssueUpdated(data) {
    return useCases.updateJournalSpecialIssuesUseCase
      .initialize(models)
      .execute(data)
  },
  async JournalSectionSpecialIssueUpdated(data) {
    return useCases.updateJournalSectionSpecialIssuesUseCase
      .initialize(models)
      .execute(data)
  },
  async JournalSectionAdded(data) {
    await useCases.ingestJournalSectionUseCase
      .initialize({ models, factories })
      .execute(data)
  },
  async JournalSpecialIssueAdded(data) {
    await useCases.ingestJournalSpecialIssueUseCase
      .initialize({ models, factories })
      .execute(data)
  },
  async JournalSectionSpecialIssueAdded(data) {
    await useCases.ingestJournalSectionSpecialIssueUseCase
      .initialize({ models, factories })
      .execute(data)
  },
}
