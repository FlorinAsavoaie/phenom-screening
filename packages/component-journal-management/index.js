const fs = require('fs')
const path = require('path')

const eventHandlers = require('./server/src/eventHandlers')

module.exports = {
  eventHandlers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
}
