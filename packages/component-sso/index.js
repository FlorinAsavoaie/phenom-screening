const service = require('./src/keycloakService')
const server = require('./src/useKeycloakBearer')

module.exports = {
  ...service,
  server,
}
