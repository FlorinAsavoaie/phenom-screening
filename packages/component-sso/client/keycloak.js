import Keycloak from 'keycloak-js'

const init = (config, onSuccess, onError) => {
  const { authServerURL, realm, clientID, checkLoginIframe } = config

  const keycloak = Keycloak({
    url: authServerURL,
    clientId: clientID,
    realm,
  })
  keycloak
    .init({
      onLoad: 'check-sso',
      checkLoginIframe,
    })
    .then(() => {
      onSuccess(keycloak)
    })
    .catch((error) => {
      onError(error, keycloak)
    })
}

export default {
  init,
}
