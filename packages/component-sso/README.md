## SSO implementation for Phenom Suite

## Configuration

1. Add the following environment variables into `.env` file
   #Keycloak config
   KEYCLOAK_REALM=
   KEYCLOAK_CLIENTID=
   KEYCLOAK_SERVER_URL=
   KEYCLOAK_USERNAME=
   KEYCLOAK_PASSWORD=
   KEYCLOAK_CERTIFICATES=

2. Add in `default.js` the following environment mapping:

```
keycloak: {
    certs: process.env.KEYCLOAK_CERTIFICATES,
    admin: {
      username: process.env.KEYCLOAK_USERNAME,
      password: process.env.KEYCLOAK_PASSWORD,
    },
    public: {
      realm: process.env.KEYCLOAK_REALM,
      clientID: process.env.KEYCLOAK_CLIENTID,
      authServerURL: process.env.KEYCLOAK_SERVER_URL,
    },
  }
```

3. Within Pubsweet
   Register it into your server component
   ex.

```
const { useKeycloak } = require('component-sso')
module.exports = {
  resolvers,
  eventHandlers,
  server: () => useKeycloak,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/server/src/typeDefs.graphqls'),
    'utf8',
  ),
}

```

## Usage

This component registers an authentication middleware, a Passport strategy over the `/graphql` endpoint on sever startup.

It exposes 2 methods: the middleware `useKeycloak` and `createSSOUser(profile)` to programmatically create users and keep in sync SSO users and your app users.

## Good to know

Disable your email notification service as the SSO Service handles the entire Login, Registration, Confirmation, Consent, Reset Password flows and sends emails for each of them.
