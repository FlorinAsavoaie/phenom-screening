const config = require('config')
const jwt = require('jsonwebtoken')
const jwksClient = require('jwks-rsa')
const BearerStrategy = require('passport-http-bearer').Strategy
const { token: tokenService } = require('pubsweet-server/src/authentication')
const { checkUser } = require('./keycloakService')

let client

function getJwksClient() {
  if (client) {
    return client
  }
  client = jwksClient({
    cache: true,
    jwksUri: config.get('keycloak.certs'),
  })
  return client
}

const getKey = async (header, callback) => {
  const key = await getJwksClient().getSigningKey(header.kid)
  const signingKey = key.publicKey || key.rsaPublicKey
  callback(null, signingKey)
}

const verifyToken = (token, done) =>
  jwt.verify(
    token,
    getKey,
    {
      algorithms: ['RS256'],
    },
    async (err, decoded) => {
      if (err || !decoded) return done(null)
      const { sub, username } = decoded

      const user = await checkUser(decoded)

      return done(null, user.id || sub, {
        username,
        id: user.id || sub,
        token,
      })
    },
  )

const useKeycloakBearer = () => (app) => {
  if (!config.has('keycloak.public.authServerURL')) {
    return
  }
  const { passport } = app.locals
  passport.use('keycloakBearer', new BearerStrategy(verifyToken))

  app.use(
    '/graphql',
    passport.authenticate(['bearer', 'keycloakBearer', 'anonymous'], {
      session: false,
    }),
  )

  // [EQS-445] overwrite verify method on pubsweet for subscriptions server
  tokenService.verify = verifyToken
}

module.exports = useKeycloakBearer
