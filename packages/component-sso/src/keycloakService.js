const config = require('config')
const logger = require('@pubsweet/logger')
const KeycloakAdminClient = require('@keycloak/keycloak-admin-client').default

const { User } = require('@pubsweet/models')

const initKeycloakAdminClient = async () => {
  const kcAdminClient = new KeycloakAdminClient({
    baseUrl: config.get('keycloak.public.authServerURL'),
    realmName: 'master',
  })

  await kcAdminClient.auth({
    username: config.get('keycloak.admin.username'),
    password: config.get('keycloak.admin.password'),
    grantType: 'password',
    clientId: 'admin-cli',
  })

  kcAdminClient.setConfig({
    realmName: config.get('keycloak.public.realm'),
  })

  async function findOneByEmail(email) {
    const users = await kcAdminClient.users.find({
      email,
    })

    if (!users.length) {
      return
    }

    return users[0]
  }

  async function sendVerifyEmail({ email, id }) {
    logger.info('Sending keycloak verify email to: ', email)

    return kcAdminClient.users.sendVerifyEmail({
      id,
      clientId: config.get('keycloak.public.clientID'),
      redirectUri: config.get('pubsweet-client.baseUrl'),
    })
  }

  async function createFromProfile({
    id,
    email,
    givenNames,
    surname,
    country,
    affiliation,
    title,
  }) {
    logger.info('Creating keycloak user for: ', email)

    return kcAdminClient.users
      .create({
        id,
        email,
        username: email,
        enabled: true,
        emailVerified: false,
        firstName: givenNames,
        lastName: surname,
        requiredActions: ['UPDATE_PASSWORD'],
        attributes: {
          country,
          affiliation,
          title,
        },
      })
      .then((kcUser) => sendVerifyEmail(kcUser))
  }

  return {
    sendVerifyEmail,
    findOneByEmail,
    createFromProfile,
  }
}

const createSSOUser = async (profile) => {
  const kcAdminClient = await initKeycloakAdminClient()

  const userFromSSO = await kcAdminClient.findOneByEmail(profile.email)

  if (userFromSSO) {
    logger.info('Skipping keycloak user creation for: ', profile.email)

    if (!userFromSSO.emailVerified) {
      return kcAdminClient.sendVerifyEmail({
        id: userFromSSO.id,
        email: userFromSSO.email,
      })
    }

    return
  }

  return kcAdminClient.createFromProfile(profile)
}

const createDBUser = async (profile, { User }) => {
  logger.info('Creating user from keycloak for: ', profile.email)

  const user = new User({
    id: profile.sub,
    email: profile.email.toLowerCase(),
    givenNames: profile.given_name,
    surname: profile.family_name,
    isConfirmed: true,
  })

  return user.save()
}

const checkUser = async (profile) => {
  const dbUser = await User.findOneByEmail(profile.email)
  if (dbUser) {
    if (!dbUser.isConfirmed && profile.email_verified) {
      dbUser.isConfirmed = true
      await dbUser.save()
    }
    return dbUser
  }
  await createDBUser(profile, { User })
  return profile
}

module.exports = {
  createSSOUser,
  createDBUser,
  checkUser,
}
