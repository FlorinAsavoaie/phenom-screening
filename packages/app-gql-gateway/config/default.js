module.exports = {
  cookieDomainValue: process.env.COOKIE_DOMAIN_VALUE || '',
  schemaRegistryUrl: process.env.SCHEMA_REGISTRY_URL || 'http://localhost:6001',
  port: process.env.PORT,
  poll_interval_in_ms: JSON.parse(process.env.ENABLE_POLL_INTERVAL || true)
    ? 10_000 // 10s
    : 0,
  debug: process.env.NODE_ENV === 'development' || false,
}
