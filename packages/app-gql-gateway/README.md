# GQL Gateway

The service adds the ability for the frontend to compute information from multiple microservices

### Environment variables -> docker-compose

```sh
NODE_ENV=production
PORT=3000
SCHEMA_REGISTRY_URL=***
COOKIE_DOMAIN_VALUE=*** => for production
```

### Environment variables -> localhost

```sh
NODE_ENV=development
PORT=4001
```

### Not Required Env vars - default values

```sh
ENABLE_POLL_INTERVAL=true by default. When it is  set to true, apollo gateway will check for changes in schema once every 10s and will refresh the schema with the new updates.
SCHEMA_REGISTRY_URL=http://localhost:6001
```

# Gateway and Schema Registry in Phenom Apps

The gateway uses the schemas registered in the Phenom Schema registry. The schema for each app is pushed in the registry on start server.  

## Usage of gateway and schema registry on local environment

Steps to run it on local env:  
1. Start connection to schema registry 
2. Start app server - this will push the graphql schema to the registry
3. Start the gateway 

### **1. Start connection to schema registry**  

In order for the graphql schema to be pushed in the registry on start server, for the local environment, the schema registry along with the db and redis must be up and running. This is a snippet from the current implementation, for creating the connection to registry, in the Review App, using docker-compose file. Similar approaches might be found in all phenom apps once the registry is added.  
 
```yml
 gql-schema-registry-db:
    image: mysql:8
    command: mysqld --default-authentication-plugin=mysql_native_password --skip-mysqlx
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: schema_registry
    ports:
      - 6000:3306
    volumes:
      - gql-schema-registry-db-volume:/var/lib/mysql
    healthcheck:
      test:
        ['CMD', 'mysqladmin', 'ping', '-h', 'localhost', '-u', 'healthcheck']
      timeout: 5s
      retries: 10

  gql-schema-registry:
    image: pipedrive/graphql-schema-registry:3.4.0
    ports:
      - 6001:3000
    environment:
      - NODE_ENV=production
      - DB_HOST=gql-schema-registry-db
      - DB_NAME=schema_registry
      - DB_PORT=3306
      - DB_SECRET=root
      - DB_USERNAME=root
      - PORT=3000
      - REDIS_HOST=gql-schema-registry-redis
      - REDIS_PORT=6004
    depends_on:
      - gql-schema-registry-redis
      - gql-schema-registry-db

  gql-schema-registry-redis:
    image: redis:6-alpine
    ports:
      - 6004:6379
  ```
### **2. Push graphql schema in the registry by starting server** 

  Once the infrastructure for the registry is created and running, the app server can be started and the graphql schema will be pushed to the registry using the schema-registry-cli package. 
  
  `The schema-registry-cli package needs a version of node >= 12.20. So make sure you are not using locally a node version smaller than 12.20.`   

  Example of schema being pushed, in the screening app: 
  ```javascript
const {validateAndRegisterSchema} = require('@phenom.pub/schema-registry-cli/lib/register-schema')

async function validateAndRegisterGQLSchema(typeDefs) {
  const service = {
    name: 'app-screening',
    url: config.get('serviceSchemaUrl'),
  }
  try {
    await validateAndRegisterSchema(service, typeDefs)
    logger.info('Schema registered successfully!')
  } catch (err) {
    logger.error(`Schema registration failed: ${err.message}`)
    process.exit(1)
  }
}
  ```

  ### Env vars needed by the schema-registry-cli  
  - REGISTRY_URL_FOR_ENVIRONMENT, example for local environment : 

  ```sh
  SCHEMA_REGISTRY_URL='http://localhost:6001' 
  ```  
  - url to the service graphql schema, needed for schema registry push, example for local environment:

   ```sh
  SERVICE_SCHEMA_URL='http://host.docker.internal:4000/graphql'
   ```
  

### **3. Start gateway**

Once the schema is pushed in the registry the gateway can be started.  
! The first time the gateway & registry setup is made it's important that the gateway is started after the schema was pushed in the registry, otherwise the gateway will throw an error and will stop. On subsequent uses of the gateway, a schema will be present in the registry and the gateway will work either way.  

This is a snippet from the current implementation of gateway in the Review App, using docker-compose file. Similar approaches might be found in all phenom apps once the registry is added. 

```yml
  app-gql-gateway:
    image: ${AWS_ACCOUNT}.dkr.ecr.${AWS_REGION}.amazonaws.com/app-gql-gateway:latest
    command: sh -c "yarn start"
    ports:
      - 4001:3000
    environment:
      - PORT=3000
      - SCHEMA_REGISTRY_URL=http://gql-schema-registry:3000
      - NODE_ENV=development
    depends_on:
      - gql-schema-registry
```

### Env needed for gateway 

```sh
GATEWAY_URI='http://localhost:4001' - example for local env
 ```   

## More information about the Federation can be found in the Apollo documentation [here](https://www.apollographql.com/docs/federation/)



