import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['qa-gamma/shared/app-gql-gateway'],
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      ...defaultValues.serviceProps.ingressOptions,
      rules: [
        {
          host: 'gateway.qa-gamma.phenom.pub',
        },
      ],
    },
  },
}
