import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod/shared/app-gql-gateway'],
  serviceProps: {
    ...defaultValues.serviceProps,
    replicaCount: 2,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/app-gql-gateway',
      tag: 'latest',
    },
    ingressOptions: {
      ...defaultValues.serviceProps.ingressOptions,
      rules: [
        {
          host: 'gateway.prod.phenom.pub',
        },
        {
          host: 'gateway.hindawi.com',
        },
      ],
    },
    podDisruptionBudget: {
      minAvailable: 1,
    },
  },
}
