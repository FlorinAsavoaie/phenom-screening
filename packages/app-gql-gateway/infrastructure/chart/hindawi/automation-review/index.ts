import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['automation/review/app-gql-gateway'],
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      ...defaultValues.serviceProps.ingressOptions,
      rules: [
        {
          host: 'gateway-review.automation.phenom.pub',
        },
      ],
    },
  },
}
