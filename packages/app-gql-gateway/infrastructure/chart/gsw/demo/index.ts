import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['demo-gsw/shared/app-gql-gateway'],
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      ...defaultValues.serviceProps.ingressOptions,
      rules: [
        {
          host: 'gateway.demo-gsw.phenom.pub',
        },
      ],
    },
    podDisruptionBudget: {
      minAvailable: 1,
    },
  },
}
