import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod-gsw/shared/app-gql-gateway'],
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/app-gql-gateway',
      tag: 'latest',
    },
    ingressOptions: {
      ...defaultValues.serviceProps.ingressOptions,
      rules: [
        {
          host: 'gateway.lithosphere.geoscienceworld.org',
        },
        {
          host: 'gateway.gsw-prod.phenom.pub',
        },
      ],
    },
    podDisruptionBudget: {
      minAvailable: 1,
    },
  },
}
