function hasExpiresField(cookie: string): boolean {
  return cookie
    .split(';')
    .map((part: string) => {
      if (part.split('=').length > 1) {
        return part.split('=')[0].trim().toLowerCase() === 'expires'
      }

      return false
    })
    .some((bool: boolean) => bool)
}

function santizeCookieProperty(cookieProperty: string): string {
  const [propertyName, propertyValue] = cookieProperty.split('=')
  const sanitizedPropertyName = propertyName
    .replace('-', '')
    .toLowerCase()
    .trim()

  if (sanitizedPropertyName === 'maxage') {
    return `maxAge=${propertyValue}`
  } else if (sanitizedPropertyName === 'httponly') {
    return 'httpOnly=true'
  } else if (sanitizedPropertyName === 'samesite') {
    return `sameSite=${propertyValue.toLowerCase()}`
  }

  if (!propertyValue) {
    return `${sanitizedPropertyName}=true`
  }

  return `${sanitizedPropertyName}=${propertyValue}`
}

interface SetMaxAgeAndExpiresOption {
  maxAge?: number
  path?: string
  expires?: Date
  [key: string]: any
}

export interface ObjectifiedCookieProperty {
  maxAge?: number
  path?: string
  expires?: string
}

function setMaxAgeAndExpires(
  objectifiedCookieProperty: ObjectifiedCookieProperty,
  options: SetMaxAgeAndExpiresOption,
): void {
  Object.entries(objectifiedCookieProperty).forEach(
    ([key, value]: [key: string, value: string]) => {
      if (key === 'expires') {
        options[key] = new Date(value)
        return
      }

      if (key === 'maxAge') {
        options[key] = Number(value) * 1000
        return
      }

      options[key] = value
    },
  )
}

interface ParseCookiesReturnValues {
  cookieName: string
  cookieValue: string
  options: ObjectifiedCookieProperty
}

export function parseCookies(rawCookies: string): ParseCookiesReturnValues[] {
  if (!rawCookies.includes('=')) {
    throw new Error(
      'Invalid raw cookies, look at the format of a set-cookie header string and provide something similar.',
    )
  }
  /*
    Cookie format: "name=value; Path=/; HttpOnly; Secure"
    Multiple cookies format: "name=value; Path=/; HttpOnly; Secure, name2=value2"
  */
  const arraifyedRawCookies = rawCookies.split(',')
  const validRawCookies = arraifyedRawCookies
    .map((rawCookie: string, index: number, ref: string[]) => {
      if (hasExpiresField(rawCookie)) {
        return `${rawCookie}${ref[index + 1]}`
      }

      if (index > 0 && hasExpiresField(ref[index - 1])) return 'invalid'

      return rawCookie
    })
    .filter((rawCookie: string) => rawCookie !== 'invalid')

  return validRawCookies.map((rawCookie: string) => {
    const [cookieNameAndValue, ...cookieProperties] = rawCookie.split(';')
    const [cookieName, cookieValue] = cookieNameAndValue.split('=')

    const sanitizedCookieProperties = cookieProperties.map(
      santizeCookieProperty,
    )

    const objectifiedCookieProperties = sanitizedCookieProperties.map(
      (sanitizedCookieProperty: string) => {
        const [propertyName, propertyValue] = sanitizedCookieProperty.split('=')

        return {
          [propertyName]: propertyValue === 'true' ? true : propertyValue,
        }
      },
    )

    const options = {}

    objectifiedCookieProperties.forEach(
      (objectifiedCookieProperty: ObjectifiedCookieProperty) => {
        setMaxAgeAndExpires(objectifiedCookieProperty, options)
      },
    )

    return {
      cookieName: cookieName.trim(),
      cookieValue: cookieValue.trim(),
      options,
    }
  })
}
