import { get } from 'lodash'
import request from 'request-promise-native'
import config from 'config'
import { ServiceDefinition } from '@apollo/federation'
import { parse } from 'graphql'

const SCHEMA_REGISTRY_URL: string = config.get('schemaRegistryUrl')

interface Schema {
  id: number
  // eslint-disable-next-line camelcase
  service_id: number
  version: string
  name: string
  url: string
  // eslint-disable-next-line camelcase
  added_time: string
  // eslint-disable-next-line camelcase
  type_defs: string
  // eslint-disable-next-line camelcase
  is_active: number
}

const getServiceDefinitions = async (): Promise<ServiceDefinition[]> => {
  const serviceTypeDefinitions = await request({
    baseUrl: SCHEMA_REGISTRY_URL,
    method: 'GET',
    url: '/schema/latest',
    json: true,
  })

  return get(serviceTypeDefinitions, 'data', []).map((schema: Schema) => {
    if (!schema.url) {
      console.warn(`Service url not found for type definition "${schema.name}"`)
    } else {
      console.info(
        `Got ${schema.name} service schema with version ${schema.version}`,
      )
    }

    return {
      name: schema.name,
      url: schema.url,
      version: schema.version,
      typeDefs: parse(schema.type_defs),
    }
  })
}

export { getServiceDefinitions }
