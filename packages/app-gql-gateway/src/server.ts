import config from 'config'

import { ApolloGateway } from '@apollo/gateway'
import { ApolloServer, ServerInfo } from 'apollo-server'

import { parseCookies } from './parse-cookies'
import { FileUploadDataSource } from './file-upload-data-source'
import { getServiceDefinitions } from './get-service-definitions'

export class ApplicationServer {
  private constructor(
    private readonly server: ApolloServer,
    private readonly serverPort: number,
  ) {}

  start(): Promise<ServerInfo> {
    return this.server.listen(this.serverPort, () => {
      console.info(`Server is ready at ${this.serverPort}`)
    })
  }

  static create(): ApplicationServer {
    const server = ApplicationServer.createServer()
    const serverPort = this.getServerPort()
    return new ApplicationServer(server, serverPort)
  }

  private static createServer() {
    const gateway: any = this.createGateway()

    return new ApolloServer({
      cors: {
        origin: true,
        credentials: true,
      },
      gateway,
      context: ({ req, res }) => ({
        authorization: req.headers.authorization || null,
        cookie: req.headers.cookie || null,
        res,
      }),
      subscriptions: false,
    })
  }

  private static createGateway() {
    return new ApolloGateway({
      buildService: ({ url }) =>
        new FileUploadDataSource({
          url,
          willSendRequest({ request, context }) {
            if (request && request.http) {
              request.http.headers.set(
                'authorization',
                (context as any).authorization || null,
              )
              request.http.headers.set(
                'cookie',
                (context as any).cookie || null,
              )
            }
          },
          didReceiveResponse({ response, context }) {
            const rawCookies =
              response &&
              response.http &&
              response.http.headers.get('set-cookie')

            if (rawCookies) {
              const cookies = parseCookies(rawCookies)
              cookies.forEach(({ cookieName, cookieValue, options }) => {
                if (context && context.res) {
                  context.res.cookie(cookieName, cookieValue, {
                    ...options,
                    domain: config.get('cookieDomainValue'),
                  })
                }
              })
            }
            return response
          },
        }),
      pollIntervalInMs: config.get('poll_interval_in_ms'),
      async experimental_updateServiceDefinitions() {
        return {
          serviceDefinitions: await getServiceDefinitions(),
          isNewSchema: true,
        }
      },
      debug: config.get('debug'),
    })
  }

  private static getServerPort(): number {
    const portNumber = config.get<string>('port')
    return parseInt(portNumber, 10)
  }
}
