import {
  GraphQLDataSourceProcessOptions,
  RemoteGraphQLDataSource,
} from '@apollo/gateway/'
import { fetch, Request, Headers } from 'apollo-server-env'
import { isObject } from '@apollo/gateway/dist/utilities/predicates'
import FormData from 'form-data'
import _ from 'lodash'

class FileUploadDataSource extends RemoteGraphQLDataSource {
  async process(options: GraphQLDataSourceProcessOptions) {
    const { request } = options

    const fileVariables = this.extract(request.variables)
    if (fileVariables.length > 0) {
      return this.processFileUpload(options, fileVariables)
    }
    return super.process(options)
  }

  async processFileUpload(
    options: GraphQLDataSourceProcessOptions,
    fileVariables: any,
  ) {
    const form = new FormData()
    const { request } = options

    // cannot mutate the request object
    const variables = _.cloneDeep(request.variables)
    if (variables) {
      // eslint-disable-next-line no-restricted-syntax
      for (const [variableName] of fileVariables) {
        _.set(variables, variableName, null)
      }
    }

    const operations = JSON.stringify({
      query: request.query,
      variables,
    })

    form.append('operations', operations)

    const resolvedFiles = await Promise.all(
      fileVariables.map(
        async ([variableName, file]: [variableName: any, file: any]) => {
          const contents = await file
          return [variableName, contents]
        },
      ),
    )

    // e.g. { "0": ["variables.file"] }
    const fileMap = resolvedFiles.reduce(
      (map: any, [variableName]: any, i: number) => ({
        ...map,
        [i]: [`variables.${variableName}`],
      }),
      {},
    )
    form.append('map', JSON.stringify(fileMap))

    interface Contents {
      filename: string
      mimetype: string
      encoding: string
      createReadStream: () => any
    }
    await Promise.all(
      resolvedFiles.map(async (value: any, i: any) => {
        const [_, contents]: [_: any, contents: Contents] = value
        const { filename, mimetype, createReadStream } = contents
        const readStream = await createReadStream()
        // TODO: Buffers performance issues? may be better solution.
        const buffer = await this.onReadStream(readStream)
        form.append(i, buffer, { filename, contentType: mimetype })
      }),
    )

    // Respect incoming http headers (eg, apollo-federation-include-trace).
    const headers = (request.http && request.http.headers) || new Headers()

    form.getLength((_, length) => {
      headers.set('Content-Length', length.toString())
    })

    Object.entries(form.getHeaders() || {}).forEach(([k, value]) => {
      headers.set(k, value)
    })

    request.http = {
      method: 'POST',
      url: this.url,
      headers,
    }
    if (this.willSendRequest) {
      await this.willSendRequest(options)
    }

    const requestOptions: any = {
      ...request.http,
      body: form,
    }

    const httpRequest: any = new Request(request.http.url, requestOptions)

    try {
      const httpResponse: any = await fetch(httpRequest)
      const body = await this.parseBody(httpResponse)

      if (!isObject(body)) {
        throw new Error(`Expected JSON response body, but received: ${body}`)
      }
      return {
        ...body,
        http: httpResponse,
      }
    } catch (error: any) {
      this.didEncounterError(error, httpRequest)
      throw error
    }
  }

  extract(obj: any) {
    const files: any = []

    const _extract = (obj: any, keys?: any): any =>
      Object.entries(obj || {}).forEach(([k, value]) => {
        const key = keys ? `${keys}.${k}` : k
        if (value instanceof Promise) {
          return files.push([key, value])
        }
        // TODO: support arrays of files
        if (value instanceof Object) {
          return _extract(value, key)
        }
      })
    _extract(obj)
    return files
  }

  onReadStream = (readStream: any) =>
    new Promise((resolve, reject) => {
      const buffers: any = []
      readStream.on('data', (data: any) => {
        buffers.push(data)
      })
      readStream.on('end', () => {
        const actualContents = Buffer.concat(buffers)

        resolve(actualContents)
      })
      readStream.on('error', (err: any) => {
        reject(err)
      })
    })
}

export { FileUploadDataSource }
