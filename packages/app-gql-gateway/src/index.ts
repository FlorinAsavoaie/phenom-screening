import 'dotenv/config'
import 'reflect-metadata'
import { ApplicationServer } from './server'

export async function main(): Promise<void> {
  const applicationServer = ApplicationServer.create()

  await applicationServer.start()
}

main().catch((error) => {
  console.error('Unexpected error')
  console.error(error)
  process.exit(1)
})
