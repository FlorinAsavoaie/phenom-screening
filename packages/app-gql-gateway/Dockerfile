# ---- Base ----
FROM node:16-alpine as base
WORKDIR /app
COPY package.json ./

# ---- Dependencies ----
FROM base AS dependencies
COPY package.json yarn.lock tsconfig.json ./
COPY packages/app-gql-gateway/config ./packages/app-gql-gateway/config
COPY packages/app-gql-gateway/src ./packages/app-gql-gateway/src
COPY packages/app-gql-gateway/package.json ./packages/app-gql-gateway/package.json
COPY packages/app-gql-gateway/tsconfig.json ./packages/app-gql-gateway/tsconfig.json
RUN yarn install --frozen-lockfile
RUN cp -R node_modules prod_node_modules
RUN yarn install && \
    yarn cache clean && \
    yarn workspace app-gql-gateway build

# ---- Production ----
FROM base AS production
COPY --from=dependencies /app/prod_node_modules/ ./node_modules
COPY --from=dependencies /app/packages/app-gql-gateway/dist/ /app/dist/
COPY --from=dependencies /app/packages/app-gql-gateway/config/default.js /app/config/
COPY --from=dependencies /app/packages/app-gql-gateway/package.json /app/package.json
EXPOSE 3000
CMD ["yarn", "start"]
