# Infrastructure Gaia

This component gathers infrastructure needs from all services and creates the necessary kubernetes manifests, in order to be deployed to our cluster.

### Using the service

The `yarn synth` command will generate kubernetes manifests in `./dist-k8s` for all packages that have an `./infrastructure` folder exporting the appropriate config.

Those can be used with `kubectl apply -f ./dist-k8s/` to apply the changes on the cluster


### Required Env vars

```sh
CLUSTER=hindawi-dev
TENANT=hindawi
NODE_ENV=qa
IMAGE_TAG=latest
APP=appScreening

# optionally: 
AWS_PROFILE=**** #specify the aws profile to be used by aws-sdk
APP_NAME=review-app-gql-gateway
```
