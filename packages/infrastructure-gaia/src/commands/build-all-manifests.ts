import { BuildManifestCommand } from './build-manifest'
import { Command } from '../contracts'
import { masterConfig } from '../config.gen'
import { App } from '../types'

export class BuildAllManifestsCommand implements Command {
  async run(): Promise<void> {
    const buildCommand = new BuildManifestCommand()

    await Promise.all(
      Object.keys(masterConfig).map((app: string) =>
        buildCommand.run(app as App),
      ),
    )
  }
}
