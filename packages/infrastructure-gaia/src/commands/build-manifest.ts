import {
  Cdk8sApp,
  HindawiServiceChart,
  WithAwsSecretsServiceProps,
} from '@hindawi/phenom-charts'

import { Command } from '../contracts'
import { getOsEnv } from '../env'
import { masterConfig } from '../config.gen'
import { App, Environment, Tenant, WithSopsSecretsServiceProps } from '../types'

interface EnvProps {
  tenant: Tenant
  environment: Environment
  tag: string
  repository: string
  appName: string
}

export class BuildManifestCommand implements Command {
  private static parseEnv(): EnvProps {
    const tenant: string = getOsEnv('TENANT')
    const environment: string = kebabToCamelCase(getOsEnv('NODE_ENV'))
    const tag: string = getOsEnv('IMAGE_TAG', '')
    const repository: string = getOsEnv('IMAGE_REPOSITORY', '')
    const appName: string = getOsEnv('APP_NAME', '')

    return {
      tenant: tenant as Tenant,
      environment: environment as Environment,
      tag,
      repository,
      appName,
    }
  }

  async run(app: App): Promise<void> {
    const env = BuildManifestCommand.parseEnv()

    const rootConstruct = new Cdk8sApp({ outdir: 'dist-k8s' })

    const appProps = masterConfig[app]?.[env.tenant]?.[env.environment]

    if (!appProps) {
      // eslint-disable-next-line no-console
      console.warn(
        `Did not find configuration for ${app}, tenant: ${env.tenant}, environment: ${env.environment}`,
      )
      return
    }

    if (env.repository) appProps.serviceProps.image!.repository = env.repository

    if (env.tag) appProps.serviceProps.image!.tag = env.tag

    const appName = env.appName || camelToKebabCase(app)

    if ((appProps as WithSopsSecretsServiceProps).sopsSecrets) {
      await HindawiServiceChart.withSopsSecrets(
        rootConstruct,
        appName,
        appProps as WithSopsSecretsServiceProps,
      )
    } else {
      await HindawiServiceChart.withAwsSecrets(
        rootConstruct,
        appName,
        appProps as WithAwsSecretsServiceProps,
      )
    }

    rootConstruct.synth()
  }
}

function camelToKebabCase(str: string) {
  return str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`)
}

function kebabToCamelCase(str: string) {
  return str.replace(/-./g, (letter) => letter[1].toUpperCase())
}
