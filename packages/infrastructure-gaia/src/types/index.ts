import {
  WithAwsSecretsServiceProps,
  HindawiServiceChartProps,
} from '@hindawi/phenom-charts'
import { EncodedTree } from 'sops-decoder'

export enum App {
  appScreening = 'appScreening',
  appGqlGateway = 'appGqlGateway',
  appGqlSchemaRegistry = 'appGqlSchemaRegistry',
  authorVerification = 'serviceAuthorVerification',
  editorSuggestion = 'serviceEditorSuggestion',
  fileConversion = 'serviceFileConversion',
  languageCheck = 'serviceLanguageCheck',
  similarityCheck = 'serviceSimilarityCheck',
  suspiciousKeywords = 'serviceSuspiciousKeywords',
  sanctionList = 'serviceSanctionList',
}

export enum Tenant {
  hindawi = 'hindawi',
  gsw = 'gsw',
}

export enum Environment {
  automation = 'automation',
  'automation-review' = 'automation-review',
  'automation-mail-builder' = 'automation-mail-builder',
  qa = 'qa',
  demo = 'demo',
  'demo-sales' = 'demo-sales',
  'qa-gamma' = 'qa-gamma',
  prod = 'prod',
}

export type WithSopsSecretsServiceProps = {
  serviceProps: HindawiServiceChartProps
  sopsSecrets: EncodedTree
}

type AppCfg = Record<
  Tenant,
  Record<Environment, WithAwsSecretsServiceProps | WithSopsSecretsServiceProps>
>

export type MasterConfig = Record<App, AppCfg>
