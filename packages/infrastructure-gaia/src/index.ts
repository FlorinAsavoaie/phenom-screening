import * as dotenv from 'dotenv'

import { BuildAllManifestsCommand, BuildManifestCommand } from './commands'
import { App } from './types'

dotenv.config()
;(async function main(): Promise<void> {
  if (!process.env.APP) {
    const command = new BuildAllManifestsCommand()
    return command.run()
  }
  const app = kebabToCamelCase(process.env.APP)
  const command = new BuildManifestCommand()
  await command.run(app as App)
})()

function kebabToCamelCase(str: string) {
  return str.replace(/-./g, (letter) => letter[1].toUpperCase())
}
