const { startsWith } = require('lodash')
const { configure } = require('@testing-library/react')
require('@testing-library/jest-dom/extend-expect')

const originalConsoleError = console.error
console.error = function newLog(msg) {
  if (startsWith(msg, 'Error: Could not parse CSS stylesheet')) return
  if (startsWith(msg, "Component 'Styled")) return
  if (startsWith(msg, "Component 'styled")) return

  originalConsoleError(msg)
}

configure({
  testIdAttribute: 'data-test-id',
})

Object.assign(global, require('@pubsweet/errors'))
