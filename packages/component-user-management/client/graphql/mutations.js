import { gql } from '@apollo/client'

export const createTeam = gql`
  mutation createTeam($team: CreateTeamInput!) {
    createTeam(input: $team) {
      id
      name
      members {
        id
        role
        email
        surname
        givenNames
      }
    }
  }
`

export const editTeam = gql`
  mutation editTeam($team: EditTeamInput!) {
    editTeam(input: $team) {
      id
      name
      members {
        id
        role
        email
        surname
        givenNames
      }
    }
  }
`
