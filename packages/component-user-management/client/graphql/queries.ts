import { gql } from '@apollo/client'

export const getTeams = gql`
  query getTeams($page: Int, $pageSize: Int, $searchValue: String) {
    getTeams(page: $page, pageSize: $pageSize, searchValue: $searchValue) {
      results {
        id
        type
        name
        members {
          id
        }
        journals {
          name
        }
      }
      total
    }
  }
`

export const getTeam = gql`
  query getTeam($teamId: ID!) {
    getTeam(teamId: $teamId) {
      id
      name
      type
      members {
        id
        role
        email
        surname
        givenNames
      }
      journals {
        id
        name
      }
    }
  }
`

export const getUsers = gql`
  query getUsers($page: Int, $pageSize: Int, $searchValue: String) {
    getUsers(page: $page, pageSize: $pageSize, searchValue: $searchValue) {
      results {
        id
        email
        surname
        givenNames
        teamMemberships {
          role
          teamName
        }
      }
      total
    }
  }
`

export const getAvailableJournals = gql`
  query getAvailableJournals($teamId: ID) {
    availableJournals: getAvailableJournals(teamId: $teamId) {
      id
      name
    }
  }
`
