import { useState, useEffect } from 'react'
import { get } from 'lodash'
import { useQuery } from '@apollo/client'

import { getAvailableJournals } from '../queries'

interface AvailableJournalsConfig {
  teamId: string
  searchValue: string
}

interface Journal {
  id: string
  name: string
}

export interface AvailableJournalsResult {
  availableJournals: Journal[]
  removeAvailableJournal: (item: Journal) => void
  insertAvailableJournal: (item: Journal) => void
  loading: boolean
}

export function useAvailableJournals({
  teamId,
  searchValue,
}: AvailableJournalsConfig): AvailableJournalsResult {
  const { data, loading } = useQuery(getAvailableJournals, {
    variables: { teamId },
    fetchPolicy: 'network-only',
  })

  const [availableJournals, setAvailableJournals] = useState<Journal[]>([])
  const [filteredJournals, setFilteredJournals] = useState(availableJournals)

  useEffect(() => {
    const journals = get(data, 'availableJournals')
    if (journals) {
      setAvailableJournals(journals)
    }
  }, [data])

  useEffect(() => {
    const filteredJournals = availableJournals.filter(({ name }) =>
      name.toUpperCase().includes(searchValue.toUpperCase()),
    )

    setFilteredJournals(filteredJournals)
  }, [searchValue, availableJournals])

  function removeAvailableJournal(item: Journal): void {
    const newAvailableJournals = availableJournals.filter(
      (availableJournal) => availableJournal.id !== item.id,
    )

    setAvailableJournals(newAvailableJournals)
  }

  function insertAvailableJournal(item: Journal): void {
    setAvailableJournals([item, ...availableJournals])
  }

  return {
    availableJournals: filteredJournals,
    removeAvailableJournal,
    insertAvailableJournal,
    loading,
  }
}
