import { useCallback } from 'react'
import { useMutation } from '@apollo/client'
import * as mutations from './../mutations'

export default function useCreateTeam() {
  const [createTeamMutation] = useMutation(mutations.createTeam)
  const createTeam = useCallback(
    (input) =>
      createTeamMutation({
        variables: {
          team: input,
        },
      }),
    [],
  )

  return {
    createTeam,
  }
}
