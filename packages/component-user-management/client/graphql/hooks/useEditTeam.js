import { useCallback } from 'react'
import { useMutation } from '@apollo/client'
import * as mutations from './../mutations'

export default function useEditTeam(teamId) {
  const [editTeamMutation] = useMutation(mutations.editTeam)
  const editTeam = useCallback(
    (input) =>
      editTeamMutation({
        variables: {
          team: { ...input, teamId },
        },
      }),
    [teamId],
  )

  return {
    editTeam,
  }
}
