import { useState } from 'react'
import { Item, Pagination } from '@hindawi/ui'
import {
  SearchField,
  DEFAULT_PAGE_SIZE,
  usePaginatedQuery,
} from 'hindawi-shared/client/components'
import { TabRoot, ScrollContainer } from '../components'
import { UsersTable } from '../components/UsersTable'
import { getUsers } from '../graphql/queries'

function UsersTab() {
  const [searchValue, setSearchValue] = useState('')
  const [inputValue, setInputValue] = useState('')
  const {
    results,
    page,
    error,
    toLast,
    toNext,
    toFirst,
    setPage,
    loading,
    toPrevious,
    total,
  } = usePaginatedQuery(getUsers, {
    variables: { pageSize: DEFAULT_PAGE_SIZE, searchValue },
    settings: {},
  })

  const handleOnChange = (e) => setInputValue(e.target.value)

  const handleOnSearch = (newSearchValue) => {
    toFirst()
    setSearchValue(newSearchValue)
  }
  return (
    <ScrollContainer>
      <TabRoot>
        <SearchField
          mb={4}
          onChange={handleOnChange}
          onSearch={handleOnSearch}
          placeholder="Search by Name, Email or Team"
          value={inputValue}
          width={88}
        />

        <UsersTable
          error={error}
          loading={loading}
          noItemsMessage="No results found."
          users={results}
        />
        {total > 0 && (
          <Item justify="flex-end" mt={4}>
            <Pagination
              itemsPerPage={DEFAULT_PAGE_SIZE}
              nextPage={toNext}
              page={page}
              prevPage={toPrevious}
              setPage={setPage}
              toFirst={toFirst}
              toLast={toLast}
              totalCount={total}
            />
          </Item>
        )}
      </TabRoot>
    </ScrollContainer>
  )
}

export default UsersTab
