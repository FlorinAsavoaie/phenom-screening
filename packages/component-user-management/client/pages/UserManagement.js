import { useState } from 'react'

import { Tabs } from 'hindawi-shared/client/components'
import TeamsTab from './TeamsTab'
import UsersTab from './UsersTab'

function UserManagement() {
  const [selectedTab, setSelectedTab] = useState(0)
  const handleClick = (index) => setSelectedTab(index)
  const tabs = [{ label: 'Users' }, { label: 'Teams' }]
  return (
    <Tabs handleClick={handleClick} selectedTab={selectedTab} tabs={tabs}>
      <UsersTab />
      <TeamsTab />
    </Tabs>
  )
}

export default UserManagement
