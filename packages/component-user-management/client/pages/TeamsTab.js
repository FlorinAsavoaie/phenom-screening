import { useState } from 'react'
import { Row, Item, Pagination } from '@hindawi/ui'

import {
  SearchField,
  usePaginatedQuery,
  DEFAULT_PAGE_SIZE,
} from 'hindawi-shared/client/components'
import { TabRoot, ScrollContainer } from '../components'
import { CreateTeamModal, TeamsTable } from '../components/TeamsTable'
import { getTeams } from '../graphql/queries'

function TeamsTab() {
  const [inputValue, setInputValue] = useState('')
  const [searchValue, setSearchValue] = useState('')
  const {
    error,
    loading,
    page,
    refetch,
    setPage,
    results,
    toFirst,
    toLast,
    toNext,
    toPrevious,
    total,
  } = usePaginatedQuery(getTeams, {
    variables: { searchValue, pageSize: DEFAULT_PAGE_SIZE },
    settings: {},
  })

  const handleOnChange = (e) => setInputValue(e.target.value)
  const handleOnSearch = (newSearchValue) => {
    toFirst()
    setSearchValue(newSearchValue)
  }
  const reset = () => {
    setSearchValue('')
    setInputValue('')
    refetch()
    toFirst()
  }

  return (
    <ScrollContainer>
      <TabRoot>
        <Row justify="flex-start">
          <SearchField
            mb={4}
            onChange={handleOnChange}
            onSearch={handleOnSearch}
            placeholder="Find team..."
            value={inputValue}
            width={88}
          />
          <Item justify="flex-end">
            <CreateTeamModal onTeamCreate={reset} />
          </Item>
        </Row>

        <TeamsTable
          error={error}
          loading={loading}
          noItemsMessage={
            !searchValue
              ? 'There are currently no teams. Please create one first.'
              : 'No results found.'
          }
          onTeamEdit={reset}
          teams={results}
        />
        {total > 0 && (
          <Item justify="flex-end" mt={4}>
            <Pagination
              itemsPerPage={DEFAULT_PAGE_SIZE}
              nextPage={toNext}
              page={page}
              prevPage={toPrevious}
              setPage={setPage}
              toFirst={toFirst}
              toLast={toLast}
              totalCount={total}
            />
          </Item>
        )}
      </TabRoot>
    </ScrollContainer>
  )
}

export default TeamsTab
