import { theme } from '@hindawi/ui'
import { ThemeProvider } from 'styled-components'
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client'
import { ModalProvider } from '@pubsweet/ui'
import { render as rtlRender } from '@testing-library/react'
import { MockLink } from '@apollo/client/testing'
import { createBrowserHistory } from 'history'
import { Router } from 'react-router'

function createClient(mocks) {
  return new ApolloClient({
    cache: new InMemoryCache(),
    link: new MockLink(mocks),
  })
}

const history = createBrowserHistory()
export const render = (ui, mocks) => {
  const client = createClient(mocks)
  const Component = () => (
    <>
      <ModalProvider>
        <ApolloProvider client={client}>
          <Router history={history}>
            <ThemeProvider theme={theme}>{ui}</ThemeProvider>
          </Router>
        </ApolloProvider>
      </ModalProvider>
      <div id="ps-modal-root" />
    </>
  )
  const utils = rtlRender(<Component />)

  return {
    ...utils,
  }
}
