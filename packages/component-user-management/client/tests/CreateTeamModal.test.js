import { cleanup, fireEvent, act } from '@testing-library/react'
import { MockedProvider } from '@apollo/client/testing'

import { render } from './testUtils'
import { TeamModalContent } from './../components/TeamsTable/TeamModal'

import { getAvailableJournals } from '../graphql/queries'

const mocks = [
  {
    request: {
      query: getAvailableJournals,
      variables: {
        teamId: '123',
      },
    },
    result: {
      data: {
        availableJournals: [
          {
            id: '1',
            name: 'Some journal',
          },
          {
            id: '2',
            name: 'Another journal',
          },
        ],
      },
    },
  },
]

describe('Create Team Modal', () => {
  let hideModalMock
  let onSubmitMock
  let container

  beforeEach(() => {
    cleanup()
    hideModalMock = jest.fn()
    onSubmitMock = jest.fn()
    container = render(
      <MockedProvider addTypename={false} mocks={mocks}>
        <TeamModalContent
          hideModal={hideModalMock}
          onSubmit={(hideModal) => () => onSubmitMock(hideModal)}
        />
      </MockedProvider>,
    )
  })

  it('should show "Required" for name field', async () => {
    const { getByTestId, getAllByText } = container
    await act(async () => {
      await fireEvent.click(getByTestId('modal-create'))
    })

    expect(getAllByText('Required').length).toEqual(1)

    expect(onSubmitMock).toHaveBeenCalledTimes(0)
  })

  it('should show error if there are no members', async () => {
    const { getByTestId, getAllByText } = container
    await act(async () => {
      await fireEvent.click(getByTestId('modal-create'))
    })

    expect(getAllByText('Required').length).toEqual(1)
    expect(
      getAllByText(
        `A team should have at least one Team Leader and one Screener.`,
      ).length,
    ).toEqual(1)
    expect(onSubmitMock).toHaveBeenCalledTimes(0)
  })

  it('Should show "Required" for every field that is required', async () => {
    const { getByText, getAllByText } = container

    await act(async () => {
      await fireEvent.click(getByText('Add'))
    })

    expect(getAllByText('Required').length).toEqual(4)
    expect(onSubmitMock).toHaveBeenCalledTimes(0)
  })
})
