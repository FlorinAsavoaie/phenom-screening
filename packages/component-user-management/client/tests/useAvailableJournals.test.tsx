import wait from 'waait'

import { renderHook, act, HookResult } from '@testing-library/react-hooks'
import { MockedProvider } from '@apollo/client/testing'

import {
  useAvailableJournals,
  AvailableJournalsResult,
} from '../graphql/hooks/useAvailableJournals'
import { getAvailableJournals } from '../graphql/queries'

const mocks = [
  {
    request: {
      query: getAvailableJournals,
      variables: {
        teamId: '123',
      },
    },
    result: {
      data: {
        availableJournals: [
          {
            id: '1',
            name: 'Some journal',
          },
          {
            id: '2',
            name: 'Another journal',
          },
        ],
      },
    },
  },
]

async function waitDataToBeFetched(): Promise<void> {
  await act(async () => {
    // wait data to be fetched
    await wait()
  })
}

describe('useAvailableJournals hook', () => {
  let result: HookResult<AvailableJournalsResult>
  let rerender: () => void
  let searchValue = ''

  beforeEach(() => {
    const teamId = '123'
    const wrapper: React.FC<any> = ({ children }) => (
      <MockedProvider addTypename={false} mocks={mocks}>
        {children}
      </MockedProvider>
    )

    const { result: hookResult, rerender: rerenderHook } = renderHook(
      () => useAvailableJournals({ teamId, searchValue }),
      {
        wrapper,
      },
    )
    result = hookResult
    rerender = rerenderHook
  })

  it('should expose loading state correctly', async () => {
    expect(result.current.loading).toBe(true)

    await waitDataToBeFetched()

    expect(result.current.loading).toBe(false)
  })

  it('should fetch journals', async () => {
    expect(result.current.availableJournals).toStrictEqual([])
    await waitDataToBeFetched()

    expect(result.current.availableJournals).toStrictEqual([
      {
        id: '1',
        name: 'Some journal',
      },
      {
        id: '2',
        name: 'Another journal',
      },
    ])
  })

  it('should remove journal', async () => {
    await waitDataToBeFetched()

    expect(result.current.availableJournals).toStrictEqual([
      {
        id: '1',
        name: 'Some journal',
      },
      {
        id: '2',
        name: 'Another journal',
      },
    ])

    await act(async () => {
      result.current.removeAvailableJournal({
        id: '1',
        name: 'Some journal',
      })
    })

    expect(result.current.availableJournals).toStrictEqual([
      {
        id: '2',
        name: 'Another journal',
      },
    ])
  })

  it('should insert journal', async () => {
    await waitDataToBeFetched()

    expect(result.current.availableJournals).toStrictEqual([
      {
        id: '1',
        name: 'Some journal',
      },
      {
        id: '2',
        name: 'Another journal',
      },
    ])

    await act(async () => {
      result.current.insertAvailableJournal({
        id: '3',
        name: 'Other Journal',
      })
    })

    expect(result.current.availableJournals).toStrictEqual([
      {
        id: '3',
        name: 'Other Journal',
      },
      {
        id: '1',
        name: 'Some journal',
      },
      {
        id: '2',
        name: 'Another journal',
      },
    ])
  })

  it('should filter journals', async () => {
    await waitDataToBeFetched()

    expect(result.current.availableJournals).toStrictEqual([
      {
        id: '1',
        name: 'Some journal',
      },
      {
        id: '2',
        name: 'Another journal',
      },
    ])

    searchValue = 'some'
    await act(async () => {
      rerender()
    })

    expect(result.current.availableJournals).toStrictEqual([
      {
        id: '1',
        name: 'Some journal',
      },
    ])
  })
})
