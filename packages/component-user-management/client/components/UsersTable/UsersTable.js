import { Text } from '@hindawi/ui'

import { Table } from 'hindawi-shared/client/components'

function FullNameColumn({ item }) {
  return <Text>{`${item.givenNames} ${item.surname}`}</Text>
}

const rolesDictionary = {
  teamLeader: 'TL',
  screener: 'SC',
  admin: 'Admin',
  qualityChecker: 'QC',
}

function getTeamMemberships(teamMemberships = []) {
  return teamMemberships.reduce(
    (acc, el, index) =>
      `${acc}${index !== 0 ? ', ' : ' '}${el.teamName} - ${
        rolesDictionary[el.role]
      }`,
    '',
  )
}

function TeamAndRolesColumn({ item }) {
  return (
    <Text pr={1}>
      {item.teamMemberships.length === 0
        ? 'Not in a team.'
        : getTeamMemberships(item.teamMemberships)}
    </Text>
  )
}

const columnDefinitions = [
  {
    headerName: 'Full Name',
    component: FullNameColumn,
  },
  {
    headerName: 'Email',
    labelFunction: (user) => user.email,
  },
  {
    headerName: 'Teams & Roles',
    component: TeamAndRolesColumn,
  },
]

export default function UsersTable({ users, error, loading, noItemsMessage }) {
  return (
    <Table
      columnDefinitions={columnDefinitions}
      error={error}
      items={users}
      loading={loading}
      noItemsMessage={noItemsMessage}
    />
  )
}
