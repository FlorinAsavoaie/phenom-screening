import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export const TabRoot = styled.div`
  background-color: ${th('white')};
  box-shadow: 0 ${th('gridUnit')} calc(${th('gridUnit')} * 2) 0
    rgba(51, 51, 51, 0.15);
  border-radius: ${th('borderRadius')};
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: calc(${th('gridUnit')} * 2);
  padding: calc(${th('gridUnit')} * 4);
`

export const ScrollContainer = styled.div`
  overflow: auto;
`
