export function prepareTeamInput(values) {
  return {
    name: values.name,
    type: values.type,
    members: values.members,
    journalIds: values.journals.map((journal) => journal.id),
  }
}
