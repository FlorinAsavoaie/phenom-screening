import styled from 'styled-components'
import { Row, Text, Item } from '@hindawi/ui'
import { Button, Spinner } from '@pubsweet/ui'
import { Icon } from 'hindawi-shared/client/components'

function Footer({
  fetchingError,
  handleSubmit,
  submitCount,
  isFetching,
  hideModal,
  teamId,
  errors,
}) {
  return (
    <Row alignItems="center" justify="flex-end" mt={6} pr={4}>
      <ShowError
        errors={errors}
        fetchingError={fetchingError}
        submitCount={submitCount}
      />

      <Item flex="inherit" justify="center" mr={3} width={32}>
        <Button data-test-id="modal-cancel" onClick={hideModal} width={32}>
          Cancel
        </Button>
      </Item>
      <Item flex="inherit" justify="center" ml={2} width={32}>
        {isFetching ? (
          <Spinner size={8} />
        ) : (
          <Button
            data-test-id="modal-create"
            onClick={handleSubmit}
            primary
            width={32}
          >
            {teamId ? 'Update' : 'Create'}
          </Button>
        )}
      </Item>
    </Row>
  )
}

const ShowError = ({ submitCount, errors, fetchingError }) => {
  if (submitCount > 0 && errors.noTeamMember) {
    return <Error errorText={errors.noTeamMember} />
  }
  if (fetchingError) {
    return <Error errorText={fetchingError.message} />
  }
  return null
}

const Error = ({ errorText }) => (
  <Row justify="center">
    <Icon color="warningColor" name="warning" size={4} />
    <ErrorMessage error>{errorText}</ErrorMessage>
  </Row>
)

export default Footer

// #region styles
const ErrorMessage = styled(Text)`
  font-size: 12px;
`
// #endregion
