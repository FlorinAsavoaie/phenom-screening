import { useCallback } from 'react'
import { get, omit, chain } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Formik, Form as BaseForm } from 'formik'
import { Row, Label, useFetching, Item } from '@hindawi/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'

import { checkTeam } from './validators'
import {
  Header,
  Footer,
  TeamMemberAdder,
  TeamNameInput,
  TeamTypeInput,
} from '../TeamModal'
import { TeamJournalSelector } from './team-journal-selector'

function TeamModalContent({
  team,
  hideModal,
  modalTitle,
  onSubmit,
  modalType,
}) {
  const { isFetching, setFetching, setError, fetchingError } = useFetching()

  const handleOnSubmit = useCallback(
    (values, { setFieldError }) => {
      setFetching(true)
      return onSubmit(values)
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch((error) => {
          setFetching(false)
          const validationError = chain(error)
            .get('graphQLErrors', [])
            .find(({ extensions }) => extensions.name === 'ValidationError')
            .value()
          if (validationError) {
            setError(validationError)
          } else {
            setFieldError('name', parseGQLError(error))
          }
        })
    },
    [onSubmit],
  )

  const teamName = get(team, 'name', '')
  const teamMembers = parseMembers(get(team, 'members', []))
  const teamId = get(team, 'id', null)
  const teamType = get(team, 'type', 'screening')
  const teamJournals = get(team, 'journals', [])

  return (
    <Root>
      <Formik
        initialValues={{
          members: teamMembers,
          journals: teamJournals,
          name: teamName,
          type: teamType,
        }}
        onSubmit={handleOnSubmit}
        validate={checkTeam}
      >
        {({ handleSubmit, errors, submitCount, values }) => (
          <Form>
            <Header hideModal={hideModal} modalTitle={modalTitle} />
            <ModalContent>
              <Row mb={1}>
                <TeamNameInput />
                <TeamTypeInput
                  disabled={
                    modalType === 'editTeam' ||
                    (values.members && values.members.length)
                  }
                  selectedValue={values.type}
                />
              </Row>
              <Row flexDirection="column">
                <Item>
                  <Label height={7} mb={2}>
                    Team Members
                  </Label>
                </Item>
                <Item>
                  <TeamMemberAdder teamType={values.type} />
                </Item>
              </Row>
              <Row flexDirection="column">
                <Item>
                  <Label height={7} mb={2} mt={6}>
                    Journals
                  </Label>
                </Item>
                <Item>
                  <TeamJournalSelector teamId={teamId} />
                </Item>
              </Row>
            </ModalContent>

            <Footer
              errors={errors}
              fetchingError={fetchingError}
              handleSubmit={handleSubmit}
              hideModal={hideModal}
              isFetching={isFetching}
              submitCount={submitCount}
              teamId={teamId}
            />
          </Form>
        )}
      </Formik>
    </Root>
  )
}

const parseMembers = (members) =>
  members.map((member) => omit(member, '__typename'))

export default TeamModalContent

// #region styles
const Root = styled.div`
  align-items: flex-start;
  background: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  border: none;
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: calc(${th('gridUnit')} * 4) 0 calc(${th('gridUnit')} * 4)
    calc(${th('gridUnit')} * 4);
  position: relative;
`

const Form = styled(BaseForm)`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 100%;
`

const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  flex: 2;
  overflow-y: scroll;
  padding-right: 16px;
`
// #endregion
