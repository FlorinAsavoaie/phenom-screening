import { keysIn } from 'lodash'
import { Formik } from 'formik'
import { Button, TextField as BaseTextField } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useCallback } from 'react'
import {
  Row,
  Item,
  Text,
  Label,
  validators,
  ValidatedFormField,
} from '@hindawi/ui'

import { UserRoleInput } from '../TeamModal'

const DEFAULT_FORM_VALUES = {
  givenNames: '',
  surname: '',
  email: '',
  role: null,
}

const AddTeamMemberForm = ({ onSubmit, validate, teamType }) => {
  const handleSubmit = useCallback(
    (values, { resetForm }) => {
      onSubmit(values)
      resetForm({ values: DEFAULT_FORM_VALUES })
    },
    [onSubmit],
  )

  return (
    <Formik
      enableReinitialize
      initialValues={DEFAULT_FORM_VALUES}
      onSubmit={handleSubmit}
      validate={validate}
    >
      {({ handleSubmit, errors, setFieldValue }) => (
        <>
          <Row>
            <Item mr={2} vertical>
              <Label mb={1} required>
                First Name
              </Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="givenNames"
                name="givenNames"
                placeholder="Fill in"
                type="text"
                validate={[validators.required]}
              />
            </Item>
            <Item mr={2} vertical>
              <Label mb={1} required>
                Last Name
              </Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="surname"
                name="surname"
                placeholder="Fill in"
                type="text"
                validate={[validators.required]}
              />
            </Item>
            <Item mr={2} vertical>
              <Label mb={1} required>
                Email
              </Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="email"
                name="email"
                placeholder="Fill in"
                type="text"
                validate={[validators.required, validators.emailValidator]}
              />
            </Item>

            <UserRoleInput
              onChangeRole={() => setFieldValue('role', null)}
              teamType={teamType}
              validators={validators}
            />
            <Item height={8}>
              <AddButton
                data-test-id="add-team-member"
                onClick={handleSubmit}
                type="button"
              >
                Add
              </AddButton>
            </Item>
          </Row>
          {errors.userAlreadyInList && keysIn(errors).length === 1 && (
            <ErrorMessage error>{errors.userAlreadyInList}</ErrorMessage>
          )}
        </>
      )}
    </Formik>
  )
}

export default AddTeamMemberForm

// #region styles
const AddButton = styled(Button)`
  background-color: ${th('textPrimaryColor')};
  border-radius: 4px;
  color: ${th('backgroundColor')};
  height: 32px;
  text-align: center;
  width: 64px;

  :hover,
  :focus {
    background-color: ${th('textSecondaryColor')};
  }
`

const ErrorMessage = styled(Text)`
  margin-top: -18px;
  font-size: 12px;
  margin-left: 8px;
`

const TextField = styled(BaseTextField)`
  margin-bottom: 0;
  width: 149px;
`

// #endregion
