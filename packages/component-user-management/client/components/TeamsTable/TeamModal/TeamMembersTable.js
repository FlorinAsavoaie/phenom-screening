import styled from 'styled-components'
import { Icon, Item, Text } from '@hindawi/ui'

import { Table, TableRow } from 'hindawi-shared/client/components'

function RoleColumn({ item, index, onRemove }) {
  return (
    <>
      <Text fontWeight="bold" title={item.role}>
        {parseRole[item.role]}
      </Text>
      <Item justify="flex-end">
        <DeleteIconButton
          color="black"
          data-test-id="remove-team-member"
          fontSize="16px"
          icon="remove"
          onClick={() => onRemove(index)}
        />
      </Item>
    </>
  )
}

const getColumnDefinitions = (onRemove) => [
  {
    headerName: 'First Name',
    labelFunction: (teamMember) => teamMember.givenNames,
  },
  {
    headerName: 'Last Name',
    labelFunction: (teamMember) => teamMember.surname,
  },
  {
    headerName: 'Email',
    labelFunction: (teamMember) => teamMember.email,
    flex: 2,
  },
  {
    headerName: 'Role',
    component: ({ item, index }) => RoleColumn({ item, index, onRemove }),
  },
]

function TeamMembersTable({ teamMembers, onRemove, error, maxHeight }) {
  return (
    <Table
      columnDefinitions={getColumnDefinitions(onRemove)}
      enableScrolling
      error={error}
      items={teamMembers}
      loading={false}
      maxHeight={maxHeight}
      noItemsMessage="There are currently no team members. Please add one first."
    />
  )
}

const parseRole = {
  admin: 'Admin',
  teamLeader: 'Team Lead',
  screener: 'Screener',
  qualityChecker: 'Quality Checker',
  '': '',
}

export default TeamMembersTable

// #region styles
const DeleteIconButton = styled(Icon)`
  visibility: hidden;
  ${TableRow}:hover & {
    visibility: visible;
  }
`
// #endregion
