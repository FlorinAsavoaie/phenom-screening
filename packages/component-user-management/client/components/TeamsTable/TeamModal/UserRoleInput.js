import { useEffect } from 'react'
import { Item, Label, ValidatedFormField, Menu as BaseMenu } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const ROLE_OPTIONS = {
  screening: [
    { label: 'Screener', value: 'screener' },
    { label: 'Team Lead', value: 'teamLeader' },
  ],
  qualityChecking: [
    { label: 'Quality Checker', value: 'qualityChecker' },
    { label: 'Team Lead', value: 'teamLeader' },
  ],
}

const UserRoleInput = ({ onChangeRole, teamType, validators }) => {
  useEffect(() => {
    onChangeRole()
  }, [teamType])

  return (
    <Item mr={2} vertical>
      <Label mb={1} required>
        Role
      </Label>
      <ValidatedFormField
        component={Menu}
        data-test-id="role"
        name="role"
        options={ROLE_OPTIONS[teamType]}
        placeholder="Select"
        validate={[validators.required]}
      />
    </Item>
  )
}

export default UserRoleInput

const Menu = styled(BaseMenu)`
  width: calc(${th('gridUnit')} * 30);
`
