import { get } from 'lodash'
import { Label } from '@hindawi/ui'
import { FieldArray } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { checkUniqueTeamMember } from './validators'
import { AddTeamMemberForm, TeamMembersTable } from '../TeamModal'

function TeamMemberAdder({ teamType }) {
  return (
    <FieldArray name="members">
      {({ form, insert, remove }) => {
        const members = get(form, 'values.members', [])
        return (
          <TeamMembersContainer>
            <AddTeamMemberForm
              onSubmit={(teamMemberToAdd) => {
                insert(0, teamMemberToAdd)
              }}
              teamType={teamType}
              validate={checkUniqueTeamMember(members)}
            />

            <Label mb={2}>Members</Label>
            <TeamMembersTable
              maxHeight={78}
              onRemove={(index) => remove(index)}
              teamMembers={members}
            />
          </TeamMembersContainer>
        )
      }}
    </FieldArray>
  )
}

export default TeamMemberAdder

// #region styles
const TeamMembersContainer = styled.div`
  background-color: #f5f5f5;
  border-radius: 6px;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 2);
`
// #endregion
