import { Icon } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

function Header({ hideModal, modalTitle }) {
  return (
    <>
      <CloseIcon
        fontSize="28px"
        icon="remove1"
        onClick={hideModal}
        right={8}
        top={12}
      />
      <Title>{modalTitle}</Title>
    </>
  )
}

export default Header

// #region styles
const CloseIcon = styled(Icon)`
  color: #939393;
`
const Title = styled.div`
  color: ${th('textSecondaryColor')};
  margin-bottom: calc(${th('gridUnit')} * 3);
  font-family: ${th('defaultFont')};
  font-size: 20px;
  font-stretch: normal;
  font-style: normal;
  font-weight: bold;
  height: 27px;
  letter-spacing: normal;
  line-height: normal;
`
// #endregion
