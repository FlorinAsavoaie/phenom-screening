import { useState } from 'react'
import { FieldArray } from 'formik'
import { get } from 'lodash'
import { Label, Row } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'

import { SearchField } from 'hindawi-shared/client/components'
import { TeamJournalsTable } from './TeamJournalsTable'
import { useAvailableJournals } from '../../../../graphql/hooks/useAvailableJournals'

export function TeamJournalSelector({ teamId }) {
  const [searchValue, setSearchValue] = useState('')
  const {
    availableJournals,
    insertAvailableJournal,
    loading,
    removeAvailableJournal,
  } = useAvailableJournals({ teamId, searchValue })

  const handleOnChange = (e) => {
    setSearchValue(e.target.value)
  }

  return (
    <Row alignItems="initial" flexDirection="column">
      <FieldArray name="journals">
        {({ form, insert, remove }) => {
          const journals = get(form, 'values.journals', [])

          return (
            <Container>
              <Label mb={2} mt={1}>
                Available Journals
              </Label>
              <SearchField
                disabled={
                  searchValue.length === 0 && availableJournals.length === 0
                }
                onChange={handleOnChange}
                placeholder="Search Journals"
                value={searchValue}
                width={88}
              />

              <TeamJournalsTable
                buttonLabel="Add"
                journals={availableJournals}
                loading={loading}
                maxHeight={60}
                noItemsMessage={
                  searchValue.length === 0
                    ? 'No journals available.'
                    : 'No results found.'
                }
                onClick={({ item }) => {
                  removeAvailableJournal(item)
                  insert(0, item)
                }}
              />
              <Label mb={2} mt={3}>
                Selected Journals
              </Label>

              <TeamJournalsTable
                buttonLabel="Remove"
                journals={journals}
                maxHeight={195}
                noItemsMessage="No journals selected."
                onClick={({ index, item }) => {
                  insertAvailableJournal(item)
                  remove(index)
                }}
              />
            </Container>
          )
        }}
      </FieldArray>
    </Row>
  )
}
// #region styles
const Container = styled.div`
  background-color: #f5f5f5;
  border-radius: 6px;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 2);
`
// #endregion
