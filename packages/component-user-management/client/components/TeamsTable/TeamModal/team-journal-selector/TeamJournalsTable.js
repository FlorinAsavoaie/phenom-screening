import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button as BaseButton } from '@pubsweet/ui'
import { Item, Text } from '@hindawi/ui'

import { Table, TableRow } from 'hindawi-shared/client/components'

export function TeamJournalsTable({
  buttonLabel,
  journals,
  noItemsMessage,
  onClick,
  maxHeight,
  loading,
}) {
  return (
    <Table
      columnDefinitions={[
        {
          headerName: 'Journal Name',
          component: ({ item, index }) => (
            <>
              <Text title={item.name}>{item.name}</Text>
              <Item justify="flex-end">
                <Button
                  data-test-id={`${buttonLabel.toLowerCase()}-journal`}
                  onClick={() => onClick({ item, index })}
                  type="button"
                >
                  {buttonLabel}
                </Button>
              </Item>
            </>
          ),
        },
      ]}
      enableScrolling
      items={journals}
      loading={loading}
      maxHeight={maxHeight}
      noItemsMessage={noItemsMessage}
    />
  )
}

// #region styles
const Button = styled(BaseButton)`
  align-items: center;
  background-color: ${th('textPrimaryColor')};
  border: 0;
  color: ${th('backgroundColor')};
  display: flex;
  height: calc(${th('gridUnit')} * 6);
  text-align: center;
  text-align: center;
  width: calc(${th('gridUnit')} * 15);

  :hover,
  :focus {
    background-color: ${th('textPrimaryColor')};
  }
  visibility: hidden;

  ${TableRow}:hover & {
    visibility: visible;
  }
`
// #endregion
