import { useState, useEffect } from 'react'
import { ValidatedFormField, Item, Label } from '@hindawi/ui'
import styled from 'styled-components'
import { RadioGroup } from 'hindawi-shared/client/components'

const initialOptions = [
  { label: 'Screening', value: 'screening', checked: false },
  { label: 'Quality Checking', value: 'qualityChecking', checked: false },
]

function setInitialValues(options, selectedValue) {
  return options.map((option) => {
    if (option.value === selectedValue) {
      return { ...option, checked: true }
    }
    return { ...option, checked: false }
  })
}

function TeamTypeInput({ selectedValue, disabled }) {
  const [options, setOptions] = useState(
    setInitialValues(initialOptions, selectedValue),
  )

  useEffect(() => {
    setOptions(setInitialValues(initialOptions, selectedValue))
  }, [selectedValue])

  return (
    <StyledItem ml={12} vertical>
      <Label mb={2}>Team Type</Label>
      <ValidatedFormField
        component={RadioGroup}
        disabled={disabled}
        name="type"
        options={options}
      />
    </StyledItem>
  )
}

export default TeamTypeInput

const StyledItem = styled(Item)`
  height: 100%;
`
