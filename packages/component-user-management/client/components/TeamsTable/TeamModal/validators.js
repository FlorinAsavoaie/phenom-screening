const areHavingSameEmail = (teamMember, anotherTeamMember) =>
  teamMember.email.toLowerCase() === anotherTeamMember.email.toLowerCase()

const isTeamLead = (teamMember) => teamMember.role === 'teamLeader'
const isChecker = (teamMember) =>
  ['screener', 'qualityChecker'].includes(teamMember.role)

const checkerTypes = {
  qualityChecking: 'Quality Checker',
  screening: 'Screener',
}
const checkUniqueTeamMember =
  (members = []) =>
  (memberToCheck) => {
    const errors = {}

    const isDuplicated = members.some((member) =>
      areHavingSameEmail(member, memberToCheck),
    )
    if (isDuplicated) {
      errors.userAlreadyInList =
        'This user is already added to the member list.'
    }

    return errors
  }

const checkTeam = (team) => {
  const errors = {}

  const hasTeamLeaders = team.members && team.members.some(isTeamLead)
  const hasCheckers = team.members && team.members.some(isChecker)

  if (!team.members || !team.members.length) {
    errors.noTeamMember = `A team should have at least one Team Leader and one ${
      checkerTypes[team.type]
    }.`
  } else if (!hasTeamLeaders) {
    errors.noTeamMember = `A team should have at least one Team Leader assigned.`
  } else if (!hasCheckers) {
    errors.noTeamMember = `A team should have at least one ${
      checkerTypes[team.type]
    } assigned.`
  }

  return errors
}

export { checkUniqueTeamMember, checkTeam }
