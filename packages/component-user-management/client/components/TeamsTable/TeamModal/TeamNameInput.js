import { Item, Row, Label, validators, ValidatedFormField } from '@hindawi/ui'
import styled from 'styled-components'
import { TextField } from '@pubsweet/ui'

function TeamNameInput() {
  return (
    <Row width={52}>
      <Item vertical>
        <Label mb="2px" required>
          Name
        </Label>
        <ValidatedFormField
          component={StyledTextField}
          data-test-id="name"
          name="name"
          placeholder="Fill in"
          type="text"
          validate={[validators.required]}
        />
      </Item>
    </Row>
  )
}

export default TeamNameInput

const StyledTextField = styled(TextField)`
  margin-bottom: 0;
`
