import { space } from 'styled-system'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Modal, Button, Icon } from '@pubsweet/ui'

import { TeamModalContent } from './TeamModal'
import { useCreateTeam } from '../../graphql/hooks'
import { prepareTeamInput } from './prepareTeamInput'

function CreateTeamModal({ onTeamCreate }) {
  const { createTeam } = useCreateTeam()

  return (
    <Modal
      component={(props) => (
        <TeamModalContent
          modalTitle="Create New Team"
          modalType="createTeam"
          {...props}
        />
      )}
      justifyContent="flex-end"
      modalKey="createTeamModal"
      onSubmit={(values) =>
        createTeam(prepareTeamInput(values)).then(onTeamCreate)
      }
    >
      {(showModal) => (
        <CreateTeamButton mb={2} onClick={showModal} primary>
          <StyledIcon color="white" mt={1 / 2} size={5}>
            plus
          </StyledIcon>
          <Label>Create Team</Label>
        </CreateTeamButton>
      )}
    </Modal>
  )
}

// #region styles
const CreateTeamButton = styled(Button)`
  align-items: center;
  border: none;
  color: ${th('white')};
  font-size: 13px;
  font-stretch: normal;
  font-style: normal;
  font-weight: bold;
  height: 24px;
  letter-spacing: normal;
  line-height: normal;
  text-align: center;
  width: calc(${th('gridUnit')} * 32);

  :hover,
  :focus {
    background-color: ${th('actionPrimaryColor')};
  }
`

const StyledIcon = styled(Icon)`
  ${space};
`

const Label = styled.span`
  margin-bottom: 3px;
`
// #endregion

export default CreateTeamModal
