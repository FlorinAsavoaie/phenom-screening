import { Text, Item } from '@hindawi/ui'

import { Table } from 'hindawi-shared/client/components'
import EditTeamModal from './EditTeamModal'

const teamTypesDictionary = {
  screening: 'Screening',
  qualityChecking: 'Quality Checking',
}

function getTeamsJournals(journals = []) {
  return journals.reduce(
    (acc, el, index) => `${acc}${index !== 0 ? ', ' : ' '}${el.name}`,
    '',
  )
}

function AssignedJournalsColumn({ item, onTeamEdit }) {
  return (
    <>
      {item.journals.length === 0 ? (
        <Text>No journals assigned.</Text>
      ) : (
        <Text fontWeight="700" my={1}>
          {getTeamsJournals(item.journals)}
        </Text>
      )}
      <Item justify="flex-end" ml={4}>
        <EditTeamModal onTeamEdit={onTeamEdit} teamId={item.id} />
      </Item>
    </>
  )
}

const getColumnDefinitions = (onTeamEdit) => [
  {
    headerName: 'Team Name',
    labelFunction: (team) => team.name,
  },
  {
    headerName: 'Team Type',
    labelFunction: (team) => teamTypesDictionary[team.type],
  },
  {
    headerName: '# of Members',
    labelFunction: (team) => team.members.length,
  },
  {
    headerName: 'Assigned Journals',
    component: ({ item }) => AssignedJournalsColumn({ item, onTeamEdit }),
    flex: 2,
  },
]

export default function TeamsTable({
  error,
  loading,
  onTeamEdit,
  noItemsMessage,
  teams,
}) {
  return (
    <Table
      columnDefinitions={getColumnDefinitions(onTeamEdit)}
      error={error}
      items={teams}
      loading={loading}
      noItemsMessage={noItemsMessage}
    />
  )
}
