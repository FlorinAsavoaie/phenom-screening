import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Modal, Button } from '@pubsweet/ui'
import { useQuery } from '@apollo/client'
import { Loader } from '@hindawi/ui'
import { queries } from 'component-user-management/client'

import { TableRow } from 'hindawi-shared/client/components'
import { TeamModalContent } from './TeamModal'
import { useEditTeam } from '../../graphql/hooks'
import { prepareTeamInput } from './prepareTeamInput'

function EditTeam({ teamId, ...props }) {
  const { loading, data } = useQuery(queries.getTeam, {
    variables: { teamId },
  })
  const team = get(data, 'getTeam', [])

  if (loading) {
    return <Loader mt={4} mx="auto" />
  }

  return <TeamModalContent {...props} modalTitle="Edit Team" team={team} />
}

export default function EditTeamModal({ teamId, onTeamEdit }) {
  const { editTeam } = useEditTeam(teamId)

  return (
    <Modal
      component={(props) => <EditTeam {...props} teamId={teamId} />}
      justifyContent="flex-end"
      modalKey={teamId}
      modalType="editTeam"
      onSubmit={({ type, ...values }) =>
        editTeam(prepareTeamInput(values)).then(onTeamEdit)
      }
    >
      {(showModal) => (
        <EditTeamButton onClick={showModal} small>
          Edit
        </EditTeamButton>
      )}
    </Modal>
  )
}

// #region styles
const EditTeamButton = styled(Button)`
  background-color: ${th('mainTextColor')};
  border-color: ${th('mainTextColor')};
  color: ${th('white')};
  visibility: hidden;
  height: calc(${th('gridUnit')} * 6);
  min-width: calc(${th('gridUnit')} * 5);

  &:hover {
    background-color: ${th('mainTextColor')};
    border-color: ${th('mainTextColor')};
  }
  ${TableRow}:hover & {
    visibility: visible;
  }
`
// #endregion
