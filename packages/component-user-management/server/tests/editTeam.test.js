const Email = require('@pubsweet/component-email-templating')
const models = require('@pubsweet/models')
const useCases = require('../src/use-cases')
const Chance = require('chance')
const notifications = require('./../src/services/notificationService/notifications')

const chance = new Chance()

const { User, Journal, Team } = models

const notificationService = notifications.initialize({ Email })
notificationService.notifyUserToConfirmAccount = jest.fn()

const teamLeader = {
  id: chance.guid(),
  role: User.Roles.TEAM_LEADER,
  email: chance.email(),
  surname: chance.last(),
  givenNames: chance.first(),
  confirmationToken: chance.guid(),
}

const checker = {
  id: chance.guid(),
  role: User.Roles.SCREENER,
  email: chance.email(),
  surname: chance.last(),
  givenNames: chance.first(),
  confirmationToken: chance.guid(),
}

const userFactory = {
  createUser: jest.fn(),
}

const assignUnassignedManuscriptsToCheckers = {
  execute: jest.fn(),
}

const automaticReassignManuscriptToChecker = {
  execute: jest.fn(),
}

const mappers = {
  teamMapper: {
    domainToCheckerTeamEventModel: jest.fn(),
  },
}
const publishMessage = jest.fn()

global.applicationEventBus = {
  publishMessage,
}

describe('Edit team use case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it("should throw an error if a team doesn't have at least one teamLeader", async () => {
    const params = {
      teamId: chance.guid(),
      name: chance.name(),
      members: [checker],
      journalIds: [],
    }

    const result = useCases.editTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: {
          userFactory,
        },
        mappers,
        notificationService,
        useCases: {
          assignUnassignedManuscriptsToCheckers,
          automaticReassignManuscriptToChecker,
        },
      })
      .execute(params)

    await expect(result).rejects.toThrow(
      'A team should have at least one team leader.',
    )
  })

  it("should throw an error if a team doesn't have at least one checker", async () => {
    const params = {
      teamId: chance.guid(),
      name: chance.name(),
      members: [teamLeader],
      journalIds: [],
    }

    const result = useCases.editTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: {
          userFactory,
        },
        mappers,
        notificationService,
        useCases: {
          assignUnassignedManuscriptsToCheckers,
          automaticReassignManuscriptToChecker,
        },
      })
      .execute(params)

    await expect(result).rejects.toThrow(
      'A team should have at least one checker.',
    )
  })

  it("should throw an error if a team doesn't have at least on confirmed teamLeader are has manuscripts assigned", async () => {
    const params = {
      teamId: chance.guid(),
      name: 'New team',
      members: [teamLeader, checker],
      journalIds: [],
    }
    jest.spyOn(Team, 'findOneBy').mockReturnValue({
      users: [],
      manuscripts: [{}],
      id: params.teamId,
    })

    jest.spyOn(User, 'findOneByEmail').mockReturnValue({
      role: User.Roles.TEAM_LEADER,
      isConfirmed: false,
    })

    const result = useCases.editTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        mappers,
        factories: {
          userFactory,
        },
        notificationService,
        useCases: {
          assignUnassignedManuscriptsToCheckers,
          automaticReassignManuscriptToChecker,
        },
      })
      .execute(params)

    await expect(result).rejects.toThrow(
      'A team should have at least one confirmed team leader.',
    )
  })

  it('should replace members with new team lead', async () => {
    Team.findOneBy = jest
      .fn()
      .mockReturnValueOnce(undefined)
      .mockReturnValueOnce({ users: [], manuscripts: [] })
      .mockReturnValueOnce({ users: [], manuscripts: [] })

    User.findOneByEmail = jest
      .fn()
      .mockReturnValueOnce(undefined)
      .mockReturnValueOnce(checker)

    User.findOneBy = jest
      .fn()
      .mockReturnValue({ created: chance.date(), updated: chance.date() })
    Journal.findIn = jest.fn().mockReturnValue([])

    const mockUpsertGraph = jest.fn().mockReturnValue({ users: [] })
    Team.upsertGraph = mockUpsertGraph

    const newTeamLead = {
      role: User.Roles.TEAM_LEADER,
      email: chance.email(),
      surname: chance.last(),
      givenNames: chance.first(),
      isConfirmed: true,
    }

    jest.spyOn(userFactory, 'createUser').mockReturnValue(newTeamLead)

    const params = {
      teamId: chance.guid(),
      name: chance.name(),
      members: [teamLeader, checker],
      journalIds: [],
    }

    await useCases.editTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        mappers,
        factories: {
          userFactory,
        },
        notificationService,
        useCases: {
          assignUnassignedManuscriptsToCheckers,
          automaticReassignManuscriptToChecker,
        },
      })
      .execute(params)

    expect(mockUpsertGraph).toHaveBeenCalledWith(
      expect.objectContaining({
        id: params.teamId,
        name: params.name,
        users: [
          {
            email: newTeamLead.email,
            givenNames: newTeamLead.givenNames,
            surname: newTeamLead.surname,
            role: newTeamLead.role,
            isConfirmed: true,
            assignationDate: null,
          },
          {
            id: params.members[1].id,
            email: params.members[1].email,
            givenNames: params.members[1].givenNames,
            surname: params.members[1].surname,
            confirmationToken: params.members[1].confirmationToken,
            role: params.members[1].role,
            assignationDate: null,
          },
        ],
      }),
      {
        relate: true,
        unrelate: true,
      },
    )
  })

  it('should add a new screener to the team, and send him a confirmation email', async () => {
    Team.findOneBy = jest
      .fn()
      .mockReturnValueOnce(undefined)
      .mockReturnValueOnce({ users: [], manuscripts: [] })
      .mockReturnValueOnce({ users: [], manuscripts: [] })

    const params = {
      teamId: chance.guid(),
      name: chance.name(),
      members: [teamLeader, checker],
      journalIds: [],
    }

    User.findOneByEmail = jest
      .fn()
      .mockReturnValueOnce({ ...teamLeader, isConfirmed: true })
      .mockReturnValueOnce(undefined)

    Journal.findIn = jest.fn().mockReturnValue([])

    const newChecker = {
      role: User.Roles.SCREENER,
      email: chance.email(),
      surname: chance.last(),
      givenNames: chance.first(),
    }

    jest.spyOn(userFactory, 'createUser').mockReturnValue(newChecker)

    const savedTeam = {
      users: [
        new User({
          id: params.members[0].id,
          email: params.members[0].email,
          givenNames: params.members[0].givenNames,
          surname: params.members[0].surname,
          role: params.members[0].role,
          confirmationToken: params.members[0].confirmationToken,
        }),
        new User({
          id: newChecker.id,
          email: newChecker.email,
          givenNames: newChecker.givenNames,
          surname: newChecker.surname,
          role: params.members[1].role,
          confirmationToken: chance.string(),
        }),
      ],
      type: 'screening',
    }
    const mockUpsertGraph = jest.fn().mockReturnValue(savedTeam)
    Team.upsertGraph = mockUpsertGraph

    await useCases.editTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        mappers,
        factories: {
          userFactory,
        },
        notificationService,
        useCases: {
          assignUnassignedManuscriptsToCheckers,
          automaticReassignManuscriptToChecker,
        },
      })
      .execute(params)

    expect(mockUpsertGraph).toHaveBeenCalledWith(
      expect.objectContaining({
        id: params.teamId,
        name: params.name,
        users: [
          {
            id: params.members[0].id,
            email: params.members[0].email,
            givenNames: params.members[0].givenNames,
            surname: params.members[0].surname,
            confirmationToken: params.members[0].confirmationToken,
            role: params.members[0].role,
            isConfirmed: true,
            assignationDate: null,
          },
          {
            email: newChecker.email,
            givenNames: newChecker.givenNames,
            surname: newChecker.surname,
            role: newChecker.role,
            assignationDate: null,
          },
        ],
      }),
      {
        relate: true,
        unrelate: true,
      },
    )
    expect(notificationService.notifyUserToConfirmAccount).toHaveBeenCalledWith(
      expect.objectContaining({
        user: {
          id: savedTeam.users[1].id,
          email: savedTeam.users[1].email,
          name: savedTeam.users[1].fullName,
          role: savedTeam.users[1].roleLabel,
          confirmationToken: savedTeam.users[1].confirmationToken,
        },
      }),
    )

    expect(publishMessage).toHaveBeenCalled()
  })

  it('should fail with -> Team name already exists.', async () => {
    const params = {
      teamId: chance.guid(),
      name: chance.name(),
      members: [
        {
          id: chance.guid(),
          role: User.Roles.TEAM_LEADER,
          email: chance.email(),
          surname: chance.last(),
          givenNames: chance.first(),
        },
        {
          id: chance.guid(),
          role: User.Roles.SCREENER,
          email: chance.email(),
          surname: chance.last(),
          givenNames: chance.first(),
        },
      ],
      journalIds: [],
    }

    Team.findOneBy = jest.fn().mockReturnValue({ id: chance.guid() })

    const result = useCases.editTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: {
          userFactory,
        },
        mappers,
        notificationService,
        useCases: {
          assignUnassignedManuscriptsToCheckers,
          automaticReassignManuscriptToChecker,
        },
      })
      .execute(params)

    await expect(result).rejects.toThrow(`Team name already exists.`)
  })
})
