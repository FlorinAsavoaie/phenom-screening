const Email = require('@pubsweet/component-email-templating')
const { Team, User, Journal } = require('@pubsweet/models')
const Chance = require('chance')
const notifications = require('./../src/services/notificationService/notifications')
const userFactory = require('../../../component-model/src/user/userFactory')
const useCases = require('../src/use-cases')

const chance = new Chance()
// #region mocking

const users = [
  {
    role: 'teamLeader',
    email: 'something@test.com',
    givenNames: 'John',
    surname: 'Doe',
  },
]
const savedTeam = {
  users: [
    {
      id: '12345',
      role: 'teamLeader',
      email: 'something@test.com',
      givenNames: 'John',
      surname: 'Doe',
      confirmationToken: 'unique-id',
      fullName: 'John Doe',
      roleLabel: 'Team Leader',
    },
  ],
  typeLabel: 'screening',
}
const mockedTeam = {
  id: 'some-team-id',
  saveGraph: jest.fn().mockReturnValue(savedTeam),
  users,
}

const mockedUser = {
  findOneBy: jest.fn(),
}

const mockedJournal = {
  findAll: jest.fn(),
}

jest.mock('@pubsweet/models', () => ({
  Team: jest.fn().mockImplementation(() => mockedTeam),
  User: jest.fn().mockImplementation(() => mockedUser),
  Journal: jest.fn().mockImplementation(() => mockedJournal),
}))

const notificationService = notifications.initialize({ Email })
notificationService.notifyUserToConfirmAccount = jest.fn()
notificationService.notifyUserThatHeWasAssignedInATeam = jest.fn()

// #endregion

describe.skip('Create team use case', () => {
  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    Team.mockClear()
    mockedTeam.saveGraph.mockClear()

    User.mockClear()
    notificationService.notifyUserToConfirmAccount.mockClear()
  })

  it('should throw an error if the team name already exists', async () => {
    Team.findOneBy = jest.fn().mockReturnValue({})

    const teamData = {
      name: 'D',
      members: [],
      journalIds: [],
      type: 'screening',
    }

    const result = useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute(teamData)

    await expect(result).rejects.toThrow(
      `Team name ${teamData.name} already exists.`,
    )
    expect(Team.findOneBy).toHaveBeenCalledTimes(1)
    expect(Team.findOneBy).toHaveBeenCalledWith({
      queryObject: {
        name: 'D',
      },
    })
  })

  it("should throw error if the members don't have the correct role for team type", async () => {
    Team.findOneBy = jest.fn().mockReturnValue(null)
    Journal.findAll = jest.fn().mockReturnValue([])
    User.findOneBy = jest.fn().mockReturnValue({
      id: '123',
    })

    const result = useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute({
        name: 'D',
        members: [
          {
            id: '123',
            givenNames: 'John',
            surname: 'Doe',
            email: 'something@test.com',
            role: 'qualityChecker',
          },
        ],
        type: 'screening',
      })

    await expect(result).rejects.toThrow(
      "Member role 'qualityChecker' is not allowed for team type screening.",
    )
  })

  it('should save the team', async () => {
    Team.findOneBy = jest.fn().mockReturnValue(null)
    Journal.findIn = jest.fn().mockReturnValue([])

    await useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute({
        name: 'D',
        members: [],
        type: 'screening',
      })

    expect(Team).toHaveBeenCalledTimes(1)
    expect(Team).toHaveBeenCalledWith({
      name: 'D',
      type: 'screening',
      journals: [],
      users: [],
    })

    expect(mockedTeam.saveGraph).toHaveBeenCalledTimes(1)
    expect(mockedTeam.saveGraph).toHaveBeenCalledWith({
      relate: true,
    })
  })

  it('should assign all journals', async () => {
    const journals = [
      {
        name: 'journal name',
        publisherName: 'publisher name',
      },
      {
        name: 'another journal name',
        publisherName: 'another publisher name',
      },
    ]
    Team.findOneBy = jest.fn().mockReturnValue(null)
    Journal.findIn = jest.fn().mockReturnValue(journals)

    await useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute({
        name: 'D',
        members: [],
        type: 'screening',
      })

    expect(Team).toHaveBeenCalledTimes(1)
    expect(Team).toHaveBeenCalledWith({
      name: 'D',
      type: 'screening',
      journals,
      users: [],
    })
  })

  it('should save and notify the user to confirm account if he is not in the DB already', async () => {
    User.findOneByEmail = jest.fn().mockReturnValue(null)
    Team.findOneBy = jest.fn().mockReturnValue(null)

    await useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute({
        name: 'D',
        type: 'screening',
        members: [
          {
            givenNames: 'John',
            surname: 'Doe',
            email: 'something@test.com',
            role: 'teamLeader',
          },
        ],
      })

    expect(User.findOneByEmail).toHaveBeenCalledTimes(1)
    expect(User.findOneByEmail).toHaveBeenCalledWith('something@test.com')

    expect(
      notificationService.notifyUserToConfirmAccount,
    ).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyUserToConfirmAccount).toHaveBeenCalledWith(
      {
        user: {
          id: '12345',
          confirmationToken: 'unique-id',
          email: 'something@test.com',
          name: 'John Doe',
          role: 'Team Leader',
        },
      },
    )
  })

  it('should not save/notify the user to confirm account if he is already in the DB', async () => {
    User.findOneByEmail = jest.fn().mockReturnValue({
      id: 'some-id',
    })

    mockedTeam.users = [
      {
        id: '12345',
        role: 'teamLeader',
        email: 'something@test.com',
        givenNames: 'John',
        surname: 'Doe',
        confirmationToken: 'unique-id',
      },
    ]

    await useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute({
        name: 'D',
        type: 'screening',
        members: [
          {
            givenNames: 'John',
            surname: 'Doe',
            email: 'something@test.com',
            role: 'teamLeader',
          },
        ],
      })

    expect(User.findOneByEmail).toHaveBeenCalledTimes(1)
    expect(User.findOneByEmail).toHaveBeenCalledWith('something@test.com')

    expect(
      notificationService.notifyUserToConfirmAccount,
    ).not.toHaveBeenCalled()
  })

  it('should return the team', async () => {
    User.findOneByEmail = jest.fn().mockReturnValue(null)
    Team.findOneBy = jest.fn().mockReturnValue(null)
    Journal.findIn = jest.fn().mockReturnValue([])

    const result = await useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute({
        name: 'D',
        type: 'screening',
        members: [
          {
            givenNames: 'John',
            surname: 'Doe',
            email: 'something@test.com',
            role: 'teamLeader',
          },
        ],
      })
    expect(result).toBe(savedTeam)
  })

  it('should create team only if it is an authenticated user and admin', () => {
    const { authsomePolicies } = useCases.createTeamUseCase
    expect(authsomePolicies.length).toBe(2)
    expect(authsomePolicies).toContain('authenticatedUser')
    expect(authsomePolicies).toContain('isAdmin')
  })

  it('should send email to inform confirmed existing users that they were assigned to a team', async () => {
    Team.findOneBy = jest.fn().mockReturnValue(null)
    Journal.findIn = jest.fn().mockReturnValue([])
    User.findOneByEmail = jest.fn().mockReturnValueOnce({})

    mockedTeam.users = [
      {
        id: chance.guid(),
        email: chance.email(),
        isConfirmed: true,
      },
      {
        email: chance.email(),
      },
    ]

    savedTeam.name = 'D'
    savedTeam.users = [
      {
        id: '12345',
        role: 'teamLeader',
        email: mockedTeam.users[0].email,
        confirmationToken: 'unique-id',
        fullName: 'John Doe',
        roleLabel: 'Team Leader',
      },
      {
        id: chance.guid(),
        email: mockedTeam.users[1].email,
        fullName: 'John Doe',
        roleLabel: 'Team Leader',
      },
    ]

    const params = {
      name: 'D',
      type: 'screening',
      members: [
        {
          id: '12345',
          name: 'John Doe',
          email: mockedTeam.users[0].email,
          role: 'teamLeader',
          confirmationToken: 'unique-id',
        },
        {
          name: 'John Doe',
          email: mockedTeam.users[1].email,
          role: 'teamLeader',
        },
      ],
    }
    await useCases.createTeamUseCase
      .initialize({
        models: { Team, User, Journal },
        factories: { userFactory },
        notificationService,
      })
      .execute(params)

    expect(
      notificationService.notifyUserToConfirmAccount,
    ).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyUserThatHeWasAssignedInATeam,
    ).toHaveBeenCalledWith({
      user: {
        id: params.members[0].id,
        email: params.members[0].email,
        name: params.members[0].name,
        confirmationToken: params.members[0].confirmationToken,
        role: 'Team Leader',
      },
      team: {
        name: params.name,
        type: 'screening',
      },
    })
  })
})
