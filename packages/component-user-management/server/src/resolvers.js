const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { withAuthsome } = require('hindawi-utils')
const { factories } = require('component-model')
const notifications = require('./services/notificationService/notifications')
const useCases = require('./use-cases')

const { mappers } = require('component-model')

const notificationService = notifications.initialize({ Email })

function assignUnassignedManuscriptsToCheckers() {
  return useCases.assignUnassignedManuscriptsToCheckersUseCase.initialize({
    models,
    mappers,
  })
}

function automaticReassignManuscriptToChecker() {
  return useCases.automaticReassignManuscriptToCheckerUseCase.initialize({
    models,
    mappers,
  })
}

const resolvers = {
  Mutation: {
    async createTeam(_, { input }, ctx) {
      return useCases.createTeamUseCase
        .initialize({
          models,
          mappers,
          factories,
          notificationService,
          useCases: {
            assignUnassignedManuscriptsToCheckers:
              assignUnassignedManuscriptsToCheckers(),
          },
        })
        .execute(input)
    },
    async editTeam(_, { input }, ctx) {
      return useCases.editTeamUseCase
        .initialize({
          models,
          mappers,
          factories,
          notificationService,
          useCases: {
            assignUnassignedManuscriptsToCheckers:
              assignUnassignedManuscriptsToCheckers(),
            automaticReassignManuscriptToChecker:
              automaticReassignManuscriptToChecker(),
          },
        })
        .execute(input)
    },
  },
  Query: {
    async getAvailableJournals(_, params, ctx) {
      return useCases.getAvailableJournalsUseCase
        .initialize({ models })
        .execute(params)
    },
  },
}

module.exports = withAuthsome(resolvers, useCases)
