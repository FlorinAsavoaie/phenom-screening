const initialize = ({ models: { Journal, Team } }) => ({
  async execute({ teamId }) {
    const allJournals = await Journal.findAll()

    if (!teamId) {
      return allJournals
    }

    const team = await Team.find(teamId, 'journals')

    const assignedJournalIds = team.journals.map((j) => j.id)

    return allJournals.filter((j) => !assignedJournalIds.includes(j.id))
  },
})

const authsomePolicies = ['authenticatedUser', 'isAdmin']

module.exports = {
  initialize,
  authsomePolicies,
}
