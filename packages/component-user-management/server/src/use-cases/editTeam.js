const { Promise } = require('bluebird')

const initialize = ({
  models: { Team, User, Journal },
  mappers: { teamMapper },
  factories: { userFactory },
  useCases: {
    assignUnassignedManuscriptsToCheckers,
    automaticReassignManuscriptToChecker,
  },
  notificationService,
}) => {
  return {
    execute,
  }

  async function execute({ teamId, name, members, journalIds }) {
    const hasTeamLeaders = members.some(
      (member) => member.role === User.Roles.TEAM_LEADER,
    )
    if (!hasTeamLeaders) {
      throw new ValidationError('A team should have at least one team leader.')
    }

    const hasCheckers = members.some((member) =>
      [User.Roles.SCREENER, User.Roles.QUALITY_CHECKER].includes(member.role),
    )

    if (!hasCheckers) {
      throw new ValidationError('A team should have at least one checker.')
    }

    const teamByName = await Team.findOneBy({
      queryObject: {
        name,
      },
    })

    if (teamByName && teamByName.id !== teamId) {
      throw new Error(`Team name already exists.`)
    }

    const dbTeam = await Team.findOneBy({
      queryObject: {
        id: teamId,
      },
      eagerLoadRelations: '[manuscripts.user, users]',
    })
    const memberIds = members.map((member) => member.id)

    const deletedUserIds = dbTeam.users
      .filter((user) => !memberIds.includes(user.id))
      .map((mm) => mm.id)

    const teamMembers = []

    await Promise.each(members, async (member) => {
      const dbUser = await User.findOneByEmail(member.email)

      let teamMember
      if (dbUser) {
        teamMember = dbUser
      } else {
        teamMember = await userFactory.createUser(member)
      }

      const assignationDate = getMemberAssignationDate(dbTeam, teamMember)

      teamMembers.push({
        ...teamMember,
        role: member.role,
        assignationDate,
      })
    })

    const confirmedTLs = teamMembers.filter(
      (member) => member.role === User.Roles.TEAM_LEADER && member.isConfirmed,
    )

    if (!confirmedTLs.length && dbTeam.manuscripts.length > 0) {
      throw new ValidationError(
        'A team should have at least one confirmed team leader.',
      )
    }

    const journals = await Journal.findIn('id', journalIds)

    let team = new Team({
      id: teamId,
      name,
      type: dbTeam.type,
      journals,
      users: teamMembers,
    })

    const insertedUserEmails = team.users
      .filter((user) => user.id === undefined)
      .map((user) => user.email)

    team = await Team.upsertGraph(team, {
      relate: true,
      unrelate: true,
    })

    await Promise.each(dbTeam.manuscripts, async (man) => {
      if (man.user && deletedUserIds.includes(man.user.id)) {
        await automaticReassignManuscriptToChecker.execute(man)
      }
    })

    team.users
      .filter((user) => insertedUserEmails.includes(user.email))
      .forEach((user) => {
        notificationService.notifyUserToConfirmAccount({
          user: {
            id: user.id,
            email: user.email,
            name: user.fullName,
            confirmationToken: user.confirmationToken,
            role: user.roleLabel,
          },
        })
      })

    const dbTeamUserIds = dbTeam.users.map((user) => user.id)

    team.users
      .filter(
        (member) => member.isConfirmed && !dbTeamUserIds.includes(member.id),
      )
      .forEach((user) => {
        notificationService.notifyUserThatHeWasAssignedInATeam({
          user: {
            id: user.id,
            email: user.email,
            name: user.fullName,
            confirmationToken: user.confirmationToken,
            role: user.roleLabel,
          },
          team: {
            name: team.name,
            type: team.typeLabel,
          },
        })
      })

    await emitEventWhenCheckerTeamIsUpdated(teamId)

    await assignUnassignedManuscriptsToCheckers.execute({
      team,
      teamMembers,
      journalIds,
      teamType: dbTeam.type,
    })

    return team
  }

  async function emitEventWhenCheckerTeamIsUpdated(teamId) {
    const teamDB = await Team.findOneBy({
      queryObject: { id: teamId },
      eagerLoadRelations: '[users,journals]',
    })

    const users = await Promise.map(teamDB.users, async (u) => {
      const user = await User.findOneBy({
        queryObject: { id: u.id },
      })

      return {
        ...u,
        created: user.created,
        updated: user.updated,
      }
    })

    const mappedTeam = teamMapper.domainToCheckerTeamEventModel({
      ...teamDB,
      users,
    })

    await applicationEventBus.publishMessage({
      event: 'CheckerTeamUpdated',
      data: mappedTeam,
    })
  }

  function getMemberAssignationDate(team, member) {
    const alreadyInTeamMember = team.users.find(
      (user) => user.email === member.email,
    )
    if (!alreadyInTeamMember) return null
    return alreadyInTeamMember.assignationDate
  }
}

const authsomePolicies = ['authenticatedUser', 'isAdmin']

module.exports = {
  initialize,
  authsomePolicies,
}
