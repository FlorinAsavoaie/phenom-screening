const { Promise } = require('bluebird')

const initialize = ({
  models: { Team, User, Journal },
  mappers: { teamMapper },
  factories: { userFactory },
  useCases: { assignUnassignedManuscriptsToCheckers },
  notificationService,
}) => {
  return {
    execute,
  }

  async function execute({ name, type, members, journalIds }) {
    const hasTeamLeaders = members.some(
      (member) => member.role === User.Roles.TEAM_LEADER,
    )
    if (!hasTeamLeaders) {
      throw new ValidationError('A team should have at least one team leader')
    }

    const hasCheckers = members.some((member) =>
      [User.Roles.SCREENER, User.Roles.QUALITY_CHECKER].includes(member.role),
    )

    if (!hasCheckers) {
      throw new ValidationError('A team should have at least one checker')
    }

    const teamByName = await Team.findOneBy({
      queryObject: {
        name,
      },
    })
    if (teamByName) {
      throw new ValidationError(`Team name ${name} already exists.`)
    }

    const teamMembers = await Promise.mapSeries(members, async (member) => {
      verifyMemberRole(member.role, type)
      const dbUser = await User.findOneByEmail(member.email)
      let teamMember
      if (dbUser) {
        teamMember = dbUser
      } else {
        teamMember = await userFactory.createUser(member)
      }
      return {
        ...teamMember,
        role: member.role,
      }
    })
    const journals = await Journal.findIn('id', journalIds)

    let team = new Team({
      name,
      type,
      journals,
      users: teamMembers,
    })

    const insertedUserEmails = team.users
      .filter((user) => !user.id)
      .map((user) => user.email)

    const existingConfirmedUserEmails = team.users
      .filter((user) => user.id && user.isConfirmed)
      .map((user) => user.email)

    team = await team.saveGraph({
      relate: true,
    })

    team.users
      .filter((user) => insertedUserEmails.includes(user.email))
      .forEach((user) =>
        notificationService.notifyUserToConfirmAccount({
          user: {
            id: user.id,
            email: user.email,
            name: user.fullName,
            confirmationToken: user.confirmationToken,
            role: user.roleLabel,
          },
        }),
      )

    team.users
      .filter((user) => existingConfirmedUserEmails.includes(user.email))
      .forEach((user) =>
        notificationService.notifyUserThatHeWasAssignedInATeam({
          user: {
            id: user.id,
            email: user.email,
            name: user.fullName,
            confirmationToken: user.confirmationToken,
            role: user.roleLabel,
          },
          team: {
            name: team.name,
            type: team.typeLabel,
          },
        }),
      )

    await emitEventWhenCheckerTeamIsCreated(team)

    await assignUnassignedManuscriptsToCheckers.execute({
      team,
      teamMembers,
      journalIds,
      teamType: type,
    })

    return team
  }

  function verifyMemberRole(memberRole, teamType) {
    const teamsType = {
      [Team.Type.SCREENING]: [User.Roles.TEAM_LEADER, User.Roles.SCREENER],
      [Team.Type.QUALITY_CHECKING]: [
        User.Roles.TEAM_LEADER,
        User.Roles.QUALITY_CHECKER,
      ],
    }

    if (!teamsType[teamType].includes(memberRole)) {
      throw new Error(
        `Member role '${memberRole}' is not allowed for team type ${teamType}.`,
      )
    }
  }

  async function emitEventWhenCheckerTeamIsCreated(team) {
    const users = await Promise.each(team.users, async (u) => {
      const user = await User.findOneBy({
        queryObject: { id: u.id },
      })

      return {
        ...u,
        created: user.created,
        updated: user.updated,
      }
    })

    const mappedTeam = teamMapper.domainToCheckerTeamEventModel({
      ...team,
      users,
    })

    await applicationEventBus.publishMessage({
      event: 'CheckerTeamCreated',
      data: mappedTeam,
    })
  }
}

const authsomePolicies = ['authenticatedUser', 'isAdmin']

module.exports = {
  initialize,
  authsomePolicies,
}
