const createTeamUseCase = require('./createTeam')
const editTeamUseCase = require('./editTeam')
const getAvailableJournalsUseCase = require('./getAvailableJournals')
const assignManuscript = require('component-screening-process/server/src/use-cases/assignManuscript')

module.exports = {
  ...assignManuscript,
  createTeamUseCase,
  editTeamUseCase,
  getAvailableJournalsUseCase,
}
