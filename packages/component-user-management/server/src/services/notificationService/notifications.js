const config = require('config')
const { get } = require('lodash')

const {
  getConfirmAccountEmailCopy,
  getUserAssignedToATeam,
} = require('./emailCopy')

const {
  accountsEmail,
  name: publisherName,
  emailData,
} = config.get('publisherConfig')

const baseUrl = config.get('pubsweet-client.baseUrl')

const createUrl = (baseUrl, slug, queryParams = null) =>
  !queryParams
    ? `${baseUrl}${slug}`
    : `${baseUrl}${slug}?${new URLSearchParams(queryParams).toString()}`

const initialize = ({ Email }) => ({
  async notifyUserToConfirmAccount({ user }) {
    if (process.env.KEYCLOAK_SERVER_URL) {
      return
    }
    const confirmAccountSlug = config.get('confirm-account.url')
    const { paragraph, ...bodyProps } = getConfirmAccountEmailCopy({
      publisherName,
      user: {
        name: user.name,
        role: user.role,
      },
    })

    const email = new Email({
      type: 'system',
      templateType: 'notification',
      fromEmail: `${publisherName} Support <${accountsEmail}>`,
      toUser: {
        email: user.email,
        name: user.name,
      },
      content: {
        subject: 'Confirm your account',
        ctaLink: createUrl(baseUrl, confirmAccountSlug, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
          givenNames: get(user, 'givenNames', ''),
          surname: get(user, 'surname', ''),
          email: get(user, 'email', ''),
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: `${publisherName} Screening System`,
        footerText: emailData.footerTextTemplate`${user.email}`,
      },
      bodyProps,
    })

    await email.sendEmail()
  },

  async notifyUserThatHeWasAssignedInATeam({ user, team }) {
    const manuscriptDashboardSlug = config.get('manuscript-dashboard.url')
    const { paragraph, ...bodyProps } = getUserAssignedToATeam({
      user: {
        name: user.name,
        role: user.role,
      },
      team,
    })
    const email = new Email({
      type: 'system',
      templateType: 'notification',
      fromEmail: `${publisherName} Support <${accountsEmail}>`,
      toUser: {
        email: user.email,
        name: user.name,
      },
      content: {
        subject: `You have been assigned to a ${team.type} team`,
        ctaLink: createUrl(baseUrl, manuscriptDashboardSlug),
        ctaText: 'GO TO DASHBOARD',
        paragraph,
        signatureName: `${publisherName} Screening System`,
        footerText: emailData.footerTextTemplate`${user.email}`,
      },
      bodyProps,
    })

    await email.sendEmail()
  },
})

module.exports = { initialize }
