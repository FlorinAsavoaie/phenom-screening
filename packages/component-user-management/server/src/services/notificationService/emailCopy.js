const getConfirmAccountEmailCopy = ({ publisherName, user }) => {
  const hasIntro = false
  const hasLink = true
  const hasSignature = true

  const paragraph = `Dear ${user.name}, <br/><br/>
  You have been invited to join ${publisherName} as a ${user.role}.
  Please confirm your account and set your account details by clicking the link below.`

  return { paragraph, hasIntro, hasLink, hasSignature }
}
const getUserAssignedToATeam = ({ user, team }) => {
  const hasIntro = false
  const hasLink = true
  const hasSignature = true

  const paragraph = `Dear ${user.name}, <br/><br/>
  You have been assigned the role of ${user.role} to team ${team.name}. </br>
  Go to the dashboard for more details.`

  return { paragraph, hasIntro, hasLink, hasSignature }
}

module.exports = {
  getConfirmAccountEmailCopy,
  getUserAssignedToATeam,
}
