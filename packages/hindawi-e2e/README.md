In order to run the tests do the following:

    1. Locally:
        - Make sure you have a 'cypress.env.json' file in your 'hindawi-e2e' folder, containing all the needed environment variables. Check the example mentioned bellow or copy the updated one from GitLab (found here: Settings -> CI/CD -> Variables -> CYPRESS_CONFIG).

       Pipeline:
        - If you add changes to the cypress.json file, you should also change the environment variables from the GitLab CI/CD configuration so you can properly run the tests.

    2. You have two options for running the tests:
            1. From the Cypress GUI:
                - from the 'hindawi-e2e' folder run `yarn cypress open`
            2. Headless in the terminal:
                - from the 'hindawi-e2e folder run `yarn cypress run`

### cypress.json example

{
"root": "https://sso.review.hindawi.com",
"realm": "",
"client_id": "",
"redirect_uri": "https://screening.automation.phenom.pub",
"graphqlUrl": "https://screening.automation.phenom.pub/graphql",
"tokenUrl": "",
"email": "auto.screening+",
"adminEmail": "auto.screening+admin@hindawi.com",
"password": "",
"passwordHash": "",
"adminPassword": "",
"dbConfigAuto": {
"user": "",
"host": "",
"database": "",
"password": "",
"port":
},
"messageQueueServiceAuto": {
"AWS_SNS_SQS_ACCESS_KEY": "",
"AWS_REGION": "",
"AWS_SNS_SQS_SECRET_KEY": "",
"AWS_SNS_TOPIC": "",
"AWS_SQS_QUEUE_NAME": "",
"EVENT_NAMESPACE": "",
"PUBLISHER_NAME": ""
},
"elasticSearch": {
"node": "",
"index": "",
"username": "",
"password": ""
}
}

