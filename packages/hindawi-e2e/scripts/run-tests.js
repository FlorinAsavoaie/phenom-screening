const cypress = require('cypress')
const { spawn } = require('child_process')
const reporter = require('cucumber-html-reporter')
const fs = require('fs')

const dir = 'cypress/html-report'

if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir)
}

cypress
  .run({
    env: {
      TAGS: 'not @ignore',
    },
  })
  .then((result) => {
    const options = {
      theme: 'hierarchy',
      jsonDir: 'cypress/cucumber-json/',
      output: 'cypress/html-report/cucumber_report.html',
      screenshotsDirectory: 'cypress/screenshots/',
      reportSuiteAsScenarios: true,
      scenarioTimestamp: true,
      metadata: {
        App: 'Screening',
        'Test Environment': 'Automation',
      },
    }

    reporter.generate(options)

    return result
  })
  .then((result) => {
    const cpHtmlReport = spawn('cp', [
      '-r',
      'cypress/html-report',
      process.env.CI_PROJECT_DIR,
    ])
    cpHtmlReport.on('close', () => {
      if (result.failures) {
        console.error('Could not execute tests')
        console.error(result.message)
        process.exit(1)
      }

      // print test results and exit
      // with the number of failed tests as exit code
      process.exit(result.totalFailed ? 1 : 0)
    })
  })
  .catch((err) => {
    console.error(err.message)
    process.exit(1)
  })
