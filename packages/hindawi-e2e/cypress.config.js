const { defineConfig } = require('cypress')
const webpack = require('@cypress/webpack-preprocessor')
const {
  addCucumberPreprocessorPlugin,
} = require('@badeball/cypress-cucumber-preprocessor')
const { createQueueService } = require('@hindawi/queue-service')
const Chance = require('chance')
const configES = require('config')

const chance = new Chance()
module.exports = defineConfig({
  defaultCommandTimeout: 10000,
  requestTimeout: 10000,
  video: false,
  projectId: 'n5564p',
  viewportWidth: 1600,
  viewportHeight: 1000,
  numTestsKeptInMemory: 25,
  watchForFileChanges: false,
  waitForAnimations: true,
  retries: 2,
  trashAssetsBeforeRuns: true,
  experimentalInteractiveRunEvents: true,
  e2e: {
    baseUrl: 'https://screening.automation.phenom.pub',
    specPattern: '**/**/*.{feature,features}',
    excludeSpecPattern: '*.js',
    setupNodeEvents,
  },
})

async function setupNodeEvents(on, config) {
  const { elasticSearch, dbConfigAuto, baseEmail, messageQueueServiceAuto } =
    config.env
  await addCucumberPreprocessorPlugin(on, config)
  on('file:preprocessor', preprocessTestFiles(config))

  const esConfig = {
    wos: {
      elasticsearch: elasticSearch,
    },
  }

  Object.assign(configES, esConfig)

  // This is a Cypress event that triggers the cleaning of the DB without
  // deleting the needed users for the tests.
  on('before:run', async () => {
    const { Client } = require('pg')
    const autoClient = new Client(dbConfigAuto)
    await autoClient.connect()
    await autoClient.query(`
    TRUNCATE TABLE "public"."author" CASCADE;
    TRUNCATE TABLE "public"."team_manuscript" CASCADE;
    TRUNCATE TABLE "public"."user_manuscript" CASCADE;
    TRUNCATE TABLE "public"."manuscript_peer_review_checks" CASCADE;
    TRUNCATE TABLE "public"."manuscript" CASCADE;
    TRUNCATE TABLE "public"."member" CASCADE;
    TRUNCATE TABLE "public"."review" CASCADE;
    TRUNCATE TABLE "public"."possible_author_profile" CASCADE;
    TRUNCATE TABLE "public"."audit_log" CASCADE;
    TRUNCATE TABLE "public"."file" CASCADE;
    TRUNCATE TABLE "public"."manuscript_checks" CASCADE;
    TRUNCATE TABLE "public"."note" CASCADE;
    TRUNCATE TABLE "public"."manuscript_validations" CASCADE;
    TRUNCATE TABLE "public"."manuscript_material_checks" CASCADE;
    UPDATE team_user SET assignation_date = NULL;
    `)
    await autoClient.end()
  })
  on('task', {
    async indexWosAuthorsInES() {
      const wosDataRepository =
        require('@hindawi/lib-wos-data-store').wosDataRepository.initialize({
          configService: configES,
        })
      const manuscriptWosId = `WOS:${chance.string({
        length: 15,
        pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
      })}`
      const wosAuthors = [
        {
          displayName: 'Author Number 1',
          fullName: 'Author Number 1',
          wosStandard: 'A1',
          firstName: 'Author',
          lastName: 'Number 1',
          email: `${baseEmail}+${chance.string({
            length: 8,
            casing: 'lower',
            alpha: true,
            numeric: true,
          })}@hindawi.com`,
          affiliation: 'Testing',
          orcId: '',
          researcherId: '',
          manuscriptDoi: '',
          manuscriptWosId,
        },
      ]
      const abstract = 'acesta este abstractul'
      await wosDataRepository.indexWoSAuthors({
        wosAuthors,
        abstract,
        manuscriptWosId,
      })
      return wosAuthors[0].email
    },
    async dropDatabase() {
      const { Client } = require('pg')
      const autoClient = new Client(dbConfigAuto)
      await autoClient.connect()
      await autoClient.query('DROP SCHEMA public CASCADE;')
      await autoClient.query('CREATE SCHEMA public;')
      await autoClient.end()
    },
    // This method will clean the entire DB.
    async truncateEntireDatabase() {
      const { Client } = require('pg')
      const autoClient = new Client(dbConfigAuto)
      await autoClient.connect()
      await autoClient.query(`TRUNCATE "public"."author" CASCADE;
      TRUNCATE "public"."manuscript_authors" CASCADE;
      TRUNCATE "public"."audit_log" CASCADE;
      TRUNCATE "public"."file" CASCADE;
      TRUNCATE "public"."journal" CASCADE;
      TRUNCATE "public"."manuscript_checks" CASCADE;
      TRUNCATE "public"."manuscript_material_checks" CASCADE;
      TRUNCATE "public"."journal_section" CASCADE;
      TRUNCATE "public"."manuscript_peer_review_checks" CASCADE;
      TRUNCATE "public"."manuscript_validations" CASCADE;
      TRUNCATE "public"."manuscript" CASCADE;
      TRUNCATE "public"."team_journal" CASCADE;
      TRUNCATE "public"."user_manuscript" CASCADE;
      TRUNCATE "public"."team" CASCADE;
      TRUNCATE "public"."possible_author_profile" CASCADE;
      TRUNCATE "public"."team_manuscript" CASCADE;
      TRUNCATE "public"."team_user" CASCADE;
      TRUNCATE "public"."review" CASCADE;
      TRUNCATE "public"."material_check_config" CASCADE;
      TRUNCATE "public"."peer_review_check_config" CASCADE;
      TRUNCATE "public"."special_issue" CASCADE;
      TRUNCATE "public"."member" CASCADE;
      TRUNCATE "public"."note" CASCADE;
      TRUNCATE "public"."user" CASCADE;
`)
      await autoClient.end()
      return true
    },
    // If you clean the entire DB this method will add the needed users for the tests.
    // In order to add more users, create the accounts and activate them in SSO
    // first, then add them to this query. Users with accounts in SSO, but not
    // linked in our DB will not be active.
    async addUsersInDatabase() {
      const { Client } = require('pg')
      const autoClient = new Client(dbConfigAuto)
      await autoClient.connect()
      await autoClient.query(
        `INSERT INTO "public"."team" ("id", "created", "updated", "name", "type") VALUES
  ('bc19a851-ef6d-45ba-bbe4-ff765b241a00', '2020-06-15 08:18:43.971', '2020-06-15 08:18:43.971', 'Admin', 'screening');
  INSERT INTO "public"."user" ("id", "created", "updated", "email", "password_hash", "given_names", "surname", "is_confirmed", "confirmation_token") VALUES
  ('477a7825-2ce9-4032-a754-df6d0be6e0e7', '2020-06-15 08:18:43.273', '2020-06-15 08:18:43.273', 'auto.screening+admin@hindawi.com', '$2b$12$.JkV7eyD0jEDcQnuigh0E.SpkGXcssf6llda.oEN9WoXsTW/cDs0W', 'Sergiu', 'Admin', 't', NULL);
  INSERT INTO "public"."team_user" ("id", "created", "updated", "team_id", "user_id", "role", "assignation_date") VALUES
  ('29a616b2-7eed-48c6-b4db-796606f17e5d', '2020-06-15 08:18:43.930866', '2020-06-15 08:18:43.930866', 'bc19a851-ef6d-45ba-bbe4-ff765b241a00', '477a7825-2ce9-4032-a754-df6d0be6e0e7', 'admin', NULL);
  INSERT INTO "public"."user" ("id", "created", "updated", "email", "password_hash", "given_names", "surname", "is_confirmed", "confirmation_token") VALUES
  ('cb0a0ec5-82ba-4fed-a92f-7905cdd2c012', '2020-11-25 12:22:03.637', '2020-11-25 12:22:03.637', 'auto.screening+tl@hindawi.com', NULL, 'Stefan', 'Leaderan', 't', NULL);
  INSERT INTO "public"."user" ("id", "created", "updated", "email", "password_hash", "given_names", "surname", "is_confirmed", "confirmation_token") VALUES
  ('109eb77f-e024-45b7-8e3f-1876670f9e35', '2020-11-25 12:22:07.116', '2020-11-25 12:22:07.116', 'auto.screening+checker1@hindawi.com', NULL, 'Vasile', 'Checkeras', 't', NULL);
  INSERT INTO "public"."user" ("id", "created", "updated", "email", "password_hash", "given_names", "surname", "is_confirmed", "confirmation_token") VALUES
  ('35c16a61-5ecb-4f29-a17a-326a9a8cf406', '2020-11-25 12:22:10.434', '2020-11-25 12:22:10.434', 'auto.screening+checker2@hindawi.com', NULL, 'Mihai', 'Checkerovici', 't', NULL);
  INSERT INTO "public"."user" ("id", "created", "updated", "email", "password_hash", "given_names", "surname", "is_confirmed", "confirmation_token") VALUES
  ('bbeba78e-95a0-4814-a974-9ec9b705114c', '2020-11-25 12:22:13.018', '2020-11-25 12:22:13.018', 'auto.screening+checker3@hindawi.com', NULL, 'Dan', 'Checkerescu', 't', NULL);`,
      )
      await autoClient.end()
      return true
    },
    async selectFromDB({ sqlQuery, value }) {
      const { Client } = require('pg')
      const autoClient = new Client(dbConfigAuto)
      await autoClient.connect()
      const queryDB = {
        rowMode: 'array',
        name: 'queryAutoDB',
        text: sqlQuery,
        values: [value],
      }
      const queryAutoDB = await autoClient
        .query(queryDB)
        .then((res) => res.rows[0][0])
      await autoClient.end()
      return queryAutoDB
    },
    async triggerEvent({ eventName, data }) {
      return createQueueService({
        region: messageQueueServiceAuto.AWS_REGION,
        accessKeyId: messageQueueServiceAuto.AWS_SNS_SQS_ACCESS_KEY,
        secretAccessKey: messageQueueServiceAuto.AWS_SNS_SQS_SECRET_KEY,
        topicName: messageQueueServiceAuto.AWS_SNS_TOPIC,
        queueName: messageQueueServiceAuto.AWS_SQS_QUEUE_NAME,
        eventNamespace: messageQueueServiceAuto.EVENT_NAMESPACE,
        publisherName: messageQueueServiceAuto.PUBLISHER_NAME,
      }).then((queueService) =>
        queueService.publishMessage({
          event: eventName,
          data,
        }),
      )
    },
  })
  return config
}

function preprocessTestFiles(config) {
  return webpack({
    webpackOptions: {
      resolve: {
        extensions: ['.ts', '.js'],
        fallback: { path: require.resolve('path-browserify') },
      },
      module: {
        rules: [
          {
            test: /\.jsx?$/,
            exclude: [/node_modules/],
            use: [
              {
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/preset-env'],
                },
              },
            ],
          },
          {
            test: /\.feature$/,
            use: [
              {
                loader: '@badeball/cypress-cucumber-preprocessor/webpack',
                options: config,
              },
            ],
          },
        ],
      },
    },
  })
}
