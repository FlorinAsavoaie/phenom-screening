const Chance = require('chance')

const chance = new Chance()

export const generateJournalData = ({ id, name }) => ({
  id,
  created: new Date().toISOString(),
  updated: new Date().toISOString(),
  name,
  publisherName: null,
  code: 'JE',
  email: chance.email(),
  issn: null,
  apc: 1000,
  isActive: true,
  activationDate: new Date().toISOString(),
  sections: [],
  specialIssues: [],
  peerReviewModel: {
    name: 'Chief Minus',
  },
  articleTypes: [
    {
      name: 'Review Article',
    },
    {
      name: 'Research Article',
    },
  ],
  editors: [],
})
