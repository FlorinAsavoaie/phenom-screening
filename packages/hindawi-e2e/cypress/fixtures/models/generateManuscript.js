const Chance = require('chance')

const chance = new Chance()

export const generateScreeningManuscript = ({
  title = `Automated test [${chance.string({
    length: 6,
  })}]`,
  fileName = 'Blockchain for Real Estate Industry.pdf',
  id,
  customId = chance.string({ length: 7, numeric: true }),
  journalId = Cypress.env('journalIdOneChecker'),
  authors = [`${Cypress.env('baseEmail')}+author@hindawi.com`],
}) => {
  const mappedAuthors = authors.map((email, index) => ({
    id: chance.guid({ version: 4 }),
    created: new Date().toISOString(),
    updated: new Date().toISOString(),
    userId: chance.guid({ version: 4 }),
    status: 'pending',
    position: 0,
    isSubmitting: true,
    isCorresponding: true,
    aff: 'Testing',
    email,
    title: 'mr',
    country: 'AM',
    surname: `Number ${index + 1}`,
    affRorId: null,
    givenNames: 'Author',
    affiliation: {
      name: 'Testing 2',
      ror: {
        id: null,
      },
    },
    orcidId: '',
    assignedDate: new Date().toISOString(),
  }))

  /**
   * Mapping the user friendly submission file name to the S3 provider key
   * Used for tests where we need to check word count and language detection
   */
  const manuscriptFileNameToS3Key = new Map([
    [
      `Blockchain for Real Estate Industry.pdf`,
      `7cc680bd-6797-409d-b90e-4634df3cf973/ff9120ca-68e4-422c-bcf7-45ed4f70bc64`,
    ],
    [
      `German Research Sample 35% english.pdf`,
      `7cc680bd-6797-409d-b90e-4634df3cf973/German-Research-Sample-35-english.pdf`,
    ],
    [
      `Lorem Ipsum No English.pdf`,
      `7cc680bd-6797-409d-b90e-4634df3cf973/Lorem-Ipsum-No-English.pdf`,
    ],
    [
      `English Spanish Dictionary 50% English.pdf`,
      `7cc680bd-6797-409d-b90e-4634df3cf973/English-Spanish-Dictionary-50-English.pdf`,
    ],
    [
      `German Book Review Article.pdf`,
      `7cc680bd-6797-409d-b90e-4634df3cf973/German-Book-Review-Article.pdf`,
    ],
  ])
  return {
    submissionId: chance.guid({ version: 4 }),
    manuscripts: [
      {
        id,
        title,
        files: [
          {
            id: '0129a7d5-a44b-4b3d-ac3e-57b003906114',
            created: new Date().toISOString(),
            updated: new Date().toISOString(),
            type: 'manuscript',
            label: null,
            fileName,
            url: null,
            mimeType: 'application/pdf',
            size: 515351,
            originalName: fileName,
            position: null,
            providerKey: manuscriptFileNameToS3Key.get(fileName),
          },
        ],
        version: '1',
        customId,
        abstract: 'The abstract should be here.',
        journalId,
        sectionId: null,
        acceptedDate: null,
        preprintValue: null,
        specialIssueId: null,
        qualityChecksSubmittedDate: null,
        dataAvailability: '',
        fundingStatement: 'test',
        conflictOfInterest: '',
        linkedSubmissionCustomId: null,
        submissionCreatedDate: new Date(),
        editors: [
          {
            id: 'f99fd926-68b4-4643-8803-8267c7b0e759',
            userId: '656fea14-042c-4055-82a1-e60a7218b1dd',
            status: 'active',
            isCorresponding: null,
            aff: 'Testing',
            email: `${Cypress.env('baseEmail')}+ea@hindawi.com`,
            title: 'mr',
            country: 'RO',
            surname: 'Editorial Assistant',
            affRorId: null,
            givenNames: 'Sergiu',
            role: {
              type: 'editorialAssistant',
              label: 'Editorial Assistant',
            },
            orcidId: '',
            invitedDate: '',
            declinedDate: '',
            acceptedDate: '',
            expiredDate: '',
            assignedDate: new Date().toISOString(),
            removedDate: '',
            declineReason: null,
          },
        ],
        submittingStaffMembers: [],
        authors: mappedAuthors,
        reviewers: [],
        reviews: [],
        articleType: {
          name: 'Review Article',
        },
        sourceJournal: {
          name: null,
          pissn: null,
          eissn: null,
          publisher: null,
        },
      },
    ],
  }
}
