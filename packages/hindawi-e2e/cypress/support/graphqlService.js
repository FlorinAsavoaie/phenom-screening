function graphqlService({ query, token, variables }) {
  return cy.request({
    method: 'POST',
    url: Cypress.env('graphqlUrl'),
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: { query, variables },
  })
}

module.exports = { graphqlService }
