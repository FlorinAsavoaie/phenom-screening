export const goTo = {
  manuscript: (id) => cy.visit(`/manuscripts/${id}`),
  settings: () => cy.visit(`/settings`),
  userManagement: () => cy.visit(`/users`),
}
