import { generateJournalData } from '../../../fixtures/models/generateJournal'

Cypress.Commands.add('journalAddedEvent', ({ id, name }) => {
  cy.task('triggerEvent', {
    eventName: 'JournalAdded',
    data: generateJournalData({ id, name }),
  })
})
