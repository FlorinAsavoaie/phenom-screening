import { generateScreeningManuscript } from '../../../fixtures/models/generateManuscript'
import { generateQcManuscript } from '../../../fixtures/models/generateQcManuscript'

const Chance = require('chance')

const chance = new Chance()

Cypress.Commands.add(
  'submitManuscript',
  ({ type, title, fileName, authors, journalId }) => {
    const id = chance.guid({ version: 4 })
    if (type === 'screening') {
      cy.task('triggerEvent', {
        eventName: 'TriggeredScreening',
        data: generateScreeningManuscript({
          title,
          fileName,
          id,
          authors,
          journalId,
        }),
      })
    } else if (type === 'qualityChecking') {
      cy.task('triggerEvent', {
        eventName: 'TriggeredQualityChecks',
        data: generateQcManuscript({
          title,
          id,
          authors,
          journalId,
        }),
      })
    }

    cy.wait(4200)

    cy.getManuscriptId(id).then((manuscriptId) =>
      cy.wrap(manuscriptId).as('manuscriptId'),
    )
  },
)
