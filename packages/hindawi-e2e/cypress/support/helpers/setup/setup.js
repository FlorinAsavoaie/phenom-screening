import { Team } from '../../../integration/userManagement/graphql/team'

Cypress.Commands.add('cleanDbAndAddUsers', () => {
  let adminToken
  cy.task('truncateEntireDatabase').should('equal', true)
  cy.task('addUsersInDatabase').should('equal', true)
  cy.log('Database was cleaned.')

  cy.journalAddedEvent({
    id: Cypress.env('journalIdOneChecker'),
    name: '[Test] Journal with 1 checker',
  })
  cy.journalAddedEvent({
    id: Cypress.env('journalIdMultipleCheckers'),
    name: '[Test] Journal with 3 checkers',
  })
  cy.journalAddedEvent({
    id: Cypress.env('journalId1'),
    name: '[Test] Journal for Edit Team 1',
  })
  cy.journalAddedEvent({
    id: Cypress.env('journalId2'),
    name: '[Test] Journal for Edit Team 2',
  })

  cy.log('Journals were added.')
  cy.wait(1500)

  cy.loginKeycloak({
    username: Cypress.env('adminEmail'),
    password: Cypress.env('password'),
  }).then(() => {
    cy.getToken()
      .then((token) => {
        adminToken = token
      })
      .then(() => {
        Team.create({
          adminToken,
          teamName: '[Screening] Team with one checker',
          teamType: 'screening',
          numberOfCheckers: 1,
          journalId: Cypress.env('journalIdOneChecker'),
        })
        Team.create({
          adminToken,
          teamName: '[Screening] Team with multiple checkers',
          teamType: 'screening',
          numberOfCheckers: 3,
          journalId: Cypress.env('journalIdMultipleCheckers'),
        })
        Team.create({
          adminToken,
          teamName: '[QC] Team with one checker',
          teamType: 'qualityChecking',
          numberOfCheckers: 1,
          journalId: Cypress.env('journalIdOneChecker'),
        })
        Team.create({
          adminToken,
          teamName: '[QC] Team with multiple checkers',
          teamType: 'qualityChecking',
          numberOfCheckers: 3,
          journalId: Cypress.env('journalIdMultipleCheckers'),
        })
      })
  })
  cy.log('Teams were created.')
  cy.logoutKeycloak()
})
