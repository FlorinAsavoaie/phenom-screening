Cypress.Commands.add('getManuscriptId', (phenomId) => {
  cy.task('selectFromDB', {
    sqlQuery: `SELECT id from manuscript WHERE phenom_id= $1`,
    value: `${phenomId}`,
  }).then((res) => res)
})
