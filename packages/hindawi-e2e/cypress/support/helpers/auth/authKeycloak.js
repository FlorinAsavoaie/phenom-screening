import 'cypress-keycloak'

Cypress.Commands.add('loginKeycloak', ({ username, password }) => {
  const response = cy.login({
    root: Cypress.env('root'),
    realm: Cypress.env('realm'),
    username,
    password,
    client_id: Cypress.env('client_id'),
    redirect_uri: Cypress.env('redirect_uri'),
  })
  return response
})

Cypress.Commands.add('logoutKeycloak', () => {
  const response = cy.logout({
    root: Cypress.env('root'),
    realm: Cypress.env('realm'),
    redirect_uri: Cypress.env('redirect_uri'),
  })
  return response
})
