Cypress.Commands.add('loginViaUI', ({ username, password }) => {
  cy.get('#username').type(username)
  cy.get('#password').type(password)
  cy.get('#kc-login').click()
})
