// You need to disable the user consent in keycloak in order for this to work
Cypress.Commands.add('getToken', () =>
  cy
    .request({
      method: 'POST',
      url: Cypress.env('tokenUrl'),
      body: {
        client_id: Cypress.env('client_id'),
        grant_type: 'password',
        scope: 'openid',
        username: Cypress.env('adminEmail'),
        password: Cypress.env('password'),
      },
      form: true,
    })
    .then((response) => response.body.access_token),
)
