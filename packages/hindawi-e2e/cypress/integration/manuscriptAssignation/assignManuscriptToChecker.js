import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { DashboardPage } from '../common/pageObjects/dashboardPage'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { goTo } from '../../support/helpers/accessByPath'

When(/^I assign the manuscript to (.*)$/, (checkerName) => {
  cy.visit('/')

  cy.get('@submissionParam').then((submission) => {
    DashboardPage.searchForManuscript(submission.title)
  })

  DashboardPage.assignChecker(checkerName)

  cy.logoutKeycloak()
})

Then(
  /^(.*) has access to the manuscript and is able to make a decision on it$/,
  (checkerEmail) => {
    cy.loginKeycloak({
      username: checkerEmail,
      password: Cypress.env('password'),
    })

    cy.get('@manuscriptId').then((id) => {
      goTo.manuscript(id)
    })

    ManuscriptPage.makeDecision({ decisionType: 'refuseToConsider' })

    DashboardPage.applyFilter('archived')

    cy.get('@submissionParam').then((submission) => {
      DashboardPage.searchForManuscript(submission.title)
    })

    cy.findByText('RTC').should('exist')
  },
)
