import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { SettingsPage } from './pageObjects/settingsPage'

const Chance = require('chance')

const chance = new Chance()

const randomStringKeyword = chance.string({ length: 9 })
const randomStringReason = chance.string({ length: 20 })

When(/^I add a keyword to the suspicious keywords list$/, () => {
  SettingsPage.accessSuspiciousKeywords()

  SettingsPage.addKeyword(randomStringKeyword, randomStringReason)
})

Then(/^the keyword can be found in the list of suspicious keywords$/, () => {
  SettingsPage.search(randomStringKeyword)

  SettingsPage.validateSuspiciousKeyword(
    randomStringKeyword,
    randomStringReason,
  )
})
