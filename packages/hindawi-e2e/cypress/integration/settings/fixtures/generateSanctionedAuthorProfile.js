export function generateSanctionedAuthorProfile({
  firstName,
  lastName,
  email,
  sanctionList,
}) {
  let author
  switch (sanctionList) {
    case 'badDebt':
      author = {
        givenNames: firstName,
        surname: lastName,
        email,
        reasons: ['6fbadb08-795e-492f-ad19-40beefa7be12'],
        comments: 'Some comment.',
        toDate: null,
        isOnBadDebtList: true,
        isOnBlackList: false,
        isOnWatchList: false,
      }
      break
    case 'blackList':
      author = {
        givenNames: firstName,
        surname: lastName,
        email,
        reasons: ['6fbadb08-795e-492f-ad19-40beefa7be12'],
        comments: 'Some comment.',
        toDate: null,
        isOnBadDebtList: false,
        isOnBlackList: true,
        isOnWatchList: false,
      }
      break
    case 'watchList':
      author = {
        givenNames: firstName,
        surname: lastName,
        email,
        reasons: ['6fbadb08-795e-492f-ad19-40beefa7be12'],
        comments: 'Some comment.',
        toDate: null,
        isOnBadDebtList: false,
        isOnBlackList: false,
        isOnWatchList: true,
      }
      break
    default:
      author = {
        givenNames: firstName,
        surname: lastName,
        email,
        reasons: ['6fbadb08-795e-492f-ad19-40beefa7be12'],
        comments: 'Some comment.',
        toDate: null,
        isOnBadDebtList: true,
        isOnBlackList: false,
        isOnWatchList: false,
      }
  }
  return author
}
