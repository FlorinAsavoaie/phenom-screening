import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { SettingsPage } from './pageObjects/settingsPage'
import { goTo } from '../../support/helpers/accessByPath'
import { Settings } from './graphql/settings'

const Chance = require('chance')

const chance = new Chance()

const email = chance.email()

Given(/^a sanctioned author for deleting$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('adminEmail'),
    password: Cypress.env('password'),
  })
    .then(() => {
      cy.getToken()
    })
    .then((token) => {
      Settings.addSanctionedAuthor({
        firstName: chance.first(),
        lastName: chance.last(),
        email,
        adminToken: token,
      })
    })
})

When(/^I delete the author from the Sanction List$/, () => {
  goTo.settings()

  SettingsPage.search(email)

  SettingsPage.deleteEntry()
})

Then(/^I can't find the author in the the Sanction List anymore$/, () => {
  SettingsPage.search(email)

  cy.findByText('No results found.').should('exist')
})
