export class SettingsPage {
  static accessSanctionList() {
    cy.get('nav div').within(() => {
      cy.findByText('Sanction List').click()
    })
  }

  static accessSuspiciousKeywords() {
    cy.get('nav div').within(() => {
      cy.findByText('Suspicious Keywords').click()
    })
  }

  static addAuthorToSanctionList({ firstName, lastName, email, comments }) {
    cy.findByText('Add Author').click()

    cy.findByTestId('givenNames').type(firstName)
    cy.findByTestId('surname').type(lastName)
    cy.findByTestId('email').type(email)

    cy.findByTestId('isOnBadDebtList').click()
    cy.findByTestId('isOnBlackList').click()
    cy.findByTestId('isOnWatchList').click()

    cy.findByTestId('comments').type(comments)

    cy.findByTestId('reasons-dropdown').click()
    cy.findByText('Plagiarism').click()

    cy.findByTestId('modal-create').click()
  }

  static search(word) {
    cy.findByTestId('search').type(word).type('{enter}')
  }

  static validateSanctionedAuthor({
    firstName,
    lastName,
    email,
    comments,
    reasons,
  }) {
    cy.findByTestId('table-row').within(() => {
      cy.findByText(firstName).should('exist')
      cy.findByText(lastName).should('exist')
      cy.findByText(email).should('exist')
      cy.findByText(comments).should('exist')
      cy.findByText(reasons).should('exist')
    })
  }

  static addKeyword(keyword, reason) {
    cy.findByText('Add keyword').click()

    cy.findByTestId('keyword').type(keyword)

    cy.get('[name="note"]').type(reason)

    cy.findByTestId('modal-create').click()
  }

  static validateSuspiciousKeyword(keyword, reason) {
    cy.findByTestId('table-row').within(() => {
      cy.findByText(keyword).should('exist')
      cy.findByText(reason).should('exist')
    })
  }

  static editEntry() {
    cy.findByTestId('table-row').realHover('mouse')
    cy.findByTestId('action-dropdown').click()
    cy.findByTestId('edit-option').click()
  }

  static deleteEntry() {
    cy.findByTestId('table-row').realHover('mouse')
    cy.findByTestId('action-dropdown').click()
    cy.findByTestId('delete-option').click()
    cy.findByTestId('modal-confirm').click()
  }

  static modal = {
    editKeywordReason: (note) => {
      cy.get('[name="note"]').clear().type(note)
      cy.findByTestId('modal-create').click()
    },
    pickSanctionTypes: (types) => {
      types.forEach((type) => {
        switch (type) {
          case 'badDebt':
            cy.findByTestId('isOnBadDebtList').click()
            break
          case 'blackList':
            cy.findByTestId('isOnBlackList').click()
            break
          case 'watchList':
            cy.findByTestId('isOnWatchList').click()
            break
          default:
            cy.log('Provided type not found.')
        }
      })
    },
    pickSanctionReasons: (reasonIds) => {
      cy.findByTestId('reasons-dropdown').click()
      reasonIds.forEach((reasonId) => {
        cy.findByTestId(reasonId).click()
      })
    },
    editComments: (text) => {
      cy.findByTestId('comments').clear()
      cy.findByTestId('comments').type(text)
    },
    removeEndDate: () => cy.findByTestId('toDate').click(),
    saveEdit: () => cy.findByTestId('modal-create').click(),
  }
}
