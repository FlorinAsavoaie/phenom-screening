import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { SettingsPage } from './pageObjects/settingsPage'
import { reasons } from './fixtures/sanctionReasons.json'
import { goTo } from '../../support/helpers/accessByPath'
import { Settings } from './graphql/settings'

const Chance = require('chance')

const chance = new Chance()

const firstName = chance.first()
const lastName = chance.last()
const email = chance.email()
const comments = 'This comment was edited.'

Given(/^a sanctioned author for editing$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('adminEmail'),
    password: Cypress.env('password'),
  })
    .then(() => {
      cy.getToken()
    })
    .then((token) => {
      Settings.addSanctionedAuthor({
        firstName,
        lastName,
        email,
        adminToken: token,
      })
    })
})

When(/^I edit the sanction details$/, () => {
  goTo.settings()

  SettingsPage.search(email)

  SettingsPage.validateSanctionedAuthor({
    firstName,
    lastName,
    email,
    comments: 'Some comment.',
    reasons: [reasons.other.name],
  })

  cy.findByText('No end date').should('exist')

  SettingsPage.editEntry()

  SettingsPage.modal.pickSanctionTypes(['blackList', 'watchList'])
  SettingsPage.modal.removeEndDate()
  SettingsPage.modal.editComments(comments)
  SettingsPage.modal.pickSanctionReasons([
    reasons.peerReviewManipulation.id,
    reasons.potentialPaperMill.id,
  ])

  SettingsPage.modal.saveEdit()
})

Then(/^I see the changes reflected in the Sanction List$/, () => {
  SettingsPage.search(email)

  SettingsPage.validateSanctionedAuthor({
    firstName,
    lastName,
    email,
    comments,
    reasons: [
      reasons.other.name,
      reasons.peerReviewManipulation.name,
      reasons.potentialPaperMill.name,
    ].join(', '),
  })

  cy.findByText('No end date').should('not.exist')
})
