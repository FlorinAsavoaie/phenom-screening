import * as path from 'path'
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { SettingsPage } from './pageObjects/settingsPage'
import { goTo } from '../../support/helpers/accessByPath'
import { Settings } from './graphql/settings'

let csvUrl

Given(/^a list of suspicious keywords$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('adminEmail'),
    password: Cypress.env('password'),
  })
    .then(() => {
      cy.getToken()
    })
    .then((token) => {
      Settings.exportSuspiciousKeywordsList({
        adminToken: token,
      })
    })
    .then((response) => (csvUrl = response))

  goTo.settings()

  SettingsPage.accessSuspiciousKeywords()
})

When(/^I export the list$/, () => {
  cy.findByText('Export Keywords').click()

  // This is a temporary solution for a bug in Cypress where
  // the page load timeouts when downloading the exported csv.
  cy.window()
    .document()
    .then((doc) => {
      doc.addEventListener('click', () => {
        setTimeout(() => {
          doc.location.reload()
        }, 8000)
      })

      /* Make sure the file exists */
      cy.intercept(csvUrl, (req) => {
        req.reply((res) => {
          expect(res.statusCode).to.equal(200)
        })
      })
      cy.findByText('EXPORT NOW').click()
    })
})

Then(/^the suspicious keywords list is downloaded on my machine$/, () => {
  const filename = path.join(
    'cypress/downloads/',
    'exported_suspicious_keywords.csv',
  )

  cy.readFile(filename, { timeout: 15000 }).should('have.length.gt', 40000)
})
