export const addSanctionListProfile = `mutation addSanctionListProfile(
    $surname: String
    $givenNames: String
    $isOnBadDebtList: Boolean
    $isOnBlackList: Boolean
    $isOnWatchList: Boolean
    $email: String
    $toDate: DateTime
    $reasons: [ID]
    $comments: String
  ) {
    addSanctionListProfile(
      surname: $surname
      givenNames: $givenNames
      isOnBadDebtList: $isOnBadDebtList
      isOnBlackList: $isOnBlackList
      isOnWatchList: $isOnWatchList
      email: $email
      toDate: $toDate
      reasons: $reasons
      comments: $comments
    )
  }`

export const addSuspiciousKeyword = `mutation addSuspiciousKeyword($keyword: String!, $note: String!) {
    addSuspiciousKeyword(keyword: $keyword, note: $note)
  }`

export const exportSuspiciousKeywords = `
  mutation exportSuspiciousKeywords {
    exportSuspiciousKeywords {
      url
      totalNumber
    }
  }
`
