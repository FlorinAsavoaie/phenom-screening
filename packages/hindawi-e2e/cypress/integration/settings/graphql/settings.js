import { generateSanctionedAuthorProfile } from '../fixtures/generateSanctionedAuthorProfile'
import { graphqlService } from '../../../support/graphqlService'
import {
  addSanctionListProfile,
  addSuspiciousKeyword,
  exportSuspiciousKeywords,
} from './mutations'

export class Settings {
  static addSanctionedAuthor({
    firstName,
    lastName,
    email,
    sanctionList,
    adminToken,
  }) {
    const author = generateSanctionedAuthorProfile({
      firstName,
      lastName,
      email,
      sanctionList,
    })
    graphqlService({
      query: addSanctionListProfile,
      token: adminToken,
      variables: author,
    })
  }

  static addSuspiciousKeyword({ keyword, note, adminToken }) {
    graphqlService({
      query: addSuspiciousKeyword,
      token: adminToken,
      variables: {
        keyword,
        note,
      },
    })
  }

  static exportSuspiciousKeywordsList({ adminToken }) {
    const response = graphqlService({
      query: exportSuspiciousKeywords,
      token: adminToken,
      variables: {},
    }).then((response) => response.body.data.exportSuspiciousKeywords.url)
    return response
  }
}
