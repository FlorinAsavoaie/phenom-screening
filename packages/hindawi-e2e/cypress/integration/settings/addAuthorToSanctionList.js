import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { SettingsPage } from './pageObjects/settingsPage'

const Chance = require('chance')

const chance = new Chance()

const firstName = chance.first()
const lastName = chance.last()
const email = chance.email()
const comments = chance.string({ length: 5 })

When(/^I add an author to the sanction list$/, () => {
  SettingsPage.accessSanctionList()

  SettingsPage.addAuthorToSanctionList({ firstName, lastName, email, comments })
})

Then(/^the author can be found in the list of sanctioned authors$/, () => {
  SettingsPage.search(email)

  SettingsPage.validateSanctionedAuthor({
    firstName,
    lastName,
    email,
    comments,
    reasons: 'Plagiarism',
    endDate: 'No end date',
  })
})
