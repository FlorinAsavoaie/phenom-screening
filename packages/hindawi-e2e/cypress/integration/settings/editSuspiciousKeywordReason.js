import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { SettingsPage } from './pageObjects/settingsPage'
import { goTo } from '../../support/helpers/accessByPath'
import { Settings } from './graphql/settings'

const Chance = require('chance')

const chance = new Chance()

const keyword = chance.string({ length: 8 })
const note = chance.string({ length: 5 })

Given(/^a suspicious keyword for editing$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('adminEmail'),
    password: Cypress.env('password'),
  })
    .then(() => {
      cy.getToken()
    })
    .then((token) => {
      Settings.addSuspiciousKeyword({
        keyword,
        note,
        adminToken: token,
      })
    })
})

When(/^I edit the reason for the keyword$/, () => {
  goTo.settings()

  SettingsPage.accessSuspiciousKeywords()

  SettingsPage.search(keyword)

  SettingsPage.validateSuspiciousKeyword(keyword, note)

  SettingsPage.editEntry()

  SettingsPage.modal.editKeywordReason('New note.')
})

Then(/^I see the changes reflected in the Suspicious Keywords List$/, () => {
  SettingsPage.accessSuspiciousKeywords()

  SettingsPage.search(keyword)

  SettingsPage.validateSuspiciousKeyword(keyword, 'New note.')
})
