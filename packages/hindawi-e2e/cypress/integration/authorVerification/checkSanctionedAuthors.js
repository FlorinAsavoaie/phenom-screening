import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { goTo } from '../../support/helpers/accessByPath'

Given(/^a submission from sanctioned authors$/, () => {
  cy.submitManuscript({
    type: 'screening',
    title: 'Testinng sanctioned authors',
    authors: [
      `${Cypress.env('baseEmail')}+badDebt@hindawi.com`,
      `${Cypress.env('baseEmail')}+blackList@hindawi.com`,
      `${Cypress.env('baseEmail')}+watchList@hindawi.com`,
    ],
  })
})

When(/^I verify the authors$/, () => {
  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  cy.findByText('Author Identity Verification').click()
})

Then(/^the authors are marked with the correct sanctioned tags$/, () => {
  cy.findByText('BAD DEBT').should('exist')
  cy.findByText('BLACKLIST').should('exist')
  cy.findByText('WATCHLIST').should('exist')
})
