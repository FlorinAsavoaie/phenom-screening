import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Author } from './pageObjects/authorIdentityVerificationPage'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { goTo } from '../../support/helpers/accessByPath'

let authorThatCantBeVerified

When(/^an author can't be verified$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('firstCheckerEmail'),
    password: Cypress.env('password'),
  })

  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  cy.get('@submissionParam').then((submission) => {
    authorThatCantBeVerified = submission.authors
    Author.cantVerify(authorThatCantBeVerified)
  })

  cy.findByText(`CAN'T BE VERIFIED`).should('exist')

  ManuscriptPage.makeDecision({ decisionType: 'approve' })
})

Then(/^the author can be verified again on the next submission$/, () => {
  cy.submitManuscript({
    type: 'screening',
    title: 'Testing if the author can be verified',
    authors: authorThatCantBeVerified,
  })

  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  cy.findByText('Author Identity Verification').click()
  cy.findByText('VERIFIED').should('not.exist')
  cy.findByText(`CAN'T BE VERIFIED`).should('not.exist')
})
