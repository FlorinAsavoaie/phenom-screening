export class Author {
  static verifyOutOfTool(author) {
    cy.findByText('Author Identity Verification').click()
    cy.findByText(author).click()
    cy.findByText('Verify Out of Tool').click()
  }

  static verifyFromWos(author) {
    cy.findByText('Author Identity Verification').click()
    cy.findAllByTestId('authors-table-row').within(() => {
      cy.findByText(author).should('exist').realHover()
      cy.findByTestId('verify-author').click({ force: true })
    })
  }

  static cantVerify(author) {
    cy.findByText('Author Identity Verification').click()
    cy.findByText(author).click()
    cy.findByText(`Can't be verified`).click()
  }
}
