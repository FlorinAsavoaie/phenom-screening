import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { Author } from './pageObjects/authorIdentityVerificationPage'
import { goTo } from '../../support/helpers/accessByPath'

let authorForWoSVerification
let manuscriptId

Given(/^a new manuscript submission$/, () => {
  cy.task('indexWosAuthorsInES').then((authorEmail) => {
    authorForWoSVerification = authorEmail
    cy.submitManuscript({
      type: 'screening',
      title: 'Testing author out of tool verification',
      authors: [authorForWoSVerification],
    }).then((id) => {
      manuscriptId = id
    })
  })
})

When(/^I verify the author from the WoS results$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('firstCheckerEmail'),
    password: Cypress.env('password'),
  })

  goTo.manuscript(manuscriptId)

  Author.verifyFromWos(authorForWoSVerification)
  ManuscriptPage.makeDecision({ decisionType: 'approve' })
})

Then(
  /^the verified author is automatically verified on the next submission$/,
  () => {
    cy.submitManuscript({
      type: 'screening',
      title: 'Testing if the author is automatically verified',
      authors: [authorForWoSVerification],
    })

    cy.get('@manuscriptId').then((id) => {
      goTo.manuscript(id)
    })

    cy.findByText('Author Identity Verification').click()

    cy.findByText('VERIFIED').should('exist')
  },
)
