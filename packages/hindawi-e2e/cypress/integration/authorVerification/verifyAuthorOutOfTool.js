import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { Author } from './pageObjects/authorIdentityVerificationPage'
import { goTo } from '../../support/helpers/accessByPath'

let authorForOutOfToolVerification

When(/^I verify an author out of tool$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('firstCheckerEmail'),
    password: Cypress.env('password'),
  })

  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  cy.get('@submissionParam').then((submission) => {
    authorForOutOfToolVerification = submission.authors
    Author.verifyOutOfTool(authorForOutOfToolVerification)
  })

  ManuscriptPage.makeDecision({ decisionType: 'approve' })
})

Then(/^the author is automatically verified on the next submission$/, () => {
  cy.submitManuscript({
    type: 'screening',
    title: 'Testing if the author is automatically verified',
    authors: authorForOutOfToolVerification,
  })

  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  cy.findByText('Author Identity Verification').click()

  cy.findByText('VERIFIED').should('exist')
})
