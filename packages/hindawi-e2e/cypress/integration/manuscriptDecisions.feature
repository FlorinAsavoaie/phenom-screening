Feature: Manuscript decisions

    Scenario Outline: Testing manuscript decisions

        Given a submitted manuscript
            | type             |
            | <manuscriptType> |
        When I log in with the assigned checker and <decisionType> the manuscript
        Then I can find it in the <dashboardFilter> filter with the <statusType> status

        Examples:
            | manuscriptType  | decisionType     | dashboardFilter | statusType      |
            | screening       | approve          | archived        | approved        |
            | screening       | refuseToConsider | archived        | RTC             |
            | screening       | returnToDraft    | inProgress      | RTD             |
            | screening       | void             | archived        | Void            |
            | qualityChecking | approveQC        | archived        | approved        |
            | qualityChecking | refuseToConsider | archived        | RTC             |
            | qualityChecking | requestFiles     | inProgress      | Files Requested |