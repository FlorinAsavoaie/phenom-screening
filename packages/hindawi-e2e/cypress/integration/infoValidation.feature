Feature: Info Validation

    Background:
        Given I'm logged in as an Admin

    Scenario: Add empty notes to manuscript
        Given a submitted manuscript
        When I add the following notes

            | type           |
            | titleNote      |
            | abstractNote   |
            | authorsNote    |
            | relevanceNote  |
            | referencesNote |

        Then the empty notes are available

            | type           | content |
            | titleNote      |         |
            | abstractNote   |         |
            | authorsNote    |         |
            | relevanceNote  |         |
            | referencesNote |         |

    Scenario: Add content to notes
        Given a submitted manuscript
        And the manuscript has the following empty notes

            | type           |
            | titleNote      |
            | abstractNote   |
            | authorsNote    |
            | relevanceNote  |
            | referencesNote |

        When I add the following content to the notes

            | content         |
            | Title note.     |
            | Abstract note.  |
            | Author note.    |
            | Relevance note. |
            | Relevance note. |

        Then the content of the notes is available

            | type           | content         |
            | titleNote      | Title note.     |
            | abstractNote   | Abstract note.  |
            | authorsNote    | Author note.    |
            | relevanceNote  | Relevance note. |
            | referencesNote | Relevance note. |

    Scenario: Delete notes from manuscript
        Given a submitted manuscript
        And the manuscript has the following notes

            | type           |
            | titleNote      |
            | abstractNote   |
            | authorsNote    |
            | relevanceNote  |
            | referencesNote |

        When I delete the notes
        Then the notes are no longer available

            | type           |
            | titleNote      |
            | abstractNote   |
            | authorsNote    |
            | relevanceNote  |
            | referencesNote |

    Scenario: Add references
        Given a submitted manuscript
        When I add the following references

            | references                |
            | These are the references. |

        Then the references are available

            | references                |
            | These are the references. |

    Scenario: Edit references
        Given a submitted manuscript
        And the manuscript has the following references

            | references                   |
            | This are the old references. |

        When I update the references with the following content

            | references                                |
            | This are the new and improved references. |

        Then the updated references are available

            | references                                |
            | This are the new and improved references. |

    Scenario: Delete references
        Given a submitted manuscript
        And the manuscript has references

            | references       |
            | Some references. |

        When I delete the references
        Then references are no longer available

            | references |
            |            |

    Scenario: Select checkboxes
        Given a submitted manuscript
        When I check the following checkboxes

            | checkboxes                  |
            | articleTypeIsNotAppropriate |
            | differentAuthors            |
            | graphical                   |
            | includesCitations           |
            | manuscriptIsMissingSections |
            | missingAffiliations         |
            | missingReferences           |
            | nonacademicAffiliations     |
            | topicNotAppropriate         |
            | wrongReferences             |

        Then the checkboxes are selected

            | checkboxes                  |
            | articleTypeIsNotAppropriate |
            | differentAuthors            |
            | graphical                   |
            | manuscriptIsMissingSections |
            | missingAffiliations         |
            | missingReferences           |
            | nonacademicAffiliations     |
            | topicNotAppropriate         |
            | wrongReferences             |

    Scenario: Unselect checkboxes
        Given a submitted manuscript with the following checkboxes selected

            | checkboxes                  |
            | articleTypeIsNotAppropriate |
            | differentAuthors            |
            | graphical                   |
            | manuscriptIsMissingSections |
            | missingAffiliations         |
            | missingReferences           |
            | nonacademicAffiliations     |
            | topicNotAppropriate         |
            | wrongReferences             |

        When I uncheck the following checkboxes

            | checkboxes                  |
            | articleTypeIsNotAppropriate |
            | differentAuthors            |
            | graphical                   |
            | includesCitations           |
            | manuscriptIsMissingSections |
            | missingAffiliations         |
            | missingReferences           |
            | nonacademicAffiliations     |
            | topicNotAppropriate         |
            | wrongReferences             |

        Then the checkboxes are no longer selected

            | checkboxes                  |
            | articleTypeIsNotAppropriate |
            | differentAuthors            |
            | graphical                   |
            | manuscriptIsMissingSections |
            | missingAffiliations         |
            | missingReferences           |
            | nonacademicAffiliations     |
            | topicNotAppropriate         |
            | wrongReferences             |