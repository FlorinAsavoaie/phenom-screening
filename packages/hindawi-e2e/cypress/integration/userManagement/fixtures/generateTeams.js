export function generateTeamData({
  teamName,
  teamType,
  numberOfCheckers,
  journalId,
}) {
  let team
  if (teamType === 'screening') {
    const teamMembers = {
      team: {
        name: teamName,
        type: 'screening',
        journalIds: journalId,
        members: [
          {
            givenNames: 'Stefan',
            surname: 'Leaderan',
            email: Cypress.env('teamLeaderEmail'),
            role: 'teamLeader',
          },
          {
            givenNames: 'Vasile',
            surname: 'Checkeras',
            email: Cypress.env('firstCheckerEmail'),
            role: 'screener',
          },
        ],
      },
    }
    switch (numberOfCheckers) {
      case 1:
        team = teamMembers
        break
      case 2:
        teamMembers.team.members.push({
          givenNames: 'Mihai',
          surname: 'Checkerovici',
          email: Cypress.env('secondCheckerEmail'),
          role: 'screener',
        })
        team = teamMembers
        break
      case 3:
        teamMembers.team.members.push(
          {
            givenNames: 'Mihai',
            surname: 'Checkerovici',
            email: Cypress.env('secondCheckerEmail'),
            role: 'screener',
          },
          {
            givenNames: 'Dan',
            surname: 'Checkerescu',
            email: Cypress.env('thirdCheckerEmail'),
            role: 'screener',
          },
        )
        team = teamMembers
        break
      default:
        cy.log('Currently you can only create teams with maximum 3 checkers.')
    }
  } else if (teamType === 'qualityChecking') {
    const teamMembers = {
      team: {
        name: teamName,
        type: 'qualityChecking',
        journalIds: journalId,
        members: [
          {
            givenNames: 'Stefan',
            surname: 'Leaderan',
            email: Cypress.env('teamLeaderEmail'),
            role: 'teamLeader',
          },
          {
            givenNames: 'Vasile',
            surname: 'Checkeras',
            email: Cypress.env('firstCheckerEmail'),
            role: 'qualityChecker',
          },
        ],
      },
    }
    switch (numberOfCheckers) {
      case 1:
        team = teamMembers
        break
      case 2:
        teamMembers.team.members.push({
          givenNames: 'Mihai',
          surname: 'Checkerovici',
          email: Cypress.env('secondCheckerEmail'),
          role: 'qualityChecker',
        })
        team = teamMembers
        break
      case 3:
        teamMembers.team.members.push(
          {
            givenNames: 'Mihai',
            surname: 'Checkerovici',
            email: Cypress.env('secondCheckerEmail'),
            role: 'qualityChecker',
          },
          {
            givenNames: 'Dan',
            surname: 'Checkerescu',
            email: Cypress.env('thirdCheckerEmail'),
            role: 'qualityChecker',
          },
        )
        team = teamMembers
        break
      default:
        cy.log('Currently you can create teams with maximum 3 checkers.')
    }
  }
  return team
}
