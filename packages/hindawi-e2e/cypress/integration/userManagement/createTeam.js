import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { UserManagementPage } from './pageObjects/userManagementPage'
import { goTo } from '../../support/helpers/accessByPath'

let timestamp

Given(/^I am on the User management section$/, () => {
  timestamp = new Date().toLocaleString()

  goTo.userManagement()

  UserManagementPage.accessTeams()
})

When(
  /^I create a (.*) team called (.*) with the following members and the (.*) assigned$/,
  // eslint-disable-next-line max-params
  (teamType, teamName, journalName, table) => {
    UserManagementPage.accessCreateTeamModal()

    UserManagementPage.modal.addTeamName(`${teamName} [${timestamp}]`)
    UserManagementPage.modal.selectTeamType(teamType)
    UserManagementPage.modal.addJournal(journalName)

    for (let i = 0; i <= 1; i += 1) {
      UserManagementPage.modal.addTeamMember({
        firstName: table.rows()[i][0],
        lastName: table.rows()[i][1],
        email: table.rows()[i][2],
        role: table.rows()[i][3],
      })
    }

    UserManagementPage.modal.confirmTeam()
  },
)

Then(
  /^I see the (.*) team in the list having (.*) members and the (.*) assigned$/,
  (teamName, numberOfMembers, assignedJournal) => {
    UserManagementPage.findTeam(`${teamName} [${timestamp}`)

    UserManagementPage.validateTeam({
      teamName: `${teamName} [${timestamp}]`,
      numberOfMembers,
      assignedJournal,
    })
  },
)
