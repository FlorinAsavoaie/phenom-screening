export class UserManagementPage {
  static modal = {
    addTeamName: (teamName) => cy.findByTestId('name').type(teamName),
    selectTeamType: (teamType) =>
      cy.get(`[name="type"][value="${teamType}"]`).click(),
    addJournal: (journalName) => {
      cy.get('[data-test-id="search"][placeholder="Search Journals"]')
        .type(journalName)
        .then(() => {
          cy.findByTestId('add-journal').eq(0).click({ force: true })
        })
    },
    removeJournal: () =>
      cy.findByTestId('remove-journal').eq(0).click({ force: true }),
    addTeamMember: ({ firstName, lastName, email, role }) => {
      cy.findByTestId('givenNames').type(firstName)
      cy.findByTestId('surname').type(lastName)
      cy.findByTestId('email').type(email)
      cy.findByTestId('role-filter').click()

      switch (role) {
        case 'Team Lead':
          cy.findByTestId('role-teamLeader').click()
          break
        case 'Screener':
          cy.findByTestId('role-screener').click()
          break
        case 'Quality Checker':
          cy.findByTestId('role-qualityChecker').click()
          break
        default:
          cy.log('Provided role not found.')
      }
      cy.findByTestId('add-team-member').click()
    },
    confirmTeam: () => cy.findByTestId('modal-create').click(),
  }
  static accessUsers() {
    cy.get('nav div').within(() => {
      cy.findByText('Users').click()
    })
  }

  static accessTeams() {
    cy.get('nav div').within(() => {
      cy.findByText('Teams').click()
    })
  }

  static accessCreateTeamModal() {
    cy.findByText('Create Team').click()
  }

  static findTeam(teamName) {
    cy.get('[placeholder="Find team..."]').type(teamName).type('{enter}')
  }

  static validateTeam({ teamName, numberOfMembers, assignedJournal }) {
    cy.findByTestId('table-row').within(() => {
      cy.findByText(teamName).should('exist')
      cy.findByText(numberOfMembers).should('exist')
      cy.findByText(assignedJournal).should('exist')
    })
  }

  static accessEditTeamModal(teamName) {
    cy.get('[placeholder="Find team..."]').type(teamName).type('{enter}')
    cy.findByTestId('table-row').realHover()
    cy.findByText('Edit').click()
  }

  static editTeamName(newTeamName) {
    cy.findByTestId('name').clear().type(newTeamName)
  }
}
