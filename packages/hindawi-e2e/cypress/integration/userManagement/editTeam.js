import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { UserManagementPage } from './pageObjects/userManagementPage'
import { goTo } from '../../support/helpers/accessByPath'
import { Team } from './graphql/team'

let timestamp
let oldTeamName
let newTeamName

Given(/^I have a (.*) team for editing called (.*)$/, (teamType, teamName) => {
  timestamp = new Date().toLocaleString()
  oldTeamName = `${teamName} [${timestamp}]`

  cy.loginKeycloak({
    username: Cypress.env('adminEmail'),
    password: Cypress.env('password'),
  }).then(() => {
    cy.getToken().then((token) => {
      Team.create({
        adminToken: token,
        teamName: oldTeamName,
        teamType,
        numberOfCheckers: 1,
        journalId: Cypress.env('journalId1'),
      })
    })
  })
})

When(/^I edit the team name$/, () => {
  newTeamName = `Edited ${oldTeamName}`

  goTo.userManagement()

  UserManagementPage.accessTeams()

  UserManagementPage.accessEditTeamModal(oldTeamName)
  UserManagementPage.editTeamName(newTeamName)
})

When(/^I add another (.*)$/, (checker) => {
  UserManagementPage.modal.addTeamMember({
    firstName: 'Edit',
    lastName: 'Editarescu',
    email: `${Cypress.env('baseEmail')}+edit@hindawi.com`,
    role: checker,
  })
})

When(/^I change the journal$/, () => {
  UserManagementPage.modal.removeJournal()

  UserManagementPage.modal.addJournal('[Test] Journal for Edit Team 2')

  UserManagementPage.modal.confirmTeam()
})

Then(/^I see the changes in the Teams tab$/, () => {
  UserManagementPage.findTeam(newTeamName)

  UserManagementPage.validateTeam({
    teamName: newTeamName,
    numberOfMembers: 3,
    assignedJournal: '[Test] Journal for Edit Team 2',
  })
})
