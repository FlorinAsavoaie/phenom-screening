import { graphqlService } from '../../../support/graphqlService'
import { createTeam } from './mutations'
import { generateTeamData } from '../fixtures/generateTeams'

export class Team {
  static create({
    adminToken,
    teamName,
    teamType,
    numberOfCheckers,
    journalId,
  }) {
    const team = generateTeamData({
      teamName,
      teamType,
      numberOfCheckers,
      journalId,
    })

    graphqlService({
      query: createTeam,
      token: adminToken,
      variables: team,
    })
  }
}
