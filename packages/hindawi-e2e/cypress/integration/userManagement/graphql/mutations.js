export const createTeam = `mutation createTeam($team: CreateTeamInput!) {
    createTeam(input: $team) {
      id
      name
      type
      members {
        id
        role
        email
        surname
        givenNames
        isConfirmed
      }
      journals {
        id
        name
      }
    }
  }`
