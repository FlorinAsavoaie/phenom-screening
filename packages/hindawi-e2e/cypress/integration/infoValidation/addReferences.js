import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Reference } from './graphql/reference'

let token
let manuscriptId

When(/^I add the following references$/, (table) => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.get('@manuscriptId')
    .then((id) => (manuscriptId = id))
    .then(() => {
      Reference.update({
        token,
        manuscriptId,
        references: table.rows()[0][0],
      })
    })
})

Then(/^the references are available$/, (table) => {
  Reference.get({ token, manuscriptId }).then((response) => {
    expect(response.body.data.getScreeningManuscript.references).to.equal(
      table.rows()[0][0],
    )
  })
})
