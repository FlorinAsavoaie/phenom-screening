import { graphqlService } from '../../../support/graphqlService'
import { addNote, updateNote, removeNote } from './mutations'
import { getNote } from './queries'

export class Note {
  static add({ token, manuscriptId, type }) {
    return graphqlService({
      query: addNote,
      token,
      variables: { manuscriptId, type },
    })
  }

  static update({ token, noteId, content }) {
    graphqlService({
      query: updateNote,
      token,
      variables: { noteId, content },
    })
  }

  static remove({ token, noteId }) {
    graphqlService({
      query: removeNote,
      token,
      variables: { noteId },
    })
  }

  static get({ token, manuscriptId, type }) {
    return graphqlService({
      query: getNote,
      token,
      variables: { manuscriptId, type },
    })
  }
}
