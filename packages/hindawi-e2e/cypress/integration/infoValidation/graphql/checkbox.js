import { graphqlService } from '../../../support/graphqlService'
import { toggleValidation } from './mutations'
import { getManuscriptValidations } from './queries'

export class Checkbox {
  static select({ token, manuscriptId, field }) {
    graphqlService({
      query: toggleValidation,
      token,
      variables: { manuscriptId, field },
    })
  }

  static get({ token, manuscriptId }) {
    return graphqlService({
      query: getManuscriptValidations,
      token,
      variables: { manuscriptId },
    })
  }
}
