export const getNote = `
query getNote($manuscriptId: ID!, $type: NoteType!) {
  getNote(manuscriptId: $manuscriptId, type: $type) {
    id
    created
    updated
    type
    content
    manuscriptId
  }
}
`

export const getReferences = `
  query getScreeningManuscript($manuscriptId: ID!) {
    getScreeningManuscript(manuscriptId: $manuscriptId) {
      references
    }
  }
`

export const getManuscriptValidations = `
  query getManuscriptValidations($manuscriptId: ID!) {
    getManuscriptValidations(manuscriptId: $manuscriptId) {
      includesCitations
      graphical
      differentAuthors
      missingAffiliations
      nonacademicAffiliations
      topicNotAppropriate
      articleTypeIsNotAppropriate
      manuscriptIsMissingSections
      missingReferences
      wrongReferences
    }
  }
`
