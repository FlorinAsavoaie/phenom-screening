export const addNote = `
  mutation addNote($manuscriptId: String, $type: NoteType) {
    addNote(manuscriptId: $manuscriptId, type: $type) {
      id
      type
      content
      manuscriptId
    }
  }
`
export const removeNote = `
  mutation removeNote($noteId: String!) {
    removeNote(noteId: $noteId)
  }
`
export const updateNote = `
  mutation updateNote($noteId: String!, $content: String) {
    updateNote(noteId: $noteId, content: $content)
  }
`

export const updateReferences = `
mutation updateReferences($manuscriptId: String!, $references: String) {
  updateReferences(manuscriptId: $manuscriptId, references: $references)
}
`

export const toggleValidation = `
  mutation toggleValidation($manuscriptId: String!, $field: ValidationFields) {
    toggleValidation(manuscriptId: $manuscriptId, field: $field)
  }
`
