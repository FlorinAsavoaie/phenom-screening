import { graphqlService } from '../../../support/graphqlService'
import { updateReferences } from './mutations'
import { getReferences } from './queries'

export class Reference {
  static update({ token, manuscriptId, references }) {
    graphqlService({
      query: updateReferences,
      token,
      variables: { manuscriptId, references },
    })
  }

  static get({ token, manuscriptId }) {
    return graphqlService({
      query: getReferences,
      token,
      variables: { manuscriptId },
    })
  }
}
