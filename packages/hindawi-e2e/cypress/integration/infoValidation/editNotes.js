import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Note } from './graphql/note'
import { getNoteId } from './helpers/getNoteId'

let token
let manuscriptId
const noteIds = []

When(/^the manuscript has the following empty notes$/, (table) => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.get('@manuscriptId')
    .then((id) => (manuscriptId = id))
    .then(() => {
      table.hashes().forEach((element) =>
        Note.add({
          token,
          manuscriptId,
          type: element.type,
        })
          .then(() => getNoteId({ type: element.type, token, manuscriptId }))
          .then((id) => noteIds.push(id)),
      )
    })
})

When(/^I add the following content to the notes$/, (table) => {
  noteIds.forEach((noteId, i) =>
    Note.update({
      token,
      noteId,
      content: table.hashes()[i].content,
    }),
  )
})

Then(/^the content of the notes is available$/, (table) => {
  table.hashes().forEach((element, i) =>
    Note.get({
      token,
      manuscriptId,
      type: element.type,
    }).then((response) => {
      expect(response.body.data.getNote.content).to.equal(
        table.hashes()[i].content,
      )
    }),
  )
})
