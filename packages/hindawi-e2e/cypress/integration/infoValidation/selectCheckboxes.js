import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Checkbox } from './graphql/checkbox'

let token
let manuscriptId

When(/^I check the following checkboxes$/, (table) => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.get('@manuscriptId')
    .then((id) => (manuscriptId = id))
    .then(() => {
      table.hashes().forEach((element) =>
        Checkbox.select({
          token,
          manuscriptId,
          field: element.checkboxes,
        }),
      )
    })
})

Then(/^the checkboxes are selected$/, (table) => {
  table.hashes().forEach((element) =>
    Checkbox.get({ token, manuscriptId }).then((response) => {
      expect(response.body.data.getManuscriptValidations[element.checkboxes]).to
        .be.true
    }),
  )
})
