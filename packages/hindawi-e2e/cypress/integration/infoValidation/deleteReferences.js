import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Reference } from './graphql/reference'

let token
let manuscriptId

When(/^the manuscript has references$/, (table) => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.get('@manuscriptId')
    .then((id) => (manuscriptId = id))
    .then(() => {
      Reference.update({
        token,
        manuscriptId,
        references: table.rows()[0][0],
      })
    })
    .then(() => {
      Reference.get({ token, manuscriptId }).then((response) => {
        expect(response.body.data.getScreeningManuscript.references).to.equal(
          table.rows()[0][0],
        )
      })
    })
})

When(/^I delete the references$/, () => {
  Reference.update({
    token,
    manuscriptId,
    references: '',
  })
})

Then(/^references are no longer available$/, (table) => {
  Reference.get({ token, manuscriptId }).then((response) => {
    expect(response.body.data.getScreeningManuscript.references).to.equal(
      table.rows()[0][0],
    )
  })
})
