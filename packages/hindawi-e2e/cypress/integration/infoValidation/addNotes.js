import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Note } from './graphql/note'

let token
let manuscriptId

When(/^I add the following notes$/, (table) => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.get('@manuscriptId')
    .then((id) => (manuscriptId = id))
    .then(() => {
      table.hashes().forEach((note) =>
        Note.add({
          token,
          manuscriptId,
          type: note.type,
        }),
      )
    })
})

Then(/^the empty notes are available$/, (table) => {
  table.hashes().forEach((note, i) =>
    Note.get({
      token,
      manuscriptId,
      type: note.type,
    }).then((response) => {
      expect(response.body.data.getNote.content).to.equal(
        table.hashes()[i].content,
      )
    }),
  )
})
