import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Note } from './graphql/note'
import { getNoteId } from './helpers/getNoteId'

let token
let manuscriptId
const noteIds = []

When(/^the manuscript has the following notes$/, (table) => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.get('@manuscriptId')
    .then((id) => (manuscriptId = id))
    .then(() => {
      table.hashes().forEach((element) =>
        Note.add({
          token,
          manuscriptId,
          type: element.type,
        })
          .then(() => getNoteId({ type: element.type, token, manuscriptId }))
          .then((id) => noteIds.push(id)),
      )
    })
})

When(/^I delete the notes$/, () => {
  noteIds.forEach((noteId) =>
    Note.remove({
      token,
      noteId,
    }),
  )
})

Then(/^the notes are no longer available$/, (table) => {
  table.hashes().forEach((element) =>
    Note.get({
      token,
      manuscriptId,
      type: element.type,
    }).then((response) => {
      expect(response.body.data.getNote).to.be.null
    }),
  )
})
