import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Checkbox } from './graphql/checkbox'

let token
let manuscriptId

Given(
  /^a submitted manuscript with the following checkboxes selected$/,
  (table) => {
    cy.getToken().then((accessToken) => (token = accessToken))

    cy.submitManuscript({
      type: 'screening',
    })
      .then(() =>
        cy
          .get('@manuscriptId')
          .then((id) => (manuscriptId = id))
          .then(() => {
            table.hashes().forEach((element) =>
              Checkbox.select({
                token,
                manuscriptId,
                field: element.checkboxes,
              }),
            )
          }),
      )
      .then(() =>
        table.hashes().forEach((element) =>
          Checkbox.get({ token, manuscriptId }).then((response) => {
            expect(
              response.body.data.getManuscriptValidations[element.checkboxes],
            ).to.be.true
          }),
        ),
      )
  },
)

When(/^I uncheck the following checkboxes$/, (table) => {
  table.hashes().forEach((element) =>
    Checkbox.select({
      token,
      manuscriptId,
      field: element.checkboxes,
    }),
  )
})

Then(/^the checkboxes are no longer selected$/, (table) => {
  table.hashes().forEach((element) =>
    Checkbox.get({ token, manuscriptId }).then((response) => {
      expect(response.body.data.getManuscriptValidations[element.checkboxes]).to
        .be.false
    }),
  )
})
