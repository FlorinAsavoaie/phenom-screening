import { Note } from '../graphql/note'

export function getNoteId({ type, token, manuscriptId }) {
  let noteId

  Note.get({
    token,
    manuscriptId,
    type,
  }).then((response) => (noteId = response.body.data.getNote.id))

  return noteId
}
