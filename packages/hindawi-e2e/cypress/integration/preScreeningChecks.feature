Feature: Pre-Screening Check

    Background:
        Given I'm logged in as an Admin

    Scenario Outline: Language & Wordcount check

        Given a submitted manuscript
            | fileName         |
            | <manuscriptFile> |

        When I do the pre-screening checks
        Then the number of words should be <wordCount>
        And the language should be <language>
        And the percentage of english words should be <percentOfEnglish>

        Examples: Full english, partially english and no english manuscripts
            | manuscriptFile                          | wordCount | percentOfEnglish | language    |
            | Blockchain for Real Estate Industry.pdf | 2683      | 99%              | ENGLISH     |
            | German Research Sample 35% english.pdf  | 295       | 33%              | NOT ENGLISH |
            | Lorem Ipsum No English.pdf              | 573       | 0%               | NOT ENGLISH |

    Scenario: Check the suspicious words

        Given a submitted manuscript
        When I verify the suspicious words
        Then the expected suspicious words, their occurrence number and reason are available

            | word   | noOfOccurrences | reason                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
            | 7      | 4               | US SDN List Sanctioned individual or organisation: ' Other identities: a.k.a. USUGA TORRES, Arley; a.k.a. CERO SIETE, Other identifying notes: DOB 14 Aug 1979; POB Tierralta, Cordoba, Colombia; citizen Colombia; Cedula No. 71255292 (Colombia) (individual) [SDNTK].. For more information, consult US SDN List.' If the suspicious keyword appears in the authors names/affiliations, escalate to Wiley Legal to check whether it is appropriate to proceed or to RTC. |
            | LET    | 1               | EU SDN List Sanctioned individual or organisation: ' Entity Remarks: , Entity Type: enterprise, List Type: TAQA, Function: . For more information, consult EU SDN List.' If the suspicious keyword appears in the authors names/affiliations, escalate to Wiley Legal to check whether it is appropriate to proceed or to RTC.                                                                                                                                              |
            | Major  | 1               | EU SDN List Sanctioned individual or organisation: ' Entity Remarks: (Date of UN designation: 2005-11-01), Entity Type: person, List Type: COD, Function: . For more information, consult EU SDN List.' If the suspicious keyword appears in the authors names/affiliations, escalate to Wiley Legal to check whether it is appropriate to proceed or to RTC.                                                                                                               |
            | Park   | 1               | Won-Gil Park. He is an author who used to cite himself a lot and ask other authors to cite him a lot in their manuscripts. We should check if there are citations to "Park" in the references in the manuscripts. Park is already in our Blacklist.                                                                                                                                                                                                                         |
            | Said   | 1               | EU SDN List Sanctioned individual or organisation: ' Entity Remarks: , Entity Type: person, List Type: TAQA, Function: . For more information, consult EU SDN List.' If the suspicious keyword appears in the authors names/affiliations, escalate to Wiley Legal to check whether it is appropriate to proceed or to RTC.                                                                                                                                                  |
            | survey | 1               | Only relevant for submissions that also mention Covid-19 or any of the other abbreviations for the disease mentioned in the Covid-19 instructions (https://docs.google.com/document/d/1FSLCgLUzjUf_SUini0Xc5toRRhBCx1hxmavf6sWXLtI/edit). Hold in Screening before performing checks and refer to instructions for actions and escalation details.                                                                                                                          |

    Scenario: Check the iThenticate score

        Given a submitted manuscript
        When I verify the iThenticate score
        Then the expected similarity score is available

    Scenario: Check similar submissions

        Given two similar submissions
        When I submit another similar submission
        Then two matches will be found for that submission
