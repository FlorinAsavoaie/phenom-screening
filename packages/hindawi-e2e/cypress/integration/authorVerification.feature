Feature: Author verification

    Background:
        Given I'm logged in as a checker

    Scenario: Verifing an author out of tool

        Given a submitted manuscript
        When I verify an author out of tool
        Then the author is automatically verified on the next submission

    Scenario: Author cannot be verified

        Given a submitted manuscript
        When an author can't be verified
        Then the author can be verified again on the next submission

    Scenario: Sanctioned authors are correctly tagged

        Given a submission from sanctioned authors
        When I verify the authors
        Then the authors are marked with the correct sanctioned tags

    Scenario: Verify author from WoS

        Given a new manuscript submission
        When I verify the author from the WoS results
        Then the verified author is automatically verified on the next submission