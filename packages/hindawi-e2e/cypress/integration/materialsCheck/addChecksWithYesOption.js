import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Option } from './graphql/option'
import { Decision } from '../peerReviewCycleCheck/graphql/decision'

let token
let manuscriptId

Given(/^a submitted manuscript in the materials check step$/, () => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.submitManuscript({
    type: 'qualityChecking',
  }).then(() =>
    cy
      .get('@manuscriptId')
      .then((id) => (manuscriptId = id))
      .then(() => {
        Decision.approvePeerReviewCycleCheck({ token, manuscriptId })
      }),
  )
})

When(
  /^I select the 'Yes' option for the following material checks$/,
  (table) => {
    table.hashes().forEach((element) => {
      Option.add({
        token,
        manuscriptId,
        name: element.checks,
        selectedOption: element.option,
      })
    })
  },
)

Then(
  /^the material checks have the 'Yes' option selected$/,
  (expectedResult) => {
    Option.get({ token, manuscriptId }).then((response) => {
      const checksResponse = response.body.data.materialChecks

      const actualResult = checksResponse
        .map((check) => ({
          checks: check.name,
          option: check.selectedOption,
        }))
        .sort((a, b) => (a.checks > b.checks ? 1 : -1))

      expect(actualResult).to.deep.equal(expectedResult.hashes())
    })
  },
)
