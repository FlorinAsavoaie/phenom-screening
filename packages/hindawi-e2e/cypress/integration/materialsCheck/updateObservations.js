import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Option } from './graphql/option'
import { Decision } from '../peerReviewCycleCheck/graphql/decision'

let token
let manuscriptId

Given(
  /^a submitted manuscript with material checks and observations$/,
  (table) => {
    cy.getToken().then((accessToken) => (token = accessToken))

    cy.submitManuscript({
      type: 'qualityChecking',
    }).then(() =>
      cy
        .get('@manuscriptId')
        .then((id) => (manuscriptId = id))
        .then(() =>
          Decision.approvePeerReviewCycleCheck({ token, manuscriptId }),
        )
        .then(() =>
          table.hashes().forEach((element) => {
            Option.add({
              token,
              manuscriptId,
              name: element.checks,
              selectedOption: element.option,
            })
          }),
        )
        .then(() =>
          table.hashes().forEach((element) =>
            Option.updateObservation({
              token,
              manuscriptId,
              name: element.checks,
              additionalInfo: element.observation,
            }),
          ),
        )
        .then(() =>
          Option.get({ token, manuscriptId }).then((response) => {
            const checksResponse = response.body.data.materialChecks

            const actualResult = checksResponse
              .map((check) => ({
                checks: check.name,
                option: check.selectedOption,
                observation: check.additionalInfo,
              }))
              .sort((a, b) => (a.checks > b.checks ? 1 : -1))

            expect(actualResult).to.deep.equal(table.hashes())
          }),
        ),
    )
  },
)

When(/^I update the observations of the material checks$/, (table) => {
  table.hashes().forEach((element) =>
    Option.updateObservation({
      token,
      manuscriptId,
      name: element.checks,
      additionalInfo: element.observation,
    }),
  )
})

Then(
  /^the new material checks observations are available$/,
  (expectedResult) => {
    Option.get({ token, manuscriptId }).then((response) => {
      const checksResponse = response.body.data.materialChecks

      const actualResult = checksResponse
        .map((check) => ({
          checks: check.name,
          option: check.selectedOption,
          observation: check.additionalInfo,
        }))
        .sort((a, b) => (a.checks > b.checks ? 1 : -1))

      expect(actualResult).to.deep.equal(expectedResult.hashes())
    })
  },
)
