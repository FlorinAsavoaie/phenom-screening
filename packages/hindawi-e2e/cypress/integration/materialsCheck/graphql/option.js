import { graphqlService } from '../../../support/graphqlService'
import {
  selectOptionForMaterialCheck,
  updateObservationForMaterialCheck,
} from './mutations'
import { getManuscriptMaterialChecks } from './queries'

export class Option {
  static add({ token, manuscriptId, name, selectedOption }) {
    graphqlService({
      query: selectOptionForMaterialCheck,
      token,
      variables: { input: { manuscriptId, name, selectedOption } },
    })
  }

  static updateObservation({ token, manuscriptId, name, additionalInfo }) {
    graphqlService({
      query: updateObservationForMaterialCheck,
      token,
      variables: { input: { manuscriptId, name, additionalInfo } },
    })
  }

  static get({ token, manuscriptId }) {
    return graphqlService({
      query: getManuscriptMaterialChecks,
      token,
      variables: { manuscriptId },
    })
  }
}
