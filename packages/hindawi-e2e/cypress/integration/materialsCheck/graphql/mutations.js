export const selectOptionForMaterialCheck = `
  mutation selectOptionForMaterialCheck(
    $input: SelectOptionForMaterialCheckInput
  ) {
    materialCheck: selectOptionForMaterialCheck(input: $input) {
      selectedOption
      additionalInfo
    }
  }
`

export const updateObservationForMaterialCheck = `
  mutation updateObservationForMaterialCheck(
    $input: UpdateObservationForMaterialCheck
  ) {
    materialCheck: updateObservationForMaterialCheck(input: $input) {
      additionalInfo
    }
  }
`
