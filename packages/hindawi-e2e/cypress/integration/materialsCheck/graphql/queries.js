export const getManuscriptMaterialChecks = `
  query getManuscriptMaterialChecks($manuscriptId: ID!) {
    materialChecks: getManuscriptMaterialChecks(manuscriptId: $manuscriptId) {
      name
      selectedOption
      additionalInfo
      config {
        options {
          value
          supportsAdditionalInfo
          label
          type
        }
        order
      }
    }
  }
`
