import { Given } from '@badeball/cypress-cucumber-preprocessor'

const Chance = require('chance')

const chance = new Chance()

Given(/^a submitted manuscript$/, (submissionDetails) => {
  prepareSubmissionParameters(submissionDetails)
  cy.get('@submissionParam').then((submission) => {
    cy.submitManuscript({
      type: submission.type,
      title: submission.title,
      fileName: submission.fileName,
      authors: submission.authors,
      journalId: submission.journal,
    })
  })
})

function prepareSubmissionParameters(submissionDetails) {
  const randomString = chance.string({
    length: 6,
  })
  const submissionParam = {
    type: 'screening',
    title: `Automated test [${randomString}]`,
    authors: [`${Cypress.env('baseEmail')}+${randomString}@hindawi.com`],
    journal: Cypress.env('journalIdOneChecker'),
  }

  if (submissionDetails) {
    const mySubmission = submissionDetails.hashes()[0]

    submissionParam.type = mySubmission.type || submissionParam.type

    submissionParam.title = mySubmission.title || submissionParam.title

    submissionParam.authors = mySubmission.author
      ? mySubmission.author
          .split(',')
          .map((eachAuthor) => `${eachAuthor}+${randomString}@hindawi.com`)
      : submissionParam.authors

    submissionParam.customId = chance.string({ length: 7, numeric: true })

    submissionParam.journal = mySubmission.journal
      ? Cypress.env(`${mySubmission.journal}`)
      : submissionParam.journal
    submissionParam.fileName = mySubmission.fileName || submissionParam.fileName
  }
  cy.wrap(submissionParam).as('submissionParam')
}
