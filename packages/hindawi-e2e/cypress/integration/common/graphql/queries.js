export const getManuscriptChecks = `
query getManuscriptChecks($manuscriptId: ID!) {
    getManuscriptChecks(manuscriptId: $manuscriptId) {
      wordCount
      reportUrl
      suspiciousWords {
        previouslyVerifiedSuspiciousWords {
          word
          noOfOccurrences
          reason
        }
        newSuspiciousWords {
          word
          noOfOccurrences
          reason
        }
      }
      languagePercentage
      totalSimilarityPercentage
      firstSourceSimilarityPercentage
    }
  }
`
