import { graphqlService } from '../../../support/graphqlService'
import { getManuscriptChecks } from './queries'

export class Manuscript {
  static getManuscriptChecks({ token, manuscriptId }) {
    graphqlService({
      query: getManuscriptChecks,
      token,
      variables: { manuscriptId },
    })
  }
}
