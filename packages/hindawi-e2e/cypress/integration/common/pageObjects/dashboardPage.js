export class DashboardPage {
  static accessManuscriptsTab() {
    cy.get('nav').within(() => {
      cy.findByText('Manuscripts').click()
    })
  }

  static accessUserManagementTab() {
    cy.get('nav').within(() => {
      cy.findByText('User management').click()
    })
  }

  static accessSettingsTab() {
    cy.get('nav').within(() => {
      cy.findByText('Settings').click()
    })
  }

  static searchForManuscript(manuscriptNameOrId) {
    cy.findByTestId('search').type(manuscriptNameOrId).type('{enter}')
  }

  static applyFilter(dashboardFilter) {
    cy.findByTestId('dashboard-filter').click()
    switch (dashboardFilter) {
      case 'processing':
        cy.findByTestId('dashboard-processing').click()
        break
      case 'inProgress':
        cy.findByTestId('dashboard-inProgress').click()
        break
      case 'escalated':
        cy.findByTestId('dashboard-escalated').click()
        break
      case 'archived':
        cy.findByTestId('dashboard-archived').click()
        break
      default:
        cy.log('Provide a valid filter.')
    }
  }

  static assignChecker(checker) {
    cy.findByTestId('assigned-checker').click()
    cy.findByText(checker).click()
    cy.findByTestId('modal-confirm').click()
  }

  static logout() {
    cy.findByText('Logout').click()
  }
}
