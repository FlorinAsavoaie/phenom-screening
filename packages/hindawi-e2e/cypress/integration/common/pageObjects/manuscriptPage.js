export class ManuscriptPage {
  static makeDecision({ decisionType }) {
    if (decisionType === 'finalizePeerReviewCycleCheck') {
      cy.findByText('Peer Review Cycle Check').click()
      cy.findByText('Finalize Peer Review Cycle Check').click()
    } else {
      switch (decisionType) {
        case 'approve':
          cy.findByText('Approve Manuscript').click()
          break
        case 'approveQC':
          cy.findByText('Peer Review Cycle Check').click()
          cy.findByText('Finalize Peer Review Cycle Check').click()
          cy.findByTestId('modal-confirm').click()
          cy.findByText('Approve Manuscript').click()
          break
        case 'refuseToConsider':
          cy.findByText('Refuse to Consider').click()
          break
        case 'returnToDraft':
          cy.findByText('Return to Draft').click()
          break
        case 'void':
          cy.findByText('Void Manuscript').click()
          break
        case 'requestFiles':
          cy.findByText('Peer Review Cycle Check').click()
          cy.findByText('Finalize Peer Review Cycle Check').click()
          cy.findByTestId('modal-confirm').click()
          cy.findByText('Request Files').click()
          break
        default:
          cy.log('Provided decision not found.')
      }
      cy.get('textarea').type('Type something here.')
    }
    cy.findByTestId('modal-confirm').click()
  }

  static escalateManuscript({ escalationType }) {
    cy.findByText('Escalate').click()
    if (escalationType === 'pause') {
      cy.get(`[name="escalationOption"][value="pause"]`).click()
    } else if (escalationType === 'sendToTeamLead') {
      cy.get(`[name="escalationOption"][value="sendToTeamLead"]`).click()
    }
    cy.get('textarea').type('Type something here.')
    cy.findByTestId('modal-confirm').click()
    cy.findByTestId('modal-confirm').click()
  }

  static resolveManuscriptEscalation({ deescalateType, checker }) {
    if (deescalateType === 'unpause') {
      cy.findByText('Unpause').click()
    } else if (deescalateType === 'returnToChecker') {
      cy.findByText(`Return to ${checker}`).click()
      cy.get('textarea').type('Type something here.')
    }
    cy.findByTestId('modal-confirm').click()
  }
  static conflictTypes = {
    coiWithTriageEditor: () => {
      cy.get('[type="checkbox"][value="triageEditor"]').click()
    },
    coiWithAcademicEditor: () => {
      cy.get('[type="checkbox"][value="academicEditor"]').click()
    },
    coiWithReviewer: () => {
      cy.get('[type="checkbox"][value="reviewers"]').click()
      cy.get('[class="ant-select-selector"]').click()
      cy.findByText('Reviewer 1 Sergiu').click()
    },
  }

  static sendToPeerReviewModal = {
    conflictOfInterest: (coiType) => {
      cy.findByText('Conflict of interest').click()
      coiType.forEach((element) => this.conflictTypes[element]())
    },
    decisionMadeInError: () => {
      cy.findByText('Decision made in error').click()
    },
    improperReview: () => {
      cy.findByText('Improper review').click()
      cy.get('[class="ant-select-selector"]').click()
      cy.findByText('Reviewer 1 Sergiu').click()
    },
  }

  static addCommentAndReturn(comment) {
    cy.get('textarea').type(comment)
    cy.findByText('RETURN TO PEER REVIEW').click()
  }
}
