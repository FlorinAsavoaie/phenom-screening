import { Given } from '@badeball/cypress-cucumber-preprocessor'
import { goTo } from '../../support/helpers/accessByPath'

Given(/^I'm on the settings tab$/, () => {
  goTo.settings()
})
