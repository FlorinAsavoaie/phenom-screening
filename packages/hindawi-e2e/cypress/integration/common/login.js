import { Given } from '@badeball/cypress-cucumber-preprocessor'

Given(/^I'm logged in as an Admin$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('adminEmail'),
    password: Cypress.env('password'),
  })
})

Given(/^I'm logged in as a Team Leader$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('teamLeaderEmail'),
    password: Cypress.env('password'),
  })
})

Given(/^I'm logged in as a checker$/, () => {
  cy.loginKeycloak({
    username: Cypress.env('firstCheckerEmail'),
    password: Cypress.env('password'),
  })
})
