Feature: Peer Review Cycle Check

    Background:
        Given I'm logged in as an Admin

    Scenario: Add peer review cycle checks

        Given a submitted manuscript

            | type            |
            | qualityChecking |

        When I select the following peer review cycle checks

            | checks                                                     | option |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | no     |
            | conflictOfInterestMention                                  | yes    |
            | dataAvailabilityStatementApproved                          | yes    |
            | editorialStaffsCommunicationIsAppropriate                  | yes    |
            | emailsReceivedFromAuthorsRequireAction                     | no     |
            | emailsReceivedFromEditorRequireAction                      | no     |
            | emailsReceivedFromReviewersRequireAction                   | no     |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | no     |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | no     |
            | otherConcernsOnReviewProcess                               | no     |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | yes    |

        Then the selected checks are available

            | checks                                                     | option |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | no     |
            | conflictOfInterestMention                                  | yes    |
            | dataAvailabilityStatementApproved                          | yes    |
            | editorialStaffsCommunicationIsAppropriate                  | yes    |
            | emailsReceivedFromAuthorsRequireAction                     | no     |
            | emailsReceivedFromEditorRequireAction                      | no     |
            | emailsReceivedFromReviewersRequireAction                   | no     |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | no     |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | no     |
            | otherConcernsOnReviewProcess                               | no     |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | yes    |

    Scenario: Add observations to peer review cycle checks

        Given a submitted manuscript with checks

            | checks                                                     | option |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    |
            | conflictOfInterestMention                                  | no     |
            | dataAvailabilityStatementApproved                          | no     |
            | editorialStaffsCommunicationIsAppropriate                  | no     |
            | emailsReceivedFromAuthorsRequireAction                     | yes    |
            | emailsReceivedFromEditorRequireAction                      | yes    |
            | emailsReceivedFromReviewersRequireAction                   | yes    |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    |
            | otherConcernsOnReviewProcess                               | yes    |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     |

        When I add observations to the checks

            | checks                                                     | option | observation                                                                   |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    | Some observation - anyConcernsRaisedByReviewReportsAndRecommendations         |
            | conflictOfInterestMention                                  | no     | Some observation - conflictOfInterestMention                                  |
            | dataAvailabilityStatementApproved                          | no     | Some observation - dataAvailabilityStatementApproved                          |
            | editorialStaffsCommunicationIsAppropriate                  | no     | Some observation - editorialStaffsCommunicationIsAppropriate                  |
            | emailsReceivedFromAuthorsRequireAction                     | yes    | Some observation - emailsReceivedFromAuthorsRequireAction                     |
            | emailsReceivedFromEditorRequireAction                      | yes    | Some observation - emailsReceivedFromEditorRequireAction                      |
            | emailsReceivedFromReviewersRequireAction                   | yes    | Some observation - emailsReceivedFromReviewersRequireAction                   |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    | Some observation - isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    | Some observation - isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors |
            | otherConcernsOnReviewProcess                               | yes    | Some observation - otherConcernsOnReviewProcess                               |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     | Some observation - reviewersIdentityConfirmedAndSeniortyLevelConfirmed        |

        Then the observations are available

            | checks                                                     | option | observation                                                                   |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    | Some observation - anyConcernsRaisedByReviewReportsAndRecommendations         |
            | conflictOfInterestMention                                  | no     | Some observation - conflictOfInterestMention                                  |
            | dataAvailabilityStatementApproved                          | no     | Some observation - dataAvailabilityStatementApproved                          |
            | editorialStaffsCommunicationIsAppropriate                  | no     | Some observation - editorialStaffsCommunicationIsAppropriate                  |
            | emailsReceivedFromAuthorsRequireAction                     | yes    | Some observation - emailsReceivedFromAuthorsRequireAction                     |
            | emailsReceivedFromEditorRequireAction                      | yes    | Some observation - emailsReceivedFromEditorRequireAction                      |
            | emailsReceivedFromReviewersRequireAction                   | yes    | Some observation - emailsReceivedFromReviewersRequireAction                   |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    | Some observation - isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    | Some observation - isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors |
            | otherConcernsOnReviewProcess                               | yes    | Some observation - otherConcernsOnReviewProcess                               |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     | Some observation - reviewersIdentityConfirmedAndSeniortyLevelConfirmed        |

    Scenario: Edit observations on the peer review cycle checks

        Given a submitted manuscript with checks and observations

            | checks                                                     | option | observation                                                                  |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    | Old observation - anyConcernsRaisedByReviewReportsAndRecommendations         |
            | conflictOfInterestMention                                  | no     | Old observation - conflictOfInterestMention                                  |
            | dataAvailabilityStatementApproved                          | no     | Old observation - dataAvailabilityStatementApproved                          |
            | editorialStaffsCommunicationIsAppropriate                  | no     | Old observation - editorialStaffsCommunicationIsAppropriate                  |
            | emailsReceivedFromAuthorsRequireAction                     | yes    | Old observation - emailsReceivedFromAuthorsRequireAction                     |
            | emailsReceivedFromEditorRequireAction                      | yes    | Old observation - emailsReceivedFromEditorRequireAction                      |
            | emailsReceivedFromReviewersRequireAction                   | yes    | Old observation - emailsReceivedFromReviewersRequireAction                   |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    | Old observation - isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    | Old observation - isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors |
            | otherConcernsOnReviewProcess                               | yes    | Old observation - otherConcernsOnReviewProcess                               |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     | Old observation - reviewersIdentityConfirmedAndSeniortyLevelConfirmed        |

        When I edit the observations of the checks

            | checks                                                     | option | observation                                                                  |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    | New observation - anyConcernsRaisedByReviewReportsAndRecommendations         |
            | conflictOfInterestMention                                  | no     | New observation - conflictOfInterestMention                                  |
            | dataAvailabilityStatementApproved                          | no     | New observation - dataAvailabilityStatementApproved                          |
            | editorialStaffsCommunicationIsAppropriate                  | no     | New observation - editorialStaffsCommunicationIsAppropriate                  |
            | emailsReceivedFromAuthorsRequireAction                     | yes    | New observation - emailsReceivedFromAuthorsRequireAction                     |
            | emailsReceivedFromEditorRequireAction                      | yes    | New observation - emailsReceivedFromEditorRequireAction                      |
            | emailsReceivedFromReviewersRequireAction                   | yes    | New observation - emailsReceivedFromReviewersRequireAction observation       |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    | New observation - isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    | New observation - isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors |
            | otherConcernsOnReviewProcess                               | yes    | New observation - otherConcernsOnReviewProcess                               |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     | New observation - reviewersIdentityConfirmedAndSeniortyLevelConfirmed        |

        Then the new observations are available

            | checks                                                     | option | observation                                                                  |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    | New observation - anyConcernsRaisedByReviewReportsAndRecommendations         |
            | conflictOfInterestMention                                  | no     | New observation - conflictOfInterestMention                                  |
            | dataAvailabilityStatementApproved                          | no     | New observation - dataAvailabilityStatementApproved                          |
            | editorialStaffsCommunicationIsAppropriate                  | no     | New observation - editorialStaffsCommunicationIsAppropriate                  |
            | emailsReceivedFromAuthorsRequireAction                     | yes    | New observation - emailsReceivedFromAuthorsRequireAction                     |
            | emailsReceivedFromEditorRequireAction                      | yes    | New observation - emailsReceivedFromEditorRequireAction                      |
            | emailsReceivedFromReviewersRequireAction                   | yes    | New observation - emailsReceivedFromReviewersRequireAction observation       |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    | New observation - isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    | New observation - isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors |
            | otherConcernsOnReviewProcess                               | yes    | New observation - otherConcernsOnReviewProcess                               |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     | New observation - reviewersIdentityConfirmedAndSeniortyLevelConfirmed        |

    Scenario: Delete observations on the peer review cycle checks

        Given a submitted manuscript with the following checks and observations

            | checks                                                     | option | observation                                                                   |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    | Some observation - anyConcernsRaisedByReviewReportsAndRecommendations         |
            | conflictOfInterestMention                                  | no     | Some observation - conflictOfInterestMention                                  |
            | dataAvailabilityStatementApproved                          | no     | Some observation - dataAvailabilityStatementApproved                          |
            | editorialStaffsCommunicationIsAppropriate                  | no     | Some observation - editorialStaffsCommunicationIsAppropriate                  |
            | emailsReceivedFromAuthorsRequireAction                     | yes    | Some observation - emailsReceivedFromAuthorsRequireAction                     |
            | emailsReceivedFromEditorRequireAction                      | yes    | Some observation - emailsReceivedFromEditorRequireAction                      |
            | emailsReceivedFromReviewersRequireAction                   | yes    | Some observation - emailsReceivedFromReviewersRequireAction                   |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    | Some observation - isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    | Some observation - isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors |
            | otherConcernsOnReviewProcess                               | yes    | Some observation - otherConcernsOnReviewProcess                               |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     | Some observation - reviewersIdentityConfirmedAndSeniortyLevelConfirmed        |

        When I delete the observations of the checks

            | checks                                                     | option | observation |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    |             |
            | conflictOfInterestMention                                  | no     |             |
            | dataAvailabilityStatementApproved                          | no     |             |
            | editorialStaffsCommunicationIsAppropriate                  | no     |             |
            | emailsReceivedFromAuthorsRequireAction                     | yes    |             |
            | emailsReceivedFromEditorRequireAction                      | yes    |             |
            | emailsReceivedFromReviewersRequireAction                   | yes    |             |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    |             |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    |             |
            | otherConcernsOnReviewProcess                               | yes    |             |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     |             |

        Then the observations are no longer available

            | checks                                                     | option | observation |
            | anyConcernsRaisedByReviewReportsAndRecommendations         | yes    |             |
            | conflictOfInterestMention                                  | no     |             |
            | dataAvailabilityStatementApproved                          | no     |             |
            | editorialStaffsCommunicationIsAppropriate                  | no     |             |
            | emailsReceivedFromAuthorsRequireAction                     | yes    |             |
            | emailsReceivedFromEditorRequireAction                      | yes    |             |
            | emailsReceivedFromReviewersRequireAction                   | yes    |             |
            | isThereAConflictOfInterestBetweenTheEditorAndTheAuthors    | yes    |             |
            | isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors | yes    |             |
            | otherConcernsOnReviewProcess                               | yes    |             |
            | reviewersIdentityConfirmedAndSeniortyLevelConfirmed        | no     |             |