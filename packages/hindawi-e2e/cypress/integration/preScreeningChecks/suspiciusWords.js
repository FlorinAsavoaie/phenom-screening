import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Manuscript } from '../common/graphql/manuscript'

let actualResultResponse

When(/^I verify the suspicious words$/, () => {
  cy.getToken().then((token) => {
    cy.get('@manuscriptId')
      .then((manuscriptId) => {
        Manuscript.getManuscriptChecks({ token, manuscriptId })
      })
      .then((response) => {
        actualResultResponse =
          response.body.data.getManuscriptChecks.suspiciousWords
            .newSuspiciousWords
      })
  })
})

Then(
  /^the expected suspicious words, their occurrence number and reason are available$/,
  (expectedResult) => {
    const actualResult = actualResultResponse
      .map((row) => ({
        word: row.word,
        noOfOccurrences: row.noOfOccurrences.toString(),
        reason: row.reason,
      }))
      .sort((a, b) => (a.word > b.word ? 1 : -1))

    expect(actualResult).to.deep.equal(expectedResult.hashes())
  },
)
