export const getSimilarSubmissions = `
  query getSimilarSubmissions($manuscriptId: ID!) {
    similarSubmissions: getSimilarSubmissions(manuscriptId: $manuscriptId) {
        id
        title
        flowType
        customId
        articleType
        submissionDate
        status
        submittingAuthor {
          givenNames
          surname
        }
        journal {
          id
          name
        }
        journalSection {
          id
          name
        }
        specialIssue {
          id
          name
        }
        assignedChecker {
          id
          givenNames
          surname
        }
        authors {
          id
          surname
          givenNames
          isSubmitting
        }
    }
  }
`
