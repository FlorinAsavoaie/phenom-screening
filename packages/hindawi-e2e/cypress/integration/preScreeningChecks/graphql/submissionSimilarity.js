import { graphqlService } from '../../../support/graphqlService'
import { getSimilarSubmissions } from './queries'

export class SubmissionSimilarity {
  static get({ token, manuscriptId }) {
    graphqlService({
      query: getSimilarSubmissions,
      token,
      variables: { manuscriptId },
    })
  }
}
