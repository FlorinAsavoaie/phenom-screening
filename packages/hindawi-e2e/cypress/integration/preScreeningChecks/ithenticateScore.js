import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Manuscript } from '../common/graphql/manuscript'

let actualResultResponse

When(/^I verify the iThenticate score$/, () => {
  cy.getToken().then((token) => {
    cy.get('@manuscriptId')
      .then((manuscriptId) => {
        Manuscript.getManuscriptChecks({ token, manuscriptId })
      })
      .then((response) => {
        actualResultResponse = response.body.data.getManuscriptChecks
      })
  })
})

Then(/^the expected similarity score is available$/, () => {
  const actualResult = {
    firstSourceSimilarityPercentage:
      actualResultResponse.firstSourceSimilarityPercentage,
    totalSimilarityPercentage: actualResultResponse.totalSimilarityPercentage,
  }

  // Since the iThenticate service is disabled on the automation
  // environment, the expectation is to have a null result.
  expect(actualResult.firstSourceSimilarityPercentage).to.be.null
  expect(actualResult.totalSimilarityPercentage).to.be.null
})
