import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { SubmissionSimilarity } from './graphql/submissionSimilarity'

const Chance = require('chance')

const chance = new Chance()

let actualResult

const firstAuthor = `${Cypress.env('baseEmail')}+${chance.string({
  length: 6,
})}@hindawi.com`
const secondAuthor = `${Cypress.env('baseEmail')}+${chance.string({
  length: 6,
})}@hindawi.com`
const manuscriptTitle = `This manuscript is similar ${chance.string({
  length: 6,
})}`

Given(/^two similar submissions$/, () => {
  cy.submitManuscript({
    type: 'screening',
    title: manuscriptTitle,
    authors: [firstAuthor, secondAuthor],
  }).then(() => {
    cy.submitManuscript({
      type: 'screening',
      title: manuscriptTitle,
      authors: [firstAuthor, secondAuthor],
    })
  })
})

When(/^I submit another similar submission$/, () => {
  cy.submitManuscript({
    type: 'screening',
    title: manuscriptTitle,
    authors: [firstAuthor, secondAuthor],
  })

  cy.getToken().then((token) => {
    cy.get('@manuscriptId')
      .then((manuscriptId) => {
        SubmissionSimilarity.get({ token, manuscriptId })
      })
      .then((response) => {
        actualResult = response.body.data.similarSubmissions
      })
  })
})

Then(/^two matches will be found for that submission$/, () => {
  const expectedResult = 2

  expect(actualResult.length).to.equal(expectedResult)
})
