import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { goTo } from '../../support/helpers/accessByPath'

When(/^I do the pre-screening checks$/, () => {
  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })
})
Then(/^the number of words should be (.\d+)$/, (expectedTotalWords) => {
  cy.get(`[label = 'Language & Wordcount']  span:nth-child(2)`).should(
    'have.text',
    expectedTotalWords,
  )
})
Then(/^the language should be (ENGLISH|NOT ENGLISH)$/, (expectedLanguage) => {
  cy.get(`[label='Language & Wordcount'] span:nth-child(3)`).should(
    'have.text',
    expectedLanguage,
  )
})
When(
  /^the percentage of english words should be (.*)$/,
  (expectedPercentOfEnglish) => {
    cy.findByText('Language & Wordcount').click()
    cy.get(
      `[label='Language & Wordcount'] div:nth-child(2) span:nth-child(2)`,
    ).should('have.text', expectedPercentOfEnglish)
  },
)
