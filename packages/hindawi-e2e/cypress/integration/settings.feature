Feature: Settings
    Using this feature you can add authors on a sanction list. Once the author is on one of the lists, every time he/she submits a manuscript, the author verification system will add a label to the author with the appropiate flag.

    Background:
        Given I'm logged in as an Admin

    Scenario: Adding an author to the sanction list

        Given I'm on the settings tab
        When I add an author to the sanction list
        Then the author can be found in the list of sanctioned authors

    Scenario: Editing an author from the sanction list

        Given a sanctioned author for editing
        When I edit the sanction details
        Then I see the changes reflected in the Sanction List

    Scenario: Deleting an author from the sanction list

        Given a sanctioned author for deleting
        When I delete the author from the Sanction List
        Then I can't find the author in the the Sanction List anymore

    Scenario: Adding a keyword to the suspicious keywords list

        Given I'm on the settings tab
        When I add a keyword to the suspicious keywords list
        Then the keyword can be found in the list of suspicious keywords

    Scenario: Editing the reason for a keyword from the suspicious keywords list

        Given a suspicious keyword for editing
        When I edit the reason for the keyword
        Then I see the changes reflected in the Suspicious Keywords List

    Scenario: Deleting a keyword from the suspicious keywords list

        Given a suspicious keyword for deleting
        When I delete the keyword
        Then I can't find the keyword in the the Suspicious Keywords List anymore
    @ignore
    Scenario: Exporting the suspicious keywords list csv

        Given a list of suspicious keywords
        When I export the list
        Then the suspicious keywords list is downloaded on my machine