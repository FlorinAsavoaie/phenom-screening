Feature: Manuscript escalation
    Background:
        Given I'm logged in as a checker

    Scenario Outline: Pausing and unpausing a manuscript as a checker

        Given a submitted manuscript
            | type             |
            | <manuscriptType> |
        When I pause the manuscript
        Then I see the manuscript in the Paused status
        And I can unpause it

        Examples:
            | manuscriptType  |
            | screening       |
            | qualityChecking |

    Scenario Outline: Sending a manuscript to the team leader and then returning it back to the checker

        Given a submitted manuscript
            | type             |
            | <manuscriptType> |
        When I send the manuscript to the team leader
        Then the team leader can see the manuscript in the Escalated filter having the Escalated status
        And the manuscript can be returned to the <checker>

        Examples:
            | manuscriptType  | checker  |
            | screening       | Screener |
            | qualityChecking | Checker  |

    Scenario Outline: Sending a manuscript to peer review for conflict of interest

        Given a submitted manuscript
            | type            |
            | qualityChecking |
        When I send the manuscript to peer review for <typeOfConflict> reason
        Then I see the manuscript with conflict of interest in the Sent to Peer Review status

        Examples:
            | typeOfConflict        |
            | coiWithTriageEditor   |
            | coiWithAcademicEditor |
            | coiWithReviewer       |

    Scenario: Sending a manuscript to peer review for a decision made in error

        Given a submitted manuscript
            | type            |
            | qualityChecking |
        When I send the manuscript to peer review for decision made in error
        Then I see the decision made in error manuscript in the Sent to Peer Review status

    Scenario: Sending a manuscript to peer review for improper review

        Given a submitted manuscript
            | type            |
            | qualityChecking |
        When I send the manuscript to peer review for improper review
        Then I see the manuscript with improper review in the Sent to Peer Review status

    Scenario: Sending a manuscript to peer review for multiple reasons

        Given a submitted manuscript
            | type            |
            | qualityChecking |
        When I send the manuscript to peer review for multiple reasons
        Then I see the manuscript with multiple problems in the Sent to Peer Review status