Feature: Manuscript assignation
    Background:
        Given I'm logged in as a Team Leader

    Scenario Outline: Assigning a manuscript to a checker as an admin

        Given a submitted manuscript
            | type             | journal                   |
            | <manuscriptType> | journalIdMultipleCheckers |
        When I assign the manuscript to <checkerName>
        Then <checkerEmail> has access to the manuscript and is able to make a decision on it

        Examples:
            | manuscriptType  | checkerName        | checkerEmail                        |
            | screening       | Mihai Checkerovici | auto.screening+checker2@hindawi.com |
            | qualityChecking | Dan Checkerescu    | auto.screening+checker3@hindawi.com |