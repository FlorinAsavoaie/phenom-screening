Feature: User management

    Background:
        Given I'm logged in as an Admin
    Scenario Outline: Admin creates a team

        Given I am on the User management section
        When I create a <teamType> team called <teamName> with the following members and the <journalName> assigned

            | First Name | Last Name | Email                               | Role      |
            | Stefan     | Leaderan  | auto.screening+tl@hindawi.com       | Team Lead |
            | Vasile     | Checkeras | auto.screening+checker1@hindawi.com | <Checker> |

        Then I see the <teamName> team in the list having <numberOfMembers> members and the <journalName> assigned

        Examples:
            | teamName  | teamType        | numberOfMembers | journalName                   | Checker         |
            | Screening | screening       | 2               | [Test] Journal with 1 checker | Screener        |
            | QC        | qualityChecking | 2               | [Test] Journal with 1 checker | Quality Checker |

    Scenario Outline: Admin edits a team

        Given I have a <teamType> team for editing called <teamName>
        When I edit the team name
        And I add another <checker>
        And I change the journal
        Then I see the changes in the Teams tab

        Examples:
            | teamType        | teamName  | checker         |
            | screening       | Screening | Screener        |
            | qualityChecking | QC        | Quality Checker |