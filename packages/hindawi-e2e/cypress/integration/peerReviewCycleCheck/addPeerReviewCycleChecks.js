import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { Option } from './graphql/option'

let token
let manuscriptId

When(/^I select the following peer review cycle checks$/, (table) => {
  cy.getToken().then((accessToken) => (token = accessToken))

  cy.get('@manuscriptId')
    .then((id) => (manuscriptId = id))
    .then(() => {
      table.hashes().forEach((element) => {
        Option.add({
          token,
          manuscriptId,
          name: element.checks,
          selectedOption: element.option,
        })
      })
    })
})

Then(/^the selected checks are available$/, (expectedResult) => {
  Option.get({ token, manuscriptId }).then((response) => {
    const checksResponse = response.body.data.checks

    const actualResult = checksResponse
      .map((check) => ({
        checks: check.name,
        option: check.selectedOption,
      }))
      .sort((a, b) => (a.checks > b.checks ? 1 : -1))

    expect(actualResult).to.deep.equal(expectedResult.hashes())
  })
})
