export const selectOptionForPeerReviewCheck = `
  mutation selectOptionForPeerReviewCheck(
    $input: SelectOptionForPeerReviewCheckInput
  ) {
    peerReviewCheck: selectOptionForPeerReviewCheck(input: $input) {
      selectedOption
      observation
    }
  }
`

export const updateObservationForPeerReviewCheck = `mutation updateObservationForPeerReviewCheck ($input: UpdateObservationForPeerReviewCheck) {
    updateObservationForPeerReviewCheck (input: $input) {
        id
        created
        updated
        name
        selectedOption
        observation
    }
}`

export const approvePeerReviewCycleMutation = `mutation approvePeerReviewCycle($manuscriptId: String!) {
    manuscript: approvePeerReviewCycle(manuscriptId: $manuscriptId) {
      step
    }
  }
`
