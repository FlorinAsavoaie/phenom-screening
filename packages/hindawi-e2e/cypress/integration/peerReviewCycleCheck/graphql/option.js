import { graphqlService } from '../../../support/graphqlService'
import {
  selectOptionForPeerReviewCheck,
  updateObservationForPeerReviewCheck,
} from './mutations'
import { getManuscriptPeerReviewChecks } from './queries'

export class Option {
  static add({ token, manuscriptId, name, selectedOption }) {
    graphqlService({
      query: selectOptionForPeerReviewCheck,
      token,
      variables: { input: { manuscriptId, name, selectedOption } },
    })
  }

  static updateObservation({ token, manuscriptId, name, observation }) {
    graphqlService({
      query: updateObservationForPeerReviewCheck,
      token,
      variables: { input: { manuscriptId, name, observation } },
    })
  }

  static get({ token, manuscriptId }) {
    return graphqlService({
      query: getManuscriptPeerReviewChecks,
      token,
      variables: { manuscriptId },
    })
  }
}
