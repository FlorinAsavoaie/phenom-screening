export const getManuscriptPeerReviewChecks = `
  query getManuscriptPeerReviewChecks($manuscriptId: ID!) {
    checks: getManuscriptPeerReviewChecks(manuscriptId: $manuscriptId) {
      name
      selectedOption
      observation
      config {
        options {
          value
          withObservation
        }
        order
      }
    }
  }
`
