import { graphqlService } from '../../../support/graphqlService'
import { approvePeerReviewCycleMutation } from './mutations'

export class Decision {
  static approvePeerReviewCycleCheck({ token, manuscriptId }) {
    graphqlService({
      query: approvePeerReviewCycleMutation,
      token,
      variables: { manuscriptId },
    })
  }
}
