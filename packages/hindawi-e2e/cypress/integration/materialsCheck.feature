Feature: Materials Check
    Background:
        Given I'm logged in as an Admin

    Scenario: Add material checks with the 'Yes' option selected

        Given a submitted manuscript in the materials check step

        When I select the 'Yes' option for the following material checks

            | checks                                   | option |
            | allSectionsPresentAndApproved            | yes    |
            | arXivID                                  | yes    |
            | clinicalStudyRegistrationNumberApproved  | yes    |
            | coverLetterApproved                      | yes    |
            | editableFigureCaptions                   | yes    |
            | figuresAndTablesPresentInPDFAndAllCited  | yes    |
            | figuresProvidedInEditableFormat          | yes    |
            | frontPageApproved                        | yes    |
            | fundingStatementApproved                 | yes    |
            | hasAcknowledgementsApproved              | yes    |
            | hasFileInEditableFormat                  | yes    |
            | hasPatientParticipantConsentFormApproved | yes    |
            | headersAndFootersApproved                | yes    |
            | manuscriptFigureFiles                    | no     |
            | supplementaryMaterialsApproved           | yes    |

        Then the material checks have the 'Yes' option selected

            | checks                                   | option |
            | allSectionsPresentAndApproved            | yes    |
            | arXivID                                  | yes    |
            | clinicalStudyRegistrationNumberApproved  | yes    |
            | coverLetterApproved                      | yes    |
            | editableFigureCaptions                   | yes    |
            | figuresAndTablesPresentInPDFAndAllCited  | yes    |
            | figuresProvidedInEditableFormat          | yes    |
            | frontPageApproved                        | yes    |
            | fundingStatementApproved                 | yes    |
            | hasAcknowledgementsApproved              | yes    |
            | hasFileInEditableFormat                  | yes    |
            | hasPatientParticipantConsentFormApproved | yes    |
            | headersAndFootersApproved                | yes    |
            | manuscriptFigureFiles                    | no     |
            | supplementaryMaterialsApproved           | yes    |

    Scenario: Add material checks with the 'Not Required' option selected

        Given a submitted manuscript that passed the peer review cycle checks

        When I select the 'Not Required' option for the following material checks

            | checks                                   | option      |
            | allSectionsPresentAndApproved            | notRequired |
            | arXivID                                  | notRequired |
            | clinicalStudyRegistrationNumberApproved  | notRequired |
            | coverLetterApproved                      | notRequired |
            | editableFigureCaptions                   | notRequired |
            | figuresAndTablesPresentInPDFAndAllCited  | notRequired |
            | figuresProvidedInEditableFormat          | notRequired |
            | frontPageApproved                        | notRequired |
            | fundingStatementApproved                 | notRequired |
            | hasAcknowledgementsApproved              | notRequired |
            | hasFileInEditableFormat                  | notRequired |
            | hasPatientParticipantConsentFormApproved | notRequired |
            | headersAndFootersApproved                | yes         |
            | manuscriptFigureFiles                    | no          |
            | supplementaryMaterialsApproved           | notRequired |

        Then the material checks have the 'Not Required' option selected

            | checks                                   | option      |
            | allSectionsPresentAndApproved            | notRequired |
            | arXivID                                  | notRequired |
            | clinicalStudyRegistrationNumberApproved  | notRequired |
            | coverLetterApproved                      | notRequired |
            | editableFigureCaptions                   | notRequired |
            | figuresAndTablesPresentInPDFAndAllCited  | notRequired |
            | figuresProvidedInEditableFormat          | notRequired |
            | frontPageApproved                        | notRequired |
            | fundingStatementApproved                 | notRequired |
            | hasAcknowledgementsApproved              | notRequired |
            | hasFileInEditableFormat                  | notRequired |
            | hasPatientParticipantConsentFormApproved | notRequired |
            | headersAndFootersApproved                | yes         |
            | manuscriptFigureFiles                    | no          |
            | supplementaryMaterialsApproved           | notRequired |

    Scenario: Add observations to material checks

        Given a submitted manuscript with material checks having the 'No' option selected

            | checks                                   | option |
            | allSectionsPresentAndApproved            | no     |
            | arXivID                                  | no     |
            | clinicalStudyRegistrationNumberApproved  | no     |
            | coverLetterApproved                      | no     |
            | editableFigureCaptions                   | no     |
            | figuresAndTablesPresentInPDFAndAllCited  | no     |
            | figuresProvidedInEditableFormat          | no     |
            | frontPageApproved                        | no     |
            | fundingStatementApproved                 | no     |
            | hasAcknowledgementsApproved              | no     |
            | hasFileInEditableFormat                  | no     |
            | hasPatientParticipantConsentFormApproved | no     |
            | headersAndFootersApproved                | no     |
            | manuscriptFigureFiles                    | yes    |
            | supplementaryMaterialsApproved           | no     |

        When I add observations to the material checks

            | checks                                   | option | observation                                                 |
            | allSectionsPresentAndApproved            | no     | Some observation - allSectionsPresentAndApproved            |
            | arXivID                                  | no     | Some observation - arXivID                                  |
            | clinicalStudyRegistrationNumberApproved  | no     | Some observation - clinicalStudyRegistrationNumberApproved  |
            | coverLetterApproved                      | no     | Some observation - coverLetterApproved                      |
            | editableFigureCaptions                   | no     | Some observation - editableFigureCaptions                   |
            | figuresAndTablesPresentInPDFAndAllCited  | no     | Some observation - figuresAndTablesPresentInPDFAndAllCited  |
            | figuresProvidedInEditableFormat          | no     | Some observation - figuresProvidedInEditableFormat          |
            | frontPageApproved                        | no     | Some observation - frontPageApproved                        |
            | fundingStatementApproved                 | no     | Some observation - fundingStatementApproved                 |
            | hasAcknowledgementsApproved              | no     | Some observation - hasAcknowledgementsApproved              |
            | hasFileInEditableFormat                  | no     | Some observation - hasFileInEditableFormat                  |
            | hasPatientParticipantConsentFormApproved | no     | Some observation - hasPatientParticipantConsentFormApproved |
            | headersAndFootersApproved                | no     | Some observation - headersAndFootersApproved                |
            | manuscriptFigureFiles                    | yes    | 12                                                          |
            | supplementaryMaterialsApproved           | no     | Some observation - supplementaryMaterialsApproved           |

        Then the material checks observations are available

            | checks                                   | option | observation                                                 |
            | allSectionsPresentAndApproved            | no     | Some observation - allSectionsPresentAndApproved            |
            | arXivID                                  | no     | Some observation - arXivID                                  |
            | clinicalStudyRegistrationNumberApproved  | no     | Some observation - clinicalStudyRegistrationNumberApproved  |
            | coverLetterApproved                      | no     | Some observation - coverLetterApproved                      |
            | editableFigureCaptions                   | no     | Some observation - editableFigureCaptions                   |
            | figuresAndTablesPresentInPDFAndAllCited  | no     | Some observation - figuresAndTablesPresentInPDFAndAllCited  |
            | figuresProvidedInEditableFormat          | no     | Some observation - figuresProvidedInEditableFormat          |
            | frontPageApproved                        | no     | Some observation - frontPageApproved                        |
            | fundingStatementApproved                 | no     | Some observation - fundingStatementApproved                 |
            | hasAcknowledgementsApproved              | no     | Some observation - hasAcknowledgementsApproved              |
            | hasFileInEditableFormat                  | no     | Some observation - hasFileInEditableFormat                  |
            | hasPatientParticipantConsentFormApproved | no     | Some observation - hasPatientParticipantConsentFormApproved |
            | headersAndFootersApproved                | no     | Some observation - headersAndFootersApproved                |
            | manuscriptFigureFiles                    | yes    | 12                                                          |
            | supplementaryMaterialsApproved           | no     | Some observation - supplementaryMaterialsApproved           |

    Scenario: Edit observations of material checks

        Given a submitted manuscript with material checks and observations

            | checks                                   | option | observation                                                |
            | allSectionsPresentAndApproved            | no     | Old observation - allSectionsPresentAndApproved            |
            | arXivID                                  | no     | Old observation - arXivID                                  |
            | clinicalStudyRegistrationNumberApproved  | no     | Old observation - clinicalStudyRegistrationNumberApproved  |
            | coverLetterApproved                      | no     | Old observation - coverLetterApproved                      |
            | editableFigureCaptions                   | no     | Old observation - editableFigureCaptions                   |
            | figuresAndTablesPresentInPDFAndAllCited  | no     | Old observation - figuresAndTablesPresentInPDFAndAllCited  |
            | figuresProvidedInEditableFormat          | no     | Old observation - figuresProvidedInEditableFormat          |
            | frontPageApproved                        | no     | Old observation - frontPageApproved                        |
            | fundingStatementApproved                 | no     | Old observation - fundingStatementApproved                 |
            | hasAcknowledgementsApproved              | no     | Old observation - hasAcknowledgementsApproved              |
            | hasFileInEditableFormat                  | no     | Old observation - hasFileInEditableFormat                  |
            | hasPatientParticipantConsentFormApproved | no     | Old observation - hasPatientParticipantConsentFormApproved |
            | headersAndFootersApproved                | no     | Old observation - headersAndFootersApproved                |
            | manuscriptFigureFiles                    | yes    | 10                                                         |
            | supplementaryMaterialsApproved           | no     | Old observation - supplementaryMaterialsApproved           |

        When I update the observations of the material checks

            | checks                                   | option | observation                                                |
            | allSectionsPresentAndApproved            | no     | New observation - allSectionsPresentAndApproved            |
            | arXivID                                  | no     | New observation - arXivID                                  |
            | clinicalStudyRegistrationNumberApproved  | no     | New observation - clinicalStudyRegistrationNumberApproved  |
            | coverLetterApproved                      | no     | New observation - coverLetterApproved                      |
            | editableFigureCaptions                   | no     | New observation - editableFigureCaptions                   |
            | figuresAndTablesPresentInPDFAndAllCited  | no     | New observation - figuresAndTablesPresentInPDFAndAllCited  |
            | figuresProvidedInEditableFormat          | no     | New observation - figuresProvidedInEditableFormat          |
            | frontPageApproved                        | no     | New observation - frontPageApproved                        |
            | fundingStatementApproved                 | no     | New observation - fundingStatementApproved                 |
            | hasAcknowledgementsApproved              | no     | New observation - hasAcknowledgementsApproved              |
            | hasFileInEditableFormat                  | no     | New observation - hasFileInEditableFormat                  |
            | hasPatientParticipantConsentFormApproved | no     | New observation - hasPatientParticipantConsentFormApproved |
            | headersAndFootersApproved                | no     | New observation - headersAndFootersApproved                |
            | manuscriptFigureFiles                    | yes    | 20                                                         |
            | supplementaryMaterialsApproved           | no     | New observation - supplementaryMaterialsApproved           |

        Then the new material checks observations are available

            | checks                                   | option | observation                                                |
            | allSectionsPresentAndApproved            | no     | New observation - allSectionsPresentAndApproved            |
            | arXivID                                  | no     | New observation - arXivID                                  |
            | clinicalStudyRegistrationNumberApproved  | no     | New observation - clinicalStudyRegistrationNumberApproved  |
            | coverLetterApproved                      | no     | New observation - coverLetterApproved                      |
            | editableFigureCaptions                   | no     | New observation - editableFigureCaptions                   |
            | figuresAndTablesPresentInPDFAndAllCited  | no     | New observation - figuresAndTablesPresentInPDFAndAllCited  |
            | figuresProvidedInEditableFormat          | no     | New observation - figuresProvidedInEditableFormat          |
            | frontPageApproved                        | no     | New observation - frontPageApproved                        |
            | fundingStatementApproved                 | no     | New observation - fundingStatementApproved                 |
            | hasAcknowledgementsApproved              | no     | New observation - hasAcknowledgementsApproved              |
            | hasFileInEditableFormat                  | no     | New observation - hasFileInEditableFormat                  |
            | hasPatientParticipantConsentFormApproved | no     | New observation - hasPatientParticipantConsentFormApproved |
            | headersAndFootersApproved                | no     | New observation - headersAndFootersApproved                |
            | manuscriptFigureFiles                    | yes    | 20                                                         |
            | supplementaryMaterialsApproved           | no     | New observation - supplementaryMaterialsApproved           |

    Scenario: Delete observations of material checks

        Given a submitted manuscript with the following material checks and observations

            | checks                                   | option | observation                                                 |
            | allSectionsPresentAndApproved            | no     | Some observation - allSectionsPresentAndApproved            |
            | arXivID                                  | no     | Some observation - arXivID                                  |
            | clinicalStudyRegistrationNumberApproved  | no     | Some observation - clinicalStudyRegistrationNumberApproved  |
            | coverLetterApproved                      | no     | Some observation - coverLetterApproved                      |
            | editableFigureCaptions                   | no     | Some observation - editableFigureCaptions                   |
            | figuresAndTablesPresentInPDFAndAllCited  | no     | Some observation - figuresAndTablesPresentInPDFAndAllCited  |
            | figuresProvidedInEditableFormat          | no     | Some observation - figuresProvidedInEditableFormat          |
            | frontPageApproved                        | no     | Some observation - frontPageApproved                        |
            | fundingStatementApproved                 | no     | Some observation - fundingStatementApproved                 |
            | hasAcknowledgementsApproved              | no     | Some observation - hasAcknowledgementsApproved              |
            | hasFileInEditableFormat                  | no     | Some observation - hasFileInEditableFormat                  |
            | hasPatientParticipantConsentFormApproved | no     | Some observation - hasPatientParticipantConsentFormApproved |
            | headersAndFootersApproved                | no     | Some observation - headersAndFootersApproved                |
            | manuscriptFigureFiles                    | yes    | 12                                                          |
            | supplementaryMaterialsApproved           | no     | Some observation - supplementaryMaterialsApproved           |

        When I delete the observations of the material checks

            | checks                                   | option | observation |
            | allSectionsPresentAndApproved            | no     |             |
            | arXivID                                  | no     |             |
            | clinicalStudyRegistrationNumberApproved  | no     |             |
            | coverLetterApproved                      | no     |             |
            | editableFigureCaptions                   | no     |             |
            | figuresAndTablesPresentInPDFAndAllCited  | no     |             |
            | figuresProvidedInEditableFormat          | no     |             |
            | frontPageApproved                        | no     |             |
            | fundingStatementApproved                 | no     |             |
            | hasAcknowledgementsApproved              | no     |             |
            | hasFileInEditableFormat                  | no     |             |
            | hasPatientParticipantConsentFormApproved | no     |             |
            | headersAndFootersApproved                | no     |             |
            | manuscriptFigureFiles                    | yes    | 0           |
            | supplementaryMaterialsApproved           | no     |             |

        Then the observations are no longer available

            | checks                                   | option | observation |
            | allSectionsPresentAndApproved            | no     |             |
            | arXivID                                  | no     |             |
            | clinicalStudyRegistrationNumberApproved  | no     |             |
            | coverLetterApproved                      | no     |             |
            | editableFigureCaptions                   | no     |             |
            | figuresAndTablesPresentInPDFAndAllCited  | no     |             |
            | figuresProvidedInEditableFormat          | no     |             |
            | frontPageApproved                        | no     |             |
            | fundingStatementApproved                 | no     |             |
            | hasAcknowledgementsApproved              | no     |             |
            | hasFileInEditableFormat                  | no     |             |
            | hasPatientParticipantConsentFormApproved | no     |             |
            | headersAndFootersApproved                | no     |             |
            | manuscriptFigureFiles                    | yes    | 0           |
            | supplementaryMaterialsApproved           | no     |             |