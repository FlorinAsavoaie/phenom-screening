import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { DashboardPage } from '../common/pageObjects/dashboardPage'
import { goTo } from '../../support/helpers/accessByPath'

When(
  /^I log in with the assigned checker and (.*) the manuscript$/,
  (decisionType) => {
    cy.loginKeycloak({
      username: Cypress.env('firstCheckerEmail'),
      password: Cypress.env('password'),
    })

    cy.get('@manuscriptId').then((id) => {
      goTo.manuscript(id)
    })

    ManuscriptPage.makeDecision({ decisionType })
  },
)

Then(
  /^I can find it in the (.*) filter with the (.*) status$/,
  (dashboardFilter, statusType) => {
    DashboardPage.applyFilter(dashboardFilter)

    cy.get('@submissionParam').then((submission) => {
      DashboardPage.searchForManuscript(submission.title)
    })

    cy.findByText(statusType).should('exist')
  },
)
