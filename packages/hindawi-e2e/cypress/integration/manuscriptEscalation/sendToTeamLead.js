import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { DashboardPage } from '../common/pageObjects/dashboardPage'
import { goTo } from '../../support/helpers/accessByPath'

When(/^I send the manuscript to the team leader$/, () => {
  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  ManuscriptPage.escalateManuscript({ escalationType: 'sendToTeamLead' })
})

Then(
  /^the team leader can see the manuscript in the Escalated filter having the Escalated status$/,
  () => {
    cy.logoutKeycloak()
    cy.reload()

    cy.loginKeycloak({
      username: Cypress.env('teamLeaderEmail'),
      password: Cypress.env('password'),
    })

    cy.reload()

    DashboardPage.applyFilter('escalated')

    cy.get('@submissionParam').then((submission) => {
      DashboardPage.searchForManuscript(submission.title)
    })

    cy.findByTestId('status-label').contains('Escalated').should('exist')
  },
)

When(/^the manuscript can be returned to the (.*)$/, (checker) => {
  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  ManuscriptPage.resolveManuscriptEscalation({
    deescalateType: 'returnToChecker',
    checker,
  })

  cy.logoutKeycloak()
  cy.reload()

  cy.loginKeycloak({
    username: Cypress.env('firstCheckerEmail'),
    password: Cypress.env('password'),
  })

  cy.reload()

  DashboardPage.accessManuscriptsTab()

  cy.get('@submissionParam').then((submission) => {
    DashboardPage.searchForManuscript(submission.title)

    cy.findByText(submission.title).should('exist')
  })

  cy.findByTestId('status-label').should('not.exist')
})
