import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { DashboardPage } from '../common/pageObjects/dashboardPage'
import { goTo } from '../../support/helpers/accessByPath'

When(/^I pause the manuscript$/, () => {
  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  ManuscriptPage.escalateManuscript({ escalationType: 'pause' })
})

Then(/^I see the manuscript in the Paused status$/, () => {
  cy.reload()

  cy.get('@submissionParam').then((submission) => {
    DashboardPage.searchForManuscript(submission.title)
  })

  cy.findByTestId('status-label').contains('paused').should('exist')
})

When(/^I can unpause it$/, () => {
  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  ManuscriptPage.resolveManuscriptEscalation({ deescalateType: 'unpause' })

  DashboardPage.accessManuscriptsTab()

  cy.get('@submissionParam').then((submission) => {
    DashboardPage.searchForManuscript(submission.title)

    cy.findByText(submission.title).should('exist')
  })

  cy.findByTestId('status-label').should('not.exist')
})
