import { When, Then } from '@badeball/cypress-cucumber-preprocessor'
import { ManuscriptPage } from '../common/pageObjects/manuscriptPage'
import { DashboardPage } from '../common/pageObjects/dashboardPage'
import { goTo } from '../../support/helpers/accessByPath'

When(/^I send the manuscript to peer review for multiple reasons$/, () => {
  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  ManuscriptPage.escalateManuscript({ escalationType: 'sendToTeamLead' })

  cy.logoutKeycloak()
  cy.reload()

  cy.loginKeycloak({
    username: Cypress.env('teamLeaderEmail'),
    password: Cypress.env('password'),
  })

  cy.reload()

  DashboardPage.applyFilter('escalated')

  cy.get('@manuscriptId').then((id) => {
    goTo.manuscript(id)
  })

  cy.findByText('Send to Peer Review').click()

  ManuscriptPage.sendToPeerReviewModal.conflictOfInterest([
    'coiWithTriageEditor',
    'coiWithAcademicEditor',
  ])

  ManuscriptPage.sendToPeerReviewModal.improperReview()

  ManuscriptPage.sendToPeerReviewModal.decisionMadeInError()

  ManuscriptPage.addCommentAndReturn(
    'There are multiple problems with this manuscript.',
  )
})

Then(
  /^I see the manuscript with multiple problems in the Sent to Peer Review status$/,
  () => {
    DashboardPage.accessManuscriptsTab()

    cy.get('@submissionParam').then((submission) => {
      DashboardPage.searchForManuscript(submission.title)
    })

    cy.findByTestId('status-label')
      .contains('Sent To Peer Review')
      .should('exist')
  },
)
