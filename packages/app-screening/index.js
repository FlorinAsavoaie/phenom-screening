const config = require('config')
const { pubsubManager } = require('pubsweet-server')
const logger = require('@pubsweet/logger')

const startServer = require('./server/startServer')

const tryRequireRelative = require('pubsweet-server/src/helpers/tryRequireRelative')
const { createQueueService } = require('@hindawi/queue-service')
const { forEach } = require('lodash')
const {
  validateAndRegisterSchema,
} = require('@phenom.pub/schema-registry-cli/lib/register-schema')

const { typeDefs } = require('./server/schema')

const events = {}

if (config.has('pubsweet.components')) {
  config.get('pubsweet.components').forEach((componentName) => {
    getSchemaRecursively(componentName)
  })
}

const useWebpackCompilation = config.get('useWebpackCompile')
const queueDisabled = config.get('queueDisabled')

createQueueService({
  region: config.get('aws.region'),
  accessKeyId: config.has('aws.sns_sqs.accessKeyId')
    ? config.get('aws.sns_sqs.accessKeyId')
    : undefined,
  secretAccessKey: config.has('aws.sns_sqs.secretAccessKey')
    ? config.get('aws.sns_sqs.secretAccessKey')
    : undefined,
  snsEndpoint: config.has('aws.sns.endpoint')
    ? config.get('aws.sns.endpoint')
    : undefined,
  sqsEndpoint: config.has('aws.sqs.endpoint')
    ? config.get('aws.sqs.endpoint')
    : undefined,
  s3Endpoint: config.has('largeEvents.s3Endpoint')
    ? config.get('largeEvents.s3Endpoint')
    : undefined,
  topicName: config.get('aws.sns.topic'),
  queueName: config.get('aws.sqs.queueName'),
  bucketName: config.get('largeEvents.bucketName'),
  bucketPrefix: config.get('largeEvents.bucketPrefix'),
  eventNamespace: config.get('eventNamespace'),
  publisherName: config.get('publisherConfig.name'),
  serviceName: config.get('serviceName'),
  defaultMessageAttributes: config.get('defaultMessageAttributes'),
})
  .then((messageQueue) => {
    forEach(events, (handler, event) =>
      messageQueue.registerEventHandler({ event, handler }),
    )
    return messageQueue
  })
  .then((queueService) => {
    if (queueDisabled) {
      logger.info('SQS consumer disabled.')
    } else {
      queueService.start()
    }
    global.applicationEventBus = queueService
  })
  .then(validateAndRegisterGQLSchema(typeDefs))
  .then(startServer(useWebpackCompilation))
  .then(async () => {
    const subscriptionsEnabled = config.get(
      'pubsweet-server.apollo.subscriptionsEnabled',
    )

    if (!subscriptionsEnabled) {
      return
    }

    const pubSub = await pubsubManager.getPubsub()
    const pubSubMaxListeners = config.get('pubsweet-server.pubSubMaxListeners')
    pubSub.ee.setMaxListeners(pubSubMaxListeners)
  })
  .catch((err) => {
    logger.error('FATAL ERROR, SHUTTING DOWN:', err)
    process.exit(1)
  })

function getSchemaRecursively(componentName) {
  const component = tryRequireRelative(componentName)

  if (!component) throw new Error(`Could not find component ${componentName}`)

  if (component.eventHandlers) {
    Object.assign(events, component.eventHandlers)
  }
}

async function validateAndRegisterGQLSchema(typeDefs) {
  const service = {
    name: 'app-screening',
    url: config.get('serviceSchemaUrl'),
  }
  try {
    await validateAndRegisterSchema(service, typeDefs)
    logger.info('Schema registered successfully!')
  } catch (err) {
    logger.error(`Schema registration failed: ${err.message}`, err)
    process.exit(1)
  }
}
