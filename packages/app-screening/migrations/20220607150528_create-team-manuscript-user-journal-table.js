const TEAM_MANUSCRIPT_TABLE = 'team_manuscript'
const TEAM_USER_TABLE = 'team_user'
const TEAM_JOURNAL_TABLE = 'team_journal'

exports.up = async (knex) => {
  const teamManuscriptTableExists = await knex.schema.hasTable(
    TEAM_MANUSCRIPT_TABLE,
  )
  const teamUserTableExists = await knex.schema.hasTable(TEAM_USER_TABLE)
  const teamJournalTableExists = await knex.schema.hasTable(TEAM_JOURNAL_TABLE)

  if (!teamManuscriptTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(TEAM_MANUSCRIPT_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('team_id')
        table.uuid('manuscript_id')

        table.foreign('team_id').references('team.id')
        table.foreign('manuscript_id').references('manuscript.id')

        table.index('team_id', 'idx_team_manuscript_team_id')
        table.index('manuscript_id', 'idx_team_manuscript_manuscript_id')
      })

      console.info(`${TEAM_MANUSCRIPT_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!teamUserTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(TEAM_USER_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('team_id')
        table.uuid('user_id')
        table.text('role')
        table.timestamp('assignation_date')

        table.foreign('team_id').references('team.id')
        table.foreign('user_id').references('user.id')

        table.index('user_id', 'idx_team_user_user_id')
        table.index('team_id', 'idx_team_user_team_id')
        table.index('role', 'idx_team_user_role')
      })

      console.info(`${TEAM_USER_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!teamJournalTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(TEAM_JOURNAL_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('team_id')
        table.uuid('journal_id')

        table.foreign('team_id').references('team.id')
        table.foreign('journal_id').references('journal.id')

        table.index('team_id', 'idx_team_journal_team_id')
        table.index('journal_id', 'idx_team_journal_journal_id')
      })

      console.info(`${TEAM_JOURNAL_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(TEAM_MANUSCRIPT_TABLE)
    .then(() =>
      console.info(`${TEAM_MANUSCRIPT_TABLE} table dropped successfully`),
    )
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(TEAM_USER_TABLE)
    .then(() => console.info(`${TEAM_USER_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(TEAM_JOURNAL_TABLE)
    .then(() =>
      console.info(`${TEAM_JOURNAL_TABLE} table dropped successfully`),
    )
    .catch((e) => console.error(e))
}
