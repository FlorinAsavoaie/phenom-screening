const MANUSCRIPT_TABLE = 'manuscript'

exports.up = async (knex) => {
  try {
    await knex.schema.table(MANUSCRIPT_TABLE, (table) => {
      table.text('linked_submission_custom_id')
    })

    console.info(
      `Successfully added linked_submission_custom_id column to ${MANUSCRIPT_TABLE}`,
    )
  } catch (e) {
    console.error(
      `Addition of linked_submission_custom_id column to ${MANUSCRIPT_TABLE} table failed.`,
    )
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.table(MANUSCRIPT_TABLE, (table) => {
      table.dropColumn('linked_submission_custom_id')
    })

    console.info(
      `Successfully dropped column linked_submission_custom_id from ${MANUSCRIPT_TABLE}`,
    )
  } catch (e) {
    console.error(
      `Dropping column linked_submission_custom_id from ${MANUSCRIPT_TABLE} failed.`,
    )
  }
}
