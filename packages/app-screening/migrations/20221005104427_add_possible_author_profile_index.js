const POSSIBLE_AUTHOR_TABLE = 'possible_author_profile'

exports.up = async (knex) => {
  try {
    await knex.schema.alterTable(POSSIBLE_AUTHOR_TABLE, (table) => {
      table.index(
        ['manuscript_id', 'id'],
        'idx_possible_author_profile_id_manuscript_id',
      )
      console.info(
        `index idx_possible_author_id_manuscript_id  added successfully on ${POSSIBLE_AUTHOR_TABLE} table`,
      )
    })
  } catch (e) {
    console.error(e)
  }
}

exports.down = async (knex) => {
  await knex.schema.alterTable(POSSIBLE_AUTHOR_TABLE, (table) => {
    table.dropIndex(
      ['manuscript_id', 'id'],
      'idx_possible_author_id_manuscript_id',
    )
  })
}
