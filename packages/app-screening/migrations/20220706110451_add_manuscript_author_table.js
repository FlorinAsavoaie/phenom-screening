const MANUSCRIPT_AUTHOR_TABLE = 'manuscript_authors'

exports.up = async (knex) => {
  try {
    await knex.schema.createTable(MANUSCRIPT_AUTHOR_TABLE, (table) => {
      table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(knex.fn.now())
      table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

      table.uuid('manuscript_id').unique().notNullable()
      table.specificType('author_list', 'text[]')
      table.text('manuscript_title')

      table.foreign('manuscript_id').references('manuscript.id')

      table.index('manuscript_id', 'idx_manuscript_author_manuscript_id')
    })
    console.info(`${MANUSCRIPT_AUTHOR_TABLE} table created successfully`)
  } catch (e) {
    console.error(e)
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(MANUSCRIPT_AUTHOR_TABLE)
    .then(() =>
      console.info(`${MANUSCRIPT_AUTHOR_TABLE} table dropped successfully`),
    )
    .catch((e) => console.error(e))
}
