const MANUSCRIPT_CHECKS_TABLE = 'manuscript_checks'
const MANUSCRIPT_MATERIAL_CHECKS_TABLE = 'manuscript_material_checks'
const MANUSCRIPT_PEER_REVIEW_CHECKS_TABLE = 'manuscript_peer_review_checks'
const MANUSCRIPT_VALIDATIONS_TABLE = 'manuscript_validations'

exports.up = async (knex) => {
  const manuscriptChecksTableExists = await knex.schema.hasTable(
    MANUSCRIPT_CHECKS_TABLE,
  )
  const manuscriptMaterialChecksTableExists = await knex.schema.hasTable(
    MANUSCRIPT_MATERIAL_CHECKS_TABLE,
  )
  const manuscriptPeerReviewChecksTableExists = await knex.schema.hasTable(
    MANUSCRIPT_PEER_REVIEW_CHECKS_TABLE,
  )
  const manuscriptValidationsTableExists = await knex.schema.hasTable(
    MANUSCRIPT_VALIDATIONS_TABLE,
  )

  if (!manuscriptChecksTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(MANUSCRIPT_CHECKS_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('manuscript_id').notNullable()
        table.integer('language_percentage')
        table.integer('word_count')
        table.jsonb('suspicious_words')
        table.integer('total_similarity_percentage')
        table.integer('first_source_similarity_percentage')
        table.boolean('authors_verified')
        table.boolean('sanction_list_check_done')
        table.boolean('file_converted')
        table.text('report_url')

        table.foreign('manuscript_id').references('manuscript.id')

        table.index('word_count', 'idx_manuscript_checks_word_count')
        table.index(
          'sanction_list_check_done',
          'idx_manuscript_checks_sanction_list_check_done',
        )
        table.index('manuscript_id', 'idx_manuscript_checks_manuscript_id')
        table.index(
          'language_percentage',
          'idx_manuscript_checks_language_percentage',
        )
        table.index('file_converted', 'idx_manuscript_checks_file_converted')
        table.index(
          'authors_verified',
          'idx_manuscript_checks_authors_verified',
        )
      })

      console.info(`${MANUSCRIPT_CHECKS_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!manuscriptMaterialChecksTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(
        MANUSCRIPT_MATERIAL_CHECKS_TABLE,
        (table) => {
          table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
          table.timestamp('created').notNullable().defaultTo(knex.fn.now())
          table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

          table.uuid('manuscript_id')
          table.text('name')
          table.text('selected_option')
          table.text('additional_info')

          table.foreign('manuscript_id').references('manuscript.id')
        },
      )

      console.info(
        `${MANUSCRIPT_MATERIAL_CHECKS_TABLE} table created successfully`,
      )
    } catch (e) {
      console.error(e)
    }
  }

  if (!manuscriptPeerReviewChecksTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(
        MANUSCRIPT_PEER_REVIEW_CHECKS_TABLE,
        (table) => {
          table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
          table.timestamp('created').notNullable().defaultTo(knex.fn.now())
          table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

          table.uuid('manuscript_id')
          table.text('name')
          table.text('selected_option')
          table.text('observation')

          table.foreign('manuscript_id').references('manuscript.id')
        },
      )

      console.info(
        `${MANUSCRIPT_PEER_REVIEW_CHECKS_TABLE} table created successfully`,
      )
    } catch (e) {
      console.error(e)
    }
  }

  if (!manuscriptValidationsTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(MANUSCRIPT_VALIDATIONS_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('manuscript_id').notNullable()
        table.boolean('includes_citations')
        table.boolean('graphical')
        table.boolean('different_authors')
        table.boolean('missing_affiliations')
        table.boolean('nonacademic_affiliations')
        table.boolean('missing_references')
        table.boolean('wrong_references')
        table.boolean('topic_not_appropriate').defaultTo(false)
        table.boolean('article_type_is_not_appropriate').defaultTo(false)
        table.boolean('manuscript_is_missing_sections').defaultTo(false)

        table.foreign('manuscript_id').references('manuscript.id')
      })

      console.info(`${MANUSCRIPT_VALIDATIONS_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(MANUSCRIPT_CHECKS_TABLE)
    .then(() =>
      console.info(`${MANUSCRIPT_CHECKS_TABLE} table dropped successfully`),
    )
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(MANUSCRIPT_MATERIAL_CHECKS_TABLE)
    .then(() =>
      console.info(
        `${MANUSCRIPT_MATERIAL_CHECKS_TABLE} table dropped successfully`,
      ),
    )
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(MANUSCRIPT_PEER_REVIEW_CHECKS_TABLE)
    .then(() =>
      console.info(
        `${MANUSCRIPT_PEER_REVIEW_CHECKS_TABLE} table dropped successfully`,
      ),
    )
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(MANUSCRIPT_VALIDATIONS_TABLE)
    .then(() =>
      console.info(
        `${MANUSCRIPT_VALIDATIONS_TABLE} table dropped successfully`,
      ),
    )
    .catch((e) => console.error(e))
}
