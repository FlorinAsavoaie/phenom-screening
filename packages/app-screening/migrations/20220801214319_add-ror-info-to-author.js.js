const AUTHOR_TABLE = 'author'

const ROR_ID = 'ror_id'
const ROR_SCORE = 'ror_score'
const ROR_AFF = 'ror_aff'

module.exports = {
  up: async (knex) =>
    knex.schema.table(AUTHOR_TABLE, (table) => {
      table.string(ROR_ID)
      table.float(ROR_SCORE)
      table.string(ROR_AFF)
    }),
  down: async (knex) =>
    knex.schema.table(AUTHOR_TABLE, (table) => {
      table.dropColumn(ROR_ID)
      table.dropColumn(ROR_SCORE)
      table.dropColumn(ROR_AFF)
    }),
}
