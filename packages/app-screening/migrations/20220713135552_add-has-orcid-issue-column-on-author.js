const AUTHOR_TABLE = 'author'

exports.up = async (knex) => {
  try {
    await knex.schema.table(AUTHOR_TABLE, (table) => {
      table.boolean('has_orcid_issue')

      console.info(
        `has_orcid_issue column added successfully on ${AUTHOR_TABLE} table`,
      )
    })
  } catch (e) {
    console.error(e)
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropColumn('has_orcid_issue')
    .then(() =>
      console.info(
        `has_orcid_issue column dropped successfully on ${AUTHOR_TABLE} table`,
      ),
    )
    .catch((e) => console.error(e))
}
