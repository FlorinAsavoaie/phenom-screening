const AUDIT_LOG_TABLE = 'audit_log'

exports.up = async (knex) => {
  const auditLogTableExists = await knex.schema.hasTable(AUDIT_LOG_TABLE)

  if (!auditLogTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(AUDIT_LOG_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('user_id').notNullable()
        table.uuid('manuscript_id').notNullable()
        table.uuid('submission_id').notNullable()
        table.text('user_role').notNullable()
        table.text('comment')
        table.text('action').notNullable()
      })

      console.info(`${AUDIT_LOG_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(AUDIT_LOG_TABLE)

    console.info(`${AUDIT_LOG_TABLE} table dropped successfully`)
  } catch (e) {
    console.error(e)
  }
}
