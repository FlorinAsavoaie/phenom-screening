const MANUSCRIPT_TABLE = 'manuscript'

exports.up = async (knex) => {
  const manuscriptTableExists = await knex.schema.hasTable(MANUSCRIPT_TABLE)

  if (!manuscriptTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(MANUSCRIPT_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('title').notNullable()
        table.text('abstract').notNullable()
        table.text('article_type').notNullable()
        table.text('custom_id')
        table.uuid('submission_id')
        table.timestamp('submission_date').notNullable()
        table.uuid('journal_id').notNullable()
        table.text('references')
        table.text('status').notNullable()
        table.text('version').defaultTo('1')
        table.text('flow_type').defaultTo('screening')
        table.text('conflict_of_interest')
        table.text('data_availability')
        table.text('funding_statement')
        table.uuid('phenom_id')
        table.uuid('journal_section_id')
        table.uuid('special_issue_id')
        table.uuid('peer_review_check_config_id')
        table.uuid('material_check_config_id')
        table.timestamp('quality_checks_submitted_date')
        table.text('step').notNullable()
        table.boolean('author_responded').notNullable().defaultTo(false)

        table.foreign('journal_id').references('journal.id')
        table.foreign('journal_section_id').references('journal_section.id')
        table.foreign('special_issue_id').references('special_issue.id')
        table
          .foreign('peer_review_check_config_id')
          .references('peer_review_check_config.id')
        table
          .foreign('material_check_config_id')
          .references('material_check_config.id')

        table.index('submission_date', 'idx_manuscript_submission_date')
        table.index('status', 'idx_manuscript_status')
      })

      console.info(`${MANUSCRIPT_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(MANUSCRIPT_TABLE)

    console.info(`${MANUSCRIPT_TABLE} table dropped successfully`)
  } catch (e) {
    console.error(e)
  }
}
