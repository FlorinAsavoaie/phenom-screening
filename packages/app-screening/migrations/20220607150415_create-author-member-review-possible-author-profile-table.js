const AUTHOR_TABLE = 'author'
const MEMBER_TABLE = 'member'
const REVIEW_TABLE = 'review'
const POSSIBLE_AUTHOR_PROFILE_TABLE = 'possible_author_profile'

exports.up = async (knex) => {
  const authorTableExists = await knex.schema.hasTable(AUTHOR_TABLE)
  const memberTableExists = await knex.schema.hasTable(MEMBER_TABLE)
  const reviewTableExists = await knex.schema.hasTable(REVIEW_TABLE)
  const possibleAuthorProfileTableExists = await knex.schema.hasTable(
    POSSIBLE_AUTHOR_PROFILE_TABLE,
  )

  if (!authorTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(AUTHOR_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('manuscript_id').notNullable()
        table.boolean('is_submitting')
        table.boolean('is_corresponding')
        table.boolean('is_verified')
        table.boolean('is_on_bad_debt_list')
        table.boolean('is_on_black_list')
        table.boolean('is_on_watch_list')
        table.boolean('is_verified_out_of_tool').defaultTo(false)
        table.integer('position').defaultTo(0)
        table.text('surname')
        table.text('given_names')
        table.text('email')
        table.text('aff')
        table.text('country')
        table.text('title')
        table.text('orcid')

        table.foreign('manuscript_id').references('manuscript.id')

        table.index('manuscript_id', 'idx_author_manuscript_id')
      })

      console.info(`${AUTHOR_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!memberTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(MEMBER_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('manuscript_id')
        table.text('surname')
        table.text('given_names')
        table.text('title')
        table.text('email').notNullable()
        table.text('aff')
        table.text('country')
        table.text('role').notNullable()
        table.integer('reviewer_number')
        table.text('role_label')
        table.uuid('phenom_member_id')
        table.text('status')

        table.foreign('manuscript_id').references('manuscript.id')
      })

      console.info(`${MEMBER_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!reviewTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(REVIEW_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('member_id')
        table.uuid('author_id')
        table.text('recommendation').notNullable()
        table.text('public_comment')
        table.text('private_comment')
        table.timestamp('submitted').notNullable()
        table.boolean('is_valid').notNullable().defaultTo(true)

        table.foreign('member_id').references('member.id')
        table.foreign('author_id').references('author.id')
      })

      console.info(`${REVIEW_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!possibleAuthorProfileTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(POSSIBLE_AUTHOR_PROFILE_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('given_names')
        table.text('surname')
        table.text('email')
        table.text('affiliation')
        table.integer('no_of_publications')
        table.uuid('author_id')
        table.uuid('manuscript_id')
        table.boolean('chosen')
        table.integer('weight').defaultTo(0)
      })

      console.info(
        `${POSSIBLE_AUTHOR_PROFILE_TABLE} table created successfully`,
      )
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(AUTHOR_TABLE)
    .then(() => console.info(`${AUTHOR_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(MEMBER_TABLE)
    .then(() => console.info(`${MEMBER_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(REVIEW_TABLE)
    .then(() => console.info(`${REVIEW_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(POSSIBLE_AUTHOR_PROFILE_TABLE)
    .then(() =>
      console.info(
        `${POSSIBLE_AUTHOR_PROFILE_TABLE} table dropped successfully`,
      ),
    )
    .catch((e) => console.error(e))
}
