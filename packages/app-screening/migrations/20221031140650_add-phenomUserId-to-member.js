const MEMBER_TABLE = 'member'

exports.up = async (knex) => {
  try {
    await knex.schema.table(MEMBER_TABLE, (table) => {
      table.uuid('phenom_user_id')

      console.info(
        `phenom_user_id column added successfully on ${MEMBER_TABLE} table`,
      )
    })
  } catch (e) {
    console.error(e)
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropColumn('phenom_user_id')
    .then(() =>
      console.info(
        `phenom_user_id column dropped successfully on ${MEMBER_TABLE} table`,
      ),
    )
    .catch((e) => console.error(e))
}
