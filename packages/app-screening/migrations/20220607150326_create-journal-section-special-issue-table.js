const JOURNAL_TABLE = 'journal'
const JOURNAL_SECTION_TABLE = 'journal_section'
const SPECIAL_ISSUE_TABLE = 'special_issue'

exports.up = async (knex) => {
  const journalTableExists = await knex.schema.hasTable(JOURNAL_TABLE)
  const journalSectionTableExists = await knex.schema.hasTable(
    JOURNAL_SECTION_TABLE,
  )
  const specialIssueTableExists = await knex.schema.hasTable(
    SPECIAL_ISSUE_TABLE,
  )

  if (!journalTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(JOURNAL_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('name').notNullable().unique()
        table.text('publisher_name')
      })

      console.info(`${JOURNAL_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!journalSectionTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(JOURNAL_SECTION_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('journal_id').notNullable()
        table.text('name').notNullable().unique()

        table.foreign('journal_id').references('journal.id')
      })

      console.info(`${JOURNAL_SECTION_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!specialIssueTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(SPECIAL_ISSUE_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('journal_id').notNullable()
        table.uuid('journal_section_id')
        table.text('name').notNullable().unique()

        table.foreign('journal_id').references('journal.id')
        table.foreign('journal_section_id').references('journal_section.id')
      })

      console.info(`${SPECIAL_ISSUE_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(JOURNAL_TABLE)
    .then(() => console.info(`${JOURNAL_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(JOURNAL_SECTION_TABLE)
    .then(() =>
      console.info(`${JOURNAL_SECTION_TABLE} table dropped successfully`),
    )
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(SPECIAL_ISSUE_TABLE)
    .then(() =>
      console.info(`${SPECIAL_ISSUE_TABLE} table dropped successfully`),
    )
    .catch((e) => console.error(e))
}
