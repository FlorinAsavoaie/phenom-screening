const TEAM_TABLE = 'team'

exports.up = async (knex) => {
  const teamTableExists = await knex.schema.hasTable(TEAM_TABLE)

  if (!teamTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(TEAM_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('type').defaultTo('screening')

        table.text('name').unique()
      })

      console.info(`${TEAM_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(TEAM_TABLE)

    console.info(`${TEAM_TABLE} table dropped successfully`)
  } catch (e) {
    console.error(e)
  }
}
