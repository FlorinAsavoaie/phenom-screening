const USER_TABLE = 'user'
const USER_MANUSCRIPT = 'user_manuscript'

exports.up = async (knex) => {
  const userTableExists = await knex.schema.hasTable(USER_TABLE)
  const userManuscriptTableExists = await knex.schema.hasTable(USER_MANUSCRIPT)

  if (!userTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(USER_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('email').unique()
        table.text('password_hash')
        table.text('given_names')
        table.text('surname')
        table.boolean('is_confirmed')
        table.text('confirmation_token')
      })

      console.info(`${USER_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!userManuscriptTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(USER_MANUSCRIPT, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('user_id')
        table.uuid('manuscript_id').unique()

        table.foreign('user_id').references('user.id')
        table.foreign('manuscript_id').references('manuscript.id')

        table.index('user_id', 'idx_user_manuscript_user_id')
        table.index('manuscript_id', 'idx_user_manuscript_manuscript_id')
      })

      console.info(`${USER_MANUSCRIPT} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(USER_TABLE)
    .then(() => console.info(`${USER_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(USER_MANUSCRIPT)
    .then(() => console.info(`${USER_MANUSCRIPT} table dropped successfully`))
    .catch((e) => console.error(e))
}
