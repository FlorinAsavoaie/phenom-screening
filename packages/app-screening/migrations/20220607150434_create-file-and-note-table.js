const FILE_TABLE = 'file'
const NOTE_TABLE = 'note'

exports.up = async (knex) => {
  const fileTableExists = await knex.schema.hasTable(FILE_TABLE)
  const noteTableExists = await knex.schema.hasTable(NOTE_TABLE)

  if (!fileTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(FILE_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('provider_key')
        table.text('original_file_provider_key')
        table.text('original_name')
        table.text('file_name')
        table.uuid('manuscript_id')
        table.text('type').notNullable()
        table.text('mime_type')
        table.integer('size')
        table.uuid('review_id')

        table.foreign('review_id').references('review.id')
        table.foreign('manuscript_id').references('manuscript.id')
      })

      console.info(`${FILE_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }

  if (!noteTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(NOTE_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('manuscript_id')
        table.text('type')
        table.text('content')

        table.foreign('manuscript_id').references('manuscript.id')
      })

      console.info(`${NOTE_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(FILE_TABLE)
    .then(() => console.info(`${FILE_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(NOTE_TABLE)
    .then(() => console.info(`${NOTE_TABLE} table dropped successfully`))
    .catch((e) => console.error(e))
}
