const PEER_REVIEW_CHECK_CONFIG_TABLE = 'peer_review_check_config'
const MATERIAL_CHECK_CONFIG_TABLE = 'material_check_config'

exports.up = async (knex) => {
  const peerReviewCheckConfigTableExists = await knex.schema.hasTable(
    PEER_REVIEW_CHECK_CONFIG_TABLE,
  )
  const materialCheckConfigTableExists = await knex.schema.hasTable(
    MATERIAL_CHECK_CONFIG_TABLE,
  )

  if (!peerReviewCheckConfigTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(PEER_REVIEW_CHECK_CONFIG_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.json('config').notNullable()
        table.integer('version').notNullable()
      })

      console.info(
        `${PEER_REVIEW_CHECK_CONFIG_TABLE} table created successfully`,
      )
    } catch (e) {
      console.error(e)
    }
  }

  if (!materialCheckConfigTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(MATERIAL_CHECK_CONFIG_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.json('config').notNullable()
        table.integer('version').notNullable()
      })

      console.info(`${MATERIAL_CHECK_CONFIG_TABLE} table created successfully`)
    } catch (e) {
      console.error(e)
    }
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(PEER_REVIEW_CHECK_CONFIG_TABLE)
    .then(() =>
      console.info(
        `${PEER_REVIEW_CHECK_CONFIG_TABLE} table dropped successfully`,
      ),
    )
    .catch((e) => console.error(e))

  await knex.schema
    .dropTable(MATERIAL_CHECK_CONFIG_TABLE)
    .then(() =>
      console.info(`${MATERIAL_CHECK_CONFIG_TABLE} table dropped successfully`),
    )
    .catch((e) => console.error(e))
}
