const path = require('path')
const config = require('config')
const fs = require('fs-extra')
const { pick } = require('lodash')

// can't use node-config in webpack so save whitelisted client config into the build and alias it below
const outputPath = path.resolve(__dirname, '..', '_build', 'config')
const clientConfig = pick(config, config.publicKeys)
fs.ensureDirSync(outputPath)
const clientConfigPath = path.join(outputPath, 'client-config.json')
fs.writeJsonSync(clientConfigPath, clientConfig, { spaces: 2 })

module.exports = {
  alias: {
    config: clientConfigPath,
  },
  extensions: [
    '.webpack.js',
    '.web.js',
    '.mjs',
    '.js',
    '.jsx',
    '.json',
    '.ts',
    '.tsx',
  ],
}
