process.env.NODE_ENV = 'production'
process.env.BABEL_ENV = 'production'

const path = require('path')
const config = require('config')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const rules = require('./rules.production')
const resolve = require('./common-resolve')

module.exports = [
  {
    name: 'app',
    target: 'web',
    mode: 'production',
    performance: {
      hints: 'error',
      maxEntrypointSize: 512000,
      maxAssetSize: 512000,
    },
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: ['./app'],
    },
    output: {
      path: path.join(__dirname, '..', '_build', 'assets'),
      filename: '[name].[hash].js',
      publicPath: '/assets/',
    },
    module: {
      rules,
    },
    resolve,
    plugins: [
      new CleanWebpackPlugin({
        root: path.join(__dirname, '..', '_build'),
      }),
      new HtmlWebpackPlugin({
        title: `${config.get('publisherConfig.name')} Screening`,
        buildTime: new Date().toString(),
        template: path.join(__dirname, '..', 'app/index-production.html'),
        inject: 'body',
        gaHead: `<!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-XXXXXX');</script>
        <!-- End Google Tag Manager -->`,
        gaBody: ` <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-XXXXXX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->`,
      }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.KEYCLOAK': JSON.stringify(config.get('keycloak.public')),
        'process.env.PUBLISHER_NAME': JSON.stringify(
          process.env.PUBLISHER_NAME,
        ),
        'process.env.REVIEW_URL':
          JSON.stringify(process.env.REVIEW_URL) ||
          JSON.stringify('http://localhost:3000'),
        'process.env.ORCID_SANDBOX_ENABLED': JSON.stringify(
          process.env.ORCID_SANDBOX_ENABLED || false,
        ),
        'process.env.GATEWAY_URI': JSON.stringify(process.env.GATEWAY_URI),
        'process.env.SUBSCRIPTIONS_ENABLED': JSON.stringify(
          config.get('pubsweet-server.apollo.subscriptionsEnabled'),
        ),
      }),
      new webpack.ContextReplacementPlugin(/./, __dirname, {
        [config.authsome.mode]: config.authsome.mode,
      }),
      new MiniCssExtractPlugin({
        filename: '[name].[hash].css',
        chunkFilename: '[id].css',
      }),
      new CopyWebpackPlugin({
        patterns: [{ from: `../static/${process.env.PUBLISHER_NAME}` }],
      }),
      new webpack.optimize.AggressiveMergingPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin(),
    ],
    node: {
      fs: 'empty',
      __dirname: true,
    },
  },
]
