process.env.NODE_ENV = 'test'
process.env.BABEL_ENV = 'test'

const config = require('config')
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

const rules = require('./rules.production')
const resolve = require('./common-resolve')

module.exports = [
  {
    // The configuration for the client
    name: 'app',
    target: 'web',
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: ['./app'],
    },
    output: {
      path: path.join(__dirname, '..', '_build', 'assets'),
      filename: '[name].[hash].js',
      publicPath: '/assets/',
    },
    module: {
      rules,
    },
    resolve,
    devtool: 'eval',
    plugins: [
      new CleanWebpackPlugin({
        root: path.join(__dirname, '..', '_build'),
      }),
      new HtmlWebpackPlugin({
        title: 'Hindawi Review',
        buildTime: new Date().toString(),
        template: '../app/index-production.html',
        inject: 'body',
      }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.REVIEW_URL': JSON.stringify(config.get('reviewUrl')),
        'process.env.ORCID_SANDBOX_ENABLED': JSON.stringify(
          config.get('orcidSandboxEnabled'),
        ),
      }),
      new webpack.ContextReplacementPlugin(/./, __dirname, {
        [config.authsome.mode]: config.authsome.mode,
      }),
      new CopyWebpackPlugin({ patterns: [{ from: '../static' }] }),
      new webpack.optimize.AggressiveMergingPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin(),
    ],
    node: {
      fs: 'empty',
      __dirname: true,
    },
  },
]
