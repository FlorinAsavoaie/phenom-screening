process.env.NODE_ENV = 'development'
process.env.BABEL_ENV = 'development'

const path = require('path')
const config = require('config')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const rules = require('./rules.development')
const resolve = require('./common-resolve')

module.exports = [
  {
    name: 'app',
    watch: true,
    target: 'web',
    mode: 'development',
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: ['webpack-hot-middleware/client?reload=true', './app'],
    },
    output: {
      path: path.join(__dirname, '..', '_build', 'assets'),
      filename: '[name].js',
      publicPath: '/assets/',
    },
    devtool: 'cheap-module-source-map',
    module: {
      rules,
    },
    resolve,
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.KEYCLOAK': JSON.stringify(config.get('keycloak.public')),
        'process.env.PUBLISHER_NAME': JSON.stringify(
          process.env.PUBLISHER_NAME,
        ),
        'process.env.REVIEW_URL':
          JSON.stringify(process.env.REVIEW_URL) ||
          JSON.stringify('http://localhost:3000'),
        'process.env.ORCID_SANDBOX_ENABLED': JSON.stringify(
          process.env.ORCID_SANDBOX_ENABLED || false,
        ),
        'process.env.GATEWAY_URI': JSON.stringify(process.env.GATEWAY_URI),
        'process.env.SUBSCRIPTIONS_ENABLED': JSON.stringify(
          config.get('pubsweet-server.apollo.subscriptionsEnabled'),
        ),
      }),
      new webpack.ContextReplacementPlugin(/./, __dirname, {
        [config.authsome.mode]: config.authsome.mode,
      }),
      new CopyWebpackPlugin({
        patterns: [{ from: `../static/${process.env.PUBLISHER_NAME}` }],
      }),
    ],
    node: {
      fs: 'empty',
      __dirname: true,
    },
  },
]
