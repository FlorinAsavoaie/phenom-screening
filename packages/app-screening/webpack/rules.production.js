const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const include = require('./babel-includes')

module.exports = [
  {
    oneOf: [
      {
        test: /\.(js|ts)x?$/,
        include,
        loader: 'babel-loader',
        options: {
          presets: [
            [require('@babel/preset-env')],
            [require('@babel/preset-react'), { runtime: 'automatic' }],
            require('@babel/preset-typescript'),
          ],
          plugins: [
            require('@babel/plugin-proposal-class-properties'),
            require('@babel/plugin-syntax-dynamic-import'),
            require('@babel/plugin-transform-runtime'),
          ],
        },
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
        ],
      },
      {
        exclude: [/\.(js|jsx)$/, /\.html$/, /\.json$/],
        loader: 'file-loader',
        options: {
          name: 'static/media/[name].[hash:8].[ext]',
        },
      },
    ],
  },
]
