# App Screening

### Required Env vars

```sh
NODE_ENV = development

AWS_REGION=eu-west-1

AWS_PROFILE=HindawiDevelopment

AWS_S3_BUCKET==****
REVIEW_AWS_S3_BUCKET==****
AWS_S3_EVENT_STORAGE_BUCKET=****

AWS_SES_ACCESS_KEY==****
AWS_SES_SECRET_KEY==****

AWS_SNS_ENDPOINT=http://localhost:4566
AWS_SNS_TOPIC=test1

AWS_SQS_ENDPOINT=http://localhost:4566
AWS_SQS_QUEUE_NAME=screening-queue

EMAIL_SENDER=*example@something.*
PUBSWEET_INVITE_PASSWORD_RESET_URL=/invite

SIMILARITY_CHECK_SERVICE_ENABLED=false
# If true the `ithenticate checks` will be taken into consideration when showing manuscripts on dashboard

EVENT_NAMESPACE=pen
PUBLISHER_NAME=hindawi

AWS_S3_ENDPOINT=.
PHENOM_LARGE_EVENTS_BUCKET=.
PHENOM_LARGE_EVENTS_PREFIX=.

DB_USER=****
DB_HOST=postgres

GATEWAY_URI=http://localhost:4001

# REGISTRY_URL_FOR_ENVIRONMENT, example for local environment
SCHEMA_REGISTRY_URL='http://host.docker.internal:6001'

REVIEW_URL=http://localhost:3000

ORCID_SANDBOX_ENABLED=false
```

### Not Required Env vars - default values

```sh
#Loaded from the Shared Credentials File (~/aws/credentials) if logged in

EMAIL_SENDING=false
# If set to false `mailer-mock` will be used, otherwise AWS Email Service.

DATABASE=screening
DB_PASSWORD=''
DB_SSL=true # if NODE_ENV is set to `production`

BABEL_ENV='development'
LOG_LEVEL='silly'

KEYCLOACK_CHECK_LOGIN_IFRAME=false
# Set to false if NODE_ENV is set to development, true otherwise (NODE_ENV=production)

# url to the service graphql schema, needed for schema registry push, example for local environment
SERVICE_SCHEMA_URL='http://host.docker.internal:4000/graphql'
```

`AWS_S3_ACCESS_KEY` & `AWS_S3_SECRET_KEY` & `AWS_SNS_SQS_ACCESS_KEY` & `AWS_SNS_SQS_SECRET_KEY` are no longer needed in the .env file. \
Access keys are loaded from the Shared Credentials File (~/aws/credentials) for the AWS profile. This means that in order to work you need to be logged in with the Development AWS account.

# Config for docker-compose

```sh
NODE_ENV=production
DB_HOST=postgres
KEYCLOACK_CHECK_LOGIN_IFRAME=false
DB_SSL=false
AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SNS_ENDPOINT=http://localstack:4566
SCHEMA_REGISTRY_URL='http://gql-schema-registry:3000'
SERVICE_SCHEMA_URL='http://app-screening:3000/graphql'
```

# Config for yarn start

```sh
NODE_ENV=development
DB_HOST=localhost
AWS_SNS_ENDPOINT=http://localhost:4566
AWS_SQS_ENDPOINT=http://localhost:4566
```

### Running populate manuscript authors table script

Inside `scripts/populate-manuscript-authors-table.js` you'll find the code base for a script that goes through all the
manuscripts, creates a list of authors emails for each of them and then adds these entries inside manuscript_authors
table.

In order to run the script, you need to deploy it in a specific Kubernetes environment and then trigger it as a job. You
may follow the next steps:

1. Make sure you edit `infrastructure/populate-manuscript-authors-table-job/populate-manuscript-authors-table-job.yaml`
   job configuration file and assign correct values for all the environment variables specified over there (you can copy
   them from a pod corresponding to app-screening from the same environment). Also, you may replace the endpoint for the
   docker image to be used depending on the enviroment where you're running it.

2. Make sure you gain access to the Kubernetes environment where you want to run the job by making use
   of `aws-azure-login` tool and setting your local context to the right cluster:

```
// dev cluster
aws eks update-kubeconfig --name hindawi-dev --profile HindawiDevelopment

// prod cluster
aws eks update-kubeconfig --name hindawi-prod --profile HindawiProduction
```

3. `cd` to the .yaml's location and
   run `kubectl apply -f populate-manuscript-authors-table-job -n [namespace-corresponding-to-the-environment]`

You can check the available namespaces with `kubectl get ns`

4. Check with Lens or `kubectl get pods` and `kubectl logs [created-job-pod-id]` to see if the job is running
   successfully.
