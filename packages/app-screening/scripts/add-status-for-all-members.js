/**
 * This script is used to add status value for all entries in member table
 *
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***

 * REVIEW_DATABASE=***
 * REVIEW_DB_HOST=***
 * REVIEW_DB_PASS=***
 * REVIEW_DB_USER=***
 * ```
 */
const config = require('config')
const { Promise } = require('bluebird')
const { uniq } = require('lodash')
const Knex = require('knex')

const PAGE_SIZE = 40000

async function execute() {
  const screeningDB = Knex({
    client: 'pg',
    connection: config.get('pubsweet-server.db'),
  })

  const reviewDB = Knex({
    client: 'pg',
    connection: {
      user: process.env.REVIEW_DB_USER,
      password: process.env.REVIEW_DB_PASS,
      database: process.env.REVIEW_DATABASE,
      host: process.env.REVIEW_DB_HOST,
      port: 5432,
      idleTimeoutMillis: 30000,
      connectionTimeoutMillis: 20000,
    },
  })

  const [{ count }] = await screeningDB
    .count('id')
    .whereNull('status')
    .from('member')

  const numberOfPages = Math.ceil(count / PAGE_SIZE)
  const pages = new Array(numberOfPages).fill(0).map((_, index) => index + 1)

  const membersToUpdate = []

  await Promise.each(pages, async (pageNumber) => {
    const offset = (pageNumber - 1) * PAGE_SIZE

    const screeningMembers = await screeningDB
      .select(['phenom_member_id'])
      .whereNull('status')
      .from('member')
      .orderBy('created')
      .offset(offset)
      .limit(PAGE_SIZE)

    const screeningPhenomMemberIds = uniq(
      screeningMembers.map((m) => m.phenom_member_id),
    )

    const reviewTeamMembers = await reviewDB
      .select(['id', 'status'])
      .whereIn('id', screeningPhenomMemberIds)
      .from('team_member')

    screeningPhenomMemberIds.map((phenom_member_id) => {
      const reviewTeamMember = reviewTeamMembers.find((reviewTeamMember) => {
        return reviewTeamMember.id === phenom_member_id
      })

      const statusFromReview = reviewTeamMember
        ? reviewTeamMember.status
        : 'removed'

      membersToUpdate.push({
        phenom_member_id,
        status: statusFromReview,
      })
    })
  })

  await screeningDB.transaction(async (trx) => {
    const queries = membersToUpdate.map((memberToUpdate) =>
      screeningDB('member')
        .where('phenom_member_id', memberToUpdate.phenom_member_id)
        .update({
          status: memberToUpdate.status,
        })
        .transacting(trx),
    )

    await Promise.all(queries)
      .then(async () => {
        await trx.commit()
      })
      .catch((err) => {
        trx.rollback()
        throw err
      })
  })
}

;(async function main() {
  try {
    await execute()
  } catch (err) {
    console.error('Something went wrong while updating...')

    console.error(err)
    process.exit(1)
  }

  console.info(`Process completed`)
  process.exit(0)
})()
