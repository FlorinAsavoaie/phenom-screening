const config = require('config')
const logger = require('@pubsweet/logger')

async function createUser(knex, kcAdminClient, user) {
  if (!user.email) {
    throw new Error(`Please add an email.`)
  }

  const userFromDb = await findUserInDb(knex, user.email)
  const userFromSSO = await findUserInSSO(kcAdminClient, user.email)

  if (!userFromDb && !userFromSSO) {
    const savedUserIdFromSSO = await createUserInSSO(kcAdminClient, user)
    await createUserInDb(knex, { ...user, id: savedUserIdFromSSO })
  }

  if (userFromDb && !userFromSSO) {
    await createUserInSSO(kcAdminClient, {
      ...userFromDb,
      password: user.password,
    })
  }

  if (!userFromDb && userFromSSO) {
    await createUserInDb(knex, userFromSSO)
  }
}

async function findUserInDb(knex, email) {
  const userFromDb = await knex
    .select('id', 'email', 'surname', 'given_names')
    .where({ email })
    .from('user')

  return userFromDb.length
    ? {
        id: userFromDb[0].id,
        email: userFromDb[0]['email'],
        firstName: userFromDb[0]['given_names'],
        lastName: userFromDb[0]['surname'],
      }
    : null
}

async function findUserInSSO(kcAdminClient, email) {
  const userFromSSO = await kcAdminClient.users.find({
    email,
  })

  if (userFromSSO.length && userFromSSO[0].email !== email) {
    throw new Error(`The email: "${email}" is invalid.`)
  }

  return userFromSSO.length
    ? {
        id: userFromSSO[0].id,
        email: userFromSSO[0].email,
        firstName: userFromSSO[0].firstName,
        lastName: userFromSSO[0].lastName,
      }
    : null
}

async function createUserInSSO(
  kcAdminClient,
  { email, firstName, lastName, password },
) {
  let savedUserFromSSO

  if (!firstName) {
    throw new Error(`Please add a firstName.`)
  }

  if (!lastName) {
    throw new Error(`Please add a lastName.`)
  }

  if (!password) {
    savedUserFromSSO = await kcAdminClient.users
      .create({
        email,
        username: email,
        enabled: true,
        emailVerified: false,
        firstName,
        lastName,
        requiredActions: ['UPDATE_PASSWORD'],
      })
      .then((kcUser) => sendVerifyEmail(kcAdminClient, kcUser))
  } else {
    verifyPassword(password)

    savedUserFromSSO = await kcAdminClient.users.create({
      email,
      username: email,
      enabled: true,
      emailVerified: true,
      firstName,
      lastName,
      credentials: [
        {
          type: 'password',
          value: password,
          temporary: false,
          algorithm: 'pbkdf2-sha256',
          hashIterations: '27500',
        },
      ],
    })
  }
  logger.info(`User "${email}" was created in sso.`)
  return savedUserFromSSO.id
}

async function createUserInDb(knex, user) {
  const savedUser = await knex('user').insert({
    id: user.id,
    email: user.email.toLowerCase(),
    surname: user.lastName,
    given_names: user.firstName,
    is_confirmed: true,
    confirmation_token: null,
  })
  logger.info(`User "${user.email}" was created in db.`)
  return savedUser[0]
}

async function sendVerifyEmail(kcAdminClient, { email, id }) {
  logger.info('Sending keycloak verify email to: ', email)
  return kcAdminClient.users.sendVerifyEmail({
    id,
    clientId: config.get('keycloak.public.clientID'),
    redirectUri: config.get('pubsweet-client.baseUrl'),
  })
}

async function verifyPassword(password) {
  const minLength = (value, min) => !!(value && value.length >= min)
  const atLeastOneUppercase = (value) => {
    const uppercaseRegex = new RegExp(/([A-Z])+/)
    return uppercaseRegex.test(value)
  }
  const atLeastOneLowercase = (value) => {
    const lowercaseRegex = new RegExp(/([a-z])+/)
    return lowercaseRegex.test(value)
  }
  const atLeastOneDigit = (value) => {
    const digitRegex = new RegExp(/([0-9])+/)
    return digitRegex.test(value)
  }
  const atLeastOnePunctuation = (value) => {
    const punctuationRegex = new RegExp(/([,'!@#$%^&*=(){}[\]<>?/\\|.:;_-])+/)
    return punctuationRegex.test(value)
  }
  if (
    !(
      minLength(password, 6) &&
      atLeastOneUppercase(password) &&
      atLeastOneLowercase(password) &&
      atLeastOnePunctuation(password) &&
      atLeastOneDigit(password)
    )
  ) {
    throw new Error(
      `The password should have at least 6 characters and at least one of uppercase letter, lowercase letter, punctuation, digit.`,
    )
  }
}
module.exports = { createUser }
