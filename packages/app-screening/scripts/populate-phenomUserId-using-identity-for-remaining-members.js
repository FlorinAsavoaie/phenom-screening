/**
 * This script is used to populate the remaining phenom_user_id 
 * from identity table into member table
 *
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***

 * REVIEW_DATABASE=***
 * REVIEW_DB_HOST=***
 * REVIEW_DB_PASS=***
 * REVIEW_DB_USER=***
 * ```
 */
const { Promise } = require('bluebird')
const { chain } = require('lodash')
const Knex = require('knex')

const PAGE_SIZE = 10000

async function execute() {
  const screeningDB = Knex({
    client: 'pg',
    connection: {
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DATABASE,
      host: process.env.DB_HOST,
      port: 5432,
      idleTimeoutMillis: 30000,
      connectionTimeoutMillis: 20000,
    },
  })

  const reviewDB = Knex({
    client: 'pg',
    connection: {
      user: process.env.REVIEW_DB_USER,
      password: process.env.REVIEW_DB_PASS,
      database: process.env.REVIEW_DATABASE,
      host: process.env.REVIEW_DB_HOST,
      port: 5432,
      idleTimeoutMillis: 30000,
      connectionTimeoutMillis: 20000,
    },
  })

  const MEMBER_TABLE = 'member'

  await screeningDB.schema.alterTable(MEMBER_TABLE, (table) => {
    table.index(['email'], 'idx_email')
    console.info(`index idx_email added successfully on ${MEMBER_TABLE} table`)
  })

  const [{ count }] = await screeningDB
    .count('id')
    .whereNull('phenom_user_id')
    .from(MEMBER_TABLE)

  let membersToUpdate = []
  const numberOfPages = Math.ceil(count / PAGE_SIZE)
  const pages = new Array(numberOfPages).fill(0).map((_, index) => index + 1)

  console.info('numberOfPages: ', numberOfPages)

  await Promise.each(pages, async (pageNumber) => {
    console.info('pageNumber: ', pageNumber)

    const offset = (pageNumber - 1) * PAGE_SIZE

    const screeningMembers = await screeningDB
      .select(['email'])
      .whereNull('phenom_user_id')
      .from(MEMBER_TABLE)
      .orderBy('email', 'created')
      .offset(offset)
      .limit(PAGE_SIZE)

    const screeningPhenomMemberEmails = chain(screeningMembers)
      .map((m) => m.email)
      .uniq()
      .value()

    const reviewTeamMembers = await reviewDB
      .select(['user_id', 'email'])
      .whereIn('email', screeningPhenomMemberEmails)
      .from('identity')

    membersToUpdate = [...membersToUpdate, ...reviewTeamMembers]
  })

  await Promise.each(membersToUpdate, async (memberToUpdate) =>
    screeningDB(MEMBER_TABLE).where('email', memberToUpdate.email).update({
      phenom_user_id: memberToUpdate.user_id,
    }),
  )

  console.info('Members update done.')

  await screeningDB.schema.alterTable(MEMBER_TABLE, (table) => {
    table.dropIndex(['email'], 'idx_email')
    console.info(
      `index idx_email dropped successfully on ${MEMBER_TABLE} table`,
    )
  })
}

;(async function main() {
  try {
    await execute()
  } catch (err) {
    console.error('Something went wrong while updating...')

    console.error(err)
    process.exit(1)
  }

  console.info('Process completed')
  process.exit(0)
})()
