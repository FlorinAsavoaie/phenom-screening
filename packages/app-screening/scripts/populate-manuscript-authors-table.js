const config = require('config')
const { Promise } = require('bluebird')
const Knex = require('knex')
const Manuscript = require('../../component-model/src/manuscript/manuscript')

const PAGE_SIZE = 1000


async function execute() {
  const screeningDB = Knex({
    client: 'pg',
    connection: config.get('pubsweet-server.db'),
  })

  console.log('---CONNECTION INIT---')
    const scriptStartTime = new Date().toISOString()
    const [{ count }] = await screeningDB
      .countDistinct('submission_id')
      .whereNot('status', 'olderVersion')
      .from('manuscript')

    console.log('no of manuscripts to be added in the new table', count)

    const numberOfPages = Math.ceil(count / PAGE_SIZE)
    const pages = new Array(numberOfPages).fill(0).map((_, index) => index + 1)

    await Promise.each(pages, async (pageNumber) => {
      const entries = []
      const offset = (pageNumber - 1) * PAGE_SIZE

      const screeningManuscripts = await Manuscript.query()
        .withGraphFetched('authors')
        .with('with_alias', screeningDB.raw(
          `SELECT  *
                , ROW_NUMBER() OVER ( PARTITION BY submission_id ORDER BY version desc, created desc )  AS OCCURENCE  
          FROM manuscript
        where status != 'olderVersion'`
          ))  //order manuscripts from one submission by version desc first and created desc after and choose the first, this should cover also the case for withdrawn submissions with multiple versions saved at the same time and with the same status 
        .select('*')
        .from('with_alias')
        .where('OCCURENCE', 1)
        .andWhere('created', '<', scriptStartTime) // don't select manuscript added during script run since they are already present in the manuscript_authors table
        .orderBy('created')
        .offset(offset)
        .limit(PAGE_SIZE)

      await Promise.each(screeningManuscripts, (manuscript) => {
        const authorList = manuscript.authors.map((a) => a.email).sort()

        entries.push({
            author_list: authorList,
            manuscript_id: manuscript.id,
            manuscript_title: manuscript.title.trim().replace(/\s\s+/g, ' '),
        })
      })
      await screeningDB('manuscript_authors').insert(entries).onConflict('manuscript_id').ignore() // on conflict added to don't save in db the manuscripts that are created between deploy and scriptStartTime if there are any
      console.log('entries ->', pageNumber*PAGE_SIZE)
    })
}

;(async function main() {
  try {
    await execute()
  } catch (err) {
    console.error('Something went wrong while updating...')

    console.error(err)
    process.exit(1)
  }

  console.info(`Process completed`)
  process.exit(0)
})()
