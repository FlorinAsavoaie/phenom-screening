const config = require('config')
const { Promise } = require('bluebird')
const KeycloakAdminClient = require('@keycloak/keycloak-admin-client').default
const Knex = require('knex')
const logger = require('@pubsweet/logger')

const ADMIN_ROLE_NAME = 'Admin'

;(async function main() {
  try {
    const kcAdminClient = await initKeycloackAdminClient()
    const knex = Knex({
      client: 'pg',
      connection: config.get('pubsweet-server.db'),
    })

    const [client] = await kcAdminClient.clients.find({
      clientId: config.get('keycloak.public.clientID'),
    })
    logger.info(`> Starting updating process in "${client.clientId}" client...`)

    logger.info(`> Creating admin role...`)
    const roles = await kcAdminClient.clients.listRoles({
      id: client.id,
    })
    const adminRole = roles.find((role) => role.name === ADMIN_ROLE_NAME)

    if (!adminRole) {
      const createdAdminRole = await kcAdminClient.clients.createRole({
        id: client.id,
        name: ADMIN_ROLE_NAME,
      })
      logger.info(`Created ${createdAdminRole.roleName} role`)
    } else {
      logger.info(`Admin role already exists`)
    }

    logger.info(`> Adding admin role...`)
    const admins = await knex
      .select()
      .from('user')
      .leftJoin('team_user', 'user.id', 'team_user.user_id')
      .where({
        'team_user.role': 'admin',
      })

    await Promise.map(admins, async ({ email }) => {
      const users = await kcAdminClient.users.find({
        email: email,
      })

      if (!users.length) {
        logger.warn(
          `User with email "${email}" was not found on keycloak server`,
        )

        return
      }

      const user = users[0]
      const availableClientRoleMappings =
        await kcAdminClient.users.listAvailableClientRoleMappings({
          id: user.id,
          clientUniqueId: client.id,
        })

      const adminClientRole = availableClientRoleMappings.find(
        (clientRole) => clientRole.name === ADMIN_ROLE_NAME,
      )

      if (adminClientRole) {
        await kcAdminClient.users.addClientRoleMappings({
          id: user.id,
          clientUniqueId: client.id,
          roles: [
            {
              id: adminClientRole.id,
              name: adminClientRole.name,
            },
          ],
        })

        logger.info(`Admin role added for "${email}"`)
      } else {
        logger.info(`User with email "${email}" is already marked as an admin`)
      }
    })
  } catch (err) {
    logger.error(err)
    process.exit(1)
  }

  logger.info(`Process completed`)
  process.exit(0)
})()

async function initKeycloackAdminClient() {
  const kcAdminClient = new KeycloakAdminClient({
    baseUrl: config.get('keycloak.public.authServerURL'),
    realmName: 'master',
  })

  await kcAdminClient.auth({
    username: config.get('keycloak.admin.username'),
    password: config.get('keycloak.admin.password'),
    grantType: 'password',
    clientId: 'admin-cli',
  })

  kcAdminClient.setConfig({
    realmName: config.get('keycloak.public.realm'),
  })

  return kcAdminClient
}
