/**
 * This script is used to add an editorialAssistant entry in member table for
 * submissions with no editorialAssistant 
 *
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***

 * REVIEW_DATABASE=***
 * REVIEW_DB_HOST=***
 * REVIEW_DB_PASS=***
 * REVIEW_DB_USER=***
 * ```
 */
const config = require('config')
const { Promise } = require('bluebird')
const Knex = require('knex')

;(async function main() {
  try {
    const screeningDB = Knex({
      client: 'pg',
      connection: config.get('pubsweet-server.db'),
    })

    const reviewDB = Knex({
      client: 'pg',
      connection: {
        user: process.env.REVIEW_DB_USER,
        password: process.env.REVIEW_DB_PASS,
        database: process.env.REVIEW_DATABASE,
        host: process.env.REVIEW_DB_HOST,
        port: 5432,
        idleTimeoutMillis: 30000,
        connectionTimeoutMillis: 20000,
      },
    })

    const manuscripts = await screeningDB
      .select(['id', 'phenom_id'])
      .from('manuscript')
      .whereIn('status', [
        'escalated',
        'inProgress',
        'paused',
        'automaticChecks',
      ])
      .orderBy('created')

    const editorialAssistantMembers = []

    await Promise.each(manuscripts, async (manuscript) => {
      const editorialAssistant = await screeningDB
        .select('id')
        .from('member')
        .where({
          manuscript_id: manuscript.id,
          role: 'editorialAssistant',
          status: 'active',
        })
        .first()

      if (!editorialAssistant) {
        const editorialAssistantTeamMember = await reviewDB
          .select()
          .from('team')
          .leftJoin('team_member', 'team_member.team_id', '=', 'team.id')
          .where({
            'team.manuscript_id': manuscript.phenom_id,
            'team.role': 'editorialAssistant',
            'team_member.status': 'active',
          })
          .first()
        if (!editorialAssistantTeamMember) {
          return
        }

        editorialAssistantMembers.push({
          manuscript_id: manuscript.id,
          surname: editorialAssistantTeamMember.alias.surname,
          given_names: editorialAssistantTeamMember.alias.givenNames,
          title: editorialAssistantTeamMember.alias.title,
          email: editorialAssistantTeamMember.alias.email,
          aff: editorialAssistantTeamMember.alias.aff,
          country: editorialAssistantTeamMember.alias.country,
          role: 'editorialAssistant',
          role_label: 'Editorial Assistant',
          phenom_member_id: editorialAssistantTeamMember.id,
          status: 'active',
        })
      }
    })

    try {
      await screeningDB.transaction(async (trx) => {
        const result = await trx('member').insert(editorialAssistantMembers)

        console.log(`${result.rowCount} new members saved.`)
      })
    } catch (error) {
      console.error('Something went wrong while inserting...')
      throw error
    }
  } catch (err) {
    console.error(err)
    process.exit(1)
  }

  console.info(`Process completed`)
  process.exit(0)
})()
