const { S3 } = require('@aws-sdk/client-s3')
const { SQS } = require('@aws-sdk/client-sqs')
const config = require('config')

const logger = require('@pubsweet/logger')
const { S3EventProducer, SqsPublishConsumer } = require('@hindawi/eve')

const s3 = new S3({
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

const sqs = new SQS({
  credentials:
    config.has('aws.sns_sqs.secretAccessKey') &&
    config.has('aws.sns_sqs.accessKeyId')
      ? {
          secretAccessKey: config.get('aws.sns_sqs.secretAccessKey'),
          accessKeyId: config.get('aws.sns_sqs.accessKeyId'),
        }
      : undefined,
  endpoint: config.has('aws.s3.endpoint')
    ? config.get('aws.s3.endpoint')
    : undefined,
  region: config.get('aws.region'),
  s3ForcePathStyle: config.has('aws.s3.endpoint'),
})

async function processOldOrcIdEvents() {
  const s3EventProducer = new S3EventProducer(
    s3,
    config.get('aws.s3.eventStorageBucket'),
  )

  const sqsPublisher = new SqsPublishConsumer(
    sqs,
    config.get('aws.sqs.queueName'),
  )

  // eslint-disable-next-line no-restricted-syntax
  for await (const events of s3EventProducer.produce()) {
    let eventBody
    // eslint-disable-next-line no-restricted-syntax
    for (const event of events) {
      try {
        eventBody = JSON.parse(event.Message)
      } catch (e) {
        logger.warn(`Can't parse event ${JSON.stringify(event)}\n${e}`)
      }

      if (
        eventBody &&
        eventBody.event &&
        ['UserORCIDAdded', 'UserORCIDRemoved'].includes(
          eventBody.event.split(':').pop(),
        )
      ) {
        // eslint-disable-next-line no-await-in-loop
        await sqsPublisher.consume(event)
      }
    }
  }
}

processOldOrcIdEvents()
