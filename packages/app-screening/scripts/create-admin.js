/**
 * This script creates an admin in screening db + sso
 *
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***
 * KEYCLOAK_USERNAME=***
 * KEYCLOAK_PASSWORD=***
 * KEYCLOAK_SERVER_URL=***
 * KEYCLOAK_REALM=***
 * KEYCLOAK_CLIENTID=***
 * ```
 *
 * example:
 * - if user already exist in db or sso run :
 * `yarn create-admin email`
 *
 * - if it's a new user run:
 * yarn create-admin email firstName lastName password`
 *
 * - or run this if you want user to add his own password (user will receive an email to add his password):
 * `yarn create-admin email firstName lastName`
 *
 */

const Knex = require('knex')
const config = require('config')
const logger = require('@pubsweet/logger')
const KeycloakAdminClient = require('@keycloak/keycloak-admin-client').default
const { createUser } = require('./create-user')

async function createAdmin(user) {
  const knex = Knex({
    client: 'pg',
    connection: config.get('pubsweet-server.db'),
  })

  const kcAdminClient = await keycloakInit()

  await createUser(knex, kcAdminClient, user)
  await giveAdminRoleToUserInSSO(kcAdminClient, user.email)
  await giveAdminRoleToUserInDb(knex, user.email)

  await knex.destroy()
}

async function keycloakInit() {
  const kcAdminClient = new KeycloakAdminClient({
    baseUrl: config.get('keycloak.public.authServerURL'),
    realmName: 'master',
  })

  await kcAdminClient.auth({
    username: config.get('keycloak.admin.username'),
    password: config.get('keycloak.admin.password'),
    grantType: 'password',
    clientId: 'admin-cli',
  })

  kcAdminClient.setConfig({
    realmName: config.get('keycloak.public.realm'),
  })

  return kcAdminClient
}

async function giveAdminRoleToUserInDb(knex, email) {
  let adminTeam

  adminTeam = await knex.select('id').where({ name: 'Admin' }).from('team')

  if (!adminTeam.length) {
    adminTeam = await knex('team')
      .insert({
        name: 'Admin',
      })
      .returning(['id'])

    logger.info(`Admin team was created in db.`)
  }

  const userFromDb = await knex
    .select('user.id', 'team_user.role')
    .leftJoin('team_user', function () {
      this.on('team_user.user_id', '=', 'user.id')
    })
    .where({ email })
    .from('user')

  if (userFromDb.length && !userFromDb[0].role) {
    await knex('team_user').insert({
      role: 'admin',
      user_id: userFromDb[0].id,
      team_id: adminTeam[0].id,
    })

    logger.info(`Admin role added to "${email}" in db.`)
  }
}

async function giveAdminRoleToUserInSSO(kcAdminClient, email) {
  const users = await kcAdminClient.users.find({
    email,
  })

  const [client] = await kcAdminClient.clients.find({
    clientId: config.get('keycloak.public.clientID'),
  })

  const availableClientRoleMappings =
    await kcAdminClient.users.listAvailableClientRoleMappings({
      id: users[0].id,
      clientUniqueId: client.id,
    })

  const adminClientRole = availableClientRoleMappings.find(
    (clientRole) => clientRole.name === 'Admin',
  )

  if (adminClientRole) {
    await kcAdminClient.users.addClientRoleMappings({
      id: users[0].id,
      clientUniqueId: client.id,
      roles: [
        {
          id: adminClientRole.id,
          name: adminClientRole.name,
        },
      ],
    })

    logger.info(`Admin role added to "${email}" in sso.`)
  }
}

async function execute() {
  let [email, firstName, lastName, password] = process.argv.slice(2)
  await createAdmin({ email, firstName, lastName, password })
}

execute()
