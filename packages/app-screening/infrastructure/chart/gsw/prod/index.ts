import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod-gsw/screening/app-screening'],
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository: '918602980697.dkr.ecr.eu-west-1.amazonaws.com/app-screening',
      tag: 'latest',
    },
    ingressOptions: {
      rules: [
        {
          host: 'screening.lithosphere.geoscienceworld.org',
        },
        {
          host: 'screening.gsw-prod.phenom.pub',
        },
      ],
    },
    podDisruptionBudget: {
      minAvailable: 1,
    },
  },
}
