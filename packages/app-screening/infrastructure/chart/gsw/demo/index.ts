import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['demo-gsw/screening/app-screening'],
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'screening.demo-gsw.phenom.pub',
        },
      ],
    },
    podDisruptionBudget: {
      minAvailable: 1,
    },
  },
}
