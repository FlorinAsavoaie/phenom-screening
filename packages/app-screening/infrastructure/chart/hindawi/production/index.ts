import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod/screening/app-screening'],
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository: '918602980697.dkr.ecr.eu-west-1.amazonaws.com/app-screening',
      tag: 'latest',
    },
    replicaCount: 2,
    ingressOptions: {
      rules: [
        {
          host: 'screening.prod.phenom.pub',
        },
        {
          host: 'screening.hindawi.com',
        },
      ],
    },
    podDisruptionBudget: {
      minAvailable: 1,
    },
  },
}
