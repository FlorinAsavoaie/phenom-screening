import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['automation/screening/app-screening'],
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'screening.automation.phenom.pub',
        },
      ],
    },
  },
}
