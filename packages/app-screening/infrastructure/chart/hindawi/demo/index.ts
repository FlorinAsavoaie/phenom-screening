import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['demo/screening/app-screening'],
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'screening.demo.phenom.pub',
        },
      ],
    },
    podDisruptionBudget: {
      minAvailable: 1,
    },
  },
}
