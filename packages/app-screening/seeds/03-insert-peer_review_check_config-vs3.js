const { PeerReviewCheckConfig } = require('@pubsweet/models')

const config = {
  reviewersIdentityConfirmedAndSeniortyLevelConfirmed: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 1,
  },
  anyConcernsRaisedByReviewReportsAndRecommendations: {
    options: [
      { value: 'yes', withObservation: true },
      { value: 'no', withObservation: false },
    ],
    order: 2,
  },
  isThereAConflictOfInterestBetweenTheEditorAndTheAuthors: {
    options: [
      { value: 'yes', withObservation: true },
      { value: 'no', withObservation: false },
    ],
    order: 3,
  },
  isThereAConflictOfInterestBetweenTheReviewersAndTheAuthors: {
    options: [
      { value: 'yes', withObservation: true },
      { value: 'no', withObservation: false },
    ],
    order: 4,
  },
  emailsReceivedFromAuthorsRequireAction: {
    options: [
      { value: 'yes', withObservation: true },
      { value: 'no', withObservation: false },
    ],
    order: 5,
  },
  emailsReceivedFromEditorRequireAction: {
    options: [
      { value: 'yes', withObservation: true },
      { value: 'no', withObservation: false },
    ],
    order: 6,
  },
  emailsReceivedFromReviewersRequireAction: {
    options: [
      { value: 'yes', withObservation: true },
      { value: 'no', withObservation: false },
    ],
    order: 7,
  },
  editorialStaffsCommunicationIsAppropriate: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 8,
  },
  otherConcernsOnReviewProcess: {
    options: [
      { value: 'yes', withObservation: true },
      { value: 'no', withObservation: false },
    ],
    order: 9,
  },
  conflictOfInterestMention: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 10,
  },
  dataAvailabilityStatementApproved: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 11,
  },
}

exports.seed = async () => {
  const peerReviewCheckConfig = new PeerReviewCheckConfig({
    config,
    version: 3,
  })

  await peerReviewCheckConfig.save()
}
