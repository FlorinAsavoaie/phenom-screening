const { MaterialCheckConfig } = require('@pubsweet/models')

const config = {
  hasFrontPage: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 1,
  },
  hasAllFiguresAndTables: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
    ],
    order: 2,
  },
  hasAllSupplementaryMaterials: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 3,
  },
  hasPatiantParticipantConsentForm: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 4,
  },
  hasAcknowledgements: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 5,
  },
  hasClinicalStudyRegistrationNumber: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 6,
  },
  hasCoverLetter: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 7,
  },
  hasAllSections: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 8,
  },
  hasFinalVersionOfTheAuthorList: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 9,
  },
  hasAllAffiliationsListed: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 10,
  },
  hasConflictOfInterestMention: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 11,
  },
}

exports.seed = async () => {
  const materialCheckConfig = new MaterialCheckConfig({
    config,
    version: 1,
  })

  await materialCheckConfig.save()
}
