const { MaterialCheckConfig } = require('@pubsweet/models')

const config = {
  frontPageApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 1,
  },
  headersAndFootersApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
    ],
    order: 2,
  },
  figuresAndTablesPresentInPDFAndAllCited: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 3,
  },
  figuresProvidedInEditableFormat: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 4,
  },
  hasFileInEditableFormat: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 5,
  },
  supplementaryMaterialsApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 6,
  },
  hasPatientParticipantConsentFormApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 7,
  },
  hasAcknowledgementsApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 8,
  },
  clinicalStudyRegistrationNumberApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 9,
  },
  coverLetterApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 10,
  },
  allSectionsPresentAndApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 11,
  },
  conflictOfInterestMention: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 12,
  },
  dataAvailabilityStatementApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 13,
  },
  fundingStatementApproved: {
    options: [
      { value: 'yes', supportsAdditionalInfo: false },
      {
        value: 'no',
        supportsAdditionalInfo: true,
        type: 'text',
        label: 'observation',
      },
      { value: 'notRequired', supportsAdditionalInfo: false },
    ],
    order: 14,
  },
}

exports.seed = async () => {
  const materialCheckConfig = new MaterialCheckConfig({
    config,
    version: 2,
  })

  await materialCheckConfig.save()
}
