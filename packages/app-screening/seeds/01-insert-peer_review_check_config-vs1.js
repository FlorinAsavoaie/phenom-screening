const { PeerReviewCheckConfig } = require('@pubsweet/models')

const config = {
  reviewersIdentityConfirmed: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 1,
  },
  reviewersAndAuthorsHaveNoConflictOfInterest: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 2,
  },
  reviewerCommentsWereAddressed: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 3,
  },
  authorsCommunicationWithReviewersIsGood: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 4,
  },
  reviewersCommunicationWithAuthorsIsGood: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 5,
  },
  editorialStaffsCommunicationIsGood: {
    options: [
      { value: 'yes', withObservation: false },
      { value: 'no', withObservation: true },
    ],
    order: 6,
  },
}

exports.seed = async () => {
  const peerReviewCheckConfig = new PeerReviewCheckConfig({
    config,
    version: 1,
  })

  await peerReviewCheckConfig.save()
}
