const config = require('config')

const connection = config.get('pubsweet-server.db')

module.exports = {
  client: 'pg',
  connection,
  migrations: {
    directory: `${__dirname}/migrations`,
  },
}
