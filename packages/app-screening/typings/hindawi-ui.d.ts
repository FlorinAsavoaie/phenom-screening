declare module '@hindawi/ui' {
  import {
    SpaceProps,
    FontWeightProps,
    AlignItemsProps,
    FontSizeProps,
  } from 'styled-system'
  import {
    ContentPosition,
    ContentDistribution,
    FlexWrapProperty,
    LineHeightProperty,
    WhiteSpaceProperty,
  } from 'csstype'

  interface ActionLinkProps
    extends SpaceProps,
      AlignItemsProps,
      FontWeightProps {
    to?: string
    onClick?: (event: Event) => void
    icon?: string
  }
  export class ActionLink extends React.Component<
    React.PropsWithChildren<ActionLinkProps>
  > {}

  export class NotFound extends React.Component<SpaceProps> {}

  export interface ItemProps extends SpaceProps, AlignItemsProps {
    vertical?: boolean
    justify?: string
    flex?: number
  }
  export class Item extends React.Component<ItemProps> {}

  interface RowProps extends SpaceProps, FontWeightProps {
    justify?: ContentPosition | ContentDistribution
    alignItems?: ContentPosition | ContentDistribution
    flexWrap?: FlexWrapProperty
    flexDirection?: string
  }
  export class Row extends React.Component<React.PropsWithChildren<RowProps>> {}
  interface TextProps extends SpaceProps, FontWeightProps, FontSizeProps {
    error?: boolean
    lineHeight?: LineHeightProperty<number | string>
    customId?: boolean
    secondary?: boolean
    title?: string
    whiteSpace?: WhiteSpaceProperty
    display?: string
    ellipsis?: boolean
    small?: boolean
    color?: string
  }
  export class Text extends React.Component<
    React.PropsWithChildren<TextProps>
  > {}

  interface LabelProps extends FontSizeProps, FontWeightProps, SpaceProps {
    required?: boolean
  }
  export class Label extends React.Component<
    React.PropsWithChildren<LabelProps>
  > {}
  interface IconProps extends SpaceProps, FontSizeProps {
    icon: string
    bold?: boolean
    color?: string
    onClick?: null | (() => void)
    secondary?: boolean
  }
  export class Icon extends React.Component<
    React.PropsWithChildren<IconProps>
  > {}

  interface LogoProps {
    goTo?: () => void
    src?: string
    title?: string
    height?: number | string
  }
  export class Logo extends React.Component<LogoProps> {}

  export class SVGLogo extends React.Component<SpaceProps> {}

  type RightChildren = (() => React.ReactNode) | React.ReactNode
  interface ContextualBoxProps extends SpaceProps {
    /** Label of the contextual box. */
    label: string
    /** Component or html to be rendered on the right side. */
    rightChildren?: RightChildren
    startExpanded?: boolean
  }
  export class ContextualBox extends React.Component<
    React.PropsWithChildren<ContextualBoxProps>
  > {}
  export type Theme = any

  interface FetchingHookResult {
    setError: (fetchingError: string) => void
    setFetching: any
    isFetching: boolean
    fetchingError: string
  }
  export function useFetching(): FetchingHookResult

  interface LoaderProps extends SpaceProps {
    iconSize?: number
  }

  export class Loader extends React.Component<LoaderProps> {}

  class Author {
    id: string
    alias: {
      email: string
      name: {
        surname: string
        givenNames: string
      }
    }
    isSubmitting: boolean
  }
  interface AuthorTagListProps {
    authors: any
    withAffiliations?: boolean
    showAffiliation?: boolean
    toggleAffiliation?: boolean
    withTooltip?: boolean
    separator?: string
    affiliationList?: string[]
  }
  export class AuthorTagList extends React.Component<AuthorTagListProps> {}

  interface FormModalProps {
    title: string
    content?: any
    onSubmit: any
    cancelText: string
    confirmText?: string
    initialValues?: any
    isCurrentUser?: any
    hideModal: () => void
    validate?: (values: any) => any
  }
  export class FormModal extends React.Component<FormModalProps> {}

  export class ValidatedFormField extends React.Component<any> {}

  interface TextareaProps {}
  export class Textarea extends React.Component<TextareaProps> {}

  interface TabsProps {
    selectedTab?: number
  }
  export class Tabs extends React.Component<TabsProps> {}

  interface PaginationProps {
    totalCount: number
    toLast: () => any
    toFirst: () => any
    nextPage: () => any
    prevPage: () => any
    setPage: () => any
    page: number
    itemsPerPage: number
  }
  export class Pagination extends React.Component<PaginationProps> {}

  interface DefaultValidators {
    required: (
      value: null | undefined | string | unknown[],
    ) => 'Required' | undefined

    emailValidator?: 'Invalid email' | null | undefined
  }
  export const validators: DefaultValidators

  export class Dropdown extends React.Component<any> {}
  export class DropdownOption extends React.Component<any> {}

  export class MultiAction extends React.Component<any> {}

  interface DatePickerProps {
    value?: string | null
    onChange: any
    disabled?: boolean
    minDate?: Date
  }
  export class DatePicker extends React.Component<DatePickerProps> {}
}
