declare module 'hindawi-screening/config/publisher' {
  interface PublisherConfig {
    name: string
    emailData: {
      logo: string
      ctaColor: string
      logoLink: string
      privacy: string
      adress: string
      publisher: string
      footerText: string
    }
    accountsEmail: string
    technologyMail: string
    shortArticleTypes: string[]
  }
  export function getConfig(): PublisherConfig
}
