declare module '@pubsweet/ui' {
  import { SpaceProps, WidthProps } from 'styled-system'

  interface ButtonProps
    extends React.ButtonHTMLAttributes<HTMLButtonElement>,
      SpaceProps,
      WidthProps {
    primary?: true
    secondary?: true
    small?: true
  }
  export class Button extends React.Component<ButtonProps> {}

  interface SpinnerProps {
    size: number
  }
  export class Spinner extends React.Component<SpinnerProps> {}
  interface CheckboxProps {
    label: string
    checked: boolean
    onChange: any
    value?: string
    type?: string
  }
  export class Checkbox extends React.Component<CheckboxProps> {}

  interface HeadingProps
    extends React.HTMLAttributes<HTMLHeadingElement>,
      SpaceProps {}
  export class H1 extends React.Component<HeadingProps> {}
  export class H3 extends React.Component<HeadingProps> {}
  export class H4 extends React.Component<HeadingProps> {}

  interface DataParserProps {
    timestamp: string | object
    humanizeThreshold?: number
    dateFormat?: string
  }
  export class DateParser extends React.Component<DataParserProps> {}

  export class ErrorText extends React.Component {}

  export function useModal(args: {
    component: any
  }): { showModal: () => void; hideModal: () => void }

  interface ModalProps {
    justifyContent?: string
    modalKey: string
    onSubmit: (values: any) => void
    component: any
    cancelText?: string
    confirmText?: string
    subtitle?: string
    title?: string
    onConfirm?: () => void
  }
  export class Modal extends React.Component<ModalProps> {}

  interface TextFieldProps {
    disabled?: boolean
    onChange: (event: any) => any
    onKeyPress?: any
    placeholder?: string
    value: string
    'data-test-id'?: string
  }
  export class TextField extends React.Component<TextFieldProps> {}
}
declare module '@pubsweet/ui-toolkit' {
  export function th(name: string): any
  export function darken(color: string, intensity: number): any
}

declare module '@pubsweet/ui/src' {
  interface RadioProps {
    checked?: boolean
    disabled?: boolean
    color?: string
    inline?: boolean
    name?: string
    value: string
    label: string
    required?: boolean
    key?: string
    onChange?: (event: any) => void
    className?: string
    marginBetween?: number
  }
  export class Radio extends React.Component<RadioProps> {}
}
