import { useState } from 'react'
import { get } from 'lodash'
import { Loader } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { gql, useQuery } from '@apollo/client'
import { useKeycloak } from 'component-sso/client'

const currentScreeningUser = gql`
  query currentScreeningUser {
    currentScreeningUser {
      id
      surname
      givenNames
      email
      isAdmin
      isTeamLeadInAtLeastOneTeam
    }
  }
`

const Route = ({
  children,
  manuscriptCustomId,
  setManuscriptCustomId,
  keycloak,
}) => {
  const { loading, data } = useQuery(currentScreeningUser)
  if (loading)
    return (
      <Root>
        <Loader />
      </Root>
    )

  const currentUser = get(data, 'currentScreeningUser', null)

  if (!currentUser) {
    keycloak.logout()
    return null
  }

  return typeof children === 'function'
    ? children({
        currentUser,
        manuscriptCustomId,
        setManuscriptCustomId,
      })
    : children
}

const AuthenticatedKeycloakRoute = ({ children, keycloak }) => {
  const [manuscriptCustomId, setManuscriptCustomId] = useState('')
  if (keycloak.authenticated) {
    if (!window.localStorage.getItem('token')) {
      window.localStorage.setItem('token', keycloak.token)
    }
  } else {
    window.localStorage.removeItem('token')
    keycloak.login()
  }

  if (!window.localStorage.getItem('token')) {
    keycloak.login()
    return null
  }
  return (
    <Route
      keycloak={keycloak}
      manuscriptCustomId={manuscriptCustomId}
      setManuscriptCustomId={setManuscriptCustomId}
    >
      {children}
    </Route>
  )
}

const AuthenticatedRoute = ({ location, ...rest }) => {
  const keycloak = useKeycloak()

  if (keycloak) {
    return (
      <AuthenticatedKeycloakRoute
        keycloak={keycloak}
        location={location}
        {...rest}
      />
    )
  }
}

export { AuthenticatedRoute }

const Root = styled.div`
  margin-top: calc(${th('gridUnit')} * 3);
  background-color: ${th('white')};
  display: flex;
  justify-content: center;
`
