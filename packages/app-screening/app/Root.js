import { BrowserRouter } from 'react-router-dom'
import styled, { ThemeProvider } from 'styled-components'
import { Normalize } from 'styled-normalize'
import { th } from '@pubsweet/ui-toolkit'
import CustomApolloProvider from './graphql/CustomApolloProvider'

const Root = ({ routes, theme }) => (
  <>
    <Normalize />
    <BrowserRouter>
      <CustomApolloProvider>
        <ThemeProvider theme={theme}>
          <StyleRoot>{routes}</StyleRoot>
        </ThemeProvider>
      </CustomApolloProvider>
    </BrowserRouter>
  </>
)

export default Root

const StyleRoot = styled.div`
  background-color: ${th('colorBackground')};
  font-family: ${th('fontInterface')}, sans-serif;
  font-size: ${th('fontSizeBase')};
  color: ${th('colorText')};
  line-height: ${th('lineHeightBase')};

  * {
    box-sizing: border-box;
  }
`
