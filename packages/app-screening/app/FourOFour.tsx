import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useHistory } from 'react-router-dom'
import { Button, H1, H4 } from '@pubsweet/ui'
import { ActionLink, NotFound, SVGLogo } from '@hindawi/ui'
import { getConfig } from 'hindawi-screening/config/publisher'

const publisherConfig = getConfig()

const FourOFour: React.FC = () => {
  const history = useHistory()
  return (
    <Root>
      <SVGLogo />
      <H1 mt={4}>Page not found</H1>
      <H4 mt={4}>We couldn’t find the page you are looking for.</H4>
      <Button
        mt={6}
        onClick={(): void => history.push('/manuscripts')}
        primary
        small
      >
        GO TO DASHBOARD
      </Button>

      <NotFound mt={6} />

      <H4 mt={4}>
        In case of any urgent situation contact
        <ActionLink ml={1} to={`mailto:${publisherConfig.technologyMail}`}>
          {publisherConfig.technologyMail}.
        </ActionLink>
      </H4>
    </Root>
  )
}
export default FourOFour

// #region styles
const Root = styled.div`
  width: 100%;
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  padding-top: calc(${th('gridUnit')} * 18);

  rect {
    fill: transparent;
  }
`
// #endregion
