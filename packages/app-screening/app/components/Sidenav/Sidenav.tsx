import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { NavButton } from '.'

interface SidenavProps {
  isAdmin: boolean
}

const Sidenav: React.FC<SidenavProps> = ({ isAdmin }) => (
  <Root>
    <NavButton icon="manuscripts" label="Manuscripts" to="/manuscripts" />
    {isAdmin && (
      <>
        <NavButton exact icon="users" label="User management" to="/users" />
        <NavButton exact icon="settings" label="Settings" to="/settings" />
      </>
    )}
  </Root>
)

export { Sidenav }

// #region styles
const Root = styled.nav`
  background-color: ${th('mainTextColor')};
  min-width: calc(${th('gridUnit')} * 24);
  width: calc(${th('gridUnit')} * 24);
`
// #endregion
