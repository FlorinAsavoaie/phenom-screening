import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { NavLink } from 'react-router-dom'
import { Icon } from 'hindawi-shared/client/components'

interface NavButtonProps {
  label: string
  to: string
  exact?: boolean
  icon: string
}

const NavButton: React.FC<NavButtonProps> = ({
  label = 'navbutton',
  to,
  exact,
  icon,
  ...rest
}) => (
  <Root exact={exact} to={to} {...rest}>
    <Icon mb={2} name={icon} size={5} />
    {label}
  </Root>
)

export { NavButton }

// #region styles
const Root = styled(NavLink)`
  align-items: center;
  background-color: transparent;
  border-bottom: 1px solid #979797;
  color: ${th('white')};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  font-size: ${th('text.fontSizeBaseSmall')};
  font-family: ${th('defaultFont')};
  height: calc(${th('gridUnit')} * 16);
  justify-content: center;
  line-height: 1;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;

  &.active {
    background-color: ${th('white')};
    color: ${th('mainTextColor')};
    box-shadow: inset -2px 1px 0 0 #c4c4c4, inset 0 -1px 0 0 #c4c4c4;
    &:hover {
      color: ${th('mainTextColor')};
    }
  }
  &:hover {
    color: ${th('white')};
  }
`
// #endregion
