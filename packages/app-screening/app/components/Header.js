import { useCallback } from 'react'
import styled from 'styled-components'
import { Button } from '@pubsweet/ui'
import { Text } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import { withRouter } from 'react-router'

import { getConfig } from 'hindawi-screening/config/publisher'

const publisherConfig = getConfig()

const pathDictionaries = {
  '/manuscripts': 'Manuscripts Dashboard',
  '/users': 'User Management',
  '/settings': 'Settings',
  undefined: 'Screening',
}

const pathTitles = (path, manuscriptCustomId) => {
  if (path.includes('/manuscripts/') && manuscriptCustomId) {
    return `Manuscript ID: ${manuscriptCustomId}`
  }

  return pathDictionaries[path]
}

function Header({
  location,
  history,
  currentUser,
  keycloak,
  apolloClient,
  manuscriptCustomId,
}) {
  const goTo = useCallback((path) => history.push(path))
  const logout = useCallback(() => {
    apolloClient.resetStore()
    window.localStorage.removeItem('token')
    if (keycloak) {
      keycloak.logout()
    } else {
      history.replace('/login')
    }
  })
  const { surname, givenNames } = currentUser

  return (
    <Root>
      <LeftContainer>
        <Logo
          alt={publisherConfig.name}
          onClick={() => goTo('/')}
          src="/assets/logo.svg"
          title={publisherConfig.name}
        />
        <Separator />
        <Title>{pathTitles(location.pathname, manuscriptCustomId)}</Title>
      </LeftContainer>

      <RightContainer>
        <Text fontWeight={700} mr={2}>
          {givenNames} {surname}
        </Text>
        <Button onClick={logout} xs>
          Logout
        </Button>
      </RightContainer>
    </Root>
  )
}

// #region styles
const Root = styled.header`
  align-items: center;
  background-color: ${th('white')};
  box-shadow: ${th('appBar.boxShadow')};
  display: flex;
  height: calc(${th('gridUnit')} * 17);
  justify-content: space-between;
  padding-right: calc(${th('gridUnit')} * 4);
  position: relative;
`
const Separator = styled.div`
  height: calc(${th('gridUnit')} * 8);
  border-left: 1px solid #e0e0e0;
  margin-left: calc(${th('gridUnit')} * 6);
  margin-right: calc(${th('gridUnit')} * 6);
`

const Title = styled.div`
  display: flex;
  justify-content: start;
  font-weight: bold;
  font-size: ${th('h3LineHeight')};
  font-family: ${th('defaultFont')};
`
const LeftContainer = styled.div`
  align-items: center;
  display: flex;
  flex: 2;
  height: ${th('appBar.height')};
`

const RightContainer = styled.div`
  align-items: center;
  display: flex;
`
const Logo = styled.img`
  margin-left: calc(${th('gridUnit')} * 5);
`

// #endregion

export default withRouter(Header)
