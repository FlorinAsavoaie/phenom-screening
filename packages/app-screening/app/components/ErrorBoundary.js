import { Component } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withRouter } from 'react-router-dom'
import { Logo, ActionLink } from '@hindawi/ui'
import { H2, H3, Button } from '@pubsweet/ui'
import { getConfig } from 'hindawi-screening/config/publisher'

import { GlobalStyles, ServiceUnavailable } from './'

const publisherConfig = getConfig()

function ErrorPage({ goToDashboard }) {
  return (
    <Root>
      <Logo goTo={goToDashboard} height={90} src="/assets/logo.svg" />
      <Heading as={H2} mt={8}>
        Something is technically wrong.
      </Heading>
      <Heading as={H3} mt={4}>
        Thanks for noticing! We’re going to fix it up and have things back to
        normal soon.
      </Heading>

      <Button mt={4} onClick={goToDashboard} primary width={30}>
        GO TO DASHBOARD
      </Button>

      <ServiceUnavailable />
      <Heading as={H3} mt={5}>
        In case of any urgent situation contact{' '}
        <ActionLink
          ml={1}
          rel="noopener noreferrer"
          to={`mailto:${publisherConfig.technologyMail}`}
        >
          {publisherConfig.technologyMail}
        </ActionLink>
      </Heading>
    </Root>
  )
}

class ErrorBoundary extends Component {
  constructor(props) {
    super(props)

    this.state = {
      hasError: false,
    }
  }

  // eslint-disable-next-line class-methods-use-this
  componentDidCatch(error) {
    console.error(error.message)
  }

  static getDerivedStateFromError() {
    return {
      hasError: true,
    }
  }

  goToDashboard = () => {
    this.setState({ hasError: false }, () => {
      this.props.history.replace('/manuscripts')
    })
  }

  render() {
    const { hasError } = this.state

    return (
      <>
        <GlobalStyles />
        {hasError ? (
          <ErrorPage goToDashboard={this.goToDashboard} />
        ) : (
          this.props.children
        )}
      </>
    )
  }
}

export default withRouter(ErrorBoundary)

// #region styles
const Heading = styled.h1`
  height: auto;
`

const Root = styled.div`
  align-items: center;
  background-color: transparent;
  display: flex;
  flex-direction: column;
  height: 100vh;
  padding: calc(${th('gridUnit')} * 4);
  padding-top: calc(${th('gridUnit')} * 10);
`
// #endregion
