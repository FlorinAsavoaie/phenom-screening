import { SettingsTab } from 'component-settings/client'
import FourOFour from '../FourOFour'

interface SettingsRouteProps {
  isAdmin: boolean
}

const SettingsRoute: React.FC<SettingsRouteProps> = ({ isAdmin }) => {
  if (isAdmin) {
    return <SettingsTab />
  }
  return <FourOFour />
}

export { SettingsRoute }
