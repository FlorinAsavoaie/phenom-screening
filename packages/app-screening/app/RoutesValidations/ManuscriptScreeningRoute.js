import { lazy } from 'react'
import styled from 'styled-components'
import { useParams } from 'react-router-dom'
import { useQuery } from '@apollo/client'
import { chain } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { Loader, Row, Text } from '@hindawi/ui'
import { Icon } from 'hindawi-shared/client/components'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { queries } from 'component-screening-process/client/graphql'

import FourOFour from '../FourOFour'

const ManuscriptScreening = lazy(() =>
  import('component-screening-process/client/pages/ManuscriptScreening'),
)
const ManuscriptQualityCheck = lazy(() =>
  import(
    'component-quality-checks-process/client/pages/ManuscriptQualityCheck'
  ),
)

function ManuscriptScreeningRoute({ setManuscriptCustomId, currentUser }) {
  const { manuscriptId } = useParams()
  const { data, error, loading } = useQuery(queries.getScreeningManuscript, {
    variables: { manuscriptId },
    fetchPolicy: 'network-only',
  })

  if (loading) {
    return <Loader mt={4} mx="auto" />
  }

  const notFoundError = chain(error)
    .get('graphQLErrors', [])
    .filter(({ extensions }) => extensions.name === 'NotFoundError')
    .value()

  if (notFoundError.length) {
    return <FourOFour />
  } else if (error) {
    setManuscriptCustomId('')
    return <Error errorText={parseGQLError(error)} />
  }
  const { getScreeningManuscript: manuscript } = data

  setManuscriptCustomId(manuscript.customId)
  if (manuscript.flowType === 'screening') {
    return (
      <ManuscriptScreening currentUser={currentUser} manuscript={manuscript} />
    )
  }
  return (
    <ManuscriptQualityCheck currentUser={currentUser} manuscript={manuscript} />
  )
}

export default ManuscriptScreeningRoute

const Error = ({ errorText }) => (
  <Row justify="center" mt={4}>
    <Icon color="warningColor" name="warning" size={4} />
    <ErrorMessage error>{errorText}</ErrorMessage>
  </Row>
)

const ErrorMessage = styled(Text)`
  font-size: ${th('fontSizeBase')};
`
