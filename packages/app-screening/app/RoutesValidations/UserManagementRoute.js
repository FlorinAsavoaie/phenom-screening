import { lazy } from 'react'
import FourOFour from '../FourOFour'

const UserManagement = lazy(() =>
  import('component-user-management/client/pages/UserManagement'),
)

function UserManagementRoute({ isAdmin }) {
  if (isAdmin) {
    return <UserManagement />
  }
  return <FourOFour />
}

export default UserManagementRoute
