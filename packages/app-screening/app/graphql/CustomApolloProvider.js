import { ApolloProvider } from '@apollo/client'
import { withRouter } from 'react-router-dom'

import useConstant from './useConstant'
import apolloClientFactory from './apolloClientFactory'

export default withRouter(({ children, history }) => {
  const apolloClient = useConstant(() => apolloClientFactory(history))

  return <ApolloProvider client={apolloClient}>{children}</ApolloProvider>
})
