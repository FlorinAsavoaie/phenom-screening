import {
  ApolloClient,
  createHttpLink,
  ApolloLink,
  split,
  InMemoryCache,
} from '@apollo/client'
import { WebSocketLink } from '@apollo/client/link/ws'
import { getMainDefinition } from '@apollo/client/utilities'
import { setContext } from '@apollo/client/link/context'
import { onError } from '@apollo/client/link/error'

export default function apolloClientFactory(history) {
  const httpLink = createHttpLink({ uri: process.env.GATEWAY_URI })
  const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('token')
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : '',
      },
    }
  })

  const linkError = onError(
    ({ graphQLErrors, networkError, operation, forward }) => {
      if (graphQLErrors) {
        const authorizationError = graphQLErrors.filter(
          ({ extensions }) => extensions.name === 'AuthorizationError',
        )

        if (authorizationError.length) {
          window.localStorage.removeItem('token')
          history.go('/login')
        }
      }
    },
  )

  let link = ApolloLink.from([linkError, authLink, httpLink])

  if (process.env.SUBSCRIPTIONS_ENABLED) {
    const wsProtocol = window.location.protocol === 'https:' ? 'wss' : 'ws'
    const wsLink = new WebSocketLink({
      uri: `${wsProtocol}://${window.location.host}/subscriptions`,
      options: {
        reconnect: true,
        connectionParams: () => ({ authToken: localStorage.getItem('token') }),
      },
    })
    link = split(
      ({ query }) => {
        const { kind, operation } = getMainDefinition(query)
        return kind === 'OperationDefinition' && operation === 'subscription'
      },
      wsLink,
      link,
    )
  }

  const cache = new InMemoryCache()

  const config = {
    link,
    cache,
  }
  return new ApolloClient(config)
}
