import { lazy, Suspense, Fragment } from 'react'
import { Loader } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { useKeycloak } from 'component-sso/client'
import { Route, Switch, Redirect } from 'react-router-dom'
import { AuthenticatedRoute } from './AuthenticatedRoute'
import FourOFour from './FourOFour'
import { Header } from './components'
import { Sidenav } from './components/Sidenav'

import {
  ManuscriptScreeningRoute,
  UserManagementRoute,
} from './RoutesValidations'

import { SettingsRoute } from './RoutesValidations/SettingsRoute'

const Dashboard = lazy(() =>
  import('component-dashboard/client/pages/Dashboard'),
)

function AuthenticatedRoutes({ currentUser, setManuscriptCustomId }) {
  const { isAdmin } = currentUser
  return (
    <Suspense fallback={<Loader mx="auto" />}>
      <Switch>
        <Route
          component={() => <Redirect to="/manuscripts" />}
          exact
          path="/"
        />
        <Route
          component={() => <Dashboard currentUser={currentUser} />}
          exact
          path="/manuscripts"
        />
        <Route
          component={() => (
            <ManuscriptScreeningRoute
              currentUser={currentUser}
              setManuscriptCustomId={setManuscriptCustomId}
            />
          )}
          exact
          path="/manuscripts/:manuscriptId"
        />
        <Route
          component={(props) => (
            <UserManagementRoute {...props} isAdmin={isAdmin} />
          )}
          exact
          path="/users"
        />
        <Route
          component={(props) => <SettingsRoute {...props} isAdmin={isAdmin} />}
          exact
          path="/settings"
        />

        <Route component={FourOFour} />
      </Switch>
    </Suspense>
  )
}

function Routes({ apolloClient }) {
  const keycloak = useKeycloak()
  return (
    <Switch>
      <AuthenticatedRoute>
        {({ currentUser, manuscriptCustomId, setManuscriptCustomId }) => (
          <Fragment>
            <Header
              apolloClient={apolloClient}
              currentUser={currentUser}
              keycloak={keycloak}
              manuscriptCustomId={manuscriptCustomId}
            />
            <MainContainer>
              <Sidenav isAdmin={currentUser.isAdmin} />
              <AuthenticatedRoutes
                currentUser={currentUser}
                setManuscriptCustomId={setManuscriptCustomId}
              />
            </MainContainer>
          </Fragment>
        )}
      </AuthenticatedRoute>
    </Switch>
  )
}

export default Routes

// #region styles
const MainContainer = styled.div`
  display: flex;
  background-color: ${th('backgroundColor')};
  height: calc(100vh - ${th('gridUnit')} * 17);
`
// #endregion
