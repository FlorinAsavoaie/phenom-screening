import { render } from 'react-dom'
import { theme } from '@hindawi/ui'
import { KeycloakProvider, keycloak } from 'component-sso/client'

import Root from './Root'
import HindawiRoot from './HindawiRoot'
import './styles.css'

const renderSuccess = (keycloak) => {
  render(
    <KeycloakProvider keycloak={keycloak}>
      <Root routes={<HindawiRoot />} theme={theme} />
    </KeycloakProvider>,
    document.getElementById('root'),
  )
}
const renderError = (keycloakError = {}, keycloak) => {
  const { error = 'An error occurred.', error_description: errorDescription } =
    keycloakError
  if (error === 'access_denied') {
    keycloak.login()
  }
  render(
    <>
      <h1 style={{ textAlign: 'center' }}>
        {errorDescription ? error.error_description : error}
      </h1>
      <p style={{ textAlign: 'center' }}>
        <a href="/">Go back</a>
      </p>
    </>,
    document.getElementById('root'),
  )
}

const keycloakEnv = process.env.KEYCLOAK
if (keycloakEnv && keycloakEnv.authServerURL) {
  keycloak.init(keycloakEnv, renderSuccess, renderError)
} else {
  renderSuccess()
}
