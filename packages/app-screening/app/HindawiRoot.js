import { ModalProvider } from '@pubsweet/ui'
import { ApolloConsumer } from '@apollo/client'

import Routes from './Routes'
import { ErrorBoundary } from './components'

function HindawiRoot() {
  return (
    <ErrorBoundary>
      <ModalProvider>
        <ApolloConsumer>
          {(apolloClient) => <Routes apolloClient={apolloClient} />}
        </ApolloConsumer>
      </ModalProvider>
      <div id="ps-modal-root" style={{ height: 0 }} />
    </ErrorBoundary>
  )
}

export default HindawiRoot
