require('dotenv').config()
const path = require('path')
const { initializeRootLogger } = require('hindawi-utils')
const { get } = require('lodash')
const components = require('./components.json')
const { getConfig } = require('hindawi-screening/config/publisher')

Object.assign(global, require('@pubsweet/errors'))

const publisherConfig = getConfig()
const PORT = process.env.NODE_ENV === 'production' ? 3000 : 4000

module.exports = {
  authsome: {
    mode: path.resolve(__dirname, 'authsome-mode.js'),
  },
  pubsweet: {
    components,
  },
  queueDisabled: process.env.QUEUE_DISABLED === 'true',
  aws: {
    region: process.env.AWS_REGION,
    s3: {
      accessKeyId: process.env.AWS_S3_ACCESS_KEY,
      secretAccessKey: process.env.AWS_S3_SECRET_KEY,
      screeningBucket: process.env.AWS_S3_BUCKET,
      reviewBucket: process.env.REVIEW_AWS_S3_BUCKET,
      eventStorageBucket: process.env.AWS_S3_EVENT_STORAGE_BUCKET,
    },
    sns_sqs: {
      accessKeyId: process.env.AWS_SNS_SQS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SNS_SQS_SECRET_KEY,
    },
    sqs: {
      endpoint: process.env.AWS_SQS_ENDPOINT,
      queueName: process.env.AWS_SQS_QUEUE_NAME,
    },
    sns: {
      endpoint: process.env.AWS_SNS_ENDPOINT,
      topic: process.env.AWS_SNS_TOPIC,
    },
    ses: {
      accessKeyId: process.env.AWS_SES_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SES_SECRET_KEY,
    },
  },
  serviceSchemaUrl:
    process.env.SERVICE_SCHEMA_URL ||
    'http://host.docker.internal:4000/graphql',
  largeEvents: {
    s3Endpoint: process.env.AWS_S3_ENDPOINT,
    bucketName: process.env.PHENOM_LARGE_EVENTS_BUCKET,
    bucketPrefix:
      process.env.PHENOM_LARGE_EVENTS_PREFIX || 'in-transit-events/',
  },
  keycloak: {
    env: process.env.KEYCLOAK || null,
    certs: process.env.KEYCLOAK_CERTIFICATES,
    admin: {
      username: process.env.KEYCLOAK_USERNAME,
      password: process.env.KEYCLOAK_PASSWORD,
    },
    public: {
      realm: process.env.KEYCLOAK_REALM,
      clientID: process.env.KEYCLOAK_CLIENTID,
      authServerURL: process.env.KEYCLOAK_SERVER_URL,
      checkLoginIframe: JSON.parse(
        get(
          process.env,
          'KEYCLOACK_CHECK_LOGIN_IFRAME',
          process.env.NODE_ENV !== 'development',
        ),
      ),
    },
  },
  logLevel: process.env.LOG_LEVEL || 'silly',
  similarityChecksServiceService: process.env.SIMILARITY_CHECK_SERVICE_ENABLED,
  nodeEnv: process.env.NODE_ENV,
  mailer: {
    path: `${__dirname}/${
      JSON.parse(get(process, 'env.EMAIL_SENDING', true))
        ? 'mailer'
        : 'mailer-mock'
    }`,
  },
  reviewUrl: process.env.REVIEW_URL,
  orcidSandboxEnabled: process.env.ORCID_SANDBOX_ENABLED,
  // Configuration used by @pubsweet/component-email-templating
  journal: {
    logo: publisherConfig.emailData.logo,
    address: publisherConfig.emailData.address,
    privacy: publisherConfig.emailData.privacy,
    ctaColor: publisherConfig.emailData.ctaColor,
    logoLink: publisherConfig.emailData.logoLink,
    publisher: publisherConfig.emailData.publisher,
    footerText: '',
  },
  publicKeys: ['pubsweet-client', 'authsome'],
  passwordStrengthRegex: new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&,.?;\'*><)([}{}":`~+=_-\\|/])(?=.{6,128})',
  ),
  publisherConfig,
  eventNamespace: process.env.EVENT_NAMESPACE,
  defaultMessageAttributes: JSON.parse(
    process.env.DEFAULT_MESSAGE_ATTRIBUTES || '{}',
  ),
  emailSender: process.env.EMAIL_SENDER,
  serviceName: getNameFromPackageJson(),
  'pubsweet-server': {
    db: getDbConfig(),
    pool: { min: 0, max: 50 },
    ignoreTerminatedConnectionError: true,
    port: PORT,
    logger: initializeRootLogger('AppScreening'),
    secret: 'SECRET',
    morganLogFormat:
      ':remote-addr :method :url :graphql[operation] :status :response-time ms',
    pubSubMaxListeners: 100,
    apollo: {
      /*
       *
       * `subscriptionsEnabled` options is disbled by default since @apollo/gateway
       * is not supporting subscriptions at the moment.
       * The actual option and the usage of it should be removed once the
       * subscriptions mechanism works as expected in apollo.
       */
      subscriptionsEnabled: false,
    },
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    baseUrl: process.env.CLIENT_BASE_URL || `http://localhost:${PORT}`,
    'login-redirect': '/',
    theme: process.env.PUBSWEET_THEME,
  },
  'password-reset': {
    url:
      process.env.PUBSWEET_PASSWORD_RESET_URL ||
      `http://localhost:${PORT}/password-reset`,
    sender: process.env.PUBSWEET_PASSWORD_RESET_SENDER || 'dev@example.com',
    hoursUntilNextRequest: 24,
  },
  'confirm-account': {
    url: process.env.PUBSWEET_CONFIRM_ACCOUNT_URL || '/confirm-account',
  },
  'manuscript-dashboard': {
    url: process.env.PUBSWEET_MANUSCRIPT_DASHBOARD || '/manuscripts',
  },
  useWebpackCompile: JSON.parse(
    get(process.env, 'WEBPACK_COMPILE', process.env.NODE_ENV === 'development'),
  ),
}

function getNameFromPackageJson() {
  const filePath = path.resolve(process.cwd(), 'package.json')
  return require(filePath).name // eslint-disable-line import/no-dynamic-require
}

function getDbConfig() {
  if (process.env.DATABASE) {
    return {
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DATABASE || 'screening',
      host: process.env.DB_HOST,
      port: 5432,
      ssl: JSON.parse(
        get(
          process.env,
          'DB_SSL',
          process.env.NODE_ENV === 'production'
            ? '{"rejectUnauthorized": false}'
            : false,
        ),
      ),

      idleTimeoutMillis: 30000,
      connectionTimeoutMillis: 20000,
    }
  }
  return {
    database: process.env.DATABASE || 'screening',
  }
}
