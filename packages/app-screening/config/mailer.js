const { SES } = require('@aws-sdk/client-ses')

module.exports = {
  transport: {
    SES: new SES({
      credentials:
        process.env.AWS_SES_ACCESS_KEY && process.env.AWS_SES_SECRET_KEY
          ? {
              accessKeyId: process.env.AWS_SES_ACCESS_KEY,
              secretAccessKey: process.env.AWS_SES_SECRET_KEY,
            }
          : undefined,
      region: process.env.AWS_REGION,
    }),
  },
}
