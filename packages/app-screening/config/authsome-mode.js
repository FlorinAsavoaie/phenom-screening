/* eslint-disable max-params */
// eslint max-params: 0

const Promise = require('bluebird')

const authsomeMode = async (userId, { name, policies = [] }, object, context) =>
  Promise.reduce(
    policies,
    async (acc, policy) => {
      if (acc === false) return acc

      if (typeof policy === 'function') {
        return policy(userId, name, object, context)
      }
      if (typeof authsomePolicies[policy] === 'function') {
        return authsomePolicies[policy](userId, name, object, context)
      }
      throw new Error(
        `⛔️ Cannot find policy '${policy}' for action '${name}'.`,
      )
    },
    true,
  )

const authsomePolicies = {
  authenticatedUser(userId) {
    return !!userId
  },
  unauthenticatedUser(userId) {
    return !userId
  },
  isAdmin(userId, name, object, { models: { User } }) {
    return User.isAdmin(userId)
  },

  async isAdminOrTeamLead(userId, name, object, { models: { User } }) {
    const roles = [User.Roles.ADMIN, User.Roles.TEAM_LEADER]
    const userRole = await User.getUserRoleOnManuscript({
      userId,
      manuscriptId: object.manuscriptId,
    })

    return roles.includes(userRole)
  },
  async hasAccessToManuscript(
    userId,
    name,
    object,
    { models: { Manuscript } },
  ) {
    let manuscriptId
    if (object.manuscriptId) ({ manuscriptId } = object)
    else ({ manuscriptId } = object.input)

    return Manuscript.canAccessManuscript({ manuscriptId, userId })
  },
}

module.exports = authsomeMode
