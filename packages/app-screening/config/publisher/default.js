module.exports = {
  name: '',
  emailData: {
    logo: '',
    ctaColor: '#63a945',
    logoLink: '',
    privacy: '',
    address: '',
    publisher: '',
    footerTextTemplate: (_, recipientEmail) =>
      `This email was sent to ${recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a paper submitted to a journal published by Hindawi Limited`,
    rtc: {
      observationsDictionary: getRtcObservationsDictionary(),
    },
  },
  accountsEmail: 'accounts@hindawi.com',
  technologyMail: 'technology@hindawi.com',
  supportedShortArticleTypes: [
    'Corrigendum',
    'Erratum',
    'Expression of Concern',
    'Retraction',
    'Editorial',
    'Letter to the Editor',
  ],
}

function getRtcObservationsDictionary() {
  return {
    languageCheck: `
      All manuscripts submitted to our journals go through an initial screening
      process to ensure they are suitable to undergo the review process. Our
      journals only publish manuscripts fully composed in English. After
      reviewing your manuscript, I regret to inform you that it was found to be
      unsuitable for publication because it contained non-English content.
      <br/><br/>
      If you are interested in language editing assistance, we have partnered
      with Editage to provide English-language editing and translation services
      to authors prior to submission. Authors submitting to Hindawi who use this
      service will receive a 10% discount from Editage. For more information,
      please visit the following URL: http://hindawi.editage.com.
      `,
    wordCount: `
      All manuscripts submitted to our journals go through an initial screening
      process to ensure they are suitable to undergo the review process. We
      consider Research and Review Articles that are at least 1000 words in
      length.
      <br/><br/>\
      After reviewing your manuscript, I regret to inform you that it was found
      to be unsuitable for publication, since it was not of the required length.
      `,
    suspiciousKeywords: `
      Suspicious keywords were found within your manuscript submission, and we
      unfortunately cannot proceed any further with the publishing process.
      Details of the suspicious keywords found are shown in the Additional
      Comments below.
      `,
    iThenticate: `
      All manuscripts submitted to our journals go through an initial screening
      process to ensure they are suitable to undergo the review process. A
      significant similarity was found between your manuscript and other
      published article(s). As a participant of CrossCheck, we use iThenticate
      software to detect instances of overlapping and similar text in submitted
      manuscripts.
      <br/><br/>
      In light of the above and after reviewing your manuscript, I regret to
      inform you that it was found to be unsuitable for publication, since a
      significant similarity was found with another source.
      `,
    '': '',
  }
}
