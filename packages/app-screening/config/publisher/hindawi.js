module.exports = {
  name: 'Hindawi',
  emailData: {
    logo: 'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-hindawi.png',
    logoLink: 'https://hindawi.com',
    privacy:
      'Hindawi respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://www.hindawi.com/privacy/">privacy policy</a> for information on how we store, process, and safeguard your data.',
    address:
      'Hindawi Limited, 3rd Floor, Adam House, 1 Fitzroy Square, London, W1T 5HF, United Kingdom',
    publisher: 'Hindawi Limited',
    footerTextTemplate: (_, recipientEmail) =>
      `This email was sent to ${recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a paper submitted to a journal published by Hindawi Limited`,
    rtc: {
      observationsDictionary: getRtcObservationsDictionary(),
    },
  },
  shortArticleTypes: [
    'Corrigendum',
    'Erratum',
    'Editorial',
    'Expression of Concern',
    'Letter to the Editor',
    'Retraction',
  ],
}

function getRtcObservationsDictionary() {
  return {
    languageCheck: `
      All manuscripts submitted to our journals go through an initial screening
      process to ensure they are suitable to undergo the review process. 
      Our journals only publish manuscripts fully composed in English. After
      reviewing your manuscript, I regret to inform you that it was found to be
      unsuitable for publication because it contained non-English content.
      <br/><br/>
      If you are interested in more in-depth language editing assistance,
      Hindawi has partnered with leading author service providers to give our
      authors discounted rates on a range of language editing services - 
      visit
      <a href='https://www.hindawi.com/publish-research/authors/author-services/'>Author services</a>
        for more information.
      `,
  }
}
