module.exports = {
  name: 'Lithosphere',
  emailData: {
    logo: 'https://s3-eu-west-1.amazonaws.com/review.hindawi.com/logo-lithosphere.png',
    logoLink: 'https://pubs.geoscienceworld.org/',
    privacy:
      'GeoScienceWorld respects your right to privacy. Please see our <a style="color: #007e92; text-decoration: none;" href="https://pubs.geoscienceworld.org/pages/privacy-policy">privacy policy</a> for information on how we store, process, and safeguard your data.',
    address:
      'GeoScienceWorld, a nonprofit corporation conducting business in the Commonwealth of Virginia at 1750 Tysons Boulevard, Suite 1500, McLean, Virginia 22102.',
    publisher: 'GeoScienceWorld',
    footerTextTemplate: (_, recipientEmail) =>
      `This email was sent to ${recipientEmail}. You have received this email in regards to the account creation, submission, or peer review process of a paper submitted to Lithosphere, published by GeoScienceWorld and supported by our publishing partner, Hindawi Limited.`,
  },
  shortArticleTypes: [
    'Commentary',
    'Corrigendum',
    'Erratum',
    'Editorial',
    'Expression of Concern',
    'Letter to the Editor',
    'Retraction',
  ],
}
