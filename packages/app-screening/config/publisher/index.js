const { merge } = require('lodash')
const hindawi = require('./hindawi')
const gsw = require('./gsw')
const defaultConfig = require('./default')

const publishers = {
  hindawi,
  gsw,
}

function getConfig() {
  return merge(
    defaultConfig,
    publishers[process.env.PUBLISHER_NAME || 'hindawi'],
  )
}

module.exports = { getConfig }
