const express = require('express')

const onError = require('pubsweet/src/error-exit')
const { startServer } = require('pubsweet-server')

const start = async (webpack = true) => {
  let startedServer

  if (webpack) {
    const { build } = require('pubsweet/src/startup/build')
    const rawApp = express()
    await build(rawApp)
    startedServer = await startServer(rawApp)
  } else {
    startedServer = await startServer()
  }

  startedServer.on('error', onError)
  return startedServer
}

module.exports = start
