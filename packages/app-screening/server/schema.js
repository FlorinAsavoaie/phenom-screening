const config = require('config')
const { merge } = require('lodash')
const { buildSubgraphSchema } = require('@apollo/federation')
const { gql } = require('apollo-server-express')
const tryRequireRelative = require('pubsweet-server/src/helpers/tryRequireRelative')

const typeDefs = []
const resolvers = {}

// recursively merge in component types and resolvers
function getSchemaRecursively(componentName) {
  const component = tryRequireRelative(componentName)

  if (component.extending) {
    getSchemaRecursively(component.extending)
  }

  if (component.typeDefs) {
    typeDefs.push(component.typeDefs)
  }
  if (component.resolvers) {
    merge(resolvers, component.resolvers)
  }
}

config.get('pubsweet.components').forEach((componentName) => {
  getSchemaRecursively(componentName)
})

function getSchema() {
  return buildSubgraphSchema(getGraphQLSchemaModule({ typeDefs, resolvers }))
}

function getGraphQLSchemaModule({ typeDefs, resolvers }) {
  const subscriptionsEnabled = config.get(
    'pubsweet-server.apollo.subscriptionsEnabled',
  )

  const documentNode = gql`
    ${typeDefs}
  `

  if (subscriptionsEnabled) {
    return {
      typeDefs: documentNode,
      resolvers,
    }
  }

  const documentNodeWithoutSubscriptions = {
    ...documentNode,
    definitions: documentNode.definitions.filter(
      (documentNode) => documentNode.name.value !== 'Subscription',
    ),
  }

  const resolversWithoutSubscriptions = {
    ...resolvers,
  }
  delete resolversWithoutSubscriptions.Subscription

  return {
    typeDefs: documentNodeWithoutSubscriptions,
    resolvers: resolversWithoutSubscriptions,
  }
}

module.exports = {
  getSchema,
  ...getGraphQLSchemaModule({ typeDefs, resolvers }),
}
