const config = require('config')
const { execute, subscribe } = require('graphql')
const { SubscriptionServer } = require('subscriptions-transport-ws')
const logger = require('@pubsweet/logger')
const { token } = require('pubsweet-server/src/authentication')

const { getSchema } = require('./schema')

module.exports = {
  addSubscriptions: (server) => {
    const subscriptionsEnabled = config.get(
      'pubsweet-server.apollo.subscriptionsEnabled',
    )

    if (!subscriptionsEnabled) {
      return
    }

    const connectors = require('pubsweet-server/src/connectors')
    const helpers = require('pubsweet-server/src/helpers/authorization')

    SubscriptionServer.create(
      {
        schema: getSchema(),
        execute,
        subscribe,
        onConnect: (connectionParams, webSocket, context) => {
          if (!connectionParams.authToken) {
            throw new Error('Missing auth token')
          }
          return new Promise((resolve, reject) => {
            token.verify(connectionParams.authToken, (_, id) => {
              if (!id) {
                logger.info('Bad auth token')
                reject(new Error('Bad auth token'))
              }
              resolve({ user: id, connectors, helpers })
            })
          })
        },
      },
      {
        server,
        path: '/subscriptions',
      },
    )
  },
}
