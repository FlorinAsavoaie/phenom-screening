const config = require('config')
const passport = require('passport')
const { ApolloServer } = require('apollo-server-express')
const isEmpty = require('lodash/isEmpty')
const logger = require('@pubsweet/logger')
const errors = require('@pubsweet/errors')
const helpers = require('pubsweet-server/src/helpers/authorization')

const { getSchema } = require('./schema')

const authBearerAndPublic = passport.authenticate(['bearer', 'anonymous'], {
  session: false,
})

const hostname = config.has('pubsweet-server.hostname')
  ? config.get('pubsweet-server.hostname')
  : 'localhost'

const extraApolloConfig = config.has('pubsweet-server.apollo')
  ? config.get('pubsweet-server.apollo')
  : {}

const api = async (app) => {
  app.use('/graphql', authBearerAndPublic)
  const server = new ApolloServer({
    schema: getSchema(),
    context: ({ req, res }) => ({
      helpers,
      user: req.user,
      req,
      res,
    }),
    formatError: (err) => {
      const error = isEmpty(err.originalError) ? err : err.originalError

      logger.error(error.message, { error })

      const isPubsweetDefinedError = Object.values(errors).some(
        (pubsweetError) => error instanceof pubsweetError,
      )
      // err is always a GraphQLError which should be passed to the client
      if (!isEmpty(err.originalError) && !isPubsweetDefinedError)
        return {
          name: 'Server Error',
          message: 'Something went wrong! Please contact your administrator',
        }

      return {
        message: error.message,
        extensions: {
          code: err.extensions.code,
          name: error.name || 'GraphQLError',
        },
      }
    },
    playground: {
      subscriptionEndpoint: `ws://${hostname}:3000/subscriptions`,
    },
    ...extraApolloConfig,
  })

  await server.start()
  server.applyMiddleware({ app })
}

module.exports = api
