function formatMetadata(props) {
  const contextArray = []
  if (props && props.length) {
    props.forEach((prop) => {
      let context = ''
      if (typeof prop === 'object') {
        context = Object.keys(prop)
          .reduce((acc, cv) => `${acc} ${cv}=${prop[cv]}`, '')
          .trim()
      } else {
        context = prop
      }
      contextArray.push(context)
    })
    return contextArray
  }
  return []
}

function formatLog(level, applicationLabel, ...args) {
  const nowDate = new Date()
  const timestamp = nowDate.toISOString()
  const props = Array.prototype.slice.call(args).pop()

  let message = props.shift()
  message = typeof message === 'object' ? JSON.stringify(message) : message

  const contextArray = formatMetadata(props)

  return `${timestamp} ${level} ${applicationLabel} [${contextArray}] ${message}`
}

function formatErrorLog(level, applicationLabel, ...args) {
  const nowDate = new Date()
  const timestamp = nowDate.toISOString()
  const props = Array.prototype.slice.call(args).pop()

  const message = props.shift()
  if (typeof message === 'object') {
    // ^this means error is the first argument here

    return `${timestamp} ${level} ${applicationLabel} [] ${message.stack}`
  }

  // when error and error not the first argument, assume is the second one
  const error = props.shift()
  const contextArray = formatMetadata(props)

  return `${timestamp} ${level} ${applicationLabel} [${contextArray}] ${message} ${
    error?.stack ?? ''
  }`
}

function initializeLogger(serviceLabel) {
  return {
    ...global.console,
    info: (...args) =>
      global.console.info(formatLog('info', serviceLabel, args)),
    warn: (...args) =>
      global.console.warn(formatLog('warn', serviceLabel, args)),
    debug: (...args) =>
      global.console.debug(formatLog('debug', serviceLabel, args)),
    trace: (...args) =>
      global.console.trace(formatLog('trace', serviceLabel, args)),
    error: (...args) =>
      global.console.error(formatErrorLog('error', serviceLabel, args)),
  }
}

module.exports = {
  initializeLogger,
}
