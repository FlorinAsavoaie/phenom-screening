/*
  Middleware that applies authsome policies to every GraphQL resolver.
*/

const standardTypes = ['Query', 'Mutation']
const withAuthsome = (resolvers = {}, useCases = {}) =>
  Object.entries(resolvers).reduce(
    (acc, [gqlType, gqlResolvers]) => ({
      ...acc,
      [gqlType]: standardTypes.includes(gqlType)
        ? Object.entries(gqlResolvers).reduce(
            (acc, [name, fn]) => ({
              ...acc,
              [name]: async (parentQuery, params, ctx) => {
                await ctx.helpers.can(
                  ctx.user,
                  composeAuthomeCanObject({
                    name,
                    policies: useCases[`${name}UseCase`].authsomePolicies || [],
                  }),
                  composeAuthomeCanObject(params),
                )

                return fn(parentQuery, params, ctx)
              },
            }),
            {},
          )
        : gqlResolvers,
    }),
    {},
  )

function composeAuthomeCanObject(obj) {
  return {
    ...obj,
    toString() {
      return this.name || ''
    },
  }
}

module.exports = withAuthsome
