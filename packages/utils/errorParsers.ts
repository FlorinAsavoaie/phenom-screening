import { first } from 'lodash'
import { ApolloError } from '@apollo/client'

export function parseGQLError(error: ApolloError): string {
  const firstError = first(error.graphQLErrors)
  return firstError ? firstError.message : 'Something went wrong.'
}
