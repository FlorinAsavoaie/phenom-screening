module.exports = {
  getParsedTransactionModels,
}

function getParsedTransactionModels(arg) {
  return arg.slice(0, -1).reduce((acc, model) => {
    acc[model.name] = model
    return acc
  }, {})
}
