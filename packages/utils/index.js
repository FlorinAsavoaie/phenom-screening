const withAuthsome = require('./withAuthsome')
const { initializeLogger } = require('./loggerCustom')

module.exports = {
  withAuthsome,
  initializeRootLogger: initializeLogger,
}
