const logger = require('@pubsweet/logger')

const PERSON_TAXONOMY_TERM = 'person_taxonomy_term'

exports.up = async (knex) => {
  const personTaxonomyTermTableExists = await knex.schema.hasTable(
    PERSON_TAXONOMY_TERM,
  )

  if (!personTaxonomyTermTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(PERSON_TAXONOMY_TERM, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('person_id').notNullable()
        table.uuid('taxonomy_term_id').notNullable()
        table.integer('occurrences').notNullable()

        table.unique(['person_id', 'taxonomy_term_id'])

        table.foreign('person_id').references('person.id')
        table.foreign('taxonomy_term_id').references('taxonomy_term.id')
      })

      logger.info(`${PERSON_TAXONOMY_TERM} table created successfully.`)
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(PERSON_TAXONOMY_TERM)

    logger.info(`${PERSON_TAXONOMY_TERM} table dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
