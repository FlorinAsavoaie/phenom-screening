const logger = require('@pubsweet/logger')

const PERSON_EXPERTISE_AUDIT_LOG = 'person_expertise_audit_log'

exports.up = async (knex) => {
  const personExpertiseAuditLogTableExists = await knex.schema.hasTable(
    PERSON_EXPERTISE_AUDIT_LOG,
  )

  if (!personExpertiseAuditLogTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(PERSON_EXPERTISE_AUDIT_LOG, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('person_id').notNullable()
        table.string('mode').notNullable()
        table.jsonb('expertise').notNullable()

        table.foreign('person_id').references('person.id')
      })

      logger.info(`${PERSON_EXPERTISE_AUDIT_LOG} table created successfully`)
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(PERSON_EXPERTISE_AUDIT_LOG)

    logger.info(`${PERSON_EXPERTISE_AUDIT_LOG} table dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
