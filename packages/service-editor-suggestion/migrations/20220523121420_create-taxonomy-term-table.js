const logger = require('@pubsweet/logger')

const TAXONOMY_TERM_TABLE = 'taxonomy_term'

exports.up = async (knex) => {
  const taxonomyTermTableExists = await knex.schema.hasTable(
    TAXONOMY_TERM_TABLE,
  )

  if (!taxonomyTermTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.raw(`
    CREATE EXTENSION IF NOT EXISTS ltree;
    CREATE TABLE taxonomy_term (
      id uuid PRIMARY KEY,
      created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      term TEXT,
      parent_id uuid,
      parsed_term text,
      path LTREE,
      CONSTRAINT uniqe_path UNIQUE (path)
    );
    CREATE INDEX taxonomy_term_path_idx ON taxonomy_term USING GIST (path);
    CREATE OR REPLACE FUNCTION update_path() RETURNS TRIGGER AS $$
      DECLARE
          full_path ltree;
      BEGIN
          IF NEW.parent_id IS NULL THEN
              NEW.path = NEW.parsed_term::ltree;
          ELSEIF TG_OP = 'INSERT' OR OLD.parent_id IS NULL OR OLD.parent_id != NEW.parent_id THEN
              SELECT path || NEW.parsed_term::text FROM taxonomy_term WHERE id = NEW.parent_id INTO full_path;
              IF full_path IS NULL THEN
                  RAISE EXCEPTION 'Invalid parent_id %', NEW.parent_id;
              END IF;
              NEW.path = full_path;
          END IF;
          RETURN NEW;
      END;
    $$ LANGUAGE plpgsql;
    CREATE TRIGGER path_tgr
      BEFORE INSERT OR UPDATE ON taxonomy_term
      FOR EACH ROW EXECUTE PROCEDURE update_path();  
    `)

      logger.info(
        `${TAXONOMY_TERM_TABLE} table and trigger created successfully`,
      )
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.raw(`DROP TRIGGER path_tgr ON ${TAXONOMY_TERM_TABLE};
      DROP function update_path()
    `)
    await knex.schema.dropTable(TAXONOMY_TERM_TABLE)

    logger.info(`${TAXONOMY_TERM_TABLE} table and trigger dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
