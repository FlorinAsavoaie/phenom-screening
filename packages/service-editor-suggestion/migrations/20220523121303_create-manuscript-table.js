const logger = require('@pubsweet/logger')

const MANUSCRIPT_TABLE = 'manuscript'

exports.up = async (knex) => {
  const manuscriptTableExists = await knex.schema.hasTable(MANUSCRIPT_TABLE)

  if (!manuscriptTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(MANUSCRIPT_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('submission_id').unique()
        table.uuid('assignation_id').notNullable()
      })

      logger.info(`${MANUSCRIPT_TABLE} table created successfully`)
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(MANUSCRIPT_TABLE)

    logger.info(`${MANUSCRIPT_TABLE} table dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
