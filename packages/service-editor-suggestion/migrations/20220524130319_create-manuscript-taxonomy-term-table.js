const logger = require('@pubsweet/logger')

const MANUSCRIPT_TAXONOMY_TERM = 'manuscript_taxonomy_term'

exports.up = async (knex) => {
  const manuscriptTaxonomyTermTableExists = await knex.schema.hasTable(
    MANUSCRIPT_TAXONOMY_TERM,
  )

  if (!manuscriptTaxonomyTermTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(MANUSCRIPT_TAXONOMY_TERM, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())
        table.uuid('manuscript_id').notNullable()
        table.uuid('taxonomy_term_id').notNullable()
        table.integer('occurrences')

        table.unique(['manuscript_id', 'taxonomy_term_id'])

        table
          .foreign('manuscript_id')
          .references('manuscript.id')
          .onDelete('CASCADE')
        table.foreign('taxonomy_term_id').references('taxonomy_term.id')
      })

      logger.info(`${MANUSCRIPT_TAXONOMY_TERM} table created successfully.`)
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(MANUSCRIPT_TAXONOMY_TERM)

    logger.info(`${MANUSCRIPT_TAXONOMY_TERM} table dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
