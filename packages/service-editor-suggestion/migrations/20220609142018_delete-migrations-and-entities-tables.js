const logger = require('@pubsweet/logger')

const MIGRATIONS_TABLE = 'migrations'
const ENTITIES_TABLE = 'entities'

exports.up = async (knex) => {
  const migrationsTableExists = await knex.schema.hasTable(MIGRATIONS_TABLE)

  if (migrationsTableExists) {
    await knex.schema.dropTable(MIGRATIONS_TABLE)
  }

  const entitiesTableExists = await knex.schema.hasTable(ENTITIES_TABLE)

  if (entitiesTableExists) {
    await knex.schema.dropTable(ENTITIES_TABLE)
  }
}

exports.down = async (knex) => {
  await knex.schema
    .createTable(MIGRATIONS_TABLE, (table) => {
      table.text('id').primary().notNullable()
      table.timestamp('run_at').defaultTo(knex.fn.now())
    })
    .then((_) => logger.info(`${MIGRATIONS_TABLE} table created successfully`))
    .catch((err) => logger.error(err))
}
