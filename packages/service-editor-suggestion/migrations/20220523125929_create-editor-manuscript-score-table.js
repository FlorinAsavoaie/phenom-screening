const logger = require('@pubsweet/logger')

const EDITOR_MANUSCRIPT_SCORE_TABLE = 'editor_manuscript_score'

exports.up = async (knex) => {
  const editorManuscriptScoreTableExists = await knex.schema.hasTable(
    EDITOR_MANUSCRIPT_SCORE_TABLE,
  )

  if (!editorManuscriptScoreTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(EDITOR_MANUSCRIPT_SCORE_TABLE, (table) => {
        table.uuid('id').primary()
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.float('score', 6).notNullable()
        table.uuid('editor_id').notNullable()
        table.uuid('manuscript_id').notNullable()

        table.unique(['editor_id', 'manuscript_id'])

        table.foreign('editor_id').references('editor.id').onDelete('CASCADE')
        table
          .foreign('manuscript_id')
          .references('manuscript.id')
          .onDelete('CASCADE')

        table.index(
          'manuscript_id',
          'editor_manuscript_score_manuscript_id_index',
        )
      })

      logger.info(`${EDITOR_MANUSCRIPT_SCORE_TABLE} table created successfully`)
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(EDITOR_MANUSCRIPT_SCORE_TABLE)

    logger.info(`${EDITOR_MANUSCRIPT_SCORE_TABLE} table dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
