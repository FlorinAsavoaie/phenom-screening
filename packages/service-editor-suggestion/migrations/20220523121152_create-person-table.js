const logger = require('@pubsweet/logger')

const PERSON_TABLE = 'person'

exports.up = async (knex) => {
  const personTableExists = await knex.schema.hasTable(PERSON_TABLE)

  if (!personTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(PERSON_TABLE, (table) => {
        table.uuid('id').primary()
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('surname')
        table.text('given_names')
        table.text('email').unique().notNullable()
        table.text('aff')
        table.text('country')
        table.text('title')
        table.text('orcid')
      })

      logger.info(`${PERSON_TABLE} table created successfully`)
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(PERSON_TABLE)

    logger.info(`${PERSON_TABLE} table dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
