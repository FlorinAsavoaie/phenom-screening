const logger = require('@pubsweet/logger')

const EDITOR_TABLE = 'editor'

exports.up = async (knex) => {
  const editorTableExists = await knex.schema.hasTable(EDITOR_TABLE)

  if (!editorTableExists) {
    await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')

    try {
      await knex.schema.createTable(EDITOR_TABLE, (table) => {
        table.uuid('id').primary()
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.uuid('person_id').notNullable()
        table.uuid('assignation_id').notNullable()
        table.text('assignation_type').notNullable()
        table.json('role').notNullable()

        table.unique(['person_id', 'assignation_id', 'assignation_type'])

        table.foreign('person_id').references('person.id')
      })

      logger.info(`${EDITOR_TABLE} table created successfully`)
    } catch (e) {
      logger.error(e)
    }
  }
}

exports.down = async (knex) => {
  try {
    await knex.schema.dropTable(EDITOR_TABLE)

    logger.info(`${EDITOR_TABLE} table dropped successfully`)
  } catch (e) {
    logger.error(e)
  }
}
