const { factories, models } = require('./models')
const configService = require('config')
const logger = require('@pubsweet/logger')
const { transaction } = require('objection')

const wosDataRepository =
  require('@hindawi/lib-wos-data-store').wosDataRepository.initialize({
    configService,
  })
const services = require('./services')
const useCases = require('./use-cases')

const { getTransactionModels } = require('./services/utils')

const {
  Person,
  Editor,
  Manuscript,
  TaxonomyTerm,
  EditorManuscriptScore,
  PersonExpertiseAuditLog,
} = models

function getPersonTermsAndIngest(models) {
  return useCases.getPersonTermsAndIngest.initialize({
    models,
    factories,
    services: {
      ...services,
      wosDataRepository,
    },
    logger,
  })
}

function getIngestAcademicEditorUseCase(models) {
  return useCases.ingestAcademicEditor.initialize({
    models,
    factories,
    useCases: {
      calculateScoreForEditors: calculateScoreForEditorsUseCase(models),
      getPersonTermsAndIngest: getPersonTermsAndIngest(models),
    },
    logger,
  })
}

function calculateScoreForEditorsUseCase(models) {
  return useCases.calculateScoreForEditors.initialize({
    models,
    factories,
    services,
  })
}

function getRemoveAcademicEditorUseCase(models) {
  return useCases.removeAcademicEditor.initialize(models)
}

function getGeneratedEditorSuggestionListForManuscripts(models) {
  return useCases.generateEditorSuggestionListForManuscripts.initialize({
    models,
    factories,
    useCases: {
      calculateScoreForEditors: calculateScoreForEditorsUseCase(models),
    },
    services: { applicationEventBus },
  })
}

module.exports = {
  async JournalEditorAssigned(data) {
    return transaction(
      Person,
      Editor,
      Manuscript,
      TaxonomyTerm,
      EditorManuscriptScore,
      PersonExpertiseAuditLog,
      (...models) => {
        const transactionModels = getTransactionModels(models)
        return useCases.ingestAcademicEditorsFromJournal
          .initialize({
            models: transactionModels,
            useCases: {
              ingestAcademicEditor:
                getIngestAcademicEditorUseCase(transactionModels),
              generateEditorSuggestionListForManuscripts:
                getGeneratedEditorSuggestionListForManuscripts(
                  transactionModels,
                ),
            },
          })
          .execute(data)
      },
    )
  },
  async JournalSectionSpecialIssueEditorAssigned(data) {
    return transaction(
      Person,
      Editor,
      Manuscript,
      TaxonomyTerm,
      EditorManuscriptScore,
      PersonExpertiseAuditLog,
      (...models) => {
        const transactionalModels = getTransactionModels(models)
        return useCases.ingestAcademicEditorsFromSectionSpecialIssue
          .initialize({
            models: transactionalModels,
            useCases: {
              ingestAcademicEditor:
                getIngestAcademicEditorUseCase(transactionalModels),
              generateEditorSuggestionListForManuscripts:
                getGeneratedEditorSuggestionListForManuscripts(
                  transactionalModels,
                ),
            },
          })
          .execute(data)
      },
    )
  },
  async JournalSpecialIssueEditorAssigned(data) {
    return transaction(
      Person,
      Editor,
      Manuscript,
      TaxonomyTerm,
      EditorManuscriptScore,
      PersonExpertiseAuditLog,
      (...models) => {
        const transactionalModels = getTransactionModels(models)
        return useCases.ingestAcademicEditorsFromSpecialIssue
          .initialize({
            models: transactionalModels,
            useCases: {
              ingestAcademicEditor:
                getIngestAcademicEditorUseCase(transactionalModels),
              generateEditorSuggestionListForManuscripts:
                getGeneratedEditorSuggestionListForManuscripts(
                  transactionalModels,
                ),
            },
          })
          .execute(data)
      },
    )
  },
  async JournalEditorRemoved(data) {
    return transaction(
      Editor,
      Manuscript,
      EditorManuscriptScore,
      (...models) => {
        const transactionalModels = getTransactionModels(models)
        return useCases.removeAcademicEditorFromJournal
          .initialize({
            models: transactionalModels,
            useCases: {
              removeAcademicEditor:
                getRemoveAcademicEditorUseCase(transactionalModels),
            },
          })
          .execute(data)
      },
    )
  },
  async JournalSpecialIssueEditorRemoved(data) {
    return transaction(
      Editor,
      Manuscript,
      EditorManuscriptScore,
      (...models) => {
        const transactionalModels = getTransactionModels(models)
        return useCases.removeAcademicEditorFromSpecialIssue
          .initialize({
            models: transactionalModels,
            useCases: {
              removeAcademicEditor:
                getRemoveAcademicEditorUseCase(transactionalModels),
            },
          })
          .execute(data)
      },
    )
  },
  async JournalSectionSpecialIssueEditorRemoved(data) {
    return transaction(
      Editor,
      Manuscript,
      EditorManuscriptScore,
      (...models) => {
        const transactionalModels = getTransactionModels(models)
        return useCases.removeAcademicEditorFromSectionSpecialIssue
          .initialize({
            models: transactionalModels,
            useCases: {
              removeAcademicEditor:
                getRemoveAcademicEditorUseCase(transactionalModels),
            },
          })
          .execute(data)
      },
    )
  },
  async TriggeredPeerReview(data) {
    return useCases.generateEditorSuggestionList
      .initialize({
        services,
        factories,
        models,
        useCases: {
          calculateScoreForEditors: calculateScoreForEditorsUseCase(models),
        },
        logger,
      })
      .execute(data)
  },
  async SubmissionQualityCheckPassed(data) {
    return useCases.removeEditorScoresAndManuscript
      .initialize({ models })
      .execute(data)
  },
  async SubmissionRejected(data) {
    return useCases.removeEditorScoresAndManuscript
      .initialize({ models })
      .execute(data)
  },
}
