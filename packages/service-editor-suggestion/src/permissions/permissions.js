const { shield, and, or } = require('graphql-shield')
const { ApolloError } = require('apollo-server')
const logger = require('@pubsweet/logger')

const {
  isAuthenticated,
  isAdmin,
  isEditorialAssistant,
  isTriageEditor,
  isAcademicEditor,
} = require('./rules')

class InternalServerError extends ApolloError {
  constructor() {
    super(
      'Something went wrong! Please contact your administrator',
      'ERR_INTERNAL_SERVER',
    )
  }
}

const permissions = shield(
  {
    Query: {
      getEditorsKeywords: and(
        isAuthenticated,
        or(isAdmin, isEditorialAssistant, isTriageEditor),
      ),
      getPersonTaxonomyTermsCount: and(isAuthenticated, isAcademicEditor),
      getPersonTaxonomyTerms: and(isAuthenticated, isAcademicEditor),
      getTermsByParentId: and(isAuthenticated, isAcademicEditor),
      getSearchedTerms: and(isAuthenticated, isAcademicEditor),
    },
    Term: {
      isLeaf: and(isAuthenticated, isAcademicEditor),
      isAssigned: and(isAuthenticated, isAcademicEditor),
    },
    Mutation: {
      editExpertise: and(isAuthenticated, isAcademicEditor),
    },
  },
  {
    fallbackError: (thrownError) => {
      if (thrownError instanceof ApolloError) {
        return thrownError
      } else if (thrownError instanceof Error) {
        logger.error(thrownError)

        return new InternalServerError()
      }

      logger.error('The resolver threw something that is not an error.')
      logger.error(thrownError)

      return new InternalServerError()
    },
  },
)

module.exports = {
  permissions,
}
