const { rule } = require('graphql-shield')

const { extractRoles } = require('./tokenUtils')
const { Roles } = require('./roles')

const isTriageEditor = rule()(async (_, __, context) => {
  const roles = await extractRoles(context.user)

  return (
    roles.includes(Roles.TRIAGE_EDITOR) ||
    new Error('User is not triage editor')
  )
})

module.exports = {
  isTriageEditor,
}
