const { rule } = require('graphql-shield')

const { extractRoles } = require('./tokenUtils')
const { Roles } = require('./roles')

const isEditorialAssistant = rule()(async (_, __, context) => {
  const roles = await extractRoles(context.user)

  return (
    roles.includes(Roles.EDITORIAL_ASSISTANT) ||
    new Error('User is not editorial assistant')
  )
})

module.exports = {
  isEditorialAssistant,
}
