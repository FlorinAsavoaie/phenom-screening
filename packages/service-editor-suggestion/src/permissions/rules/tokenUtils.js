const jwt = require('jsonwebtoken')
const jwksClient = require('jwks-rsa')
const config = require('config')
const { get } = require('lodash')

const SSO_CLIENT_ID = config.get('keycloak.ssoClientId')

async function extractRoles(user) {
  return get(user, `resource_access.${SSO_CLIENT_ID}.roles`, [])
}

async function verifyToken(authorizationHeader = '') {
  return new Promise((resolve, reject) => {
    const token = authorizationHeader.split(' ')[1]

    jwt.verify(
      token,
      getKey,
      {
        algorithms: ['RS256'],
      },
      (err, decoded) => {
        if (err || !decoded) {
          reject(err)
          return
        }

        resolve(decoded)
      },
    )
  })
}

async function getKey(header, callback) {
  const key = await getJwksClient().getSigningKey(header.kid)
  const signingKey = key.publicKey || key.rsaPublicKey
  callback(null, signingKey)
}

function getJwksClient() {
  return jwksClient({
    cache: true,
    jwksUri: config.get('keycloak.certs'),
  })
}

module.exports = {
  extractRoles,
  verifyToken,
}
