const { rule } = require('graphql-shield')

const { extractRoles } = require('./tokenUtils')
const { Roles } = require('./roles')

const isAcademicEditor = rule()(async (_, __, context) => {
  const roles = await extractRoles(context.user)

  return (
    roles.includes(Roles.ACADEMIC_EDITOR) ||
    new Error('User is not an academic editor')
  )
})

module.exports = {
  isAcademicEditor,
}
