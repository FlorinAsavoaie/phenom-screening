const { isAuthenticated } = require('./isAuthenticated')
const { isAdmin } = require('./isAdmin')
const { isEditorialAssistant } = require('./isEditorialAssistant')
const { isTriageEditor } = require('./isTriageEditor')
const { isAcademicEditor } = require('./isAcademicEditor')

module.exports = {
  isAuthenticated,
  isAdmin,
  isEditorialAssistant,
  isTriageEditor,
  isAcademicEditor,
}
