const { rule } = require('graphql-shield')

const { extractRoles } = require('./tokenUtils')
const { Roles } = require('./roles')

const isAdmin = rule()(async (_, __, context) => {
  const roles = await extractRoles(context.user)

  return roles.includes(Roles.ADMIN) || new Error('User is not admin')
})

module.exports = {
  isAdmin,
}
