const Roles = {
  ADMIN: 'admin',
  EDITORIAL_ASSISTANT: 'editorial_assistant',
  TRIAGE_EDITOR: 'triage_editor',
  ACADEMIC_EDITOR: 'academic_editor',
}

module.exports = {
  Roles,
}
