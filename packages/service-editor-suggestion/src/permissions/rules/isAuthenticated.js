const { rule } = require('graphql-shield')

const isAuthenticated = rule()(async (_, __, context) => {
  if (context.user) {
    return true
  }
  return new Error('User is not authenticated')
})

module.exports = {
  isAuthenticated,
}
