const fs = require('fs')
const path = require('path')
const { gql } = require('apollo-server')

const resolvers = require('./resolvers')
const { permissions } = require('./permissions')

module.exports = {
  resolvers,
  typeDefs: gql(
    fs.readFileSync(path.join(__dirname, 'typeDefs.graphqls'), 'utf8'),
  ),
  permissions,
}
