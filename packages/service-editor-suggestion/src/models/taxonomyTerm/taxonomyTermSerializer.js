function serialize(term) {
  return {
    id: term.id,
    parentId: term.parentId,
    name: term.term,
    path: normalizePath(term.path),
    depth: term.depth,
    occurrences: term.occurrences,
  }
}

function normalizePath(path) {
  return path.replace(/_/g, ' ').replace(/\./g, '/')
}

module.exports = {
  serialize,
}
