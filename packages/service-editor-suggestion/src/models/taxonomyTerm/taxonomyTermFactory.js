const TaxonomyTerm = require('./taxonomyTerm')

module.exports = {
  createTerm({ term, parsedTerm, parentId }) {
    return new TaxonomyTerm({ term, parsedTerm, parentId })
  },
}
