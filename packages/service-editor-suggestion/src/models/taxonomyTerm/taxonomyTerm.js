const BaseModel = require('../baseModel')

class TaxonomyTerm extends BaseModel {
  static get tableName() {
    return 'taxonomy_term'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        term: { type: 'string' },
        parsedTerm: { type: 'string' },
        parentId: { type: ['string', null], format: 'uuid' },
        path: { type: 'string' },
      },
      additionalProperties: true,
    }
  }

  get depth() {
    return this.path.split('.').length
  }

  static get relationMappings() {
    return {
      persons: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: require('./../person').Person,
        join: {
          from: 'taxonomy_term.id',
          through: {
            from: 'person_taxonomy_term.termId',
            to: 'person_taxonomy_term.personId',
            extra: ['occurrences'],
          },

          to: 'person.id',
        },
      },
      manuscripts: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: require('./../manuscript').Manuscript,
        join: {
          from: 'taxonomy_term.id',
          through: {
            from: 'manuscript_taxonomy_term.termId',
            to: 'manuscript_taxonomy_term.manuscriptId',
            extra: ['occurrences'],
          },

          to: 'manuscript.id',
        },
      },
    }
  }

  static async getTermsByParentId(parentId) {
    return TaxonomyTerm.query().andWhere('parent_id', parentId).orderBy('term')
  }

  static async getLeafsByIds(termsIds) {
    return TaxonomyTerm.query()
      .from('taxonomy_term as tt1')
      .whereNotExists((builder) =>
        builder
          .from('taxonomy_term as tt2')
          .whereRaw('tt1.path <> tt2.path')
          .whereRaw('tt1.path @> tt2.path'),
      )
      .whereIn('id', termsIds)
  }

  static async isLeaf(termId) {
    const terms = await TaxonomyTerm.getLeafsByIds([termId])
    return terms.length === 1
  }

  static async isAssigned(termId, personId) {
    const count = await TaxonomyTerm.query()
      .leftJoin(
        'person_taxonomy_term',
        'person_taxonomy_term.taxonomy_term_id',
        '=',
        'taxonomy_term.id',
      )
      .where({
        'taxonomy_term.id': termId,
        person_id: personId,
      })
      .resultSize()

    return count > 0
  }

  static async getSearchedTerms(searchTerm) {
    const term = searchTerm.replace(/\W/g, '_')
    const ltxtquery = `${term}*@%`

    return TaxonomyTerm.query()
      .whereRaw('path @ ?', [ltxtquery])
      .andWhere((builder) =>
        builder
          .whereRaw('nlevel(path) = 1')
          .orWhereRaw('NOT subltree (path, 0, nlevel (path) - 2) @ ?', [
            ltxtquery,
          ]),
      )
  }

  static async getAscendants(paths) {
    if (paths.length === 0) return []

    const serializedPaths = paths.map((path) => `'${path}'`) // paths need to be quoted

    return TaxonomyTerm.query().whereRaw(
      `ARRAY[${serializedPaths}::ltree] <@ path `,
    )
  }

  static async findInByPersonId({ termsIds, personId }) {
    return this.query()
      .select('taxonomy_term.*', 'occurrences')
      .leftJoin(
        'person_taxonomy_term',
        'person_taxonomy_term.taxonomy_term_id',
        '=',
        'taxonomy_term.id',
      )
      .whereIn('taxonomy_term.id', termsIds)
      .andWhere('person_id', personId)
  }
}

module.exports = TaxonomyTerm
