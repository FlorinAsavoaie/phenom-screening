const TaxonomyTerm = require('./taxonomyTerm')
const taxonomyTermFactory = require('./taxonomyTermFactory')
const taxonomyTermSerializer = require('./taxonomyTermSerializer')

module.exports = {
  TaxonomyTerm,
  taxonomyTermFactory,
  taxonomyTermSerializer,
}
