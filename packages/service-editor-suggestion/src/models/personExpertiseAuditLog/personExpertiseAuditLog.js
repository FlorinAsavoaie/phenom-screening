const BaseModel = require('../baseModel')

class PersonExpertiseAuditLog extends BaseModel {
  static get tableName() {
    return 'person_expertise_audit_log'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        personId: {
          type: 'string',
        },
        mode: {
          type: 'string',
          enum: Object.values(this.Modes),
        },
        expertise: {
          type: 'json',
        },
      },
    }
  }

  static get Modes() {
    return { AUTO: 'auto', MANUAL: 'manual' }
  }
}

module.exports = PersonExpertiseAuditLog
