const PersonExpertiseAuditLog = require('./personExpertiseAuditLog')

const { taxonomyTermSerializer } = require('../taxonomyTerm')

module.exports = {
  createPersonExpertiseAuditLog({
    personId,
    mode,
    assignedTerms,
    unassignedTerms,
  }) {
    return new PersonExpertiseAuditLog({
      personId,
      mode,
      expertise: {
        assigned: assignedTerms.map(taxonomyTermSerializer.serialize),
        unassigned: unassignedTerms.map(taxonomyTermSerializer.serialize),
      },
    })
  },
}
