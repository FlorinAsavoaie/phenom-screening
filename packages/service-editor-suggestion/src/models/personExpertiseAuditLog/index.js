const PersonExpertiseAuditLog = require('./personExpertiseAuditLog')
const personExpertiseAuditLogFactory = require('./personExpertiseAuditLogFactory')

module.exports = {
  PersonExpertiseAuditLog,
  personExpertiseAuditLogFactory,
}
