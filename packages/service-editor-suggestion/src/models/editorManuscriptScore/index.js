const EditorManuscriptScore = require('./editorManuscriptScore')
const editorManuscriptScoreFactory = require('./editorManuscriptScoreFactory')

module.exports = {
  EditorManuscriptScore,
  editorManuscriptScoreFactory,
}
