const BaseModel = require('../baseModel')

class EditorManuscriptScore extends BaseModel {
  static get tableName() {
    return 'editor_manuscript_score'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        score: {
          type: 'real',
        },
        editorId: {
          type: 'string',
          format: 'uuid',
        },
        manuscriptId: {
          type: 'string',
          format: 'uuid',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: require('./../manuscript').Manuscript,
        join: {
          from: `${this.tableName}.manuscriptId`,
          to: 'manuscript.id',
        },
      },
      editors: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: require('./../editor').Editor,
        join: {
          from: `${this.tableName}.editorId`,
          to: 'editor.id',
        },
      },
    }
  }
}
module.exports = EditorManuscriptScore
