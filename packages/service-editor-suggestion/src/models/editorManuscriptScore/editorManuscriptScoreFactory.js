const EditorManuscriptScore = require('./editorManuscriptScore')

module.exports = {
  createEditorManuscriptScore({ score, manuscriptId, editorId, id }) {
    if (id) {
      return new EditorManuscriptScore({
        id,
        score,
      })
    }
    return new EditorManuscriptScore({
      score,
      manuscriptId,
      editorId,
    })
  },
}
