const Editor = require('./editor')

module.exports = {
  createEditorProfile({ editor, assignationId, assignationType }) {
    return new Editor({
      id: editor.id,
      role: editor.role,
      assignationId,
      assignationType,
    })
  },
}
