const BaseModel = require('../baseModel')

class Editor extends BaseModel {
  static get tableName() {
    return 'editor'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        personId: {
          type: 'uuid',
        },
        role: {
          type: 'json',
        },
        assignationId: {
          type: 'string',
        },
        assignationType: {
          type: 'string',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      person: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: require('./../person').Person,
        join: {
          from: 'editor.personId',
          to: 'person.id',
        },
      },
      editorManuscriptScores: {
        relation: BaseModel.HasManyRelation,
        modelClass: require('./../editorManuscriptScore').EditorManuscriptScore,
        join: {
          from: 'editor.id',
          to: 'editor_manuscript_score.editorId',
        },
      },
    }
  }

  static get AssignationTypes() {
    return {
      JOURNAL: 'journal',
      SPECIAL_ISSUE: 'specialIssue',
      SECTION_SPECIAL_ISSUE: 'sectionSpecialIssue',
    }
  }

  static async findByEmail({ email, eagerLoadRelations }) {
    const object = await Editor.query()
      .leftJoin('person', 'person.id', '=', 'editor.personId')
      .limit(1)
      .where({
        'person.email': email,
      })
      .withGraphFetched(eagerLoadRelations)

    if (!object.length) {
      return
    }

    return object[0]
  }

  static async getTerms({ assignationId, manuscriptTerms }) {
    const manuscriptTermsIds = manuscriptTerms.map((manTerms) => manTerms.id)

    return this.query()
      .where({ assignationId })
      .withGraphFetched('person.[terms(selectManuscriptTerms)]')
      .modifiers({
        selectManuscriptTerms: (builder) => {
          builder.whereIn('term.id', manuscriptTermsIds)
        },
      })
  }

  static async findAllBy({ queryObject, excludedIds }) {
    return Editor.query().where(queryObject).whereNotIn('id', excludedIds)
  }
}

module.exports = Editor
