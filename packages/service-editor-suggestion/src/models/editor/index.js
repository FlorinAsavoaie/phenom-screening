const Editor = require('./editor')
const editorFactory = require('./editorFactory')

module.exports = {
  Editor,
  editorFactory,
}
