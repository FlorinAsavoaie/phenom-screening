const BaseModel = require('../baseModel')

class Manuscript extends BaseModel {
  static get tableName() {
    return 'manuscript'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        submissionId: {
          type: 'string',
          format: 'uuid',
        },
        assignationId: {
          type: 'string',
        },
        assignationType: {
          type: 'string',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      terms: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: require('./../taxonomyTerm').TaxonomyTerm,
        join: {
          from: 'manuscript.id',
          through: {
            from: 'manuscript_taxonomy_term.manuscriptId',
            to: 'manuscript_taxonomy_term.taxonomyTermId',
            extra: ['occurrences'],
          },
          to: 'taxonomy_term.id',
        },
      },
      editorManuscriptScores: {
        relation: BaseModel.HasManyRelation,
        modelClass: require('./../editorManuscriptScore').EditorManuscriptScore,
        join: {
          from: `${this.tableName}.id`,
          to: 'editor_manuscript_score.manuscriptId',
        },
      },
    }
  }

  async getManuscriptTerms() {
    return this.$relatedQuery('terms').select('taxonomy_term.*', 'occurrences')
  }
}
module.exports = Manuscript
