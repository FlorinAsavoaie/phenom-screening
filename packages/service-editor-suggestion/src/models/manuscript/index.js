const Manuscript = require('./manuscript')
const manuscriptFactory = require('./manuscriptFactory')

module.exports = {
  Manuscript,
  manuscriptFactory,
}
