const Manuscript = require('./manuscript')

module.exports = {
  createManuscript({ manuscriptId, assignationId, submissionId }) {
    return new Manuscript({
      id: manuscriptId,
      submissionId,
      assignationId,
    })
  },
}
