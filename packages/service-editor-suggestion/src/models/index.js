const { Editor, editorFactory } = require('./editor')
const { Manuscript, manuscriptFactory } = require('./manuscript')
const { Person, personFactory } = require('./person')
const {
  TaxonomyTerm,
  taxonomyTermFactory,
  taxonomyTermSerializer,
} = require('./taxonomyTerm')
const {
  EditorManuscriptScore,
  editorManuscriptScoreFactory,
} = require('./editorManuscriptScore')

const {
  PersonExpertiseAuditLog,
  personExpertiseAuditLogFactory,
} = require('./personExpertiseAuditLog')

module.exports = {
  models: {
    Editor,
    EditorManuscriptScore,
    Manuscript,
    Person,
    TaxonomyTerm,
    PersonExpertiseAuditLog,
  },
  factories: {
    editorFactory,
    personFactory,
    taxonomyTermFactory,
    manuscriptFactory,
    editorManuscriptScoreFactory,
    personExpertiseAuditLogFactory,
  },
  serializers: {
    taxonomyTermSerializer,
  },
}
