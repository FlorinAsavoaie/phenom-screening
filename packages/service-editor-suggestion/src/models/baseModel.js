const logger = require('@pubsweet/logger')
const BaseModel = require('@pubsweet/base-model')
const { NotFoundError } = require('@pubsweet/errors')

class ServiceBaseModel extends BaseModel {
  static async find(id, eagerLoadRelations) {
    const object = await this.query()
      .findById(id)
      .withGraphFetched(eagerLoadRelations)

    if (!object) {
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)
    }

    return object
  }

  static async findOneBy({ queryObject, eagerLoadRelations }) {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .withGraphFetched(eagerLoadRelations)

    if (!object.length) {
      return
    }

    return object[0]
  }

  static async findBy(queryObject, eagerLoadRelations) {
    return this.query().where(queryObject).withGraphFetched(eagerLoadRelations)
  }

  static async findIn(field, options, eagerLoadRelations) {
    return this.query()
      .whereIn(field, options)
      .withGraphFetched(eagerLoadRelations)
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  } = {}) {
    return this.query()
      .where(queryObject)
      .orderBy(orderByField, order)
      .withGraphFetched(eagerLoadRelations)
  }

  static async saveAll(entities) {
    try {
      await this.query().insert(entities)
    } catch (err) {
      logger.error(err)
      throw new Error(err)
    }
  }

  static async upsertGraph(object, options) {
    if (this.knex().isTransaction) {
      return this.query().upsertGraph(object, options)
    }

    return this.knex().transaction(async (trx) =>
      this.query(trx).upsertGraph(object, options),
    )
  }

  static async insertGraph(object, options) {
    if (this.knex().isTransaction) {
      return this.query().insertGraph(object, options)
    }

    return this.knex().transaction(async (trx) =>
      this.query(trx).insertGraph(object, options),
    )
  }

  static async deleteByIds(ids) {
    return this.query().delete().whereIn('id', ids)
  }
}

module.exports = ServiceBaseModel
