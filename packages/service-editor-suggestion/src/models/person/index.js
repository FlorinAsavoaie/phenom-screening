const Person = require('./person')
const personFactory = require('./personFactory')

module.exports = {
  Person,
  personFactory,
}
