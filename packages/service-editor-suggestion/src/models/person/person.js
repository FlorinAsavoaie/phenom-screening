const BaseModel = require('../baseModel')

class Person extends BaseModel {
  static get tableName() {
    return 'person'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        surname: {
          type: ['string', null],
        },
        givenNames: {
          type: ['string', null],
        },
        email: {
          type: 'string',
        },
        aff: {
          type: ['string', null],
        },
        country: {
          type: ['string', null],
        },
        title: {
          type: ['string', null],
        },
        orcid: {
          type: ['string', null],
        },
      },
    }
  }

  static get relationMappings() {
    return {
      editors: {
        relation: BaseModel.HasManyRelation,
        modelClass: require('./../editor').Editor,
        join: {
          from: 'person.id',
          to: 'editor.personId',
        },
      },
      terms: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: require('./../taxonomyTerm').TaxonomyTerm,
        join: {
          from: 'person.id',
          through: {
            from: 'person_taxonomy_term.personId',
            to: 'person_taxonomy_term.taxonomyTermId',
            extra: ['occurrences'],
          },
          to: 'taxonomy_term.id',
        },
      },
    }
  }

  static async getPersonWithAssociatedEditorAndTerms({
    editor,
    assignationId,
    assignationType,
  }) {
    const person = await this.query()
      .where({ email: editor.email })
      .withGraphFetched('[editors(findAssignedEditor), terms]')
      .modifiers({
        findAssignedEditor: (builder) =>
          builder.where({ assignationId, assignationType }),
      })

    return person[0]
  }

  static async getTerms({ personId, manuscriptTerms }) {
    const person = await this.query()
      .where({ id: personId })
      .first()
      .withGraphFetched('terms')

    return manuscriptTerms.map((manuscriptTerm) => {
      const editorTerm = person.terms.find(
        (personTerm) => personTerm.id === manuscriptTerm.id,
      )

      return editorTerm || null
    })
  }

  static async getPersonTermsForManuscript(email, manuscriptTerms) {
    const personKeywords = await this.query()
      .where({ email })
      .first()
      .withGraphFetched('terms')
      .modifyGraph('terms', async (builder) =>
        builder
          .orderBy('occurrences', 'DESC')
          .select('term')
          .whereIn('term', manuscriptTerms)
          .limit(10),
      )

    return personKeywords.terms.map((keyword) => keyword.term)
  }

  static async getTaxonomyTermsCount(personId) {
    const [{ count: personTaxonomyTermsCount }] = await this.knex()
      .count('id')
      .from('person_taxonomy_term')
      .where('person_taxonomy_term.person_id', personId)

    return personTaxonomyTermsCount
  }

  static async updateTerms({
    personId,
    assignedTermsIds,
    unassignedTermsIds,
    defaultOccurrences,
  }) {
    let assignedTermsCount = 0
    let unassignedTermsCount = 0

    if (assignedTermsIds.length > 0) {
      assignedTermsCount = await this.relatedQuery('terms')
        .for(personId)
        .relate(
          assignedTermsIds.map((id) => ({
            id,
            occurrences: defaultOccurrences,
          })),
        )
    }

    if (unassignedTermsIds.length > 0) {
      unassignedTermsCount = await this.relatedQuery('terms')
        .for(personId)
        .unrelate()
        .whereIn('person_taxonomy_term.taxonomy_term_id', unassignedTermsIds)
    }

    return {
      assignedTermsCount,
      unassignedTermsCount,
    }
  }
}

module.exports = Person
