const Person = require('./person')

module.exports = {
  createPerson(person) {
    return new Person({
      id: person.userId,
      aff: person.aff,
      country: person.country,
      email: person.email,
      givenNames: person.givenNames,
      surname: person.surname,
      title: person.title,
      orcid: person.orcidId,
    })
  },
}
