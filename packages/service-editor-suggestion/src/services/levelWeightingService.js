function calculate(xArray, yArray, L) {
  const X = xArray.map(getValue)
  const Y = yArray.map(getValue)

  const numerator = getNumerator(X, Y, L)
  const denominator = getDenominator(X, Y)

  return denominator === 0 ? 0 : numerator / denominator
}

function getNumerator(X, Y, L) {
  return X.reduce((sum, x, index) => sum + x * Y[index] * L[index], 0)
}

function calculateRootSum(array) {
  return array.reduce((sum, value) => sum + value ** 2, 0)
}

function getDenominator(X, Y) {
  return Math.sqrt(calculateRootSum(X) * calculateRootSum(Y))
}

function getValue(x) {
  return x ? Math.log10(x) : 0
}

module.exports = {
  calculate,
}
