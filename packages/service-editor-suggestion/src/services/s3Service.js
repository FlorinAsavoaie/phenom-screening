const config = require('config')
const { S3 } = require('@aws-sdk/client-s3')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

const bucket = config.get('aws.s3.bucket')

const getObjectBody = async (key) => {
  const object = await getObject(key, bucket)

  const streamToString = (stream) =>
    new Promise((resolve, reject) => {
      const chunks = []
      stream.on('data', (chunk) => chunks.push(chunk))
      stream.on('error', reject)
      stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
    })

  return streamToString(object.Body)
}

const getObject = async (key, bucket) =>
  s3.getObject({ Key: key, Bucket: bucket })

const listObjects = async (params) => s3.listObjectsV2(params)

module.exports = {
  listObjects,
  getObjectBody,
  getObject,
}
