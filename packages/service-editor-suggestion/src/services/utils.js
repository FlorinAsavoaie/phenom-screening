module.exports = {
  mapTaxonomyPathForLTreeForm,
  getTransactionModels,
}

function mapTaxonomyPathForLTreeForm(path) {
  return path
    .split('/')
    .filter((category) => !!category.length) // delete '/' if path begin with '/'
    .map((category) => category.replace(/\W/g, '_')) // replace all non alphanumeric and _ characters
    .join('.')
}

function getTransactionModels(arg) {
  return arg.slice(0, -1).reduce((acc, model) => {
    acc[model.name] = model
    return acc
  }, {})
}
