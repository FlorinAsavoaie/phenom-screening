const axios = require('axios')
const { Parser } = require('xml2js')
const config = require('config')
const { mapTaxonomyPathForLTreeForm } = require('./utils')

const parser = new Parser({
  explicitRoot: false,
  mergeAttrs: true,
  normalizeTags: true,
  normalize: true,
})

module.exports = {
  getTerms: getIdealTermFullPath,
  getThesaurusTaxonomy,
}

async function getIdealTermFullPath(text) {
  const authTicket = await getAuthTicket()

  const body = dhBody({
    text: `<StringParam><![CDATA[${text.replace(']]>', '')}]]></StringParam>`,
    methodName: 'getIdealTermFullPathJSON',
    returnType: 'java.lang.String',
    authTicket,
  })

  const terms = await request(body)

  return getParsedTerms(terms)
}

async function request(body) {
  let fetchedData

  try {
    const { data } = await axios.post(config.get('dataHarmony.endpoint'), body)
    fetchedData = data
  } catch (error) {
    const { message } = error.toJSON()

    throw new Error(message)
  }

  return fetchedData
}

function dhBody({ text, methodName, returnType, authTicket }) {
  return `
    <TMMAI 
    project='${config.get('dataHarmony.projectName')}'
    location='${config.get('dataHarmony.projectLocation')}'
    ticket="${authTicket}"
    user="${config.get('dataHarmony.projectUser')}">
      <Method 
      name="${methodName}"
      returnType="${returnType}"/>
        ${text}
    </TMMAI>
    `
}

async function getParsedTerms(terms) {
  const parsedTerms = await parser
    .parseStringPromise(terms)
    .then((result) => result.stringreturn)
    .then((result) => {
      if (result[0].startsWith('Error')) {
        throw new Error(`Data Harmony ${result}`)
      }
      return result
    })
    .then(JSON.parse)

  return parsedTerms.map((term) => ({
    term: term.term.split('/').slice(-1)[0],
    path: mapTaxonomyPathForLTreeForm(term.term),
    occurrences: term.totalFreq,
  }))
}

function dhGetTicketBody() {
  return `<DH 
    serverType="TMMAI"
    project="${config.get('dataHarmony.projectName')}"
    location="${config.get('dataHarmony.projectLocation')}"
    user="${config.get('dataHarmony.projectUser')}"
    pw="${config.get('dataHarmony.projectPass')}"
    isEncrypted="false"
    action="getTicket"
    decode="false" />`
}

// The ticket is valid for 4 hours. Currently, we create a new ticket for each request. If this proves to be a problem with DH, we'll have to implement a refresh mechanism
async function getAuthTicket() {
  const body = dhGetTicketBody()
  const authTicketData = await request(body)

  return parser
    .parseStringPromise(authTicketData)
    .then((result) => result.ticket[0])
}

async function getThesaurusTaxonomy() {
  const authTicket = await getAuthTicket()

  const body = dhBody({
    methodName: 'getJSON',
    returnType: 'java.lang.String',
    authTicket,
  })

  const result = await request(body)

  return parser
    .parseStringPromise(result)
    .then((result) => result.stringreturn)
    .then((result) => {
      if (result[0].startsWith('Error')) {
        throw new Error(`Data Harmony ${result}`)
      }
      return result
    })
    .then(JSON.parse)
}
