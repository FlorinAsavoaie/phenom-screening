const useCases = require('./use-cases')
const { models, serializers, factories } = require('./models')
const configService = require('config')
const logger = require('@pubsweet/logger')
const { transaction } = require('objection')

const { Person, TaxonomyTerm, PersonExpertiseAuditLog } = models

const resolvers = {
  Query: {
    async getEditorsKeywords(_, params) {
      return useCases.getEditorsKeywords.initialize(models).execute(params)
    },
    async getPersonTaxonomyTermsCount(_, __, context) {
      return useCases.getPersonTaxonomyTermsCount.initialize(models).execute({
        personId: context.user.phenomId,
      })
    },
    async getPersonTaxonomyTerms(_, __, context) {
      return useCases.getPersonTaxonomyTerms
        .initialize({ models, serializers })
        .execute({
          personId: context.user.phenomId,
        })
    },
    async getTermsByParentId(_, params) {
      return useCases.getTermsByParentId
        .initialize({ models, serializers })
        .execute({
          parentId: params.parentId,
        })
    },
    async getSearchedTerms(_, params) {
      return useCases.getSearchedTerms
        .initialize({ models, serializers })
        .execute(params)
    },
  },
  Term: {
    async isLeaf(term) {
      return useCases.isLeafTerm.initialize(models).execute(term.id)
    },
    async isAssigned(term, _, context) {
      return useCases.isTermAssigned
        .initialize(models)
        .execute(term.id, context.user.phenomId)
    },
  },
  Mutation: {
    async editExpertise(_, params, context) {
      return transaction(
        Person,
        TaxonomyTerm,
        PersonExpertiseAuditLog,
        async (Person, TaxonomyTerm, PersonExpertiseAuditLog) =>
          useCases.editExpertise
            .initialize({
              models: { Person, TaxonomyTerm, PersonExpertiseAuditLog },
              factories,
              services: { logger, configService },
            })
            .execute({
              personId: context.user.phenomId,
              assignedTermsIds: params.expertise.assignedTermsIds,
              unassignedTermsIds: params.expertise.unassignedTermsIds,
            }),
      )
    },
  },
}

module.exports = resolvers
