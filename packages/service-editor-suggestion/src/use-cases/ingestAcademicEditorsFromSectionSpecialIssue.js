const { Promise } = require('bluebird')

function initialize({
  useCases: {
    ingestAcademicEditor,
    generateEditorSuggestionListForManuscripts,
  },
  models: { Editor },
}) {
  return {
    execute,
  }

  async function execute(data) {
    const assignationType = Editor.AssignationTypes.SECTION_SPECIAL_ISSUE
    const academicEditors = getAcademicEditors(data)

    const newEditors = await Promise.mapSeries(academicEditors, (editor) =>
      ingestAcademicEditor.execute({
        editor,
        assignationId: editor.assignationId,
        assignationType,
      }),
    )

    const filteredNewEditors = newEditors.filter((e) => !!e)

    await generateEditorSuggestionListForManuscripts.execute(filteredNewEditors)
  }

  function getAcademicEditors(data) {
    const editors = []
    data.sections.forEach((section) => {
      section.specialIssues.forEach((specialIssue) => {
        editors.push(
          ...specialIssue.editors
            .filter((editor) => editor.role.type === 'academicEditor')
            .map((editor) => ({
              ...editor,
              assignationId: specialIssue.id,
            })),
        )
      })
    })

    return editors
  }
}

module.exports = { initialize }
