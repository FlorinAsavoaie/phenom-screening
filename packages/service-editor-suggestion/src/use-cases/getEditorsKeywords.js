const { Promise } = require('bluebird')

function initialize({ Person, Manuscript }) {
  return { execute }

  async function execute({ submissionId, editorEmails }) {
    const manuscript = await Manuscript.findOneBy({
      queryObject: { submissionId },
    })
    const manuscriptTerms = await manuscript.getManuscriptTerms()
    const mappedManuscriptTerms = manuscriptTerms.map((term) => term.term)

    const editorsKeywords = await Promise.map(editorEmails, async (email) => {
      const editorTerms = await Person.getPersonTermsForManuscript(
        email,
        mappedManuscriptTerms,
      )
      return { email, terms: editorTerms }
    })
    return editorsKeywords
  }
}

module.exports = { initialize }
