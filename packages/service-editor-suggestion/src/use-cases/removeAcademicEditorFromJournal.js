function initialize({
  models: { Editor },
  useCases: { removeAcademicEditor },
}) {
  return { execute }

  async function execute(data) {
    const assignationId = data.id
    const assignationType = Editor.AssignationTypes.JOURNAL

    const journalAcademicEditorIds = data.editors
      .filter((editor) => editor.role.type === 'academicEditor')
      .map((e) => e.id)

    await removeAcademicEditor.execute({
      editorIds: journalAcademicEditorIds,
      assignationId,
      assignationType,
    })
  }
}

module.exports = { initialize }
