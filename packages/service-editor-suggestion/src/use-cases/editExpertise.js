function initialize({
  services: { logger, configService },
  models: { Person, TaxonomyTerm, PersonExpertiseAuditLog },
  factories: { personExpertiseAuditLogFactory },
}) {
  return {
    execute,
  }

  async function execute({ personId, assignedTermsIds, unassignedTermsIds }) {
    await assertTermsAreUnassigned({ personId, termsIds: assignedTermsIds })
    await assertTermsAreAssigned({ personId, termsIds: unassignedTermsIds })

    const unassignedTerms = await TaxonomyTerm.findInByPersonId({
      termsIds: unassignedTermsIds,
      personId,
    })

    const { assignedTermsCount, unassignedTermsCount } =
      await Person.updateTerms({
        personId,
        assignedTermsIds,
        unassignedTermsIds,
        defaultOccurrences: configService.get('dataHarmony.defaultOccurrences'),
      })

    logger.info(
      `Related ${assignedTermsCount} terms for person with id "${personId}".`,
    )

    logger.info(
      `Unrelated ${unassignedTermsCount} terms for person with id "${personId}".`,
    )

    await savePersonExpertiseAuditLog({
      personId,
      assignedTermsIds,
      unassignedTerms,
    })
  }

  async function assertTermsAreUnassigned({ personId, termsIds }) {
    const assignedTerms = await TaxonomyTerm.findInByPersonId({
      personId,
      termsIds,
    })

    if (assignedTerms.length > 0) {
      throw new Error('Terms are already assigned.')
    }
  }

  async function assertTermsAreAssigned({ personId, termsIds }) {
    const assignedTerms = await TaxonomyTerm.findInByPersonId({
      personId,
      termsIds,
    })

    const assignedTermsIds = assignedTerms.map((term) => term.id)
    const alreadyAssignedTerms = termsIds.filter(
      (termId) => !assignedTermsIds.includes(termId),
    )

    if (alreadyAssignedTerms.length > 0) {
      throw new Error('Terms are not assigned yet.')
    }
  }

  async function savePersonExpertiseAuditLog({
    personId,
    assignedTermsIds,
    unassignedTerms,
  }) {
    const assignedTerms = await TaxonomyTerm.findInByPersonId({
      termsIds: assignedTermsIds,
      personId,
    })

    const personExpertiseAuditLog =
      personExpertiseAuditLogFactory.createPersonExpertiseAuditLog({
        mode: PersonExpertiseAuditLog.Modes.MANUAL,
        personId,
        assignedTerms,
        unassignedTerms,
      })

    await PersonExpertiseAuditLog.insertGraph(personExpertiseAuditLog)

    logger.info(
      `Taxonomy expertise audit log saved for person with id: "${personId}".`,
    )
  }
}

module.exports = {
  initialize,
}
