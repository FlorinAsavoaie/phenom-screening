const { Promise } = require('bluebird')

function initialize({
  useCases: {
    ingestAcademicEditor,
    generateEditorSuggestionListForManuscripts,
  },
  models: { Editor },
}) {
  return {
    execute,
  }

  async function execute(data) {
    const assignationType = Editor.AssignationTypes.JOURNAL

    const academicEditors = getAcademicEditors(data)

    const newEditors = await Promise.mapSeries(academicEditors, (editor) =>
      ingestAcademicEditor.execute({
        editor,
        assignationId: data.id,
        assignationType,
      }),
    )

    const filteredNewEditors = newEditors.filter((e) => !!e)

    await generateEditorSuggestionListForManuscripts.execute(filteredNewEditors)
  }

  function getAcademicEditors(data) {
    return data.editors.filter(
      (editor) => editor.role.type === 'academicEditor',
    )
  }
}

module.exports = { initialize }
