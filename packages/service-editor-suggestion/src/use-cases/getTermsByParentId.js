function initialize({
  models: { TaxonomyTerm },
  serializers: { taxonomyTermSerializer },
}) {
  return {
    execute,
  }

  async function execute({ parentId }) {
    const terms = await TaxonomyTerm.getTermsByParentId(parentId)

    return terms.map(taxonomyTermSerializer.serialize)
  }
}

module.exports = {
  initialize,
}
