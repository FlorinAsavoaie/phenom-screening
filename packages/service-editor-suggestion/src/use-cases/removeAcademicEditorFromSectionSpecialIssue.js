const { Promise } = require('bluebird')

function initialize({
  models: { Editor },
  useCases: { removeAcademicEditor },
}) {
  return { execute }

  async function execute(data) {
    const assignationType = Editor.AssignationTypes.SECTION_SPECIAL_ISSUE

    await Promise.each(data.sections, async (section) => {
      await Promise.each(section.specialIssues, async (specialIssue) => {
        const sectionSpecialIssueAcademicEditorIds = specialIssue.editors
          .filter((editor) => editor.role.type === 'academicEditor')
          .map((e) => e.id)

        await removeAcademicEditor.execute({
          editorIds: sectionSpecialIssueAcademicEditorIds,
          assignationId: specialIssue.id,
          assignationType,
        })
      })
    })
  }
}

module.exports = { initialize }
