const { Promise } = require('bluebird')

function initialize({
  models: { Editor },
  useCases: { removeAcademicEditor },
}) {
  return { execute }

  async function execute(data) {
    const assignationType = Editor.AssignationTypes.SPECIAL_ISSUE

    await Promise.each(data.specialIssues, async (specialIssue) => {
      const specialIssueAcademicEditorIds = specialIssue.editors
        .filter((editor) => editor.role.type === 'academicEditor')
        .map((e) => e.id)

      await removeAcademicEditor.execute({
        editorIds: specialIssueAcademicEditorIds,
        assignationId: specialIssue.id,
        assignationType,
      })
    })
  }
}

module.exports = { initialize }
