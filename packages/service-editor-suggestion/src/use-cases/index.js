const ingestAcademicEditor = require('./ingestAcademicEditor')
const ingestAcademicEditorsFromJournal = require('./ingestAcademicEditorsFromJournal')
const ingestAcademicEditorsFromSectionSpecialIssue = require('./ingestAcademicEditorsFromSectionSpecialIssue')
const ingestAcademicEditorsFromSpecialIssue = require('./ingestAcademicEditorsFromSpecialIssue')
const removeAcademicEditorFromJournal = require('./removeAcademicEditorFromJournal')
const removeAcademicEditorFromSpecialIssue = require('./removeAcademicEditorFromSpecialIssue')
const removeAcademicEditorFromSectionSpecialIssue = require('./removeAcademicEditorFromSectionSpecialIssue')
const removeAcademicEditor = require('./removeAcademicEditor')
const getEditorsKeywords = require('./getEditorsKeywords')

const generateEditorSuggestionList = require('./generateEditorSuggestionList')
const generateEditorSuggestionListForManuscripts = require('./generateEditorSuggestionListForManuscripts')

const calculateScoreForEditors = require('./calculateScoreForEditors')

const removeEditorScoresAndManuscript = require('./removeEditorScoresAndManuscript')
const getPersonTermsAndIngest = require('./getPersonTermsAndIngest')
const getPersonTaxonomyTermsCount = require('./getPersonTaxonomyTermsCount')
const getPersonTaxonomyTerms = require('./getPersonTaxonomyTerms')
const getTermsByParentId = require('./getTermsByParentId')
const editExpertise = require('./editExpertise')
const isLeafTerm = require('./isLeafTerm')
const isTermAssigned = require('./isTermAssigned')
const getSearchedTerms = require('./getSearchedTerms')

module.exports = {
  getSearchedTerms,
  getEditorsKeywords,
  ingestAcademicEditor,
  removeAcademicEditor,
  getPersonTermsAndIngest,
  getPersonTaxonomyTerms,
  calculateScoreForEditors,
  getPersonTaxonomyTermsCount,
  generateEditorSuggestionList,
  removeAcademicEditorFromJournal,
  removeEditorScoresAndManuscript,
  ingestAcademicEditorsFromJournal,
  removeAcademicEditorFromSpecialIssue,
  ingestAcademicEditorsFromSpecialIssue,
  generateEditorSuggestionListForManuscripts,
  removeAcademicEditorFromSectionSpecialIssue,
  ingestAcademicEditorsFromSectionSpecialIssue,
  getTermsByParentId,
  editExpertise,
  isLeafTerm,
  isTermAssigned,
}
