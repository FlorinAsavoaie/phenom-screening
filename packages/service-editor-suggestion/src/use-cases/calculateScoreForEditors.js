const { Promise } = require('bluebird')

const NUMBER_OF_DIGITS_AFTER_DECIMAL_POINT = 6

function initialize({
  models: { Person },
  services: { levelWeightingService },
}) {
  return {
    execute,
  }

  async function execute({ editors, manuscriptTerms }) {
    const manuscriptTermsOccurrences = manuscriptTerms.map(
      (manuscriptTerm) => manuscriptTerm.occurrences,
    )
    const manuscriptTermsTaxonomicLevels = manuscriptTerms.map(
      (manuscriptTerm) => manuscriptTerm.depth,
    )

    const editorsScore = await Promise.map(
      editors,
      async (editor) => {
        const editorTerms = await Person.getTerms({
          personId: editor.person.id,
          manuscriptTerms,
        })

        const editorTermsOccurrences = editorTerms.map((editorTerm) =>
          editorTerm ? editorTerm.occurrences : null,
        )

        const score = levelWeightingService.calculate(
          manuscriptTermsOccurrences,
          editorTermsOccurrences,
          manuscriptTermsTaxonomicLevels,
        )

        return {
          ...editor,
          score: parseFloat(
            score === 0
              ? '0'
              : score.toFixed(NUMBER_OF_DIGITS_AFTER_DECIMAL_POINT),
          ),
        }
      },
      { concurrency: 10 },
    )

    return editorsScore
  }
}

module.exports = { initialize }
