function initialize({ models: { Manuscript } }) {
  return {
    execute,
  }

  async function execute({ submissionId }) {
    const manuscript = await Manuscript.findOneBy({
      queryObject: {
        submissionId,
      },
    })

    if (!manuscript) {
      return
    }

    await manuscript.delete()
  }
}

module.exports = { initialize }
