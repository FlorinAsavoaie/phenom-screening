const { Promise } = require('bluebird')
const { flatMap } = require('lodash')

function initialize({
  useCases: {
    ingestAcademicEditor,
    generateEditorSuggestionListForManuscripts,
  },
  models: { Editor },
}) {
  return {
    execute,
  }

  async function execute(data) {
    const assignationType = Editor.AssignationTypes.SPECIAL_ISSUE
    const academicEditors = getAcademicEditors(data)

    const newEditors = await Promise.mapSeries(academicEditors, (editor) =>
      ingestAcademicEditor.execute({
        editor,
        assignationId: editor.assignationId,
        assignationType,
      }),
    )

    const filteredNewEditors = newEditors.filter((e) => !!e)

    await generateEditorSuggestionListForManuscripts.execute(filteredNewEditors)
  }

  function getAcademicEditors(data) {
    return flatMap(data.specialIssues, (specialIssue) =>
      specialIssue.editors
        .filter((editor) => editor.role.type === 'academicEditor')
        .map((editor) => ({
          ...editor,
          assignationId: specialIssue.id,
        })),
    )
  }
}

module.exports = { initialize }
