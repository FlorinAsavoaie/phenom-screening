const logger = require('@pubsweet/logger')

const { Promise } = require('bluebird')
const { unionBy } = require('lodash')

module.exports = {
  initialize,
}

function initialize({
  models: { EditorManuscriptScore, Manuscript },
  factories: { editorManuscriptScoreFactory },
  useCases: { calculateScoreForEditors },
  services: { applicationEventBus },
}) {
  return {
    execute,
  }

  async function execute(editors) {
    let changedManuscripts = []

    await Promise.each(editors, async (editor) => {
      const manuscripts = await Manuscript.findBy({
        assignationId: editor.assignationId,
      })
      await Promise.each(manuscripts, async (manuscript) => {
        await calculateScoreForNewEditor({ editor, manuscript })
      })

      changedManuscripts = changedManuscripts.concat(manuscripts)
    })

    const uniqueManuscripts = unionBy(changedManuscripts, 'id')

    await Promise.each(uniqueManuscripts, emitNewEditorSuggestionList)
  }

  async function calculateScoreForNewEditor({ editor, manuscript }) {
    const manuscriptTerms = await manuscript.getManuscriptTerms()

    const newEditorScore = await calculateScoreForEditors.execute({
      manuscriptTerms,
      editors: [editor],
    })

    const editorManuscriptScore = await EditorManuscriptScore.findOneBy({
      queryObject: {
        editor_id: editor.id,
        manuscript_id: manuscript.id,
      },
    })

    const createdEditorScore =
      editorManuscriptScoreFactory.createEditorManuscriptScore({
        score: newEditorScore[0].score,
        editorId: editor.id,
        manuscriptId: manuscript.id,
        id: editorManuscriptScore ? editorManuscriptScore.id : null,
      })

    await EditorManuscriptScore.upsertGraph(createdEditorScore, {
      relate: true,
    })

    logger.info(
      `Upserted editor_manuscript_score table for editor ${editor.id}`,
    )
  }

  async function emitNewEditorSuggestionList(manuscript) {
    const allEditorsScore = await EditorManuscriptScore.findBy({
      manuscriptId: manuscript.id,
    })

    await applicationEventBus.publishMessage({
      event: 'EditorSuggestionListGenerated',
      data: {
        submissionId: manuscript.submissionId,
        manuscriptId: manuscript.id,
        editorSuggestions: allEditorsScore.map((editorScore) => ({
          editorId: editorScore.editorId,
          score: editorScore.score,
        })),
      },
    })
  }
}
