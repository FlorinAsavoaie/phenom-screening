const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

function initialize({ Manuscript, Editor, EditorManuscriptScore }) {
  return { execute }

  async function execute({ editorIds, assignationId, assignationType }) {
    const removedEditors = await Editor.findAllBy({
      excludedIds: editorIds,
      queryObject: {
        assignationType,
        assignationId,
      },
    })

    if (!removedEditors.length) {
      logger.info(
        `No academic editor was removed from ${assignationType} ${assignationId}`,
      )
      return
    }

    await deleteEditors(removedEditors)

    await emitNewEditorSuggestionList(assignationId)
  }

  async function deleteEditors(editors) {
    const editorsIds = editors.map((editor) => editor.id)
    await Editor.deleteByIds(editorsIds)
  }

  async function emitNewEditorSuggestionList(assignationId) {
    const affectedManuscripts = await Manuscript.findBy({
      assignationId,
    })

    await Promise.each(affectedManuscripts, async (manuscript) => {
      const editorsScore = await EditorManuscriptScore.findBy({
        manuscriptId: manuscript.id,
      })
      mapEditorSuggestionListGeneratedEvent(manuscript, editorsScore)
    })
  }

  async function mapEditorSuggestionListGeneratedEvent(
    manuscript,
    editorsScore,
  ) {
    await applicationEventBus.publishMessage({
      event: 'EditorSuggestionListGenerated',
      data: {
        submissionId: manuscript.submissionId,
        manuscriptId: manuscript.id,
        editorSuggestions: editorsScore.map((editorScore) => ({
          editorId: editorScore.editorId,
          score: editorScore.score,
        })),
      },
    })
  }
}

module.exports = { initialize }
