const { chain } = require('lodash')

function initialize({
  services: { dataHarmonyService, wosDataRepository },
  models: { Person, TaxonomyTerm, PersonExpertiseAuditLog },
  factories: { personExpertiseAuditLogFactory },
  logger,
}) {
  return {
    execute,
  }

  async function execute(person) {
    const { email, orcid } = person

    const wosProfiles = await wosDataRepository.getWoSProfilesByEmailOrOrcid({
      email,
      orcid,
    })

    const abstractsFromWos = wosProfiles.map(
      (profile) => profile.manuscriptAbstract,
    )

    const personTermsFromDataHarmony = await getPersonTermsFromDataHarmony(
      abstractsFromWos,
    )

    const personTerms = chain(personTermsFromDataHarmony)
      .orderBy('occurrences', 'desc')
      .take(50)
      .value()

    const savedPersonTerms = await savePersonTermsInDb(person, personTerms)

    await savePersonExpertiseAuditLog({
      personId: savedPersonTerms.id,
      terms: savedPersonTerms.terms,
    })

    return savedPersonTerms
  }

  async function getPersonTermsFromDataHarmony(abstracts) {
    const parsedAbstract = abstracts.join(' ')
    return dataHarmonyService.getTerms(parsedAbstract)
  }

  async function savePersonTermsInDb(person, personTerms) {
    const termsPath = personTerms.map((term) => term.path)

    const termsFromDb = await TaxonomyTerm.findIn('path', termsPath)

    const createdTerms = personTerms.reduce((acc, personTerm) => {
      const termFromDb = termsFromDb.find(
        (term) => term.path === personTerm.path,
      )
      if (!termFromDb) {
        logger.warn(
          `Term with path ${personTerm.path} from person ${person.id} was not found in taxonomy_term table.`,
        )
        return acc
      }

      return acc.concat({
        id: termFromDb.id,
        occurrences: personTerm.occurrences,
      })
    }, [])

    const savedPersonTerms = await Person.upsertGraph(
      { id: person.id, terms: createdTerms },
      { relate: true },
    )

    logger.info(
      `Saved ${savedPersonTerms.terms.length} terms for person with id: "${person.id}".`,
    )

    return savedPersonTerms
  }

  async function savePersonExpertiseAuditLog({ personId, terms }) {
    const assignedTerms = await TaxonomyTerm.findInByPersonId({
      termsIds: terms.map((t) => t.id),
      personId,
    })

    const personExpertiseAuditLog =
      personExpertiseAuditLogFactory.createPersonExpertiseAuditLog({
        mode: PersonExpertiseAuditLog.Modes.AUTO,
        personId,
        assignedTerms,
        unassignedTerms: [],
      })

    await PersonExpertiseAuditLog.insertGraph(personExpertiseAuditLog)

    logger.info(
      `Taxonomy expertise audit log saved for person with id: "${personId}".`,
    )
  }
}

module.exports = { initialize }
