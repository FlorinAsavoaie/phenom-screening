function initialize({ TaxonomyTerm }) {
  return {
    execute,
  }

  async function execute(termId, personId) {
    return TaxonomyTerm.isAssigned(termId, personId)
  }
}

module.exports = {
  initialize,
}
