function initialize({ Person }) {
  return { execute }

  async function execute({ personId }) {
    return Person.getTaxonomyTermsCount(personId)
  }
}

module.exports = { initialize }
