function initialize({
  models: { Person, Editor },
  factories: { editorFactory, personFactory },
  useCases: { getPersonTermsAndIngest },
  logger,
}) {
  return {
    execute,
  }

  async function execute({ editor, assignationId, assignationType }) {
    const person = await Person.getPersonWithAssociatedEditorAndTerms({
      editor,
      assignationId,
      assignationType,
    })

    if (person && person.editors[0]) return

    let savedEditor

    if (!person) {
      savedEditor = await createEditorAndPerson({
        assignationId,
        assignationType,
        editor,
      })
    } else if (!person.editors[0]) {
      savedEditor = await createEditorForPerson({
        assignationId,
        assignationType,
        editor,
        person,
      })
    }

    if (!savedEditor.person.terms.length) {
      await getPersonTermsAndIngest.execute(savedEditor.person)
    }

    return savedEditor
  }

  async function createEditorAndPerson({
    assignationId,
    assignationType,
    editor,
  }) {
    const newPerson = personFactory.createPerson(editor)
    const newEditor = editorFactory.createEditorProfile({
      editor,
      assignationId,
      assignationType,
    })

    const savedPerson = await Editor.insertGraph({
      ...newEditor,
      person: { ...newPerson, terms: [] },
    })

    logger.info(
      `Saved person with id: "${newPerson.id}" and editor with id: "${newEditor.id}".`,
    )

    return savedPerson
  }

  async function createEditorForPerson({
    assignationId,
    assignationType,
    person,
    editor,
  }) {
    const newEditor = editorFactory.createEditorProfile({
      editor,
      assignationId,
      assignationType,
    })

    const savedEditor = await Editor.insertGraph(
      { ...newEditor, person },
      { relate: true },
    )

    logger.info(
      `Saved editor with id: "${newEditor.id}" for person with id: "${person.id}".`,
    )

    return savedEditor
  }
}

module.exports = { initialize }
