function initialize({
  models: { TaxonomyTerm },
  serializers: { taxonomyTermSerializer },
}) {
  return {
    execute,
  }

  async function execute({ searchedValue }) {
    if (searchedValue.length < 3) return []

    const searchedTerms = await TaxonomyTerm.getSearchedTerms(searchedValue)

    if (searchedTerms.length === 0) return []

    const paths = searchedTerms.map((term) => term.path)

    const termsWithAscendants = await TaxonomyTerm.getAscendants(paths)

    const mappedTerms = termsWithAscendants.map((term) => ({
      ...taxonomyTermSerializer.serialize(term),
      children: [],
    }))

    const nest = (taxonomy, termId = null) =>
      taxonomy
        .filter((item) => item.parentId === termId)
        .map((item) => ({ ...item, children: nest(taxonomy, item.id) }))

    return nest(mappedTerms)
  }
}

module.exports = {
  initialize,
}
