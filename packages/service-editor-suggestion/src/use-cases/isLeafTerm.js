function initialize({ TaxonomyTerm }) {
  return {
    execute,
  }

  async function execute(termId) {
    return TaxonomyTerm.isLeaf(termId)
  }
}

module.exports = {
  initialize,
}
