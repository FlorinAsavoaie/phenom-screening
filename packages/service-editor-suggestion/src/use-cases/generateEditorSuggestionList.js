const { Promise } = require('bluebird')

const initialize = ({
  models: { TaxonomyTerm, Editor },
  services: { dataHarmonyService, s3Service },
  factories: { manuscriptFactory, editorManuscriptScoreFactory },
  useCases: { calculateScoreForEditors },
  logger,
}) => {
  return {
    execute,
  }

  async function execute({ submissionId, manuscripts }) {
    const manuscript = manuscripts[0]

    const manuscriptTerms = await getTermsForManuscript(manuscript)

    const assignationId = manuscript.specialIssueId || manuscript.journalId

    const createdManuscript = await saveManuscriptTerms({
      manuscriptId: manuscript.id,
      submissionId,
      assignationId,
      manuscriptTerms,
    })

    const editors = await Editor.findBy(
      {
        assignationId,
      },
      'person',
    )

    const editorsScore = await calculateScoreForEditors.execute({
      manuscriptTerms: createdManuscript.terms,
      editors,
    })

    await Promise.each(editorsScore, async (editor) => {
      const createdEditorScore =
        editorManuscriptScoreFactory.createEditorManuscriptScore({
          score: editor.score,
          editorId: editor.id,
          manuscriptId: manuscript.id,
        })

      await createdEditorScore.save()
    })

    await applicationEventBus.publishMessage({
      event: 'EditorSuggestionListGenerated',
      data: {
        submissionId: manuscript.submissionId,
        manuscriptId: manuscript.id,
        editorSuggestions: editorsScore.map((editorScore) => ({
          editorId: editorScore.id,
          score: editorScore.score,
        })),
      },
    })
  }

  async function getTermsForManuscript(manuscript) {
    const providerKey = getProviderKeyForManuscriptFile(manuscript)
    const text = await s3Service.getObjectBody(`${providerKey}-text`)

    return dataHarmonyService.getTerms(text)
  }

  function getProviderKeyForManuscriptFile(manuscript) {
    const file = manuscript.files.find((file) => file.type === 'manuscript')
    return file.providerKey
  }

  async function saveManuscriptTerms({
    manuscriptId,
    submissionId,
    assignationId,
    manuscriptTerms,
  }) {
    const createdManuscript = manuscriptFactory.createManuscript({
      manuscriptId,
      submissionId,
      assignationId,
    })

    const manuscriptTermsPath = manuscriptTerms.map((term) => term.path)

    const manuscriptTermsFromDb = await TaxonomyTerm.findIn(
      'path',
      manuscriptTermsPath,
    )

    createdManuscript.terms = manuscriptTerms.reduce((acc, manuscriptTerm) => {
      const dbTerm = manuscriptTermsFromDb.find(
        (dbterm) => dbterm.path === manuscriptTerm.path,
      )

      if (!dbTerm) {
        logger.warn(
          `Term with path ${manuscriptTerm.path} from manuscript ${manuscriptId} was not found in taxonomy_term table.`,
        )
        return acc
      }

      return acc.concat({
        id: dbTerm.id,
        occurrences: manuscriptTerm.occurrences,
      })
    }, [])

    return createdManuscript.saveGraph({
      relate: true,
    })
  }
}

module.exports = {
  initialize,
}
