function initialize({
  models: { Person },
  serializers: { taxonomyTermSerializer },
}) {
  return { execute }

  async function execute({ personId }) {
    const person = await Person.findOneBy({
      queryObject: { id: personId },
      eagerLoadRelations: 'terms',
    })

    if (!person) {
      return []
    }
    return person.terms.map(taxonomyTermSerializer.serialize)
  }
}

module.exports = {
  initialize,
}
