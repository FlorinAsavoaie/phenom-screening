require('dotenv').config()
const config = require('config')

module.exports = {
  client: 'pg',
  connection: config.get('pubsweet-server.db'),
  seeds: {
    directory: './seeds',
  },
  migrations: {
    directory: './migrations',
  },
}
