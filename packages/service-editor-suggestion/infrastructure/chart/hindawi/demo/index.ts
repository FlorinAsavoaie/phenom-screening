import { WithSopsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithSopsSecretsServiceProps = {
  ...defaultValues,
  sopsSecrets: require('./demo.env.enc.json'),
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'service-editor-suggestion.demo.phenom.pub',
        },
      ],
    },
  },
}
