import { WithSopsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithSopsSecretsServiceProps = {
  ...defaultValues,
  sopsSecrets: require('./prod.env.enc.json'),
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/service-editor-suggestion',
      tag: 'latest',
    },
    resources: {
      requests: {
        memory: '200Mi',
      },
      limits: {
        memory: '250Mi',
      },
    },
    ingressOptions: {
      rules: [
        {
          host: 'service-editor-suggestion.prod.phenom.pub',
        },
      ],
    },
  },
}
