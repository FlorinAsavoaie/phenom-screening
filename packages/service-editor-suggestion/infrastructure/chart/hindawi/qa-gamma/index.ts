import { WithSopsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithSopsSecretsServiceProps = {
  ...defaultValues,
  sopsSecrets: require('./qa-gamma.env.enc.json'),
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'service-editor-suggestion.qa-gamma.phenom.pub',
        },
      ],
    },
  },
}
