# Editor Suggestion Service

This service adds the ability to generate an ordered list of editors for handling a manuscript based on their expertise

### Required Env vars

```sh
AWS_REGION=eu-west-1

AWS_PROFILE=HindawiDevelopment

AWS_SNS_ENDPOINT=http://localstack:4566
AWS_SNS_TOPIC=test1

AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SQS_QUEUE_NAME=editor-suggestion-queue

EVENT_NAMESPACE=****
PUBLISHER_NAME=****

DB_USER=****
DB_HOST=postgres

AWS_S3_BUCKET=****
AWS_S3_EVENT_STORAGE_BUCKET=****

DH_ENDPOINT=****
DH_PROJECT_NAME=YOUR_PROJECT_NAME
DH_PROJECT_LOCATION=YOUR_PROJECT_LOCATION
DH_PROJECT_USER=***
DH_PROJECT_PASS=***

AWS_S3_ENDPOINT=.
PHENOM_LARGE_EVENTS_BUCKET=.
PHENOM_LARGE_EVENTS_PREFIX=.

WOS_ELASTICSEARCH_NODE=****
WOS_ELASTICSEARCH_INDEX=****
WOS_ELASTICSEARCH_USERNAME=****
WOS_ELASTICSEARCH_PASSWORD=****

KEYCLOAK_CERTIFICATES=****
SSO_CLIENT_ID=****

# REGISTRY_URL_FOR_ENVIRONMENT, example for local environment
SCHEMA_REGISTRY_URL='http://host.docker.internal:6001'
```

### Not Required Env vars - default values

```sh
DATABASE=editor_suggestion
DB_PASSWORD=''
PORT=3000

# url to the service graphql schema, needed for schema registry push, example for local environment
SERVICE_SCHEMA_URL='http://host.docker.internal:3008/graphql'
```

`AWS_S3_ACCESS_KEY` & `AWS_S3_SECRET_KEY` & `AWS_SNS_SQS_ACCESS_KEY` & `AWS_SNS_SQS_SECRET_KEY` are no longer needed in the .env file. \
Access keys are loaded from the Shared Credentials File (~/aws/credentials) for the AWS profile. This means that in order to work you need to be logged in with the Development AWS account.

# Config for docker-compose

```sh
DB_HOST=postgres
AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SNS_ENDPOINT=http://localstack:4566
SCHEMA_REGISTRY_URL=http://host.docker.internal:6001
```

# Config for yarn start

```sh
PORT=3008
AWS_SNS_ENDPOINT=http://localhost:4566
AWS_SQS_ENDPOINT=http://localhost:4566
SCHEMA_REGISTRY_URL=http://localhost:6001
```

### Starting the service

To start the service run `docker-compose up service-editor-suggestion`.

### Running editors' expertise keywords repopulation script

Inside `scripts/repopulate-terms-for-editors.js` you'll find the code base for a script that goes through all the
editors that did not manually assign their expertise keywords on their profile and triggers the automatic functionality
to accomplish that.

In order to run the script, you need to deploy it in a specific Kubernetes environment and then trigger it as a job. You
may follow the next steps:

1. Make sure you edit `infrastructure/repopulate-keywords-for-editors-job/repopulate-keywords-for-editors-job.yaml` job
   configuration file and assign correct values for all the environment variables specified over there (you can copy
   them from a pod corresponding to service-editor-suggestion in the same environment). Also, you may replace the
   endpoint for the docker image to be used depending on the enviroment where you're running it.

2. Make sure you gain access to the Kubernetes environment where you want to run the job by making use
   of `aws-azure-login` tool and setting your local context to the right cluster:

```
// dev cluster
aws eks update-kubeconfig --name hindawi-dev --profile HindawiDevelopment

// prod cluster
aws eks update-kubeconfig --name hindawi-prod --profile HindawiProduction
```

3. `cd` to the .yaml's location and
   run `kubectl apply -f repopulate-keywords-for-editors-job.yaml -n [namespace-corresponding-to-the-environment]`

You can check the available namespaces with `kubectl get ns`

4. Check with Lens or `kubectl get pods` and `kubectl logs [created-job-pod-id]` to see if the job is running
   successfully.

_Obs: Depending on the number of entries, it may take a few hours for the job to finish._

### Secrets Management

The `editor-suggestion-service` manages secrets in git, encrypted using SOPS. For more information please
visit [[Confluence] Config & Secrets Management](https://confluence.wiley.com/pages/viewpage.action?pageId=233511946)

#### Quick commands overview

- If you need to modify a value OR add an encrypted value  
  ⚠️ Always prefer this option

```shell
EDITOR=<editor> AWS_PROFILE=<aws-profile> sops <path-to-encrypted-file>
# EDITOR (optional) - specify which editor to use. By default SOPS uses vim. Most editors work with the --wait flag to make them keep the process open while you edit the file. E.g. "subl --wait" / "wstorm --wait"
# AWS_PROFILE - required so that it can access the KMS key specified in .sops.yaml
```

- If you want to add an unencrypted value, you will need to decrypt and re-encrypt specifying a new value for
  unencrypted-regex

```shell
AWS_PROFILE=<aws-profile> sops -d <path-to-encrypted-file>.enc.json > <path-to-unencrypted-file>.dec.json
# -d = decrypt

### Do your modifications

### Update <service>/.sops.yaml unencrypted_regex with the new values you want to keep unencrypted

AWS_PROFILE=<aws-profile> sops -e <path-to-unencrypted-file>.unenc.json > <path-to-encrypted-file>.enc.json
# -e = encrypt

### Delete the unencrypted file

### You need to have sops installed
brew install sops
```
