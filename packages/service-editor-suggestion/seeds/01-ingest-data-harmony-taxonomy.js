require('dotenv').config()
const { Promise } = require('bluebird')
const { uniq } = require('lodash')
const logger = require('@pubsweet/logger')
const { dataHarmonyService, utils } = require('../src/services')

const {
  taxonomyTermFactory,
  TaxonomyTerm,
} = require('../src/models/taxonomyTerm')

exports.seed = async function ingestDataHarmonyTaxonomy(knex) {
  const result = await dataHarmonyService.getThesaurusTaxonomy()
  const taxonomy = result.terms.flatMap((term) => {
    if (!term.BT || term.BT.length === 1) {
      return {
        term: term.term,
        parent: term.BT ? term.BT[0] : null,
        parsedTerm: utils.mapTaxonomyPathForLTreeForm(term.term),
      }
    }
    const parents = uniq(term.BT) // necessary step because in data harmony there is a word that has the same parent listed twice
    return parents.map((parent) => ({
      term: term.term,
      parent,
      parsedTerm: utils.mapTaxonomyPathForLTreeForm(term.term),
    }))
  })

  const nest = (taxonomy, term = null) =>
    taxonomy
      .filter((item) => item.parent === term)
      .map((item) => ({ ...item, children: nest(taxonomy, item.term) }))

  const taxonomyTree = nest(taxonomy)

  async function insertTerm(term, parentId = null) {
    if (term == null) return
    const newTerm = taxonomyTermFactory.createTerm({
      term: term.term,
      parsedTerm: term.parsedTerm,
      parentId,
    })
    await TaxonomyTerm.insertGraph(newTerm)

    logger.info(`Saved Taxonomy Term with id ${newTerm.id}`)

    await Promise.each(term.children, async (term) =>
      insertTerm(term, newTerm.id),
    )
  }

  await Promise.each(taxonomyTree, async (term) => insertTerm(term))
}
