require('dotenv').config()
const config = require('config')

const logger = require('@pubsweet/logger')
const { createQueueService } = require('@hindawi/queue-service')
const { ApolloServer } = require('apollo-server')
const { buildSubgraphSchema } = require('@apollo/federation')
const { applyMiddleware } = require('graphql-middleware')
const { resolvers, typeDefs, permissions } = require('./src')
const tokenUtils = require('./src/permissions/rules/tokenUtils')
const {
  validateAndRegisterSchema,
} = require('@phenom.pub/schema-registry-cli/lib/register-schema')

const port = config.get('port')

function registerEventHandlers(messageQueue) {
  const handlers = require('./src/eventHandlers')

  Object.entries(handlers).forEach(([event, handler]) => {
    messageQueue.registerEventHandler({ event, handler })
  })
}

;(async function main() {
  const messageQueue = await createQueueService({
    region: config.get('aws.region'),
    accessKeyId: config.has('aws.sns_sqs.accessKeyId')
      ? config.get('aws.sns_sqs.accessKeyId')
      : undefined,
    secretAccessKey: config.has('aws.sns_sqs.secretAccessKey')
      ? config.get('aws.sns_sqs.secretAccessKey')
      : undefined,
    snsEndpoint: config.has('aws.sns.endpoint')
      ? config.get('aws.sns.endpoint')
      : undefined,
    sqsEndpoint: config.has('aws.sqs.endpoint')
      ? config.get('aws.sqs.endpoint')
      : undefined,
    s3Endpoint: config.has('largeEvents.s3Endpoint')
      ? config.get('largeEvents.s3Endpoint')
      : undefined,
    topicName: config.get('aws.sns.topic'),
    queueName: config.get('aws.sqs.queueName'),
    bucketName: config.get('largeEvents.bucketName'),
    bucketPrefix: config.get('largeEvents.bucketPrefix'),
    eventNamespace: config.get('eventNamespace'),
    publisherName: config.get('publisherName'),
    serviceName: config.get('serviceName'),
    defaultMessageAttributes: config.get('defaultMessageAttributes'),
  })

  registerEventHandlers(messageQueue)

  global.applicationEventBus = messageQueue

  messageQueue.start()

  const server = new ApolloServer({
    schema: applyMiddleware(
      buildSubgraphSchema([{ typeDefs, resolvers }]),
      permissions,
    ),
    context: async ({ req }) => {
      try {
        const user = await tokenUtils.verifyToken(req.headers.authorization)

        return {
          user,
        }
      } catch (e) {
        return {}
      }
    },
  })

  const service = {
    name: config.get('serviceName'),
    url: config.get('serviceSchemaUrl'),
  }
  try {
    await validateAndRegisterSchema(service, typeDefs)
    logger.info('Schema registered successfully!')
  } catch (err) {
    logger.error(`Schema registration failed: ${err.message}`)
    process.exit(1)
  }

  server.listen(port).then(async () => {
    logger.info(`Service Editor Suggestion listening on port ${port}.`)
  })
})()
