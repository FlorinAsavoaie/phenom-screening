const { S3 } = require('@aws-sdk/client-s3')
const { SQS } = require('@aws-sdk/client-sqs')
const config = require('config')

const logger = require('@pubsweet/logger')
const { S3EventProducer, SqsPublishConsumer } = require('@hindawi/eve')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

const sqs = new SQS({
  credentials:
    config.has('aws.sns_sqs.secretAccessKey') &&
    config.has('aws.sns_sqs.accessKeyId')
      ? {
          secretAccessKey: config.get('aws.sns_sqs.secretAccessKey'),
          accessKeyId: config.get('aws.sns_sqs.accessKeyId'),
        }
      : undefined,
  endpoint: config.get('aws.sqs.endpoint'),
  region: config.get('aws.region'),
})

module.exports = {
  up: async () => {
    const s3EventProducer = new S3EventProducer(
      s3,
      config.get('aws.s3.eventStorageBucket'),
    )

    const sqsPublisher = new SqsPublishConsumer(
      sqs,
      config.get('aws.sqs.queueName'),
    )

    // eslint-disable-next-line no-restricted-syntax
    for await (const events of s3EventProducer.produce()) {
      // eslint-disable-next-line no-restricted-syntax
      for (const event of events) {
        let eventBody

        try {
          eventBody = JSON.parse(event.Message)
        } catch (e) {
          logger.warn(`Can't parse event ${JSON.stringify(event)}\n${e}`)
        }

        if (
          eventBody &&
          eventBody.event &&
          [
            'JournalEditorAssigned',
            'JournalSectionEditorAssigned',
            'JournalSpecialIssueEditorAssigned',
            'JournalSectionSpecialIssueEditorAssigned',
            'JournalEditorRemoved',
            'JournalSectionEditorRemoved',
            'JournalSpecialIssueEditorRemoved',
            'JournalSectionSpecialIssueEditorRemoved',
          ].includes(eventBody.event.split(':').pop())
        ) {
          // eslint-disable-next-line no-await-in-loop
          await sqsPublisher.consume(event).catch(logger.error)
        }
      }
    }
  },
}
