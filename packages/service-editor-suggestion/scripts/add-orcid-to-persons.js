const logger = require('@pubsweet/logger')

/**
 * This script is used to add orcid id to all entries in person table

 *
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***

 * REVIEW_DATABASE=***
 * REVIEW_DB_HOST=***
 * REVIEW_DB_PASS=***
 * REVIEW_DB_USER=***
 * ```
 */
const config = require('config')
const { Promise } = require('bluebird')
const Knex = require('knex')

;(async function main() {
  try {
    const estDB = Knex({
      client: 'pg',
      connection: {
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        password: process.env.DB_PASS,
        database: process.env.DATABASE,
      },
    })

    const reviewDB = Knex({
      client: 'pg',
      connection: {
        user: process.env.REVIEW_DB_USER,
        password: process.env.REVIEW_DB_PASS,
        database: process.env.REVIEW_DATABASE,
        host: process.env.REVIEW_DB_HOST,
      },
    })

    const persons = await estDB('person').select('id')

    await Promise.each(persons, async (person) => {
      const orcid = await reviewDB('identity')
        .select('identifier')
        .where({ user_id: person.id, type: 'orcid' })
        .first()

      if (!orcid) return

      return estDB('person')
        .where('id', person.id)
        .update('orcid', orcid.identifier)
    })
  } catch (err) {
    logger.error(err)
    process.exit(1)
  }

  logger.info(`Process completed`)
  process.exit(0)
})()
