/**
 * That it's the purpose of this script?
 * After finished editor suggestion v2 we need to truncate editor table and ingest journal editors from event store
 * in order to recalculate all person terms again.
 *
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***
 *
 * AWS_S3_ACCESS_KEY
 * AWS_S3_SECRET_KEY
 * AWS_S3_EVENT_STORAGE_BUCKET
 *
 * AWS_SNS_SQS_ACCESS_KEY
 * AWS_SNS_SQS_SECRET_KEY
 * AWS_SQS_QUEUE_NAME
 * AWS_SQS_ENDPOINT
 * AWS_REGION
 * ```
 */

require('dotenv').config()
const { S3 } = require('@aws-sdk/client-s3')
const { SQS } = require('@aws-sdk/client-sqs')
const config = require('config')
const logger = require('@pubsweet/logger')
const Knex = require('knex')

const { S3EventProducer, SqsPublishConsumer } = require('@hindawi/eve')

const s3 = new S3({
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
  region: config.get('aws.region'),
})

const sqs = new AWS.SQS({
  credentials:
  config.has('aws.sqs.AKDisabled')
    ? undefined
    : config.has('aws.sns_sqs.secretAccessKey') &&
      config.has('aws.sns_sqs.accessKeyId')
      ? {
        secretAccessKey: config.get('aws.sns_sqs.secretAccessKey'),
        accessKeyId: config.get('aws.sns_sqs.accessKeyId'),
      }
      : undefined,
  endpoint: config.get('aws.sqs.endpoint'),
  region: config.get('aws.region'),
})

const screeningDB = Knex({
  client: 'pg',
  connection: config.get('pubsweet-server.db'),
})

;(async function main() {
  try {
    await screeningDB.transaction(async (trx) => {
      await trx.raw('TRUNCATE TABLE editor CASCADE')
      await processEventStore()
    })
    logger.info('Process completed')
    process.exit(0)
  } catch (err) {
    logger.error(err)
    process.exit(1)
  }
})()

async function processEventStore() {
  const s3EventProducer = new S3EventProducer(
    s3,
    config.get('aws.s3.eventStorageBucket'),
  )

  const sqsPublisher = new SqsPublishConsumer(
    sqs,
    config.get('aws.sqs.queueName'),
  )

  for await (const events of s3EventProducer.produce()) {
    for (const event of events) {
      let eventBody

      try {
        eventBody = JSON.parse(event.Message)
      } catch (e) {
        logger.warn(`Can't parse event ${JSON.stringify(event)}\n${e}`)
      }

      if (
        eventBody &&
        eventBody.event &&
        [
          'JournalEditorAssigned',
          'JournalSectionEditorAssigned',
          'JournalSpecialIssueEditorAssigned',
          'JournalSectionSpecialIssueEditorAssigned',
          'JournalEditorRemoved',
          'JournalSectionEditorRemoved',
          'JournalSpecialIssueEditorRemoved',
          'JournalSectionSpecialIssueEditorRemoved',
        ].includes(eventBody.event.split(':').pop())
      ) {
        await sqsPublisher.consume(event)
      }
    }
  }
}
