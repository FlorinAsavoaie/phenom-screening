const logger = require('@pubsweet/logger')

/**
 * This script is used to repopulate
 * terms for Editors who have zero
 */

const Knex = require('knex')
const { Promise } = require('bluebird')
const configService = require('config')
const logger = require('@pubsweet/logger')
const { createQueueService } = require('@hindawi/queue-service')
const wosDataRepository =
  require('@hindawi/lib-wos-data-store').wosDataRepository.initialize({
    configService,
  })

const services = require('../src/services')
const useCases = require('../src/use-cases')
const { factories, models } = require('../src/models')

const {
  getPersonTermsAndIngest,
  generateEditorSuggestionListForManuscripts,
  calculateScoreForEditors,
} = useCases

;(async function main() {
  try {
    const estDB = Knex({
      client: 'pg',
      connection: configService.get('pubsweet-server.db'),
    })

    const applicationEventBus = await createQueueService()

    const personsWithoutTerms = await estDB('person')
      .select('id', 'email', 'orcid')
      .whereNotIn('id', (builder) => {
        builder.distinct('person_id').from('person_taxonomy_term')
      })

    const personWithNewTermsIds = []

    await Promise.each(personsWithoutTerms, async (person) => {
      const { terms } = await getPersonTermsAndIngest
        .initialize({
          models,
          factories,
          services: {
            ...services,
            wosDataRepository,
          },
          logger,
        })
        .execute(person)

      if (terms.length) {
        personWithNewTermsIds.push(person.id)
      }
    })

    const editorsWithoutTerms = await estDB('editor')
      .select('assignation_id as assignationId', 'id', 'person_id as personId')
      .whereIn('person_id', personWithNewTermsIds)

    const editorsWithoutTermsWithPersonIds = editorsWithoutTerms.map(
      (editor) => ({
        ...editor,
        person: { id: editor.personId },
      }),
    )

    await generateEditorSuggestionListForManuscripts
      .initialize({
        models,
        factories,
        useCases: {
          calculateScoreForEditors: calculateScoreForEditors.initialize({
            models,
            factories,
            services,
          }),
        },
        services: { applicationEventBus },
      })
      .execute(editorsWithoutTermsWithPersonIds)
  } catch (err) {
    logger.error(err)
    process.exit(1)
  }

  logger.info(`Process completed`)
  process.exit(0)
})()
