const path = require('path')
const { initializeRootLogger } = require('hindawi-utils')

module.exports = {
  port: process.env.PORT || 3000,
  'pubsweet-server': {
    db: getDbConfig(),
    pool: { min: 0, max: 50 },
    logger: initializeRootLogger('ServiceEditorSuggestion'),
  },
  aws: {
    s3: {
      accessKeyId: process.env.AWS_S3_ACCESS_KEY,
      secretAccessKey: process.env.AWS_S3_SECRET_KEY,
      bucket: process.env.AWS_S3_BUCKET,
      eventStorageBucket: process.env.AWS_S3_EVENT_STORAGE_BUCKET,
      editorListCsvKey: process.env.AWS_S3_EDITOR_LIST_CSV_KEY,
    },
    sns_sqs: {
      accessKeyId: process.env.AWS_SNS_SQS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SNS_SQS_SECRET_KEY,
    },
    sqs: {
      AKDisabled: process.env.AWS_SQS_AK_DISABLED,
      endpoint: process.env.AWS_SQS_ENDPOINT,
      queueName: process.env.AWS_SQS_QUEUE_NAME,
    },
    sns: {
      endpoint: process.env.AWS_SNS_ENDPOINT,
      topic: process.env.AWS_SNS_TOPIC,
    },
    region: process.env.AWS_REGION,
  },
  dataHarmony: {
    endpoint: process.env.DH_ENDPOINT,
    projectName: process.env.DH_PROJECT_NAME,
    projectLocation: process.env.DH_PROJECT_LOCATION,
    projectUser: process.env.DH_PROJECT_USER,
    projectPass: process.env.DH_PROJECT_PASS,
    // default value used for custom keywords assigned by AE
    defaultOccurrences: 3,
  },
  eventNamespace: process.env.EVENT_NAMESPACE,
  publisherName: process.env.PUBLISHER_NAME,
  serviceName: getNameFromPackageJson(),
  defaultMessageAttributes: JSON.parse(
    process.env.DEFAULT_MESSAGE_ATTRIBUTES || '{}',
  ),
  largeEvents: {
    s3Endpoint: process.env.AWS_S3_ENDPOINT,
    bucketName: process.env.PHENOM_LARGE_EVENTS_BUCKET,
    bucketPrefix:
      process.env.PHENOM_LARGE_EVENTS_PREFIX || 'in-transit-events/',
  },
  wos: {
    elasticsearch: {
      node: process.env.WOS_ELASTICSEARCH_NODE,
      index: process.env.WOS_ELASTICSEARCH_INDEX,
      username: process.env.WOS_ELASTICSEARCH_USERNAME,
      password: process.env.WOS_ELASTICSEARCH_PASSWORD,
    },
  },
  keycloak: {
    certs: process.env.KEYCLOAK_CERTIFICATES,
    ssoClientId: process.env.SSO_CLIENT_ID,
  },
  serviceSchemaUrl:
    process.env.SERVICE_SCHEMA_URL ||
    'http://host.docker.internal:3008/graphql',
}

function getDbConfig() {
  return {
    database: process.env.DATABASE || 'editor_suggestion',
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    password: process.env.DB_PASSWORD || '',
    port: 5432,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 20000,
  }
}

function getNameFromPackageJson() {
  const filePath = path.resolve(process.cwd(), 'package.json')
  return require(filePath).name // eslint-disable-line import/no-dynamic-require
}
