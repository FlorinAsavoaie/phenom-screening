const { getPersonTermsAndIngest } = require('../src/use-cases')

const Chance = require('chance')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const noop = () => {
  throw new Error('should not be called')
}

const chance = new Chance()

const models = {
  Person: {
    upsertGraph: noop,
  },
  TaxonomyTerm: {
    findIn: noop,
    findInByPersonId: noop,
  },
  PersonExpertiseAuditLog: {
    insertGraph: noop,
    Modes: {
      AUTO: 'auto',
    },
  },
}

const services = {
  dataHarmonyService: {
    getTerms: noop,
  },
  wosDataRepository: {
    getWoSProfilesByEmailOrOrcid: noop,
  },
}

const factories = {
  personExpertiseAuditLogFactory: { createPersonExpertiseAuditLog: noop },
}

const logger = {
  warn: noop,
  info: noop,
}

const { TaxonomyTerm, Person, PersonExpertiseAuditLog } = models
const { personExpertiseAuditLogFactory } = factories
const { dataHarmonyService, wosDataRepository } = services

const person = {
  id: chance.guid(),
  email: chance.email(),
  orcid: 'orcid-id',
}

const dbTerm1 = {
  id: chance.guid(),
  term: 'Motivation',
  path: 'People_and_places.Population',
}

const dbTerm2 = {
  id: chance.guid(),
  term: 'Electronic',
  path: 'Engineering_and_technology',
}

const dhTerm1 = {
  term: 'Motivation',
  path: 'People_and_places.Population',
  occurrences: 3,
}

const dhTerm2 = {
  term: 'Electronic',
  path: 'Engineering_and_technology',
  occurrences: 2,
}

const dhTerms = [dhTerm1, dhTerm2]

const createdTerm1 = {
  id: dbTerm1.id,
  occurrences: dhTerm1.occurrences,
}
const createdTerm2 = {
  id: dbTerm2.id,
  occurrences: dhTerm2.occurrences,
}

const abstract = chance.sentence()
const wosProfiles = [
  {
    manuscriptAbstract: abstract,
  },
]

describe('Get terms for editors and save them in db', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should get terms for editors and save them in db', async () => {
    jest
      .spyOn(wosDataRepository, 'getWoSProfilesByEmailOrOrcid')
      .mockResolvedValueOnce(wosProfiles)
    jest.spyOn(dataHarmonyService, 'getTerms').mockResolvedValue(dhTerms)
    jest.spyOn(TaxonomyTerm, 'findIn').mockResolvedValue([dbTerm1, dbTerm2])
    jest.spyOn(Person, 'upsertGraph').mockResolvedValue({
      id: person.id,
      terms: [dbTerm1, dbTerm2],
    })
    jest
      .spyOn(TaxonomyTerm, 'findInByPersonId')
      .mockResolvedValue([createdTerm1, createdTerm2])

    jest
      .spyOn(personExpertiseAuditLogFactory, 'createPersonExpertiseAuditLog')
      .mockReturnValue({
        mode: PersonExpertiseAuditLog.Modes.AUTO,
        personId: person.id,
        assignedTerms: [createdTerm1, createdTerm2],
        unassignedTerms: [],
      })

    jest.spyOn(PersonExpertiseAuditLog, 'insertGraph').mockResolvedValue({
      mode: PersonExpertiseAuditLog.Modes.AUTO,
      personId: person.id,
      assignedTerms: [createdTerm1, createdTerm2],
      unassignedTerms: [],
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    await getPersonTermsAndIngest
      .initialize({
        services,
        models,
        factories,
        logger,
      })
      .execute(person)

    expect(
      wosDataRepository.getWoSProfilesByEmailOrOrcid,
    ).toHaveBeenCalledTimes(1)
    expect(wosDataRepository.getWoSProfilesByEmailOrOrcid).toHaveBeenCalledWith(
      { email: person.email, orcid: person.orcid },
    )

    expect(dataHarmonyService.getTerms).toHaveBeenCalledTimes(1)
    expect(dataHarmonyService.getTerms).toHaveBeenCalledWith(abstract)

    expect(Person.upsertGraph).toHaveBeenCalledTimes(1)
    expect(Person.upsertGraph).toHaveBeenCalledWith(
      { id: person.id, terms: [createdTerm1, createdTerm2] },
      { relate: true },
    )
  })

  it('should take the first 50 terms with the highest occurrences', async () => {
    const dhTerms = [...Array(100)].map((_, index) => ({
      term: 'Motivation',
      path: `${index} - People_and_places.Population`,
      occurrences: index,
    }))

    const savedTerms = [...Array(50)].map((_, index) => ({
      id: chance.guid(),
      term: 'Motivation',
      path: `${99 - index} - People_and_places.Population`,
    }))

    jest
      .spyOn(wosDataRepository, 'getWoSProfilesByEmailOrOrcid')
      .mockResolvedValueOnce(wosProfiles)
    jest.spyOn(dataHarmonyService, 'getTerms').mockResolvedValueOnce(dhTerms)
    jest.spyOn(TaxonomyTerm, 'findIn').mockResolvedValueOnce(savedTerms)
    jest.spyOn(Person, 'upsertGraph').mockResolvedValue({
      id: person.id,
      terms: savedTerms,
    })

    const createdTerms = savedTerms.map((id, occurrences) => ({
      id,
      occurrences,
    }))

    jest.spyOn(TaxonomyTerm, 'findInByPersonId').mockResolvedValue(createdTerms)

    jest
      .spyOn(personExpertiseAuditLogFactory, 'createPersonExpertiseAuditLog')
      .mockReturnValue({
        mode: PersonExpertiseAuditLog.Modes.AUTO,
        personId: person.id,
        assignedTerms: createdTerms,
        unassignedTerms: [],
      })

    jest.spyOn(PersonExpertiseAuditLog, 'insertGraph').mockResolvedValue({
      mode: PersonExpertiseAuditLog.Modes.AUTO,
      personId: person.id,
      assignedTerms: createdTerms,
      unassignedTerms: [],
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    await getPersonTermsAndIngest
      .initialize({
        models,
        services,
        factories,
        logger,
      })
      .execute(person)

    const termsPaths = savedTerms.map((term) => term.path)
    expect(TaxonomyTerm.findIn).toHaveBeenCalledWith('path', termsPaths)
  })

  it('should save person expertise audit log', async () => {
    jest
      .spyOn(wosDataRepository, 'getWoSProfilesByEmailOrOrcid')
      .mockResolvedValueOnce(wosProfiles)
    jest.spyOn(dataHarmonyService, 'getTerms').mockResolvedValueOnce(dhTerms)
    jest.spyOn(TaxonomyTerm, 'findIn').mockResolvedValueOnce([dbTerm1, dbTerm2])
    jest.spyOn(Person, 'upsertGraph').mockResolvedValue({
      id: person.id,
      terms: [dbTerm1, dbTerm2],
    })
    jest
      .spyOn(TaxonomyTerm, 'findInByPersonId')
      .mockResolvedValue([createdTerm1, createdTerm2])

    jest
      .spyOn(personExpertiseAuditLogFactory, 'createPersonExpertiseAuditLog')
      .mockReturnValue({
        mode: PersonExpertiseAuditLog.Modes.AUTO,
        personId: person.id,
        assignedTerms: [createdTerm1, createdTerm2],
        unassignedTerms: [],
      })

    jest.spyOn(PersonExpertiseAuditLog, 'insertGraph').mockResolvedValue({
      mode: PersonExpertiseAuditLog.Modes.AUTO,
      personId: person.id,
      assignedTerms: [createdTerm1, createdTerm2],
      unassignedTerms: [],
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    await getPersonTermsAndIngest
      .initialize({
        models,
        services,
        factories,
        logger,
      })
      .execute(person)

    expect(TaxonomyTerm.findInByPersonId).toHaveBeenCalledTimes(1)
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenCalledWith({
      termsIds: [createdTerm1.id, createdTerm2.id],
      personId: person.id,
    })

    expect(
      personExpertiseAuditLogFactory.createPersonExpertiseAuditLog,
    ).toHaveBeenCalledTimes(1)

    expect(
      personExpertiseAuditLogFactory.createPersonExpertiseAuditLog,
    ).toHaveBeenCalledWith({
      mode: PersonExpertiseAuditLog.Modes.AUTO,
      personId: person.id,
      assignedTerms: [createdTerm1, createdTerm2],
      unassignedTerms: [],
    })

    expect(PersonExpertiseAuditLog.insertGraph).toHaveBeenCalledTimes(1)

    expect(PersonExpertiseAuditLog.insertGraph).toHaveBeenCalledWith({
      mode: PersonExpertiseAuditLog.Modes.AUTO,
      personId: person.id,
      assignedTerms: [createdTerm1, createdTerm2],
      unassignedTerms: [],
    })
  })
})
