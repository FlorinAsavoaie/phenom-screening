const {
  removeAcademicEditorFromSectionSpecialIssue,
} = require('../src/use-cases')
const { Editor } = require('../src/models/editor')
const Chance = require('chance')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const chance = new Chance()

const journalId = chance.guid()

const createEditor = () => ({
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
})

const editor1 = createEditor()
const editor2 = createEditor()
const editor3 = createEditor()
const editor4 = createEditor()

const createSpecialIssue = () => ({
  id: chance.guid(),
})

const specialIssue1 = createSpecialIssue()
const specialIssue2 = createSpecialIssue()
const specialIssue3 = createSpecialIssue()
const specialIssue4 = createSpecialIssue()

const section1 = {
  id: chance.guid(),
  specialIssues: [
    { ...specialIssue1, editors: [editor1, editor2] },
    { ...specialIssue2, editors: [editor3, editor4] },
  ],
}

const section2 = {
  id: chance.guid(),
  specialIssues: [
    { ...specialIssue3, editors: [editor1, editor2] },
    { ...specialIssue4, editors: [editor3, editor4] },
  ],
}

const data = {
  id: journalId,
  sections: [section1, section2],
}

const useCases = {
  removeAcademicEditor: {
    execute: jest.fn(),
  },
}

describe('Remove academic editors from special issue from section', () => {
  it('should remove academic editors from special issue from section', async () => {
    await removeAcademicEditorFromSectionSpecialIssue
      .initialize({
        models: { Editor },
        useCases,
      })
      .execute(data)

    expect(useCases.removeAcademicEditor.execute).toHaveBeenCalledTimes(4)
    expect(useCases.removeAcademicEditor.execute).toHaveBeenNthCalledWith(1, {
      editorIds: [editor1.id, editor2.id],
      assignationId: specialIssue1.id,
      assignationType: Editor.AssignationTypes.SECTION_SPECIAL_ISSUE,
    })

    expect(useCases.removeAcademicEditor.execute).toHaveBeenNthCalledWith(2, {
      editorIds: [editor3.id, editor4.id],
      assignationId: specialIssue2.id,
      assignationType: Editor.AssignationTypes.SECTION_SPECIAL_ISSUE,
    })

    expect(useCases.removeAcademicEditor.execute).toHaveBeenNthCalledWith(3, {
      editorIds: [editor1.id, editor2.id],
      assignationId: specialIssue3.id,
      assignationType: Editor.AssignationTypes.SECTION_SPECIAL_ISSUE,
    })

    expect(useCases.removeAcademicEditor.execute).toHaveBeenNthCalledWith(4, {
      editorIds: [editor3.id, editor4.id],
      assignationId: specialIssue4.id,
      assignationType: Editor.AssignationTypes.SECTION_SPECIAL_ISSUE,
    })
  })
})
