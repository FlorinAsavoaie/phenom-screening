const { getSearchedTerms } = require('../src/use-cases')
const {
  TaxonomyTerm,
  taxonomyTermSerializer,
} = require('../src/models/taxonomyTerm')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const searchedTerms = [
  {
    id: '026b8fa0-700e-40a1-9f82-85b3bd30b6b6',
    term: 'Fruit and seed anatomy',
    parentId: '58c42f9f-2619-4edb-aa1e-6bf999736df4',
    parsedTerm: 'Fruit_and_seed_anatomy',
    path: 'Biology_and_life_sciences.Plant_science.Plant_anatomy.Fruit_and_seed_anatomy',
  },
  {
    id: 'b7906761-3013-4412-9b8b-31ab699bea62',
    term: 'Olive fruit fly',
    parentId: '87b731d7-8e61-4a46-83b4-2fc572b7a597',
    parsedTerm: 'Olive_fruit_fly',
    path: 'Biology_and_life_sciences.Agriculture.Pests.Insect_pests.Olive_fruit_fly',
  },
]

const searchedTermsPaths = [
  'Biology_and_life_sciences.Plant_science.Plant_anatomy.Fruit_and_seed_anatomy',
  'Biology_and_life_sciences.Agriculture.Pests.Insect_pests.Olive_fruit_fly',
]

const termsWithAscendants = [
  {
    id: '9917564d-60f7-4ea1-8705-0cd3e6e5b925',
    term: 'Biology and life sciences',
    parentId: null,
    parsedTerm: 'Biology_and_life_sciences',
    path: 'Biology_and_life_sciences',
  },
  {
    id: '1231a26f-7cc0-4e12-b02c-bfa2d654fbe2',
    term: 'Agriculture',
    parentId: '9917564d-60f7-4ea1-8705-0cd3e6e5b925',
    parsedTerm: 'Agriculture',
    path: 'Biology_and_life_sciences.Agriculture',
  },
  {
    id: '7fe78be1-ecaf-45d6-8ccb-a759bc967192',
    term: 'Pests',
    parentId: '1231a26f-7cc0-4e12-b02c-bfa2d654fbe2',
    parsedTerm: 'Pests',
    path: 'Biology_and_life_sciences.Agriculture.Pests',
  },
  {
    id: '87b731d7-8e61-4a46-83b4-2fc572b7a597',
    term: 'Insect pests',
    parentId: '7fe78be1-ecaf-45d6-8ccb-a759bc967192',
    parsedTerm: 'Insect_pests',
    path: 'Biology_and_life_sciences.Agriculture.Pests.Insect_pests',
  },
  {
    id: 'b7906761-3013-4412-9b8b-31ab699bea62',
    term: 'Olive fruit fly',
    parentId: '87b731d7-8e61-4a46-83b4-2fc572b7a597',
    parsedTerm: 'Olive_fruit_fly',
    path: 'Biology_and_life_sciences.Agriculture.Pests.Insect_pests.Olive_fruit_fly',
  },
  {
    id: '3d24fcba-7650-4603-9139-0e424664bd49',
    term: 'Plant science',
    parentId: '9917564d-60f7-4ea1-8705-0cd3e6e5b925',
    parsedTerm: 'Plant_science',
    path: 'Biology_and_life_sciences.Plant_science',
  },
  {
    id: '58c42f9f-2619-4edb-aa1e-6bf999736df4',
    term: 'Plant anatomy',
    parentId: '3d24fcba-7650-4603-9139-0e424664bd49',
    parsedTerm: 'Plant_anatomy',
    path: 'Biology_and_life_sciences.Plant_science.Plant_anatomy',
  },
  {
    id: '026b8fa0-700e-40a1-9f82-85b3bd30b6b6',
    term: 'Fruit and seed anatomy',
    parentId: '58c42f9f-2619-4edb-aa1e-6bf999736df4',
    parsedTerm: 'Fruit_and_seed_anatomy',
    path: 'Biology_and_life_sciences.Plant_science.Plant_anatomy.Fruit_and_seed_anatomy',
  },
]

const resultTree = [
  {
    id: '9917564d-60f7-4ea1-8705-0cd3e6e5b925',
    parentId: null,
    name: 'Biology and life sciences',
    path: 'Biology and life sciences',
    children: [
      {
        id: '1231a26f-7cc0-4e12-b02c-bfa2d654fbe2',
        parentId: '9917564d-60f7-4ea1-8705-0cd3e6e5b925',
        name: 'Agriculture',
        path: 'Biology and life sciences/Agriculture',
        children: [
          {
            id: '7fe78be1-ecaf-45d6-8ccb-a759bc967192',
            parentId: '1231a26f-7cc0-4e12-b02c-bfa2d654fbe2',
            name: 'Pests',
            path: 'Biology and life sciences/Agriculture/Pests',
            children: [
              {
                id: '87b731d7-8e61-4a46-83b4-2fc572b7a597',
                parentId: '7fe78be1-ecaf-45d6-8ccb-a759bc967192',
                name: 'Insect pests',
                path: 'Biology and life sciences/Agriculture/Pests/Insect pests',
                children: [
                  {
                    id: 'b7906761-3013-4412-9b8b-31ab699bea62',
                    parentId: '87b731d7-8e61-4a46-83b4-2fc572b7a597',
                    name: 'Olive fruit fly',
                    path: 'Biology and life sciences/Agriculture/Pests/Insect pests/Olive fruit fly',
                    children: [],
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        id: '3d24fcba-7650-4603-9139-0e424664bd49',
        parentId: '9917564d-60f7-4ea1-8705-0cd3e6e5b925',
        name: 'Plant science',
        path: 'Biology and life sciences/Plant science',
        children: [
          {
            id: '58c42f9f-2619-4edb-aa1e-6bf999736df4',
            parentId: '3d24fcba-7650-4603-9139-0e424664bd49',
            name: 'Plant anatomy',
            path: 'Biology and life sciences/Plant science/Plant anatomy',
            children: [
              {
                id: '026b8fa0-700e-40a1-9f82-85b3bd30b6b6',
                parentId: '58c42f9f-2619-4edb-aa1e-6bf999736df4',
                name: 'Fruit and seed anatomy',
                path: 'Biology and life sciences/Plant science/Plant anatomy/Fruit and seed anatomy',
                children: [],
              },
            ],
          },
        ],
      },
    ],
  },
]

describe('Get searched terms', () => {
  it('should return empty array if search value have less than 3 characters', async () => {
    const searchedValue = 'fr'
    TaxonomyTerm.getSearchedTerms = jest.fn()
    TaxonomyTerm.getAscendants = jest.fn()

    const tree = await getSearchedTerms
      .initialize({
        models: { TaxonomyTerm },
        serializers: { taxonomyTermSerializer },
      })
      .execute({ searchedValue })

    expect(TaxonomyTerm.getSearchedTerms).toHaveBeenCalledTimes(0)
    expect(TaxonomyTerm.getAscendants).toHaveBeenCalledTimes(0)
    expect(tree).toEqual([])
  })

  it("should return empty array if search value it's not found", async () => {
    const searchedValue = 'random-term'
    TaxonomyTerm.getSearchedTerms = jest.fn().mockResolvedValue([])

    const tree = await getSearchedTerms
      .initialize({
        models: { TaxonomyTerm },
        serializers: { taxonomyTermSerializer },
      })
      .execute({ searchedValue })

    expect(TaxonomyTerm.getSearchedTerms).toHaveBeenCalledTimes(1)
    expect(TaxonomyTerm.getAscendants).toHaveBeenCalledTimes(0)
    expect(tree).toEqual([])
  })

  it('should get terms and return a tree with children', async () => {
    const searchedValue = 'fruit'
    TaxonomyTerm.getSearchedTerms = jest.fn().mockResolvedValue(searchedTerms)
    TaxonomyTerm.getAscendants = jest
      .fn()
      .mockResolvedValue(termsWithAscendants)

    const tree = await getSearchedTerms
      .initialize({
        models: { TaxonomyTerm },
        serializers: { taxonomyTermSerializer },
      })
      .execute({ searchedValue })

    expect(TaxonomyTerm.getSearchedTerms).toHaveBeenCalledWith(searchedValue)
    expect(TaxonomyTerm.getAscendants).toHaveBeenCalledWith(searchedTermsPaths)
    expect(tree).toEqual(resultTree)
  })
})
