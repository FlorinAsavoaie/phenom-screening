const Chance = require('chance')

const chance = new Chance()

const { ingestAcademicEditor } = require('../src/use-cases')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const noop = () => {
  throw new Error('should not be called')
}

const models = {
  Person: {
    getPersonWithAssociatedEditorAndTerms: noop,
  },
  Editor: {
    insertGraph: noop,
  },
}

const factories = {
  personFactory: {
    createPerson: noop,
  },
  editorFactory: {
    createEditorProfile: noop,
  },
}

const useCases = {
  getPersonTermsAndIngest: {
    execute: noop,
  },
}

const logger = {
  info: noop,
}

const { Person, Editor } = models
const { personFactory, editorFactory } = factories
const { getPersonTermsAndIngest } = useCases

const assignationId = chance.guid()

const editor = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: { type: 'academicEditor', label: 'Academic Editor' },
}

const assignationType = 'journal'

const term1 = {
  id: chance.guid(),
  term: 'Payment',
  path: 'Social_sciences.Economics.Commerce.Payment',
  occurrences: 17,
}

const term2 = {
  id: chance.guid(),
  term: 'Finance',
  path: 'Social_sciences.Economics.Finance',
  occurrences: 8,
}

describe('Ingest academic editor', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('Return from use-case if person with editor already exist', async () => {
    jest
      .spyOn(Person, 'getPersonWithAssociatedEditorAndTerms')
      .mockResolvedValue({
        userId: chance.guid(),
        editors: [editor],
      })

    const result = ingestAcademicEditor
      .initialize({
        models,
        factories,
        useCases,
        logger,
      })
      .execute({
        editor,
        assignationId,
        assignationType,
      })

    expect(Person.getPersonWithAssociatedEditorAndTerms).toHaveBeenCalledTimes(
      1,
    )

    expect(Person.getPersonWithAssociatedEditorAndTerms).toHaveBeenCalledWith({
      editor,
      assignationId,
      assignationType,
    })
    expect(await result).toBe(undefined)
  })

  it('Save person and editor if not exist', async () => {
    const person = {
      id: editor.userId,
      email: editor.email,
    }

    jest
      .spyOn(Person, 'getPersonWithAssociatedEditorAndTerms')
      .mockResolvedValue(null)

    jest.spyOn(personFactory, 'createPerson').mockReturnValue({
      ...person,
      terms: [],
    })

    jest.spyOn(editorFactory, 'createEditorProfile').mockReturnValue(editor)

    jest.spyOn(Editor, 'insertGraph').mockReturnValue({
      ...editor,
      person: { ...person, terms: [] },
    })

    jest.spyOn(getPersonTermsAndIngest, 'execute').mockReturnValue({
      ...person,
      terms: [],
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    await ingestAcademicEditor
      .initialize({
        models,
        factories,
        useCases,
        logger,
      })
      .execute({
        editor,
        assignationId,
        assignationType,
      })

    expect(personFactory.createPerson).toHaveBeenCalledTimes(1)
    expect(personFactory.createPerson).toHaveBeenCalledWith(editor)

    expect(editorFactory.createEditorProfile).toHaveBeenCalledTimes(1)
    expect(editorFactory.createEditorProfile).toHaveBeenCalledWith({
      editor,
      assignationId,
      assignationType,
    })

    expect(Editor.insertGraph).toHaveBeenCalledTimes(1)
    expect(Editor.insertGraph).toHaveBeenCalledWith({
      ...editor,
      person: { ...person, terms: [] },
    })
  })

  it('Save editor for an existing person', async () => {
    const person = {
      id: editor.userId,
      email: editor.email,
    }

    jest
      .spyOn(Person, 'getPersonWithAssociatedEditorAndTerms')
      .mockResolvedValue({ ...person, editors: [] })

    jest.spyOn(editorFactory, 'createEditorProfile').mockReturnValue(editor)

    jest.spyOn(Editor, 'insertGraph').mockReturnValue({
      ...editor,
      person: { ...person, terms: [] },
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    await ingestAcademicEditor
      .initialize({
        models,
        factories,
        useCases,
        logger,
      })
      .execute({
        editor,
        assignationId,
        assignationType,
      })

    expect(editorFactory.createEditorProfile).toHaveBeenCalledTimes(1)
    expect(editorFactory.createEditorProfile).toHaveBeenCalledWith({
      editor,
      assignationId,
      assignationType,
    })

    expect(Editor.insertGraph).toHaveBeenCalledTimes(1)
    expect(Editor.insertGraph).toHaveBeenCalledWith(
      {
        ...editor,
        person: { ...person, editors: [] },
      },
      { relate: true },
    )

    expect(getPersonTermsAndIngest.execute).toHaveBeenCalledTimes(1)
    expect(getPersonTermsAndIngest.execute).toHaveBeenCalledWith({
      ...person,
      terms: [],
    })
  })

  it('Get person terms if there are none', async () => {
    const person = {
      id: editor.userId,
      email: editor.email,
    }

    jest
      .spyOn(Person, 'getPersonWithAssociatedEditorAndTerms')
      .mockResolvedValue(null)

    jest.spyOn(personFactory, 'createPerson').mockReturnValue(person)
    jest.spyOn(editorFactory, 'createEditorProfile').mockReturnValue(editor)

    jest.spyOn(Editor, 'insertGraph').mockReturnValue({
      ...editor,
      person: { ...person, terms: [] },
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    await ingestAcademicEditor
      .initialize({
        models,
        factories,
        useCases,
        logger,
      })
      .execute({
        editor,
        assignationId,
        assignationType,
      })

    expect(getPersonTermsAndIngest.execute).toHaveBeenCalledTimes(1)
    expect(getPersonTermsAndIngest.execute).toHaveBeenCalledWith({
      ...person,
      terms: [],
    })
  })

  it('Skip getting terms if there are already ingested', async () => {
    const person = {
      id: editor.userId,
      email: editor.email,
    }

    jest
      .spyOn(Person, 'getPersonWithAssociatedEditorAndTerms')
      .mockResolvedValue({ ...person, editors: [] })

    jest.spyOn(editorFactory, 'createEditorProfile').mockReturnValue(editor)

    jest.spyOn(Editor, 'insertGraph').mockReturnValue({
      ...editor,
      person: { ...person, terms: [term1, term2] },
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    await ingestAcademicEditor
      .initialize({
        models,
        factories,
        useCases,
        logger,
      })
      .execute({
        editor,
        assignationId,
        assignationType,
      })

    expect(getPersonTermsAndIngest.execute).toHaveBeenCalledTimes(0)
  })

  it('Return editor if everything works as expected', async () => {
    const person = {
      id: editor.userId,
      email: editor.email,
    }

    jest
      .spyOn(Person, 'getPersonWithAssociatedEditorAndTerms')
      .mockResolvedValue(null)

    jest.spyOn(personFactory, 'createPerson').mockReturnValue(person)
    jest.spyOn(editorFactory, 'createEditorProfile').mockReturnValue(editor)

    jest.spyOn(Editor, 'insertGraph').mockReturnValue({
      ...editor,
      person: { ...person, terms: [] },
    })

    jest.spyOn(logger, 'info').mockReturnValue()

    const result = ingestAcademicEditor
      .initialize({
        models,
        factories,
        useCases,
        logger,
      })
      .execute({
        editor,
        assignationId,
        assignationType,
      })

    expect(await result).toStrictEqual({
      ...editor,
      person: { ...person, terms: [] },
    })
  })
})
