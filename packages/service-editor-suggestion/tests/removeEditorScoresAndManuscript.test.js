const { removeEditorScoresAndManuscript } = require('../src/use-cases')
const { Manuscript } = require('../src/models/manuscript')
const { EditorManuscriptScore } = require('../src/models/editorManuscriptScore')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

describe('Remove Editor Scores Use Case', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should remove editor manuscript scores', async () => {
    const mockDelete = jest.fn()
    jest
      .spyOn(Manuscript, 'findOneBy')
      .mockResolvedValueOnce({ delete: mockDelete })

    await removeEditorScoresAndManuscript
      .initialize({
        models: { Manuscript, EditorManuscriptScore },
      })
      .execute({ submissionId: 'some-submission-id' })

    expect(Manuscript.findOneBy).toHaveBeenCalledTimes(1)
    expect(Manuscript.findOneBy).toHaveBeenCalledWith({
      queryObject: {
        submissionId: 'some-submission-id',
      },
    })

    expect(mockDelete).toHaveBeenCalledTimes(1)
  })
})
