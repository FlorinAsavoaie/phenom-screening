const Chance = require('chance')

const chance = new Chance()

const { editExpertise } = require('../src/use-cases')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const noop = () => {
  throw new Error('should not be called')
}

const models = {
  Person: {
    updateTerms: noop,
    find: noop,
  },
  TaxonomyTerm: {
    findInByPersonId: noop,
  },
  PersonExpertiseAuditLog: {
    Modes: {
      MANUAL: 'manual',
    },
    insertGraph: noop,
  },
}

const factories = {
  personExpertiseAuditLogFactory: {
    createPersonExpertiseAuditLog: noop,
  },
}

const services = {
  logger: {
    info: noop,
  },
  configService: {
    get: noop,
  },
}

const { Person, TaxonomyTerm, PersonExpertiseAuditLog } = models
const { personExpertiseAuditLogFactory } = factories
const { logger, configService } = services

describe('Edit expertise', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should throw an error if terms to assign are already assigned', async () => {
    const personId = chance.guid()
    const term1 = chance.guid()
    const term2 = chance.guid()
    const term3 = chance.guid()
    const term4 = chance.guid()
    const term5 = chance.guid()
    const assignedTermsIds = [term1, term2]
    const unassignedTermsIds = [term3, term4, term5]

    jest
      .spyOn(TaxonomyTerm, 'findInByPersonId')
      .mockResolvedValueOnce([{ id: term1 }])

    const result = editExpertise
      .initialize({
        models,
        factories,
        services,
      })
      .execute({
        personId,
        assignedTermsIds,
        unassignedTermsIds,
      })

    await expect(result).rejects.toThrow('Terms are already assigned')
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenCalledTimes(1)
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenNthCalledWith(1, {
      personId,
      termsIds: [term1, term2],
    })
  })

  it('should throw an error if unassigned terms are not assigned', async () => {
    const personId = chance.guid()
    const term1 = chance.guid()
    const term2 = chance.guid()
    const term3 = chance.guid()
    const term4 = chance.guid()
    const term5 = chance.guid()
    const assignedTermsIds = [term1, term2]
    const unassignedTermsIds = [term3, term4, term5]

    jest
      .spyOn(TaxonomyTerm, 'findInByPersonId')
      .mockResolvedValueOnce([])
      .mockResolvedValueOnce([{ id: term3 }])

    const result = editExpertise
      .initialize({
        models,
        factories,
        services,
      })
      .execute({
        personId,
        assignedTermsIds,
        unassignedTermsIds,
      })

    await expect(result).rejects.toThrow('Terms are not assigned yet')
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenCalledTimes(2)
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenNthCalledWith(1, {
      personId,
      termsIds: [term1, term2],
    })
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenNthCalledWith(2, {
      personId,
      termsIds: [term3, term4, term5],
    })
  })

  it('should update person expertise', async () => {
    const personId = chance.guid()
    const term1 = chance.guid()
    const term2 = chance.guid()
    const term3 = chance.guid()
    const term4 = chance.guid()
    const term5 = chance.guid()
    const assignedTermsIds = [term1, term2]
    const unassignedTermsIds = [term3, term4, term5]
    const DEFAULT_OCCURENCES = 1000

    // jest
    //   .spyOn(TaxonomyTerm, 'findInByPersonId')
    //   .mockResolvedValueOnce([])
    //   .mockResolvedValueOnce([{ id: term3 }, { id: term4 }, { id: term5 }])

    jest
      .spyOn(TaxonomyTerm, 'findInByPersonId')
      .mockResolvedValueOnce([])
      .mockResolvedValueOnce([{ id: term3 }, { id: term4 }, { id: term5 }])
      .mockResolvedValueOnce([{ id: term3 }, { id: term4 }, { id: term5 }])
      .mockResolvedValueOnce([{ id: term1 }, { id: term2 }])

    jest.spyOn(Person, 'find').mockResolvedValueOnce({ id: personId })

    jest.spyOn(Person, 'updateTerms').mockResolvedValueOnce({
      assignedTermsCount: assignedTermsIds.length,
      unassignedTermsCount: unassignedTermsIds.length,
    })
    jest.spyOn(configService, 'get').mockReturnValueOnce(DEFAULT_OCCURENCES)
    jest
      .spyOn(logger, 'info')
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()

    jest
      .spyOn(personExpertiseAuditLogFactory, 'createPersonExpertiseAuditLog')
      .mockReturnValue({
        mode: PersonExpertiseAuditLog.Modes.MANUAL,
        personId,
        assignedTerms: [{ id: term3 }, { id: term4 }, { id: term5 }],
        unassignedTerms: [{ id: term1 }, { id: term2 }],
      })

    jest.spyOn(PersonExpertiseAuditLog, 'insertGraph').mockResolvedValue({
      mode: PersonExpertiseAuditLog.Modes.MANUAL,
      personId,
      assignedTerms: [{ id: term3 }, { id: term4 }, { id: term5 }],
      unassignedTerms: [{ id: term1 }, { id: term2 }],
    })

    await editExpertise
      .initialize({
        models,
        factories,
        services,
      })
      .execute({
        personId,
        assignedTermsIds,
        unassignedTermsIds,
      })

    expect(TaxonomyTerm.findInByPersonId).toHaveBeenCalledTimes(4)
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenNthCalledWith(1, {
      personId,
      termsIds: [term1, term2],
    })
    expect(TaxonomyTerm.findInByPersonId).toHaveBeenNthCalledWith(2, {
      personId,
      termsIds: [term3, term4, term5],
    })
    expect(Person.updateTerms).toHaveBeenCalledTimes(1)
    expect(Person.updateTerms).toHaveBeenNthCalledWith(1, {
      personId,
      assignedTermsIds: [term1, term2],
      unassignedTermsIds: [term3, term4, term5],
      defaultOccurrences: 1000,
    })
    expect(logger.info).toHaveBeenCalledTimes(3)
    expect(logger.info).toHaveBeenNthCalledWith(
      1,
      `Related 2 terms for person with id "${personId}".`,
    )
    expect(logger.info).toHaveBeenNthCalledWith(
      2,
      `Unrelated 3 terms for person with id "${personId}".`,
    )
  })

  it('should save person expertise audit log', async () => {
    const personId = chance.guid()
    const term1 = chance.guid()
    const term2 = chance.guid()
    const term3 = chance.guid()
    const term4 = chance.guid()
    const term5 = chance.guid()
    const assignedTermsIds = [term1, term2]
    const unassignedTermsIds = [term3, term4, term5]
    const DEFAULT_OCCURENCES = 1000

    jest
      .spyOn(TaxonomyTerm, 'findInByPersonId')
      .mockResolvedValueOnce([])
      .mockResolvedValueOnce([{ id: term3 }, { id: term4 }, { id: term5 }])
      .mockResolvedValueOnce([{ id: term3 }, { id: term4 }, { id: term5 }])
      .mockResolvedValueOnce([{ id: term1 }, { id: term2 }])

    jest.spyOn(Person, 'find').mockResolvedValueOnce({ id: personId })

    jest.spyOn(Person, 'updateTerms').mockResolvedValueOnce({
      assignedTermsCount: assignedTermsIds.length,
      unassignedTermsCount: unassignedTermsIds.length,
    })
    jest.spyOn(configService, 'get').mockReturnValueOnce(DEFAULT_OCCURENCES)
    jest
      .spyOn(logger, 'info')
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()
      .mockResolvedValueOnce()

    jest
      .spyOn(personExpertiseAuditLogFactory, 'createPersonExpertiseAuditLog')
      .mockReturnValue({
        mode: PersonExpertiseAuditLog.Modes.MANUAL,
        personId,
        assignedTerms: [{ id: term1 }, { id: term2 }],
        unassignedTerms: [{ id: term3 }, { id: term4 }, { id: term5 }],
      })

    jest.spyOn(PersonExpertiseAuditLog, 'insertGraph').mockResolvedValue({
      mode: PersonExpertiseAuditLog.Modes.MANUAL,
      personId,
      assignedTerms: [{ id: term1 }, { id: term2 }],
      unassignedTerms: [{ id: term3 }, { id: term4 }, { id: term5 }],
    })

    await editExpertise
      .initialize({
        models,
        factories,
        services,
      })
      .execute({
        personId,
        assignedTermsIds,
        unassignedTermsIds,
      })

    expect(TaxonomyTerm.findInByPersonId).toHaveBeenCalledTimes(4)

    expect(TaxonomyTerm.findInByPersonId).toHaveBeenNthCalledWith(3, {
      personId,
      termsIds: [term3, term4, term5],
    })

    expect(TaxonomyTerm.findInByPersonId).toHaveBeenNthCalledWith(4, {
      personId,
      termsIds: [term1, term2],
    })

    expect(PersonExpertiseAuditLog.insertGraph).toHaveBeenCalledTimes(1)
    expect(PersonExpertiseAuditLog.insertGraph).toHaveBeenCalledWith({
      mode: PersonExpertiseAuditLog.Modes.MANUAL,
      personId,
      assignedTerms: [{ id: term1 }, { id: term2 }],
      unassignedTerms: [{ id: term3 }, { id: term4 }, { id: term5 }],
    })

    expect(logger.info).toHaveBeenCalledTimes(3)
  })
})
