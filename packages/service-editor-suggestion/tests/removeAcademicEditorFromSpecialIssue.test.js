const { removeAcademicEditorFromSpecialIssue } = require('../src/use-cases')
const { Editor } = require('../src/models/editor')
const Chance = require('chance')

const chance = new Chance()

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const journalId = chance.guid()

const createEditor = () => ({
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
})

const editor1 = createEditor()
const editor2 = createEditor()
const editor3 = createEditor()
const editor4 = createEditor()

const specialIssue1 = {
  id: chance.guid(),
}

const specialIssue2 = {
  id: chance.guid(),
}

const data = {
  id: journalId,
  specialIssues: [
    { ...specialIssue1, editors: [editor1, editor2] },
    { ...specialIssue2, editors: [editor3, editor4] },
  ],
}

const useCases = {
  removeAcademicEditor: {
    execute: jest.fn(),
  },
}

describe('Remove academic editors from special issue', () => {
  it('should remove academic editors from special issue', async () => {
    await removeAcademicEditorFromSpecialIssue
      .initialize({
        models: { Editor },
        useCases,
      })
      .execute(data)

    expect(useCases.removeAcademicEditor.execute).toHaveBeenCalledTimes(2)
    expect(useCases.removeAcademicEditor.execute).toHaveBeenNthCalledWith(1, {
      editorIds: [editor1.id, editor2.id],
      assignationId: specialIssue1.id,
      assignationType: Editor.AssignationTypes.SPECIAL_ISSUE,
    })

    expect(useCases.removeAcademicEditor.execute).toHaveBeenNthCalledWith(2, {
      editorIds: [editor3.id, editor4.id],
      assignationId: specialIssue2.id,
      assignationType: Editor.AssignationTypes.SPECIAL_ISSUE,
    })
  })
})
