const { generateEditorSuggestionList } = require('../src/use-cases')
const { noop } = require('lodash')
const { TaxonomyTerm } = require('../src/models/taxonomyTerm')
const { Manuscript } = require('../src/models/manuscript')
const { Editor } = require('../src/models/editor')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

describe('Generate editor suggestion list', () => {
  const mockTerms = [
    {
      term: 'term-A',
      occurrences: 2,
    },
    {
      term: 'term-B',
      occurrences: 5,
    },
  ]
  const dataHarmonyService = {
    getTerms: noop,
  }

  const s3Service = {
    getObjectBody: noop,
  }

  const logger = {
    info: noop,
  }

  const factories = {
    editorManuscriptScoreFactory: { createEditorManuscriptScore: noop },
    manuscriptFactory: {
      createManuscript: jest.fn().mockReturnValue({
        id: 'some-id',
        saveGraph: jest.fn().mockReturnValue({
          terms: mockTerms,
        }),
      }),
    },
  }

  const useCases = {
    calculateScoreForEditors: {
      execute: jest.fn().mockReturnValue([]),
    },
  }

  global.applicationEventBus = {
    publishMessage: jest.fn(async () => {}),
  }

  it('should ingest manuscript terms', async () => {
    jest
      .spyOn(s3Service, 'getObjectBody')
      .mockResolvedValueOnce('some text from s3')

    const termMock = { updateProperties: jest.fn() }
    jest.spyOn(TaxonomyTerm, 'findIn').mockReturnValue([termMock])
    jest.spyOn(Editor, 'findBy').mockReturnValue([{}])
    jest.spyOn(dataHarmonyService, 'getTerms').mockResolvedValueOnce(mockTerms)

    await generateEditorSuggestionList
      .initialize({
        services: { dataHarmonyService, s3Service, logger },
        models: { TaxonomyTerm, Manuscript, Editor },
        factories,
        useCases,
      })
      .execute({
        submissionId: 'some-submission-id',
        manuscripts: [
          {
            files: [
              {
                type: 'some-other-file-type',
                providerKey: 'wrong-provider-key',
              },
              {
                type: 'manuscript',
                providerKey: 'target-provider-key',
              },
            ],
          },
        ],
      })

    expect(s3Service.getObjectBody).toHaveBeenCalledTimes(1)
    expect(s3Service.getObjectBody).toHaveBeenCalledWith(
      'target-provider-key-text',
    )
  })
})
