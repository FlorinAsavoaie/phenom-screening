const {
  ingestAcademicEditorsFromSectionSpecialIssue,
} = require('../src/use-cases')
const { Editor } = require('../src/models/editor')
const Chance = require('chance')

const chance = new Chance()

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const journalId = chance.guid()

const editor1 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const editor2 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const specialIssue1 = {
  id: chance.guid(),
}

const specialIssue2 = {
  id: chance.guid(),
}

const section1 = {
  id: chance.guid(),
  specialIssues: [{ ...specialIssue1, editors: [editor1] }],
}

const section2 = {
  id: chance.guid(),
  specialIssues: [{ ...specialIssue2, editors: [editor2] }],
}

const data = {
  id: journalId,
  editors: [editor1, editor2],
  sections: [section1, section2],
}

const useCases = {
  ingestAcademicEditor: {
    execute: jest.fn(),
  },
  generateEditorSuggestionListForManuscripts: {
    execute: jest.fn(),
  },
}

describe('Ingest academic editors from special issue from section', () => {
  it('should ingest academic editors from special issue from section', async () => {
    const ingestedEditor = {
      ...editor1,
      assignationId: specialIssue1.id,
      person: {
        terms: [],
      },
    }
    jest
      .spyOn(useCases.ingestAcademicEditor, 'execute')
      .mockResolvedValueOnce(ingestedEditor)
      .mockResolvedValueOnce(null)

    await ingestAcademicEditorsFromSectionSpecialIssue
      .initialize({
        models: { Editor },
        useCases,
      })
      .execute(data)

    expect(useCases.ingestAcademicEditor.execute).toHaveBeenCalledTimes(2)
    expect(useCases.ingestAcademicEditor.execute).toHaveBeenNthCalledWith(1, {
      editor: { ...editor1, assignationId: specialIssue1.id },
      assignationId: specialIssue1.id,
      assignationType: Editor.AssignationTypes.SECTION_SPECIAL_ISSUE,
    })

    expect(useCases.ingestAcademicEditor.execute).toHaveBeenNthCalledWith(2, {
      editor: { ...editor2, assignationId: specialIssue2.id },
      assignationId: specialIssue2.id,
      assignationType: Editor.AssignationTypes.SECTION_SPECIAL_ISSUE,
    })

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledWith([ingestedEditor])

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledTimes(1)

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledWith([ingestedEditor])
  })
})
