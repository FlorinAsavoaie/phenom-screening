const { noop } = require('lodash')
const Chance = require('chance')
const {
  generateEditorSuggestionListForManuscripts,
} = require('../src/use-cases')
const { Manuscript } = require('../src/models/manuscript')
const { EditorManuscriptScore } = require('../src/models/editorManuscriptScore')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
  info: jest.fn(),
}))

const chance = new Chance()

describe('Generate editor suggestion list for all manuscripts published in a journal', () => {
  const factories = {
    editorManuscriptScoreFactory: { createEditorManuscriptScore: noop },
  }

  const useCases = {
    calculateScoreForEditors: {
      execute: jest.fn().mockResolvedValue([
        {
          score: 12,
        },
      ]),
    },
  }

  global.applicationEventBus = {
    publishMessage: jest.fn(async () => {}),
  }

  it('should compute new scores for all manuscripts where the editor could be assigned', async () => {
    const firstManuscript = {
      id: chance.guid(),
      submissionId: chance.guid(),
      getManuscriptTerms: jest.fn().mockResolvedValue([]),
    }

    const secondManuscript = {
      id: chance.guid(),
      submissionId: chance.guid(),
      getManuscriptTerms: jest.fn().mockResolvedValue([]),
    }

    const firstEditorId = chance.guid()
    const secondEditorId = chance.guid()

    const firstManuscriptScoreId = chance.guid()
    const secondManuscriptScoreId = chance.guid()

    const editors = [firstEditorId, secondEditorId]

    jest
      .spyOn(Manuscript, 'findBy')
      .mockReturnValue([firstManuscript, secondManuscript])
    jest.spyOn(EditorManuscriptScore, 'findBy').mockResolvedValue([
      {
        editorId: firstEditorId,
        score: 12,
      },
      {
        editorId: secondEditorId,
        score: 12,
      },
    ])
    jest.spyOn(EditorManuscriptScore, 'upsertGraph').mockResolvedValue({})

    jest.spyOn(EditorManuscriptScore, 'findOneBy').mockResolvedValue([
      {
        id: firstManuscriptScoreId,
      },
      {
        id: secondManuscriptScoreId,
      },
    ])

    await generateEditorSuggestionListForManuscripts
      .initialize({
        models: { Manuscript, EditorManuscriptScore },
        factories,
        useCases,
        services: { applicationEventBus },
      })
      .execute(editors)

    expect(global.applicationEventBus.publishMessage).toHaveBeenCalledTimes(2)
    expect(global.applicationEventBus.publishMessage).toHaveBeenNthCalledWith(
      1,
      {
        event: 'EditorSuggestionListGenerated',
        data: {
          submissionId: firstManuscript.submissionId,
          manuscriptId: firstManuscript.id,
          editorSuggestions: [
            {
              editorId: firstEditorId,
              score: 12,
            },
            {
              editorId: secondEditorId,
              score: 12,
            },
          ],
        },
      },
    )
  })
})
