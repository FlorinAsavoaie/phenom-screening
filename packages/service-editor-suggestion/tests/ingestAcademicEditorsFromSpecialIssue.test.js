const { ingestAcademicEditorsFromSpecialIssue } = require('../src/use-cases')
const { Editor } = require('../src/models/editor')
const Chance = require('chance')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const chance = new Chance()

const journalId = chance.guid()

const editor1 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const editor2 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const specialIssue1 = {
  id: chance.guid(),
}

const specialIssue2 = {
  id: chance.guid(),
}

const data = {
  id: journalId,
  editors: [editor1, editor2],
  specialIssues: [
    { ...specialIssue1, editors: [editor1] },
    { ...specialIssue2, editors: [editor2] },
  ],
}

const useCases = {
  ingestAcademicEditor: {
    execute: jest.fn(),
  },
  generateEditorSuggestionListForManuscripts: {
    execute: jest.fn(),
  },
}

describe('Ingest academic editors from special issue', () => {
  it('should ingest academic editors from special issue', async () => {
    const ingestedEditor = {
      ...editor1,
      assignationId: specialIssue1.id,
      person: {
        terms: [],
      },
    }
    jest
      .spyOn(useCases.ingestAcademicEditor, 'execute')
      .mockResolvedValueOnce(ingestedEditor)
      .mockResolvedValueOnce(null)

    await ingestAcademicEditorsFromSpecialIssue
      .initialize({
        models: { Editor },
        useCases,
      })
      .execute(data)

    expect(useCases.ingestAcademicEditor.execute).toHaveBeenCalledTimes(2)
    expect(useCases.ingestAcademicEditor.execute).toHaveBeenNthCalledWith(1, {
      editor: { ...editor1, assignationId: specialIssue1.id },
      assignationId: specialIssue1.id,
      assignationType: Editor.AssignationTypes.SPECIAL_ISSUE,
    })

    expect(useCases.ingestAcademicEditor.execute).toHaveBeenNthCalledWith(2, {
      editor: { ...editor2, assignationId: specialIssue2.id },
      assignationId: specialIssue2.id,
      assignationType: Editor.AssignationTypes.SPECIAL_ISSUE,
    })

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledWith([ingestedEditor])

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledTimes(1)

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledWith([ingestedEditor])
  })
})
