const { ingestAcademicEditorsFromJournal } = require('../src/use-cases')
const { Editor } = require('../src/models/editor')
const Chance = require('chance')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const chance = new Chance()

const journalId = chance.guid()

const editor1 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const editor2 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const data = {
  id: journalId,
  editors: [editor1, editor2],
}

const useCases = {
  ingestAcademicEditor: {
    execute: jest.fn(),
  },
  generateEditorSuggestionListForManuscripts: {
    execute: jest.fn(),
  },
}

describe('Ingest academic editors from journal', () => {
  it('should ingest academic editors from journal', async () => {
    const ingestedEditor = {
      ...editor1,
      assignationId: journalId,
      person: {
        terms: [],
      },
    }
    jest
      .spyOn(useCases.ingestAcademicEditor, 'execute')
      .mockResolvedValueOnce(ingestedEditor)
      .mockResolvedValueOnce(null)

    await ingestAcademicEditorsFromJournal
      .initialize({
        models: { Editor },
        useCases,
      })
      .execute(data)

    expect(useCases.ingestAcademicEditor.execute).toHaveBeenCalledTimes(2)
    expect(useCases.ingestAcademicEditor.execute).toHaveBeenNthCalledWith(1, {
      editor: editor1,
      assignationId: journalId,
      assignationType: Editor.AssignationTypes.JOURNAL,
    })

    expect(useCases.ingestAcademicEditor.execute).toHaveBeenNthCalledWith(2, {
      editor: editor2,
      assignationId: journalId,
      assignationType: Editor.AssignationTypes.JOURNAL,
    })

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledTimes(1)

    expect(
      useCases.generateEditorSuggestionListForManuscripts.execute,
    ).toHaveBeenCalledWith([ingestedEditor])
  })
})
