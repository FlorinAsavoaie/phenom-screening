const { removeAcademicEditor } = require('../src/use-cases')
const { Manuscript } = require('../src/models/manuscript')
const { Editor } = require('../src/models/editor')
const { EditorManuscriptScore } = require('../src/models/editorManuscriptScore')

const Chance = require('chance')

const chance = new Chance()

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
  info: jest.fn(),
}))

describe('Remove Academic Editor', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  global.applicationEventBus = {
    publishMessage: jest.fn(async () => {}),
  }

  it('should return from use-case if there are no editors to remove', async () => {
    const assignationId = chance.guid()
    const assignationType = 'journal'
    const editorIdsFromEvent = ['editor-id-1', 'editor-id-2']

    jest.spyOn(Editor, 'findAllBy').mockReturnValue([])
    jest.spyOn(Editor, 'deleteByIds')
    jest.spyOn(Manuscript, 'findBy')

    await removeAcademicEditor
      .initialize({
        Manuscript,
        Editor,
      })
      .execute({
        editorIds: editorIdsFromEvent,
        assignationId,
        assignationType,
      })

    expect(Editor.deleteByIds).toHaveBeenCalledTimes(0)
    expect(Manuscript.findBy).toHaveBeenCalledTimes(0)
    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(0)
  })

  it('should remove Academic Editor and generate new suggestions list', async () => {
    const assignationId = chance.guid()
    const assignationType = 'journal'
    const editorIdsFromEvent = ['editor-id-3', 'editor-id-4']

    const removedEditor1 = { id: 'editor-id-1' }
    const removedEditor2 = { id: 'editor-id-2' }

    const removedEditorMock = [removedEditor1, removedEditor2]

    const manuscript1 = {
      id: 'manuscript-id-1',
      submissionId: 'submission-id-1',
    }
    const editorManuscriptScores1 = [
      {
        editorId: 'editor-id-3',
        score: 0.8863,
      },
      {
        editorId: 'editor-id-4',
        score: 0.4519,
      },
    ]
    const editorManuscriptScores2 = [
      {
        editorId: 'editor-id-3',
        score: 0.8863,
      },
      {
        editorId: 'editor-id-4',
        score: 0.4519,
      },
    ]

    const manuscript2 = {
      id: 'manuscript-id-2',
      submissionId: 'submission-id-2',
    }

    const returnedAffectedManuscripts = [manuscript1, manuscript2]

    jest.spyOn(Editor, 'findAllBy').mockReturnValue(removedEditorMock)
    jest.spyOn(Editor, 'deleteByIds').mockReturnValue(1)

    jest
      .spyOn(Manuscript, 'findBy')
      .mockReturnValue(returnedAffectedManuscripts)

    jest
      .spyOn(EditorManuscriptScore, 'findBy')
      .mockReturnValueOnce(editorManuscriptScores1)
      .mockReturnValueOnce(editorManuscriptScores2)

    await removeAcademicEditor
      .initialize({
        Manuscript,
        Editor,
        EditorManuscriptScore,
      })
      .execute({
        editorIds: editorIdsFromEvent,
        assignationId,
        assignationType,
      })

    expect(Editor.findAllBy).toHaveBeenCalledTimes(1)
    expect(Editor.findAllBy).toHaveBeenCalledWith({
      excludedIds: editorIdsFromEvent,
      queryObject: {
        assignationType,
        assignationId,
      },
    })

    expect(Editor.deleteByIds).toHaveBeenCalledTimes(1)
    expect(Editor.deleteByIds).toHaveBeenCalledWith([
      removedEditor1.id,
      removedEditor2.id,
    ])

    expect(Manuscript.findBy).toHaveBeenCalledTimes(1)
    expect(Manuscript.findBy).toHaveBeenCalledWith({ assignationId })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(2)
    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(1, {
      event: 'EditorSuggestionListGenerated',
      data: {
        submissionId: manuscript1.submissionId,
        manuscriptId: manuscript1.id,
        editorSuggestions: editorManuscriptScores1.map((editorScore) => ({
          editorId: editorScore.editorId,
          score: editorScore.score,
        })),
      },
    })
    expect(applicationEventBus.publishMessage).toHaveBeenNthCalledWith(2, {
      event: 'EditorSuggestionListGenerated',
      data: {
        submissionId: manuscript2.submissionId,
        manuscriptId: manuscript2.id,
        editorSuggestions: editorManuscriptScores2.map((editorScore) => ({
          editorId: editorScore.editorId,
          score: editorScore.score,
        })),
      },
    })
  })
})
