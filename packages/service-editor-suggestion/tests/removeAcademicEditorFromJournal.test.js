const { removeAcademicEditorFromJournal } = require('../src/use-cases')
const { Editor } = require('../src/models/editor')
const Chance = require('chance')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const chance = new Chance()

const journalId = chance.guid()

const editor1 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const editor2 = {
  id: chance.guid(),
  userId: chance.guid(),
  email: chance.email(),
  role: {
    type: 'academicEditor',
  },
}

const data = {
  id: journalId,
  editors: [editor1, editor2],
}

const useCases = {
  removeAcademicEditor: {
    execute: jest.fn(),
  },
}

describe('Remove academic editors from journal', () => {
  it('should remove academic editors from journal', async () => {
    await removeAcademicEditorFromJournal
      .initialize({
        models: { Editor },
        useCases,
      })
      .execute(data)

    expect(useCases.removeAcademicEditor.execute).toHaveBeenCalledTimes(1)
    expect(useCases.removeAcademicEditor.execute).toHaveBeenCalledWith({
      editorIds: [editor1.id, editor2.id],
      assignationId: journalId,
      assignationType: Editor.AssignationTypes.JOURNAL,
    })
  })
})
