import { WithAwsSecretsCronProps } from '@hindawi/phenom-charts'

export const cronProps: WithAwsSecretsCronProps = {
  secretNames: ['prod/screening/job-wos-index-elasticsearch'],
  cronProps: {
    schedule: '0 0 * * *', // everyday at 00:00.
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/job-wos-index-elasticsearch',
      tag: 'production',
    },
    metadata: {
      labels: {
        owner: 'QAlas',
      },
    },
    concurrencyPolicy: 'Forbid',
    restartPolicy: 'Never',
  },
}
