import * as dotenv from 'dotenv'
import { HindawiCronChart, Cdk8sApp } from '@hindawi/phenom-charts'

dotenv.config()
;(async function main() {
  const rootConstruct = new Cdk8sApp({ outdir: 'dist-k8s' })

  // eslint-disable-next-line @typescript-eslint/no-var-requires,import/no-dynamic-require
  const { cronProps } = require(`./config/${process.env.NODE_ENV}`)

  await HindawiCronChart.withAwsSecrets(
    rootConstruct,
    'job-wos-index-elasticsearch',
    cronProps,
  )

  rootConstruct.synth()
})()
