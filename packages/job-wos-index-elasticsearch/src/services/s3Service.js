const { S3 } = require('@aws-sdk/client-s3')
const config = require('config')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

const wosBucket = config.get('aws.s3.wosBucket')
const FOLDER = 'UNPROCESSED_FILES'

const listAllObjects = (s3, params) =>
  s3
    .listObjectsV2(params)
    .then(({ Contents, IsTruncated, NextContinuationToken }) =>
      IsTruncated && NextContinuationToken
        ? listAllObjects(
            s3,
            Object.assign({}, params, {
              ContinuationToken: NextContinuationToken,
            }),
          ).then((x) => Contents.concat(x))
        : Contents,
    )

module.exports.getAllS3Files = async () =>
  listAllObjects(s3, { Bucket: wosBucket, Prefix: FOLDER })

module.exports.getObject = async (key) =>
  s3.getObject({ Key: key, Bucket: wosBucket })

module.exports.moveFile = async (key) => {
  await s3.copyObject({
    Bucket: wosBucket,
    CopySource: `${wosBucket}/${key}`,
    Key: key.split('/').slice(1).join('/'),
  })
  await s3.deleteObject({
    Bucket: wosBucket,
    Key: key,
  })
}
