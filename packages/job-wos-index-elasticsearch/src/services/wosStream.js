const config = require('config')
const wosDataService = require('./wosS3DataImportService')
const stream = require('stream')
const logger = require('@pubsweet/logger')

const wosDataRepository =
  require('@hindawi/lib-wos-data-store').wosDataRepository.initialize({
    configService: config,
  })
const { chain, get } = require('lodash')
const { Promise } = require('bluebird')

const summary = {
  fileToIngest: '',
  deleteFailed: [],
  manuscriptUpdateFailed: [],
  xmlParsingFailed: [],
}

module.exports.wosStream = async (fileToIngest) =>
  new Promise(async (resolve, reject) => {
    logger.info(`New file to migrate: ${fileToIngest.Key}`)

    summary.fileToIngest = fileToIngest.Key
    const wosDataStream = await wosDataService.wosDataImport(fileToIngest.Key)

    const processWoSEntryStream = new stream.Writable({
      objectMode: true,
      write: async ({ type, data }, encoding, callback) => {
        try {
          if (type === 'delete') {
            await deleteAuthorProfiles(data, summary)
          } else if (type === 'update') {
            if (
              get(data, 'static_data.summary.pub_info.pubtype') !== 'Journal'
            ) {
              return callback()
            }
            const wosAuthors = extractAuthorsFromManuscript(data)
            const abstract = getManuscriptAbstract(data)

            await wosDataRepository.indexWoSAuthors({
              wosAuthors,
              abstract,
              manuscriptWosId: data.uid,
            })
          }
          callback()
        } catch (err) {
          err.type = `processing-${type}-failed`
          err.data = { type, data }
          callback(err)
        }
      },
    })

    wosDataStream.pipe(processWoSEntryStream)

    processWoSEntryStream.on('finish', async () => {
      logger.info('WoS data ingestion finished')

      logger.info(summary)
      try {
        fileToIngest = {
          ...fileToIngest,
          Key: fileToIngest.Key.split('/').slice(1).join('/'),
        }
        await wosDataRepository.indexIngestedFiles(fileToIngest, summary)
      } catch (error) {
        logger.info('ingested files error', error)
      }
      resolve()
    })

    processWoSEntryStream.on('error', (err) => {
      logger.error(err)
      wosDataStream.pipe(processWoSEntryStream)

      if (err.type === 'processing-update-failed') {
        summary.manuscriptUpdateFailed.push({
          wosId: err.data.data.uid,
          error: err.message,
        })
      }
    })

    wosDataStream.on('error', (err) => {
      logger.error(err)

      if (err.type === 'xml-parsing') {
        summary.xmlParsingFailed.push({
          file: err.file,
          error: err.message,
        })
      }
    })

    async function deleteAuthorProfiles(line, deletionSummary) {
      let manuscriptWosIdsToDelete

      if (Array.isArray(line)) {
        manuscriptWosIdsToDelete = line.map((line) =>
          computeWosIdForDeletion(line),
        )
      } else {
        manuscriptWosIdsToDelete = [computeWosIdForDeletion(line)]
      }

      try {
        await Promise.each(
          manuscriptWosIdsToDelete,
          wosDataRepository.deleteWoSProfilesByManuscript,
        )
      } catch (err) {
        deletionSummary.notDeleted.count += 1
        deletionSummary.notDeleted.ids.push(...manuscriptWosIdsToDelete)
      }
    }

    function extractAuthorsFromManuscript(data) {
      if (get(data, 'static_data.summary.pub_info.pubtype') !== 'Journal') {
        return []
      }

      const wosAuthors = getWosAuthors(data)

      const manuscriptDoi = getManuscriptDoi(data)

      logger.info(`Processing ${data.uid} - ${wosAuthors.length} authors`)

      return wosAuthors.map((authorData) => {
        const affiliation = findAffiliation(data, authorData)
        const associatedContributors = getAssociatedContributors(
          data,
          authorData,
        )
        const orcId = getOrcId(associatedContributors)
        const researcherId = getResearcherId(associatedContributors)

        return {
          displayName: authorData.display_name,
          fullName: authorData.full_name,
          wosStandard: authorData.wos_standard,
          firstName: authorData.first_name,
          lastName: authorData.last_name,
          email: authorData.email_addr,
          affiliation,
          orcId,
          researcherId,
          manuscriptDoi,
          manuscriptWosId: data.uid,
        }
      })
    }

    function getWosAuthors(data) {
      return chain(data)
        .get('static_data.summary.names.name', [])
        .thru(castObjectToArray)
        .filter((author) => author.role === 'author')
        .value()
    }

    function findAffiliation(data, wosAuthor) {
      const wosAddresses = get(
        data,
        'static_data.fullrecord_metadata.addresses.address_name',
        [],
      )

      const addrNos = get(wosAuthor, 'addr_no', '').slice(0, 1)

      if (wosAddresses) {
        if (!Array.isArray(wosAddresses)) {
          return wosAddresses.address_spec
            ? get(wosAddresses, 'address_spec.full_address')
            : get(wosAddresses, 'full_address')
        }

        return chain(wosAddresses)
          .find(
            (address) =>
              address.address_spec && address.address_spec.addr_no === addrNos,
          )
          .get('address_spec.full_address')
          .value()
      }
    }

    function getAssociatedContributors(data, authorData) {
      return chain(data)
        .get('static_data.contributors.contributor')
        .thru(castObjectToArray)
        .compact()
        .filter(
          (contributor) => contributor.name.last_name === authorData.last_name,
        )
        .thru((contributors) =>
          contributors.length === 1
            ? contributors
            : contributors.filter(
                (c) => c.name.first_name === authorData.first_name,
              ),
        )
        .value()
    }

    function getOrcId(associatedContributors) {
      return chain(associatedContributors)
        .find((contributor) => contributor.name.orcid_id)
        .get('name.orcid_id')
        .value()
    }

    function getResearcherId(associatedContributors) {
      return chain(associatedContributors)
        .find((contributor) => contributor.name.r_id)
        .get('name.r_id')
        .value()
    }

    function castObjectToArray(data) {
      return Array.isArray(data) ? data : [data]
    }

    function getManuscriptAbstract(data) {
      return []
        .concat(
          get(
            data,
            'static_data.fullrecord_metadata.abstracts.abstract.abstract_text.p',
          ),
        )
        .join('\n')
    }

    function getManuscriptDoi(data) {
      return chain(data)
        .get('dynamic_data.cluster_related.identifiers.identifier')
        .find(
          (identifier) =>
            identifier.type === 'doi' || identifier.type === 'xref_doi',
        )
        .get('value')
        .value()
    }

    function computeWosIdForDeletion(line) {
      const [first, second] = line.split(',')

      return `${first}:${second}`
    }
  })
