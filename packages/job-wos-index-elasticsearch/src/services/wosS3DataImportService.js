const stream = require('stream')
const readline = require('readline')
const xmlNodes = require('xml-nodes')
const { Parser } = require('xml2js')
const logger = require('@pubsweet/logger')

const s3 = require('./s3Service')

module.exports.wosDataImport = async (file) => {
  const accumulatorStream = new stream.PassThrough({ objectMode: true })
  const { Body: fileStream } = await s3.getObject(file)

  if (file.endsWith('.del')) {
    handleDeleteFile(fileStream, file)
  } else {
    handleDataFile(fileStream, file)
  }
  return accumulatorStream

  function handleDeleteFile(fileStream, path) {
    const readLine = readline.createInterface({ input: fileStream })

    readLine.on('line', (line) => {
      accumulatorStream.push({ type: 'delete', data: line })
    })

    return new Promise((resolve) => {
      readLine.on('close', () => {
        logger.info(`Finished parsing file: ${path}`)
        accumulatorStream.push(null)
        resolve()
      })
    })
  }

  function handleDataFile(sourceStream, fileName) {
    stream.pipeline(
      sourceStream,
      xmlNodes('REC'),
      createXMLParserStream(),
      accumulatorStream,
      (err) => {
        if (err) {
          err.type = 'xml-parsing'
          err.file = fileName
          err.message = `XML parsing failed ${fileName} - ${err.message}`
          accumulatorStream.emit('error', err)
          accumulatorStream.push(null)
        }
      },
    )
  }

  function createXMLParserStream() {
    const parser = new Parser({
      explicitRoot: false,
      explicitArray: false,
      mergeAttrs: true,
      normalizeTags: true,
      normalize: true,
    })

    return new stream.Transform({
      objectMode: true,
      transform(chunk, encoding, callback) {
        parser.parseString(chunk.toString(), (err, data) => {
          if (err) {
            return callback(err)
          }
          this.push({
            type: 'update',
            data,
          })
          callback()
        })
      },
    })
  }
}
