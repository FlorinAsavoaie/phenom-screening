/* eslint-disable no-await-in-loop */
require('dotenv').config()
const logger = require('@pubsweet/logger')
const s3Service = require('./services/s3Service')
const wosStreamResult = require('./services/wosStream')
const { chain } = require('lodash')

async function ingestWosData() {
  const wosFilesFromS3 = await s3Service.getAllS3Files()

  const filesFromS3 = chain(wosFilesFromS3)
    .sortBy(['LastModified', 'Key'])
    .value()

  if (filesFromS3.length === 0) {
    logger.info(`No new file to migrate ...`)
  }

  // eslint-disable-next-line no-restricted-syntax
  for (const fileToIngest of filesFromS3) {
    await wosStreamResult.wosStream(fileToIngest)
    await s3Service.moveFile(fileToIngest.Key)
  }
}

ingestWosData()
