module.exports = {
  aws: {
    region: process.env.AWS_REGION,
    s3: {
      accessKeyId: process.env.AWS_S3_ACCESS_KEY,
      secretAccessKey: process.env.AWS_S3_SECRET_KEY,
      wosBucket: process.env.AWS_S3_WOS_BUCKET,
    },
  },
  wos: {
    elasticsearch: {
      node: process.env.WOS_ELASTICSEARCH_NODE,
      index: process.env.WOS_ELASTICSEARCH_INDEX,
      username: process.env.WOS_ELASTICSEARCH_USERNAME,
      password: process.env.WOS_ELASTICSEARCH_PASSWORD,
      indexIngestionDetails:
        process.env.WOS_ELASTICSEARCH_INDEX_INGESTION_DETAILS,
    },
  },
}
