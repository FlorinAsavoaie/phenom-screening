# Job wos index elasticsearch

The purpose of this service is to save and process archives from wos s3 to elasticsearch

### Required Env vars

```sh
AWS_REGION=eu-west-1

AWS_S3_WOS_BUCKET=****

WOS_ELASTICSEARCH_NODE=***
WOS_ELASTICSEARCH_INDEX=***
WOS_ELASTICSEARCH_PASSWORD=***
WOS_ELASTICSEARCH_USERNAME=***
WOS_ELASTICSEARCH_INDEX_INGESTION_DETAILS=***
```

### How to test it:
`yarn start`

### How to deploy it
Create image and deploy it. 
The `yarn synth` command will generate kubernetes manifests in `./dist-k8s` Those can be used with `kubectl apply -f ./dist-k8s/` to apply the changes on the cluster or `kubectl apply -f ./dist-k8s/ -n <env>` 



