import * as cdk from 'aws-cdk-lib'
import {
  ScreeningEnvironment,
  ScreeningEnvironmentType,
} from '../lib/screening-environment'

const app = new cdk.App()

new ScreeningEnvironment(app, 'QAScreeningEnvironment', {
  environmentType: ScreeningEnvironmentType.testing,
  environmentName: 'qa',
})
new ScreeningEnvironment(app, 'ProdScreeningEnvironment', {
  environmentType: ScreeningEnvironmentType.production,
  environmentName: 'production',
})
new ScreeningEnvironment(app, 'GSWQAScreeningEnvironment', {
  environmentType: ScreeningEnvironmentType.testing,
  environmentName: 'gsw-qa',
})
new ScreeningEnvironment(app, 'GSWProdScreeningEnvironment', {
  environmentType: ScreeningEnvironmentType.production,
  environmentName: 'gsw-production',
})
