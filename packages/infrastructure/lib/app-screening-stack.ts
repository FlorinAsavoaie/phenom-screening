import { Construct } from 'constructs'

import { ServiceWithDatabaseStack } from './constructs/service-with-database-stack'
import { AppConfig } from './constructs/service-stack'

export class AppScreeningStack extends ServiceWithDatabaseStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'screening-queue',
      serviceDeadLetterQueueName: 'screening-dead-letter-queue',
      databaseInstanceIdentifier: 'prod-app-screening-2',
    })
  }
}
