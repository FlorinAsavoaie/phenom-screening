import * as s3 from 'aws-cdk-lib/aws-s3'
import { Construct } from 'constructs'

import { ServiceWithDatabaseStack } from './constructs/service-with-database-stack'
import { AppConfig } from './constructs/service-stack'

export class AuthorVerificationStack extends ServiceWithDatabaseStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'author-verification-queue',
      serviceDeadLetterQueueName: 'author-verification-dead-letter-queue',
      databaseInstanceIdentifier: 'prod-author-verification',
    })

    new s3.Bucket(this, 'WOS-Articles-Bucket')
  }
}
