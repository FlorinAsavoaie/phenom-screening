import * as sns from 'aws-cdk-lib/aws-sns'
import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch'
import * as cwActions from 'aws-cdk-lib/aws-cloudwatch-actions'
import * as rds from 'aws-cdk-lib/aws-rds'
import { Construct } from 'constructs'

export interface DatabaseAlarmsConstructProps {
  notificationTopic: sns.ITopic
  database: rds.IDatabaseInstance
}

export class DatabaseAlarmsConstruct extends Construct {
  constructor(
    scope: Construct,
    id: string,
    props: DatabaseAlarmsConstructProps,
  ) {
    super(scope, id)

    const alarm = new cloudwatch.Alarm(this, 'CPUCreditBalance', {
      metric: props.database.metric('CPUCreditBalance'),
      threshold: 50,
      evaluationPeriods: 1,
      comparisonOperator:
        cloudwatch.ComparisonOperator.LESS_THAN_OR_EQUAL_TO_THRESHOLD,
    })

    alarm.addAlarmAction(new cwActions.SnsAction(props.notificationTopic))
  }
}
