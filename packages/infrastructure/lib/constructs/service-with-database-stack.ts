import { Construct } from 'constructs'
import * as rds from 'aws-cdk-lib/aws-rds'

import { DatabaseAlarmsConstruct } from './databaseAlarmsConstruct'
import { ServiceStack, ServiceStackProps } from './service-stack'

export interface ServiceWithDatabaseStackProps extends ServiceStackProps {
  databaseInstanceIdentifier: string
}

export class ServiceWithDatabaseStack extends ServiceStack {
  constructor(
    scope: Construct,
    id: string,
    props: ServiceWithDatabaseStackProps,
  ) {
    super(scope, id, props)

    const database = rds.DatabaseInstance.fromDatabaseInstanceAttributes(
      this,
      id,
      {
        instanceIdentifier: props.databaseInstanceIdentifier,
        instanceEndpointAddress: '',
        port: 5432,
        securityGroups: [],
      },
    )

    if (props.enableAlarms) {
      if (!props.notificationsTopic)
        throw new Error('notificationTopic not defined')

      new DatabaseAlarmsConstruct(this, `${id}DBAlarms`, {
        notificationTopic: props.notificationsTopic,
        database,
      })
    }
  }
}
