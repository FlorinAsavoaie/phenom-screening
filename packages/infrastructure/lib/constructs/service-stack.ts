import * as core from 'aws-cdk-lib'
import * as sqs from 'aws-cdk-lib/aws-sqs'
import * as sns from 'aws-cdk-lib/aws-sns'
import { Construct } from 'constructs'

import { EventsAlarmsConstruct } from './eventsAlarmsConstruct'

export interface ServiceStackProps extends core.StackProps {
  notificationsTopic?: sns.ITopic
  serviceQueueName: string
  serviceDeadLetterQueueName: string
  enableAlarms: boolean
}

export interface AppConfig extends core.StackProps {
  notificationsTopic?: sns.ITopic
  enableAlarms: boolean
}

export class ServiceStack extends core.Stack {
  constructor(scope: Construct, id: string, props: ServiceStackProps) {
    super(scope, id, props)

    const queueArn = core.Arn.format(
      {
        service: 'sqs',
        resource: props.serviceQueueName,
      },
      this,
    )

    const queue = sqs.Queue.fromQueueArn(this, 'queue', queueArn)

    const deadLetterQueueArn = core.Arn.format(
      {
        service: 'sqs',
        resource: props.serviceDeadLetterQueueName,
      },
      this,
    )

    const deadLetterQueue = sqs.Queue.fromQueueArn(
      this,
      'DLQ',
      deadLetterQueueArn,
    )

    if (props.enableAlarms) {
      if (!props.notificationsTopic)
        throw new Error('notificationTopic not defined')

      new EventsAlarmsConstruct(this, `${id}Alarms`, {
        notificationTopic: props.notificationsTopic,
        queue,
        deadLetterQueue,
      })
    }
  }
}
