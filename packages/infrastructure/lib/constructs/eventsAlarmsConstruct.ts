import * as sns from 'aws-cdk-lib/aws-sns'
import * as sqs from 'aws-cdk-lib/aws-sqs'
import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch'
import * as cwActions from 'aws-cdk-lib/aws-cloudwatch-actions'
import { Construct } from 'constructs'

export interface EventsAlarmsConstructProps {
  notificationTopic: sns.ITopic
  queue: sqs.IQueue
  deadLetterQueue: sqs.IQueue
}

export class EventsAlarmsConstruct extends Construct {
  constructor(scope: Construct, id: string, props: EventsAlarmsConstructProps) {
    super(scope, id)

    const numberOfMessagesAlarm = new cloudwatch.Alarm(
      this,
      'NumberOfMessages',
      {
        metric:
          props.deadLetterQueue.metricApproximateNumberOfMessagesVisible(),
        threshold: 1,
        evaluationPeriods: 1,
      },
    )

    numberOfMessagesAlarm.addAlarmAction(
      new cwActions.SnsAction(props.notificationTopic),
    )

    const messagesExpiresAlarm = new cloudwatch.Alarm(
      this,
      'MessageWillBeDeleted',
      {
        metric: props.deadLetterQueue.metricApproximateAgeOfOldestMessage(),
        threshold: 1123400, // 13 days since the message was added in the DLQ
        evaluationPeriods: 1,
      },
    )

    messagesExpiresAlarm.addAlarmAction(
      new cwActions.SnsAction(props.notificationTopic),
    )

    const noMessagesReceivedInNAmountOfTimeAlarm = new cloudwatch.Alarm(
      this,
      'NoMessagesReceived',
      {
        metric: props.queue.metricNumberOfMessagesReceived(),
        threshold: 0,
        evaluationPeriods: 24, // no messages consumed for 24 datapoints * 5 min period within 2 hours
        comparisonOperator:
          cloudwatch.ComparisonOperator.LESS_THAN_OR_EQUAL_TO_THRESHOLD,
      },
    )

    noMessagesReceivedInNAmountOfTimeAlarm.addAlarmAction(
      new cwActions.SnsAction(props.notificationTopic),
    )
  }
}
