import { Construct } from 'constructs'
import { ServiceStack, AppConfig } from './constructs/service-stack'

export class FileConversionStack extends ServiceStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'file-conversion-service-queue',
      serviceDeadLetterQueueName: 'file-conversion-service-dead-letter-queue',
    })
  }
}
