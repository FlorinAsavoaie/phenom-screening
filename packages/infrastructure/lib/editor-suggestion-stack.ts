import { Construct } from 'constructs'

import { ServiceWithDatabaseStack } from './constructs/service-with-database-stack'
import { AppConfig } from './constructs/service-stack'

export class EditorSuggestionStack extends ServiceWithDatabaseStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'editor-suggestion-queue',
      serviceDeadLetterQueueName: 'editor-suggestion-dead-letter-queue',
      databaseInstanceIdentifier: 'prod-editor-suggestion',
    })
  }
}
