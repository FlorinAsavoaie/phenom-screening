import * as os from 'aws-cdk-lib/aws-opensearchservice'
import * as core from 'aws-cdk-lib'
import * as sm from 'aws-cdk-lib/aws-secretsmanager'
import * as iam from 'aws-cdk-lib/aws-iam'
import { Construct } from 'constructs'

interface WosDataStoreConfig {
  dataNodeInstanceType: string
  dataNodes: number
  masterNodeInstanceType?: string
  masterNodes?: number
  volumeSize: number
}
export interface WosDataStoreStackProps extends core.StackProps {
  environmentName: string
  config: WosDataStoreConfig
}

export const WosDataStoreProductionConfig: WosDataStoreConfig = {
  dataNodeInstanceType: 'r5.large.search',
  dataNodes: 3,
  masterNodeInstanceType: 't3.medium.search',
  masterNodes: 3,
  volumeSize: 300,
}

export const WosDataStoreTestingConfig: WosDataStoreConfig = {
  dataNodeInstanceType: 't3.medium.search',
  dataNodes: 1,
  volumeSize: 20,
}

export class WosDataStoreStack extends core.Stack {
  constructor(scope: Construct, id: string, props: WosDataStoreStackProps) {
    super(scope, id, props)

    const secrets = sm.Secret.fromSecretNameV2(
      this,
      'WoSDataStoreSecrets',
      `screening/${props.environmentName}/wosDataStore`,
    )

    const searchDomain = new os.Domain(this, 'ElasticsearchDomain', {
      version: os.EngineVersion.OPENSEARCH_1_0,
      domainName: `wos-index-${props.environmentName}`,
      capacity: {
        dataNodeInstanceType: props.config.dataNodeInstanceType,
        dataNodes: props.config.dataNodes,
        masterNodeInstanceType: props.config.masterNodeInstanceType,
        masterNodes: props.config.masterNodes,
      },
      ebs: {
        volumeSize: props.config.volumeSize,
      },
      enforceHttps: true,
      encryptionAtRest: { enabled: true },
      nodeToNodeEncryption: true,
      useUnsignedBasicAuth: true,
      fineGrainedAccessControl: {
        masterUserName: secrets
          .secretValueFromJson('elasticsearchUsername')
          .toString(),
        masterUserPassword: secrets.secretValueFromJson(
          'elasticsearchPassword',
        ),
      },
    })

    searchDomain.grantReadWrite({
      grantPrincipal: new iam.ArnPrincipal(searchDomain.domainArn),
    })
  }
}
