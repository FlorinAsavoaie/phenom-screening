import { Construct } from 'constructs'
import { ServiceStack, AppConfig } from './constructs/service-stack'

export class LanguageCheckStack extends ServiceStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'language-check-queue',
      serviceDeadLetterQueueName: 'language-check-dead-letter-queue',
    })
  }
}
