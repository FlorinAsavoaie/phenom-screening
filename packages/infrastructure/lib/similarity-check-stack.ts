import { Construct } from 'constructs'
import { AppConfig, ServiceStack } from './constructs/service-stack'

export class SimilarityCheckStack extends ServiceStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'similarity-check-queue',
      serviceDeadLetterQueueName: 'similarity-check-dead-letter-queue',
    })
  }
}
