import { Construct } from 'constructs'

import { ServiceWithDatabaseStack } from './constructs/service-with-database-stack'
import { AppConfig } from './constructs/service-stack'

export class SuspiciousKeywordsStack extends ServiceWithDatabaseStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'suspicious-keywords-queue',
      serviceDeadLetterQueueName: 'suspicious-keywords-dead-letter-queue',
      databaseInstanceIdentifier: 'prod-suspicious-keywords',
    })
  }
}
