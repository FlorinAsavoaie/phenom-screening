import * as core from 'aws-cdk-lib'
import * as sns from 'aws-cdk-lib/aws-sns'
import * as subs from 'aws-cdk-lib/aws-sns-subscriptions'
import { Construct } from 'constructs'

import { AppScreeningStack } from './app-screening-stack'
import { AuthorVerificationStack } from './author-verification-stack'
import { LanguageCheckStack } from './language-check-stack'
import { FileConversionStack } from './file-conversion-stack'
import { SuspiciousKeywordsStack } from './suspicious-keywords-stack'
import { SanctionListStack } from './sanction-list-stack'
import { EditorSuggestionStack } from './editor-suggestion-stack'
import { SimilarityCheckStack } from './similarity-check-stack'
import {
  WosDataStoreStack,
  WosDataStoreProductionConfig,
  WosDataStoreTestingConfig,
} from './wos-data-store-stack'

export enum ScreeningEnvironmentType {
  production = 'production',
  testing = 'testing',
}
export interface ScreeningEnvironmentProps extends core.StackProps {
  environmentType: ScreeningEnvironmentType
  environmentName: string
}

export class ScreeningEnvironment extends core.Stack {
  constructor(scope: Construct, id: string, props: ScreeningEnvironmentProps) {
    super(scope, id, props)

    const enableAlarms =
      props.environmentType === ScreeningEnvironmentType.production

    const notificationsTopic = enableAlarms
      ? this.createMonitoringTopic()
      : undefined

    new AppScreeningStack(this, 'AppScreening', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    new AuthorVerificationStack(this, 'AuthorVerification', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    new FileConversionStack(this, 'FileConversion', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    new LanguageCheckStack(this, 'LanguageCheck', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    new SuspiciousKeywordsStack(this, 'SuspiciousKeywords', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    new SanctionListStack(this, 'SanctionList', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    new SimilarityCheckStack(this, 'SimilarityCheck', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    new EditorSuggestionStack(this, 'EditorSuggestion', {
      ...props,
      notificationsTopic,
      enableAlarms,
    })

    const wosDataStoreConfig =
      props.environmentType === ScreeningEnvironmentType.production
        ? WosDataStoreProductionConfig
        : WosDataStoreTestingConfig

    new WosDataStoreStack(this, 'WoSDataStore', {
      ...props,
      environmentName: props.environmentName,
      config: wosDataStoreConfig,
    })
  }

  private createMonitoringTopic(): sns.ITopic {
    const notificationsTopic = new sns.Topic(this, 'Notification-Topic', {
      topicName: 'qalas-notifications',
    })

    notificationsTopic.addSubscription(
      new subs.EmailSubscription('team.qalas@hindawi.com'),
    )

    return notificationsTopic
  }
}
