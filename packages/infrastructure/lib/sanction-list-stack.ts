import { Construct } from 'constructs'

import { ServiceWithDatabaseStack } from './constructs/service-with-database-stack'
import { AppConfig } from './constructs/service-stack'

export class SanctionListStack extends ServiceWithDatabaseStack {
  constructor(scope: Construct, id: string, props: AppConfig) {
    super(scope, id, {
      ...props,
      serviceQueueName: 'sanction-list-queue',
      serviceDeadLetterQueueName: 'sanction-list-dead-letter-queue',
      databaseInstanceIdentifier: 'prod-sanction-list',
    })
  }
}
