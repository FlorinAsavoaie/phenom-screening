#infrastructure 

### grafana alerts back-up

Inside `grafana\alerts` you'll find more environments specific JSONs generated from Grafana, that can be used to regenerate the alerts in case of loss after migrations, updates or other infrastructure related factors.

! Please make sure these JSONs are updated each time you add or update alerts inside Grafana !

For a guideline about how to update or use these JSONs, read the last heading in here: 
https://confluence.wiley.com/x/rxQnDQ

