import {
  WithAwsSecretsServiceProps,
  ConfigurationMountType,
} from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['gsw-prod/app-gql-schema-registry/database'],
  serviceProps: {
    ...defaultValues.serviceProps,
    configMaps: {
      'app-gql-schema-registry-redis': {
        as: ConfigurationMountType.ENV,
        items: {
          'prod-gsw-gql-schema-registry-redis.endpoint': 'REDIS_HOST',
          'prod-gsw-gql-schema-registry-redis.port': 'REDIS_PORT',
        },
      },
    },
  },
}
