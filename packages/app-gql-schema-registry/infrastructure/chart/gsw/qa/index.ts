import {
  WithAwsSecretsServiceProps,
  ConfigurationMountType,
} from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['qa-gsw/app-gql-schema-registry/database'],
  serviceProps: {
    ...defaultValues.serviceProps,
    configMaps: {
      'app-gql-schema-registry-redis': {
        as: ConfigurationMountType.ENV,
        items: {
          'qa-gsw-gql-schema-registry.endpoint': 'REDIS_HOST',
          'qa-gsw-gql-schema-registry.port': 'REDIS_PORT',
        },
      },
    },
  },
}
