import {
  WithAwsSecretsServiceProps,
  ConfigurationMountType,
  ServiceType,
} from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['automation/review-app-gql-schema-registry/database'],
  serviceProps: {
    ...defaultValues.serviceProps,
    service: {
      port: 80,
      type: ServiceType.CLUSTER_IP,
      name: 'review-app-gql-schema-registry',
    },
    configMaps: {
      'review-app-gql-schema-registry-redis': {
        as: ConfigurationMountType.ENV,
        items: {
          'review-gql-schema-registry-redis.endpoint': 'REDIS_HOST',
          'review-gql-schema-registry-redis.port': 'REDIS_PORT',
        },
      },
    },
  },
}
