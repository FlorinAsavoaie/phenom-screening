import {
  WithAwsSecretsServiceProps,
  ConfigurationMountType,
} from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['qa/app-gql-schema-registry/database'],
  serviceProps: {
    ...defaultValues.serviceProps,
    configMaps: {
      'app-gql-schema-registry-redis': {
        as: ConfigurationMountType.ENV,
        items: {
          'gql-schema-registry-redis.endpoint': 'REDIS_HOST',
          'gql-schema-registry-redis.port': 'REDIS_PORT',
        },
      },
    },
  },
}
