import {
  WithAwsSecretsServiceProps,
  ConfigurationMountType,
  ServiceType,
} from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['automation/mail-builder-app-gql-schema-registry/database'],
  serviceProps: {
    ...defaultValues.serviceProps,
    service: {
      port: 80,
      type: ServiceType.CLUSTER_IP,
      name: 'mail-builder-app-gql-schema-registry',
    },
    configMaps: {
      'mail-builder-app-gql-schema-registry-redis': {
        as: ConfigurationMountType.ENV,
        items: {
          'mail-builder-gql-schema-registry-redis.endpoint': 'REDIS_HOST',
          'mail-builder-gql-schema-registry-redis.port': 'REDIS_PORT',
        },
      },
    },
  },
}
