import {
  WithAwsSecretsServiceProps,
  ConfigurationMountType,
  ServiceType,
} from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['automation/screening-app-gql-schema-registry/database'],
  serviceProps: {
    ...defaultValues.serviceProps,
    service: {
      port: 80,
      type: ServiceType.CLUSTER_IP,
      name: 'screening-app-gql-schema-registry',
    },
    configMaps: {
      'screening-app-gql-schema-registry-redis': {
        as: ConfigurationMountType.ENV,
        items: {
          'screening-gql-schema-registry-redis.endpoint': 'REDIS_HOST',
          'screening-gql-schema-registry-redis.port': 'REDIS_PORT',
        },
      },
    },
  },
}
