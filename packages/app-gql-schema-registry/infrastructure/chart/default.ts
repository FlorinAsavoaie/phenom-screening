import { WithAwsSecretsServiceProps, ServiceType } from '@hindawi/phenom-charts'

const defaultValues: WithAwsSecretsServiceProps = {
  secretNames: [],
  serviceProps: {
    image: {
      repository: 'pipedrive/graphql-schema-registry',
      tag: '3.4.0',
    },
    replicaCount: 1,
    service: {
      port: 80,
      type: ServiceType.CLUSTER_IP,
      name: 'app-gql-schema-registry',
    },
    containerPort: 6001,
    labels: {
      owner: 'QAlas',
    },
    resources: {
      requests: {
        memory: '200Mi',
      },
    },
    livenessProbe: {
      path: '/health',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    readinessProbe: {
      path: '/health',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    startupProbe: {
      path: '/health',
      failureThreshold: 30,
      periodSeconds: 10,
    },
  },
}

export { defaultValues }
