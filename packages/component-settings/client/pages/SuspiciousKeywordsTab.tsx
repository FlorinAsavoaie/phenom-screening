import { useState, ReactElement } from 'react'
import { Item, Pagination } from '@hindawi/ui'
import {
  Icon as BaseIcon,
  SearchField,
  usePaginatedQuery,
} from 'hindawi-shared/client/components'

import { th } from '@pubsweet/ui-toolkit'
import { Modal } from '@pubsweet/ui'
import { Button } from '@hindawi/phenom-ui'
import styled from 'styled-components'

import {
  SuspiciousKeywordsTable,
  SuspiciousKeywordModal,
  ExportSuspiciousKeywordsButton,
  ImportSuspiciousKeywordsButton,
} from '../components'
import { getSuspiciousKeywords } from '../graphql/queries'
import { useAddSuspiciousKeyword } from '../graphql/hooks/useAddSuspiciousKeyword'

const SuspiciousKeywordsTab: React.FC = () => {
  const [searchValue, setSearchValue] = useState('')
  const [inputValue, setInputValue] = useState('')

  const itemsPerPage = 11

  const {
    error,
    loading,
    page,
    setPage,
    results,
    toFirst,
    toLast,
    toNext,
    toPrevious,
    total,
    refetch,
  } = usePaginatedQuery(getSuspiciousKeywords, {
    variables: { searchValue, pageSize: itemsPerPage },
    settings: {},
  })
  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>): void =>
    setInputValue(event.target.value)
  const addSuspiciousKeyword = useAddSuspiciousKeyword()

  const reset = (): void => {
    setSearchValue('')
    setInputValue('')
    refetch()
    toFirst()
  }

  const initialValues = {
    keyword: '',
    note: '',
  }

  interface ModalComponentProps {
    hideModal: () => void
    onSubmit: (values: any) => void
  }

  return (
    <ScrollContainer>
      <TabRoot>
        <TopContainer>
          <SearchField
            mb={4}
            onChange={handleOnChange}
            onSearch={(item: string): void => {
              toFirst()
              setSearchValue(item)
            }}
            placeholder="Search"
            value={inputValue}
            width={88}
          />
          <ButtonsContainer>
            <ExportSuspiciousKeywordsButton />
            <ImportSuspiciousKeywordsButton />
            <Modal
              component={({
                hideModal,
                onSubmit,
              }: ModalComponentProps): ReactElement => (
                <SuspiciousKeywordModal
                  hideModal={hideModal}
                  keyword={initialValues}
                  modalTitle="Add suspicious keyword"
                  modalType="addKeyword"
                  onSubmit={onSubmit}
                />
              )}
              justifyContent="flex-end"
              modalKey="addSuspiciousKeyword"
              onSubmit={async ({ keyword, note }): Promise<void> => {
                await addSuspiciousKeyword({ keyword, note })
                reset()
              }}
            >
              {(showModal: any): ReactElement => (
                <Button onClick={showModal} type="primary">
                  <Icon color="white" mr="2" name="plus" size={3} />
                  Add keyword
                </Button>
              )}
            </Modal>
          </ButtonsContainer>
        </TopContainer>
        <SuspiciousKeywordsTable
          error={error}
          loading={loading}
          noItemsMessage="No results found."
          resetInputFields={reset}
          suspiciousKeywords={results}
        />
        {total > 0 && (
          <Item justify="flex-end" mt={4}>
            <Pagination
              itemsPerPage={itemsPerPage}
              nextPage={toNext}
              page={page}
              prevPage={toPrevious}
              setPage={setPage}
              toFirst={toFirst}
              toLast={toLast}
              totalCount={total}
            />
          </Item>
        )}
      </TabRoot>
    </ScrollContainer>
  )
}

export { SuspiciousKeywordsTab }

export const TabRoot = styled.div`
  background-color: ${th('white')};
  box-shadow: 0 ${th('gridUnit')} calc(${th('gridUnit')} * 2) 0
    rgba(51, 51, 51, 0.15);
  border-radius: ${th('borderRadius')};
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: calc(${th('gridUnit')} * 2);
  padding: calc(${th('gridUnit')} * 4);
`
export const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
`
export const ButtonsContainer = styled.div`
  display: flex;
`

export const ScrollContainer = styled.div`
  overflow: auto;
`
const Icon = styled(BaseIcon)`
  cursor: pointer;
`

const AddKeywordButton = styled(Button)`
  align-items: center;
  border: none;
  color: ${th('white')};
  font-size: ${th('fontSizeBase')};
  font-stretch: normal;
  font-style: normal;
  font-weight: bold;
  height: 32px;
  letter-spacing: normal;
  line-height: normal;
  text-align: center;
  width: calc(${th('gridUnit')} * 33);

  :hover,
  :focus {
    background-color: ${th('actionPrimaryColor')};
  }
`
