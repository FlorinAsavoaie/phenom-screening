import { useState } from 'react'
import { Item, Pagination, Row } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {
  SearchField,
  usePaginatedQuery,
} from 'hindawi-shared/client/components'
import { SanctionListTable, SanctionListProfileModal } from '../components'
import { getSanctionListProfiles } from '../graphql/queries'

const SanctionListTab: React.FC = () => {
  const [searchValue, setSearchValue] = useState('')
  const [inputValue, setInputValue] = useState('')
  const itemsPerPage = 11

  const {
    error,
    loading,
    page,
    setPage,
    results,
    toFirst,
    toLast,
    toNext,
    toPrevious,
    total,
    refetch,
  } = usePaginatedQuery(getSanctionListProfiles, {
    variables: { searchValue, pageSize: itemsPerPage },
    settings: {},
  })
  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>): void =>
    setInputValue(event.target.value)

  const reset = (): void => {
    setSearchValue('')
    setInputValue('')
    refetch()
    toFirst()
  }

  return (
    <ScrollContainer>
      <TabRoot>
        <Row alignItems="flex-start" justify="flex-start">
          <SearchField
            mb={4}
            onChange={handleOnChange}
            onSearch={(item: any) => {
              toFirst()
              setSearchValue(item)
            }}
            placeholder="Search"
            value={inputValue}
            width={88}
          />
          <Item justify="flex-end">
            <SanctionListProfileModal reset={reset} />
          </Item>
        </Row>
        <SanctionListTable
          error={error}
          loading={loading}
          noItemsMessage="No results found."
          resetInputFields={reset}
          sanctionList={results}
        />
        {total > 0 && (
          <Item justify="flex-end" mt={4}>
            <Pagination
              itemsPerPage={itemsPerPage}
              nextPage={toNext}
              page={page}
              prevPage={toPrevious}
              setPage={setPage}
              toFirst={toFirst}
              toLast={toLast}
              totalCount={total}
            />
          </Item>
        )}
      </TabRoot>
    </ScrollContainer>
  )
}

export { SanctionListTab }

export const TabRoot = styled.div`
  background-color: ${th('white')};
  box-shadow: 0 ${th('gridUnit')} calc(${th('gridUnit')} * 2) 0
    rgba(51, 51, 51, 0.15);
  border-radius: ${th('borderRadius')};
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: calc(${th('gridUnit')} * 2);
  padding: calc(${th('gridUnit')} * 4);
`

export const ScrollContainer = styled.div`
  overflow: auto;
`
