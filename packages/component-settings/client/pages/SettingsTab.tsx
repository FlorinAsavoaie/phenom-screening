import { useState } from 'react'
import { Tabs } from 'hindawi-shared/client/components'
import { SuspiciousKeywordsTab, SanctionListTab } from '.'

const SettingsTab: React.FC = () => {
  const [selectedTab, setSelectedTab] = useState(0)
  const handleClick = (index: number): void => setSelectedTab(index)
  const tabs = [{ label: 'Sanction List' }, { label: 'Suspicious Keywords' }]

  return (
    <Tabs handleClick={handleClick} selectedTab={selectedTab} tabs={tabs}>
      <SanctionListTab />
      <SuspiciousKeywordsTab />
    </Tabs>
  )
}

export { SettingsTab }
