export interface SubmitFormProps {
  keyword: string
  note: string
}
export interface ModalContentProps {
  hideModal: () => void
  modalTitle: string
  modalType: string
  onSubmit: (values: any) => void
  keyword: {
    keyword: string
    note: string
  }
}
