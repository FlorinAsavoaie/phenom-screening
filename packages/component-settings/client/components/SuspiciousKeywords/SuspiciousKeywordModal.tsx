import { useCallback } from 'react'
import styled from 'styled-components'
import {
  Item,
  Row,
  Label,
  ValidatedFormField,
  validators,
  Textarea,
  useFetching,
} from '@hindawi/ui'
import { TextField } from '@pubsweet/ui'
import { Formik, Form as BaseForm } from 'formik'
import { th } from '@pubsweet/ui-toolkit'
import { parseGQLError } from 'hindawi-utils/errorParsers'

import { Header, Footer } from '../modal'
import { ModalContentProps, SubmitFormProps } from './interfaces'

const SuspiciousKeywordModal: React.FC<ModalContentProps> = ({
  hideModal,
  modalTitle,
  onSubmit,
  keyword,
  modalType,
}) => {
  const { isFetching, setFetching } = useFetching()
  const handleOnSubmit = useCallback(
    async (values: SubmitFormProps, { setFieldError }): Promise<void> => {
      setFetching(true)
      try {
        await onSubmit(values)
        hideModal()
      } catch (error) {
        const duplicateKeyword = parseGQLError(error)
        setFetching(false)
        setFieldError('keyword', duplicateKeyword)
      }
    },
    [onSubmit],
  )

  const initialValues = {
    keyword: keyword.keyword,
    note: keyword.note,
  }
  return (
    <Root>
      <Formik initialValues={initialValues} onSubmit={handleOnSubmit}>
        {({ handleSubmit }): any => (
          <Form>
            <Header hideModal={hideModal} modalTitle={modalTitle} />
            <ModalContent>
              <Row>
                <Item vertical>
                  <Label mb="2px" required>
                    Keyword
                  </Label>
                  <ValidatedFormField
                    // eslint-disable-next-line @typescript-eslint/no-use-before-define
                    component={StyledTextField}
                    data-test-id="keyword"
                    disabled={modalType === 'editKeyword'}
                    name="keyword"
                    placeholder="Fill in"
                    type="text"
                    validate={[validators.required]}
                  />
                </Item>
              </Row>
              <Row>
                <Item vertical>
                  <Label mb="2px" mt="2px" required>
                    Reason & Recommendations
                  </Label>
                  <ValidatedFormField
                    component={Textarea}
                    name="note"
                    placeholder="Fill in"
                    validate={[validators.required]}
                  />
                </Item>
              </Row>
            </ModalContent>
            <Footer
              confirmButton={modalType !== 'editKeyword' ? 'ADD' : 'SAVE'}
              handleSubmit={handleSubmit}
              hideModal={hideModal}
              isFetching={isFetching}
            />
          </Form>
        )}
      </Formik>
    </Root>
  )
}

export { SuspiciousKeywordModal }

const Root = styled.div`
  align-items: flex-start;
  background: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  border: none;
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: calc(${th('gridUnit')} * 6);
  width: 50%;
`

const Form = styled(BaseForm)`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 100%;
  width: 100%;
`

const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  flex: 2;
  overflow-y: scroll;
`
const StyledTextField = styled(TextField)`
  margin-bottom: 0;
`
