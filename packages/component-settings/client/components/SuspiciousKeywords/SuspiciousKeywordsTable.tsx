import { ReactElement } from 'react'
import { Text, Loader } from '@hindawi/ui'
import { ApolloError, useQuery } from '@apollo/client'
import { DateParser } from '@pubsweet/ui'
import { Table } from 'hindawi-shared/client/components'

import { get } from 'lodash'

import { EditDeleteDropdownMenu } from '../modal'
import { SuspiciousKeywordModal } from './SuspiciousKeywordModal'
import { useDeleteSuspiciousKeyword } from '../../graphql/hooks/useDeleteSuspiciousKeyword'
import { useEditSuspiciousKeyword } from '../../graphql/hooks/useEditSuspiciousKeyword'
import { getSuspiciousKeyword } from '../../graphql/queries'

interface SuspiciousKeywordItem {
  id: string
  keyword: string
  note: string
  created: string
}

interface SuspiciousKeywordItemProps {
  item: SuspiciousKeywordItem
}

interface EditDeleteDropdownProps {
  item: SuspiciousKeywordItem
  resetInputFields: () => any
}

const AddedOnDate: React.FC<{ created: string }> = ({ created }) => (
  <DateParser humanizeThreshold={0} timestamp={created}>
    {(timestamp: string): ReactElement => (
      <Text display="flex">{timestamp}</Text>
    )}
  </DateParser>
)

const NoteAndRecommendationColumn: React.FC<SuspiciousKeywordItemProps> = ({
  item,
}) => (
  <Text pr={2} whiteSpace="pre-wrap">
    {item.note}
  </Text>
)

const KeywordColumn: React.FC<SuspiciousKeywordItemProps> = ({ item }) => (
  <Text pr={2}>{item.keyword}</Text>
)

const EditKeyword: React.FC<any> = ({ itemId, ...props }) => {
  const { loading, data } = useQuery(getSuspiciousKeyword, {
    variables: {
      keywordId: itemId,
    },
    fetchPolicy: 'no-cache',
  })
  const suspiciousKeyword = get(data, 'getSuspiciousKeyword', '')

  if (loading) return <Loader mt={4} mx="auto" />

  return (
    <SuspiciousKeywordModal
      keyword={suspiciousKeyword}
      modalTitle="Edit keyword"
      modalType="editKeyword"
      {...props}
    />
  )
}

const EditDeleteKeywordDropdown: React.FC<EditDeleteDropdownProps> = ({
  item,
  resetInputFields,
}) => {
  const editSuspiciousKeyword = useEditSuspiciousKeyword()
  const deleteSuspiciousKeyword = useDeleteSuspiciousKeyword()
  return (
    <EditDeleteDropdownMenu
      deleteFn={deleteSuspiciousKeyword}
      deleteModalKey="deleteSuspiciousKeyword"
      deleteTitle="Are you sure you want to delete this keyword?"
      EditComponent={EditKeyword}
      editFn={editSuspiciousKeyword}
      editModalKey="editSuspiciousKeyword"
      item={item}
      resetInputs={resetInputFields}
    />
  )
}

const getColumnDefinitions = (resetInputFields: () => any): unknown => [
  {
    headerName: 'Keyword',
    component: KeywordColumn,
    flex: 3,
  },
  {
    headerName: 'Reason & Recommendations',
    component: NoteAndRecommendationColumn,
    flex: 12,
  },
  {
    headerName: 'Added On',
    component: ({ item }: SuspiciousKeywordItemProps): ReactElement => (
      <AddedOnDate created={item.created} />
    ),
    flex: 1,
  },
  {
    headerName: '',
    disableEllipsis: true,
    component: ({ item }: SuspiciousKeywordItemProps): unknown =>
      EditDeleteKeywordDropdown({ item, resetInputFields }),
    flex: 1,
  },
]

interface SuspiciousKeywordsTableProps {
  error?: ApolloError
  loading: boolean
  noItemsMessage: string
  suspiciousKeywords: SuspiciousKeywordItem[]
  resetInputFields: () => void
}
const SuspiciousKeywordsTable: React.FC<SuspiciousKeywordsTableProps> = ({
  error,
  loading,
  noItemsMessage,
  suspiciousKeywords,
  resetInputFields,
}) => (
  <Table
    columnDefinitions={getColumnDefinitions(resetInputFields)}
    error={error}
    items={suspiciousKeywords}
    loading={loading}
    noItemsMessage={noItemsMessage}
  />
)

export { SuspiciousKeywordsTable }
