import { Form, Text, Row } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import { RcFile } from 'antd/lib/upload'
import styled from 'styled-components'
import { UploadButton } from './UploadButton'
import { ErrorMessage } from './ErrorMessage'
import { ImportStatus } from './useImportReducer'
import { ImportProgress } from './ImportProgress'

type ImportFormProps = {
  beforeUpload: (file: RcFile) => void
  disabled: boolean
  error: string
  importStatus: ImportStatus | null
  isImporting: boolean
  isUploading: boolean
  onError: (errorMessage: string) => void
  onImportStatusUpdate: (importStatus: ImportStatus) => void
  onRemove: () => void
}

export const ImportForm = ({
  beforeUpload,
  disabled,
  error,
  importStatus,
  isImporting,
  isUploading,
  onError,
  onImportStatusUpdate,
  onRemove,
}: ImportFormProps): JSX.Element => (
  <Form>
    <FormItem
      label="Select file with suspicious keywords (.csv files only) "
      name="keywordsFile"
      rules={[{ required: true }]}
    />

    <Form.Item style={{ marginBottom: 16 }}>
      <UploadButton
        beforeUpload={beforeUpload}
        disabled={disabled}
        onRemove={onRemove}
      />
    </Form.Item>
    {error && <ErrorMessage error={error} />}
    {(error || !importStatus) && (
      <Row>
        <Text preset={Preset.PRIMARY}>
          The .csv file must contain only these two columns:
          <ul style={{ listStyleType: 'disc', marginBottom: 16 }}>
            <li>“Keyword”</li>
            <li>“Reason & Recommendations”.</li>
          </ul>
        </Text>
        <Text preset={Preset.PRIMARY}>
          Please make sure the file is formatted correctly before uploading.
        </Text>
      </Row>
    )}
    {importStatus && (
      <ImportProgress
        importStatus={importStatus}
        isImporting={isImporting}
        isUploading={isUploading}
        onError={onError}
        onImportStatusUpdate={onImportStatusUpdate}
      />
    )}
  </Form>
)

const FormItem = styled(Form.Item)`
  height: 24px;
  margin-bottom: 0;
  .ant-form-item-label > label {
    height: 16px;
    font-size: 14px;

    :not(.ant-form-item-required-mark-optional) {
      ::before {
        display: none;
      }
    }

    ::after {
      display: inline;
      margin-right: 4px;
      color: #ff4d4f;
      font-size: 14px;
      font-family: SimSun, sans-serif;
      line-height: 1;
      content: '*';
    }
  }
`
