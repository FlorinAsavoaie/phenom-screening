import { Button } from '@hindawi/phenom-ui'

type FooterProps = {
  error: string
  file: File | null
  finished: boolean
  isImporting: boolean
  handleUpload: () => Promise<void>
  onCancel: () => void
}

export const ModalFooter = ({
  finished,
  error,
  onCancel,
  isImporting,
  handleUpload,
  file,
}: FooterProps): JSX.Element => (
  <>
    {finished && !error ? (
      <Button
        key="submit"
        onClick={onCancel}
        size="large"
        type="primary"
        style={{ padding: '16px 24px' }}
      >
        Done
      </Button>
    ) : (
      <>
        <Button
          disabled={isImporting}
          key="back"
          onClick={onCancel}
          size="large"
          type="ghost"
        >
          Cancel
        </Button>
        <Button
          disabled={!file || isImporting || !!error}
          key="submit"
          onClick={handleUpload}
          size="large"
          type="primary"
        >
          {isImporting ? 'Importing...' : 'Import'}
        </Button>
      </>
    )}
  </>
)
