import { Upload } from 'antd'
import { RcFile } from 'antd/lib/upload'
import { Button, Row } from '@hindawi/phenom-ui'
import { useState } from 'react'
import styled from 'styled-components'
import { FilePreviewer } from './FilePreviewer'

type UploadButtonProps = {
  beforeUpload: (file: RcFile) => void
  disabled: boolean
  onRemove: () => void
}

export const UploadButton = ({
  beforeUpload,
  disabled,
  onRemove,
}: UploadButtonProps): JSX.Element => {
  const [file, setFile] = useState<RcFile | null>(null)

  return (
    <Row wrap={false}>
      <FilePreviewer file={file} />
      <UploadWrapper
        accept=".csv"
        beforeUpload={(file) => {
          setFile(file)
          beforeUpload(file)
          return false
        }}
        disabled={disabled}
        itemRender={() => null}
        maxCount={1}
        onRemove={() => {
          onRemove()
        }}
      >
        <Button
          disabled={disabled}
          style={{
            borderTopLeftRadius: 'unset',
            borderBottomLeftRadius: 'unset',
          }}
          type="secondary"
        >
          Browse
        </Button>
      </UploadWrapper>
    </Row>
  )
}
const UploadWrapper = styled(Upload)`
  .ant-upload.ant-upload-select {
    border: 0px none;
    padding: 0px;
    :hover {
      border: none;
      border-radius: 0;
    }
  }
`
