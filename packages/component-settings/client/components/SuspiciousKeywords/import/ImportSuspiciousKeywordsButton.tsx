import { useState } from 'react'
import { Button } from '@hindawi/phenom-ui'
import { ImportKeywordsModal } from './ImportKeywordsModal'
import {
  useImportSuspiciousKeywords,
  useUploadFile,
} from '../../../graphql/hooks'

export const ImportSuspiciousKeywordsButton = (): JSX.Element => {
  const [isModalVisible, openModal, closeModal] = useModalToggler()
  const uploadFile = useUploadFile()
  const importFile = useImportSuspiciousKeywords()

  return (
    <>
      <Button onClick={openModal} style={{ marginRight: '8px' }} type="ghost">
        Import Keywords
      </Button>
      <ImportKeywordsModal
        isModalVisible={isModalVisible}
        onClose={closeModal}
        onImportFile={importFile}
        onUploadFile={uploadFile}
      />
    </>
  )
}

function useModalToggler(): [boolean, () => void, () => void] {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const openModal = () => setIsModalVisible(true)
  const closeModal = () => setIsModalVisible(false)

  return [isModalVisible, openModal, closeModal]
}
