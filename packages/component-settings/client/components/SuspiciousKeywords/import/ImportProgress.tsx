import { Progress } from 'antd'
import { Text, Row } from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import { ImportStatus } from './useImportReducer'
import { useInterval } from './useInterval'
import { useImportStatus } from '../../../graphql/hooks'

type ImportProgressProps = {
  importStatus: ImportStatus
  isImporting: boolean
  isUploading: boolean
  onImportStatusUpdate: (importStatus: ImportStatus) => void
  onError: (errorMessage: string) => void
}

export const ImportProgress = ({
  importStatus,
  isImporting,
  isUploading,
  onImportStatusUpdate,
  onError,
}: ImportProgressProps): JSX.Element => {
  const getImportStatus = useImportStatus()

  useInterval(async () => {
    try {
      if (isImporting && !isUploading) {
        const importStatus = await getImportStatus()
        onImportStatusUpdate(importStatus)
      }
    } catch (error) {
      onError(error.message)
    }
  }, 3500)

  return (
    <>
      <Row>
        <Progress
          percent={importStatus.percentage}
          strokeColor={{
            '0%': '#007e92',
            '100%': '#63a945',
          }}
        />
      </Row>

      <Row>
        <Text
          preset={Preset.PRIMARY}
          style={{
            textAlign: 'center',
            width: '100%',
          }}
        >
          {importStatus.message}
        </Text>
      </Row>
    </>
  )
}
