import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import { Text, Row, IconWarning } from '@hindawi/phenom-ui'

type ErrorMessageProps = {
  error: string
}

export const ErrorMessage = ({ error }: ErrorMessageProps): JSX.Element => (
  <Row
    style={{
      marginTop: 16,
      marginBottom: 13,
      alignItems: 'center',
    }}
  >
    <Text preset={Preset.MESSAGE} type="danger">
      <IconWarning
        style={{ marginRight: '4px', color: '#FF5547', fontSize: '16px' }}
      />
      {error}
    </Text>
  </Row>
)
