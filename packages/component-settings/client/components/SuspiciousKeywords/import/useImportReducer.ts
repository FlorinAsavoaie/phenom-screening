import { RcFile } from 'antd/lib/upload'
import { Dispatch, useReducer } from 'react'

export type ImportStatus = {
  percentage: number
  message: string
}

type State = {
  data: {
    file: RcFile | null
    isImporting: boolean
    isUploading: boolean
    finished: boolean
    importStatus: ImportStatus | null
  }
  error: string
}

type Action =
  | {
      type: 'ADD_FILE'
      file: RcFile
    }
  | { type: 'REMOVE_FILE' }
  | { type: 'UPLOAD_FILE' }
  | { type: 'IMPORT_FILE' }
  | { type: 'UPDATE_PROGRESS'; importStatus: ImportStatus }
  | { type: 'ERROR'; error: string }
  | { type: 'DONE' }

const INITIAL_STATE: State = {
  data: {
    file: null,
    isImporting: false,
    isUploading: false,
    finished: false,
    importStatus: null,
  },
  error: '',
}

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'ADD_FILE':
      return {
        data: {
          ...state.data,
          file: action.file,
          finished: false,
        },
        error: '',
      }
    case 'REMOVE_FILE':
      return {
        data: {
          ...state.data,
          file: null,
          finished: false,
        },
        error: '',
      }
    case 'UPLOAD_FILE':
      return {
        data: {
          ...state.data,
          isImporting: true,
          isUploading: true,
          importStatus: {
            percentage: 0,
            message: 'Uploading file...',
          },
        },
        error: state.error,
      }
    case 'IMPORT_FILE':
      return {
        data: {
          ...state.data,
          isUploading: false,
        },
        error: state.error,
      }
    case 'UPDATE_PROGRESS': {
      const isComplete =
        action.importStatus && action.importStatus.percentage === 100

      return {
        data: {
          ...state.data,
          importStatus: action.importStatus,
          isImporting: !isComplete,
          finished: isComplete,
        },
        error: state.error,
      }
    }
    case 'ERROR':
      return {
        data: {
          ...state.data,
          isImporting: false,
          isUploading: false,
          finished: true,
          importStatus: null,
        },
        error: action.error,
      }
    case 'DONE':
      return INITIAL_STATE
    default:
      throw new Error('Invalid action type')
  }
}

export function useImportReducer(): [State, Dispatch<Action>] {
  return useReducer(reducer, INITIAL_STATE)
}
