import styled from 'styled-components'

type FilePreviewerProps = {
  file: File | null
}

export const FilePreviewer = ({ file }: FilePreviewerProps): JSX.Element => (
  <Root>
    {file ? (
      <FileName>{file.name}</FileName>
    ) : (
      <Placeholder>Please select a CSV file</Placeholder>
    )}
  </Root>
)

const Root = styled.div`
  background: #ffffff;
  border: 1px solid #bdbdbd;
  box-sizing: border-box;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;

  font-family: Nunito;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
  color: #4f4f4f;

  height: 32px;
  width: 410px;
`

const FileName = styled.p`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 6px;
`
const Placeholder = styled.p`
  font-style: italic;
  padding: 6px;
`
