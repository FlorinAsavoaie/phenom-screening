import {
  IconNotificationSuccess as BaseIconSuccess,
  Text,
  Row,
} from '@hindawi/phenom-ui'
import styled, { CSSProperties } from 'styled-components'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'

type SuccessNotificationProps = {
  message: string
  // eslint-disable-next-line react/require-default-props
  style?: CSSProperties
}

export const SuccessNotification = ({
  message,
  style,
}: SuccessNotificationProps): JSX.Element => (
  <Row
    style={{
      justifyContent: 'center',
      flexDirection: 'column',
      alignItems: 'center',
      ...style,
    }}
  >
    <IconNotificationSuccess />
    <Text preset={Preset.MESSAGE}>{message}</Text>
  </Row>
)

const IconNotificationSuccess = styled(BaseIconSuccess)`
  font-size: 200px;
  margin-bottom: 16px;
  margin-top: 8px;
  svg > path {
    fill: #74a73a;
  }
`
