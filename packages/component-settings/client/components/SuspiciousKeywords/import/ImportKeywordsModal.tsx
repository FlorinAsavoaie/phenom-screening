import { Modal } from '@hindawi/phenom-ui'
import { useDebounce } from 'hindawi-shared/client/graphql/hooks'
import styled from 'styled-components'
import { useImportReducer } from './useImportReducer'
import { ImportForm } from './ImportForm'
import { SuccessNotification } from './SuccessNotification'
import { ModalFooter } from './ModalFooter'

type ImportKeywordsModalProps = {
  isModalVisible: boolean
  onClose: () => void
  onImportFile: (fileId: string) => Promise<void>
  onUploadFile: (file: File) => Promise<string>
}

export const ImportKeywordsModal = ({
  isModalVisible,
  onClose,
  onImportFile,
  onUploadFile,
}: ImportKeywordsModalProps): JSX.Element => {
  const [
    {
      data: { file, isImporting, isUploading, importStatus, finished },
      error,
    },
    dispatch,
  ] = useImportReducer()

  const showSuccessNotification = useDebounce({
    value: finished && !error,
    delay: 1000,
  })

  const onCancel = () => {
    onClose()
    dispatch({ type: 'DONE' })
  }
  const handleUpload = async () => {
    if (file) {
      try {
        dispatch({ type: 'UPLOAD_FILE' })
        const fileId = await onUploadFile(file)

        dispatch({ type: 'IMPORT_FILE' })
        await onImportFile(fileId)
      } catch (error) {
        dispatch({ type: 'ERROR', error: error.message })
      }
    }
  }
  return (
    <Modal
      bodyStyle={{
        height: '281px',
      }}
      centered
      closable={false}
      destroyOnClose
      footer={
        <ModalFooter
          error={error}
          file={file}
          finished={finished}
          handleUpload={handleUpload}
          isImporting={isImporting}
          onCancel={onCancel}
        />
      }
      keyboard={false}
      maskClosable={false}
      onCancel={onCancel}
      title={<ModalTitle>Import Suspicious Keywords</ModalTitle>}
      visible={isModalVisible}
      width={565}
    >
      {showSuccessNotification && importStatus ? (
        <SuccessNotification message={importStatus.message} />
      ) : (
        <ImportForm
          beforeUpload={(file) => {
            dispatch({ type: 'ADD_FILE', file })
          }}
          disabled={isImporting || (finished && !error)}
          error={error}
          importStatus={importStatus}
          isImporting={isImporting}
          isUploading={isUploading}
          onError={(errorMessage) => {
            dispatch({ type: 'ERROR', error: errorMessage })
          }}
          onImportStatusUpdate={(importStatus) => {
            dispatch({ type: 'UPDATE_PROGRESS', importStatus })
          }}
          onRemove={() => {
            dispatch({ type: 'REMOVE_FILE' })
          }}
        />
      )}
    </Modal>
  )
}
const ModalTitle = styled.div`
  line-height: 20px;
  font-size: 18px;
`
