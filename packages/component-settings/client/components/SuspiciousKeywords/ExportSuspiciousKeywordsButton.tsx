import { useState, useEffect } from 'react'
import { useMutation } from '@apollo/client'
import styled from 'styled-components'
import {
  Modal,
  Button,
  Text,
  Spinner,
  Row,
  IconCheck,
  IconWarning,
} from '@hindawi/phenom-ui'
import { Preset } from '@hindawi/phenom-ui/dist/Typography/Text'
import { get } from 'lodash'

import { useDebounce } from 'hindawi-shared/client/graphql/hooks'
import { removeAnchorElement } from 'component-quality-checks-process/client/components/manuscript-files/fileUtils'
import { exportSuspiciousKeywords } from '../../graphql/mutations'
import { SuccessNotification } from './import/SuccessNotification'

const ExportSuspiciousKeywordsButton: React.FC = () => {
  const [displayModalData, setDisplayModalData] = useState(false)
  const [err, setErr] = useState('')
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isCanceled, setIsCanceled] = useState(false)
  const showModal = () => setIsModalVisible(true)

  const [exportSuspiciousKeywordsMutation, { data, error, loading }] =
    useMutation(exportSuspiciousKeywords)

  const exportedKeywordsUrl = get(data, 'exportSuspiciousKeywords.url', '')
  const numberOfExportedKeywords = get(
    data,
    'exportSuspiciousKeywords.totalNumber',
    0,
  )

  const debouncedShowingSuccessIcon = useDebounce({
    value: displayModalData,
    delay: 500,
  })

  const handleOk = () => {
    setIsCanceled(false)
    if (exportedKeywordsUrl && displayModalData) {
      setDisplayModalData(false)
      return setIsModalVisible(false)
    }
    return exportSuspiciousKeywordsMutation()
  }

  const onCancel = () => {
    setIsCanceled(true)
    setIsModalVisible(false)
    setDisplayModalData(false)
    setErr('')
  }

  useEffect(() => {
    if (exportedKeywordsUrl && !isCanceled) {
      const a = document.createElement('a')
      a.href = exportedKeywordsUrl
      document.body.appendChild(a)
      a.click()
      removeAnchorElement(a, exportedKeywordsUrl)
      setDisplayModalData(true)
    }
  }, [exportedKeywordsUrl, isCanceled])

  useEffect(() => {
    if (error) {
      setErr(error.message)
    }
  }, [error])

  const getFooterButtons = () => {
    if (displayModalData)
      return (
        <Button key="submit" onClick={handleOk} size="large" type="primary">
          Done
        </Button>
      )
    if (err) {
      return (
        <Button key="back" onClick={onCancel} size="large" type="primary">
          Close
        </Button>
      )
    }
    return (
      <>
        <Button key="back" onClick={onCancel} size="large" type="ghost">
          Cancel
        </Button>
        <Button
          disabled={loading && !isCanceled}
          key="submit"
          onClick={handleOk}
          size="large"
          type="primary"
        >
          {loading && !isCanceled ? 'EXPORTING...' : 'EXPORT NOW'}
        </Button>
      </>
    )
  }

  return (
    <>
      <Button onClick={showModal} style={{ marginRight: '8px' }} type="ghost">
        Export Keywords
      </Button>
      <Modal
        bodyStyle={{ height: '351px', paddingTop: '4px' }}
        centered
        closable={false}
        destroyOnClose
        footer={getFooterButtons()}
        keyboard={false}
        maskClosable={false}
        onCancel={onCancel}
        title="Export Suspicious Keywords"
        visible={isModalVisible}
        width={565}
      >
        <Text preset={Preset.MESSAGE}>
          Export all suspicious keywords to a .csv file for importing in other
          systems.
        </Text>

        {loading && !isCanceled && (
          <ExportSuspiciousKeywordsContainer>
            <Spinner />
            <Text preset={Preset.MESSAGE}> Preparing file. Please wait…</Text>
          </ExportSuspiciousKeywordsContainer>
        )}

        {err && (
          <Row style={{ marginTop: 8, alignItems: 'center' }}>
            <IconWarning
              style={{ marginRight: '4px', color: '#FF5547', fontSize: '16px' }}
            />
            <Text preset={Preset.MESSAGE} type="danger">
              There was a problem exporting this file. Please close and try
              again.
            </Text>
          </Row>
        )}

        {displayModalData && (
          <Row style={{ marginTop: 8, alignItems: 'center' }}>
            <IconCheck
              style={{ marginRight: '4px', color: '#74A73A', fontSize: '16px' }}
            />
            <Text preset={Preset.MESSAGE} type="success">
              {' '}
              File ready. Download will start automatically...
            </Text>
          </Row>
        )}

        {debouncedShowingSuccessIcon && (
          <SuccessNotification
            message={`You have successfully exported ${numberOfExportedKeywords} keywords.`}
            style={{
              marginTop: '26px',
            }}
          />
        )}
      </Modal>
    </>
  )
}

export { ExportSuspiciousKeywordsButton }

const ExportSuspiciousKeywordsContainer = styled.div`
  display: flex;
  margin-top: 8px;
  align-items: center;

  .hdw-spinner {
    svg {
      height: 16px;
      width: 16px;
    }

    font-size: 16px;
    margin-right: 4px;
  }
`
