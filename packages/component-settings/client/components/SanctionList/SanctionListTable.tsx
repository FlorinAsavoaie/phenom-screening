import { ReactElement } from 'react'
import { Text, Loader } from '@hindawi/ui'
import { ApolloError, useQuery } from '@apollo/client'
import { DateParser } from '@pubsweet/ui'

import { get } from 'lodash'

import styled from 'styled-components'
import { Table } from 'hindawi-shared/client/components'
import { EditDeleteDropdownMenu } from '../modal/EditDeleteDropdownMenu'
import { useEditSanctionListProfile } from '../../graphql/hooks/useEditSanctionListProfile'
import { useDeleteSanctionListProfile } from '../../graphql/hooks/useDeleteSanctionListProfile'
import { getSanctionListProfile } from '../../graphql/queries'
import { SanctionListProfileModalContent } from './SanctionListModal/SanctionListProfileModalContent'

interface Reason {
  name: string
}
interface SanctionListProfile {
  id: string
  surname: string
  givenNames: string
  email: string
  types: string[]
  toDate: string
  fromDate: string
  created: string
  reasons: Reason[]
  comments: string
}

interface SanctionListProfileProps {
  item: SanctionListProfile
}
interface EditDeleteDropdownProps {
  item: string
  resetInputFields: () => any
}
interface EditProfileProps {
  itemId: string
  hideModal: () => void
  justifyContent: string
  modalKey: string
  onSubmit: () => Promise<void>
}
enum ModalType {
  EDIT_PROFILE = 'editProfile',
}
function TypeColumn({ item }: SanctionListProfileProps): ReactElement {
  const types = item.types.reduce(
    (acc, el, index) => `${acc}${index !== 0 ? ', ' : ' '}${el}`,
    '',
  )
  return <Text pr={2}>{types}</Text>
}
function StartDateColumn({ item }: SanctionListProfileProps): ReactElement {
  return (
    <DateParser
      dateFormat="YYYY-MM-DD"
      humanizeThreshold={0}
      timestamp={item.fromDate ? item.fromDate : item.created}
    >
      {(timestamp: string): ReactElement => (
        <Text display="flex">{timestamp}</Text>
      )}
    </DateParser>
  )
}

function EndDateColumn({ item }: SanctionListProfileProps): ReactElement {
  return item.toDate ? (
    <DateParser
      dateFormat="YYYY-MM-DD"
      humanizeThreshold={Number.MIN_SAFE_INTEGER}
      timestamp={item.toDate}
    >
      {(timestamp: string): ReactElement => (
        <Text display="flex">{timestamp}</Text>
      )}
    </DateParser>
  ) : (
    <Text>No end date</Text>
  )
}
function ReasonColumn({ item }: SanctionListProfileProps): ReactElement {
  const reasonNames = item.reasons.reduce(
    (acc, el, index) => `${acc}${index !== 0 ? ', ' : ' '}${el.name}`,
    '',
  )
  return <Text pr={2}>{reasonNames}</Text>
}
function CommentsColumn({ item }: SanctionListProfileProps): ReactElement {
  return (
    <CommentsColumnStyled pr={2} whiteSpace="pre-wrap">
      {item.comments || 'Not available.'}
    </CommentsColumnStyled>
  )
}

const EditProfile: React.FC<EditProfileProps> = ({ itemId, ...props }) => {
  const { loading, data } = useQuery(getSanctionListProfile, {
    variables: {
      profileId: itemId,
    },
    fetchPolicy: 'no-cache',
  })
  if (loading) return <Loader mt={4} mx="auto" />

  const sanctionListProfile = get(data, 'getSanctionListProfile', '')

  return (
    <SanctionListProfileModalContent
      modalTitle="Edit sanctioned author"
      modalType={ModalType.EDIT_PROFILE}
      profile={sanctionListProfile}
      {...props}
    />
  )
}

const EditDeleteProfileDropdown: React.FC<EditDeleteDropdownProps> = ({
  item,
  resetInputFields,
}) => {
  const editSanctionListProfile = useEditSanctionListProfile()
  const deleteSanctionListProfile = useDeleteSanctionListProfile()
  return (
    <EditDeleteDropdownMenu
      deleteFn={deleteSanctionListProfile}
      deleteModalKey="deleteSuspiciousKeyword"
      deleteTitle="Are you sure you want to delete this author profile?"
      EditComponent={EditProfile}
      editFn={editSanctionListProfile}
      editModalKey="editSuspiciousKeyword"
      item={item}
      resetInputs={resetInputFields}
    />
  )
}

const columnDefinitions = (resetInputFields: () => any): unknown => [
  {
    headerName: 'First Name',
    labelFunction: (item: SanctionListProfile): string => item.givenNames,
    flex: 2,
  },
  {
    headerName: 'Last Name',
    labelFunction: (item: SanctionListProfile): string => item.surname,
    flex: 3,
  },
  {
    headerName: 'Email',
    labelFunction: (item: SanctionListProfile): string => item.email,
    flex: 4,
  },
  {
    headerName: 'Type',
    component: TypeColumn,
    flex: 2,
  },
  {
    headerName: 'Start Date',
    component: StartDateColumn,
    flex: 3,
  },
  {
    headerName: 'End Date',
    component: EndDateColumn,
    flex: 3,
  },
  {
    headerName: 'Reason',
    component: ReasonColumn,
    flex: 3,
  },
  {
    headerName: 'Comments',
    component: CommentsColumn,
    flex: 5,
  },
  {
    headerName: '',
    disableEllipsis: true,
    component: ({ item }: any): unknown =>
      EditDeleteProfileDropdown({ item, resetInputFields }),
    flex: 1,
  },
]

interface SanctionListTableProps {
  error?: ApolloError
  loading: boolean
  noItemsMessage: string
  sanctionList: SanctionListProfile[]
  resetInputFields: () => void
}
const SanctionListTable: React.FC<SanctionListTableProps> = ({
  error,
  loading,
  noItemsMessage,
  sanctionList,
  resetInputFields,
}) => (
  <Table
    columnDefinitions={columnDefinitions(resetInputFields)}
    error={error}
    items={sanctionList}
    loading={loading}
    noItemsMessage={noItemsMessage}
  />
)

export { SanctionListTable }

const CommentsColumnStyled = styled(Text)`
  height: fit-content;
  line-height: 20px;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -moz-box-orient: vertical;
  -ms-box-orient: vertical;
  box-orient: vertical;
  -webkit-line-clamp: 5;
  -moz-line-clamp: 5;
  -ms-line-clamp: 5;
  line-clamp: 5;
  overflow: hidden;
`
