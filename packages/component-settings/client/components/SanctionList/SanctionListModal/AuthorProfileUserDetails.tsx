import { Item, Row, Label, ValidatedFormField, validators } from '@hindawi/ui'
import styled from 'styled-components'

import { TextField as BaseTextField } from '@pubsweet/ui'

const StyledTextField = styled(BaseTextField)`
  margin-bottom: 0;
`
interface AuthorProfileInterface {
  modalType: string
}

enum ModalType {
  EDIT_PROFILE = 'editProfile',
}

const AuthorProfileUserDetails: React.FC<AuthorProfileInterface> = ({
  modalType,
}) => (
  <Row mt={1}>
    <Item mr={6} vertical>
      <Label mb={1} required>
        First Name
      </Label>
      <ValidatedFormField
        component={StyledTextField}
        data-test-id="givenNames"
        disabled={modalType === ModalType.EDIT_PROFILE}
        name="givenNames"
        placeholder="Fill in"
        type="text"
        validate={[validators.required]}
      />
    </Item>
    <Item mr={6} vertical>
      <Label mb={1} required>
        Last Name
      </Label>
      <ValidatedFormField
        component={StyledTextField}
        data-test-id="surname"
        disabled={modalType === ModalType.EDIT_PROFILE}
        name="surname"
        placeholder="Fill in"
        type="text"
        validate={[validators.required]}
      />
    </Item>
    <Item vertical>
      <Label mb={1} required>
        Email
      </Label>
      <ValidatedFormField
        component={StyledTextField}
        data-test-id="email"
        disabled={modalType === ModalType.EDIT_PROFILE}
        name="email"
        placeholder="Fill in"
        type="text"
        validate={[validators.required, validators.emailValidator]}
      />
    </Item>
  </Row>
)

export { AuthorProfileUserDetails }
