export interface FormErrors {
  email?: string
  givenNames?: string
  surname?: string
  reasonsIsRequired?: string
  typeIsRequired?: string
}

export const INITIAL_VALUES = {
  givenNames: '',
  surname: '',
  email: '',
  reasons: [],
  comments: '',
  toDate: null,
  isOnBadDebtList: false,
  isOnBlackList: false,
  isOnWatchList: false,
}

export interface FormValues {
  givenNames: string
  surname: string
  email: string
  reasons: string[]
  comments: string
  toDate: string | null
  isOnBadDebtList: boolean
  isOnBlackList: boolean
  isOnWatchList: boolean
}

function validateSendToPeerReviewForm({
  reasons,
  isOnBadDebtList,
  isOnBlackList,
  isOnWatchList,
}: FormValues): FormErrors {
  const errors: FormErrors = {}
  if (reasons.length === 0) {
    errors.reasonsIsRequired = 'Please select at least one reason'
  }

  if (!isOnBadDebtList && !isOnBlackList && !isOnWatchList) {
    errors.typeIsRequired = 'Please select at least one type'
  }

  return errors
}

export { validateSendToPeerReviewForm }
