import { ReactElement } from 'react'
import { Modal, Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon } from 'hindawi-shared/client/components'
import { SanctionListProfileModalContent } from './SanctionListProfileModalContent'
import { useAddSanctionListProfile } from '../../../graphql/hooks'

interface SanctionListProfileModalProps {
  reset: () => void
}

const SanctionListProfileModal: React.FC<SanctionListProfileModalProps> = ({
  reset,
}) => {
  const addSanctionListProfile = useAddSanctionListProfile()
  return (
    <Modal
      component={({ hideModal, onSubmit }: any): ReactElement => (
        <SanctionListProfileModalContent
          hideModal={hideModal}
          modalTitle="Add author to the sanction list"
          modalType="addAuthor"
          onSubmit={onSubmit}
        />
      )}
      justifyContent="flex-end"
      modalKey="addSuspiciousKeyword"
      onSubmit={async (values): Promise<void> => {
        await addSanctionListProfile(values)
        reset()
      }}
    >
      {(showModal: () => any): ReactElement => (
        <CreateAuthorProfileButton mb={2} onClick={showModal} primary>
          <Icon color="white" mr="2" name="plus" size={3} />
          Add Author
        </CreateAuthorProfileButton>
      )}
    </Modal>
  )
}

export { SanctionListProfileModal }

const CreateAuthorProfileButton = styled(Button)`
  align-items: center;
  border: none;
  color: ${th('white')};
  font-size: ${th('fontSizeBase')};
  font-stretch: normal;
  font-style: normal;
  font-weight: bold;
  height: 32px;
  letter-spacing: normal;
  line-height: normal;
  text-align: center;
  width: calc(${th('gridUnit')} * 33);

  :hover,
  :focus {
    background-color: ${th('actionPrimaryColor')};
  }
`
