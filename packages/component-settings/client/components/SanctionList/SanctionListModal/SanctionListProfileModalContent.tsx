import { ReactElement, useCallback } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Formik, Form as BaseForm } from 'formik'

import { useFetching } from '@hindawi/ui'
import { parseGQLError } from 'hindawi-utils/errorParsers'
import { Header, Footer } from '../../modal'
import { AuthorProfileUserDetails } from './AuthorProfileUserDetails'
import { SanctionDetails } from './SanctionDetails/SanctionDetails'
import {
  validateSendToPeerReviewForm,
  INITIAL_VALUES,
  FormValues,
} from './validateAuthorSanctionList'

interface SanctionListProfileModalContentProps {
  hideModal: () => void
  modalTitle: string
  onSubmit: (values: FormValues) => any
  modalType: string
  profile?: any
}
enum ModalType {
  EDIT_PROFILE = 'editProfile',
}

const SanctionListProfileModalContent: React.FC<SanctionListProfileModalContentProps> =
  ({ hideModal, modalTitle, onSubmit, modalType, profile }) => {
    const { isFetching, setFetching } = useFetching()
    const initialValues = profile || INITIAL_VALUES
    const handleOnSubmit = useCallback(
      (values, { setErrors, setFieldError }) => {
        setFetching(true)
        return onSubmit(values)
          .then(() => {
            setFetching(false)
            hideModal()
          })
          .catch((error: any) => {
            const fetchingError = parseGQLError(error)
            if (fetchingError.includes('Email already exists.')) {
              setFieldError('email', fetchingError)
            } else setErrors({ fetchingError: parseGQLError(error) })
            setFetching(false)
          })
      },
      [onSubmit],
    )
    return (
      <Root>
        <Formik
          initialValues={initialValues}
          onSubmit={handleOnSubmit}
          validate={validateSendToPeerReviewForm}
        >
          {({
            handleSubmit,
            errors,
            values,
            submitCount,
            setValues,
          }): ReactElement => (
            <Form>
              <Header hideModal={hideModal} modalTitle={modalTitle} />
              <ModalContent>
                <AuthorProfileUserDetails modalType={modalType} />
                <SanctionDetails
                  errors={errors}
                  profileReasons={profile ? profile.reasons : []}
                  setValues={setValues}
                  submitCount={submitCount}
                  values={values}
                />
              </ModalContent>
              <Footer
                confirmButton={
                  modalType !== ModalType.EDIT_PROFILE ? 'ADD AUTHOR' : 'SAVE'
                }
                errors={errors}
                handleSubmit={handleSubmit}
                hideModal={hideModal}
                isFetching={isFetching}
              />
            </Form>
          )}
        </Formik>
      </Root>
    )
  }

export { SanctionListProfileModalContent }

const Root = styled.div`
  align-items: flex-start;
  background: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  border: none;
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: calc(${th('gridUnit')} * 6);
  width: 50%;
`
const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  flex: 2;
  overflow-y: scroll;
`

const Form = styled(BaseForm)`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 100%;
  width: 100%;
`
