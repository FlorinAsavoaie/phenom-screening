import { useState } from 'react'
import { DatePicker, Label, Item, Row } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'
import { Checkbox } from 'hindawi-shared/client/components'
import { FormValues } from '../validateAuthorSanctionList'

interface EndDateSelectorProps {
  values: FormValues
  setValues: (values: any) => any
}
const EndDateSelector: React.FC<EndDateSelectorProps> = ({
  values,
  setValues,
}) => {
  const todayDate = new Date(Date.now()).toISOString()
  const hasEndDate = values.toDate !== null

  const [hasNoEndDate, setHasNoEndDate] = useState(!hasEndDate)

  const handleSetEndDate = (dateInput: string): void => {
    setValues({ ...values, toDate: dateInput })
  }
  const handleSetCalendarInactive = (): void => {
    const changedHasNoEndDate = !hasNoEndDate
    setHasNoEndDate(changedHasNoEndDate)
    if (changedHasNoEndDate) {
      setValues({ ...values, toDate: null })
    } else {
      setValues({ ...values, toDate: todayDate })
    }
  }

  return (
    <Root>
      <Label required>End Date</Label>
      <Row>
        <CalendarWrapper>
          <DatePicker
            disabled={hasNoEndDate}
            onChange={handleSetEndDate}
            value={values.toDate}
          />
        </CalendarWrapper>
        <Item alignItems="flex-end" ml={4}>
          <Checkbox
            checked={hasNoEndDate}
            label="No end date"
            onChange={handleSetCalendarInactive}
            value="toDate"
          />
        </Item>
      </Row>
    </Root>
  )
}

export { EndDateSelector }

const Root = styled.div``

const CalendarWrapper = styled.div`
  background-color: ${th('white')};
  .react-calendar {
    margin-top: 2px;
  }
  :nth-child(1) {
    height: calc(${th('gridUnit')} * 8);
    width: calc(${th('gridUnit')} * 35);
  }
`
