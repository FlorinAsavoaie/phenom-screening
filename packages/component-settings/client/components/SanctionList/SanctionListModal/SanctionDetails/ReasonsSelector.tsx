import { Item, Loader } from '@hindawi/ui'
import { useQuery } from '@apollo/client'
import { get } from 'lodash'
import {
  Option,
  ErrorField,
  MultipleSelectDropdown,
} from 'hindawi-shared/client/components'
import { FormErrors, FormValues } from '../validateAuthorSanctionList'
import { getReasons } from '../../../../graphql/queries'

interface Reason {
  id: string
  name: string
}
interface ReasonsSelectorProps {
  errors: FormErrors
  submitCount: number
  values: FormValues
  setValues: (values: any) => any
  profileReasons: [Reason]
}

const ReasonsSelector: React.FC<ReasonsSelectorProps> = ({
  errors,
  submitCount,
  values,
  setValues,
  profileReasons,
}) => {
  const { data, loading } = useQuery(getReasons, { fetchPolicy: 'cache-first' })

  if (loading) return <Loader mt={4} mx="auto" />

  const result = get(data, 'getReasons', [])

  const reasonOptions = result.map((reason: Reason) => {
    const newReason = profileReasons.filter((item) => item.id === reason.id)
    return {
      id: reason.id,
      label: reason.name,
      checked: newReason.length > 0,
    }
  })

  const handleSetReason = (reasons: Option[]): void => {
    const changedReason = reasons.map((reason: Option) => reason.id)
    setValues({
      ...values,
      reasons: changedReason,
    })
  }

  return (
    <Item alignItems="flex-start" mt={1} vertical>
      <MultipleSelectDropdown
        label="Reasons"
        onChange={handleSetReason}
        options={reasonOptions}
        placeholder="Select"
        required
      />
      <ErrorField
        errorText={errors.reasonsIsRequired}
        visible={!!errors.reasonsIsRequired && submitCount !== 0}
      />
    </Item>
  )
}

export { ReasonsSelector }
