import styled from 'styled-components'
import { Item, Label } from '@hindawi/ui'
import { Checkbox, ErrorField } from 'hindawi-shared/client/components'
import { FormValues, FormErrors } from '../validateAuthorSanctionList'

interface TypeSelectorProps {
  errors: FormErrors
  values: FormValues
  setValues: (values: any) => any
  submitCount: number
}

const TypeSelector: React.FC<TypeSelectorProps> = ({
  errors,
  values,
  setValues,
  submitCount,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setValues({ ...values, [event.target.value]: event.target.checked })
  }

  return (
    <Root>
      <Label required>Type</Label>
      <Item justify="flex-start" mt={2}>
        <Item mr={4}>
          <Checkbox
            checked={values.isOnBadDebtList}
            label="Bad debt"
            onChange={handleChange}
            value="isOnBadDebtList"
          />
        </Item>
        <Item mr={4}>
          <Checkbox
            checked={values.isOnBlackList}
            label="Blacklist"
            onChange={handleChange}
            value="isOnBlackList"
          />
        </Item>
        <Item mr={4}>
          <Checkbox
            checked={values.isOnWatchList}
            label="Watchlist"
            onChange={handleChange}
            value="isOnWatchList"
          />
        </Item>
      </Item>
      <ErrorField
        errorText={errors.typeIsRequired}
        visible={!!errors.typeIsRequired && submitCount !== 0}
      />
    </Root>
  )
}

export { TypeSelector }

const Root = styled.div``
