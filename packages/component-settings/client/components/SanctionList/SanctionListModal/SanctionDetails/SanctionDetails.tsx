import { Row, Item, Label, ValidatedFormField, Textarea } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H3 } from '@pubsweet/ui'
import { ReasonsSelector } from './ReasonsSelector'
import { EndDateSelector } from './EndDateSelector'
import { TypeSelector } from './TypeSelector'
import { FormErrors, FormValues } from '../validateAuthorSanctionList'

interface Reason {
  id: string
  name: string
}
interface SanctionDetailsProps {
  errors: FormErrors
  submitCount: number
  values: FormValues
  setValues: (values: any) => any
  profileReasons: [Reason]
}

const SanctionDetails: React.FC<SanctionDetailsProps> = ({
  errors,
  submitCount,
  values,
  setValues,
  profileReasons,
}) => (
  <Root>
    <Row justify="flex-start">
      <H3 mb={2} mt={4}>
        Sanction Details
      </H3>
    </Row>
    <Card>
      <Row alignItems="flex-start" justify="space-between">
        <TypeSelector
          errors={errors}
          setValues={setValues}
          submitCount={submitCount}
          values={values}
        />
        <EndDateSelector setValues={setValues} values={values} />
      </Row>
      <ReasonsSelector
        errors={errors}
        profileReasons={profileReasons}
        setValues={setValues}
        submitCount={submitCount}
        values={values}
      />
      <Row>
        <Item vertical>
          <Label mb={1}>Comments</Label>
          <ValidatedFormField
            component={Textarea}
            data-test-id="comments"
            name="comments"
            placeholder="Fill in"
            type="text"
          />
        </Item>
      </Row>
    </Card>
  </Root>
)
export { SanctionDetails }

const Root = styled.div``

const Card = styled.div`
  background-color: ${th('backgroundColor')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 4) calc(${th('gridUnit')} * 4) 0;
`
