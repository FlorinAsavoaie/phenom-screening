import { ReactElement } from 'react'
import { Dropdown, DropdownOption } from '@hindawi/ui'
import { Modal } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DeleteModal } from './DeleteModal'

interface EditComponentProps {
  itemId: string
  hideModal: () => void
  justifyContent: string
  modalKey: string
  onSubmit: () => Promise<void>
}
interface EditDeleteProps {
  item: any
  EditComponent: React.FC<EditComponentProps>
  editModalKey: string
  deleteModalKey: string
  deleteTitle: string
  resetInputs: () => any
  editFn: (data: any, itemId: string) => void
  deleteFn: (itemId: string) => void
}

const EditDeleteDropdownMenu: React.FC<EditDeleteProps> = ({
  item,
  EditComponent,
  editFn,
  deleteFn,
  editModalKey,
  deleteModalKey,
  resetInputs,
  deleteTitle,
}) => (
  <ActionDropdown className="actionDropdown" data-test-id="action-dropdown">
    <Dropdown>
      <Modal
        component={(props: any) => (
          <EditComponent {...props} itemId={item.id} />
        )}
        justifyContent="flex-end"
        modalKey={editModalKey}
        onSubmit={async (data): Promise<void> => {
          await editFn(data, item.id)
          resetInputs()
        }}
      >
        {(showModal: unknown): ReactElement => (
          <DropdownOptionStyled data-test-id="edit-option" onClick={showModal}>
            Edit
          </DropdownOptionStyled>
        )}
      </Modal>
      <Modal
        cancelText="NO"
        component={DeleteModal}
        confirmText="YES"
        justifyContent="center"
        modalKey={deleteModalKey}
        onSubmit={async (): Promise<void> => {
          await deleteFn(item.id)
          resetInputs()
        }}
        title={deleteTitle}
      >
        {(showModal: unknown): ReactElement => (
          <DropdownOptionStyled
            data-test-id="delete-option"
            onClick={showModal}
          >
            Delete
          </DropdownOptionStyled>
        )}
      </Modal>
    </Dropdown>
  </ActionDropdown>
)

export const ActionDropdown = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: 0;
  margin-left: calc(${th('gridUnit')} * 8);
  align-items: center;
  display: flex;
  justify-content: flex-end;
  visibility: hidden;
`
const DropdownOptionStyled = styled(DropdownOption)`
  justify-content: flex-end;
`
export { EditDeleteDropdownMenu }
