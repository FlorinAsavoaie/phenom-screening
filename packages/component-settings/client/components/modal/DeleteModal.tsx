import { FormModal } from '@hindawi/ui'

interface DeleteModalProps {
  onSubmit: () => void
  cancelText: string
  confirmText: string
  title: string
  hideModal: () => void
}

const DeleteModal: React.FC<DeleteModalProps> = ({
  cancelText,
  confirmText,
  hideModal,
  title,
  onSubmit,
}) => (
  <FormModal
    cancelText={cancelText}
    confirmText={confirmText}
    hideModal={hideModal}
    initialValues=""
    onSubmit={(): void => {
      onSubmit()
      hideModal()
    }}
    title={title}
  />
)

export { DeleteModal }
