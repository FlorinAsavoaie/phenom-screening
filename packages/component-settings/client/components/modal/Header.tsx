import { Icon } from '@hindawi/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

interface HeaderProps {
  hideModal: () => void
  modalTitle: string
}

const Header: React.FC<HeaderProps> = ({ hideModal, modalTitle }) => (
  <HeaderWrapper>
    <Title>{modalTitle}</Title>
    <CloseIcon fontSize="28px" icon="remove1" onClick={hideModal} />
  </HeaderWrapper>
)
export { Header }
// #region styles
const CloseIcon = styled(Icon)`
  color: ${th('mainTextColor')};
`
const Title = styled.div`
  color: ${th('textSecondaryColor')};
  font-family: ${th('defaultFont')};
  font-size: ${th('h2Size')};
  font-weight: bold;
  line-height: normal;
`
const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: calc(${th('gridUnit')} * 3);
`
// #endregion
