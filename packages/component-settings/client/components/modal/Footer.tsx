import styled from 'styled-components'
import { Button, Spinner } from '@pubsweet/ui'
import { Row, Item } from '@hindawi/ui'
import { th } from '@pubsweet/ui-toolkit'
import { ErrorField } from 'hindawi-shared/client/components'

interface FooterProps {
  hideModal: () => void
  handleSubmit: () => void
  isFetching: boolean
  confirmButton: string
  errors?: any
}

const Footer: React.FC<FooterProps> = ({
  handleSubmit,
  hideModal,
  isFetching,
  confirmButton,
  errors = {},
}) => (
  <Row>
    <ErrorField
      errorText={errors.fetchingError}
      visible={!!errors.fetchingError}
    />
    <Item>
      <ButtonWrapper>
        <Button data-test-id="modal-cancel" onClick={hideModal} width={35}>
          CANCEL
        </Button>
      </ButtonWrapper>
      <ButtonWrapper>
        {isFetching ? (
          <Spinner size={8} />
        ) : (
          <Button
            data-test-id="modal-create"
            onClick={handleSubmit}
            primary
            width={35}
          >
            {confirmButton}
          </Button>
        )}
      </ButtonWrapper>
    </Item>
  </Row>
)

export { Footer }

const ButtonWrapper = styled.div`
  margin-left: calc(${th('gridUnit')} * 6);
  width: calc(${th('gridUnit')} * 35);
`
