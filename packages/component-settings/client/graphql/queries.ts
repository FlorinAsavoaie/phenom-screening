import { gql } from '@apollo/client'

export const getSuspiciousKeywords = gql`
  query getSuspiciousKeywords(
    $page: Int
    $pageSize: Int
    $searchValue: String
  ) {
    getSuspiciousKeywords(
      page: $page
      pageSize: $pageSize
      searchValue: $searchValue
    ) {
      results {
        id
        keyword
        note
        created
      }
      total
    }
  }
`

export const getSuspiciousKeyword = gql`
  query getSuspiciousKeyword($keywordId: ID!) {
    getSuspiciousKeyword(keywordId: $keywordId) {
      keyword
      note
    }
  }
`

export const getSanctionListProfiles = gql`
  query getSanctionListProfiles(
    $page: Int
    $pageSize: Int
    $searchValue: String
  ) {
    getSanctionListProfiles(
      page: $page
      pageSize: $pageSize
      searchValue: $searchValue
    ) {
      results {
        id
        surname
        givenNames
        email
        toDate
        fromDate
        created
        types
        comments
        reasons {
          id
          name
        }
      }
      total
    }
  }
`
export const getReasons = gql`
  query getReasons {
    getReasons {
      id
      name
    }
  }
`
export const getSanctionListProfile = gql`
  query getSanctionListProfile($profileId: ID!) {
    getSanctionListProfile(profileId: $profileId) {
      surname
      givenNames
      email
      isOnWatchList
      isOnBlackList
      isOnBadDebtList
      toDate
      comments
      reasons {
        id
        name
      }
    }
  }
`

export const getParamsForImportingKeywords = gql`
  query getParamsForImportingKeywords {
    getParamsForImportingKeywords {
      url
      fileId
    }
  }
`
export const getImportStatus = gql`
  query getImportStatus {
    getImportStatus {
      percentage
      message
    }
  }
`
