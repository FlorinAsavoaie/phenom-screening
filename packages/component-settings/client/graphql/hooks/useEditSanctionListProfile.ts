import { useMutation } from '@apollo/client'
import { ExecutionResult } from 'graphql'
import * as mutations from './../mutations'

type MutationData = boolean
type MutationVariables = {
  profileId: string
  isOnBadDebtList: boolean
  isOnWatchList: boolean
  isOnBlackList: boolean
  toDate: Date
  comments: string
  reasons: [string]
}
type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (data: MutationVariables, item: string) => MutationResult

function useEditSanctionListProfile(): HookResult {
  const [editSanctionListProfileMutation] = useMutation<
    MutationData,
    MutationVariables
  >(mutations.editSanctionListProfile)

  return (
    {
      isOnBlackList,
      isOnWatchList,
      isOnBadDebtList,
      toDate,
      comments,
      reasons,
    },
    profileId,
  ): MutationResult =>
    editSanctionListProfileMutation({
      variables: {
        profileId,
        isOnBlackList,
        isOnWatchList,
        isOnBadDebtList,
        toDate,
        comments,
        reasons,
      },
    })
}

export { useEditSanctionListProfile }
