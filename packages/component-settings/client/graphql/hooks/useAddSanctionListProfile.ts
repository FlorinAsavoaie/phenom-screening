import { useMutation } from '@apollo/client'
import { ExecutionResult } from 'graphql'
import { addSanctionListProfile } from './../mutations'

type MutationData = boolean
type MutationVariables = {
  givenNames: string
  surname: string
  email: string
  reasons: string[]
  comments: string
  toDate: string | null
  isOnBadDebtList: boolean
  isOnBlackList: boolean
  isOnWatchList: boolean
}
type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (args: MutationVariables) => MutationResult

function useAddSanctionListProfile(): HookResult {
  const [addSanctionListProfileMutation] = useMutation<
    MutationData,
    MutationVariables
  >(addSanctionListProfile)

  return (variables: MutationVariables): MutationResult =>
    addSanctionListProfileMutation({
      variables,
    })
}

export { useAddSanctionListProfile }
