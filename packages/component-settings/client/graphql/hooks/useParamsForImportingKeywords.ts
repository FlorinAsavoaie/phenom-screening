import { useApolloClient } from '@apollo/client'
import { useCallback } from 'react'
import { getParamsForImportingKeywords } from '../queries'

type ImportParams = {
  url: string
  fileId: string
}

type QueryResult = {
  getParamsForImportingKeywords: ImportParams
}

export function useParamsForImportingKeywords(): () => Promise<ImportParams> {
  const client = useApolloClient()

  return useCallback(async () => {
    const { data } = await client.query<QueryResult>({
      query: getParamsForImportingKeywords,
      fetchPolicy: 'no-cache',
    })

    return data.getParamsForImportingKeywords
  }, [client])
}
