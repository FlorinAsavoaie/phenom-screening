import { useApolloClient } from '@apollo/client'
import { useCallback } from 'react'
import { getImportStatus } from '../queries'

type ImportStatus = {
  percentage: number
  message: string
}
type QueryResult = {
  getImportStatus: ImportStatus
}

export function useImportStatus(): () => Promise<ImportStatus> {
  const client = useApolloClient()

  return useCallback(async () => {
    const { data } = await client.query<QueryResult>({
      query: getImportStatus,
      fetchPolicy: 'no-cache',
    })

    return data.getImportStatus
  }, [client])
}
