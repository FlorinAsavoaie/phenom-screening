import { useMutation } from '@apollo/client'
import { ExecutionResult } from 'graphql'

import * as mutations from './../mutations'

type MutationData = boolean
type MutationVariables = {
  keyword: string
  note: string
}
type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (args: MutationVariables) => MutationResult

function useAddSuspiciousKeyword(): HookResult {
  const [addSuspiciousKeywordMutation] = useMutation<
    MutationData,
    MutationVariables
  >(mutations.addSuspiciousKeyword)

  return ({ keyword, note }): MutationResult =>
    addSuspiciousKeywordMutation({
      variables: {
        keyword,
        note,
      },
    })
}
export { useAddSuspiciousKeyword }
