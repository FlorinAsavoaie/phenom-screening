import { useMutation } from '@apollo/client'
import { ExecutionResult } from 'graphql'
import * as mutations from './../mutations'

type MutationData = boolean

type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (profileId: string) => MutationResult

function useDeleteSanctionListProfile(): HookResult {
  const [deleteSanctionListProfileMutation] = useMutation<MutationData>(
    mutations.deleteSanctionListProfile,
  )

  return (profileId): MutationResult =>
    deleteSanctionListProfileMutation({
      variables: {
        profileId,
      },
    })
}

export { useDeleteSanctionListProfile }
