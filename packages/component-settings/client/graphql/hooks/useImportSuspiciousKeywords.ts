import { useApolloClient } from '@apollo/client'
import { useCallback } from 'react'
import { importSuspiciousKeywords } from '../mutations'

type MutationVariables = {
  fileId: string
}
type MutationResult = void

export function useImportSuspiciousKeywords(): (
  fileId: string,
) => Promise<void> {
  const client = useApolloClient()

  return useCallback(
    async (fileId: string) => {
      await client.mutate<MutationResult, MutationVariables>({
        mutation: importSuspiciousKeywords,
        variables: {
          fileId,
        },
      })
    },
    [client],
  )
}
