import { useCallback } from 'react'
import { useParamsForImportingKeywords } from './useParamsForImportingKeywords'

export function useUploadFile(): (file: File) => Promise<string> {
  const getImportParams = useParamsForImportingKeywords()

  return useCallback(
    async (file) => {
      const importParams = await getImportParams()
      const requestOptions = {
        method: 'PUT',
        body: file,
      }
      await fetch(importParams.url, requestOptions)

      return importParams.fileId
    },
    [getImportParams],
  )
}
