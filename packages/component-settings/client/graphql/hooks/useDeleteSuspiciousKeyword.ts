import { useMutation } from '@apollo/client'
import { ExecutionResult } from 'graphql'

import * as mutations from './../mutations'

type MutationData = boolean

type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (args: string) => MutationResult

function useDeleteSuspiciousKeyword(): HookResult {
  const [deleteSuspiciousKeywordMutation] = useMutation<MutationData>(
    mutations.deleteSuspiciousKeyword,
  )

  return (keywordId: string): MutationResult =>
    deleteSuspiciousKeywordMutation({
      variables: {
        keywordId,
      },
    })
}
export { useDeleteSuspiciousKeyword }
