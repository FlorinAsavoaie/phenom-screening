import { useMutation } from '@apollo/client'
import { ExecutionResult } from 'graphql'
import * as mutations from './../mutations'

type MutationData = boolean
type MutationVariables = {
  keywordId: string
  note: string
}
type MutationResult = Promise<ExecutionResult<MutationData>>
type HookResult = (data: MutationVariables, item: string) => MutationResult

function useEditSuspiciousKeyword(): HookResult {
  const [editSuspiciousKeywordMutation] = useMutation<
    MutationData,
    MutationVariables
  >(mutations.editSuspiciousKeyword)

  return ({ note }, keywordId): MutationResult =>
    editSuspiciousKeywordMutation({
      variables: { keywordId, note },
    })
}

export { useEditSuspiciousKeyword }
