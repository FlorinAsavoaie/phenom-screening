import { gql } from '@apollo/client'

export const addSuspiciousKeyword = gql`
  mutation addSuspiciousKeyword($keyword: String!, $note: String!) {
    addSuspiciousKeyword(keyword: $keyword, note: $note)
  }
`

export const editSuspiciousKeyword = gql`
  mutation editSuspiciousKeyword($keywordId: ID!, $note: String!) {
    editSuspiciousKeyword(keywordId: $keywordId, note: $note)
  }
`

export const deleteSuspiciousKeyword = gql`
  mutation deleteSuspiciousKeyword($keywordId: ID!) {
    deleteSuspiciousKeyword(keywordId: $keywordId)
  }
`

export const addSanctionListProfile = gql`
  mutation addSanctionListProfile(
    $surname: String
    $givenNames: String
    $isOnBadDebtList: Boolean
    $isOnBlackList: Boolean
    $isOnWatchList: Boolean
    $email: String
    $toDate: DateTime
    $reasons: [ID]
    $comments: String
  ) {
    addSanctionListProfile(
      surname: $surname
      givenNames: $givenNames
      isOnBadDebtList: $isOnBadDebtList
      isOnBlackList: $isOnBlackList
      isOnWatchList: $isOnWatchList
      email: $email
      toDate: $toDate
      reasons: $reasons
      comments: $comments
    )
  }
`
export const editSanctionListProfile = gql`
  mutation editSanctionListProfile(
    $profileId: ID!
    $isOnBadDebtList: Boolean
    $isOnBlackList: Boolean
    $isOnWatchList: Boolean
    $toDate: DateTime
    $comments: String
    $reasons: [ID]
  ) {
    editSanctionListProfile(
      profileId: $profileId
      isOnBadDebtList: $isOnBadDebtList
      isOnWatchList: $isOnWatchList
      isOnBlackList: $isOnBlackList
      toDate: $toDate
      comments: $comments
      reasons: $reasons
    )
  }
`
export const deleteSanctionListProfile = gql`
  mutation deleteSanctionListProfile($profileId: ID!) {
    deleteSanctionListProfile(profileId: $profileId)
  }
`

export const exportSuspiciousKeywords = gql`
  mutation exportSuspiciousKeywords {
    exportSuspiciousKeywords {
      url
      totalNumber
    }
  }
`

export const importSuspiciousKeywords = gql`
  mutation importSuspiciousKeywords($fileId: String!) {
    importSuspiciousKeywords(fileId: $fileId)
  }
`
