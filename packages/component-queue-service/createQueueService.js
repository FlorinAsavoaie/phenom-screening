const AWS = require('aws-sdk')
const path = require('path')
const uuid = require('uuid')
const pubsweetLogger = require('@pubsweet/logger')
const { get, isEmpty } = require('lodash')

const { createSqsConsumer } = require('./createSqsConsumer')
const { createLargeEventsService } = require('./createLargeEventsService')

function getNameFromPackageJson() {
  const filePath = path.resolve(process.cwd(), 'package.json')
  return require(filePath).name // eslint-disable-line import/no-dynamic-require
}

async function createQueueService(
  {
    region,
    accessKeyId,
    secretAccessKey,
    snsEndpoint,
    sqsEndpoint,
    s3Endpoint,
    topicName,
    queueName,
    bucketName,
    bucketPrefix,
    eventNamespace,
    publisherName,
    serviceName,
    defaultMessageAttributes,
  } = {
    region: process.env.AWS_SNS_SQS_REGION,
    accessKeyId: process.env.AWS_SNS_SQS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SNS_SQS_SECRET_KEY,
    snsEndpoint: process.env.AWS_SNS_ENDPOINT,
    sqsEndpoint: process.env.AWS_SQS_ENDPOINT,
    s3Endpoint: process.env.AWS_S3_ENDPOINT,
    topicName: process.env.AWS_SNS_TOPIC,
    queueName: process.env.AWS_SQS_QUEUE_NAME,
    bucketName: process.env.PHENOM_LARGE_EVENTS_BUCKET,
    bucketPrefix:
      process.env.PHENOM_LARGE_EVENTS_PREFIX || 'in-transit-events/',
    eventNamespace: process.env.EVENT_NAMESPACE,
    publisherName: process.env.PUBLISHER_NAME,
    serviceName: getNameFromPackageJson(),
    defaultMessageAttributes: JSON.parse(
      process.env.DEFAULT_MESSAGE_ATTRIBUTES || '{}',
    ),
  },
  logger = pubsweetLogger,
) {
  const sns = new AWS.SNS({
    endpoint: snsEndpoint,
    region,
    accessKeyId,
    secretAccessKey,
  })

  const { TopicArn } = await sns.createTopic({ Name: topicName }).promise()

  const largeEventsService = createLargeEventsService({
    storage: new AWS.S3({
      endpoint: s3Endpoint,
      accessKeyId,
      secretAccessKey,
    }),
    Bucket: bucketName,
    prefix: bucketPrefix,
  })

  async function processMessage(rawResponse) {
    const body = JSON.parse(get(rawResponse, 'Body'))
    const isLargeEvent =
      get(body, 'MessageAttributes.PhenomMessageTooLarge.Value', 'N') === 'Y'
    return handleMessage({ ...JSON.parse(get(body, 'Message')), isLargeEvent })
  }

  const sqsConsumer = await createSqsConsumer({
    handleMessage: processMessage,
    sqsEndpoint,
    queueName,
    region,
    accessKeyId,
    secretAccessKey,
  })

  const eventHandlerMap = initEventHandlerMap()

  return {
    publishMessage,
    registerEventHandler,
    start,
  }

  function getMessageAttributesSize(messageAttributes) {
    return Object.entries(messageAttributes).reduce(
      (acc, [key, { DataType, StringValue }]) =>
        [key, DataType, StringValue].reduce(
          (sum, val) => sum + Buffer.byteLength(val),
          0,
        ),
      0,
    )
  }

  async function publishMessage({
    event,
    data,
    messageAttributes,
    timestamp = new Date().toISOString(),
    id = uuid.v4(),
  }) {
    const MessageObject = {
      event: getEventName(event),
      timestamp,
      data,
      id,
    }
    const jsonData = JSON.stringify(MessageObject)

    const MessageAttributesObject = {}
    const MessageAttributes = new Proxy(MessageAttributesObject, {
      get(obj, prop) {
        return prop in obj
          ? {
              DataType: 'String',
              StringValue: obj[prop],
            }
          : undefined
      },
    })

    Object.assign(
      MessageAttributesObject,
      defaultMessageAttributes,
      messageAttributes,
    )

    logger.info(
      `Sending event: ${jsonData} with attributes ${JSON.stringify(
        MessageAttributesObject,
      )}`,
    )

    let Message = jsonData

    // SNS & SQS Limit Size = 256K including message attributes
    if (
      Buffer.byteLength(jsonData) +
        getMessageAttributesSize(MessageAttributes) >
      262144
    ) {
      logger.info(
        'Event larger than 256K. Uploading to S3 and sending a reference',
      )
      MessageObject.data = await largeEventsService.put(JSON.stringify(data))
      Message = JSON.stringify(MessageObject)
      Object.assign(MessageAttributesObject, { PhenomMessageTooLarge: 'Y' })
    }

    return sns.publish({ TopicArn, Message, MessageAttributes }).promise()
  }

  function registerEventHandler({ event, handler }) {
    if (isEmpty(eventHandlerMap[event])) {
      eventHandlerMap[event] = []
    }

    eventHandlerMap[event].push(handler)
  }

  function start() {
    if (!sqsConsumer.isRunning) {
      sqsConsumer.start()
    }

    logger.info(`Service connected to queue ${sqsConsumer.queueUrl}`)
  }

  function getEventName(actionName) {
    return `${eventNamespace}:${publisherName}:${serviceName}:${actionName}`
  }

  function extractActionName(eventName) {
    return [...eventName.split(':')].pop()
  }

  async function handleMessage({ event, data, timestamp, isLargeEvent }) {
    const actionName = extractActionName(event)

    if (isEmpty(eventHandlerMap[actionName])) {
      return
    }

    if (isLargeEvent === true) {
      logger.info('Received an event with large size. Retrieving data')
      data = JSON.parse(await largeEventsService.get(data))
    }

    logger.info(
      `Event triggered: ${actionName}. Data: ${JSON.stringify(
        data,
      )}. Timestamp: ${timestamp}`,
    )

    return Promise.all(
      eventHandlerMap[actionName].map(async (handler) => handler(data)),
    )
  }

  function initEventHandlerMap() {
    return {
      HealthCheck: [healthCheckHandler],
    }

    async function healthCheckHandler(event) {
      return publishMessage({
        event: 'HealthCheckResponse',
        data: event,
        timestamp: new Date().toISOString(),
        messageAttributes: { type: 'health-check' },
      })
    }
  }
}

module.exports = {
  createQueueService,
}
