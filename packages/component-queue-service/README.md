# Message Queue Service

This service adds the ability to connect to a SQS message queue, consume messages and publish messages to SNS

It is best used if you employ a SNS/SQS setup for interservice communication

![sns/sqs setup visual](https://d2908q01vomqb2.cloudfront.net/1b6453892473a467d07372d45eb05abc2031647a/2017/11/03/cross_acct_integration_sns_image_1.png)

### API

- `createQueueService()` - creates an instance of the messageQueueService
- `service.publishMessage(message)` - publishes a message to the configure SNS topic.  
  The message should be of type `{event: String, data: Any, timestamp?: Date.toISOString, messageAttributes?: Object}`
- `service.registerListener({event: String, handler: Function})` - registers a handler function for the specified
  event.  
  You can register multiple handlers for each event. To do this, you can call this function multiple times with the
  same `event` value  
  The service will call all handlers registered for the `event` value found in the message
- `service.startListening()` - starts listening on the specified queue

### Required Env vars

```sh
NODE_ENV=development
AWS_SNS_SQS_REGION=us-east-1
AWS_SNS_SQS_ACCESS_KEY=****
AWS_SNS_SQS_SECRET_KEY=****
AWS_SQS_ENDPOINT=http://localhost:4566
AWS_SNS_ENDPOINT=http://localhost:4566
AWS_SNS_TOPIC=test1
AWS_SQS_QUEUE_NAME=screening-queue
```

### Usage

If you want to use it in an `xpub` app, you can find an
example [here](https://gitlab.com/hindawi/xpub/xpub-screening/tree/master/packages/app-screening).

Check [this documentation](https://docs.localstack.cloud/overview/)
for an example of how you can configure a SNS/SQS clone service to run locally in docker.
