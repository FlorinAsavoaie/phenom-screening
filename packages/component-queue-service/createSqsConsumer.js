const AWS = require('aws-sdk')
const logger = require('@pubsweet/logger')
const { Consumer } = require('sqs-consumer')

async function createSqsConsumer({
  region,
  queueName,
  sqsEndpoint,
  accessKeyId,
  handleMessage,
  secretAccessKey,
}) {
  const sqs = new AWS.SQS({
    region,
    accessKeyId,
    secretAccessKey,
    endpoint: sqsEndpoint,
  })

  let { QueueUrl } = await sqs.getQueueUrl({ QueueName: queueName }).promise()

  if (process.env.NODE_ENV === 'development') {
    QueueUrl = QueueUrl.replace('sqs', 'localhost')
  }

  const sqsConsumer = Consumer.create({
    sqs,
    queueUrl: QueueUrl,
    handleMessage,
  })

  sqsConsumer.on('error', logger.error)

  sqsConsumer.on('processing_error', logger.error)

  return sqsConsumer
}

module.exports = {
  createSqsConsumer,
}
