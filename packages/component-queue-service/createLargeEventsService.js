const uuid = require('uuid')

function createLargeEventsService({ storage, Bucket, prefix }) {
  return {
    get,
    put,
  }

  async function put(Body) {
    const Key = `${prefix}/${uuid.v4()}`
    return storage
      .putObject({ Body, Bucket, Key })
      .promise()
      .then(() => `${Bucket}::${Key}`)
  }

  async function get(url) {
    const [Bucket, Key] = url.split('::')
    return storage
      .getObject({ Bucket, Key })
      .promise()
      .then((result) => result.Body.toString())
  }
}

module.exports = {
  createLargeEventsService,
}
