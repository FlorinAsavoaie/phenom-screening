const path = require('path')
const { get } = require('lodash')
const { initializeRootLogger } = require('hindawi-utils')

module.exports = {
  'pubsweet-server': {
    db: getDbConfig(),
    pool: { min: 0, max: 50 },
    logger: initializeRootLogger('ServiceAuthorVerification'),
  },
  pubsweet: {
    components: ['./models'],
  },
  port: process.env.PORT || 3000,
  aws: {
    region: process.env.AWS_REGION,
    s3: {
      accessKeyId: process.env.AWS_S3_ACCESS_KEY,
      secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    },
    sns_sqs: {
      accessKeyId: process.env.AWS_SNS_SQS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SNS_SQS_SECRET_KEY,
    },
    sqs: {
      endpoint: process.env.AWS_SQS_ENDPOINT,
      queueName: process.env.AWS_SQS_QUEUE_NAME,
    },
    sns: {
      endpoint: process.env.AWS_SNS_ENDPOINT,
      topic: process.env.AWS_SNS_TOPIC,
    },
  },
  eventNamespace: process.env.EVENT_NAMESPACE,
  publisherName: process.env.PUBLISHER_NAME,
  serviceName: getNameFromPackageJson(),
  defaultMessageAttributes: JSON.parse(
    process.env.DEFAULT_MESSAGE_ATTRIBUTES || '{}',
  ),
  largeEvents: {
    s3Endpoint: process.env.AWS_S3_ENDPOINT,
    bucketName: process.env.PHENOM_LARGE_EVENTS_BUCKET,
    bucketPrefix:
      process.env.PHENOM_LARGE_EVENTS_PREFIX || 'in-transit-events/',
  },
  wos: {
    elasticsearch: {
      node: process.env.WOS_ELASTICSEARCH_NODE,
      index: process.env.WOS_ELASTICSEARCH_INDEX,
      username: process.env.WOS_ELASTICSEARCH_USERNAME,
      password: process.env.WOS_ELASTICSEARCH_PASSWORD,
    },
  },
  orcid_api_endpoint: JSON.parse(
    get(process, 'env.ORCID_SANDBOX_ENABLED', false),
  )
    ? 'https://pub.sandbox.orcid.org/v3.0/'
    : 'https://pub.orcid.org/v3.0/',
}

function getDbConfig() {
  return {
    database: process.env.DATABASE || 'author_verification',
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    password: process.env.DB_PASSWORD || '',
    port: 5432,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 20000,
  }
}

function getNameFromPackageJson() {
  const filePath = path.resolve(process.cwd(), 'package.json')
  return require(filePath).name // eslint-disable-line import/no-dynamic-require
}
