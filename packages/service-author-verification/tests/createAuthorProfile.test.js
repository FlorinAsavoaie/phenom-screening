const useCases = require('../src/use-cases')
const models = require('@pubsweet/models')
const Chance = require('chance')

const chance = new Chance()

describe('Create Author Profile service', () => {
  let mockSave
  const data = {
    firstName: chance.name(),
    lastName: chance.name(),
    email: chance.email(),
  }

  models.AuthorProfile = jest.fn((authorData) => ({
    ...authorData,
    save: mockSave,
  }))

  beforeEach(() => {
    mockSave = jest.fn().mockReturnValue({
      primaryEmail: data.email,
      firstName: data.firstName,
      lastName: data.lastName,
    })
  })

  it('should correctly create author profile entity', async () => {
    models.AuthorProfile.findOneBy = jest.fn().mockReturnValue(undefined)

    const authorProfile = await useCases.createAuthorProfileUseCase
      .initialize(models)
      .execute(data)

    expect(mockSave).toHaveBeenCalled()
    expect(authorProfile.primaryEmail).toEqual(data.email)
    expect(authorProfile.firstName).toEqual(data.firstName)
    expect(authorProfile.lastName).toEqual(data.lastName)
  })
  it('should not create author profile entity because author already exists', async () => {
    models.AuthorProfile.findOneBy = jest.fn().mockReturnValue({})

    await useCases.createAuthorProfileUseCase.initialize(models).execute(data)

    expect(mockSave).toHaveBeenCalledTimes(0)
  })
})
