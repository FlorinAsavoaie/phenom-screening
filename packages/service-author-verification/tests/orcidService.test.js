const logger = require('@pubsweet/logger')
const axios = require('axios')
const { isAuthorValidOnOrcid } = require('../src/services')

jest.mock('axios')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
  info: jest.fn(),
}))

describe('Orcid Service', () => {
  beforeEach(() => {
    const mockResponse = {
      name: {
        'given-names': {
          value: 'John',
        },
        'family-name': {
          value: 'Doe',
        },
        'credit-name': { value: 'John-Snow Doe' },
      },
      'other-names': {
        'other-name': [{ content: 'J S Doe' }, { content: 'John S Doe-Pow' }],
      },
    }

    axios.get.mockImplementation(() =>
      Promise.resolve({ status: 200, data: mockResponse }),
    )
  })

  it('should skip orcid validation if no orcid value is provided', async () => {
    const author = {
      id: 'author-id',
      manuscriptId: 'manuscript-id',
      email: 'test@test.test',
      country: 'AD',
      surname: 'John',
      givenNames: 'Doe',
      orcid: '',
    }

    const result = await isAuthorValidOnOrcid(author)

    expect(logger.info).toHaveBeenCalledWith(
      `Skipping orcid validation for author with id: ${author.id}. No orcid value provided.`,
    )
    expect(result).toStrictEqual(null)
  })

  it('should find no issue on validation when names match', async () => {
    const author = {
      id: 'author-id',
      manuscriptId: 'manuscript-id',
      email: 'test@test.test',
      country: 'AD',
      givenNames: 'John',
      surname: 'Doe',
      orcid: '0000-0000-0000-0000',
    }

    const result = await isAuthorValidOnOrcid(author)

    expect(result).toStrictEqual(false)
  })

  it('should find no issue on validation no matter the names order', async () => {
    const author = {
      id: 'author-id',
      manuscriptId: 'manuscript-id',
      email: 'test@test.test',
      country: 'AD',
      givenNames: 'Doe',
      surname: 'S John Pow',
      orcid: '0000-0000-0000-0000',
    }

    const result = await isAuthorValidOnOrcid(author)

    expect(result).toStrictEqual(false)
  })

  it('should find issue on validation when names dont match', async () => {
    const author = {
      id: 'author-id',
      manuscriptId: 'manuscript-id',
      email: 'test@test.test',
      country: 'AD',
      surname: 'John',
      givenNames: 'Smith',
      orcid: '0000-0000-0000-0000',
    }

    const result = await isAuthorValidOnOrcid(author)

    expect(result).toStrictEqual(true)
  })
})
