const axios = require('axios')
const { findClosestROROrganization } = require('../src/services/rorService')

jest.mock('axios')

describe('should find closest ROR organization', () => {
  it('should return null values if the affiliation is empty', async () => {
    axios.get.mockImplementation(() =>
      Promise.resolve({ status: 200, data: [] }),
    )

    const affiliation = ''
    const result = await findClosestROROrganization(affiliation)

    expect(result).toEqual({
      rorScore: null,
      rorId: null,
      rorAff: null,
    })
  })

  it("should return null values if the affiliation doesn't contain at least one letter", async () => {
    axios.get.mockImplementation(() =>
      Promise.resolve({ status: 200, data: [] }),
    )

    const affiliation = '123.'
    const result = await findClosestROROrganization(affiliation)

    expect(result).toEqual({
      rorScore: null,
      rorId: null,
      rorAff: null,
    })
  })

  it('should return null values if the affiliation is in round brackets', async () => {
    axios.get.mockImplementation(() =>
      Promise.resolve({ status: 200, data: [] }),
    )

    const affiliation = '(test)'
    const result = await findClosestROROrganization(affiliation)

    expect(result).toEqual({
      rorScore: null,
      rorId: null,
      rorAff: null,
    })
  })

  it('should return the organization with chosen true', async () => {
    const mockResponse = {
      items: [
        {
          score: 1,
          chosen: true,
          organization: {
            id: 'https://ror.org/03pehnk14',
            name: 'A&A Biotechnology (Poland)',
          },
        },
        {
          score: 0.88,
          chosen: false,
          organization: {
            id: 'organization-id',
            name: 'organization-name',
          },
        },
      ],
    }

    axios.get.mockImplementation(() =>
      Promise.resolve({ status: 200, data: mockResponse }),
    )

    const affiliation = 'A&A Biotechnology (Poland), Gdynia'
    const result = await findClosestROROrganization(affiliation)

    expect(result).toEqual({
      rorScore: 1,
      rorId: '03pehnk14',
      rorAff: 'A&A Biotechnology (Poland)',
    })
  })

  it('should return the first organization if none of them have chosen true', async () => {
    const mockResponse = {
      items: [
        {
          score: 1,
          chosen: false,
          organization: {
            id: 'https://ror.org/03pehnk14',
            name: 'A&A Biotechnology (Poland)',
          },
        },
        {
          score: 0.88,
          chosen: false,
          organization: {
            id: 'organization-id',
            name: 'organization-name',
          },
        },
      ],
    }

    axios.get.mockImplementation(() =>
      Promise.resolve({ status: 200, data: mockResponse }),
    )

    const affiliation = 'A&A Biotechnology (Poland), Gdynia'
    const result = await findClosestROROrganization(affiliation)

    expect(result).toEqual({
      rorScore: 1,
      rorId: '03pehnk14',
      rorAff: 'A&A Biotechnology (Poland)',
    })
  })

  it('should return null values if no organization is found', async () => {
    const mockResponse = { items: [] }

    axios.get.mockImplementation(() =>
      Promise.resolve({ status: 200, data: mockResponse }),
    )

    const affiliation = 'A&A Biotechnology (Poland), Gdynia'
    const result = await findClosestROROrganization(affiliation)

    expect(result).toEqual({
      rorScore: null,
      rorId: null,
      rorAff: null,
    })
  })
})
