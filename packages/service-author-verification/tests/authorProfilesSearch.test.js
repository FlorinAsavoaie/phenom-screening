const models = require('@pubsweet/models')
const useCases = require('../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { AuthorProfile: authorProfile } = models
const wosDataRepository = {
  searchWoSAuthors: jest.fn(async () => {}),
}

const services = {
  isAuthorValidOnOrcid: jest.fn(),
  findClosestROROrganization: jest.fn(),
}

const generateInput = () => [
  {
    email: chance.email(),
    surename: chance.name(),
    givenNames: chance.name(),
    id: chance.guid(),
  },
  {
    email: 'test@test.test',
    surename: chance.name(),
    givenNames: chance.name(),
    id: chance.guid(),
  },
]

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

describe('Author Verified Use Case', () => {
  let authorsInput
  let manuscriptIdInput
  beforeEach(async () => {
    authorsInput = generateInput()
    manuscriptIdInput = chance.guid()

    wosDataRepository.searchWoSAuthors = jest.fn().mockReturnValue([
      {
        affiliation: chance.string(),
        authorId: chance.guid(),
        email: chance.email(),
        givenNames: chance.name(),
        noOfPublications: chance.natural(),
        surname: chance.name(),
        manuscriptDoi: chance.guid(),
        manuscriptWosId: chance.guid(),
      },
    ])
    authorProfile.findOneBy = jest.fn().mockImplementation((query) => {
      if (query.queryObject.primaryEmail === 'test@test.test') {
        return {
          primaryEmail: 'test@test.test',
        }
      }
      return null
    })
    services.isAuthorValidOnOrcid = jest
      .fn()
      .mockReturnValueOnce(true)
      .mockReturnValueOnce(false)

    services.findClosestROROrganization = jest
      .fn()
      .mockReturnValueOnce({
        rorId: '12345',
        rorScore: 0.33,
        rorAff: 'ROR Affiliation',
      })
      .mockReturnValueOnce({ rorId: null, rorScore: null, rorAff: null })
  })

  it('should publish a hashmap containing the verified authors data from author profiles table', async () => {
    await useCases.authorProfilesSearchUseCase
      .initialize({ models, wosDataRepository, services })
      .execute({ manuscriptId: manuscriptIdInput, authors: authorsInput })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'AuthorProfilesSearchFinished',
        data: {
          manuscriptId: manuscriptIdInput,
          authors: {
            [authorsInput[0].email]: expect.objectContaining({
              hasOrcidIssue: true,
              ror: expect.objectContaining({
                id: '12345',
                score: 0.33,
                aff: 'ROR Affiliation',
              }),
              isVerified: false,
              profiles: expect.arrayContaining([
                expect.objectContaining({
                  email: expect.anything(),
                  noOfPublications: expect.anything(),
                  surname: expect.anything(),
                  givenNames: expect.anything(),
                  affiliation: expect.anything(),
                  authorId: expect.anything(),
                  publications: expect.arrayContaining([
                    expect.objectContaining({
                      manuscriptWosId: expect.anything(),
                      manuscriptDoi: expect.anything(),
                    }),
                  ]),
                }),
              ]),
            }),
            [authorsInput[1].email]: expect.objectContaining({
              hasOrcidIssue: false,
              isVerified: true,
              profiles: null,
              ror: expect.objectContaining({
                id: null,
                score: null,
                aff: null,
              }),
            }),
          },
        },
      }),
    )
  })
})

describe('Author profile weights are correct', () => {
  let authorsInput
  let manuscriptIdInput
  beforeEach(async () => {
    jest.clearAllMocks()

    manuscriptIdInput = chance.guid()

    authorProfile.findOneBy = jest.fn().mockImplementation((query) => {
      if (query.queryObject.primaryEmail === 'test@test.test') {
        return {
          primaryEmail: 'test@test.test',
        }
      }
      return null
    })
  })

  // eslint-disable-next-line max-params
  function getWosProfile(email, givenNames, surname, affiliation) {
    return {
      affiliation,
      authorId: chance.guid(),
      email,
      firstName: givenNames,
      noOfPublications: chance.natural(),
      lastName: surname,
    }
  }

  test.each`
    email      | givenNames | surname | affiliation | existingWosProfiles                          | expectedWeight
    ${'a@a.a'} | ${'ION'}   | ${'X'}  | ${'Y'}      | ${[getWosProfile('a@a.a', 'ION', 'X', 'Y')]} | ${18}
    ${'a@a.a'} | ${'ION'}   | ${'X'}  | ${'Y'}      | ${[getWosProfile('', 'ION', 'X', 'Y')]}      | ${8}
    ${'a@a.a'} | ${'ION'}   | ${'X'}  | ${'Y'}      | ${[getWosProfile('a@a.a', '', 'X', 'Y')]}    | ${17}
    ${'a@a.a'} | ${'ION'}   | ${'X'}  | ${'Y'}      | ${[getWosProfile('a@a.a', 'ION', '', 'Y')]}  | ${13}
    ${'a@a.a'} | ${'ION'}   | ${'X'}  | ${'Y'}      | ${[getWosProfile('a@a.a', 'ION', 'X', '')]}  | ${16}
    ${'a@a.a'} | ${'ION'}   | ${'X'}  | ${'Y'}      | ${[getWosProfile('', '', 'X', 'Y')]}         | ${7}
    ${'a@a.a'} | ${'ION'}   | ${'X'}  | ${'Y'}      | ${[getWosProfile('a@a.a', '', 'X', '')]}     | ${15}
  `(
    'returns weight: $expectedWeight when searching in $existingWosProfiles by $email, $givenNames, $surname, $affiliation',
    async ({
      email,
      givenNames,
      surname,
      affiliation,
      existingWosProfiles,
      expectedWeight,
    }) => {
      authorsInput = [
        {
          email,
          aff: affiliation,
          surname,
          givenNames,
          id: chance.guid(),
        },
      ]

      wosDataRepository.searchWoSAuthors = jest
        .fn()
        .mockImplementation(() => existingWosProfiles)

      services.isAuthorValidOnOrcid = jest.fn().mockReturnValue(true)
      services.findClosestROROrganization = jest.fn().mockReturnValue({
        rorId: '24680',
        rorScore: 1,
        rorAff: 'ROR Affiliation',
      })

      await useCases.authorProfilesSearchUseCase
        .initialize({ models, wosDataRepository, services })
        .execute({ manuscriptId: manuscriptIdInput, authors: authorsInput })

      expect(applicationEventBus.publishMessage).toHaveBeenCalledWith(
        expect.objectContaining({
          event: 'AuthorProfilesSearchFinished',
          data: {
            manuscriptId: manuscriptIdInput,
            authors: {
              [authorsInput[0].email]: expect.objectContaining({
                hasOrcidIssue: true,
                isVerified: false,
                ror: expect.objectContaining({
                  id: '24680',
                  score: 1,
                  aff: 'ROR Affiliation',
                }),
                profiles: expect.arrayContaining([
                  expect.objectContaining({
                    email: existingWosProfiles[0].email,
                    noOfPublications: expect.anything(),
                    lastName: existingWosProfiles[0].lastName,
                    firstName: existingWosProfiles[0].firstName,
                    affiliation: existingWosProfiles[0].affiliation,
                    authorId: expect.anything(),
                    weight: expectedWeight,
                  }),
                ]),
              }),
            },
          },
        }),
      )
    },
  )
})
