const logger = require('@pubsweet/logger')

const MIGRATIONS_TABLE = 'migrations'
const MANUSCRIPT_PROFILE_TABLE = 'manuscript_profile'
const ENTITIES_TABLE = 'entities'

exports.up = async (knex) => {
  const migrationsTableExists = await knex.schema.hasTable(MIGRATIONS_TABLE)
  if (migrationsTableExists) {
    await knex.schema.dropTable(MIGRATIONS_TABLE)
  }
  const manuscriptProfileTableExists = await knex.schema.hasTable(
    MANUSCRIPT_PROFILE_TABLE,
  )
  if (manuscriptProfileTableExists) {
    await knex.schema.dropTable(MANUSCRIPT_PROFILE_TABLE)
  }
  const entitiesTableExists = await knex.schema.hasTable(ENTITIES_TABLE)
  if (entitiesTableExists) {
    await knex.schema.dropTable(ENTITIES_TABLE)
  }
}

exports.down = async (knex) => {
  await knex.schema
    .createTable(MIGRATIONS_TABLE, (table) => {
      table.text('id').primary().notNullable()
      table.timestamp('run_at').defaultTo(knex.fn.now())
    })
    .then((_) => logger.info(`${MIGRATIONS_TABLE} table created successfully`))
    .catch((err) => logger.error(err))

  await knex.schema
    .createTable(MANUSCRIPT_PROFILE_TABLE, (table) => {
      table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(knex.fn.now())
      table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

      table.text('doi')
      table.text('mts_id')
      table.text('manuscript_title')
      table.text('journal_title')
      table.text('author_name')
      table.text('author_email_address')
      table.text('author_hindawi_unique_id')
      table.text('author_mts_id')
      table.text('author_orcid_id')
      table.text('author_scopus_id')
      table.text('author_researcher_id')
      table.boolean('is_coauthor')
      table.boolean('is_editor')
      table.timestamp('publication_date')
    })
    .then((_) =>
      logger.info(`${MANUSCRIPT_PROFILE_TABLE} table created successfully`),
    )
    .catch((err) => logger.error(err))
}
