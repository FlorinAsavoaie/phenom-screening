const logger = require('@pubsweet/logger')

const AUTHOR_PROFILE_TABLE = 'author_profile'
const MANUSCRIPT_PROFILE_TABLE = 'manuscript_profile'

exports.up = async (knex) => {
  const authorProfileExists = await knex.schema.hasTable(AUTHOR_PROFILE_TABLE)

  if (!authorProfileExists) {
    await knex.raw(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`)
    await knex.schema
      .createTable(AUTHOR_PROFILE_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())
        table.text('hindawi_unique_id')
        table.text('mts_id')
        table.text('scopus_id')
        table.text('researcher_id')
        table.text('orcid_id')
        table.text('title')
        table.text('first_name')
        table.text('last_name')
        table.text('primary_email')
        table.text('primary_affiliation')
        table.text('country')
        table.text('secondary_email')
        table.text('secondary_affiliation')
        table.boolean('is_author')
        table.boolean('is_reviewer')
        table.boolean('is_editor')
        table.boolean('pub_record_verified')
        table.boolean('rev_history_verified')
        table
          .timestamp('account_created_date')
          .notNullable()
          .defaultTo(knex.fn.now())
      })
      .then((_) =>
        logger.info(`${AUTHOR_PROFILE_TABLE} table created successfully`),
      )
      .catch((err) => logger.error(err))

    await knex.schema
      .createTable(MANUSCRIPT_PROFILE_TABLE, (table) => {
        table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
        table.timestamp('created').notNullable().defaultTo(knex.fn.now())
        table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

        table.text('doi')
        table.text('mts_id')
        table.text('manuscript_title')
        table.text('journal_title')
        table.text('author_name')
        table.text('author_email_address')
        table.text('author_hindawi_unique_id')
        table.text('author_mts_id')
        table.text('author_orcid_id')
        table.text('author_scopus_id')
        table.text('author_researcher_id')
        table.boolean('is_coauthor')
        table.boolean('is_editor')
        table.timestamp('publication_date')
      })
      .then((_) =>
        logger.info(`${MANUSCRIPT_PROFILE_TABLE} table created successfully`),
      )
      .catch((err) => logger.error(err))
  }
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(AUTHOR_PROFILE_TABLE)
    .then((_) =>
      logger.info(`${AUTHOR_PROFILE_TABLE} table dropped successfully`),
    )
    .catch((err) => logger.error(err))

  await knex.schema
    .dropTable(MANUSCRIPT_PROFILE_TABLE)
    .then((_) =>
      logger.info(`${MANUSCRIPT_PROFILE_TABLE} table dropped successfully`),
    )
    .catch((err) => logger.error(err))
}
