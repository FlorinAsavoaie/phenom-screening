const models = require('@pubsweet/models')
const config = require('config')

const useCases = require('./use-cases')
const services = require('./services')

const wosDataRepository =
  require('@hindawi/lib-wos-data-store').wosDataRepository.initialize({
    configService: config,
  })

module.exports = {
  async ManuscriptIngested(data) {
    return useCases.authorProfilesSearchUseCase
      .initialize({
        models,
        wosDataRepository,
        services,
      })
      .execute(data)
  },
  async AuthorConfirmed(data) {
    return useCases.createAuthorProfileUseCase.initialize(models).execute(data)
  },
}
