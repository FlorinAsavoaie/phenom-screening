const axios = require('axios')

async function findClosestROROrganization(affiliation) {
  const parsedAffiliation = getParsedAffiliation(affiliation)

  const affiliationIsValid = checkIfTheAffiliationIsValid(parsedAffiliation)

  if (!affiliationIsValid)
    return {
      rorScore: null,
      rorAff: null,
      rorId: null,
    }

  const rorOrganizations = await getRorOrganizations(parsedAffiliation)

  if (!rorOrganizations.length)
    return {
      rorScore: null,
      rorAff: null,
      rorId: null,
    }

  const chosenRorOrganization = rorOrganizations.find((item) => item.chosen)

  const closestRorOrganization = chosenRorOrganization || rorOrganizations[0]

  return {
    rorScore: closestRorOrganization.score,
    rorAff: closestRorOrganization.organization.name,
    rorId: closestRorOrganization.organization.id.split('/').pop(),
  }
}

async function getRorOrganizations(affiliation) {
  let fetchedData
  const url = `https://api.ror.org/organizations?affiliation=${encodeURIComponent(
    affiliation,
  )}`

  try {
    const { data } = await axios.get(url)
    fetchedData = data
  } catch (error) {
    const { message } = error.toJSON()
    throw new Error(`RORID ERROR: ${message}`)
  }

  return fetchedData.items
}

function getParsedAffiliation(aff) {
  return aff.replace(/ *\([^)]*\) */g, '').replace(/["]+/g, '')
}

function checkIfTheAffiliationIsValid(affiliation) {
  if (!affiliation) return false
  return hasAtLeastOneLetter(affiliation)
}

function hasAtLeastOneLetter(affiliation) {
  const letterRegex = new RegExp(/[A-Za-z]+/)
  return letterRegex.test(affiliation)
}

module.exports = {
  findClosestROROrganization,
}
