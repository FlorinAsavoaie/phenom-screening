const logger = require('@pubsweet/logger')
const { get, union } = require('lodash')
const config = require('config')
const axios = require('axios')

const isAuthorValidOnOrcid = async ({ id, orcid, givenNames, surname }) => {
  /**
   * @typedef OrcidValidationResult
   * @type {object}
   * @property {string} email
   * @property {boolean | null} hasOrcidIssue
   *
   * @returns {OrcidValidationResult}
   */

  if (!orcid) {
    logger.info(
      `Skipping orcid validation for author with id: ${id}. No orcid value provided.`,
    )

    return null
  }

  const orcidApiData = await fetchOrcidApi(orcid)
  const orcidApiNames = getOrcidApiNames(orcidApiData)
  const authorNames = getAuthorNames(givenNames, surname)
  const isMatchFound = doesAuthorNameMatchApiName(authorNames, orcidApiNames)

  return !isMatchFound
}

const fetchOrcidApi = async (orcid) => {
  const ORCID_API_ENDPOINT = config.get('orcid_api_endpoint')
  const headers = {
    accept: 'application/json;charset=UTF-8',
    'content-type': 'application/json;charset=UTF-8',
  }

  let fetchedData

  try {
    const { data } = await axios.get(`${ORCID_API_ENDPOINT}${orcid}/person`, {
      headers,
    })

    fetchedData = data
  } catch (error) {
    const { message } = error.toJSON()
    throw new Error(`ORCID ERROR: ${message}`)
  }

  return fetchedData
}

const getOrcidApiNames = (apiData) => {
  const givenName = get(apiData, 'name.given-names.value', '')
  const familyName = get(apiData, 'name.family-name.value', '')
  const creditName = get(apiData, 'name.credit-name.value', '')
  const otherNames = get(apiData, 'other-names.other-name', [])
  const otherNamesContent = otherNames.map((name) => get(name, 'content', ''))

  const standardizedGivenName = standardize(givenName).split(' ')
  const standardizedFamilyName = standardize(familyName).split(' ')
  const standardizedCreditName = standardize(creditName).split(' ')
  const standardizedOtherNames = otherNamesContent.map((name) =>
    standardize(name).split(' '),
  )

  return union(
    standardizedGivenName,
    standardizedFamilyName,
    standardizedCreditName,
    ...standardizedOtherNames,
  )
}

const getAuthorNames = (givenNames, surname) =>
  `${standardize(givenNames)} ${standardize(surname)}`.split(' ')

const doesAuthorNameMatchApiName = (authorNames, orcidApiNames) =>
  authorNames.every((name) => orcidApiNames.includes(name))

const standardize = (string) => string.replace(/\W/g, ' ').toLowerCase().trim()

module.exports = {
  isAuthorValidOnOrcid,
}
