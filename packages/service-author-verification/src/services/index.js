const { isAuthorValidOnOrcid } = require('./orcidService')
const { findClosestROROrganization } = require('./rorService')

module.exports = {
  isAuthorValidOnOrcid,
  findClosestROROrganization,
}
