const initialize = ({ AuthorProfile }) => ({
  async execute(data) {
    const author = await AuthorProfile.findOneBy({
      queryObject: { primaryEmail: data.email },
    })

    if (!author) {
      const authorProfile = new AuthorProfile({
        primaryEmail: data.email,
        firstName: data.firstName,
        lastName: data.lastName,
      })
      return authorProfile.save()
    }
  },
})

module.exports = {
  initialize,
}
