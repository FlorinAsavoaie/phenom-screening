const { chain } = require('lodash')
const { Promise } = require('bluebird')

const WEIGHTS = {
  email: 10,
  lastName: 5,
  affiliation: 2,
  firstName: 1,
}

const initialize = ({
  models: { AuthorProfile },
  wosDataRepository,
  services,
}) => {
  return {
    async execute({ manuscriptId, authors = [] }) {
      const foundAuthorsEmails = await Promise.map(
        authors,
        async (author) => AuthorProfile.getAuthor(author.email),
        { concurrency: 10 },
      )
        .filter((author) => author)
        .map((author) => author.primaryEmail)

      const notFoundAuthors = authors.filter(
        ({ email }) => !foundAuthorsEmails.includes(email),
      )

      const wosAuthorsProfiles = await Promise.map(
        notFoundAuthors,
        async (author) => getWosAuthorProfiles(author),
        { concurrency: 10 },
      )

      const authorsWithRorScoreAndOrcidValidation = await Promise.reduce(
        authors,
        async (acc, author) => {
          const { rorId, rorAff, rorScore } =
            await services.findClosestROROrganization(author.aff)
          const hasOrcidIssue = await services.isAuthorValidOnOrcid(author)

          return {
            ...acc,
            [author.email]: {
              rorId,
              rorAff,
              rorScore,
              hasOrcidIssue,
            },
          }
        },
        {},
      )

      await applicationEventBus.publishMessage({
        event: 'AuthorProfilesSearchFinished',
        data: {
          manuscriptId,
          authors: getMappedAuthors(
            foundAuthorsEmails,
            wosAuthorsProfiles,
            authorsWithRorScoreAndOrcidValidation,
          ),
        },
      })
    },
  }

  async function getWosAuthorProfiles(author) {
    const parsedAuthor = {
      authorId: author.id,
      email: author.email || '',
      lastName: author.surname || '',
      firstName: author.givenNames || '',
      affiliation: author.aff || '',
    }

    const wosAuthorProfiles = await wosDataRepository.searchWoSAuthors(
      parsedAuthor,
    )

    const result = computePublications(wosAuthorProfiles)

    return {
      email: parsedAuthor.email,
      wosAuthorProfiles: chain(result)
        .map(addWeight(parsedAuthor))
        .orderBy('weight', 'desc')
        .slice(0, 20)
        .value(),
    }
  }
}

function computePublications(possibleProfilesFromWos) {
  // group by the 4 search criterias, resulting in an unique array containing each combination of criterias
  const uniquePossibleAuthors = chain(possibleProfilesFromWos)
    .uniqBy(
      (author) =>
        author.email + author.lastName + author.firstName + author.affiliation,
    )
    .value()

  // for each of the unique authors, compute what manuscripts they published, storing the result in the 'publications' array
  return uniquePossibleAuthors.map((uniqueAuthor) => {
    const publications = possibleProfilesFromWos
      .filter(
        (author) =>
          author.email === uniqueAuthor.email &&
          author.lastName === uniqueAuthor.lastName &&
          author.firstName === uniqueAuthor.firstName &&
          author.affiliation === uniqueAuthor.affiliation,
      )
      .map((author) => ({
        manuscriptWosId: author.manuscriptWosId,
        manuscriptDoi: author.manuscriptDoi,
      }))

    return {
      ...uniqueAuthor,
      publications,
      noOfPublications: publications.length,
    }
  })
}

function addWeight(notFoundAuthor) {
  return (wosAuthorProfile) => {
    let weight = 0
    if (wosAuthorProfile.email === notFoundAuthor.email) {
      weight += WEIGHTS.email
    }
    if (wosAuthorProfile.lastName === notFoundAuthor.lastName) {
      weight += WEIGHTS.lastName
    }

    if (wosAuthorProfile.affiliation === notFoundAuthor.affiliation) {
      weight += WEIGHTS.affiliation
    }

    if (wosAuthorProfile.firstName === notFoundAuthor.firstName) {
      weight += WEIGHTS.firstName
    }

    return {
      ...wosAuthorProfile,
      weight,
      authorId: notFoundAuthor.authorId,
    }
  }
}

function getMappedAuthors(
  foundAuthorsEmails,
  possibleWosProfilesAndEmail,
  authorsWithRorScoreAndOrcidValidation,
) {
  /**
   * @typedef {object} MappedAuthors
   * @property {boolean} isVerified
   * @property {array | null} profiles
   * @property {boolean} hasOrcidIssue
   *
   * @param foundAuthorsEmails - authors that are already verified
   * @param possibleWosProfilesAndEmail - authors found in wos
   * @param foundOrcidIssue - validation for author's orcid id against ORCiD API
   * @returns {Object.<string, MappedAuthors>} - will return an object with emails as keys and each email will
   * have as value an object with @typedef MappedAuthors
   *
   * @type {(
   * foundAuthorsEmails: string[],
   * possibleWosProfilesAndEmail: {email:string, wosAuthorProfiles: Object[]}[],
   * foundOrcidIssue: {email: boolean}[])
   * => MappedAuthors}
   */

  const authorProfiles = foundAuthorsEmails.reduce(
    (acc, email) => ({
      ...acc,
      [email]: {
        isVerified: true,
        profiles: null,
        hasOrcidIssue:
          authorsWithRorScoreAndOrcidValidation[email].hasOrcidIssue,
        ror: {
          id: authorsWithRorScoreAndOrcidValidation[email].rorId,
          aff: authorsWithRorScoreAndOrcidValidation[email].rorAff,
          score: authorsWithRorScoreAndOrcidValidation[email].rorScore,
        },
      },
    }),
    {},
  )

  const wosAuthorsProfiles = possibleWosProfilesAndEmail.reduce(
    (acc, { email, wosAuthorProfiles }) => ({
      ...acc,
      [email]: {
        isVerified: false,
        profiles: wosAuthorProfiles,
        hasOrcidIssue:
          authorsWithRorScoreAndOrcidValidation[email].hasOrcidIssue,
        ror: {
          id: authorsWithRorScoreAndOrcidValidation[email].rorId,
          aff: authorsWithRorScoreAndOrcidValidation[email].rorAff,
          score: authorsWithRorScoreAndOrcidValidation[email].rorScore,
        },
      },
    }),
    {},
  )

  return { ...authorProfiles, ...wosAuthorsProfiles }
}

module.exports = {
  initialize,
}
