const authorProfilesSearchUseCase = require('./authorProfilesSearch')
const createAuthorProfileUseCase = require('./createAuthorProfile')

module.exports = {
  authorProfilesSearchUseCase,
  createAuthorProfileUseCase,
}
