import { ServiceType } from '@hindawi/phenom-charts'
import { WithSopsSecretsServiceProps } from 'infrastructure-gaia/src/types'

const defaultValues: Partial<WithSopsSecretsServiceProps> = {
  serviceProps: {
    image: {
      repository:
        '496598730381.dkr.ecr.eu-west-1.amazonaws.com/service-author-verification',
      tag: 'latest',
    },
    replicaCount: 1,
    service: {
      port: 80,
      type: ServiceType.NODE_PORT,
    },
    containerPort: 3000,
    labels: {
      owner: 'QAlas',
    },
    livenessProbe: {
      path: '/',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    readinessProbe: {
      path: '/',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    startupProbe: {
      path: '/',
      failureThreshold: 30,
      periodSeconds: 10,
    },
  },
}

export { defaultValues }
