import { WithSopsSecretsServiceProps } from 'infrastructure-gaia/src/types'
import { defaultValues } from '../../default'

export const values: WithSopsSecretsServiceProps = {
  ...defaultValues,
  sopsSecrets: require('./prod-gsw.env.enc.json'),
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/service-author-verification',
      tag: 'latest',
    },
    ingressOptions: {
      rules: [
        {
          host: 'author-verification.gsw-prod.phenom.pub',
        },
      ],
    },
  },
}
