import { WithSopsSecretsServiceProps } from 'infrastructure-gaia/src/types'
import { defaultValues } from '../../default'

export const values: WithSopsSecretsServiceProps = {
  ...defaultValues,
  sopsSecrets: require('./qa-gamma.env.enc.json'),
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'service-author-verification.qa-gamma.phenom.pub',
        },
      ],
    },
  },
}
