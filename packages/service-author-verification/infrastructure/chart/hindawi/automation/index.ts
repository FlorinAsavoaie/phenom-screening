import { WithSopsSecretsServiceProps } from 'infrastructure-gaia/src/types'
import { defaultValues } from '../../default'

export const values: WithSopsSecretsServiceProps = {
  ...defaultValues,
  sopsSecrets: require('./automation.env.enc.json'),
  serviceProps: {
    ...defaultValues.serviceProps,
    ingressOptions: {
      rules: [
        {
          host: 'service-author-verification.automation.phenom.pub',
        },
      ],
    },
  },
}
