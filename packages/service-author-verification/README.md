# Author Verification Service

This service adds the ability to verify the identity of an author by searching the author info (email, first name, last
name, affiliation) into Web of Science

### Required Env vars

```sh
AWS_REGION=eu-west-1

AWS_PROFILE=HIndawiDevelopment

AWS_SNS_ENDPOINT=http://localhost:4566
AWS_SNS_TOPIC=test1

AWS_SQS_ENDPOINT=http://localhost:4566
AWS_SQS_QUEUE_NAME=author-verification-queue

DB_USER=****
DB_HOST=postgres

EVENT_NAMESPACE=****
PUBLISHER_NAME=hindawi

AWS_S3_ENDPOINT=.
PHENOM_LARGE_EVENTS_BUCKET=.
PHENOM_LARGE_EVENTS_PREFIX=.

WOS_ELASTICSEARCH_NODE=****
WOS_ELASTICSEARCH_INDEX=****
WOS_ELASTICSEARCH_USERNAME=****
WOS_ELASTICSEARCH_PASSWORD=****
```

### Not Required Env vars - default values

```sh
DATABASE=author_verification
DB_PASSWORD=''
PORT=3000

ORCID_SANDBOX_ENABLED=true #in order to use sandbox version of orcid
```
`AWS_S3_ACCESS_KEY` & `AWS_S3_SECRET_KEY` & `AWS_SNS_SQS_ACCESS_KEY` & `AWS_SNS_SQS_SECRET_KEY` are no longer needed in the .env file. \
Access keys are loaded from the Shared Credentials File (~/aws/credentials) for the AWS profile. This means that in order to work you need to be logged in with the Development AWS account.

# Config for docker-compose

```sh
DB_HOST=postgres
AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SNS_ENDPOINT=http://localstack:4566
```

# Config for yarn start

```sh
PORT=3006
AWS_SQS_ENDPOINT=http://localhost:4566
AWS_SNS_ENDPOINT=http://localhost:4566
```

### Starting the service

To start the service run `docker-compose up service-author-verification`.

### Secrets Management

The `author-verification-service` manages secrets in git, encrypted using SOPS. For more information please
visit [[Confluence] Config & Secrets Management](https://confluence.wiley.com/pages/viewpage.action?pageId=233511946)

#### Quick commands overview

- If you need to modify a value OR add an encrypted value  
  ⚠️ Always prefer this option

```shell
EDITOR=<editor> AWS_PROFILE=<aws-profile> sops <path-to-encrypted-file>
# EDITOR (optional) - specify which editor to use. By default SOPS uses vim. Most editors work with the --wait flag to make them keep the process open while you edit the file. E.g. "subl --wait" / "wstorm --wait"
# AWS_PROFILE - required so that it can access the KMS key specified in .sops.yaml
```

- If you want to add an unencrypted value, you will need to decrypt and re-encrypt specifying a new value for
  unencrypted-regex

```shell
AWS_PROFILE=<aws-profile> sops -d <path-to-encrypted-file>.enc.json > <path-to-unencrypted-file>.dec.json
# -d = decrypt

### Do your modifications

### Update <service>/.sops.yaml unencrypted_regex with the new values you want to keep unencrypted

AWS_PROFILE=<aws-profile> sops -e <path-to-unencrypted-file>.unenc.json > <path-to-encrypted-file>.enc.json
# -e = encrypt

### Delete the unencrypted file

### You need to have sops installed
brew install sops
```
