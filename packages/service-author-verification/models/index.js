module.exports = {
  models: [
    { model: require('./authorProfile'), modelName: 'AuthorProfile' },
    { model: require('./manuscriptProfile'), modelName: 'ManuscriptProfile' },
  ],
}
