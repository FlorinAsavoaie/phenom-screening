const BaseModel = require('./baseModel')

class ManuscriptProfile extends BaseModel {
  static get tableName() {
    return 'manuscript_profile'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        doi: { type: 'string' },
        mtsId: { type: 'string' },
        manuscriptTitle: { type: 'string' },
        journalTitle: { type: 'string' },
        authorName: { type: 'string' },
        authorEmailAddress: { type: 'string' },
        authorHindawiUniqueId: { type: 'string' },
        authorMtsId: { type: 'string' },
        authorOrcidId: { type: 'string' },
        authorScopusId: { type: 'string' },
        authorResearcherId: { type: 'string' },
        isCoAuthor: { type: 'boolean' },
        isEditor: { type: 'boolean' },
        publicationDate: { type: 'boolean' },
      },
    }
  }
}

module.exports = ManuscriptProfile
