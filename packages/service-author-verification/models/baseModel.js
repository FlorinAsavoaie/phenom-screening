const BaseModel = require('@pubsweet/base-model')

class ServiceBaseModel extends BaseModel {
  static async find(id, eagerLoadRelations) {
    const object = await this.query()
      .findById(id)
      .withGraphFetched(eagerLoadRelations)
    if (!object) {
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)
    }

    return object
  }

  static async findOneBy({ queryObject, eagerLoadRelations }) {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .withGraphFetched(eagerLoadRelations)

    if (!object.length) {
      return
    }

    return object[0]
  }

  static findBy(queryObject, eagerLoadRelations) {
    return this.query().where(queryObject).withGraphFetched(eagerLoadRelations)
  }

  static findIn(field, options, eagerLoadRelations) {
    return this.query()
      .whereIn(field, options)
      .withGraphFetched(eagerLoadRelations)
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  } = {}) {
    return this.query()
      .where(queryObject)
      .orderBy(orderByField, order)
      .withGraphFetched(eagerLoadRelations)
  }

  static async upsert(queryObject, entity) {
    const savedEntity = await this.findOneBy({
      queryObject,
    })

    if (savedEntity) {
      savedEntity.updateProperties(entity)
      return savedEntity.save()
    }

    return entity.save()
  }
}

module.exports = ServiceBaseModel
