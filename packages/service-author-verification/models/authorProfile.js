const BaseModel = require('./baseModel')

class AuthorProfile extends BaseModel {
  static get tableName() {
    return 'author_profile'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        hindawiUniqueId: { type: 'string' },
        mtsId: { type: 'string' },
        scopusId: { type: 'string' },
        researcherId: { type: 'string' },
        orcidId: { type: 'string' },
        title: { type: 'string' },
        firstName: { type: 'string' },
        lastName: { type: 'string' },
        primaryEmail: { type: 'string' },
        primaryAffiliation: { type: 'string' },
        country: { type: 'string' },
        secondaryEmail: { type: 'string' },
        secondaryAffiliation: { type: 'string' },
        isAuthor: { type: 'boolean' },
        isReviewer: { type: 'boolean' },
        isEditor: { type: 'boolean', default: false },
        pubRecordVerified: { type: 'boolean', default: false },
        revHistoryVerified: { type: 'boolean', default: false },
        accountCreatedDate: { type: ['string', 'object'], format: 'date-time' },
      },
    }
  }

  static async getAuthor(email) {
    return AuthorProfile.findOneBy({
      queryObject: { primaryEmail: email },
    })
  }
}

module.exports = AuthorProfile
