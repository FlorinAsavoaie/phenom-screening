/**
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***
 *
 * AWS_REGION
 * AWS_S3_ACCESS_KEY
 * AWS_S3_SECRET_KEY
 * AWS_S3_BUCKET
 *
 * SUSPICIOUS_KEYWORDS_PROVIDER_KEY
 * ```
 */

const config = require('config')
const csv = require('fast-csv')
const stream = require('stream')
const logger = require('@pubsweet/logger')
const Keyword = require('../models/keyword')
const s3Service = require('../src/services/s3Service')

exports.seed = async function seedKeywords() {
  const suspiciousKeywordsProviderKey = config.get(
    'providerKey.suspiciousKeywords',
  )
  logger.info('seeding database...')

  if (!suspiciousKeywordsProviderKey) {
    return logger.warn(
      'No SUSPICIOUS_KEYWORDS_PROVIDER_KEY specified! Skipping reading authors',
    )
  }

  const { Body: suspiciousKeywordsStream } = await s3Service.getObjectStream(
    suspiciousKeywordsProviderKey,
  )

  return new Promise((resolve) => {
    suspiciousKeywordsStream
      .on('error', logger.error)
      .pipe(csv.parse({ headers: true }))
      .pipe(createWritableStream())
      .on('finish', () => {
        resolve()
      })
  })
}

function createWritableStream() {
  return new stream.Writable({
    objectMode: true,
    write: async (chunk, _, callback) => {
      try {
        await addItemToDB(chunk)
        callback()
      } catch (e) {
        callback(e)
      }
    },
  })
}

async function addItemToDB(item) {
  const note = `${item.Reason}${item.Reason && item.Note ? '\n' : ''}${
    item.Note
  }`
  const keywordItem = new Keyword({
    keyword: item.Keyword,
    note: note || 'Not available.',
  })

  if (!keywordItem.keyword) return

  await Keyword.insertIfNotExists(keywordItem)
}
