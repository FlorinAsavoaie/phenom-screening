const logger = require('@pubsweet/logger')

exports.seed = async function cleanDatabase(knex) {
  const tableName = 'suspicious_keywords'

  await knex(tableName).del()

  logger.info('database was cleaned')
}
