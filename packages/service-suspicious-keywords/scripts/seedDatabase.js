const fs = require('fs')
const csv = require('fast-csv')
const logger = require('@pubsweet/logger')
const Keyword = require('../models/keyword')

seed()

async function seed() {
  await seedSuspiciousWords()
}

async function addItemToDB(item) {
  const keyword = new Keyword({
    keyword: item.keyword,
    note: `${item.reason}${item.reason && item.note ? '\n' : ''}${item.note}`,
  })

  await keyword.save()
}

async function seedSuspiciousWords() {
  logger.info('seeding database...')

  return new Promise((resolve, reject) => {
    const stream = fs
      .createReadStream('./seed.csv')
      .on('error', (err) => {
        logger.error(err.message)
        logger.info(
          'Create a file named seed.csv in the root of your service with the required seed data for suspicious keywords.',
        )
        reject(err)
      })
      .pipe(csv.parse({ headers: true }))

    stream
      .on('data', (data) => {
        stream.pause()
        addItemToDB(data)
          .then(() => stream.resume())
          .catch(reject)
      })
      .on('error', reject)
      .on('end', resolve)
  })
}
