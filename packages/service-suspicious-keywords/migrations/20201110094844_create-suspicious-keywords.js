const logger = require('@pubsweet/logger')

const SUSPICIOUS_KEYWORDS_TABLE = 'suspicious_keywords'

exports.up = async (knex) => {
  await knex.raw(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`)
  await knex.schema
    .createTable(SUSPICIOUS_KEYWORDS_TABLE, (table) => {
      table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(knex.fn.now())
      table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

      table.text('keyword').notNullable()
      table.unique('keyword')

      table.text('note').defaultTo('Not available.').notNullable()
    })
    .then((_) =>
      logger.info(`${SUSPICIOUS_KEYWORDS_TABLE} table created successfully`),
    )
    .catch((err) => logger.error(err))
}

exports.down = async (knex) =>
  knex.schema
    .dropTable(SUSPICIOUS_KEYWORDS_TABLE)
    .then((_) =>
      logger.info(`${SUSPICIOUS_KEYWORDS_TABLE} table dropped successfully`),
    )
    .catch((err) => logger.error(err))
