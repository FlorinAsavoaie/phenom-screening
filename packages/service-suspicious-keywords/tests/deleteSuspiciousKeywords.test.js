const { models } = require('../models')
const { deleteSuspiciousKeywordsUseCase } = require('../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const { Keyword } = models

describe('Delete suspicious keyword', () => {
  const keywordId = chance.guid()

  it('should delete keyword if exist in db', async () => {
    Keyword.deleteById = jest.fn().mockResolvedValue(1)

    await deleteSuspiciousKeywordsUseCase.initialize(models).execute({
      keywordId,
    })

    expect(Keyword.deleteById).toHaveBeenCalledTimes(1)
    expect(Keyword.deleteById).toHaveBeenCalledWith(keywordId)
  })
})
