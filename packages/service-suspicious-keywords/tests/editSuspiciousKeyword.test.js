const { models } = require('../models')
const { editSuspiciousKeywordsUseCase } = require('../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const { Keyword } = models

describe('Edit suspicious keyword', () => {
  const keywordId = chance.guid()
  const note = chance.paragraph()

  it('should edit keyword if exist in db', async () => {
    Keyword.updateBy = jest.fn().mockResolvedValue(1)

    await editSuspiciousKeywordsUseCase.initialize(models).execute({
      keywordId,
      note,
    })

    expect(Keyword.updateBy).toHaveBeenCalledTimes(1)
    expect(Keyword.updateBy).toHaveBeenCalledWith({
      queryObject: { id: keywordId },
      propertiesToUpdate: { note },
    })
  })
})
