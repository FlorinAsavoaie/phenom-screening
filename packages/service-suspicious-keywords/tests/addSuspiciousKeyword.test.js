const { models } = require('../models')
const { addSuspiciousKeywordUseCase } = require('../src/use-cases')

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

const mockSaveKeyword = jest.fn()
const keyword = 'test'
const factories = {
  keywordFactory: {
    createKeyword: jest.fn().mockReturnValue({
      keyword,
      save: mockSaveKeyword,
    }),
  },
}

describe('Add suspicious keywords use case', () => {
  it('should do suspicious keyword and note', async () => {
    jest
      .spyOn(models.Keyword, 'getUniqueKeyword')
      .mockReturnValueOnce(undefined)

    await addSuspiciousKeywordUseCase
      .initialize({
        models,
        factories,
      })
      .execute({ keyword })

    expect(factories.keywordFactory.createKeyword).toHaveBeenCalledTimes(1)
    expect(mockSaveKeyword).toHaveBeenCalledTimes(1)
  })
  it('should throw an error if keyword already exists', async () => {
    jest.spyOn(models.Keyword, 'getUniqueKeyword').mockReturnValueOnce(keyword)
    const result = addSuspiciousKeywordUseCase
      .initialize({
        models,
        factories,
      })
      .execute({ keyword })
    await expect(result).rejects.toThrow(`Keyword ${keyword} already exists.`)
  })
})
