const { models } = require('../models')
const { countSuspiciousKeywordsUseCase } = require('../src/use-cases')

const { Keyword } = models

const s3Service = {
  getObject: jest
    .fn()
    .mockReturnValue({ Body: 'prayer mosque Bon Jovi transgender text' }),
}

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

jest.mock('@pubsweet/logger', () => ({
  configure: jest.fn(),
}))

describe('Suspicious Keywords service', () => {
  it('should correctly count suspicious words', async () => {
    Keyword.getSuspiciousKeywords = jest.fn().mockReturnValue([
      {
        keyword: 'prayer',
        note: "some reason why this word shouldn't be here",
        count: 1,
      },
      {
        keyword: 'mosque',
        note: "some reason why this word shouldn't be here",
        count: 2,
      },
      {
        keyword: 'bon jovi',
        note: "some reason why this word shouldn't be here",
        count: 1,
      },
      {
        keyword: 'transgender',
        note: "some reason why this word shouldn't be here",
        count: 1,
      },
    ])

    await countSuspiciousKeywordsUseCase
      .initialize({ models, s3Service })
      .execute({
        text: 'A dummy transgender text with mosque and Bon Jovi who is living on a prayer in a mosque',
      })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith({
      event: 'SuspiciousWordsProcessingDone',
      data: {
        suspiciousWords: {
          prayer: {
            count: 1,
            reason: "some reason why this word shouldn't be here",
          },
          mosque: {
            count: 2,
            reason: "some reason why this word shouldn't be here",
          },
          'bon jovi': {
            count: 1,
            reason: "some reason why this word shouldn't be here",
          },
          transgender: {
            count: 1,
            reason: "some reason why this word shouldn't be here",
          },
        },
      },
    })
  })
})
