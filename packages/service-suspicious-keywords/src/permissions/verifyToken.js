const jwt = require('jsonwebtoken')
const jwksClient = require('jwks-rsa')
const config = require('config')

module.exports = verifyToken

async function verifyToken(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(
      token,
      getKey,
      {
        algorithms: ['RS256'],
      },
      (err, decoded) => {
        if (err || !decoded) {
          reject(err)
          return
        }

        resolve(decoded)
      },
    )
  })
}

async function getKey(header, callback) {
  const key = await getJwksClient().getSigningKey(header.kid)
  const signingKey = key.publicKey || key.rsaPublicKey
  callback(null, signingKey)
}

function getJwksClient() {
  return jwksClient({
    cache: true,
    jwksUri: config.get('keycloak.certs'),
  })
}
