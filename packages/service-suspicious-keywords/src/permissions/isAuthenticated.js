const { rule } = require('graphql-shield')
const verifyToken = require('./verifyToken')

const isAuthenticated = rule()(
  async (parent, args, { authorizationHeader }) => {
    const token = authorizationHeader.split(' ')[1]

    if (!token) {
      return new Error('Authentication token is required.')
    }

    const user = await verifyToken(token)

    return user !== null
  },
)

module.exports = isAuthenticated
