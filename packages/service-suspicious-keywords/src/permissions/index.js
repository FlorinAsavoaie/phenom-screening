const isAdmin = require('./isAdmin')
const isAuthenticated = require('./isAuthenticated')
const verifyToken = require('./verifyToken')

module.exports = {
  isAdmin,
  isAuthenticated,
  verifyToken,
}
