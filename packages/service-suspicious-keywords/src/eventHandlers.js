const { models } = require('../models')
const s3Service = require('./services/s3Service')

const useCases = require('./use-cases')

module.exports = {
  async FileConverted(data) {
    return useCases.countSuspiciousKeywordsUseCase
      .initialize({ models, s3Service })
      .execute(data)
  },
}
