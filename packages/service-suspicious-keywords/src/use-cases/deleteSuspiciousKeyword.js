function initialize({ Keyword }) {
  return { execute }

  async function execute({ keywordId }) {
    await Keyword.deleteById(keywordId)
  }
}

module.exports = {
  initialize,
}
