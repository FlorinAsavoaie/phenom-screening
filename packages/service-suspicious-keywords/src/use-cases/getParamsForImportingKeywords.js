function initialize({
  s3Service,
  uuidGenerator,
  configService,
  importService,
}) {
  async function execute() {
    const fileId = uuidGenerator.v4()
    const key = importService.getFileKey(fileId)

    const url = await s3Service.getSignedUrlForUpload(key)

    return {
      url,
      fileId,
    }
  }

  return { execute }
}

module.exports = {
  initialize,
}
