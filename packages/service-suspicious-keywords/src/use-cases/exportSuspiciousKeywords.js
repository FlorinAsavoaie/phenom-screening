function initialize({ models: { Keyword }, s3Service, config, exportService }) {
  return { execute }

  async function execute() {
    const exportedSuspiciousKeywordsProviderKey = config.get(
      'providerKey.exportedSuspiciousKeywords',
    )

    const numberOfExportedSuspiciousKeywords = await Keyword.countKeywords()

    await exportService.exportKeywords(exportedSuspiciousKeywordsProviderKey)

    const exportedSuspiciousKeywordsUrl = s3Service.getSignedUrl(
      exportedSuspiciousKeywordsProviderKey,
    )

    return {
      url: exportedSuspiciousKeywordsUrl,
      totalNumber: numberOfExportedSuspiciousKeywords,
    }
  }
}

module.exports = {
  initialize,
}
