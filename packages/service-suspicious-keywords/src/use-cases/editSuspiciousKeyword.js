function initialize({ Keyword }) {
  return { execute }

  async function execute({ keywordId, note }) {
    await Keyword.updateBy({
      queryObject: { id: keywordId },
      propertiesToUpdate: { note },
    })
  }
}

module.exports = {
  initialize,
}
