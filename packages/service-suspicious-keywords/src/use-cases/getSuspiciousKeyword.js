function initialize({ Keyword }) {
  async function execute({ keywordId }) {
    return Keyword.find(keywordId)
  }

  return { execute }
}

module.exports = {
  initialize,
}
