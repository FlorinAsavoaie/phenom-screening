function initialize({
  models: { Keyword },
  services: { importService, exportService, configService, logger },
}) {
  async function execute({ fileId }) {
    try {
      logger.info(`Validating CSV file ${fileId} ...`)
      importService.setImportStatus('VALIDATING')
      await importService.validateKeywords(fileId)

      logger.info(`Creating backup for ${fileId} file...`)
      importService.setImportStatus('BACKUPING')
      const backupFileKey = importService.getBackupFileKey(fileId)
      await exportService.exportKeywords(backupFileKey)

      logger.info(`Fetching suspicious keywords from ${fileId} file...`)
      importService.setImportStatus('PROCESSING')
      const keywords = await importService.getImportedKeywords(fileId)

      logger.info(`Importing suspicioys keywords from ${fileId} file...`)
      importService.setImportStatus('IMPORTING')
      await Keyword.insertOrUpdate(
        keywords,
        configService.get('import.batchSize'),
      )

      logger.info(
        `Successfully imported ${keywords.length} keywords from ${fileId} file.`,
      )
      importService.setImportStatus('FINISHED', {
        keywordsLength: keywords.length,
      })
    } catch (e) {
      importService.setImportStatus('ERROR', { error: e })
    }
  }

  return { execute }
}

module.exports = {
  initialize,
}
