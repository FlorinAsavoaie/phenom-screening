const initialize = ({ Keyword }) => {
  return {
    execute,
  }
  async function execute({ page = 0, pageSize = 20, searchValue = '' }) {
    return Keyword.findPaginatedKeywords({
      page,
      pageSize,
      searchValue,
    })
  }
}

module.exports = {
  initialize,
}
