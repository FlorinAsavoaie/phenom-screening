const { ValidationError } = require('apollo-server')

const initialize = ({ models: { Keyword }, factories: { keywordFactory } }) => {
  return {
    execute,
  }
  async function execute({ keyword, note }) {
    const uniqueKeyword = await Keyword.getUniqueKeyword({ keyword })

    if (uniqueKeyword) {
      throw new ValidationError(`Keyword ${keyword} already exists.`)
    }
    const suspiciousKeyword = keywordFactory.createKeyword({ keyword, note })
    await suspiciousKeyword.save()
  }
}

module.exports = {
  initialize,
}
