const { escape, escapeRegExp } = require('lodash')

function initialize({ models: { Keyword }, s3Service }) {
  return { execute }

  async function execute({ textKey, manuscriptId, text = '' }) {
    if (textKey) {
      const object = await s3Service.getObject(textKey)
      text = await streamToString(object.Body)
    }

    const escapedText = escape(text)
    const words = await Keyword.getSuspiciousKeywords(escapedText)
    const suspiciousWords = words.reduce((obj, item) => {
      const nrOfWords = countSuspiciousWords(text, item.keyword)
      if (nrOfWords > 0) {
        return Object.assign(obj, {
          [item.keyword]: {
            reason: item.note,
            count: nrOfWords,
          },
        })
      }
      return obj
    }, {})

    await applicationEventBus.publishMessage({
      event: 'SuspiciousWordsProcessingDone',
      data: {
        manuscriptId,
        suspiciousWords,
      },
    })
  }
}

function countSuspiciousWords(text, word) {
  const wordWithoutEscapeCharacters = escapeRegExp(word)
  const regex = new RegExp(
    `(?:^|\\W)${wordWithoutEscapeCharacters}(?:$|\\W)`,
    'gi',
  )
  return (text.match(regex) || []).length
}

const streamToString = (stream) =>
  new Promise((resolve, reject) => {
    const chunks = []
    stream.on('data', (chunk) => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
  })

module.exports = {
  initialize,
}
