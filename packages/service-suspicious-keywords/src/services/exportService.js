function initialize({ models: { Keyword }, s3Service, csvService, logger }) {
  async function exportKeywords(providerKey) {
    const keywords = await Keyword.getAllKeywords()
    const parsedKeywords = keywords.map((k) => ({
      Keyword: k.keyword,
      'Reason & Recommendations': k.note,
    }))

    const { writeStream, writeStreamPromise } =
      s3Service.uploadFromStream(providerKey)
    csvService
      .writeToStream(writeStream, parsedKeywords, { headers: true })
      .on('finish', () => {
        logger.info(`Suspicious keywords exported to CSV file successfully.`)
      })
    try {
      const result = await writeStreamPromise
      logger.info(`File ${result.Key} uploaded to s3.`)
    } catch (error) {
      throw new Error(error)
    }
  }

  return {
    exportKeywords,
  }
}

module.exports = {
  initialize,
}
