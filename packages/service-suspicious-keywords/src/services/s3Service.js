const { S3, GetObjectCommand, PutObjectCommand } = require('@aws-sdk/client-s3')
const { Upload } = require('@aws-sdk/lib-storage')
const { getSignedUrl } = require('@aws-sdk/s3-request-presigner')
const config = require('config')
const stream = require('stream')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

const bucket = config.get('aws.s3.bucket')

module.exports.getObject = (key) => s3.getObject({ Key: key, Bucket: bucket })

module.exports.headObject = (key) =>
  s3.headObject({ Key: key, Bucket: bucket }).then(
    (result) => result,
    (err) => {
      if (err.code === 'NotFound') {
        return null
      }
      throw err
    },
  )

module.exports.getObjectStream = (key) =>
  s3.getObject({ Key: key, Bucket: bucket })

module.exports.uploadFromStream = (fileName) => {
  const streamData = new stream.PassThrough()

  const uploadParams = {
    Key: fileName,
    Body: streamData,
    Bucket: bucket,
  }

  return {
    writeStream: streamData,
    writeStreamPromise: upload(uploadParams),
  }
}

module.exports.getSignedUrl = async (key) =>
  getSignedUrl(
    s3,
    new GetObjectCommand({
      Key: key,
      Bucket: bucket,
    }),
    {
      expiresIn: 3600,
    },
  )

async function upload(uploadParams) {
  const upload = new Upload({
    params: uploadParams,
    client: s3,
  })
  return upload.done()
}

module.exports.getSignedUrlForUpload = async (key) =>
  getSignedUrl(
    s3,
    new PutObjectCommand({
      Key: key,
      Bucket: bucket,
    }),
    {
      expiresIn: 3600,
    },
  )
