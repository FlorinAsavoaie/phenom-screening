const { Promise } = require('bluebird')
const { ValidationError } = require('apollo-server')
const { isEqual } = require('lodash')

function initialize({ s3Service, csvService, configService }) {
  let importStatus = null

  const CSV_HEADERS = ['Keyword', 'Reason & Recommendations']

  async function validateKeywords(fileId) {
    const key = getFileKey(fileId)

    const { Body: keywordsStream } = await s3Service.getObjectStream(key)

    return new Promise((resolve, reject) => {
      const keywordsSet = new Set()

      keywordsStream
        .on('error', reject)
        .pipe(csvService.parse(getParserOptions(reject)))
        .on('error', reject)
        .transform(csvRowToKeyword)
        .validate(validateKeyword(keywordsSet))
        .on('data-invalid', (_, rowNumber, reason) => {
          if (reason) {
            return reject(
              new ValidationError(
                `${reason} at row ${rowNumber}. Please select a different file`,
              ),
            )
          }

          return reject(
            new ValidationError(
              `The CSV file contains invalid formatting at row ${rowNumber}. Please select a different file`,
            ),
          )
        })
        .on('data', async (data) => {
          keywordsSet.add(data.keyword)
        })
        .on('finish', async () => {
          if (keywordsSet.size === 0) {
            reject(
              new ValidationError(
                'The CSV file is empty. Please select a different file',
              ),
            )
          }

          resolve()
        })
    })
  }

  function getParserOptions(reject) {
    return {
      headers: (headers) => {
        if (isEqual(CSV_HEADERS, headers)) {
          return headers
        }
        reject(
          new ValidationError(
            'The headers of this CSV file have invalid formatting. Please select a different file',
          ),
        )
      },
      strictColumnHandling: true,
    }
  }

  function getFileKey(fileId) {
    const fileLocation = configService.get('import.fileLocation')
    return `${fileLocation}/${fileId}`
  }

  function getBackupFileKey(fileId) {
    const key = getFileKey(fileId)
    return `${key}-backup`
  }

  function csvRowToKeyword(csvRow) {
    return {
      keyword: csvRow.Keyword,
      note: csvRow['Reason & Recommendations'],
    }
  }

  function validateKeyword(keywordsSet) {
    return (data, cb) => {
      const noteIsValid = !!data.note
      if (!noteIsValid) {
        return cb(null, false, 'The column "Reason & Recommendations" is empty')
      }

      const keywordIsValid = !!data.keyword
      if (!keywordIsValid) {
        return cb(null, false, 'The column "Keyword" is empty')
      }

      if (keywordsSet.has(data.keyword)) {
        return cb(null, false, 'The column "Keyword" contains a duplicate')
      }

      return cb(null, true)
    }
  }

  async function getImportedKeywords(fileId) {
    const key = getFileKey(fileId)
    const { Body: keywordsStream } = await s3Service.getObjectStream(key)

    return new Promise((resolve, reject) => {
      const keywords = []

      keywordsStream
        .on('error', reject)
        .pipe(csvService.parse(getParserOptions(reject)))
        .on('error', reject)
        .transform(csvRowToKeyword)
        .on('data', async (data) => {
          keywords.push(data)
        })
        .on('finish', async () => {
          resolve(keywords)
        })
    })
  }

  function getImportStatus() {
    const currentImportStatus = importStatus

    if (
      currentImportStatus &&
      (currentImportStatus.percentage === 100 || currentImportStatus.error)
    ) {
      importStatus = null
    }

    if (currentImportStatus && currentImportStatus.error) {
      throw currentImportStatus.error
    }

    return currentImportStatus
  }

  function setImportStatus(newStatus, data) {
    switch (newStatus) {
      case 'VALIDATING':
        importStatus = {
          percentage: 20,
          message: 'File uploaded. Validating CSV file...',
        }
        break
      case 'BACKUPING':
        importStatus = {
          percentage: 40,
          message: 'File uploaded. Creating backup file...',
        }
        break
      case 'PROCESSING':
        importStatus = {
          percentage: 60,
          message: 'File uploaded. Processing keywords for import...',
        }
        break
      case 'IMPORTING':
        importStatus = {
          percentage: 80,
          message: 'File uploaded. Importing keywords...',
        }
        break
      case 'FINISHED':
        importStatus = {
          percentage: 100,
          message: `You have successfully imported ${data.keywordsLength} keywords.`,
        }
        break
      case 'ERROR':
        importStatus = {
          error: data.error,
        }
        break

      default:
        throw new Error('Invalid import status')
    }
  }

  return {
    validateKeywords,
    getImportedKeywords,
    getImportStatus,
    getFileKey,
    getBackupFileKey,
    setImportStatus,
  }
}

module.exports = {
  initialize,
}
