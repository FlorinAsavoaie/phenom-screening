const useCases = require('./use-cases')
const { models, factories } = require('../models')
const config = require('config')
const logger = require('@pubsweet/logger')
const csvService = require('fast-csv')
const s3Service = require('./services/s3Service')
const services = require('./services')
const uuid = require('uuid')

const importService = services.importService.initialize({
  s3Service,
  csvService,
  configService: config,
})
const exportService = services.exportService.initialize({
  models,
  s3Service,
  csvService,
  logger,
})

const resolvers = {
  Query: {
    async getSuspiciousKeywords(_, params) {
      return useCases.getSuspiciousKeywordsUseCase
        .initialize(models)
        .execute(params)
    },
    async getSuspiciousKeyword(_, params) {
      return useCases.getSuspiciousKeywordUseCase
        .initialize(models)
        .execute(params)
    },
    async getParamsForImportingKeywords() {
      return useCases.getParamsForImportingKeywordsUseCase
        .initialize({
          s3Service,
          uuidGenerator: uuid,
          configService: config,
          importService,
        })
        .execute()
    },
    async getImportStatus() {
      return importService.getImportStatus()
    },
  },

  Mutation: {
    async addSuspiciousKeyword(_, params) {
      return useCases.addSuspiciousKeywordUseCase
        .initialize({ models, factories })
        .execute(params)
    },
    async deleteSuspiciousKeyword(_, params) {
      return useCases.deleteSuspiciousKeywordsUseCase
        .initialize(models)
        .execute(params)
    },
    async editSuspiciousKeyword(_, params) {
      return useCases.editSuspiciousKeywordsUseCase
        .initialize(models)
        .execute(params)
    },
    async exportSuspiciousKeywords(_, params) {
      return useCases.exportSuspiciousKeywordsUseCase
        .initialize({ models, s3Service, config, exportService })
        .execute()
    },
    async importSuspiciousKeywords(_, params) {
      useCases.importSuspiciousKeywordsUseCase
        .initialize({
          models,
          services: {
            importService,
            exportService,
            configService: config,
            logger,
          },
        })
        .execute(params)
    },
  },
}

module.exports = resolvers
