const BaseModel = require('./baseModel')
const { Promise } = require('bluebird')

class Keyword extends BaseModel {
  static get tableName() {
    return 'suspicious_keywords'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'string' },
        created: { type: ['string', 'object'] },
        updated: { type: ['string', 'object'] },
        keyword: { type: 'string' },
        note: { type: 'string', default: 'Not available.' },
      },
    }
  }

  static async findPaginatedKeywords({ page, pageSize, searchValue }) {
    return this.query()
      .select('id', 'keyword', 'note', 'created')
      .whereRaw('lower(keyword) LIKE ?', [`%${searchValue.toLowerCase()}%`])
      .orderBy('keyword', 'asc')
      .page(page, pageSize)
  }

  static async getUniqueKeyword({ keyword }) {
    return Keyword.findOneBy({
      queryObject: (builder) => {
        if (keyword) {
          builder = builder.whereRaw('lower(keyword) = ?', [
            keyword.toLowerCase(),
          ])
        }

        return builder
      },
    })
  }

  static async insertIfNotExists(keywordItem) {
    const keywordFromDB = await Keyword.findOneBy({
      queryObject: { keyword: keywordItem.keyword },
    })

    if (!keywordFromDB) {
      await keywordItem.save()
    }
  }

  static async insertOrUpdate(keywords, batchSize) {
    const trx = await Keyword.startTransaction()

    try {
      const timestamp = new Date().toISOString()
      const keywordsBatches = keywords.reduce(
        (rows, key, index) =>
          (index % batchSize === 0
            ? rows.push([key])
            : rows[rows.length - 1].push(key)) && rows,
        [],
      )

      await Promise.each(keywordsBatches, async (batch) =>
        Keyword._insertOrUpdateBatch(batch, timestamp, trx),
      )

      await trx.commit()
    } catch (err) {
      await trx.rollback()
      throw err
    }
  }

  static async _insertOrUpdateBatch(keywords, timestamp, trx) {
    const created = timestamp
    const updated = timestamp

    const values = Array(keywords.length).fill('(?,?,?,?)').join(',')
    const bindings = keywords.reduce(
      (currentBindings, keyword) => [
        ...currentBindings,
        ...[created, updated, keyword.keyword, keyword.note],
      ],
      [],
    )

    await Keyword.knex()
      .raw(
        `insert into "suspicious_keywords" ("created",  "updated", "keyword", "note")
      values ${values} 
      on conflict ("keyword") 
      do update set "note" = excluded."note", "updated" = excluded."updated"
      `,
        bindings,
      )
      .transacting(trx)
  }

  static async getAllKeywords() {
    return this.query()
      .select('keyword', 'note', 'created')
      .orderBy('keyword', 'asc')
  }

  static async getLatestAddedKeyword() {
    return this.query().orderBy('created', 'desc').first()
  }

  static async getSuspiciousKeywords(text) {
    // tsvector() @@ tsquery() searches for keywords in text
    const { rows: keywords } = await this.knex()
      .raw(`select *  from suspicious_keywords 
      where (to_tsvector('simple', '${text}') @@ phraseto_tsquery('simple', keyword))
        `)
    return keywords
  }

  static async countKeywords() {
    return this.query().resultSize()
  }
}

module.exports = Keyword
