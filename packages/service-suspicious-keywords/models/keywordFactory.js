const Keyword = require('./keyword')

module.exports = {
  createKeyword({ keyword, note }) {
    return new Keyword({ keyword, note })
  },
}
