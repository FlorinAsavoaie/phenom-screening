const keywordFactory = require('./keywordFactory')
const Keyword = require('./keyword')

module.exports = {
  models: { Keyword },
  factories: { keywordFactory },
}
