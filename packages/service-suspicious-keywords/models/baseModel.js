const { Model, transaction } = require('objection')
const uuid = require('uuid')
const logger = require('@pubsweet/logger')
const db = require('./dbConnection')

Model.knex(db)

class BaseModel extends Model {
  constructor(properties) {
    super()
    if (properties) {
      this._updateProperties(properties)
    }
  }

  $beforeInsert() {
    this.id = this.id || uuid.v4()
    this.created = new Date().toISOString()
    this.updated = this.created
  }

  $beforeUpdate() {
    this.updated = new Date().toISOString()
  }

  saveGraph(opts = {}) {
    const updateAndFetch = (graph, trx) =>
      this.constructor
        .query(trx)
        .upsertGraphAndFetch(
          graph,
          Object.assign({ insertMissing: true, noDelete: true }, opts),
        )

    const insertAndFetch = (graph, trx) =>
      this.constructor.query(trx).insertGraphAndFetch(graph, opts)

    return this._save(insertAndFetch, updateAndFetch)
  }

  save(trx) {
    const updateAndFetch = (instance, trx) =>
      this.constructor.query(trx).patchAndFetchById(instance.id, instance)

    const insertAndFetch = (instance, trx) =>
      this.constructor.query(trx).insertAndFetch(instance)

    return this._save(insertAndFetch, updateAndFetch, trx)
  }

  async _save(insertAndFetch, updateAndFetch, trx) {
    const simpleSave = (trx = null) => updateAndFetch(this, trx)

    const protectedSave = async (trx) => {
      const current = await this.constructor
        .query(trx)
        .findById(this.id)
        .forUpdate()

      const storedUpdateTime = new Date(current.updated).getTime()
      const instanceUpdateTime = new Date(this.updated).getTime()

      if (instanceUpdateTime < storedUpdateTime) {
        throw new Error(
          `Data Integrity Error property 'updated' set to ${this.updated} is older than the one stored in the database!`,
        )
      }

      return simpleSave(trx)
    }

    // start of save function...
    let result
    // Do the validation manually here, since inserting
    // model instances skips validation, and using toJSON() first will
    // not save certain fields ommited in $formatJSON (e.g. passwordHash)
    this.$validate()

    const saveFn = async (trx) => {
      let savedEntity

      // If an id is set on the model instance, find out if the instance
      // already exists in the database
      if (this.id) {
        if (!this.updated && this.created) {
          throw new Error(
            `Data Integrity Error property 'updated' set to ${this.updated}: must be set when created is set!`,
          )
        }
        if (!this.updated && !this.created) {
          savedEntity = await simpleSave(trx)
        } else {
          // If the model instance has created/updated times set, provide
          // protection against potentially overwriting newer data in db.
          savedEntity = await protectedSave(trx)
        }
      }
      // If it doesn't exist, simply insert the instance in the database
      if (!savedEntity) {
        savedEntity = await insertAndFetch(this, trx)
      }
      return savedEntity
    }

    try {
      if (trx) {
        result = await saveFn(trx)
      } else {
        result = await transaction(Model.knex(), saveFn)
      }
      logger.info(`Saved ${this.constructor.name} with UUID ${result.id}`)
    } catch (err) {
      logger.info(`Rolled back ${this.constructor.name}`)
      logger.error(err)
      throw err
    }

    return result
  }

  async delete() {
    const deleted = await this.$query().delete()
    logger.info(`Deleted ${deleted} ${this.constructor.name} records.`)
    return this
  }

  // A private method that you shouldn't override
  _updateProperties(properties) {
    Object.keys(properties).forEach((prop) => {
      this[prop] = properties[prop]
    })
    return this
  }

  updateProperties(properties) {
    return this._updateProperties(properties)
  }

  static async find(id, eagerLoadRelations) {
    const object = await this.query()
      .findById(id)
      .withGraphFetched(eagerLoadRelations)

    if (!object) {
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)
    }

    return object
  }

  static async findOneBy({ queryObject, eagerLoadRelations }) {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .withGraphFetched(eagerLoadRelations)

    if (!object.length) {
      return
    }

    return object[0]
  }

  static findBy(queryObject, eagerLoadRelations) {
    return this.query().where(queryObject).withGraphFetched(eagerLoadRelations)
  }

  static findIn(field, options, eagerLoadRelations) {
    return this.query()
      .whereIn(field, options)
      .withGraphFetched(eagerLoadRelations)
  }

  static async upsertGraph(object, options) {
    return this.knex().transaction(async (trx) =>
      this.query(trx).upsertGraph(object, options),
    )
  }

  static async updateBy({ queryObject, propertiesToUpdate }) {
    await this.query().patch(propertiesToUpdate).where(queryObject)
  }

  static async deleteById(id) {
    return this.query().delete().where('id', id)
  }
}

module.exports = BaseModel
