# Service Suspicious Keywords

This service adds the ability to check if the provided text contains suspicious keywords and counts the number of
occurrences; the service also is responsible for managing the suspicious keywords list

The service listens to the following events:

- `FileConverted`

After checking the suspicious keywords, this service triggers the `SuspiciousWordsProcessingDone` event with the
following format

```
{
      event: 'SuspiciousWordsProcessingDone',
      data: {
        manuscriptId,
        suspiciousWords,
      },
}
```

### Required Env vars

```sh
AWS_S3_BUCKET=screening-sandbox-eu-west-1

AWS_REGION=eu-west-1

AWS_PROFILE=HindawiDevelopment

AWS_SNS_ENDPOINT=http://localstack:4566
AWS_SNS_TOPIC=test1

AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SQS_QUEUE_NAME=suspicious-keywords-queue

EVENT_NAMESPACE=pen
PUBLISHER_NAME=hindawi

AWS_S3_ENDPOINT=.
PHENOM_LARGE_EVENTS_BUCKET=.
PHENOM_LARGE_EVENTS_PREFIX=.

SUSPICIOUS_KEYWORDS_PROVIDER_KEY=****

DB_USER=****
DB_HOST=postgres

KEYCLOAK_CERTIFICATES=****
SSO_CLIENT_ID=****

# REGISTRY_URL_FOR_ENVIRONMENT, example for local environment
SCHEMA_REGISTRY_URL='http://host.docker.internal:6001'
```

### Not Required Env vars - default values

```sh
DATABASE=suspicious_keywords
DB_PASSWORD=''

# url to the service graphql schema, needed for schema registry push, example for local environment
SERVICE_SCHEMA_URL='http://host.docker.internal:3004/graphql'
```

`AWS_S3_ACCESS_KEY` & `AWS_S3_SECRET_KEY` & `AWS_SNS_SQS_ACCESS_KEY` & `AWS_SNS_SQS_SECRET_KEY` are no longer needed in the .env file. \
Access keys are loaded from the Shared Credentials File (~/aws/credentials) for the AWS profile. This means that in order to work you need to be logged in with the Development AWS account.

# Config for docker-compose

```sh
DB_HOST=postgres
AWS_SQS_ENDPOINT=http://localstack:4566
AWS_SNS_ENDPOINT=http://localstack:4566
SCHEMA_REGISTRY_URL=http://host.docker.internal:6001
```

# Config for yarn start

```sh
PORT=3004
AWS_SNS_ENDPOINT=http://localhost:4566
AWS_SQS_ENDPOINT=http://localhost:4566
SCHEMA_REGISTRY_URL=http://localhost:6001
```

### Starting the service

To start the service run `docker-compose up service-suspicious-keywords`

### Run Scripts

To create migration run `yarn make-migration migration-name`
To create seeds run `yarn make-seed seed-name`

To run migration `yarn migrate`
To run seeds `yarn seed-db`
