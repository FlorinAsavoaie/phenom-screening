import { WithAwsSecretsServiceProps, ServiceType } from '@hindawi/phenom-charts'

const defaultValues: WithAwsSecretsServiceProps = {
  secretNames: [],
  serviceProps: {
    image: {
      repository:
        '496598730381.dkr.ecr.eu-west-1.amazonaws.com/service-suspicious-keywords',
      tag: 'latest',
    },
    replicaCount: 1,
    service: {
      port: 80,
      type: ServiceType.NODE_PORT,
    },
    containerPort: 3000,
    labels: {
      owner: 'QAlas',
    },
    resources: {
      requests: {
        memory: '800Mi',
      },
    },
    livenessProbe: {
      path: '/.well-known/apollo/server-health',
      periodSeconds: 20,
      initialDelaySeconds: 10,
      failureThreshold: 5,
    },
    readinessProbe: {
      path: '/.well-known/apollo/server-health',
      periodSeconds: 20,
      initialDelaySeconds: 10,
      failureThreshold: 5,
    },
    startupProbe: {
      path: '/.well-known/apollo/server-health',
      failureThreshold: 30,
      periodSeconds: 10,
    },
  },
}

export { defaultValues }
