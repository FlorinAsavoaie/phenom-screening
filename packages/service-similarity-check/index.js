require('dotenv').config()
const config = require('config')
const { forEach } = require('lodash')
const { createServer } = require('http')
const { createQueueService } = require('@hindawi/queue-service')
const logger = require('./config/loggerCustom')

const port = config.get('port')
const server = createServer((req, res) => {
  res.end('Service Similarity Check is UP')
})

function registerEventHandlers(messageQueue) {
  const handlers = require('./src/eventHandlers')

  forEach(handlers, (handler, event) =>
    messageQueue.registerEventHandler({ event, handler }),
  )
}

;(async function main() {
  const messageQueue = await createQueueService(
    {
      region: config.get('aws.region'),
      accessKeyId: config.has('aws.sns_sqs.accessKeyId')
        ? config.get('aws.sns_sqs.accessKeyId')
        : undefined,
      secretAccessKey: config.has('aws.sns_sqs.secretAccessKey')
        ? config.get('aws.sns_sqs.secretAccessKey')
        : undefined,
      snsEndpoint: config.has('aws.sns.endpoint')
        ? config.get('aws.sns.endpoint')
        : undefined,
      sqsEndpoint: config.has('aws.sqs.endpoint')
        ? config.get('aws.sqs.endpoint')
        : undefined,
      s3Endpoint: config.has('largeEvents.s3Endpoint')
        ? config.get('largeEvents.s3Endpoint')
        : undefined,
      topicName: config.get('aws.sns.topic'),
      queueName: config.get('aws.sqs.queueName'),
      bucketName: config.get('largeEvents.bucketName'),
      bucketPrefix: config.get('largeEvents.bucketPrefix'),
      eventNamespace: config.get('eventNamespace'),
      publisherName: config.get('publisherName'),
      serviceName: config.get('serviceName'),
      defaultMessageAttributes: config.get('defaultMessageAttributes'),
    },
    logger,
  )

  registerEventHandlers(messageQueue)

  global.applicationEventBus = messageQueue

  messageQueue.start()

  server.listen(port, () => {
    logger.info(`Service Similarity Check listening on port ${port}.`)
  })
})()
