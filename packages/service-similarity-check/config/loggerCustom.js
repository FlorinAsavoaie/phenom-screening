const { initializeRootLogger } = require('hindawi-utils')

module.exports = initializeRootLogger('ServiceSimilarityCheck')
