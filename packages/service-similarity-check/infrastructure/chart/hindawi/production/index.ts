import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod/screening/similarity-check'],
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/service-similarity-check',
      tag: 'latest',
    },
    replicaCount: 4,
    resources: {
      requests: {
        memory: '800Mi',
      },
      limits: {
        memory: '800Mi',
      },
    },
    ingressOptions: {
      rules: [
        {
          host: 'similarity-check.prod.phenom.pub',
        },
      ],
    },
  },
}
