import { WithAwsSecretsServiceProps, ServiceType } from '@hindawi/phenom-charts'

const defaultValues: WithAwsSecretsServiceProps = {
  secretNames: [],
  serviceProps: {
    image: {
      repository:
        '496598730381.dkr.ecr.eu-west-1.amazonaws.com/service-similarity-check',
      tag: 'latest',
    },
    replicaCount: 1,
    service: {
      port: 80,
      type: ServiceType.NODE_PORT,
    },
    livenessProbe: {
      path: '/',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    readinessProbe: {
      path: '/',
      periodSeconds: 10,
      initialDelaySeconds: 10,
      failureThreshold: 3,
    },
    startupProbe: {
      path: '/',
      failureThreshold: 30,
      periodSeconds: 10,
    },
    containerPort: 3000,
    labels: {
      owner: 'QAlas',
    },
  },
}

export { defaultValues }
