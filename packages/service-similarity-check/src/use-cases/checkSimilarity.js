const logger = require('../../config/loggerCustom')

const initialize = ({ s3Service, similarityService, config }) => ({
  async execute({ manuscriptId, textKey, convertedFileProviderKey }) {
    if (!config.get('similarityCheckServiceEnabled')) {
      logger.info(
        `Similarity check is skipped for manuscript: ${manuscriptId}.`,
      )
      return
    }

    const MAX_FILE_SIZE = 100 // mb

    let manuscriptFileBuffer

    const pdfFileMetadata = await s3Service.headObject(convertedFileProviderKey)

    if (parseFileSize(pdfFileMetadata.ContentLength) < MAX_FILE_SIZE) {
      manuscriptFileBuffer = await s3Service.getObjectBuffer(
        convertedFileProviderKey,
      )
    } else {
      manuscriptFileBuffer = await s3Service.getObjectBuffer(textKey)
    }

    const {
      totalSimilarityPercentage,
      firstSourceSimilarityPercentage,
      reportUrl,
    } = await similarityService.getSimilarityPercentage(
      manuscriptId,
      manuscriptFileBuffer,
    )

    await applicationEventBus.publishMessage({
      event: 'SimilarityCheckFinished',
      data: {
        manuscriptId,
        totalSimilarityPercentage,
        firstSourceSimilarityPercentage,
        reportUrl,
      },
    })
  },
})
const parseFileSize = (size) => size / 1000000

module.exports = {
  initialize,
}
