const config = require('config')
const xmlrpc = require('xmlrpc-es6-promise')

const CHECK_PROCESS_INTERVAL = 3000

const rpcClient = xmlrpc.createSecureClient('https://api.ithenticate.com/rpc')

const getSimilarityPercentage = async (manuscriptId, manuscriptFileBuffer) => {
  const sid = await loginIthenticate()
  const folder = await getIthenticateFolder(sid)
  const uploaded = await uploadDocumentToIthenticate({
    sid,
    folder,
    manuscriptId,
    manuscriptFileBuffer,
  })

  return getIthenticateResults({ sid, uploaded })
}

const loginIthenticate = async () => {
  const { sid } = await rpcClient.methodCall('login', [
    {
      username: config.get('ithenticate.username'),
      password: config.get('ithenticate.password'),
    },
  ])

  if (!sid) throw new Error('Could not login to iThenticate')

  return sid
}

const getIthenticateFolder = async (sid) => {
  const { folders } = await rpcClient.methodCall('folder.list', [
    {
      sid,
    },
  ])

  const folder = folders.find(
    (f) => f.name === config.get('ithenticate.folder'),
  )

  if (!folder)
    throw new Error(
      `Could not find ithenticate folder ${config.get('ithenticate.folder')}`,
    )

  return folder
}

const uploadDocumentToIthenticate = async ({
  sid,
  folder,
  manuscriptFileBuffer,
  manuscriptId,
}) => {
  const result = await rpcClient.methodCall('document.add', [
    {
      sid,
      folder: folder.id,
      submit_to: 1,
      uploads: [
        {
          title: manuscriptId,
          upload: manuscriptFileBuffer,
        },
      ],
    },
  ])

  if (result.uploaded.length === 0)
    throw new Error(
      `Could not upload manuscript ${manuscriptId} to iThenticate. Response: ${JSON.stringify(
        result.errors,
      )}`,
    )

  return result.uploaded[0]
}

const getIthenticateResults = async ({ sid, uploaded }) =>
  new Promise((resolve, reject) => {
    const timeoutId = setTimeout(() => {
      clearTimeout(timeoutId)
      clearInterval(intervalId)
      reject(
        new Error(
          'the ithenticate response took too long, please check report',
        ),
      )
    }, config.get('ithenticate.timeout'))

    const intervalId = setInterval(() => {
      queryIthenticate(sid, uploaded)
        .then((ithenticateResults) => {
          if (!ithenticateResults) {
            return
          }
          clearInterval(intervalId)
          clearTimeout(timeoutId)
          resolve(ithenticateResults)
        })
        .catch((err) => {
          clearInterval(intervalId)
          clearTimeout(timeoutId)
          err.message += ` - Document ${uploaded.id}`
          reject(err)
        })
    }, CHECK_PROCESS_INTERVAL)
  })

async function queryIthenticate(sid, uploaded) {
  const { documents } = await rpcClient.methodCall('document.get', [
    {
      sid,
      id: uploaded.id,
    },
  ])

  if (!Array.isArray(documents))
    throw new Error(`Could not find document with id ${uploaded.id}`)

  const [document] = documents

  if (document.is_pending) {
    return
  }

  if (document.error) {
    throw new Error(document.error)
  }

  const {
    score: totalSimilarityPercentage,
    max_percent_match: firstSourceSimilarityPercentage,
  } = document.parts[0]

  const { report_url: reportUrl } = await rpcClient.methodCall(
    'report.get_document',
    [
      {
        sid,
        id: uploaded.id,
      },
    ],
  )

  if (!reportUrl)
    throw new Error(`Could not get document ${uploaded.id} report`)

  return {
    totalSimilarityPercentage,
    firstSourceSimilarityPercentage,
    reportUrl,
  }
}

module.exports = { getSimilarityPercentage }
