const { S3 } = require('@aws-sdk/client-s3')
const config = require('config')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
  useAccelerateEndpoint: true,
})

const bucket = config.get('aws.s3.bucket')

module.exports.getObjectBuffer = async (key) => {
  const object = await s3
    .getObject({ Key: key, Bucket: bucket })
    .then((res) => res.Body)

  return streamToBuffer(object)
}

async function streamToBuffer(stream) {
  return new Promise((resolve, reject) => {
    const chunks = []
    stream.on('data', (chunk) => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks)))
  })
}

module.exports.headObject = (key) => s3.headObject({ Key: key, Bucket: bucket })
