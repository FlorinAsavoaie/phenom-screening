const useCases = require('./use-cases')

const similarityService = require('./services/ithenticateService')
const s3Service = require('./services/s3Service')
const config = require('config')

module.exports = {
  async FileConverted(data) {
    return useCases.checkSimilarityUseCase
      .initialize({ similarityService, s3Service, config })
      .execute(data)
  },
}
