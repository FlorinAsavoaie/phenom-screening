const logger = require('../config/loggerCustom')

const SANCTION_LIST_PROFILE_TABLE = 'sanction_list_profile'

exports.up = async (knex) => {
  await knex.raw(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`)
  await knex.schema
    .createTable(SANCTION_LIST_PROFILE_TABLE, (table) => {
      table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(knex.fn.now())
      table.timestamp('updated').notNullable().defaultTo(knex.fn.now())

      table.text('surname').notNullable()
      table.text('given_names').notNullable()
      table.boolean('is_on_bad_debt_list').notNullable()
      table.boolean('is_on_black_list').notNullable()
      table.boolean('is_on_watch_list').notNullable()

      table.text('email').notNullable()
      table.unique('email')

      table.timestamp('from_date')
      table.timestamp('to_date')

      table.text('comments')
      table.text('country')
    })
    .then((_) =>
      logger.info(`${SANCTION_LIST_PROFILE_TABLE} table created successfully`),
    )
    .catch((err) => logger.error(err))
}

exports.down = async (knex) =>
  knex.schema
    .dropTable(SANCTION_LIST_PROFILE_TABLE)
    .then((_) =>
      logger.info(`${SANCTION_LIST_PROFILE_TABLE} table dropped successfully`),
    )
    .catch((err) => logger.error(err))
