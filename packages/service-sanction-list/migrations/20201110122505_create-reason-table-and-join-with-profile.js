const logger = require('../config/loggerCustom')

const REASON_TABLE = 'reason'
const PROFILE_REASON_TABLE = 'sanction_list_profile_reason'

const reasons = [
  { name: 'Citation manipulation' },
  { name: 'Duplicate submission' },
  { name: 'Falsification of results' },
  { name: 'Plagiarism' },
  { name: 'Figure concerns' },
  { name: 'Data concerns' },
  { name: 'Authorship' },
  { name: 'Peer review manipulation' },
  { name: 'Research ethics' },
  { name: 'Unprofessional' },
  { name: 'Legal issues' },
  { name: 'Conflict of interest' },
  { name: 'Potential paper mill' },
  { name: 'Redundant publication' },
  { name: 'Editorial handling' },
  { name: 'Other' },
]

exports.up = async (knex) => {
  await knex.schema
    .createTable(REASON_TABLE, (table) => {
      table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(knex.fn.now())
      table.timestamp('updated').notNullable().defaultTo(knex.fn.now())
      table.text('name').notNullable()
    })
    .then((_) => logger.info(`${REASON_TABLE} table created successfully`))
    .catch((err) => logger.error(err))

  await knex(REASON_TABLE).insert(reasons)

  await knex.schema
    .createTable(PROFILE_REASON_TABLE, (table) => {
      table.uuid('id').primary().defaultTo(knex.raw('uuid_generate_v4()'))
      table.timestamp('created').notNullable().defaultTo(knex.fn.now())
      table.timestamp('updated').notNullable().defaultTo(knex.fn.now())
      table.uuid('sanction_list_profile_id').notNullable()
      table.uuid('reason_id').notNullable()

      table
        .foreign('sanction_list_profile_id')
        .references('sanction_list_profile.id')
        .onDelete('CASCADE')
      table.foreign('reason_id').references('reason.id')
    })
    .then((_) =>
      logger.info(`${PROFILE_REASON_TABLE} table created successfully`),
    )
    .catch((err) => logger.error(err))
}

exports.down = async (knex) => {
  await knex.schema
    .dropTable(PROFILE_REASON_TABLE)
    .then((_) =>
      logger.info(`${PROFILE_REASON_TABLE} table dropped successfully`),
    )
    .catch((err) => logger.error(err))

  await knex.schema
    .dropTable(REASON_TABLE)
    .then((_) => logger.info(`${REASON_TABLE} table dropped successfully`))
    .catch((err) => logger.error(err))
}
