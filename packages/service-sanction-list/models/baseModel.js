const config = require('config')
const knex = require('knex')
const {
  knexSnakeCaseMappers,
  Model,
  NotFoundError,
  AjvValidator,
} = require('objection')
const { merge } = require('lodash')
const uuid = require('uuid')
const addFormats = require('ajv-formats')

const db = knex({
  client: 'pg',
  connection: config.get('db.connection'),
  pool: config.get('db.pool'),
  ...knexSnakeCaseMappers(),
  acquireConnectionTimeout: 5000,
  asyncStackTraces: true,
})

Model.knex(db)

class BaseModel extends Model {
  static createValidator() {
    return new AjvValidator({
      onCreateAjv: (ajv) => {
        addFormats(ajv)
      },
      options: {
        allErrors: true,
        validateSchema: false,
        ownProperties: true,
        validateFormats: false,
        allowUnionTypes: true,
        v5: true,
      },
    })
  }

  constructor(properties) {
    super(properties)

    if (properties) {
      this._updateProperties(properties)
    }
  }

  _updateProperties(properties) {
    Object.keys(properties).forEach((prop) => {
      this[prop] = properties[prop]
    })
    return this
  }

  updateProperties(properties) {
    return this._updateProperties(properties)
  }

  static get jsonSchema() {
    let schema

    const mergeSchema = (additionalSchema) => {
      if (additionalSchema) {
        schema = merge(schema, additionalSchema)
      }
    }

    const getSchemasRecursively = (object) => {
      mergeSchema(object.schema)
      const proto = Object.getPrototypeOf(object)

      if (proto.name !== 'BaseModel') {
        getSchemasRecursively(proto)
      }
    }

    getSchemasRecursively(this)

    const baseSchema = {
      type: 'object',
      properties: {
        id: { type: 'string', format: 'uuid' },
        created: { type: ['string', 'object'], format: 'date-time' },
        updated: { type: ['string', 'object'], format: 'date-time' },
      },
      additionalProperties: false,
    }

    if (schema) {
      return merge(baseSchema, schema)
    }

    return baseSchema
  }

  $beforeInsert() {
    this.id = this.id || uuid.v4()
    this.created = new Date().toISOString()
    this.updated = this.created
  }

  $beforeUpdate() {
    this.updated = new Date().toISOString()
  }

  static async find(id, eagerLoadRelations) {
    const object = await this.query()
      .findById(id)
      .withGraphFetched(eagerLoadRelations)

    if (!object) {
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)
    }

    return object
  }

  static async findOneBy({ queryObject, eagerLoadRelations }) {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .withGraphFetched(eagerLoadRelations)

    if (!object.length) {
      return
    }

    return object[0]
  }

  static findBy(queryObject, eagerLoadRelations) {
    return this.query().where(queryObject).withGraphFetched(eagerLoadRelations)
  }

  static findIn(field, options, eagerLoadRelations) {
    return this.query()
      .whereIn(field, options)
      .withGraphFetched(eagerLoadRelations)
  }

  static async findAll({
    order,
    queryObject,
    orderByField,
    eagerLoadRelations,
  } = {}) {
    return this.query()
      .where(queryObject)
      .orderBy(orderByField, order)
      .withGraphFetched(eagerLoadRelations)
  }

  static async insertGraph(object, options) {
    return this.knex().transaction(async (trx) =>
      this.query(trx).insertGraph(object, options),
    )
  }

  static async upsertGraph(object, options) {
    return this.knex().transaction(async (trx) =>
      this.query(trx).upsertGraph(object, options),
    )
  }

  static async deleteById(id) {
    return this.query().delete().where('id', id)
  }

  static async all() {
    return this.query()
  }
}

module.exports = BaseModel
