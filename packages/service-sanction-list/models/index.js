const SanctionListProfile = require('./sanctionListProfile')
const Reason = require('./reason')
const profileFactory = require('./profileFactory')

module.exports = {
  models: { SanctionListProfile, Reason },
  factories: { profileFactory },
}
