const BaseModel = require('./baseModel')

class Reason extends BaseModel {
  static get tableName() {
    return 'reason'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
      },
    }
  }

  static get Reasons() {
    return {
      citationManipulation: 'Citation manipulation',
      duplicateSubmission: 'Duplicate submission',
      falsificationOfResults: 'Falsification of results',
      plagiarism: 'Plagiarism',
      figureConcerns: 'Figure concerns',
      dataConcerns: 'Data concerns',
      authorship: 'Authorship',
      peerReviewManipulation: 'Peer review manipulation',
      researchEthics: 'Research ethics',
      unprofessional: 'Unprofessional',
      legalIssues: 'Legal issues',
      conflictOfInterest: 'Conflict of interest',
      potentialPaperMill: 'Potential paper mill',
      redundantPublication: 'Redundant publication',
      editorialHandling: 'Editorial handling',
      other: 'Other',
    }
  }

  static async findAllByIds(reasonIds) {
    return Reason.query().where((builder) => builder.whereIn('id', reasonIds))
  }
}

module.exports = Reason
