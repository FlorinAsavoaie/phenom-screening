const BaseModel = require('./baseModel')

class SanctionListProfile extends BaseModel {
  static get tableName() {
    return 'sanction_list_profile'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        surname: { type: ['string'] },
        givenNames: { type: ['string'] },
        isOnBadDebtList: { type: 'boolean' },
        isOnBlackList: { type: 'boolean' },
        isOnWatchList: { type: 'boolean' },
        email: { type: 'string' },
        fromDate: { type: ['string', 'object', 'null'], format: 'date-time' },
        toDate: { type: ['string', 'object', 'null'], format: 'date-time' },
        comments: { type: ['string', 'null'] },
        country: { type: ['string', 'null'] },
      },
    }
  }

  static get relationMappings() {
    return {
      reasons: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: require('./reason'),
        join: {
          from: 'sanction_list_profile.id',
          through: {
            from: 'sanction_list_profile_reason.sanctionListProfileId',
            to: 'sanction_list_profile_reason.reasonId',
          },
          to: 'reason.id',
        },
      },
    }
  }

  static async findPaginateSanctionListProfiles({
    page,
    pageSize,
    searchValue,
  }) {
    return this.query()
      .select('*')

      .where((builder) => {
        if (searchValue) {
          return builder
            .whereRaw('lower(email) LIKE ?', [`%${searchValue.toLowerCase()}%`])
            .orWhereRaw('lower(given_names) LIKE ?', [
              `%${searchValue.toLowerCase()}%`,
            ])
            .orWhereRaw('lower(surname) LIKE ?', [
              `%${searchValue.toLowerCase()}%`,
            ])
            .orWhereRaw(
              `concat(lower(given_names), ' ', lower(surname)) LIKE ?`,
              [`%${searchValue.toLowerCase()}%`],
            )
            .orWhereRaw(
              `concat(lower(surname), ' ', lower(given_names)) LIKE ?`,
              [`%${searchValue.toLowerCase()}%`],
            )
        }
      })
      .orderBy('givenNames', 'asc')
      .orderBy('surname', 'asc')
      .page(page, pageSize)
  }

  static async getUniqueEmail({ email }) {
    return SanctionListProfile.findOneBy({
      queryObject: (builder) => {
        if (email) {
          builder = builder.whereRaw('lower(email) = ?', [email.toLowerCase()])
        }

        return builder
      },
    })
  }

  static async insertOrUpdate({ sanctionAuthorProfile, propsToUpdate }) {
    const authorProfile = await SanctionListProfile.findOneBy({
      queryObject: { email: sanctionAuthorProfile.email },
    })

    if (!authorProfile) {
      await SanctionListProfile.insertGraph(sanctionAuthorProfile, {
        relate: true,
      })
    } else {
      authorProfile.updateProperties(propsToUpdate)
      await SanctionListProfile.upsertGraph(authorProfile)
    }
  }
}

module.exports = SanctionListProfile
