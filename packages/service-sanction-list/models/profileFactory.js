const Profile = require('./sanctionListProfile')

module.exports = {
  createProfile({
    surname,
    givenNames,
    isOnBadDebtList,
    isOnBlackList,
    isOnWatchList,
    email,
    fromDate,
    toDate,
    comments,
    reasons,
  }) {
    return new Profile({
      surname,
      givenNames,
      isOnBadDebtList,
      isOnBlackList,
      isOnWatchList,
      email,
      fromDate,
      toDate,
      comments,
      reasons,
    })
  },
}
