import { WithSopsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithSopsSecretsServiceProps = {
  ...defaultValues,
  sopsSecrets: require('./prod.env.enc.json'),
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/service-sanction-list',
      tag: 'latest',
    },
    ingressOptions: {
      rules: [
        {
          host: 'sanction-list.prod.phenom.pub',
        },
      ],
    },
  },
}
