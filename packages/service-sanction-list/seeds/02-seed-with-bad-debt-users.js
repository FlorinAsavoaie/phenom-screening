/**
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***
 *
 * AWS_REGION
 * AWS_S3_ACCESS_KEY
 * AWS_S3_SECRET_KEY
 * AWS_S3_BUCKET
 *
 * BAD_DEBT_AUTHORS_PROVIDER_KEY
 * SANCTION_USERS_PROVIDER_KEY
 * ```
 */

const csv = require('fast-csv')
const stream = require('stream')
const logger = require('../config/loggerCustom')
const config = require('config')

const s3Service = require('../src/service/s3Service')
const SanctionListProfile = require('../models/sanctionListProfile')
const Reason = require('../models/reason')
const profileFactory = require('../models/profileFactory')

exports.seed = async function seedDBWithBadDebtUsers() {
  logger.info('seeding bad dept users...')

  const badDebtAuthorsProviderKey = config.get('providerKey.badDebtAuthors')

  if (!badDebtAuthorsProviderKey) {
    return logger.warn(
      'No BAD_DEBT_AUTHORS_PROVIDER_KEY specified! Skipping reading authors',
    )
  }

  const { Body: badDebtAuthorsStream } = await s3Service.getObjectStream(
    badDebtAuthorsProviderKey,
  )

  return new Promise((resolve) => {
    badDebtAuthorsStream
      .on('error', logger.error)
      .pipe(csv.parse({ headers: true }))
      .pipe(createWritableStream())
      .on('finish', () => {
        logger.info('seeding database with bad debt authors finished')
        resolve()
      })
  })
}

function createWritableStream() {
  return new stream.Writable({
    objectMode: true,
    write: async (chunk, _, callback) => {
      try {
        await saveSanctionedAuthorProfile(chunk)
        callback()
      } catch (e) {
        callback(e)
      }
    },
  })
}

async function saveSanctionedAuthorProfile(item) {
  const parsedAuthor = await parseBadDeptUser(item)

  const sanctionAuthorProfile = profileFactory.createProfile(parsedAuthor)

  if (!sanctionAuthorProfile.email) return

  await SanctionListProfile.insertOrUpdate({
    sanctionAuthorProfile,
    propsToUpdate: {
      isOnBadDebtList: sanctionAuthorProfile.isOnBadDebtList,
    },
  })
}

async function parseBadDeptUser(item) {
  const reasons = await Reason.findBy({ name: 'Other' })

  return {
    surname: item['Last Name'] || 'n/a',
    givenNames: item['Given Names'] || 'n/a',
    email: item['Email Address'],
    fromDate: null,
    toDate: null,
    isOnBlackList: false,
    isOnWatchList: false,
    isOnBadDebtList: true,
    comments: '',
    reasons,
    country: item.Country,
  }
}
