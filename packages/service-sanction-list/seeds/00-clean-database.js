const logger = require('../config/loggerCustom')

const SANCTION_LIST_PROFILE_TABLE = 'sanction_list_profile'
const PROFILE_REASON_TABLE = 'sanction_list_profile_reason'

exports.seed = async function cleanDatabase(knex) {
  await knex(PROFILE_REASON_TABLE).del()
  await knex(SANCTION_LIST_PROFILE_TABLE).del()

  logger.info('database was cleaned')
}
