/**
 * Required environment variables:
 * ```
 * DATABASE=***
 * DB_HOST=***
 * DB_PASS=***
 * DB_USER=***
 *
 * AWS_REGION
 * AWS_S3_ACCESS_KEY
 * AWS_S3_SECRET_KEY
 * AWS_S3_BUCKET
 *
 * BAD_DEBT_AUTHORS_PROVIDER_KEY
 * SANCTION_USERS_PROVIDER_KEY
 * ```
 */

const csv = require('fast-csv')
const stream = require('stream')
const logger = require('../config/loggerCustom')
const config = require('config')

const s3Service = require('../src/service/s3Service')
const SanctionListProfile = require('../models/sanctionListProfile')
const profileFactory = require('../models/profileFactory')
const Reason = require('../models/reason')

exports.seed = async function seedDBWithSanctionedUsers() {
  logger.info('seeding sanctioned users...')
  const sanctionUsersProviderKey = config.get('providerKey.sanctionUsers')

  if (!sanctionUsersProviderKey) {
    return logger.warn(
      'No SANCTION_USERS_PROVIDER_KEY specified! Skipping reading authors',
    )
  }

  const { Body: sanctionedListAuthorsStream } = await s3Service.getObjectStream(
    sanctionUsersProviderKey,
  )

  return new Promise((resolve) => {
    sanctionedListAuthorsStream
      .on('error', logger.error)
      .pipe(csv.parse({ headers: true }))
      .pipe(createWritableStream())
      .on('finish', () => {
        logger.info('seeding database with sanctioned authors finished')
        resolve()
      })
  })
}

function createWritableStream() {
  return new stream.Writable({
    objectMode: true,
    write: async (chunk, _, callback) => {
      try {
        await saveSanctionedAuthorProfile(chunk)
        callback()
      } catch (e) {
        callback(e)
      }
    },
  })
}

async function saveSanctionedAuthorProfile(item) {
  const parsedAuthor = await parseSanctionedUserList(item)

  const sanctionAuthorProfile = profileFactory.createProfile(parsedAuthor)

  if (!sanctionAuthorProfile.email) return

  await SanctionListProfile.insertOrUpdate({
    sanctionAuthorProfile,
    propsToUpdate: {
      isOnBlackList: sanctionAuthorProfile.isOnBlackList,
      isOnWatchList: sanctionAuthorProfile.isOnWatchList,
    },
  })
}

async function parseSanctionedUserList(item) {
  const { reasons, otherReasons } = await getReasonsByName(item.Reason)

  const comment = item.Comment ? item.Comment.trim() : ''
  const comments = otherReasons.length
    ? `"Other" reason includes ${otherReasons}. \n ${comment}`
    : comment

  return {
    surname: item.LastName || 'n/a',
    givenNames: item.FirstName || 'n/a',
    email: item.Email,
    fromDate: parseDate(item.FromDate),
    toDate: parseDate(item.ToDate),
    isOnBlackList: item['Blacklist/Watchlist'] === 'Blacklisted',
    isOnWatchList: item['Blacklist/Watchlist'] === 'Watchlist',
    isOnBadDebtList: false,
    comments,
    reasons,
    country: item.CountryName,
  }
}

function parseDate(date) {
  if (date === '0' || !date) return null
  const dateArray = date.split('/')
  return new Date(dateArray[2], dateArray[0] - 1, dateArray[1]).toISOString()
}

async function getReasonsByName(reason) {
  const reasonsFromDb = await Reason.all()
  const reasons = reason ? reason.split(', ') : []
  const otherReason = reasonsFromDb.find((reason) => reason.name === 'Other')

  const resultReasons = []
  const otherReasons = []

  reasons.forEach((reason) => {
    const reasonFromDb = reasonsFromDb.find(
      (reasonFromDb) =>
        reasonFromDb.name.toLowerCase() === reason.toLowerCase(),
    )

    if (reasonFromDb) {
      resultReasons.push(reasonFromDb)
    } else {
      otherReasons.push(reason)
    }
  }, [])

  if (resultReasons.length === 0) {
    resultReasons.push(otherReason)
  }

  return { reasons: resultReasons, otherReasons }
}
