const { rule } = require('graphql-shield')
const verifyToken = require('./verifyToken')

const isAuthenticated = rule()(
  async (parent, args, { authorizationHeader }) => {
    const token = authorizationHeader.split(' ')[1]

    if (!token) {
      return new Error('Authentication token is required.')
    }

    try {
      const user = await verifyToken(token)
      return user !== null
    } catch (error) {
      return new Error('invalid token')
    }
  },
)

module.exports = isAuthenticated
