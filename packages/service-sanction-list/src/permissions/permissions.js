const { shield } = require('graphql-shield')
const { isAdmin } = require('../permissions')
const { ApolloError } = require('apollo-server')
const logger = require('../../config/loggerCustom')

class InternalServerError extends ApolloError {
  constructor() {
    super(
      'Something went wrong! Please contact your administrator',
      'ERR_INTERNAL_SERVER',
    )
  }
}

const permissions = shield(
  {
    Query: {
      getSanctionListProfiles: isAdmin,
      getSanctionListProfile: isAdmin,
      getReasons: isAdmin,
    },
    Mutation: {
      addSanctionListProfile: isAdmin,
      editSanctionListProfile: isAdmin,
      deleteSanctionListProfile: isAdmin,
    },
  },
  {
    fallbackError: (thrownError) => {
      if (thrownError instanceof ApolloError) {
        return thrownError
      } else if (thrownError instanceof Error) {
        logger.error(thrownError)

        return new InternalServerError()
      }
      logger.error('The resolver threw something that is not an error.')
      logger.error(thrownError)

      return new InternalServerError()
    },
  },
)

module.exports = permissions
