const { rule } = require('graphql-shield')
const verifyToken = require('./verifyToken')
const { get } = require('lodash')
const config = require('config')

const isAdmin = rule()(async (parent, args, { authorizationHeader }) => {
  const SSO_CLIENT_ID = config.get('keycloak.ssoClientId')
  const Roles = {
    ADMIN: 'Admin',
  }

  const user = await verifyToken(authorizationHeader.split(' ')[1])

  const roles = get(user, `resource_access.${SSO_CLIENT_ID}.roles`, [])

  return roles.includes(Roles.ADMIN) || new Error('User is not admin')
})

module.exports = isAdmin
