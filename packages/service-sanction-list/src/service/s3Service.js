const { S3 } = require('@aws-sdk/client-s3')
const config = require('config')

const s3 = new S3({
  region: config.get('aws.region'),
  credentials:
    config.has('aws.s3.accessKeyId') && config.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: config.get('aws.s3.accessKeyId'),
          secretAccessKey: config.get('aws.s3.secretAccessKey'),
        }
      : undefined,
})

const bucket = config.get('aws.s3.bucket')

module.exports.getFile = (key) => s3.getObject({ Key: key, Bucket: bucket })

module.exports.getObjectStream = (key) =>
  s3.getObject({ Key: key, Bucket: bucket })
