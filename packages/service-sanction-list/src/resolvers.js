const useCases = require('./use-cases')
const { models, factories } = require('../models')

const resolvers = {
  Query: {
    async getSanctionListProfiles(_, params, ctx) {
      return useCases.getSanctionListProfilesUseCase
        .initialize(models)
        .execute(params)
    },
    async getReasons(_, params) {
      return useCases.getReasonsUseCase.initialize(models).execute(params)
    },
    async getSanctionListProfile(_, params) {
      return useCases.getSanctionListProfileUseCase
        .initialize(models)
        .execute(params)
    },
  },
  Mutation: {
    async addSanctionListProfile(_, params) {
      return useCases.addSanctionListProfileUseCase
        .initialize({ models, factories })
        .execute(params)
    },
    async editSanctionListProfile(_, params) {
      return useCases.editSanctionListProfileUseCase
        .initialize({ models })
        .execute(params)
    },
    async deleteSanctionListProfile(_, params) {
      return useCases.deleteSanctionListProfileUseCase
        .initialize({ models })
        .execute(params)
    },
  },
  SanctionListProfile: {
    async reasons(rootQuery, params, ctx) {
      return useCases.reasonsUseCase.initialize(models).execute(rootQuery)
    },
  },
}

module.exports = resolvers
