const { models } = require('../models')
const useCases = require('./use-cases')

module.exports = {
  async ManuscriptIngested(data) {
    return useCases.searchAuthorsInSanctionListsUseCase
      .initialize(models)
      .execute(data)
  },
}
