const { ValidationError } = require('apollo-server')

const initialize = ({
  models: { SanctionListProfile, Reason },
  factories: { profileFactory },
}) => {
  return {
    execute,
  }
  async function execute({
    surname,
    givenNames,
    isOnBadDebtList,
    isOnBlackList,
    isOnWatchList,
    email,
    toDate,
    comments,
    reasons,
  }) {
    const reasonsInstances = await Reason.findAllByIds(reasons)
    await checkIfReasonsExists(reasonsInstances)
    await checkIfEmailIsUnique(email)
    await checkIfEndDateIsOlderThanToday(toDate)
    checkIfTypesAreValid(isOnBadDebtList, isOnBlackList, isOnWatchList)

    const sanctionListProfile = profileFactory.createProfile({
      surname,
      givenNames,
      isOnBadDebtList,
      isOnBlackList,
      isOnWatchList,
      email,
      fromDate: new Date(),
      toDate,
      comments,
      reasons: reasonsInstances,
    })

    await SanctionListProfile.insertGraph(sanctionListProfile, {
      relate: true,
      unrelate: true,
    })
  }

  async function checkIfEmailIsUnique(email) {
    const uniqueEmail = await SanctionListProfile.getUniqueEmail({ email })
    if (uniqueEmail) {
      throw new ValidationError(`Email already exists.`)
    }
  }
  function checkIfTypesAreValid(isOnBadDebtList, isOnWatchList, isOnBlackList) {
    if (
      isOnBadDebtList === false &&
      isOnBlackList === false &&
      isOnWatchList === false
    ) {
      throw new ValidationError(`At least one type is required to be checked `)
    }
  }

  async function checkIfReasonsExists(reasons) {
    if (reasons.length === 0) {
      throw new ValidationError(`We don't have these reasons in database`)
    }
  }

  async function checkIfEndDateIsOlderThanToday(endDate) {
    if (endDate && parseDate(endDate) < parseDate(Date.now())) {
      throw new ValidationError('Today date is older than end date')
    }
  }
}

module.exports = {
  initialize,
}

function parseDate(date) {
  const parsedDate = new Date(date)
  parsedDate.setHours(0)
  parsedDate.setMinutes(0)
  parsedDate.setSeconds(0)
  parsedDate.setMilliseconds(0)

  return parsedDate
}
