function initialize({ SanctionListProfile }) {
  return {
    execute,
  }

  async function execute({ page = 0, pageSize = 20, searchValue = '' }) {
    const profiles = await SanctionListProfile.findPaginateSanctionListProfiles(
      {
        page,
        pageSize,
        searchValue,
      },
    )
    const results = profiles.results.map((profile) => ({
      ...profile,
      types: parseTypes(profile),
    }))

    return { results, total: profiles.total }
  }

  function parseTypes(profile) {
    const types = []
    if (profile.isOnBadDebtList) types.push('Bad Debt')
    if (profile.isOnBlackList) types.push('Blacklist')
    if (profile.isOnWatchList) types.push('Watchlist')
    return types
  }
}
module.exports = {
  initialize,
}
