function initialize({ SanctionListProfile }) {
  return {
    execute,
  }

  function execute({ profileId }) {
    return SanctionListProfile.find(profileId, 'reasons')
  }
}
module.exports = {
  initialize,
}
