const initialize = ({ models: { SanctionListProfile } }) => {
  return {
    execute,
  }
  async function execute({ profileId }) {
    await SanctionListProfile.deleteById(profileId)
  }
}

module.exports = { initialize }
