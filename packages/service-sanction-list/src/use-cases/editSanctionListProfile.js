const { ValidationError } = require('apollo-server')

const initialize = ({ models: { Reason, SanctionListProfile } }) => {
  return {
    execute,
  }
  async function execute({
    profileId,
    isOnBadDebtList,
    isOnBlackList,
    isOnWatchList,
    toDate,
    comments,
    reasons,
  }) {
    const reasonsInstances = await Reason.findAllByIds(reasons)
    await checkIfReasonsExists(reasonsInstances)

    checkIfTypesAreValid(isOnBadDebtList, isOnBlackList, isOnWatchList)

    await SanctionListProfile.upsertGraph(
      {
        id: profileId,
        isOnBadDebtList,
        isOnBlackList,
        isOnWatchList,
        toDate,
        comments,
        reasons: reasonsInstances,
      },
      { relate: true, unrelate: true, noUpdate: ['reasons'] },
    )
  }
  function checkIfTypesAreValid(isOnBadDebtList, isOnWatchList, isOnBlackList) {
    if (
      isOnBadDebtList === false &&
      isOnBlackList === false &&
      isOnWatchList === false
    ) {
      throw new ValidationError(`At least one type is required to be checked `)
    }
  }

  async function checkIfReasonsExists(reasons) {
    if (reasons.length === 0) {
      throw new ValidationError(`We don't have these reasons in database`)
    }
  }
}
module.exports = {
  initialize,
}
