const { Promise } = require('bluebird')

function initialize({ SanctionListProfile }) {
  async function execute({ manuscriptId, authors = [] }) {
    const sanctionedAuthors = await Promise.map(
      authors,
      async (author) =>
        SanctionListProfile.findOneBy({
          queryObject: { email: author.email },
        }),
      { concurrency: 10 },
    ).then((foundAuthors) =>
      foundAuthors.reduce(
        (acc, author, index) => ({
          ...acc,
          [authors[index].id]: getSanctionListObject(author),
        }),
        {},
      ),
    )

    await applicationEventBus.publishMessage({
      event: 'SanctionListCheckDone',
      data: {
        manuscriptId,
        authors: sanctionedAuthors,
      },
    })
  }
  return { execute }
}

const getSanctionListObject = (author) => {
  if (!author)
    return {
      isOnBadDebtList: false,
      isOnBlackList: false,
      isOnWatchList: false,
    }

  let { isOnBlackList } = author
  let { isOnWatchList } = author
  let { isOnBadDebtList } = author

  // check if author sanction is not expired
  if (author.toDate && new Date(author.toDate) < Date.now()) {
    isOnBlackList = false
    isOnWatchList = false
    isOnBadDebtList = false
  }

  return {
    isOnBadDebtList,
    isOnBlackList,
    isOnWatchList,
  }
}
module.exports = { initialize }
