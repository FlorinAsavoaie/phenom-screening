function initialize({ SanctionListProfile }) {
  async function execute({ id }) {
    const profile = await SanctionListProfile.findOneBy({
      queryObject: { id },
      eagerLoadRelations: 'reasons',
    })

    return profile.reasons.map((reason) => ({
      id: reason.id,
      name: reason.name,
    }))
  }

  return { execute }
}

module.exports = {
  initialize,
}
