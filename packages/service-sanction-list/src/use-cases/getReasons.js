const initialize = ({ Reason }) => {
  return {
    execute,
  }
  async function execute() {
    return Reason.all()
  }
}

module.exports = {
  initialize,
}
