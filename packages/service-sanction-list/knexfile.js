require('dotenv').config()
const config = require('config')

module.exports = {
  client: 'pg',
  connection: config.get('db.connection'),
  seeds: {
    directory: './seeds',
  },
  migrations: {
    directory: './migrations',
  },
}
