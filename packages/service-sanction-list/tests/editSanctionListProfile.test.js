const { models } = require('../models')
const { editSanctionListProfileUseCase } = require('../src/use-cases')
const Chance = require('chance')

const { SanctionListProfile, Reason } = models
const chance = new Chance()
const reasons = [{ id: chance.guid(), name: chance.name() }]
const profileId = chance.guid()
const profileMock = {
  id: profileId,
  isOnBadDebtList: false,
  isOnBlackList: false,
  isOnWatchList: true,
  toDate: chance.date(),
  comments: chance.sentence(),
  reasons,
}

describe('Edit sanction list profile use case', () => {
  it('should edit profile', async () => {
    Reason.findAllByIds = jest.fn().mockReturnValue(reasons)
    SanctionListProfile.upsertGraph = jest.fn().mockReturnValue({ profileMock })

    await editSanctionListProfileUseCase
      .initialize({ models })
      .execute(profileMock)
    expect(SanctionListProfile.upsertGraph).toHaveBeenCalledTimes(1)
  })
  it('should throw an error if we have no reasons selected', async () => {
    await editSanctionListProfileUseCase
      .initialize({ models })
      .execute(profileMock)
    expect(reasons.length).toBeGreaterThan(0)
  })
})
