const { models } = require('../models')
const { deleteSanctionListProfileUseCase } = require('../src/use-cases')
const Chance = require('chance')

const { SanctionListProfile } = models
const chance = new Chance()

const profileId = chance.guid()

describe('Delete author profile from sanction list', () => {
  it('should delete author profile if exists in db', async () => {
    SanctionListProfile.deleteById = jest.fn().mockReturnValue(1)

    await deleteSanctionListProfileUseCase
      .initialize({ models })
      .execute({ profileId })

    expect(SanctionListProfile.deleteById).toHaveBeenCalledTimes(1)
    expect(SanctionListProfile.deleteById).toHaveBeenCalledWith(profileId)
  })
})
