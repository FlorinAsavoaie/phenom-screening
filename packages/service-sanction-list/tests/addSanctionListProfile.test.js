const { models } = require('../models')
const { addSanctionListProfileUseCase } = require('../src/use-cases')
const Chance = require('chance')

const { SanctionListProfile, Reason } = models
const chance = new Chance()
const reasons = [{ id: chance.guid(), name: chance.name() }]
const email = chance.email()
const profileMock = {
  surname: chance.first(),
  givenNames: chance.name(),
  isOnBadDebtList: false,
  isOnBlackList: false,
  isOnWatchList: true,
  email,
  toDate: chance.date(),
  comments: chance.sentence(),
  reasons,
}
const factories = {
  profileFactory: {
    createProfile: jest.fn().mockReturnValue({ ...profileMock }),
  },
}

describe('Add sanction list profile use case', () => {
  it('should throw an error if email already exists', async () => {
    jest.spyOn(Reason, 'findAllByIds').mockReturnValue(reasons)
    jest.spyOn(SanctionListProfile, 'getUniqueEmail').mockReturnValue(email)
    const result = addSanctionListProfileUseCase
      .initialize({
        models,
        factories,
      })
      .execute(profileMock)
    await expect(result).rejects.toThrow(`Email already exists.`)
  })

  it('should throw an error if the reason is invalid', async () => {
    jest.spyOn(Reason, 'findAllByIds').mockReturnValue([])
    const result = addSanctionListProfileUseCase
      .initialize({
        models,
        factories,
      })
      .execute(profileMock)
    await expect(result).rejects.toThrow(
      `We don't have these reasons in database`,
    )
  })

  it('should add profile and reason', async () => {
    jest.spyOn(Reason, 'findAllByIds').mockReturnValue(reasons)
    jest.spyOn(SanctionListProfile, 'getUniqueEmail').mockReturnValue(undefined)
    SanctionListProfile.insertGraph = jest
      .fn()
      .mockReturnValue({ ...profileMock, fromDate: chance.date() })
    Reason.findIn = jest.fn().mockReturnValue(reasons)

    await addSanctionListProfileUseCase
      .initialize({
        models,
        factories,
      })
      .execute(profileMock)
    expect(factories.profileFactory.createProfile).toHaveBeenCalledTimes(1)
    expect(SanctionListProfile.insertGraph).toHaveBeenCalledTimes(1)
  })
})
