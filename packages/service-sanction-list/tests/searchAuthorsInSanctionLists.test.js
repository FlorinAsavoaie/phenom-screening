const { models } = require('../models')
const useCases = require('../src/use-cases')
const Chance = require('chance')

const chance = new Chance()

const { SanctionListProfile } = models

const generateInput = () => [
  {
    email: 'firstTestEmail@test.com',
    id: chance.guid(),
  },
  {
    email: 'secondTestEmail@test.com',
    id: chance.guid(),
  },
  {
    email: 'thirdTestEmail@test.com',
    id: chance.guid(),
  },
  {
    email: 'forthTestEmail@test.com',
    id: chance.guid(),
  },
]

global.applicationEventBus = {
  publishMessage: jest.fn(async () => {}),
}

describe('Search Authors in Sanction Lists', () => {
  let authorsInput
  let manuscriptId
  beforeEach(async () => {
    authorsInput = generateInput()
    manuscriptId = chance.guid()
  })
  it('should correctly rerun the result of the search', async () => {
    SanctionListProfile.findOneBy = jest
      .fn()
      .mockImplementation(({ queryObject }) => {
        if (queryObject.email === 'firstTestEmail@test.com') {
          return {
            email: 'firstTestEmail@test.com',
            isOnBadDebtList: true,
            isOnBlackList: false,
            isOnWatchList: false,
          }
        }
        if (queryObject.email === 'secondTestEmail@test.com') {
          return {
            email: 'secondTestEmail@test.com',
            isOnBadDebtList: true,
            isOnBlackList: false,
            isOnWatchList: true,
            fromDate: '2019-05-20 00:00:00',
            toDate: null,
          }
        }
        if (queryObject.email === 'thirdTestEmail@test.com') {
          return {
            email: 'thirdTestEmail@test.com',
            isOnBadDebtList: true,
            isOnBlackList: true,
            isOnWatchList: false,
            fromDate: '2006-09-24 00:00:00',
            toDate: '2019-05-20 00:00:00',
          }
        }
        return null
      })

    await useCases.searchAuthorsInSanctionListsUseCase
      .initialize(models)
      .execute({ manuscriptId, authors: authorsInput })

    expect(applicationEventBus.publishMessage).toHaveBeenCalledTimes(1)
    expect(applicationEventBus.publishMessage).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'SanctionListCheckDone',
        data: {
          manuscriptId,
          authors: {
            [authorsInput[0].id]: {
              isOnBadDebtList: true,
              isOnBlackList: false,
              isOnWatchList: false,
            },
            [authorsInput[1].id]: {
              isOnBadDebtList: true,
              isOnBlackList: false,
              isOnWatchList: true,
            },
            [authorsInput[2].id]: {
              isOnBadDebtList: false,
              isOnBlackList: false,
              isOnWatchList: false,
            },
            [authorsInput[3].id]: {
              isOnBadDebtList: false,
              isOnBlackList: false,
              isOnWatchList: false,
            },
          },
        },
      }),
    )
  })
})
