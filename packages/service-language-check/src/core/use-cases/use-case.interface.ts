export interface UseCase<InputType, OutputType = void> {
  execute(input: InputType): Promise<OutputType>
}
