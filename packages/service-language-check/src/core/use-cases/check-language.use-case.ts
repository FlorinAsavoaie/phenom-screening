import { injectable, inject } from 'tsyringe'

import { Tokens } from '../../application/di/tokens'

import { LanguageService, WordCounterService } from '../services'
import { UseCase } from './use-case.interface'
import { FileRepository } from '../repositories'

interface CheckLanguageInput {
  textKey?: string
  text?: string
}

interface TextDetails {
  englishPercentage: number
  wordCount: number
}

export type CheckLanguageUseCase = UseCase<CheckLanguageInput, TextDetails>

@injectable()
export class CheckLanguageUseCaseImpl implements CheckLanguageUseCase {
  constructor(
    @inject(Tokens.FILE_REPOSITORY)
    private fileRepository: FileRepository,

    @inject(Tokens.LANGUAGE_SERVICE)
    private languageService: LanguageService,

    @inject(Tokens.WORD_COUNTER_SERVICE)
    private wordCounterService: WordCounterService,
  ) {}

  public async execute({
    textKey,
    text = '',
  }: CheckLanguageInput): Promise<TextDetails> {
    if (textKey) {
      text = await this.fileRepository.getContent(textKey)
    }
    let englishPercentage = 0
    englishPercentage = await this.languageService.getPercentage(text)

    const wordCount = this.wordCounterService.getForText(text)

    return {
      englishPercentage,
      wordCount,
    }
  }
}
