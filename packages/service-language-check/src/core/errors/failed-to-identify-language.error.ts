export class FailedToIdentifyLanguageError extends Error {
  constructor(message?: string) {
    super(message)
    this.name = 'FailedToIdentifyLanguageError'
  }
}
