export interface WordCounterService {
  getForText(text: string): number
}
