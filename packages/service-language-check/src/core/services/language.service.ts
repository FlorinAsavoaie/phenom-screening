export interface LanguageService {
  getPercentage(text: string): Promise<number>
}
