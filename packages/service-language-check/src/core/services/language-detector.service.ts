export interface LanguageDetails {
  name: string
  percent: number
}

export interface LanguageDetectorService {
  detect(text: string): Promise<LanguageDetails[]>
}
