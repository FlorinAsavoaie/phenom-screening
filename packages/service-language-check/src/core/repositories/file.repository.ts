export interface FileRepository {
  getContent: (key: string) => Promise<string>
}
