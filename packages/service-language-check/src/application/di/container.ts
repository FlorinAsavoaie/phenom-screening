import { container } from 'tsyringe'
import { CheckLanguageUseCaseImpl } from '../../core/use-cases'
import {
  configService,
  EnglishLanguageService,
  awsQueueServiceFactory,
  CompactLanguageDetectorService,
  RegexWordCounterService,
  LangDetectorService,
} from '../../infrastructure/services'
import { s3FileRepositoryFactory } from '../../infrastructure/repositories'
import { Tokens } from './tokens'
import { eventHandlers } from '../event-handlers'

async function registerServices(): Promise<void> {
  container.register(Tokens.CONFIG_SERVICE, {
    useValue: configService,
  })

  container.register(Tokens.LANGUAGE_DETECTOR_SERVICE, {
    useClass: CompactLanguageDetectorService,
  })

  container.register(Tokens.LANGUAGE_DETECTOR_ALTERNATIVE_SERVICE, {
    useClass: LangDetectorService,
  })

  container.register(Tokens.LANGUAGE_SERVICE, {
    useClass: EnglishLanguageService,
  })

  const queueService = await awsQueueServiceFactory(container)
  container.register(Tokens.QUEUE_SERVICE, {
    useValue: queueService,
  })

  container.register(Tokens.WORD_COUNTER_SERVICE, {
    useClass: RegexWordCounterService,
  })
}

function registerRepositories(): void {
  container.register(Tokens.FILE_REPOSITORY, {
    useFactory: s3FileRepositoryFactory,
  })
}

function registerUseCases(): void {
  container.register(Tokens.CHECK_LANGUAGE_USE_CASE, {
    useClass: CheckLanguageUseCaseImpl,
  })
}

function registerEventHandlers(): void {
  Object.entries(eventHandlers).forEach(([_, EventHandler]) => {
    container.register<any>(EventHandler, { useClass: EventHandler })
  })
}

async function initContainer(): Promise<void> {
  await registerServices()
  registerRepositories()
  registerUseCases()
  registerEventHandlers()
}

export { initContainer }
