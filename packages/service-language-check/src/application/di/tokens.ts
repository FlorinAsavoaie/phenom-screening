export const Tokens = {
  /* Services */
  CONFIG_SERVICE: Symbol.for('ConfigService'),
  LANGUAGE_DETECTOR_SERVICE: Symbol.for('LanguageDetectorService'),
  LANGUAGE_DETECTOR_ALTERNATIVE_SERVICE: Symbol.for(
    'LanguageDetectorAlternativeService',
  ),
  LANGUAGE_SERVICE: Symbol.for('LanguageService'),
  QUEUE_SERVICE: Symbol.for('QueueService'),
  WORD_COUNTER_SERVICE: Symbol.for('WordCounterService'),
  /* Repositories */
  FILE_REPOSITORY: Symbol.for('FileRepository'),
  /* UseCases */
  CHECK_LANGUAGE_USE_CASE: Symbol.for('CheckLanguageUseCase'),
}
