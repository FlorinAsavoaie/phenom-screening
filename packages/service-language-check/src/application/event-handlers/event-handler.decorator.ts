export interface EventHandler<InputType, OutputType = void> {
  eventName?: string
  handle: (data: InputType) => Promise<OutputType>
}

export function eventHandler(eventName: string): any {
  return function decorator(target: any): void {
    target.eventName = eventName
  }
}
