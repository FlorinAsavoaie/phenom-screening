import { container } from 'tsyringe'
import { QueueService } from '@hindawi/queue-service'

import { Tokens } from '../di/tokens'
import { eventHandlers } from '../event-handlers'

export async function registerEventHandlers(): Promise<void> {
  const queueService: QueueService = await container.resolve(
    Tokens.QUEUE_SERVICE,
  )

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Object.entries(eventHandlers).forEach(([_, handler]) => {
    const eventHandler = container.resolve(handler)
    queueService.registerEventHandler({
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      event: (handler as any).eventName,
      handler: eventHandler.handle.bind(eventHandler),
    })
  })
}
