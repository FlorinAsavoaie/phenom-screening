import { injectable, inject } from 'tsyringe'

import { eventHandler, EventHandler } from './event-handler.decorator'

import { Tokens } from '../di/tokens'
import { CheckLanguageUseCase } from '../../core/use-cases'
import { QueueService } from '../../core/services'

interface FileConvertedEventData {
  fileId: string
  manuscriptId: string
  convertedFileProviderKey: string
  text?: string
  textKey?: string
}

interface LanguageCheckedEventData {
  manuscriptId: string
  englishPercentage: number
  wordCount: number
}

@injectable()
@eventHandler('FileConverted')
export class FileConvertedEventHandler
  implements EventHandler<FileConvertedEventData>
{
  constructor(
    @inject(Tokens.CHECK_LANGUAGE_USE_CASE)
    private checkLanguageUseCase: CheckLanguageUseCase,

    @inject(Tokens.QUEUE_SERVICE)
    private queueService: QueueService,
  ) {}

  async handle(data: FileConvertedEventData): Promise<void> {
    const { englishPercentage, wordCount } =
      await this.checkLanguageUseCase.execute({
        text: data.text,
        textKey: data.textKey,
      })

    await this.queueService.publishMessage<LanguageCheckedEventData>({
      event: 'LanguageChecked',
      data: {
        manuscriptId: data.manuscriptId,
        englishPercentage,
        wordCount,
      },
    })
  }
}
