import { FileConvertedEventHandler } from './file-converted.handler'

export const eventHandlers = {
  FileConvertedEventHandler,
}

export { registerEventHandlers } from './register-event-handlers'
