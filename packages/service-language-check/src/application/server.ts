import { createServer, Server as HttpServer } from 'http'
import { container } from 'tsyringe'

import { Tokens } from './di/tokens'
import { initContainer } from './di/container'
import { registerEventHandlers } from './event-handlers'
import { ConfigService, QueueService } from '../core/services'
import logger from '../../config/loggerCustom'

export class ApplicationServer {
  private constructor(
    private readonly httpServer: HttpServer,
    private readonly queueService: QueueService,
    private readonly httpServerPort: number,
  ) {}

  start(): void {
    this.queueService.start()
    this.httpServer.listen(this.httpServerPort, () => {
      logger.info(
        `Service Language Check listening on port ${this.httpServerPort}.`,
      )
    })
  }

  static async create(): Promise<ApplicationServer> {
    await initContainer()
    await registerEventHandlers()

    const httpServer = ApplicationServer.createHttpServer()
    const queueService = await ApplicationServer.createQueueService()
    const httpServerPort = await ApplicationServer.getHttpServerPort()

    return new ApplicationServer(httpServer, queueService, httpServerPort)
  }

  private static createHttpServer(): HttpServer {
    const server = createServer((_, res) => {
      res.end('Service Language Check is UP')
    })
    return server
  }

  private static async createQueueService(): Promise<QueueService> {
    return container.resolve(Tokens.QUEUE_SERVICE)
  }

  private static async getHttpServerPort(): Promise<number> {
    const configService: ConfigService = await container.resolve(
      Tokens.CONFIG_SERVICE,
    )

    const portNumber = configService.get<string>('port')
    return parseInt(portNumber, 10)
  }
}
