/* eslint-disable */
require('dotenv').config()

import 'reflect-metadata'
import { ApplicationServer } from './application/server'

export async function main(): Promise<void> {
  const applicationServer = await ApplicationServer.create()

  applicationServer.start()
}
