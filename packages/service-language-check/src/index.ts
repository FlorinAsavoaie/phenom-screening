import { main } from './main'

main().catch((error) => {
  console.error('Unexpected error')
  console.error(error)
  process.exit(1)
})
