import { DependencyContainer } from 'tsyringe'

import { createQueueService } from '@hindawi/queue-service'

import { QueueService, ConfigService } from '../../core/services'
import { Tokens } from '../../application/di/tokens'
import logger from '../../../config/loggerCustom'

export async function awsQueueServiceFactory(
  dependencyContainer: DependencyContainer,
): Promise<QueueService> {
  const configService: ConfigService = dependencyContainer.resolve(
    Tokens.CONFIG_SERVICE,
  )

  return createQueueService(
    {
      region: configService.get('aws.region'),
      accessKeyId: configService.has('aws.sns_sqs.accessKeyId')
        ? configService.get('aws.sns_sqs.accessKeyId')
        : undefined,
      secretAccessKey: configService.has('aws.sns_sqs.secretAccessKey')
        ? configService.get('aws.sns_sqs.secretAccessKey')
        : undefined,
      snsEndpoint: configService.has('aws.sns.endpoint')
        ? configService.get('aws.sns.endpoint')
        : undefined,
      sqsEndpoint: configService.has('aws.sqs.endpoint')
        ? configService.get('aws.sqs.endpoint')
        : undefined,
      s3Endpoint: configService.has('largeEvents.s3Endpoint')
        ? configService.get('largeEvents.s3Endpoint')
        : undefined,
      topicName: configService.get('aws.sns.topic'),
      queueName: configService.get('aws.sqs.queueName'),
      bucketName: configService.get('largeEvents.bucketName'),
      bucketPrefix: configService.get('largeEvents.bucketPrefix'),
      eventNamespace: configService.get('eventNamespace'),
      publisherName: configService.get('publisherName'),
      serviceName: configService.get('serviceName'),
      defaultMessageAttributes: configService.get('defaultMessageAttributes'),
    },
    logger,
  )
}
