import { WordCounterService } from '../../core/services'

export class RegexWordCounterService implements WordCounterService {
  getForText(text: string): number {
    const matches = text.match(/\S+/gi)

    return matches ? matches.length : 0
  }
}
