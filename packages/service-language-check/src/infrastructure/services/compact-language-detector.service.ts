import { promisify } from 'util'
import { detect, DetectLanguage, Language } from 'cld'

import { LanguageDetectorService, LanguageDetails } from '../../core/services'
import { FailedToIdentifyLanguageError } from '../../core/errors'

export class CompactLanguageDetectorService implements LanguageDetectorService {
  static FAILED_TO_IDENTIFY_LANGUAGE_ERROR = 'Failed to identify language'

  async detect(text: string): Promise<LanguageDetails[]> {
    try {
      const detectLanguageResult = await this.detectLanguage(text)

      return detectLanguageResult.languages.map(
        (detectedLanguage: Language) => ({
          name: detectedLanguage.name,
          percent: detectedLanguage.percent,
        }),
      )
    } catch (err: any) {
      if (
        err.message ===
        CompactLanguageDetectorService.FAILED_TO_IDENTIFY_LANGUAGE_ERROR
      )
        throw new FailedToIdentifyLanguageError()
      throw new Error(err.message)
    }
  }

  private detectLanguage = promisify<string, DetectLanguage>(detect)
}
