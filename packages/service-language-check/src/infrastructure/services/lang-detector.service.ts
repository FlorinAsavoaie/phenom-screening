import { Language, detect } from 'langdetect'

import { LanguageDetails, LanguageDetectorService } from '../../core/services'

export class LangDetectorService implements LanguageDetectorService {
  detect(text: string): Promise<LanguageDetails[]> {
    const detectLanguageResult = this.detectedLanguage(text)

    return new Promise((resolve) => {
      resolve(
        detectLanguageResult.map((detectedLanguage: Language) => ({
          name: detectedLanguage.lang,
          percent: Math.round(detectedLanguage.prob * 100),
        })),
      )
    })
  }

  private detectedLanguage = detect
}
