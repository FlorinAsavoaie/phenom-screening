import { injectable, inject } from 'tsyringe'

import { get } from 'lodash'

import {
  LanguageService,
  LanguageDetectorService,
  LanguageDetails,
} from '../../core/services'
import { FailedToIdentifyLanguageError } from '../../core/errors'
import { Tokens } from '../../application/di/tokens'

@injectable()
export class EnglishLanguageService implements LanguageService {
  constructor(
    @inject(Tokens.LANGUAGE_DETECTOR_SERVICE)
    private languageDetectorService: LanguageDetectorService,
    @inject(Tokens.LANGUAGE_DETECTOR_ALTERNATIVE_SERVICE)
    private languageDetectorAlternativeService: LanguageDetectorService,
  ) {}

  static LANGUAGE_NAME_CLD = 'ENGLISH'
  static LANGUAGE_NAME_LANG_DETECT = 'en'

  private getEnglishPercentage(
    detectedLanguages: LanguageDetails[],
    languageCode: string,
  ): number {
    const englishLanguage = detectedLanguages.find(
      (l) => l.name === languageCode,
    )
    return englishLanguage ? get(englishLanguage, 'percent', 0) : 0
  }

  async getPercentage(text: string): Promise<number> {
    let englishPercentage = 0

    try {
      const detectedLanguages = await this.languageDetectorService.detect(text)

      englishPercentage = this.getEnglishPercentage(
        detectedLanguages,
        EnglishLanguageService.LANGUAGE_NAME_CLD,
      )
    } catch (err: any) {
      if (err instanceof FailedToIdentifyLanguageError) {
        const languages = await this.languageDetectorAlternativeService.detect(
          text,
        )

        if (languages) {
          englishPercentage = this.getEnglishPercentage(
            languages,
            EnglishLanguageService.LANGUAGE_NAME_LANG_DETECT,
          )
        }
      } else {
        throw new Error(err)
      }
    }

    return englishPercentage
  }
}
