import { DependencyContainer } from 'tsyringe'

import { S3 } from '@aws-sdk/client-s3'

import { ConfigService } from '../../core/services'
import { FileRepository } from '../../core/repositories'
import { Tokens } from '../../application/di/tokens'

export function s3FileRepositoryFactory(
  dependencyContainer: DependencyContainer,
): FileRepository {
  const configService: ConfigService = dependencyContainer.resolve(
    Tokens.CONFIG_SERVICE,
  )

  const region = configService.get<string>('aws.region')
  const credentials =
    configService.has('aws.s3.accessKeyId') &&
    configService.has('aws.s3.secretAccessKey')
      ? {
          accessKeyId: configService.get<string>('aws.s3.accessKeyId'),
          secretAccessKey: configService.get<string>('aws.s3.secretAccessKey'),
        }
      : undefined

  const Bucket = configService.get<string>('aws.s3.bucket')

  const s3 = new S3({
    region,
    credentials,
  })

  const streamToString = (stream: any): Promise<string> =>
    new Promise((resolve, reject) => {
      const chunks: any[] = []
      stream.on('data', (chunk: any) => chunks.push(chunk))
      stream.on('error', reject)
      stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
    })
  return {
    getContent: async (key) => {
      const object = await s3.getObject({
        Bucket,
        Key: key,
      })
      const content = await streamToString(object.Body)

      return content || ''
    },
  } as FileRepository
}
