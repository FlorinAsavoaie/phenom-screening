import {
  CheckLanguageUseCaseImpl,
  CheckLanguageUseCase,
} from '../../src/core/use-cases'
import { FileRepository } from '../../src/core/repositories/file.repository'
import { LanguageService, WordCounterService } from '../../src/core/services'

describe('CheckLanguage UseCase', () => {
  let checkLanguageUseCase: CheckLanguageUseCase

  const fileRepository: FileRepository = {
    getContent: () => {
      throw new Error('Not implemented')
    },
  }

  const languageService: LanguageService = {
    getPercentage: () => {
      throw new Error('Not implemented')
    },
  }

  const wordCounterService: WordCounterService = {
    getForText: () => {
      throw new Error('Not implemented')
    },
  }

  beforeEach(() => {
    jest.clearAllMocks()

    checkLanguageUseCase = new CheckLanguageUseCaseImpl(
      fileRepository,
      languageService,
      wordCounterService,
    )
  })

  it('should perform language check for object from s3 if textKey is provided', async () => {
    const someData = {
      manuscriptId: 'some-manuscript-id',
      textKey: 'some-text-key',
    }

    jest
      .spyOn(fileRepository, 'getContent')
      .mockResolvedValueOnce('some-text-body-from-s3')
    jest.spyOn(languageService, 'getPercentage').mockResolvedValueOnce(10)
    jest.spyOn(wordCounterService, 'getForText').mockReturnValueOnce(5)

    await checkLanguageUseCase.execute(someData)

    expect(fileRepository.getContent).toHaveBeenCalledTimes(1)
    expect(fileRepository.getContent).toHaveBeenCalledWith('some-text-key')

    expect(languageService.getPercentage).toHaveBeenCalledTimes(1)
    expect(languageService.getPercentage).toHaveBeenCalledWith(
      'some-text-body-from-s3',
    )

    expect(wordCounterService.getForText).toHaveBeenCalledTimes(1)
    expect(wordCounterService.getForText).toHaveBeenCalledWith(
      'some-text-body-from-s3',
    )
  })

  it('should perform language check for provided text if textKey is not provided', async () => {
    const someData = {
      manuscriptId: 'some-manuscript-id',
      text: 'some-text',
    }

    jest
      .spyOn(fileRepository, 'getContent')
      .mockResolvedValueOnce('some-text-body-from-s3')
    jest.spyOn(languageService, 'getPercentage').mockResolvedValueOnce(10)
    jest.spyOn(wordCounterService, 'getForText').mockReturnValueOnce(5)

    await checkLanguageUseCase.execute(someData)

    expect(fileRepository.getContent).not.toHaveBeenCalled()

    expect(languageService.getPercentage).toHaveBeenCalledTimes(1)
    expect(languageService.getPercentage).toHaveBeenCalledWith('some-text')

    expect(wordCounterService.getForText).toHaveBeenCalledTimes(1)
    expect(wordCounterService.getForText).toHaveBeenCalledWith('some-text')
  })
})
