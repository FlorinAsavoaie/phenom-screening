import {
  LanguageService,
  LanguageDetectorService,
} from '../../src/core/services'
import { EnglishLanguageService } from '../../src/infrastructure/services'
import { FailedToIdentifyLanguageError } from '../../src/core/errors'

describe('English Language Service', () => {
  let languageService: LanguageService

  const languageDetector: LanguageDetectorService = {
    detect: () => {
      throw new FailedToIdentifyLanguageError()
    },
  }
  const languageDetectorAlternative: LanguageDetectorService = {
    detect: () => {
      throw new Error('Not implemented')
    },
  }

  beforeEach(() => {
    jest.clearAllMocks()

    languageService = new EnglishLanguageService(
      languageDetector,
      languageDetectorAlternative,
    )
  })

  describe('getPercentage', () => {
    it('should return percentage found by cld detector', async () => {
      jest.spyOn(languageDetector, 'detect').mockResolvedValueOnce([
        {
          name: 'ENGLISH',
          percent: 25,
        },
      ])

      const englishPercentage = await languageService.getPercentage('some-text')
      expect(languageDetector.detect).toHaveBeenCalledTimes(1)
      expect(languageDetector.detect).toHaveBeenCalledWith('some-text')

      expect(englishPercentage).toBe(25)
    })

    it('should return default percentage if english language is not found', async () => {
      jest.spyOn(languageDetector, 'detect').mockResolvedValueOnce([])

      const englishPercentage = await languageService.getPercentage('some-text')
      expect(languageDetector.detect).toHaveBeenCalledTimes(1)
      expect(languageDetector.detect).toHaveBeenCalledWith('some-text')

      expect(englishPercentage).toBe(0)
    })

    it('should return percentage found by alternative lang detector if cld throws "Failed to identify language"', async () => {
      jest.spyOn(languageDetectorAlternative, 'detect').mockResolvedValueOnce([
        {
          name: 'en',
          percent: 85,
        },
      ])
      const englishPercentage = await languageService.getPercentage('some-text')
      expect(languageDetector.detect).toHaveBeenCalledTimes(1)
      expect(languageDetector.detect).toThrowError()
      expect(languageDetectorAlternative.detect).toHaveBeenCalledTimes(1)
      expect(languageDetectorAlternative.detect).toHaveBeenCalledWith(
        'some-text',
      )

      expect(englishPercentage).toBe(85)
    })
  })
})
