import { WordCounterService } from '../../src/core/services'
import { RegexWordCounterService } from '../../src/infrastructure/services'

describe('Regex Word Counter Service', () => {
  let wordCounterService: WordCounterService

  beforeEach(() => {
    jest.clearAllMocks()
    wordCounterService = new RegexWordCounterService()
  })

  describe('getWordCount', () => {
    it('should return 0 for empty', () => {
      const result = wordCounterService.getForText('')

      expect(result).toBe(0)
    })

    it('should return 3', () => {
      const result = wordCounterService.getForText('some words here')

      expect(result).toBe(3)
    })
  })
})
