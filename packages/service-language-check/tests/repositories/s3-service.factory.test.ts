import { noop } from 'lodash'
import { S3 } from '@aws-sdk/client-s3'
import { DependencyContainer } from 'tsyringe'
import { Readable } from 'stream'

import { s3FileRepositoryFactory } from '../../src/infrastructure/repositories'
import { Tokens } from '../../src/application/di/tokens'
import { FileRepository } from '../../src/core/repositories'

const mockReadStream = jest.fn().mockImplementation(() => {
  const readable = new Readable()
  readable.push('test')
  readable.push(null)

  return readable
})

const someObject = { Body: mockReadStream() }
const getObjectSpy = jest.fn().mockReturnValue(someObject)
const mockedS3 = {
  getObject: getObjectSpy,
}

jest.mock('@aws-sdk/client-s3', () => ({
  S3: jest.fn().mockImplementation(() => mockedS3),
}))

describe('S3 File Repository Factory', () => {
  let s3Service: FileRepository

  const dependencyContainer: any = {
    resolve: noop,
  }
  const configService: any = {
    get: noop,
    has: jest.fn().mockResolvedValue(true),
  }

  let dependencyContainerResolveSpy: jest.SpyInstance
  let configServiceGetSpy: jest.SpyInstance

  beforeEach(() => {
    jest.clearAllMocks()

    dependencyContainerResolveSpy = jest
      .spyOn(dependencyContainer, 'resolve')
      .mockReturnValueOnce(configService)

    configServiceGetSpy = jest
      .spyOn(configService, 'get')
      .mockReturnValueOnce('aws-region-var')
      .mockReturnValueOnce('aws-s3-access-key-id-var')
      .mockReturnValueOnce('aws-s3-secret-access-key-var')
      .mockReturnValueOnce('aws-s3-bucket-var')

    s3Service = s3FileRepositoryFactory(
      dependencyContainer as DependencyContainer,
    )
  })

  it('should resolve config service from the dependency container', () => {
    expect(dependencyContainerResolveSpy).toHaveBeenCalledTimes(1)
    expect(dependencyContainerResolveSpy).toHaveBeenCalledWith(
      Tokens.CONFIG_SERVICE,
    )
  })

  it('should instantiate s3', () => {
    expect((S3 as any).mock.calls[0][0]).toEqual({
      region: 'aws-region-var',
      credentials: {
        accessKeyId: 'aws-s3-access-key-id-var',
        secretAccessKey: 'aws-s3-secret-access-key-var',
      },
    })
    expect((S3 as any).mock.calls.length).toEqual(1)
  })

  it('should get object body by key', async () => {
    const objectBody = await s3Service.getContent('some-key')

    expect(getObjectSpy).toHaveBeenCalledTimes(1)
    expect(getObjectSpy).toHaveBeenCalledWith({
      Bucket: 'aws-s3-bucket-var',
      Key: 'some-key',
    })
    expect(getObjectSpy).toHaveBeenCalledTimes(1)
    expect(objectBody).toBe('test')
  })
})
