import { FileConvertedEventHandler } from '../../src/application/event-handlers/file-converted.handler'
import { CheckLanguageUseCase } from '../../src/core/use-cases'
import { QueueService } from '../../src/core/services'

describe('File Converted Handler', () => {
  let fileConvertedHandler: FileConvertedEventHandler

  const checkLanguageUseCase: CheckLanguageUseCase = {
    execute: () => {
      throw new Error('Not implemented')
    },
  }

  const queueService: QueueService = {
    publishMessage: () => {
      throw new Error('Not implemented')
    },
    registerEventHandler: () => {
      throw new Error('Not implemented')
    },
    start: () => {
      throw new Error('Not implemented')
    },
  }

  beforeEach(() => {
    fileConvertedHandler = new FileConvertedEventHandler(
      checkLanguageUseCase,
      queueService,
    )
  })

  it('should call checkLanguageUseCase and publish message', async () => {
    jest.spyOn(checkLanguageUseCase, 'execute').mockResolvedValueOnce({
      englishPercentage: 78,
      wordCount: 20000,
    })
    jest
      .spyOn(queueService, 'publishMessage')
      .mockResolvedValueOnce({ MessageId: 'some-message-id' })

    await fileConvertedHandler.handle({
      fileId: 'file-id-1',
      manuscriptId: 'manuscript-id-1',
      convertedFileProviderKey: 'convertedFileProviderKey',
      textKey: 'some-text-key',
    })

    expect(checkLanguageUseCase.execute).toHaveBeenCalledTimes(1)
    expect(checkLanguageUseCase.execute).toHaveBeenCalledWith({
      textKey: 'some-text-key',
    })

    expect(queueService.publishMessage).toHaveBeenCalledTimes(1)
    expect(queueService.publishMessage).toHaveBeenCalledWith({
      event: 'LanguageChecked',
      data: {
        manuscriptId: 'manuscript-id-1',
        englishPercentage: 78,
        wordCount: 20000,
      },
    })
  })
})
