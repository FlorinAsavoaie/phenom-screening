const { initializeRootLogger } = require('../../utils')

module.exports = initializeRootLogger('ServiceLanguageCheck')
