const path = require('path')

function getNameFromPackageJson() {
  const filePath = path.resolve(process.cwd(), 'package.json')
  // eslint-disable-next-line
  return require(filePath).name
}

module.exports = {
  port: process.env.PORT || 3000,
  aws: {
    region: process.env.AWS_REGION,
    s3: {
      accessKeyId: process.env.AWS_S3_ACCESS_KEY,
      secretAccessKey: process.env.AWS_S3_SECRET_KEY,
      bucket: process.env.AWS_S3_BUCKET,
    },
    sns_sqs: {
      accessKeyId: process.env.AWS_SNS_SQS_ACCESS_KEY,
      secretAccessKey: process.env.AWS_SNS_SQS_SECRET_KEY,
    },
    sqs: {
      endpoint: process.env.AWS_SQS_ENDPOINT,
      queueName: process.env.AWS_SQS_QUEUE_NAME,
    },
    sns: {
      endpoint: process.env.AWS_SNS_ENDPOINT,
      topic: process.env.AWS_SNS_TOPIC,
    },
  },
  eventNamespace: process.env.EVENT_NAMESPACE,
  publisherName: process.env.PUBLISHER_NAME,
  serviceName: getNameFromPackageJson(),
  defaultMessageAttributes: JSON.parse(
    process.env.DEFAULT_MESSAGE_ATTRIBUTES || '{}',
  ),
  largeEvents: {
    s3Endpoint: process.env.AWS_S3_ENDPOINT,
    bucketName: process.env.PHENOM_LARGE_EVENTS_BUCKET,
    bucketPrefix:
      process.env.PHENOM_LARGE_EVENTS_PREFIX || 'in-transit-events/',
  },
}
