import { WithAwsSecretsServiceProps } from '@hindawi/phenom-charts'
import { defaultValues } from '../../default'

export const values: WithAwsSecretsServiceProps = {
  ...defaultValues,
  secretNames: ['prod-gsw/screening/language-check'],
  serviceProps: {
    ...defaultValues.serviceProps,
    image: {
      repository:
        '918602980697.dkr.ecr.eu-west-1.amazonaws.com/service-language-check',
      tag: 'latest',
    },
    ingressOptions: {
      rules: [
        {
          host: 'language-check.gsw-prod.phenom.pub',
        },
      ],
    },
  },
}
