declare module '@hindawi/queue-service' {
  import { PublishResponse, MessageAttributeValue } from '@aws-sdk/client-sns'

  type MessageAttributeMap = { [key: string]: MessageAttributeValue }

  interface MessageConfig<T> {
    event: string
    data: T
    messageAttributes?: MessageAttributeMap
  }

  interface EventHandlerConfig {
    event: string
    handler: (data: any) => Promise<void>
  }

  interface QueueService {
    publishMessage: <T>(config: MessageConfig<T>) => Promise<PublishResponse>
    registerEventHandler: (config: EventHandlerConfig) => void
    start: () => void
  }

  type CustomLogger = {
    logger?: (serviceLabe: string) => {
      info: (...args: any[]) => void
      warn: (...args: any[]) => void
      debug: (...args: any[]) => void
      trace: (...args: any[]) => void
      error: (...args: any[]) => void
    }
  } & Console

  interface QueueServiceConfig {
    region: string
    accessKeyId: string | undefined
    secretAccessKey: string | undefined
    snsEndpoint?: string
    sqsEndpoint?: string
    s3Endpoint?: string
    topicName: string
    queueName: string
    bucketName: string
    bucketPrefix: string
    eventNamespace: string
    publisherName: string
    serviceName: string
    defaultMessageAttributes: string
    logger?: CustomLogger
  }

  export function createQueueService(
    config?: QueueServiceConfig,
    logger?: CustomLogger,
  ): Promise<QueueService>
}
