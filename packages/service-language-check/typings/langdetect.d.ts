declare module 'langdetect' {
  export interface Language {
    readonly lang: string
    readonly prob: number
  }

  export function detect(text: string): Language[]
}
