# xpub-screening

Editorial Quality Screening system used :

- By the screening teams to perform screening on the manuscripts
- By the admins to manage screening teams (user management)
- To provide the UI for managing the suspicious keywords and sanction lists (settings)

#### Starting up

1. Clone repo
2. Run `yarn install` in project root
3. Run `yarn start:services` to start all docker containers needed for the app. Like:
    1. postgres
    2. localstack - if you don't need to run localstack because it is already running from another project then
       run: `sh ./scripts/setup-localstack.sh` to create topic and queues for screening
4. Create `.env` files in `app-*` and `service-*` folders. Check their respective README for the required environment
   variables
5. If it's the first time you run an app run `yarn setupdb`. This will create all the databases necessary for the
   microservices.
6. To start services:
    1. Run `yarn start` (in root or app folder). You need to be logged in with the Development AWS account or use `yarn start:dev` that does this prior to starting the app
    2. Or run `docker-compose up <service-name>` (in root)
       For full functionality, you should have all services running
7. Create admin with: `yarn create-admin email firstName lastName password` or follow instruction below
8. Profit

### Run scripts

1. Create Admin:

- if user already exist in db or sso run :
  `yarn create-admin email`

- if it's a new user run:
  `yarn create-admin email firstName lastName password`

- or run this if you want user to add his own password (user will receive an email to add his password):
  `yarn create-admin email firstName lastName`

example: yarn create-admin john_smith@hindawi.com John Smith pass12345!
